# Deployments

## Staging
As soon as you merged a merge request to master it will be automatically deployed to staging\
https://staging.grit.sc

## Production
Once you tested everything in staging and ready to perform a production deploy, you need to create a tag. Go to 
application repository, switch to master branch, check the available tags, e.g.:
```
main$ git tag
0.0.1
0.0.2
0.0.3
0.0.4
0.0.5
``` 

Create a new tag and push it to the repository, e.g.:
```
git tag 0.0.6
git push origin 0.0.6
```

Once this is done, please go to the https://gitlab.com/grit5/main/-/pipelines and trigger manually:
`build_images_production` and `deploy_apps_production` jobs there. Once the pipeline is finished, you can check result at:
https://grit.sc

## Problems reporting

If there some problems occur during the build or deploy process, please contact DevOps guys, so that they can check and
assist.

# Local development via docker-compose

`COMPOSE_HTTP_TIMEOUT=200 docker-compose -f docker-compose.yml up -d --build` - run the app

`docker-compose down` - stop the app


## Create dump (backup) 
go to db container\
`docker exec -it cockroach bash`

create a backup file\
`./cockroach dump grit --insecure --host=localhost:26257 > ./cockroach-data/grit_backup.sql`

## Copy backups from the container to local mashine
To copy backup file from the container to localhost execute\
`docker cp cockroach:/cockroach/cockroach-data/grit_backup.sql ~/projects/main`\
[docker cp reference](https://docs.docker.com/engine/reference/commandline/cp/)

or for kubernetes\
`kubectl -n staging cp cockroachdb-0:/path/to/dump local/path`\



## Restore from dump 
check if .sql backup file exists in `./data/cockroach/` on your local mashine 

go to db container\
`docker exec -it cockroach bash`


and execute in `cockroach` container\
`./cockroach sql --insecure --database=grit < ./cockroach-data/grit_backup.sql`



## DROP DATABASE
go to db container\
`docker exec -it cockroach bash`


execute in `cockroach` container\
`./cockroach sql --insecure --database=grit --execute="DROP DATABASE IF EXISTS grit;"`

## GENERATE MIGRATION
go to node container:
`docker exec -it node bash`

`node_modules/typeorm/cli.js migration:create -n addLatLngToPhoto` - create an empty migration

`node_modules/typeorm/cli.js migration:generate -n addedSomeIndiciesToAccount` - generate migrations

`npm run db:migrate` or `node_modules/typeorm/cli.js migration:run` - run migrations

`npm run db:revert` or `node_modules/typeorm/cli.js migration:revert` - revert migrations

`node_modules/typeorm/cli.js schema:sync` - sync the schema

