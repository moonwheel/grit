use grit;

DELETE FROM t_business WHERE 1=1;
DELETE FROM t_address WHERE 1=1;
DELETE FROM t_person WHERE 1=1;
DELETE FROM t_account WHERE 1=1;
DELETE FROM t_article WHERE 1=1;
DELETE FROM t_photo WHERE 1=1;
DELETE FROM t_product_photo WHERE 1=1;
DELETE FROM t_product WHERE 1=1;
DELETE FROM t_service WHERE 1=1;
DELETE FROM t_text WHERE 1=1;
DELETE FROM t_video WHERE 1=1;
DELETE FROM t_followings WHERE 1=1;
DELETE FROM t_video_comment WHERE 1=1;
DELETE FROM t_product_variant WHERE 1=1;
DELETE FROM t_video_comment_replies WHERE 1=1;
DELETE FROM t_product_variant_t_product_photos WHERE 1=1;
DELETE FROM t_service_schedule WHERE 1=1;
DELETE FROM t_schedule_days WHERE 1=1;
DELETE FROM t_schedule_days_intervals WHERE 1=1;
DELETE FROM t_service_photo WHERE 1=1;
DELETE FROM t_video_comment_like WHERE 1=1;
DELETE FROM t_video_like WHERE 1=1;
DELETE FROM t_video_views_t_account where 1=1;

INSERT INTO t_business (id, photo, thumb, name, "pageName", phone, description, legal_notice, privacy, created, updated, deleted, "categoryId", "deliveryAddressId", "addressId") VALUES
	(1, NULL, NULL, 'Websailors', 'websailors', '12333322211', NULL, NULL, NULL, '2020-02-20 12:46:12.92+00:00', '2020-02-20 12:46:13.153+00:00', NULL, 2, NULL, NULL),
	(2, NULL, NULL, 'Ws2', 'ws2', '223323232', NULL, NULL, NULL, '2020-02-20 12:51:08.948+00:00', '2020-02-20 12:51:09.134+00:00', NULL, 2, NULL, NULL);

INSERT INTO t_address (id, type, "fullName", "careOf", street, appendix, zip, city, state, country, selected, created, updated, deleted, lat, lng, "personId", "businessId") VALUES
	(1, 'businessAddress', NULL, NULL, 'Str 1', NULL, 123333, 'Taganrog', NULL, NULL, false, '2020-02-20 12:46:13.034+00:00', NULL, NULL, NULL, NULL, NULL, 1),
	(2, 'businessAddress', NULL, NULL, 'Str 2', NULL, 2223233, 'Tgn', NULL, NULL, false, '2020-02-20 12:51:09.03+00:00', NULL, NULL, NULL, NULL, NULL, 2),
	(3, '', 'SergeyF', NULL, 'Petrovskaya 51', '', 347900, 'Taganrog', NULL, NULL, false, '2020-02-24 09:39:20.827669+00:00', NULL, NULL, 47.21204, 38.93392, NULL, NULL);

UPDATE t_address SET "businessId" = 1 WHERE id = 1;
UPDATE t_address SET "businessId" = 2 WHERE id = 2;

INSERT INTO t_person (id, photo, thumb, "pageName", "fullName", email, password, gender, birthday, residence, description, language, privacy, active, token, passtoken, "sellerRating", "sellerReviews", "tokenAttempts", "tokenAttemptsStamp", created, updated, deactivated, reactivationtoken, deleted, "deliveryAddressId") VALUES
	(1, NULL, NULL, 'sergey', 'Sergey', 'sergey.fegon@websailors.pro', '$argon2i$v=19$m=4096,t=3,p=1$9XbMcFLp9h1g2zQlmb+eTA$aShdkDGAJjYE++BSU6ovyMVyEzyJbD1YYih4rSTgg80', 'male', '1991-01-01T00:00:00.000+03:00', NULL, NULL, 'deutsch', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTgyODAyNzcyMTk1LCJpZGVudGlmaWVyIjoic2VyZ2V5LmZlZ29uQGdtYWlsLmNvbSJ9.ckpIvxxwVwF7k6ewxe7zZRB8wreUodqKfPTw48dg2vDj-MUx0Z7jz0YV2ZpXeBEygjvYSz-S_lPsubdlzF0qkir2flsN-AcZSlyUfiiSC9B4JHLeTD-p86iWkL2Ch63D1ULyvFmhy0djhSxKG85GpnfoOnQ-AsM-hR7SbJekLMARs71tyXYDykeg4dfo0x3eIhPw5t85K_7_GmMcIVf6uCSTRvSvTM6Wlvor8C_N3yF3swfopOH2sPQheH6W4vW0j5FNirb4-1kvkB-fKQz7oce0mmXdtJN2EjNZW6D0C__b28f4uvyHjVnMSuM-OND-APOLGLsReytRViguCowlAg', NULL, NULL, NULL, NULL, '2020-02-20 12:46:12.212+00:00', '2020-02-20 12:46:12.212+00:00', NULL, NULL, NULL, NULL, NULL),
	(2, NULL, NULL, 'sergeyF', 'SergeyF', 'ws.fegon@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$V8vipIXNIXtwppkk+UwI9Q$0w5ii69z8yCYB1RrvwRjnZIa6uxqcNfyUMYDzOo2C6A', 'male', '1992-01-01T00:00:00.000+02:00', NULL, NULL, 'deutsch', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTgyODAzMDY4MzkxLCJpZGVudGlmaWVyIjoid3MuZmVnb25AZ21haWwuY29tIn0.HRhhnXxkM5w34ceExZ8qXp4cZrpK1T-lvKmcHatAXOrHtIfGAeTnb4M4TbV2eRdAgDbKokZRBOmS6PLsBstSnl-pmi0lMjTRqY_TSb6VQT4-K7ZI5AZc4eJm1kVXT8XiERvhYnf83R-_6KuMqsGujk9fSsE9b2L9G3jFdPZS9OGZeGhNUzhb_aJ_UyychFVKuIT-ybCJUa6GzuFvY5X4EkjM0UoqWb-GbMGHwaLtOwulWKLIwfa9w4lIIv__kVkC8gsQYyyqjkD4wa9OxgqLqaMDZoq2unqpj2wga2_i6XfwpPxr-Fr5iQfo8_hB1t8L7XN6b5DSvmtgmwbdpjCcQw', NULL, NULL, NULL, NULL, '2020-02-20 12:51:08.401+00:00', '2020-02-20 12:51:08.401+00:00', NULL, NULL, NULL, NULL, NULL);

UPDATE t_address SET "personId" = 2 WHERE id = 3;

INSERT INTO t_account (id, created, updated, deleted, "personId", "businessId", role_id, "baseBusinessAccountId") VALUES
	(1, '2020-02-20 12:46:12.268+00:00', NULL, NULL, 1, NULL, NULL, NULL),
	(2, '2020-02-20 12:46:12.993+00:00', NULL, NULL, 1, 1, 2, NULL),
	(3, '2020-02-20 12:51:08.434+00:00', NULL, NULL, 2, NULL, NULL, NULL),
	(4, '2020-02-20 12:51:08.989+00:00', NULL, NULL, 2, 2, 2, NULL),
	(5, '2020-02-20 12:51:08.989+00:00', NULL, NULL, 1, 2, 2, NULL);

UPDATE t_account SET "baseBusinessAccountId" = 4 WHERE id = 5;

INSERT INTO t_article (id, cover, thumb, title, text, category, location, draft, created, updated, deleted, user_id) VALUES
	(1, 'http://localhost:9000/photo/photo-405opk1ck709i2ce-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709i2ce-thumb.jpg', 'Article 1', '<p>Article about my <strong>code</strong></p>', 'Education', NULL, false, '2020-02-24 09:31:57.080521+00:00', '2020-02-24 09:31:57.707+00:00', NULL, 3);

INSERT INTO t_photo (id, photo, thumb, title, description, category, location, draft, "filterName", created, updated, deleted, user_id) VALUES
	(1, 'http://localhost:9000/photo/photo-405opk1ck709h4kj-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709h4kj-thumb.jpg', 'Photo 1', 'asdadsasd', 'Education', 'Taganrog', false, 'normal', '2020-02-24 09:31:13.28149+00:00', '2020-02-24 09:31:13.955+00:00', NULL, 3);

INSERT INTO t_product (id, type, title, condition, price, quantity, category, description, "variantOptions", "deliveryOptions", "deliveryTime", "shippingCosts", "payerReturn", bank_id, active, draft, created, updated, deleted, user_id, business_id, shopcategory_id, address_id, cover_id) VALUES
	(1, 'productVariants', 'Producr', 'new', NULL, NULL, 'Category 1', '123', 'size', 'onlyShipping', 1, 1, 'buyer', NULL, true, false, '2020-02-24 09:37:28.411957+00:00', '2020-02-24 09:37:28.764+00:00', NULL, 3, NULL, NULL, NULL, NULL);

INSERT INTO t_product_photo (id, "photoUrl", "thumbUrl", "order", created, deleted, product_id) VALUES
	(1, 'http://localhost:9000/photo/photo-405opk1ck709p6e9-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709p6e9-thumb.jpg', 0, '2020-02-24 09:37:28.48076+00:00', NULL, 1);

INSERT INTO t_service (id, active, draft, title, price, quantity, "hourlyPrice", description, performance, category, bank_id, created, updated, deleted, user_id, address_id) VALUES
	(1, true, false, 'Development', 30, 1, false, '', 'provider', 'Other', NULL, '2020-02-24 09:40:48.366767+00:00', '2020-02-24 09:40:48.366767+00:00', NULL, 3, 3);

INSERT INTO t_text (id, text, draft, created, updated, deleted, user_id) VALUES
	(1, 'Text 123456', false, '2020-02-24 09:30:40.601136+00:00', NULL, NULL, 3);

INSERT INTO t_video (id, video, cover, thumb, title, description, duration, category, location, draft, created, updated, deleted, user_id) VALUES
	(1, 'http://localhost:9000/video/video-405opk1ck709izsu/video-405opk1ck709izsu_h264_master', 'http://localhost:9000/video/video-405opk1ck709izsu/video-405opk1ck709izsu_cover.jpg', NULL, 'Video1', 'Test', '02:50', 'TV & Movies', NULL, false, '2020-02-24 09:32:34.405046+00:00', '2020-02-24 09:33:46.525+00:00', NULL, 3),
	(2, 'http://localhost:9000/video/video-405opk1ck709izsu/video-405opk1ck709izsu_h264_master', 'http://localhost:9000/video/video-405opk1ck709izsu/video-405opk1ck709izsu_cover.jpg', NULL, 'Video2', 'Test', '02:50', 'TV & Movies', NULL, false, '2020-02-24 09:32:34.405', '2020-02-24 09:33:46.525', NULL, 1);

INSERT INTO t_followings (id, created, "accountId", "followedAccountId") VALUES
	(1, '2020-02-24 09:34:46.658+00:00', 1, 3);

INSERT INTO t_video_comment (id, text, created, updated, user_id, video_id) VALUES
	(1, e'Thats funny!', '2020-02-24 09:35:26.086491+00:00', NULL, 1, 1);

INSERT INTO t_video_comment_replies (id, text, created, updated, comment_id, user_id) VALUES
	(1, 'TY, Dude', '2020-02-24 09:41:49.126609+00:00', NULL, 1, 3);

INSERT INTO t_product_variant (id, size, color, flavor, material, price, quantity, enable, created, updated, deleted, product_id) VALUES
	(1, '1', NULL, NULL, NULL, 1, 2, true, '2020-02-24 09:37:28.561235+00:00', NULL, NULL, 1),
	(2, '2', NULL, NULL, NULL, 1, 2, true, '2020-02-24 09:37:28.663746+00:00', NULL, NULL, 1);

INSERT INTO t_product_variant_t_product_photos ("variantId", "photoId", "order", "productId") VALUES
	(1, 1, 0, 1),
	(2, 1, 0, 1);

INSERT INTO t_service_schedule (id, duration, break, created, updated, deleted, service_id) VALUES
	(1, '08:00', '01:00', '2020-02-24 09:40:48.577683+00:00', NULL, NULL, 1);

INSERT INTO t_schedule_days (id, "from", "to", created, updated, deleted, "scheduleId", "dayId") VALUES
	(1, '09:00:00', '18:00:00', '2020-02-24 09:40:48.650024+00:00', NULL, NULL, 1, 1),
	(2, '09:00:00', '18:00:00', '2020-02-24 09:40:48.725914+00:00', NULL, NULL, 1, 2);

INSERT INTO t_schedule_days_intervals (id, "from", "to", "scheduleByDayId") VALUES
	(1, '09:00:00', '17:00:00', 1),
	(2, '09:00:00', '17:00:00', 2);

INSERT INTO t_service_photo (id, link, thumb, "index", created, updated, deleted, service_id) VALUES
	(1, 'http://localhost:9000/photo/photo-405opk1ck709tgy3-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709tgy3-thumb.jpg', 0, '2020-02-24 09:40:48.425263+00:00', '2020-02-24 09:40:49.937+00:00', NULL, 1),
	(2, 'http://localhost:9000/photo/photo-405opk1ck709thil-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709thil-thumb.jpg', 1, '2020-02-24 09:40:48.487505+00:00', '2020-02-24 09:40:50.602+00:00', NULL, 1),
	(3, 'http://localhost:9000/photo/photo-405opk1ck709thse-photo.jpg', 'http://localhost:9000/photo/photo-405opk1ck709thse-thumb.jpg', 2, '2020-02-24 09:40:48.528917+00:00', '2020-02-24 09:40:50.986+00:00', NULL, 1);

INSERT INTO t_video_comment_like (id, created, updated, user_id, comment_id, reply_id) VALUES
	(1, '2020-02-24 09:35:33.630414+00:00', NULL, 1, 1, NULL),
	(2, '2020-02-24 09:41:51.604411+00:00', NULL, 3, NULL, 1),
	(3, '2020-02-24 09:41:55.422908+00:00', NULL, 3, 1, NULL),
	(4, '2020-02-24 09:42:09.671052+00:00', NULL, 1, NULL, 1);

INSERT INTO t_video_like (id, created, updated, user_id, video_id) VALUES
	(1, '2020-02-24 09:35:35.512363+00:00', NULL, 1, 1);
	

INSERT INTO t_video_views_t_account ("tVideoId", "tAccountId") VALUES
	(1, 1),
	(1, 2),
	(2, 1),
	(2, 3);
