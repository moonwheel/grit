module.exports = {
  apps: [
    {
      script: './build/index.js',
      exec_mode: 'cluster',
      instances: 'max',
      env_test: {
        name: 'prod',
        PORT: 4000,
        NODE_ENV: 'prod',
      },
    },
  ],
};
