# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2019-08-26
### Added
- Follow up functionality
### Changed
- Linter extending & code cleanup
- Video upload api improvements
- Return from gulp to simple npm build rules


## [0.3.0] - 2019-08-19
### Added
- Staging server configuration
- Linter added to CI (on git push events)
- JWT token refresh functionality
- Gulp tasks for project build & development
### Changed
- Api documentation improvements
- Database entities refactoring
- Authorization session & cookies functionality cleared
### Fixed
- Resend emails (incorrect email recipient)
- Authorization expiration (previously - http/500)
- Timeline data filtering (by content author)

## [0.2.0] - 2019-08-15
### Added
- Backend dockerization
- Continuous integration
### Changed
- Convert API documentation from ApiDocJS format to OpenApi, docker container for ReDocAPI
- Build script optimization
- Package.json cleanup
- Moved from nodemon to tsc-watch
- Initial project setup documentation

## [0.1.0] - 2019-08-05
Initial release

[0.4.0]: https://gitlab.com/grit1/grit-backend/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/grit1/grit-backend/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/grit1/grit-backend/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/grit1/grit-backend/-/tags/0.1.0
