const Joi = require("@hapi/joi"); 

const business = Joi.object().keys({
    name: Joi.string().required(),
    category: Joi.number().required(),
    description: Joi.string(),
    language: Joi.string(),
    privacy: Joi.string(),
    photo: Joi.string(),
    phone: Joi.string().required(),
});

export default (service: any): any => Joi.validate(service, business);
