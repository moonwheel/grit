const Joi = require("joi");


const serviceSchema: any = Joi.object().keys({
    title: Joi.string().required(),
    price: Joi.string().required(),
    category: Joi.string().required(),
    performance: Joi.string().required(),
    street: Joi.string().when("performance", {is: "customer", then: Joi.required()}),
    zip: Joi.string().when("performance", {is: "customer", then: Joi.required()}),
    city: Joi.string().when("performance", {is: "customer", then: Joi.required()}),
    duration: Joi.string().required(),
    break: Joi.string().required(),
    description: Joi.string(),
    hourlyPrice: Joi.boolean(),
    dateRange: Joi.array(),
});

export default (service: any): any => Joi.validate(service, serviceSchema);
