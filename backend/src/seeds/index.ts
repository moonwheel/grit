import { createConnection, getRepository } from "typeorm";
import categories from "./static/categories";
import roles from "./static/roles";
import days from "./static/days";
import dictionary from "./static/search_dictionary";
import reportTypes from "./static/reportTypes";
import reportCategories from "./static/reportsCategories";
import languages from "./static/languages";
import cancelOrderReasons from "./static/cancel_order_reasons";
import returnOrderReasons from "./static/return_order_reasons";
import cancelBookingReasons from "./static/cancel_booking_reason";
import { Seed } from "../interfaces/global/seed.interface";

async function plant(seed: Seed){
    try {
        const { entity, data } = seed;
        const repo = getRepository(entity);
        await repo.save(data.map((elem, index) => ({
            ...elem,
            id: index + 1
        })));
    } catch (error) {
        throw error;
    }
}

createConnection()
    .then(async () => {
        await plant(categories);
        await plant(roles);
        await plant(days);
        await plant(reportTypes);
        await plant(reportCategories);
        await plant(dictionary);
        await plant(languages);
        await plant(cancelOrderReasons);
        await plant(returnOrderReasons);
        await plant(cancelBookingReasons);
    })
    .catch(error => console.log(error));
