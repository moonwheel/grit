import { Category } from "../../entities/category";
import { Seed } from "../../interfaces/global/seed.interface";

const categories: Seed = {
    entity: Category,
    data: [
        {
            name: "Agriculture",
            type: "Producer/Merchant"
        },
        {
            name: "Electronics",
            type: "Producer/Merchant"
        },
        {
            name: "Erotic",
            type: "Producer/Merchant"
        },
        {
            name: "Fashion/Accessory",
            type: "Producer/Merchant"
        },
        {
            name: "Food/Beverage",
            type: "Producer/Merchant"
        },
        {
            name: "FMCG",
            type: "Producer/Merchant"
        },
        {
            name: "Furniture/Home",
            type: "Producer/Merchant"
        },
        {
            name: "Garden Supplies",
            type: "Producer/Merchant"
        },
        {
            name: "Materials",
            type: "Producer/Merchant"
        },
        {
            name: "Media",
            type: "Producer/Merchant"
        },
        {
            name: "Pet Supplies",
            type: "Producer/Merchant"
        },
        {
            name: "Pharmaceutical",
            type: "Producer/Merchant"
        },
        {
            name: "Real Estate",
            type: "Producer/Merchant"
        },
        {
            name: "Toys",
            type: "Producer/Merchant"
        },
        {
            name: "Vehicles",
            type: "Producer/Merchant"
        },
        {
            name: "Other",
            type: "Producer/Merchant"
        },
        {
            name: "Accommodation",
            type: "Service Provider"
        },
        {
            name: "Banking",
            type: "Service Provider"
        },
        {
            name: "Craft",
            type: "Service Provider"
        },
        {
            name: "Education",
            type: "Service Provider"
        },
        {
            name: "Energy",
            type: "Service Provider"
        },
        {
            name: "Engineering",
            type: "Service Provider"
        },
        {
            name: "Event",
            type: "Service Provider"
        },
        {
            name: "Gastronomy",
            type: "Service Provider"
        },
        {
            name: "Healthcare",
            type: "Service Provider"
        },
        {
            name: "Insurance",
            type: "Service Provider"
        },
        {
            name: "IT",
            type: "Service Provider"
        },
        {
            name: "Law",
            type: "Service Provider"
        },
        {
            name: "Leisure",
            type: "Service Provider"
        },
        {
            name: "Media",
            type: "Service Provider"
        },
        {
            name: "Transportation (Goods)",
            type: "Service Provider"
        },
        {
            name: "Transportation (Passengers)",
            type: "Service Provider"
        },
        {
            name: "Travel",
            type: "Service Provider"
        },
        {
            name: "Other",
            type: "Service Provider"
        },
        {
            name: "Aid Agency",
            type: "Organization"
        },
        {
            name: "Association",
            type: "Organization"
        },
        {
            name: "Governmental Institution",
            type: "Organization"
        },
        {
            name: "Political Party",
            type: "Organization"
        },
        {
            name: "University/School",
            type: "Organization"
        },
        {
            name: "Other",
            type: "Organization"
        },
    ],
};

export default categories;
