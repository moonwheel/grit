import { Seed } from "../../interfaces/global/seed.interface";
import { CancelOrderReason } from "../../entities/cancel_order_reason";

const cancelOrderReasons: Seed = {
    entity: CancelOrderReason,
    data: [
        {
            name: "I don’t like the product",
        },
        {
            name: "Poor quality",
        },
        {
            name: "Price-performance",
        },
        {
            name: "Wrong product",
        },
        {
            name: "Product damaged",
        },
        {
            name: "Product defective",
        },
        {
            name: "Delivery incomplete",
        },
        {
            name: "Delivered too late",
        },
        {
            name: "Other",
        },
    ],
};

export default cancelOrderReasons;
