import { Seed } from "../../interfaces/global/seed.interface";
import { ReportCategory } from "../../entities/report_category";

const reportCategories: Seed = {
    entity: ReportCategory,
    data: [
        {
            name: "Spam"
        },
        {
            name: "Insult"
        },
        {
            name: "Violence"
        },
        {
            name: "Pornography"
        },
        {
            name: "Terrorism"
        },
        {
            name: "Infringement"
        }
    ],
};

export default reportCategories;
