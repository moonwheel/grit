import { Role } from "../../entities/role";
import { Seed } from "../../interfaces/global/seed.interface";

const roles: Seed = {
    entity: Role,
    data: [
        {
            type: "creator",
        },
        {
            type: "admin",
        },
        {
            type: "viewer",
        }
    ]
};

export default roles;
