import { Seed } from "../../interfaces/global/seed.interface";
import { CancelBookingReason } from "../../entities/cancel_booking_reason";

const cancelOrderReasons: Seed = {
    entity: CancelBookingReason,
    data: [
        {
            name: "I don’t like the service",
        },
        {
            name: "Poor service",
        },
        {
            name: "Price-performance",
        },
        {
            name: "Other",
        },
    ],
};

export default cancelOrderReasons;
