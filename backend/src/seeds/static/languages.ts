import { Seed } from "../../interfaces/global/seed.interface";
import { Languages } from "../../entities/languages";

const languages: Seed = {
    entity: Languages,
    data: [
        {
            id: 1,
            value: 'german',
            display_name: 'German',
            default: true
        }, {
            id: 2,
            value: 'english',
            display_name: 'English',
            default: false
        }
    ],
};

export default languages