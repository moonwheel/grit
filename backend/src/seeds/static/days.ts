import { Day } from "../../entities/day";
import { Seed } from "../../interfaces/global/seed.interface";

const days: Seed = {
    entity: Day,
    data: [
        {
            day: "Monday",
        },
        {
            day: "Tuesday",
        },
        {
            day: "Wednesday",
        },
        {
            day: "Thursday",
        },
        {
            day: "Friday",
        },
        {
            day: "Saturday",
        },
        {
            day: "Sunday",
        }
    ],
};

export default days;
