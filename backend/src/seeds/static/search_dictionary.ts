import { SearchDictionary } from "../../entities/search_dictionary";
import { Seed } from "../../interfaces/global/seed.interface";

const words: Seed = {
    entity: SearchDictionary,
    data: [
        {
            word: "black",
            type: "color"
        },
        {
            word: "white",
            type: "color"
        },
        {
            word: "grey",
            type: "color"
        },
        {
            word: "red",
            type: "color"
        },
        {
            word: "blue",
            type: "color"
        },
        {
            word: "yellow",
            type: "color"
        },
        {
            word: "purple",
            type: "color"
        },
        {
            word: "green",
            type: "color"
        },
        {
            word: "brown",
            type: "color"
        },
        {
            word: "khaki",
            type: "color"
        },
        {
            word: "cotton",
            type: "material"
        },
        {
            word: "acrylic",
            type: "material"
        },
        {
            word: "alloy",
            type: "material"
        },
        {
            word: "aluminum",
            type: "material"
        },
        {
            word: "brass",
            type: "material"
        },
        {
            word: "brick",
            type: "material"
        },
        {
            word: "bronze",
            type: "material"
        },
        {
            word: "carbon",
            type: "material"
        },
        {
            word: "cardboard",
            type: "material"
        },
        {
            word: "cast",
            type: "material"
        },
        { 
            word: "iron",
            type: "material"
        },
        {
            word: "cement",
            type: "material"
        },
        {
            word: "ceramics",
            type: "material"
        },
        {
            word: "copper",
            type: "material"
        },
        {
            word: "diamond",
            type: "material"
        },
        {
            word: "epoxy",
            type: "material"
        },
        {
            word: "fiber",
            type: "material"
        },
        {
            word: "fiberglass",
            type: "material"
        },
        {
            word: "glass",
            type: "material"
        },
        {
            word: "glue",
            type: "material"
        },
        {
            word: "gold",
            type: "material"
        },
        {
            word: "leather",
            type: "material"
        },
        {
            word: "linen",
            type: "material"
        },
        {
            word: "nylon",
            type: "material"
        },
        {
            word: "paper",
            type: "material"
        },
        {
            word: "polyester",
            type: "material"
        },
        {
            word: "rubber",
            type: "material"
        },
        {
            word: "sand",
            type: "material"
        },
        {
            word: "silica",
            type: "material"
        },
        {
            word: "silver",
            type: "material"
        },
        {
            word: "skin",
            type: "material"
        },
        {
            word: "steel",
            type: "material"
        },
        {
            word: "stone",
            type: "material"
        },
        {
            word: "titanium",
            type: "material"
        },
        {
            word: "vinyl",
            type: "material"
        },
        {
            word: "viscose",
            type: "material"
        },
        {
            word: "wood",
            type: "material"
        },
        {
            word: "wool",
            type: "material"
        },
        {
            word: "denim",
            type: "material"
        },
        {
            word: "vanilla",
            type: "flavor"
        },
        {
            word: "chocolate",
            type: "flavor"
        },
        {
            word: "mint",
            type: "flavor"
        },
        {
            word: "strawberry",
            type: "flavor"
        },
        {
            word: "banana",
            type: "flavor"
        },
        {
            word: "apple",
            type: "flavor"
        },
        {
            word: "orange",
            type: "flavor"
        },
        {
            word: "lemon",
            type: "flavor"
        },
        {
            word: "honey",
            type: "flavor"
        },
        {
            word: "coffee",
            type: "flavor"
        }
    ],
};

export default words;
