import { Seed } from "../../interfaces/global/seed.interface";
import { ReturnOrderReason } from "../../entities/return_order_reason";

const returnOrderReasons: Seed = {
    entity: ReturnOrderReason,
    data: [
        {
            name: "I don’t like the product",
        },
        {
            name: "Poor quality",
        },
        {
            name: "Price-performance",
        },
        {
            name: "Wrong product",
        },
        {
            name: "Product damaged",
        },
        {
            name: "Product defective",
        },
        {
            name: "Delivery incomplete",
        },
        {
            name: "Delivered too late",
        },
        {
            name: "Other",
        },
    ],
};

export default returnOrderReasons;
