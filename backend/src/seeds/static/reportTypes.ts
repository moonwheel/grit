import { Seed } from "../../interfaces/global/seed.interface";
import { ReportType } from "../../entities/report_type";

const reportTypes: Seed = {
    entity: ReportType,
    data: [
        {
            name: "User"
        },
        {
            name: "Text"
        },
        {
            name: "Photo"
        },
        {
            name: "Video"
        },
        {
            name: "Article"
        },
        {
            name: "Product"
        },
        {
            name: "Service"
        },
        {
            name: "Comment"
        },
        {
            name: "Review"
        }
    ],
};

export default reportTypes;
