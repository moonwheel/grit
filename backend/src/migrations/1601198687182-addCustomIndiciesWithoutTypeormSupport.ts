import {MigrationInterface, QueryRunner} from "typeorm";

export class addCustomIndiciesWithoutTypeormSupport1601198687182 implements MigrationInterface {
    name = 'addCustomIndiciesWithoutTypeormSupport1601198687182'

    public async up(queryRunner: QueryRunner): Promise<any> {
        //  You have attempted to use a feature that is not yet implemented.\n' +
        // 'See: https://go.crdb.dev/issue-v/9682/v20.1
        // await queryRunner.query(`CREATE INDEX "person_lower_pagename_idx" ON "t_person" (LOWER("pageName"))`);
        // await queryRunner.query(`CREATE INDEX "business_lower_pagename_idx" ON "t_business" (LOWER("pageName"))`);


        //  You have attempted to use a feature that is not yet implemented.\n' +
        // https://go.crdb.dev/issue-v/9683/v20.1
        // await queryRunner.query(`CREATE INDEX "chatroom_user_deleted_isblocked_idx" ON "t_chatroom_user" (deleted, is_blocked) WHERE deleted IS NULL;`);

        await queryRunner.query(`CREATE INDEX "message_created_desc_idx" ON "t_message" (created DESC)`);
        await queryRunner.query(`CREATE INDEX "bookmarks_created_desc_idx" ON "t_bookmarks" (created DESC)`);
        await queryRunner.query(`CREATE INDEX "notifications_created_desc_idx" ON "t_notifications" (created DESC)`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // await queryRunner.query(`DROP INDEX "t_person"@"person_lower_pagename_idx" CASCADE`);
        // await queryRunner.query(`DROP INDEX "t_business"@"business_lower_pagename_idx" CASCADE`);

        // await queryRunner.query(`DROP INDEX "t_chatroom_user"@"chatroom_user_deleted_isblocked_idx" CASCADE`);

        await queryRunner.query(`DROP INDEX "t_message"@"message_created_desc_idx" CASCADE`);
        await queryRunner.query(`DROP INDEX "t_bookmarks"@"bookmarks_created_desc_idx" CASCADE`);
        await queryRunner.query(`DROP INDEX "t_notifications"@"notifications_created_desc_idx" CASCADE`);
    }

}
