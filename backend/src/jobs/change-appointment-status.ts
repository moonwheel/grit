import paymentProcess from "../../src/utils/payment-process";

export class ChangeAppointmentStatus {

    static changeStatus = async (): Promise<void> => {
        try {
            await paymentProcess.payMoneyAndChangeStatus();
        } catch (error) {
            console.log("changeStatus", error);
        }
    }

}