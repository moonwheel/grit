import { Business } from './../entities/business';
import { getConnection, getRepository, IsNull } from "typeorm";
import * as schedule from "node-schedule";
import { Person } from "../entities/person";
import { App } from "../config/app.config";
import { Account } from "../entities/account";

export class CompletelyRemoveUser {

    static jobs = {}

    static setJobForPerson = (personId, deactivetedTime) => {
        const delay = App.COMPLETELY_REMOVE_USER_DELAY_MS;

        // const delay = 60000; test delay (1 min)

        const dateToRemoveUser = new Date(new Date(deactivetedTime).getTime() + delay);
        console.log(`\n\n[ dateToRemovePERSON ] ${personId} = ${dateToRemoveUser}`);

        if(new Date() < dateToRemoveUser) {
            const job = schedule.scheduleJob(dateToRemoveUser, function(){
                console.log('the task executed at', new Date() )
                 // TODO: EXECUTE A FUNCTION TO REMOVE FROM THE DB

                CompletelyRemoveUser.removeUserQuery(personId)
            });
            CompletelyRemoveUser.jobs[personId] = job
            console.log('SET CompletelyRemoveUser.jobs', CompletelyRemoveUser.jobs)
        } else {
            console.log(`\n\n[ DELETE PERSON IMMEDIATELY ] ${personId} = ${dateToRemoveUser}`)
            // TODO: EXECUTE A FUNCTION TO REMOVE FROM THE DB

            CompletelyRemoveUser.removeUserQuery(personId)
        }

    }

    static getJobs = () => {
        console.log('GET CompletelyRemoveUser.jobs', CompletelyRemoveUser.jobs)
        return CompletelyRemoveUser.jobs
    }

    static cancelJobByPersonId = (personId) => {
        CompletelyRemoveUser.jobs[personId].cancel();
        console.log('CANCELED job for', personId);
        delete CompletelyRemoveUser.jobs[personId];
        console.log('CANCELED CompletelyRemoveUser.jobs', CompletelyRemoveUser.jobs)
    }

    static scheduleRemovingForAllDeactivatedPersons = async (): Promise<void> => {
        try {
            const deactivatedUsers = await getRepository(Person)
                .createQueryBuilder("person")
                .where("person.deactivated IS NOT NULL")
                .getMany();
            // console.log('deactivatedUsers', deactivatedUsers)

            deactivatedUsers.forEach((user) => {
                CompletelyRemoveUser.setJobForPerson(user.id, user.deactivated)
            })
        } catch (error) {
            console.log('runCron error', error)
        }

    }

    static removeUserQuery = async (personId) => {
        console.log('[[[ COMPLETELY REMOVE USER FROM THE DB]]]', personId)

        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Account)
                .where('"personId" = :id AND "businessId" IS NULL', { id: personId })
                .execute();
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Person)
                .where("id = :id AND deactivated IS NOT NULL", { id: personId })
                .execute();

        } catch (error) {
            console.log('removeUserQuery error', error)
        }
    }


    // Business
    static setJobForBusiness = (businessId, deactivetedTime) => {
        const delay = App.COMPLETELY_REMOVE_USER_DELAY_MS;

        // const delay = 60000; // test delay (1 min)

        const dateToRemoveUser = new Date(new Date(deactivetedTime).getTime() + delay);
        console.log(`\n\n[ dateToRemove BUSINESS ] ${businessId} = ${dateToRemoveUser}`);

        if(new Date() < dateToRemoveUser) {
            const job = schedule.scheduleJob(dateToRemoveUser, function(){
                console.log('the task executed at', new Date() )
                 // TODO: EXECUTE A FUNCTION TO REMOVE FROM THE DB

                CompletelyRemoveUser.removeBusinessQuery(businessId)
            });
            CompletelyRemoveUser.jobs['business-'+businessId] = job
            console.log('SET CompletelyRemoveUser.jobs', CompletelyRemoveUser.jobs)
        } else {
            console.log(`\n\n[ DELETE BUSINESS IMMEDIATELY ] ${businessId} = ${dateToRemoveUser}`)
            // TODO: EXECUTE A FUNCTION TO REMOVE FROM THE DB

            CompletelyRemoveUser.removeBusinessQuery(businessId)
        }

    }

    static removeBusinessQuery = async (businessId) => {
        console.log('[[[ COMPLETELY REMOVE USER FROM THE DB]]]', businessId)

        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Account)
                .where('"businessId" = :id AND baseBusinessAccount  IS NOT NULL', { id: businessId })
                .execute();
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Account)
                .where('"businessId" = :id AND baseBusinessAccount  IS NULL', { id: businessId })
                .execute();
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Business)
                .where("id = :id AND deleted IS NOT NULL", { id: businessId })
                .execute();

        } catch (error) {
            console.log('removeUserQuery error', error)
        }
    }

    static cancelJobByBusinessId = (businessId) => {
        CompletelyRemoveUser.jobs['business-'+businessId].cancel();
        console.log('CANCELED job for busines', businessId);
        delete CompletelyRemoveUser.jobs['business-'+businessId];
        console.log('CANCELED CompletelyRemoveUser.jobs', CompletelyRemoveUser.jobs)
    }

    static scheduleRemovingForAllDeactivatedBusiness = async (): Promise<void> => {
        try {
            const deactivatedUsers = await getRepository(Business)
                .createQueryBuilder("business")
                .where("business.deleted IS NOT NULL")
                .getMany();
            // console.log('deactivatedUsers', deactivatedUsers)

            deactivatedUsers.forEach((user) => {
                CompletelyRemoveUser.setJobForBusiness(user.id, user.deleted)
            })
        } catch (error) {
            console.log('runCron error', error)
        }

    }

}


