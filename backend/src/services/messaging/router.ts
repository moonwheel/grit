const express = require("express");
const router = express.Router();
const passport = require("passport");

import {MessagingController} from "./controller";



router.get("/chatrooms", passport.authenticate("jwt", {session: false}), MessagingController.getChatroomList);

router.get("/chatrooms/unread_amount", passport.authenticate("jwt", {session: false}), MessagingController.getUnreadChatroomAmount);

router.get("/chatrooms/:chatroom_id", passport.authenticate("jwt", {session: false}), MessagingController.getChatroomInfoByChatroomId);

router.get("/chatrooms/:chatroom_id/messages", passport.authenticate("jwt", {session: false}), MessagingController.getMessagesByChatroomId);

router.post("/chatrooms/checkBlocks", passport.authenticate("jwt", {session: false}), MessagingController.checkBlocks);

router.post("/chatrooms/find", passport.authenticate("jwt", {session: false}), MessagingController.findChatroom);

router.post("/chatrooms/share", passport.authenticate("jwt", {session: false}), MessagingController.shareWithUsers);

router.post("/chatrooms", passport.authenticate("jwt", {session: false}), MessagingController.addChatroom);

router.delete("/chatrooms/:chatroom_id", passport.authenticate("jwt", {session: false}), MessagingController.deleteChatroom);

router.post("/chatrooms/:chatroom_id/messages", passport.authenticate("jwt", {session: false}), MessagingController.addMessage);

router.delete("/chatrooms/:chatroom_id/messages/:message_id", passport.authenticate("jwt", {session: false}), MessagingController.deleteMessage);

router.put("/chatrooms/:chatroom_id/messages/:message_id", passport.authenticate("jwt", {session: false}), MessagingController.editMessage);

router.use("/chatrooms/:chatroom_id/upload", passport.authenticate("jwt", {session: false}), MessagingController.uploadAttachment);

router.put("/chatrooms/:chatroom_id/read", passport.authenticate("jwt", {session: false}), MessagingController.readMessages);

router.put("/chatrooms/:chatroom_id/mute", passport.authenticate("jwt", {session: false}), MessagingController.muteUnmuteChat);

// // router.get("/following/requests", authenticate("jwt", {session: false}), FollowingController.getOutgoingFollowingsRequests);

// router.get("/followers", authenticate("jwt", {session: false}), FollowingController.getFollowers);

// router.get("/followers/requests", authenticate("jwt", {session: false}), FollowingController.getIncomingFollowingsRequests);

// router.put("/followers/requests/approve/:requestId", authenticate("jwt", {session: false}), FollowingController.approveIncomingFollowingRequest);

// router.put("/followers/requests/reject/:requestId", authenticate("jwt", {session: false}), FollowingController.rejectIncomingFollowingRequest);

// router.get("/:targetUserId/followers", authenticate("jwt", {session: false}), FollowingController.getFollowers);

// router.post("/following/:followId", authenticate("jwt", {session: false}), FollowingController.follow);

// router.delete("/following/:followId", authenticate("jwt", {session: false}), FollowingController.unfollow);

export = router;

