export const getMessagesByChatroomIdRawQuery = (chatroomId: string, limit: number, offset: number, search?: string): string => {
  const rawQuery = `
    select
      "messages"."id" as "messages_id",
      "messages"."text" as "messages_text",
      "messages"."is_forwarded" as "messages_is_forwarded",
      "messages"."is_service_message" as "messages_is_service_message",
      "messages"."created" as "messages_created",
      "messages"."updated" as "messages_updated",
      "reply_to"."id" as "reply_to_id",
      "reply_to"."text" as "reply_to_text",
      "reply_to"."is_forwarded" as "reply_to_is_forwarded",
      "reply_to"."is_service_message" as "reply_to_is_service_message",
      "reply_to"."created" as "reply_to_created",
      "reply_to"."updated" as "reply_to_updated",
      "attachment"."id" as "attachment_id",
      "attachment"."link" as "attachment_link",
      "attachment"."thumb" as "attachment_thumb",
      "attachment"."duration" as "attachment_duration",
      "attachment"."file_name" as "attachment_file_name",
      "attachment"."type" as "attachment_type",
      "attachment"."created" as "attachment_created",
      "shared_user"."id" as "shared_user_id",
      "shared_user_business"."id" as "shared_user_business_id",
      "shared_user_business"."photo" as "shared_user_business_photo",
      "shared_user_business"."name" as "shared_user_business_name",
      "shared_user_business"."pageName" as "shared_user_business_pageName",
      "shared_user_person"."id" as "shared_user_person_id",
      "shared_user_person"."photo" as "shared_user_person_photo",
      "shared_user_person"."pageName" as "shared_user_person_pageName",
      "shared_user_person"."fullName" as "shared_user_person_fullName",
      "shared_text"."id" as "shared_text_id",
      "shared_text"."text" as "shared_text_text",
      "shared_text"."created" as "shared_text_created",
      "shared_text"."updated" as "shared_text_updated",
      "shared_text_user"."id" as "shared_text_user_id",
      "shared_text_user_business"."id" as "shared_text_user_business_id",
      "shared_text_user_business"."photo" as "shared_text_user_business_photo",
      "shared_text_user_business"."name" as "shared_text_user_business_name",
      "shared_text_user_business"."pageName" as "shared_text_user_business_pageName",
      "shared_text_user_person"."id" as "shared_text_user_person_id",
      "shared_text_user_person"."photo" as "shared_text_user_person_photo",
      "shared_text_user_person"."pageName" as "shared_text_user_person_pageName",
      "shared_text_user_person"."fullName" as "shared_text_user_person_fullName",
      "shared_photo"."id" as "shared_photo_id",
      "shared_photo"."photo" as "shared_photo_photo",
      "shared_photo"."thumb" as "shared_photo_thumb",
      "shared_photo"."title" as "shared_photo_title",
      "shared_photo"."description" as "shared_photo_description",
      "shared_photo"."category" as "shared_photo_category",
      "shared_photo"."location" as "shared_photo_location",
      "shared_photo"."filterName" as "shared_photo_filterName",
      "shared_photo"."created" as "shared_photo_created",
      "shared_photo"."updated" as "shared_photo_updated",
      "shared_photo_user"."id" as "shared_photo_user_id",
      "shared_photo_user_business"."id" as "shared_photo_user_business_id",
      "shared_photo_user_business"."photo" as "shared_photo_user_business_photo",
      "shared_photo_user_business"."name" as "shared_photo_user_business_name",
      "shared_photo_user_business"."pageName" as "shared_photo_user_business_pageName",
      "shared_photo_user_person"."id" as "shared_photo_user_person_id",
      "shared_photo_user_person"."photo" as "shared_photo_user_person_photo",
      "shared_photo_user_person"."pageName" as "shared_photo_user_person_pageName",
      "shared_photo_user_person"."fullName" as "shared_photo_user_person_fullName",
      "shared_video"."id" as "shared_video_id",
      "shared_video"."video" as "shared_video_video",
      "shared_video"."cover" as "shared_video_cover",
      "shared_video"."thumb" as "shared_video_thumb",
      "shared_video"."title" as "shared_video_title",
      "shared_video"."description" as "shared_video_description",
      "shared_video"."duration" as "shared_video_duration",
      "shared_video"."category" as "shared_video_category",
      "shared_video"."location" as "shared_video_location",
      "shared_video"."created" as "shared_video_created",
      "shared_video"."updated" as "shared_video_updated",
      "shared_video_user"."id" as "shared_video_user_id",
      "shared_video_user_business"."id" as "shared_video_user_business_id",
      "shared_video_user_business"."photo" as "shared_video_user_business_photo",
      "shared_video_user_business"."name" as "shared_video_user_business_name",
      "shared_video_user_business"."pageName" as "shared_video_user_business_pageName",
      "shared_video_user_person"."id" as "shared_video_user_person_id",
      "shared_video_user_person"."photo" as "shared_video_user_person_photo",
      "shared_video_user_person"."pageName" as "shared_video_user_person_pageName",
      "shared_video_user_person"."fullName" as "shared_video_user_person_fullName",
      "shared_article"."id" as "shared_article_id",
      "shared_article"."cover" as "shared_article_cover",
      "shared_article"."thumb" as "shared_article_thumb",
      "shared_article"."title" as "shared_article_title",
      "shared_article"."text" as "shared_article_text",
      "shared_article"."category" as "shared_article_category",
      "shared_article"."location" as "shared_article_location",
      "shared_article"."created" as "shared_article_created",
      "shared_article"."updated" as "shared_article_updated",
      "shared_article_user"."id" as "shared_article_user_id",
      "shared_article_user_business"."id" as "shared_article_user_business_id",
      "shared_article_user_business"."photo" as "shared_article_user_business_photo",
      "shared_article_user_business"."name" as "shared_article_user_business_name",
      "shared_article_user_business"."pageName" as "shared_article_user_business_pageName",
      "shared_article_user_person"."id" as "shared_article_user_person_id",
      "shared_article_user_person"."photo" as "shared_article_user_person_photo",
      "shared_article_user_person"."pageName" as "shared_article_user_person_pageName",
      "shared_article_user_person"."fullName" as "shared_article_user_person_fullName",
      "shared_product"."id" as "shared_product_id",
      "shared_product"."type" as "shared_product_type",
      "shared_product"."title" as "shared_product_title",
      "shared_product"."condition" as "shared_product_condition",
      "shared_product"."price" as "shared_product_price",
      "shared_product"."quantity" as "shared_product_quantity",
      "shared_product"."category" as "shared_product_category",
      "shared_product"."description" as "shared_product_description",
      "shared_product"."variantOptions" as "shared_product_variantOptions",
      "shared_product"."deliveryOptions" as "shared_product_deliveryOptions",
      "shared_product"."deliveryTime" as "shared_product_deliveryTime",
      "shared_product"."shippingCosts" as "shared_product_shippingCosts",
      "shared_product"."payerReturn" as "shared_product_payerReturn",
      "shared_product"."active" as "shared_product_active",
      "shared_product"."created" as "shared_product_created",
      "shared_product"."updated" as "shared_product_updated",
      "shared_product_user"."id" as "shared_product_user_id",
      "shared_product_user_business"."id" as "shared_product_user_business_id",
      "shared_product_user_business"."photo" as "shared_product_user_business_photo",
      "shared_product_user_business"."name" as "shared_product_user_business_name",
      "shared_product_user_business"."pageName" as "shared_product_user_business_pageName",
      "shared_product_user_person"."id" as "shared_product_user_person_id",
      "shared_product_user_person"."photo" as "shared_product_user_person_photo",
      "shared_product_user_person"."pageName" as "shared_product_user_person_pageName",
      "shared_product_user_person"."fullName" as "shared_product_user_person_fullName",
      "shared_service"."id" as "shared_service_id",
      "shared_service"."active" as "shared_service_active",
      "shared_service"."title" as "shared_service_title",
      "shared_service"."price" as "shared_service_price",
      "shared_service"."quantity" as "shared_service_quantity",
      "shared_service"."hourlyPrice" as "shared_service_hourlyPrice",
      "shared_service"."description" as "shared_service_description",
      "shared_service"."performance" as "shared_service_performance",
      "shared_service"."category" as "shared_service_category",
      "shared_service"."created" as "shared_service_created",
      "shared_service"."updated" as "shared_service_updated",
      "shared_service_user"."id" as "shared_service_user_id",
      "shared_service_user_business"."id" as "shared_service_user_business_id",
      "shared_service_user_business"."photo" as "shared_service_user_business_photo",
      "shared_service_user_business"."name" as "shared_service_user_business_name",
      "shared_service_user_business"."pageName" as "shared_service_user_business_pageName",
      "shared_service_user_person"."id" as "shared_service_user_person_id",
      "shared_service_user_person"."photo" as "shared_service_user_person_photo",
      "shared_service_user_person"."pageName" as "shared_service_user_person_pageName",
      "shared_service_user_person"."fullName" as "shared_service_user_person_fullName",
      "author"."id" as "author_id",
      "author_business"."id" as "author_business_id",
      "author_business"."photo" as "author_business_photo",
      "author_business"."name" as "author_business_name",
      "author_business"."pageName" as "author_business_pageName",
      "author_person"."id" as "author_person_id",
      "author_person"."photo" as "author_person_photo",
      "author_person"."pageName" as "author_person_pageName",
      "author_person"."fullName" as "author_person_fullName"
    from
      "t_message" "messages"
    left join "t_message" "reply_to" on
      "reply_to"."id" = "messages"."reply_to"
    left join "t_message_attachment" "attachment" on
      "attachment"."message_id" = "messages"."id"
    left join "t_account" "shared_user" on
      "shared_user"."id" = "messages"."shared_user_id"
    left join "t_business" "shared_user_business" on
      "shared_user_business"."id" = "shared_user"."businessId"
    left join "t_person" "shared_user_person" on
      "shared_user_person"."id" = "shared_user"."personId"
    left join "t_text" "shared_text" on
      "shared_text"."id" = "messages"."shared_text_id"
    left join "t_account" "shared_text_user" on
      "shared_text_user"."id" = "shared_text"."user_id"
    left join "t_business" "shared_text_user_business" on
      "shared_text_user_business"."id" = "shared_text_user"."businessId"
    left join "t_person" "shared_text_user_person" on
      "shared_text_user_person"."id" = "shared_text_user"."personId"
    left join "t_photo" "shared_photo" on
      "shared_photo"."id" = "messages"."shared_photo_id"
    left join "t_account" "shared_photo_user" on
      "shared_photo_user"."id" = "shared_photo"."user_id"
    left join "t_business" "shared_photo_user_business" on
      "shared_photo_user_business"."id" = "shared_photo_user"."businessId"
    left join "t_person" "shared_photo_user_person" on
      "shared_photo_user_person"."id" = "shared_photo_user"."personId"
    left join "t_video" "shared_video" on
      "shared_video"."id" = "messages"."shared_video_id"
    left join "t_account" "shared_video_user" on
      "shared_video_user"."id" = "shared_video"."user_id"
    left join "t_business" "shared_video_user_business" on
      "shared_video_user_business"."id" = "shared_video_user"."businessId"
    left join "t_person" "shared_video_user_person" on
      "shared_video_user_person"."id" = "shared_video_user"."personId"
    left join "t_article" "shared_article" on
      "shared_article"."id" = "messages"."shared_article_id"
    left join "t_account" "shared_article_user" on
      "shared_article_user"."id" = "shared_article"."user_id"
    left join "t_business" "shared_article_user_business" on
      "shared_article_user_business"."id" = "shared_article_user"."businessId"
    left join "t_person" "shared_article_user_person" on
      "shared_article_user_person"."id" = "shared_article_user"."personId"
    left join "t_product" "shared_product" on
      "shared_product"."id" = "messages"."shared_product_id"
    left join "t_account" "shared_product_user" on
      "shared_product_user"."id" = "shared_product"."user_id"
    left join "t_business" "shared_product_user_business" on
      "shared_product_user_business"."id" = "shared_product_user"."businessId"
    left join "t_person" "shared_product_user_person" on
      "shared_product_user_person"."id" = "shared_product_user"."personId"
    left join "t_service" "shared_service" on
      "shared_service"."id" = "messages"."shared_service_id"
    left join "t_account" "shared_service_user" on
      "shared_service_user"."id" = "shared_service"."user_id"
    left join "t_business" "shared_service_user_business" on
      "shared_service_user_business"."id" = "shared_service_user"."businessId"
    left join "t_person" "shared_service_user_person" on
      "shared_service_user_person"."id" = "shared_service_user"."personId"
    left join "t_account" "author" on
      "author"."id" = "messages"."user_id"
    left join "t_business" "author_business" on
      "author_business"."id" = "author"."businessId"
    left join "t_person" "author_person" on
      "author_person"."id" = "author"."personId"
    where
      "messages"."chatroom_id" = ${chatroomId}
      and "messages"."deleted" is null
      ${search ? `and (LOWER(messages.text) SIMILAR TO '%${search.toLowerCase()}%')` : ''}
    order by
      "messages"."created" desc
    limit ${limit}
    offset ${offset}
  `;
  return rawQuery
}
