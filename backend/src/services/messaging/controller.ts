import { Blacklist } from './../../entities/blacklist';
import { Request, Response } from 'express';
import { getRepository } from "typeorm";
import {intersection, isEqual} from "lodash"
import * as fs from 'fs';
import uniqid = require("uniqid");
const axios = require("axios");
const http = require("http");

import { tusServer, TUS_EVENTS, decodeMetadata } from "../../utils/tus-server";
import { MessageAttachment } from './../../entities/message_attachment';
import { Message } from './../../entities/message';
import { ChatroomUser } from './../../entities/chatroom_user';
import { Chatroom } from '../../entities/chatroom';
import { Account } from '../../entities/account';
import getBlockRepository from '../../entities/repositories/block-repository';
import { AttachmentType } from '../../entities/message_attachment';
import { socketIoServer as io } from '../../utils/websocket-server';
import getAccountRepository from '../../entities/repositories/account-repository';
import { MediaConverter } from '../../utils/media-converter';
import { App } from '../../config/app.config';
import { publish } from '../../utils/sse';
import { VideoController } from '../video/controller';
import MinioInstance from '../../utils/minio';
import { getMessagesByChatroomIdRawQuery } from './raw_queries';
import { sanitizeRawData } from '../../utils/helpers';

// import getFollowingRepository from "../../entities/repositories/following-repository";
// import getFollowingRequestRepository from "../../entities/repositories/following-request-repository";
// import AccountRepository from "../../entities/repositories/account-repository";
// import { Followings_request, RequestStatus } from "../../entities/followings_request";
// import { App } from "../../config/app.config";
// import { publish } from "../../utils/sse";
// import { NotificationController } from "../notifications/controller";
// import getAccountRepository from "../../entities/repositories/account-repository";
// import {Account} from "../../entities/account";

interface AttachmentData {
    link?: string,
    thumb?: string,
    type?: AttachmentType,
    duration?: string,
    file_name?: string,
}

interface NewMessageResponse extends Omit<Message, "updateDates"> {
    user: Account,
    attachmentData?: AttachmentData,
    temporaryId?: string,
}

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata);
    // console.log('attachment upload metadata', metadata)
    // console.log('attachment upload event', event)
    if (metadata.entityType === 'photo-attachment' || metadata.entityType === 'video-attachment' || metadata.entityType === 'document-attachment') {
        const tmpFileId = event.file.id
        await MessagingController.onUploadEnd(tmpFileId, metadata)
    }
});
export class MessagingController {

    static uploadAttachment = async (req: Request, res: Response) => {
        if (+req.headers['upload-length'] > App.MSG_FILE_SIZE_LIMIT * 1024 * 1024) {
            return res.status(400).send(`A file can not be larger than ${App.MSG_FILE_SIZE_LIMIT} Mb`);
        }
        return tusServer.handle(req, res);
    };

    // TODO: THIS FUNCTION HASN'T BEEN TESTED YET. TEST AND DEBUG ONCE THE FRONTEND IS READY
    static onUploadEnd = async (tmpFileId, metadata): Promise<AttachmentData[]> => {
        console.log('upload end', tmpFileId, metadata)
        // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
        const { id, type, userId, name, temporaryId } = metadata; // id === chatroomId
        const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;
        const fileType = type; // for example image/jpg
        const splitName = (name as string).split('.'); // ['filename', 'jpg']
        const fileFormat = splitName[splitName.length - 1]; // jpg
        const uniqueFileName = uniqid(`${metadata.entityType}-`);
        const fullFileName = `${uniqueFileName}.${fileFormat}`;
        const attachment: AttachmentData = {};
        const newFilePath = metadata.entityType === 'video-attachment' ? `${App.UPLOAD_FOLDER}/${uniqueFileName}/${fullFileName}` : `${App.UPLOAD_FOLDER}/${fullFileName}`;
        try {
            if(metadata.entityType === 'photo-attachment') {
                fs.rename(oldFilePath, newFilePath, async (err) => {
                    if (err) throw err;
                    return MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250, true)
                        .then(async () => {
                            attachment.link = `${App.MINIO_PUBLIC_URL}/photo/${App.TEMP_FOLDER_PREFIX}${uniqueFileName}-photo.jpg`
                            attachment.thumb = `${App.MINIO_PUBLIC_URL}/photo/${App.TEMP_FOLDER_PREFIX}${uniqueFileName}-thumb.jpg`
                            attachment.type = 'photo'
                            attachment.file_name = name;

                            const newMessages = await MessagingController._addMessageToChat({ id: userId } as any, id, null, [attachment]);
                            const participants = await MessagingController._getParticipants(id);

                            for (const message of newMessages) {
                                message.temporaryId = temporaryId;
                            }


                            console.log('document upload success in messaging. publish')
                            // publish({ userId, photoId: tmpFileId, type: 'attachment_file_processing_complete', entityType: metadata.entityType, entity: id, temporaryId });
                            participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('new_messages', newMessages));
                        })
                })

            } else if(metadata.entityType === 'video-attachment') {
                if (!fs.existsSync(`${App.UPLOAD_FOLDER}/${uniqueFileName}`)){
                    fs.mkdirSync(`${App.UPLOAD_FOLDER}/${uniqueFileName}`);
                }

                fs.rename(oldFilePath, newFilePath, async (err) => {

                    attachment.duration = (await VideoController.getVideoDuration({ video: newFilePath })) as string;

                    if (err) throw err;
                    // SUBSCRIBE TO NOTIFICATIONS FROM MEDIA-SERVER
                    http.get({
                        agent: false,
                        path: `/media-server-events/${userId}/${tmpFileId}`,
                        host: App.SHAKA_SERVER_HOST,
                        port: App.SHAKA_SERVER_PORT
                        // hostname: 'shaka:5000'
                    }, (response) => {
                        response.on('data', async (buf) => {
                            const json = JSON.parse(buf.toString())
                            // console.log('buf', buf)
                            // console.log('video-attahment-json', json)
                            if(json.data) {
                                const data = JSON.parse(json.data);
                                if(data.videoId === tmpFileId) {
                                    attachment.link = data.links.video; // http://localhost:9000/video/video-attachment-405gzs9dka73nnwe/video-attachment-405gzs9dka73nnwe_h264_master
                                    attachment.thumb = data.links.cover; // http://localhost:9000/video/video-attachment-405gzs9dka73nnwe/video-attachment-405gzs9dka73nnwe_cover.jpg
                                    attachment.type = 'video'
                                    attachment.file_name = name;

                                    const newMessages = await MessagingController._addMessageToChat({ id: userId } as any, id, null, [attachment]);
                                    const participants = await MessagingController._getParticipants(id);

                                    for (const message of newMessages) {
                                        message.temporaryId = temporaryId;
                                    }

                                    console.log('video upload success in messaging. publish')
                                    // publish({ userId, videoId: tmpFileId, type: 'attachment_file_processing_complete', entityType: metadata.entityType, entity: id, temporaryId });

                                    participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('new_messages', newMessages));
                                }
                            }
                        })
                    })
                    .on('error', (e) => {
                        console.error(`/media-server-events/${userId}/${tmpFileId} - Got error: ${e.message}`);
                        throw new Error(e)
                    })

                    console.log('uniqueFileName', uniqueFileName)
                    console.log(' fileFormat',  fileFormat)
                    const axiosResponse = await axios.post(`http://${App.SHAKA_SERVER_HOST}:${App.SHAKA_SERVER_PORT}/convert`, {
                        "input_filename": uniqueFileName,
                        "input_extension": fileFormat,
                        "sizes": ["1280x720", "854x480", "640x360", "426x240","256x144"],
                        "needScreenshot": true,
                        userId,
                        videoId: tmpFileId,
                        tempFile: true,
                    })
                    console.log('axiosResponse.data', axiosResponse.data)
                });

            } else if(metadata.entityType === 'document-attachment') {
                fs.rename(oldFilePath, newFilePath, async (err) => {
                    if (err) throw err;
                    const fileName = `${App.TEMP_FOLDER_PREFIX}${fullFileName}`;
                    await MinioInstance.uploadFile('document', fileName, newFilePath);

                    attachment.link = `${App.MINIO_PUBLIC_URL}/document/${fileName}`
                    attachment.thumb = `assets/images/document.jpg`; // Add document icon to frontend assets folder
                    attachment.type = 'document'
                    attachment.file_name = name;

                    const newMessages = await MessagingController._addMessageToChat({ id: userId } as any, id, null, [attachment]);
                    const participants = await MessagingController._getParticipants(id);

                    for (const message of newMessages) {
                        message.temporaryId = temporaryId;
                    }

                    console.log('document upload success in messaging. publish')
                    // publish({ userId, documentId: tmpFileId, type: 'attachment_file_processing_complete', entityType: metadata.entityType, entity: id, temporaryId });
                    participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('new_messages', newMessages));
                })
            }

            return [attachment]
            // const fullFileName = `${uniqueFileName}.${fileFormat === 'jpeg' ? 'jpg' : fileFormat}`;

        } catch (error) {
            console.error('Error while attachment uploading', error);

            if (newFilePath) {
                fs.unlinkSync(newFilePath);
            }
            if (attachment.link) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${attachment.link}`);
            }
            if (attachment.thumb) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${attachment.thumb}`);
            }
        }
    }

    /**
     * Getting a list of chatrooms for user
     * @param req.query.page - page number (default value: 1)
     * @param req.query.limit - limit of elements (default value: 50)
     * @param req.params.search - search by person or business name (or title in group chats)
     */
    static getChatroomList = async (req: Request, res: Response) => {
        try {
            const {
                query: {
                    page = 1,
                    limit = 50,
                    search = null
                }
            } = req
            const chatroomUserRepo = getRepository(ChatroomUser);

            console.log('req.user', req.user)

            const userId = req.user.baseBusinessAccount || req.user.id;

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            let query = chatroomUserRepo
                .createQueryBuilder("chatroom_user")
                .leftJoinAndSelect('chatroom_user.chatroom', 'chatroom')
                .leftJoinAndMapOne('chatroom.last_message', `chatroom.messages`, 'last_message', `last_message.id = (
                    select
                        id
                    from
                        "t_message" "m"
                    where
                        "m"."chatroom_id" = "chatroom_user"."chatroom_id"
                    order by
                        "m"."created" desc
                    limit 1
                )` )
                .leftJoinAndMapOne('chatroom_user.last_read_message', 'chatroom_user.last_read_message', 'last_read_message')
                .leftJoinAndMapMany('chatroom.chatroom_users', 'chatroom.chatroom_users', 'participants', '"participants"."id" != "chatroom_user"."id"')
                .leftJoinAndMapOne('participants.user', 'participants.user', 'participant_account')
                .leftJoin("participant_account.business", "participant_account_business")
                .addSelect("participant_account_business.id")
                .addSelect("participant_account_business.photo")
                .addSelect("participant_account_business.name")
                .addSelect("participant_account_business.pageName")
                .leftJoin("participant_account.person", "participant_account_person")
                .addSelect("participant_account_person.id")
                .addSelect("participant_account_person.photo")
                .addSelect("participant_account_person.fullName")
                .addSelect("participant_account_person.pageName")
                .where("chatroom_user.user_id = :userId", { userId })
                .andWhere("chatroom_user.deleted IS NULL")
                .skip(skip)
                .take(limitNumb)
                .orderBy("last_message.created", "DESC"); // last message date

            if(search) {
                query = query
                    .andWhere(`
                        ( LOWER(participant_account_business.name) SIMILAR TO '%${search.toLowerCase()}%'
                          OR LOWER(participant_account_person.fullName) SIMILAR TO '%${search.toLowerCase()}%'
                          OR LOWER(chatroom.title) SIMILAR TO '%${search.toLowerCase()}%'
                        )
                    `);
            }
            const chatroomList = await query.getManyAndCount();
            res.status(200).send({ items: chatroomList[0], total: chatroomList[1] })
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Finding a chatroom between current user and single/multiple recipients
     * (for using within 'message' button in profile)
     * @constructor
     * @param req.body.users - Array participant's id
     */
    static findChatroom = async (req: Request, res: Response) => {
        try {
            const {
                body: {
                    users
                },
                user: currentUser
            } = req;

            // You can't request chat existence between other users
            if(users.indexOf(currentUser.baseBusinessAccount || currentUser.id) === -1) {
                throw new Error('Bad Request')
            }
            const foundChatId = await MessagingController._findChatroomByParticipantIds(users)
            res.status(200).send({chatroom_id: foundChatId})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Get unread chatrooms amount.
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     */
    static getUnreadChatroomAmount = async (req: Request, res: Response) => {
        try {
            const {
                user: currentUser
            } = req;

            const currentDate = new Date();
            const chatroomUserRepo = getRepository(ChatroomUser);

            let query = chatroomUserRepo
                .createQueryBuilder("chatroom_user")
                .leftJoinAndSelect('chatroom_user.chatroom', 'chatroom')
                .leftJoinAndMapOne('chatroom_user.last_read_message', 'chatroom_user.last_read_message', 'last_read_message')
                .leftJoinAndMapOne('chatroom.last_message', `chatroom.messages`, 'last_message', `last_message.id = (
                    select
                        id
                    from
                        "t_message" "m"
                    where
                        "m"."chatroom_id" = "chatroom_user"."chatroom_id"
                    order by
                        "m"."created" desc
                    limit 1
                )` )
                .where("chatroom_user.user_id = :id", { id: currentUser.baseBusinessAccount || currentUser.id })
                .andWhere("chatroom_user.deleted IS NULL")
                .andWhere("(last_read_message IS NULL OR last_read_message.created < last_message.created)")

            // console.log('query.getSql()', query.getSql())

            // const chatroomList = await query.getMany();
            // console.log('chatroomList', chatroomList)

            const count = await query.getCount();

            res.status(200).send({count})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    /**
     * Getting Chatroom and info about its participants by chatroom_id.
     * It doesn't return the messages
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     */
    static getChatroomInfoByChatroomId = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    chatroom_id
                },
                user: currentUser
            } = req;

            const chatroomUserRepo = getRepository(ChatroomUser);

            let query = chatroomUserRepo
                .createQueryBuilder("chatroom_user")
                .leftJoinAndSelect('chatroom_user.chatroom', 'chatroom')
                .leftJoinAndMapOne('chatroom_user.last_read_message', 'chatroom_user.last_read_message', 'last_read_message')
                .leftJoinAndMapMany('chatroom.chatroom_users', 'chatroom.chatroom_users', 'participants', '"participants"."id" != "chatroom_user"."id"')
                .leftJoinAndMapOne('participants.user', 'participants.user', 'participant_account')
                .leftJoin("participant_account.business", "participant_account_business")
                .addSelect("participant_account_business.id")
                .addSelect("participant_account_business.photo")
                .addSelect("participant_account_business.name")
                .addSelect("participant_account_business.pageName")
                .leftJoin("participant_account.person", "participant_account_person")
                .addSelect("participant_account_person.id")
                .addSelect("participant_account_person.photo")
                .addSelect("participant_account_person.fullName")
                .addSelect("participant_account_person.pageName")
                .leftJoinAndMapOne('chatroom.last_message', `chatroom.messages`, 'last_message', `last_message.id = (
                    select
                        id
                    from
                        "t_message" "m"
                    where
                        "m"."chatroom_id" = "chatroom_user"."chatroom_id"
                    order by
                        "m"."created" desc
                    limit 1
                )` )
                .where("chatroom_user.user_id = :id", { id: currentUser.baseBusinessAccount || currentUser.id })
                .andWhere("chatroom_user.chatroom_id = :chatroom_id", { chatroom_id })
                .andWhere("chatroom_user.deleted IS NULL")

            const chatroomInfo = await query.getOne();

            if(!chatroomInfo) {
                throw new Error('Forbidden')
            }

            res.status(200).send(chatroomInfo)


        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    /**
     * Getting messages by chatroom_id.
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     * @param req.query.page - page number (default value: 1)
     * @param req.query.limit - limit of elements (default value: 50)
     * @param req.params.search - search by message's text
     */
    static getMessagesByChatroomId = async (req: Request, res: Response) => {
        try {
            const {
                query: {
                    page = 1,
                    limit = 50,
                    search = null
                },
                params: {
                    chatroom_id
                },
                user: currentUser
            } = req


            const messageRepo = getRepository(Message)

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id)

            // let query = MessagingController._buildSelectMessageQuery(chatroom_id)
            //     .skip(skip)
            //     .take(limitNumb)
            //     .orderBy("messages.created", "DESC");

            //     console.log('query.getSql()', query.getSql())

            //     if(search) {
            //         query = query
            //             .andWhere(`
            //                 ( LOWER(messages.text) SIMILAR TO '%${search.toLowerCase()}%')
            //             `);
            //     }

            // const messages = await query.getMany();
            // console.log('messages', messages.filter(message => message.attachment));

            const rawQuery = getMessagesByChatroomIdRawQuery(chatroom_id, limitNumb, skip, search)
            const messages = await messageRepo.query(rawQuery)
            res.status(200).send({messages: sanitizeRawData(messages), raw: true})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Create a chatroom and chatroom_user records for creator and all recipients
     * this function should also add the first message
     * @constructor
     * @param req.body.users - Array participant's id
     * @param req.body.message - String with the first message in the chat
     * @param req.body.attachments - Array with attachments. A new message will be created for each attachment
     */
    static addChatroom = async (req: Request, res: Response) => {
        try {
            const {
                body: {
                    users,
                    message = null,
                    attachments = null
                },
                user: currentUser
            } = req;

            const currentChatroomUser = await MessagingController._addChatroom(currentUser, users, message, attachments)

            res.status(200).send(currentChatroomUser)
        } catch (error) {
            res.status(error.status || 400).send({ error: error.message });
        }
    };

    /**
     * Should be called when you adding a new recipient
     * before creating a new Chatroom
     * or in case of adding people in exist group chat (in future)
     * @constructor
     * @param req.body.users - Array participant's id
     */
    static checkBlocks = async (req: Request, res: Response) => {
        try {

            const {
                body: {
                    users: participants
                },
                user: currentUser
            } = req;

            // console.log('checkBlocks participants', participants)
            await MessagingController._checkIfRecipientBlockedYou(participants, currentUser)

            // TODO: Continue adding chat and chat_users

            res.status(200).send({status: "success"});
        } catch (error) {

            console.log('main catch', error)
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Delete chatroom for all recipients.
     * Only author can delete chatroom
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     */
    static deleteChatroom = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    chatroom_id
                },
                user: currentUser
            } = req;

            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id)
            if(currentUser.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(currentUser)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }

            const chatroomRepo = getRepository(Chatroom);

            const participants = await MessagingController._getParticipants(chatroom_id)

            const result = await chatroomRepo.delete({id: chatroom_id})
            if(!result.affected) {
                throw new Error('Bad Request')
            }

            participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('deleted_chatroom', {
                chatroom: +chatroom_id,
            }))

            res.status(200).send({status: "success"})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Add a mesasge
     * @constructor
     * @param req.body.message - String with the text
     * @param req.body.attachments - Array with attachments. A new message will be created for each attachment
     * @param req.params.chatroom_id - Id of the target chatroom
     */
    static addMessage = async (req: Request, res: Response) => {
        try {
            const {
                body: {
                    message = {},
                    attachments = null,
                },
                params: {
                    chatroom_id
                },
                user: currentUser
            } = req;

            const newMessages = await MessagingController._addMessageToChatroom(currentUser, chatroom_id, message, attachments);

            res.status(200).send(newMessages)
        } catch (error) {
            res.status(error.status || 400).send({ error: error.message });
        }
    };

    /**
     * Delete message for all recipients.
     * Only author can delete a message
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     * @param req.params.message_id - Id of the message to delete
     */
    static deleteMessage = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    message_id,
                    chatroom_id
                },
                user: currentUser
            } = req;

            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id)
            if(currentUser.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(currentUser)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const messageRepo = getRepository(Message);

            // TODO: Find and clear the attachment from the Minio.
            // You should find the attachment's url before deleting a message,
            // because of attachment row will be deleted cascade

            const result = await messageRepo.delete({id: message_id, user: currentUser.baseBusinessAccount || currentUser.id, chatroom: chatroom_id})
            if(!result.affected) {
                throw new Error('Bad Request')
            }

            const participants = await MessagingController._getParticipants(chatroom_id)
            participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('deleted_message', {
                id: +message_id,
                chatroom: +chatroom_id,
            }))

            res.status(200).send({status: "success"})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Edit text of the message.
     * Only author can delete a message.
     * It doesn't support editing of attachments.
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     * @param req.params.message_id - Id of the message to delete
     * @param req.body.text - new text of the message
     */
    static editMessage = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    message_id,
                    chatroom_id
                },
                body: {
                    text
                },
                user: currentUser
            } = req
            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id)
            if(currentUser.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(currentUser)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const messageRepo = getRepository(Message);

            const foundMessage = await messageRepo.findOne({id: message_id, user: currentUser.baseBusinessAccount || currentUser.id, chatroom: chatroom_id});
            console.log('foundMessage', foundMessage)

            if(!foundMessage) {
                throw new Error('Bad Request')
            }

            const updated = new Date();
            const updatedMessage = await messageRepo.update(
                {
                    id: message_id,
                    user: currentUser.baseBusinessAccount || currentUser.id,
                    chatroom: chatroom_id
                }, {
                    text: text,
                    updated
                }
            );

            const participants = await MessagingController._getParticipants(chatroom_id)
            participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('updated_message', {
                id: +message_id,
                chatroom: +chatroom_id,
                text: text,
                updated
            }))

            res.status(200).send({status: "success"})

        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Set last_read_message for current user
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     * @param req.body.last_read_message_id - Id of the last read message (with the newest date)
     */
    static readMessages = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    chatroom_id
                },
                body: {
                    last_read_message_id
                },
                user: currentUser
            } = req
            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id);

            const messageRepo = getRepository(Message);
            const chatroomUserRepo = getRepository(ChatroomUser);

            const foundMessage = await messageRepo.findOne({
                where: {
                    id: last_read_message_id,
                    chatroom: chatroom_id
                },
                relations:['chatroom']
            });

            if(!foundMessage) {
                throw new Error('Bad Request')
            }

            const updated = new Date();

            await chatroomUserRepo.update({
                chatroom: foundMessage.chatroom,
                user: currentUser.baseBusinessAccount || currentUser.id
            }, {
                last_read_message: last_read_message_id,
                updated
            })

            const participants = await MessagingController._getParticipants(chatroom_id)
            participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('read_message', {
                id: +last_read_message_id,
                chatroom: +foundMessage.chatroom.id,
                user: +currentUser.baseBusinessAccount || +currentUser.id,
                updated
            }))

            res.status(200).send({status: "success"})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Mute/unmute chat
     * @constructor
     * @param req.params.chatroom_id - Id of the target chatroom
     * @param req.body.is_muted - boolean. true if you need to mute, false if unmute a channel
     */
    static muteUnmuteChat = async (req: Request, res: Response) => {
        try {
            const {
                params: {
                    chatroom_id
                },
                body: {
                    is_muted
                },
                user: currentUser
            } = req

            await MessagingController._checkPermissionsToChatroom(currentUser, chatroom_id);

            const chatroomUserRepo = getRepository(ChatroomUser);
            await chatroomUserRepo.update({
                chatroom: chatroom_id,
                user: currentUser.baseBusinessAccount || currentUser.id
            }, {
                is_muted: !!is_muted,
                updated: new Date()
            })

            res.status(200).send({is_muted: !!is_muted})
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    /**
     * Send message with shared contend to list of users
     * @param req.body.userIds - IDs of users
     * @param req.body.message - message text
     */
    static shareWithUsers = async (req: Request, res: Response) => {
        try {
            const {
                body: {
                    userIds,
                    message,
                    entityType,
                    entityId,
                },
                user: currentUser
            } = req

            await Promise.all(
                userIds.map(async id => {
                    const participantIds = [id, currentUser.baseBusinessAccount || currentUser.id];
                    const foundChatroomId = await MessagingController._findChatroomByParticipantIds(participantIds)

                    if (foundChatroomId) {
                        await MessagingController._addMessageToChatroom(currentUser, foundChatroomId, { text: message }, null, entityType, entityId);
                    } else {
                        await MessagingController._addChatroom(currentUser, participantIds, message, null, entityType, entityId);
                    }
                })
            );

            res.status(200).send({ status: 'ok' })
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    // TODO: For group chats
    // static editChatroom = async (req: Request, res: Response) => {
    //     try {
    //         res.status(200).send([])
    //     } catch (error) {
    //         res.status(400).send({ error: error.message });
    //     }
    // };

    // TODO: For group chats
    // static banUser = async (req: Request, res: Response) => {
    //     try {
    //         res.status(200).send([])
    //     } catch (error) {
    //         res.status(400).send({ error: error.message });
    //     }
    // };

    /**
     *
     * MESSAGING HELPER METHODS
     *
     */
    static _checkIfRecipientBlockedYou = async (participants: number[], currentUser: Account) => {
        if (!participants || !participants[0]) throw new Error('Array of users is required')
        // console.log('_checkIfRecipientBlockedYou participants', participants)
        // array of users who are blocked current user
        const blockedBy = await getBlockRepository().blockedBy(currentUser.baseBusinessAccount || currentUser.id)
        // console.log('blockedBy', blockedBy)
        await Promise.all(
            participants.map((participant: number) => {
                return new Promise(async (resolve, reject) => {
                    // console.log('participant', participant)
                    const isBlocked = blockedBy.indexOf(participant) !== -1
                    // console.log('isBlocked', isBlocked)
                    if (isBlocked) {
                        return reject({ message: 'Some of participants blocked you' })
                    }
                    // isBlocked ? reject('Some of participants blocked you'): resolve(participant)
                    return resolve(participant)
                })
            })
        )
    }

    static _inviteParticipantsToChatroom = async (participants: Account[], chatroom: Chatroom) => {
        if (!participants || !participants[0]) throw new Error('Array of users is required')
        const chatroomUserRepo = getRepository(ChatroomUser);
        await Promise.all(
            participants.map((user: Account) => {
                return new Promise(async (resolve, reject) => {
                    // console.log('participant', user)
                    try {
                        const alreadyInChatroom = !!await chatroomUserRepo.findOne({
                            user,
                            chatroom
                        })
                        if (!alreadyInChatroom) {
                            const newParticipant = {
                                user,
                                chatroom,
                                last_read_message: null
                            }
                            await chatroomUserRepo.save(newParticipant)
                        }
                        resolve()
                    } catch (error) {
                        reject(error)
                    }
                })
            })
        )
    }

    static _addMessageToChat = async (
        currentUser: Account,
        chatroom: Chatroom,
        text?: string,
        attachmentData?: AttachmentData[],
        sharedEntityType: string = null,
        sharedEntityId: number = null,
    ): Promise<NewMessageResponse[]> => {
        try {
            const messageRepo = getRepository(Message)
            const messageAttachmentRepo = getRepository(MessageAttachment)
            const accountInfo = await getAccountRepository().getSmallAccountInfo({key: 'id', val: currentUser.id})

            let dataToReturn;

            if (text || sharedEntityType && sharedEntityId) {
                const message = await messageRepo.save({
                    text,
                    user: currentUser.baseBusinessAccount || currentUser,
                    chatroom,
                    reply_to: null, // TODO: we can handle it in future,
                    [`shared_${sharedEntityType}`]: sharedEntityId
                })

                const savedMessage = {...message, user: {
                  id: accountInfo.id,
                  business: accountInfo.business,
                  person: accountInfo.person
                }}

                dataToReturn = [{
                    ...savedMessage,
                    chatroom: +chatroom
                }]
            } else if (attachmentData) {
                dataToReturn = await Promise.all(
                    attachmentData.map((attachment: AttachmentData) => {
                        return new Promise(async (resolve, reject) => {
                            try {
                                const message = await messageRepo.save({
                                    text,
                                    user: currentUser.baseBusinessAccount || currentUser,
                                    chatroom,
                                    reply_to: null, // TODO: we can handle it in future,
                                })
                                // console.log('new attachment message', message)
                                const newAttachment = await messageAttachmentRepo.save({
                                    ...attachment,
                                    message,
                                    chatroom,
                                    user: currentUser.baseBusinessAccount || currentUser,
                                })
                                return resolve({
                                    ...message,
                                    attachment,
                                    user: accountInfo
                                })
                            } catch (error) {
                                reject(error)
                            }
                        })
                    })
                )
            }
            // CHANGE LAST_READ_MESSAGE BECAUSE OF YOUR MESSAGES SHOULD BE READ AUTOMATICALLY
            const chatroomUserRepo = getRepository(ChatroomUser);
            await chatroomUserRepo.update({
                chatroom: chatroom,
                user: currentUser
            }, {
                last_read_message: dataToReturn[dataToReturn.length - 1],
                updated: new Date()
            })
            return dataToReturn
        } catch (error) {
            console.log('error', error)
            throw new Error('Failed to add message')
        }
    }


    static _findChatroomByParticipantIds = async (users: Account[]) => {
        // console.log('_findChatroomByParticipantIds', users)
        // console.log('========================')
        try {
            const chatroomUserRepo = getRepository(ChatroomUser);
            const query = chatroomUserRepo
                .createQueryBuilder("chatroom_user")
                .select("chatroom_user.chatroom_id", 'chatroom_id')
                .addSelect('array_agg(chatroom_user.user_id)', 'participants')
                .where(qb => {
                    const subQuery = qb.subQuery()
                        .select("usr.chatroom_id")
                        .from(ChatroomUser, "usr")
                        .where("usr.user_id in (:...users)", {users})
                        .getQuery();
                    return "chatroom_user.chatroom_id IN " + subQuery;
                })
                .groupBy('chatroom_user.chatroom_id')
            const chats = await query.getRawMany();

            let foundChatId;
            const sortedUsersArray = users.sort((a, b) => +a - +b);
            chats.forEach((chat, index) => {
                const sortedParticipantsArray = chat.participants.map(id => +id).sort((a, b) => +a - +b)
                // console.log('sortedUsersArray', sortedUsersArray)
                // console.log('sortedParticipantsArray', sortedParticipantsArray)
                const weakIntersection = intersection(sortedParticipantsArray, sortedUsersArray)
                const fullIntersection = isEqual(sortedParticipantsArray, weakIntersection) && isEqual(sortedUsersArray, weakIntersection)
                // console.log('weakIntersection', weakIntersection)
                // console.log('fullIntersection', fullIntersection)
                // console.log('=======================================\n\n\n')
                if (fullIntersection) {
                    foundChatId = chat.chatroom_id
                }
            })
            // console.log('foundChatId', foundChatId)
            return foundChatId;
        } catch (error) {
            throw new Error('Failed to find chatroom')
        }
    }

    /**
     * Throws an error if current user is not in the chatroom or he's blocked (banned) or deleted (from a group chat)
     */
    static _checkPermissionsToChatroom = async (currentUser: Account, chatroom_id) => {
        try {
            const chatroomUserRepo = getRepository(ChatroomUser);
            const query = chatroomUserRepo
                    .createQueryBuilder("chatroom_user")
                    .select('chatroom_user.id')
                    .where("chatroom_user.user_id = :id", { id: currentUser.baseBusinessAccount || currentUser.id })
                    .andWhere("chatroom_user.chatroom_id = :chatroom_id", { chatroom_id })
                    .andWhere("chatroom_user.deleted IS NULL")
                    .andWhere("chatroom_user.is_blocked = FALSE")

                const chatroomUser = await query.getOne()


                // console.log('chatroomUser', chatroomUser)
                if(!chatroomUser) {
                    throw new Error('Forbidden')
                }

                return

        } catch (error) {
            throw error;

        }
    }

    static _getParticipants = async (chatroom_id) => {
        const chatroomUserRepo = getRepository(ChatroomUser);
        let participants = await chatroomUserRepo
        .createQueryBuilder("chatroom_user")
        .leftJoinAndSelect( 'chatroom_user.user', 'participant')
        .leftJoin("participant.business", "participant_business")
        .addSelect("participant_business.id")
        .addSelect("participant_business.photo")
        .addSelect("participant_business.name")
        .addSelect("participant_business.pageName")
        .leftJoin("participant.person", "participant_person")
        .addSelect("participant_person.id")
        .addSelect("participant_person.photo")
        .addSelect("participant_person.fullName")
        .addSelect("participant_person.pageName")
        .where('chatroom_user.chatroom = :chatroom_id', {chatroom_id})
        .getMany()

        return participants

        // console.log('query', query)
    }

    static async _addMessageToChatroom(
        currentUser: any,
        chatroomId: any,
        message: { text: string, temporaryId?: string },
        attachments = null,
        sharedEntityType = null,
        sharedEntityId = null,
    ) {
        if ((!message && !attachments && (!sharedEntityType || !sharedEntityId)) || !chatroomId) throw new Error('Bad Request');

        if (currentUser.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(currentUser)
            if (!permissions.canWrite) {
                throw { message: 'Forbidden', status: 403 };
            }
        }

        await MessagingController._checkPermissionsToChatroom(currentUser, chatroomId)

        const newMessages = await MessagingController._addMessageToChat(
            currentUser,
            chatroomId,
            message.text,
            attachments,
            sharedEntityType,
            sharedEntityId
        );
        const participants = await MessagingController._getParticipants(chatroomId)

        newMessages.forEach(msg => msg.temporaryId = message.temporaryId);
        participants.forEach(obj => io.to(`user_${obj.user.id}`).emit('new_messages', newMessages))

        return newMessages;
    }

    static async _addChatroom(
        currentUser: any,
        users: number[],
        message: string,
        attachments: any[] = null,
        sharedEntityType = null,
        sharedEntityId = null,
    ) {
        // console.log('_addChatroom users', users)
        if (currentUser.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(currentUser)
            if (!permissions.canWrite) {
                throw { status: 403, message: 'Forbidden' };
            }
        }
        const foundChatId = await MessagingController._findChatroomByParticipantIds(users as unknown as Account[])

        if (foundChatId) {
            throw new Error('This chat already exists')
        }

        const chatroomRepo = getRepository(Chatroom);

        // Throws an error if somebody of your participants are blocked you
        await MessagingController._checkIfRecipientBlockedYou(users, currentUser)

        const newChatroomData = {
            title: '',
            cover: '',
            thumb: '',
            created: new Date()
        }
        const savedChatroom = await chatroomRepo.save(newChatroomData);

        await MessagingController._inviteParticipantsToChatroom(users as unknown as Account[], savedChatroom);

        const participants = await MessagingController._getParticipants(savedChatroom.id)
        // console.log('_addChatroom participants', participants)

        const messages = await MessagingController._addMessageToChat(
            currentUser,
            savedChatroom,
            message,
            attachments,
            sharedEntityType,
            sharedEntityId
        );

        const chatroom = {
            ...savedChatroom,
            chatroom_users: participants,
            messages,
        };

        let currentChatroomUser;

        for (const participant of participants) {
            const chatroomUser = {
                ...participant,
                chatroom
            };

            if (participant.user.id === currentUser.baseBusinessAccount || participant.user.id === currentUser.id) {
                currentChatroomUser = chatroomUser;
            }

            io.to(`user_${participant.user.id}`).emit('new_chat', chatroomUser);
        }

        return currentChatroomUser;
    }

    static _buildSelectMessageQuery(chatroom_id: number) {
        const messageRepo = getRepository(Message)

        // orig
        return messageRepo
            .createQueryBuilder("messages")
            .select('messages.id')
            .addSelect('messages.text')
            .addSelect('messages.is_forwarded')
            .addSelect('messages.is_service_message')
            .addSelect('messages.created')
            .addSelect('messages.updated')
            .leftJoin('messages.reply_to', 'reply_to')
            .addSelect('reply_to.id')
            .addSelect('reply_to.text')
            .addSelect('reply_to.is_forwarded')
            .addSelect('reply_to.is_service_message')
            .addSelect('reply_to.created')
            .addSelect('reply_to.updated')
            .leftJoin('messages.attachment', 'attachment')
            .addSelect('attachment.id')
            .addSelect('attachment.link')
            .addSelect('attachment.thumb')
            .addSelect('attachment.duration')
            .addSelect('attachment.file_name')
            .addSelect('attachment.type')
            .addSelect('attachment.created')
            .leftJoin('messages.shared_user', 'shared_user')
            .addSelect("shared_user.id")
            .leftJoin("shared_user.business", "shared_user_business")
            .addSelect("shared_user_business.id")
            .addSelect("shared_user_business.photo")
            .addSelect("shared_user_business.name")
            .addSelect("shared_user_business.pageName")
            .leftJoin("shared_user.person", "shared_user_person")
            .addSelect("shared_user_person.id")
            .addSelect("shared_user_person.photo")
            .addSelect("shared_user_person.fullName")
            .addSelect("shared_user_person.pageName")

            .leftJoin('messages.shared_text', 'shared_text')
            .addSelect('shared_text.id')
            .addSelect('shared_text.text')
            .addSelect('shared_text.created')
            .addSelect('shared_text.updated')
            .leftJoin('shared_text.user', 'shared_text_user')
            .addSelect("shared_text_user.id")
            .leftJoin("shared_text_user.business", "shared_text_user_business")
            .addSelect("shared_text_user_business.id")
            .addSelect("shared_text_user_business.photo")
            .addSelect("shared_text_user_business.name")
            .addSelect("shared_text_user_business.pageName")
            .leftJoin("shared_text_user.person", "shared_text_user_person")
            .addSelect("shared_text_user_person.id")
            .addSelect("shared_text_user_person.photo")
            .addSelect("shared_text_user_person.fullName")
            .addSelect("shared_text_user_person.pageName")

            .leftJoin('messages.shared_photo', 'shared_photo')
            .addSelect('shared_photo.id')
            .addSelect('shared_photo.photo')
            .addSelect('shared_photo.thumb')
            .addSelect('shared_photo.title')
            .addSelect('shared_photo.description')
            .addSelect('shared_photo.category')
            .addSelect('shared_photo.location')
            .addSelect('shared_photo.filterName')
            .addSelect('shared_photo.created')
            .addSelect('shared_photo.updated')
            .leftJoin('shared_photo.user', 'shared_photo_user')
            .addSelect("shared_photo_user.id")
            .leftJoin("shared_photo_user.business", "shared_photo_user_business")
            .addSelect("shared_photo_user_business.id")
            .addSelect("shared_photo_user_business.photo")
            .addSelect("shared_photo_user_business.name")
            .addSelect("shared_photo_user_business.pageName")
            .leftJoin("shared_photo_user.person", "shared_photo_user_person")
            .addSelect("shared_photo_user_person.id")
            .addSelect("shared_photo_user_person.photo")
            .addSelect("shared_photo_user_person.fullName")
            .addSelect("shared_photo_user_person.pageName")

            .leftJoin('messages.shared_video', 'shared_video')
            .addSelect("shared_video.id")
            .addSelect("shared_video.video")
            .addSelect("shared_video.cover")
            .addSelect("shared_video.thumb")
            .addSelect("shared_video.title")
            .addSelect("shared_video.description")
            .addSelect("shared_video.duration")
            .addSelect("shared_video.category")
            .addSelect("shared_video.location")
            .addSelect("shared_video.created")
            .addSelect("shared_video.updated")
            .leftJoin('shared_video.user', 'shared_video_user')
            .addSelect("shared_video_user.id")
            .leftJoin("shared_video_user.business", "shared_video_user_business")
            .addSelect("shared_video_user_business.id")
            .addSelect("shared_video_user_business.photo")
            .addSelect("shared_video_user_business.name")
            .addSelect("shared_video_user_business.pageName")
            .leftJoin("shared_video_user.person", "shared_video_user_person")
            .addSelect("shared_video_user_person.id")
            .addSelect("shared_video_user_person.photo")
            .addSelect("shared_video_user_person.fullName")
            .addSelect("shared_video_user_person.pageName")

            .leftJoin('messages.shared_article', 'shared_article')
            .addSelect("shared_article.id")
            .addSelect("shared_article.cover")
            .addSelect("shared_article.thumb")
            .addSelect("shared_article.title")
            .addSelect("shared_article.text")
            .addSelect("shared_article.category")
            .addSelect("shared_article.location")
            .addSelect("shared_article.created")
            .addSelect("shared_article.updated")
            .leftJoin('shared_article.user', 'shared_article_user')
            .addSelect("shared_article_user.id")
            .leftJoin("shared_article_user.business", "shared_article_user_business")
            .addSelect("shared_article_user_business.id")
            .addSelect("shared_article_user_business.photo")
            .addSelect("shared_article_user_business.name")
            .addSelect("shared_article_user_business.pageName")
            .leftJoin("shared_article_user.person", "shared_article_user_person")
            .addSelect("shared_article_user_person.id")
            .addSelect("shared_article_user_person.photo")
            .addSelect("shared_article_user_person.fullName")
            .addSelect("shared_article_user_person.pageName")

            .leftJoin('messages.shared_product', 'shared_product')
            .addSelect("shared_product.id")
            .addSelect("shared_product.type")
            .addSelect("shared_product.title")
            .addSelect("shared_product.condition")
            .addSelect("shared_product.price")
            .addSelect("shared_product.quantity")
            .addSelect("shared_product.category")
            .addSelect("shared_product.description")
            .addSelect("shared_product.variantOptions")
            .addSelect("shared_product.deliveryOptions")
            .addSelect("shared_product.deliveryTime")
            .addSelect("shared_product.shippingCosts")
            .addSelect("shared_product.payerReturn")
            .addSelect("shared_product.active")
            .addSelect("shared_product.created")
            .addSelect("shared_product.updated")
            .leftJoin('shared_product.user', 'shared_product_user')
            .addSelect("shared_product_user.id")
            .leftJoin("shared_product_user.business", "shared_product_user_business")
            .addSelect("shared_product_user_business.id")
            .addSelect("shared_product_user_business.photo")
            .addSelect("shared_product_user_business.name")
            .addSelect("shared_product_user_business.pageName")
            .leftJoin("shared_product_user.person", "shared_product_user_person")
            .addSelect("shared_product_user_person.id")
            .addSelect("shared_product_user_person.photo")
            .addSelect("shared_product_user_person.fullName")
            .addSelect("shared_product_user_person.pageName")

            .leftJoin('messages.shared_service', 'shared_service')
            .addSelect("shared_service.id")
            .addSelect("shared_service.active")
            .addSelect("shared_service.title")
            .addSelect("shared_service.price")
            .addSelect("shared_service.quantity")
            .addSelect("shared_service.hourlyPrice")
            .addSelect("shared_service.description")
            .addSelect("shared_service.performance")
            .addSelect("shared_service.category")
            .addSelect("shared_service.created")
            .addSelect("shared_service.updated")
            .leftJoin('shared_service.user', 'shared_service_user')
            .addSelect("shared_service_user.id")
            .leftJoin("shared_service_user.business", "shared_service_user_business")
            .addSelect("shared_service_user_business.id")
            .addSelect("shared_service_user_business.photo")
            .addSelect("shared_service_user_business.name")
            .addSelect("shared_service_user_business.pageName")
            .leftJoin("shared_service_user.person", "shared_service_user_person")
            .addSelect("shared_service_user_person.id")
            .addSelect("shared_service_user_person.photo")
            .addSelect("shared_service_user_person.fullName")
            .addSelect("shared_service_user_person.pageName")

            .leftJoin('messages.user', 'author')
            .addSelect("author.id")
            .leftJoin("author.business", "author_business")
            .addSelect("author_business.id")
            .addSelect("author_business.photo")
            .addSelect("author_business.name")
            .addSelect("author_business.pageName")
            .leftJoin("author.person", "author_person")
            .addSelect("author_person.id")
            .addSelect("author_person.photo")
            .addSelect("author_person.fullName")
            .addSelect("author_person.pageName")
            .where("messages.chatroom_id = :chatroom_id", { chatroom_id })
            .andWhere("messages.deleted IS NULL")


    }

    // BLOCK and REPORT should be added in their own controllers (add a)
}
