import * as express from "express";
const router = express.Router();

import passport = require("passport");
import { LanguagesController } from "./controller"


router.get("/", passport.authenticate("jwt", {session: false}), LanguagesController.getLanguages);


export = router;