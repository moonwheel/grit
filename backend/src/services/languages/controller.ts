import { getRepository } from "typeorm";
import { Languages } from "../../entities/languages";

export class LanguagesController {

    static getLanguages = async(req, res) => {
        try {
    
            const languages = await getRepository(Languages)
                        .createQueryBuilder("languages")
                        .getMany()
    
            res.status(200).send({ languages });
        } catch (error) {
            console.log('getLanguages error',error)
            res.status(400).send({ error: error.message });
        }
    }
}
