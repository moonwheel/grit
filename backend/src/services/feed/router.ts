import {FeedController} from "./controller";
import {authenticate} from "passport";
import {Router} from "express";

const router: Router = Router();

router.get("/feed", authenticate("jwt", {session: false}), FeedController.getFeed);

export default router;
