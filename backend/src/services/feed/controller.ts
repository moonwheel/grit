import getFollowingRepository from "../../entities/repositories/following-repository";
import { getConnection, getRepository } from "typeorm";
import { PageParams } from "../../interfaces/global/page.interface";
import { Helpers } from "../../utils/helpers";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import { Photo } from "../../entities/photo";
import { Video } from "../../entities/video";
import { Text } from "../../entities/text";
import { Bookmark } from "../../entities/bookmarks";

export class FeedController {
    static getFeed = async (req, res) => {
        try {
            const {
                query: {
                    page,
                    limit,
                }
            } = req;
            const id = req.user.baseBusinessAccount || req.user.id
            const followings = await getFollowingRepository().getFollowings(id, {});
            const currentUserId = id;
            const query = await Helpers.generateFeedQuery(followings, currentUserId, { page, limit });
            const data = await getConnection().query(query);


            res.status(200).send(data);
        } catch (error) {
            console.log('getFeed error!!!', error)
            res.status(400).send({ error: error.message });
        }
    };
}
