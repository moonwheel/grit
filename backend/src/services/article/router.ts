const express = require("express");
const router = express.Router();

import passport = require("passport");
import { ArticleController } from "./controller";
import { ViewController } from "../view/controller";
import { Middleware } from "../../utils/middleware";

router.post("/", passport.authenticate("jwt", {session: false}), ArticleController.addArticle);
router.use("/:id/upload", passport.authenticate("jwt", {session: false}), ArticleController.uploadCover);

router.get("/all", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, ArticleController.getArticles);
router.get("/:id", passport.authenticate("jwt", {session: false}), Middleware.checkInBlaklist("article", "id"), Middleware.checkDraftAccess, ArticleController.getSingleArticle);
router.put("/", passport.authenticate("jwt", {session: false}), ArticleController.updateArticle);
router.delete("/:id", passport.authenticate("jwt", {session: false}), ArticleController.deleteArticle);

router.post("/:id/like", passport.authenticate("jwt", {session: false}), ArticleController.likeArticle);

router.get("/:id/like", passport.authenticate("jwt", {session: false}), ArticleController.getArticleLikes);

router.post("/:id/view", passport.authenticate("jwt", {session: false}), ViewController.view("article"));

router.get("/:id/comment", passport.authenticate("jwt", {session: false}), ArticleController.getComments);
router.post("/:id/comment", passport.authenticate("jwt", {session: false}), ArticleController.addComment);
router.put("/:id/comment", passport.authenticate("jwt", {session: false}), ArticleController.editComment);

router.get("/comment/:id/replies", passport.authenticate("jwt", {session: false}), ArticleController.getReplies);

router.post("/comment/:id/like", passport.authenticate("jwt", {session: false}), ArticleController.likeComment);

router.get("/comment/:id/like", passport.authenticate("jwt", {session: false}), ArticleController.getCommentLikes);

router.delete("/comment/:id", passport.authenticate("jwt", {session: false}), ArticleController.deleteComment);

export = router;
