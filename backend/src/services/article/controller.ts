const dateTime = require("date-time");
import uniqid = require("uniqid");
import * as fs from 'fs';
import { getRepository } from "typeorm";
import { Article } from "../../entities/article";
import { PhotoController } from "../photo/controller";
import { Article_photo } from "../../entities/article_photo";
import { Article_Like } from "../../entities/article_like";
import { Filter } from "../../interfaces/global/filter.interface";
import { Article_Comment } from "../../entities/article_comment";
import { Article_Comment_Replies } from "../../entities/article_comment_reply";
import { Article_Comment_Like } from "../../entities/article_comment_like";
import getMentionRepository from "../../entities/repositories/mention-repository";
import { Helpers } from "../../utils/helpers";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import getHashtagRepository from "../../entities/repositories/hashtag-repository";
import { App } from "../../config/app.config";
import MinioInstance from "../../utils/minio";
import { tusServer, TUS_EVENTS, decodeMetadata, UploadedFileMetadata } from "../../utils/tus-server";
import { publish } from '../../utils/sse';
import { MediaConverter, ConvertedPhoto } from "../../utils/media-converter";
import getAccountRepository from "../../entities/repositories/account-repository";
import getFollowingRepository from "../../entities/repositories/following-repository";
import { NotificationController } from "../notifications/controller";
import { NotificationContentType } from "../../entities/notification";
import { Bookmark } from "../../entities/bookmarks";


tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata);

    if (metadata.entityType === 'article') {
        // console.log('upload article completed metadata', metadata)
        // console.log('upload article completed event', event)
        const tmpFileId = event.file.id;
        await ArticleController.onUploadEnd(tmpFileId, metadata)
    }
});

export class ArticleController {
    static addArticle = async(req, res) => {
        const user = req.user;

        if(user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }

        const articleRepo = getRepository(Article);
        const article = new Article();

        try {
            article.category = req.body.category;
            article.draft = req.body.draft ? true : false;
            article.location = req.body.location;
            article.business_address = req.body.business_address;
            article.text = req.body.text;
            article.title = req.body.title;
            article.user = user.baseBusinessAccount || user.id;
            const savedArticle = await articleRepo.save(article);
            savedArticle.mentions = await getMentionRepository().addMention("article", savedArticle, req.body.mentions ,"article",user);
            await getHashtagRepository().addHashtag("article", savedArticle.id, req.body.hashtags || []);

            res.status(200).send(savedArticle);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static uploadCover = async (req: any, res: any) => {
        if(req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }
        return tusServer.handle(req, res);
    };

    static onUploadEnd = async (tmpFileId: string, metadata: UploadedFileMetadata) => {
        let newFilePath: string;
        let outputFiles: ConvertedPhoto;
        const { id, type, userId } = metadata;
        try {
            const uniqueId = +metadata['grit-file-entity-id'];
            const fileType = type;
            const articleRepo = getRepository(Article);
            const article = await articleRepo.findOne({ id });

            // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;

            const uniqueFileName = uniqid("photo-");
            const spliType = (fileType as string).split('/');
            const fileFormat = spliType[1];

            const fullFileName = `${uniqueFileName}.${fileFormat}`;

            newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

            fs.renameSync(oldFilePath, newFilePath);

            MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                .then(async () => {
                    if (uniqueId === 0) {
                        article.cover = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                        article.thumb = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`
                    } else {
                        article.text = article.text.replace(
                            `{{${uniqueId}}}`,
                            `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                        );
                    }

                    await articleRepo.save(article);
                    publish({ userId, type: 'file_processing_complete', entityType: 'article', entity: article });
                })
        } catch (error) {
            console.error('Error while article uploading' ,error);

            if (newFilePath) {
                fs.unlinkSync(newFilePath);
            }
            if (outputFiles.photo) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.photo}`);
            }
            if (outputFiles.thumb) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.thumb}`);
            }
        }
    }

    static getArticles = async (req: any, res: any) => {
        try {
            const {
                user,
                query: { page = 1, filter = "date", limit = 15, order = "desc", draft = false, userId = null }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }


            if(userId && +userId !== +user.id) {
                const targetAccount = await getAccountRepository().getFullAccountInfo({ key: 'id', val: userId })
                const target = targetAccount;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({error: 'Private account'});
                }
            }

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            const articleRepo = getRepository(Article);

            const query = AbstractController.buildPhotoVideoArticleListQuery(
                articleRepo,
                'article',
                userId || user.baseBusinessAccount || user.id,
                user.baseBusinessAccount || user.id,
                skip,
                limitNumb,
                filter,
                order,
                draft
            )

            const data = await query.getManyAndCount();

            res.status(200).send({ items: data[0], total: data[1] });
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getSingleArticle = async (req: any, res: any) => {
        try {
            const {
                user,
                params: { id = 1 }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const articleRepo = getRepository(Article);
            const query = AbstractController.getSinglePhotoVideoArticle(articleRepo, 'article', id, user.baseBusinessAccount || user.id)

            const article: any = await query.getOne()

            if(!article) return res.status(404).send({error: 'Not Found'})

            if(+article.user.id !== +user.id) {
                const target = article.user;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({error: 'Private account'});
                }
            }

            res.status(200).send(article);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    }

    static updateArticle = async(req, res) => {
        const user = req.user;

        if(user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }

        const articleRepo = getRepository(Article);

        try {
            const articleId = req.body.id;
            if (articleId) {
                const article: any = {
                    category: req.body.category,
                    draft: req.body.draft ? true : false,
                    location: req.body.location,
                    business_address: req.body.business_address,
                    text: req.body.text,
                    title: req.body.title,
                    updated: dateTime()
                };

                await articleRepo.update({id: articleId}, article);
                const updatedArticle = await articleRepo.findOne(articleId);
                updatedArticle.mentions = await getMentionRepository().addMention("article", updatedArticle, req.body.mentions || [], "article", user);
                await getHashtagRepository().updateHashtagsByPostId("article", articleId, req.body.hashtags || []);
                res.status(200).send(updatedArticle);
            } else {
                res.status(400).send({error: "No article ID"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteArticle = async(req, res) => {
        const user = req.user;

        if(user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }

        const articleRepo = getRepository(Article);
        try {
            const articleId = req.params.id;
            if (articleId) {
                await articleRepo.update({id: articleId}, {deleted: dateTime()});
                res.status(200).send({status: "success"});
            } else {
                res.status(400).send({error: "No ID field found"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static photoUploader = async(req, res) => {
        const user = req.user;
        const articlePhotoRepo = getRepository(Article_photo);
        try {


            res.status(400).send({error: "No ID field found"});

        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static likeArticle = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
            } = req;

            const likesRepo = getRepository(Article_Like);

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere("like.article_ = :id", { id })
                .getOne();

            const respObj = {
                status: "success",
                liked: false,
            };
            let content;
            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
            } else {
                const likeObj = new Article_Like();
                likeObj.user = userID;
                likeObj.article_ = id;
                content = await likesRepo.save(likeObj);

                respObj.liked = true;

                const articleRepo = getRepository(Article);
                const query = AbstractController.getSinglePhotoVideoArticleForNotification(articleRepo, 'article', id, req.user.baseBusinessAccount || req.user.id)
                const article: any = await query.getOne()

                await NotificationController.addNotification(
                    'liked',
                    'article',
                    'article',
                    article,
                    issuer,
                    article.user
                )
            }


            res.status(200).send(respObj);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getArticleLikes = async(req, res) => {
        try {
            const {
                user,
                query: { page = 1, limit = 15 },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Article_Like)
                .createQueryBuilder("like")
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .leftJoinAndMapOne(`like_user.following`, `like_user.followers`, `like_user_followers`, `like_user_followers.account = ${user.baseBusinessAccount || user.id}`)
                .where("like.article_ = :id", { id })
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static addComment = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: {
                    text,
                    replyTo,
                    mentions = []
                },
            } = req;

            if (!text) throw Error("Empty text field");
            const commentRepo = getRepository(Article_Comment);
            const articleRepo = getRepository(Article);
            const data: any = {};
            let article, comment, issuer, savedComment, savedMentions;

            if (replyTo) {
                const repliesRepo = getRepository(Article_Comment_Replies);

                [ article, issuer, comment ] = await Promise.all([
                  AbstractController.getSinglePhotoVideoArticleForNotification(articleRepo, 'article', id, req.user.baseBusinessAccount || req.user.id).getOne(),
                  getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID }),
                  commentRepo.createQueryBuilder("comment").select("comment.id").where("id = :id", { id: replyTo }).getOne()
                ])

                // const comment = await commentRepo
                //     .createQueryBuilder()
                //     .where("id = :id", { id: replyTo })
                //     .getOne();

                if (!comment) throw Error("Comment not exist");

                const reply = new Article_Comment_Replies();
                reply.user = userID;
                reply.comment_ = replyTo;
                reply.article_ = id;
                reply.text = text;

                [savedComment, savedMentions] = await Promise.all([
                  data.comment = await repliesRepo.save(reply),
                  data.comment.mentions = await getMentionRepository().addMention("article_comment_reply", article, mentions,"article",issuer,data.comment)
                ])
              } else {
                [ article, issuer ] = await Promise.all([
                  AbstractController.getSinglePhotoVideoArticleForNotification(articleRepo, 'article', id, req.user.baseBusinessAccount || req.user.id).getOne(),
                  getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })
                ])
                const comment = new Article_Comment();
                comment.user = userID;
                comment.article_ = id;
                comment.text = text;
                [savedComment, savedMentions] = await Promise.all([
                  commentRepo.save(comment),
                  getMentionRepository().addMention("article_comment", article, mentions,"article",issuer,data.comment)
                ])
              }
              data.comment = savedComment;
              data.comment.mentions = savedMentions;
              res.status(200).send(data);

              const action = replyTo ? 'replied' : 'commented'
              const content_display_name = replyTo ? 'comment' : 'article'

              console.log('article', article)

              NotificationController.addNotification(
                action,
                content_display_name,
                "article",
                article,
                issuer,
                article.user
              )


        } catch (error) {
            console.log('article_addComment error', error)
            res.status(400).send({error: error.message});
        }
    };

    static editComment = async (req, res) => {
        return await AbstractController.editComment(
            req,
            res,
            Article_Comment,
            Article_Comment_Replies,
            'article_comment_reply',
            'article_comment',
        )
    }

    static getComments = async(req, res) => {
        return await AbstractController.getComments(req, res, Article_Comment, 'article_', 'article');
    };

    static getReplies = async(req, res) => {
      return await AbstractController.getReplies(req, res, Article_Comment_Replies, 'article');
        // try {
        //     const {
        //         user,
        //         params: { id },
        //         query: { page = 1, filterBy = "date", limit = 15, order = "asc" }
        //     } = req;

        //     const replyRepo = getRepository(Article_Comment_Replies);

        //     const filter: Filter = {
        //         by: filterBy.toLowerCase() === "likes" ? "reply_total_likes" : "reply_created",
        //         order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
        //     };
        //     const limitNumb = Number(limit);
        //     const skip = (Number(page) - 1) * limitNumb;

        //     const replies: any = await replyRepo
        //         .createQueryBuilder("reply")
        //         .leftJoinAndSelect("reply.user", "user")
        //         .leftJoin("user.business", "user_business")
        //         .addSelect("user_business.id")
        //         .addSelect("user_business.photo")
        //         .addSelect("user_business.name")
        //         .addSelect("user_business.pageName")
        //         .leftJoin("user.person", "user_person")
        //         .addSelect("user_person.id")
        //         .addSelect("user_person.photo")
        //         .addSelect("user_person.fullName")
        //         .addSelect("user_person.pageName")
        //         .leftJoinAndSelect("reply.mentions", "reply_mention")
        //         .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
        //         .leftJoin("reply_mention_target.business", "reply_mention_target_business")
        //         .addSelect("reply_mention_target_business.id")
        //         .addSelect("reply_mention_target_business.photo")
        //         .addSelect("reply_mention_target_business.name")
        //         .addSelect("reply_mention_target_business.pageName")
        //         .leftJoin("reply_mention_target.person", "reply_mention_target_person")
        //         .addSelect("reply_mention_target_person.id")
        //         .addSelect("reply_mention_target_person.photo")
        //         .addSelect("reply_mention_target_person.fullName")
        //         .addSelect("reply_mention_target_person.pageName")
        //         .addSelect("reply.created", "reply_created")
        //         .leftJoinAndMapOne(`reply.liked`, `reply.likes`, `reply_likes`, `reply_likes.user_id = ${user.baseBusinessAccount || user.id}`)
        //         .andWhere("reply.comment_ = :id", { id })
        //         .skip(skip)
        //         .take(limitNumb)
        //         .orderBy( filter.by, filter.order)
        //         .getMany();

        //     res.status(200).send({ replies });

        // } catch (error) {
        //     res.status(400).send({error: error.message});
        // }
    };

    static likeComment = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: { reply },
            } = req;

            const likesRepo = getRepository(Article_Comment_Like);

            // AbstractController.getSinglePhotoVideoArticle(articleRepo, 'article', id, req.user.baseBusinessAccount || req.user.id)
            const commentContext = reply ? Article_Comment_Replies : Article_Comment
            const articleRepo = getRepository(commentContext);
            const query = AbstractController.getSingleComment(articleRepo, 'article', id, req.user.baseBusinessAccount || req.user.id)
            const comment: any = await query.getOne()

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere(`like.${reply ?"reply_" : "comment_"} = :id`, { id })
                .getOne();

            let liked: boolean;
            let content;
            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
                liked = false;
            } else {
                const likeObj = new Article_Comment_Like();
                likeObj.user = userID;
                likeObj.article_ = comment.article_ // check it
                if (reply) {
                    likeObj.reply_ = comment;
                } else {
                    likeObj.comment_ = comment;
                }
                content = await likesRepo.save(likeObj);
                liked = true;

                await NotificationController.addNotification(
                    'liked',
                    'comment',
                    'article',
                    comment.article_,
                    issuer,
                    comment.user
                )
            }

            res.status(200).send({liked});

        } catch (error) {
            console.log('article_comment_like error', error)
            res.status(400).send({error: error.message});
        }
    };

    static getCommentLikes = async(req, res) => {
        try {
            const {
                query: { page = 1, limit = 15, reply = false },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Article_Comment_Like)
                .createQueryBuilder("like")
                .where(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteComment = async(req, res) => {
        try {
            const {
                query: { reply = false },
                params: { id },
                user
            } = req;

            const repo = reply ? getRepository(Article_Comment_Replies) : getRepository(Article_Comment);

            const deleted = await repo.delete({ id });

            if (deleted.affected) {
                res.status(200).send({ status: "success" });
            } else {
                res.status(404).send({ status: "comment not found" });
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };
}
