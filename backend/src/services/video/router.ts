import {Middleware} from "../../utils/middleware";

const express = require("express");
const router = express.Router();

import passport = require("passport");
import { VideoController } from "./controller";
import { ViewController } from "../view/controller";

router.post("/", passport.authenticate("jwt", {session: false}), VideoController.addVideo);
router.use("/:id/upload", passport.authenticate("jwt", {session: false}), VideoController.uploadVideo);

router.get("/all", passport.authenticate("jwt", {session: false}),  VideoController.getVideos);
router.get("/:id", passport.authenticate("jwt", {session: false}), Middleware.checkInBlaklist("video", "id"), VideoController.getSingleVideo);
router.put("/", passport.authenticate("jwt", {session: false}), VideoController.updateVideo);
router.delete("/:id", passport.authenticate("jwt", {session: false}), VideoController.deleteVideo);

router.post("/:id/like", passport.authenticate("jwt", {session: false}), VideoController.likeVideo);

router.get("/:id/like", passport.authenticate("jwt", {session: false}), VideoController.getVideoLikes);

router.post("/:id/view", passport.authenticate("jwt", {session: false}), ViewController.view("video"));

router.post("/:id/comment", passport.authenticate("jwt", {session: false}), VideoController.addComment);
router.put("/:id/comment", passport.authenticate("jwt", {session: false}), VideoController.editComment);
router.get("/:id/comment", passport.authenticate("jwt", {session: false}), VideoController.getComments);

router.get("/comment/:id/replies", passport.authenticate("jwt", {session: false}), VideoController.getReplies);

router.post("/comment/:id/like", passport.authenticate("jwt", {session: false}), VideoController.likeComment);

router.get("/comment/:id/like", passport.authenticate("jwt", {session: false}), VideoController.getCommentLikes);

router.delete("/comment/:id", passport.authenticate("jwt", {session: false}), VideoController.deleteComment);

export = router;
