const dateTime = require("date-time");
import uniqid = require("uniqid");
const ffmpegInstaller = require("@ffmpeg-installer/ffmpeg");
const ffmpeg = require("fluent-ffmpeg");
const axios = require("axios");
const http = require("http");
import * as fs from 'fs';

ffmpeg.setFfmpegPath(ffmpegInstaller.path);

import TimeFormat = require("hh-mm-ss");
import { getRepository } from "typeorm";
import { Video } from "../../entities/video";
import { App } from "../../config/app.config";
import { Video_Like } from "../../entities/video_like";
import { Filter } from "../../interfaces/global/filter.interface";
import { Video_Comment } from "../../entities/video_comment";
import { Video_Comment_Replies } from "../../entities/video_comment_reply";
import { Video_Comment_Like } from "../../entities/video_comment_like";
import getMentionRepository from "../../entities/repositories/mention-repository";
import { Helpers } from "../../utils/helpers";
import MinioInstance from "../../utils/minio";
import { tusServer, decodeMetadata, TUS_EVENTS } from "../../utils/tus-server";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import { Request, Response } from "express";
import getHashtagRepository from "../../entities/repositories/hashtag-repository";
import { MediaConverter, ConvertedVideo, ConvertedPhoto } from "../../utils/media-converter";
import { publish } from "../../utils/sse";
import { resolve } from "url";
import getAccountRepository from "../../entities/repositories/account-repository";
import getFollowingRepository from "../../entities/repositories/following-repository";
import { NotificationController } from "../notifications/controller";
import { NotificationContentType } from "../../entities/notification";
import { Bookmark } from "../../entities/bookmarks";

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata)
    if (metadata.entityType === 'video') {
        // console.log('upload video completed metadata', metadata)
        // console.log('upload video completed event', event)
        const tmpFileId = event.file.id;
        await VideoController.onUploadEnd(tmpFileId, metadata);
    }
});

export class VideoController {
    static io;

    static addVideo = async (req, res) => {
        try {
            const {
                body: {
                    draft,
                    title,
                    category,
                    description,
                    mentions = [],
                    hashtags = [],
                    location = [],
                    business_address,
                }, user,
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const videoRepo = getRepository(Video);
            const video = new Video();
            video.category = category;
            video.description = description;
            video.location = location;
            video.business_address = business_address;
            video.title = title;
            video.draft = !!draft;
            video.user = user.baseBusinessAccount || user.id;
            const savedVideo = await videoRepo.save(video);
            savedVideo.mentions = await getMentionRepository().addMention("video", savedVideo, mentions, "video", user);
            await getHashtagRepository().addHashtag("video", savedVideo.id, hashtags);
            res.status(200).send(savedVideo);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static uploadVideo = async (req: Request, res: Response) => {
        if(req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }

        if (+req.headers['upload-length'] > App.FILE_SIZE_LIMIT * 1024 * 1024) {
            const videoRepo = getRepository(Video);
            const metadata = decodeMetadata(req.headers['upload-metadata'])

            // TODO: DELETE video from the db addVideo
            videoRepo.delete({ id: metadata.id })

            return res.status(400).send(`A file can not be larger than ${App.FILE_SIZE_LIMIT} Mb`);
        }

        return tusServer.handle(req, res);
    }

    static onUploadEnd = async function (tmpFileId, metadata) {
        const { id, type, userId } = metadata;

        let newFilePath: string;
        let outputFiles: ConvertedPhoto;

        try {
            const fileType = type;
            const videoRepo = getRepository(Video);
            const video = await videoRepo.findOne({ id: id });
            // calculate oldFilePath because the file is uploaded, but it have encoded filename and no file type
            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;



            if (fileType.includes('video')) {
                const uniqueFileName = uniqid("video-");
                const spliType = (fileType as string).split('/');
                const fileFormat = App.VIDEO_FORMATS.get(spliType[1]);

                const fullFileName = `${uniqueFileName}${fileFormat}`;
                newFilePath = `${App.UPLOAD_FOLDER}/${uniqueFileName}/${fullFileName}`;
                const needScreenshot = !video.cover

                if (!fs.existsSync(`${App.UPLOAD_FOLDER}/${uniqueFileName}`)){
                    fs.mkdirSync(`${App.UPLOAD_FOLDER}/${uniqueFileName}`);
                }


                console.log('oldFilePath', oldFilePath)
                console.log('newFilePath', newFilePath)
                fs.rename(oldFilePath, newFilePath, async (err) => {

                    video.duration = (await VideoController.getVideoDuration({ video: newFilePath })) as string;
                    if (err) throw err;
                    console.log('Rename complete!');

                        http.get({
                            agent: false,
                            path: `/media-server-events/${userId}/${id}`,
                            host: App.SHAKA_SERVER_HOST,
                            port: App.SHAKA_SERVER_PORT
                            // hostname: 'shaka:5000'
                        }, (response) => {
                            response.on('data', async (buf) => {
                                const json = JSON.parse(buf.toString())
                                // console.log('buf', buf)
                                // console.log('json', json)
                                if(json.data) {
                                    const data = JSON.parse(json.data);
                                    if(data.videoId === id) {
                                        console.log('videoId', id)
                                        // console.log('video', video)
                                        console.log('data', data)
                                        video.video = data.links.video;
                                        if(data.links.cover) video.cover = data.links.cover
                                        await videoRepo.save(video);
                                        publish({ userId, videoId: id, type: 'file_processing_complete', entityType: 'video', entity: video });
                                    }
                                }
                            })
                        })
                        .on('error', (e) => {
                            console.error(`/media-server-events/${userId}/${id} - Got error: ${e.message}`);
                            throw new Error(e)
                        })

                    const axiosResponse = await axios.post(`http://${App.SHAKA_SERVER_HOST}:${App.SHAKA_SERVER_PORT}/convert`, {
                        "input_filename": uniqueFileName,
                        "input_extension": fileFormat.substr(1),
                        "sizes": ["1920x1080", "1280x720", "854x480", "640x360", "426x240","256x144"],
                        "needScreenshot": needScreenshot,
                        userId,
                        videoId: id
                    })
                    console.log('axiosResponse.data', axiosResponse.data)
                });
            } else
                if (fileType.includes('image')) {
                    const uniqueFileName = uniqid("cover-");
                    const spliType = (fileType as string).split('/');
                    const fileFormat = spliType[1];

                    const fullFileName = `${uniqueFileName}.${fileFormat}`;
                    newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

                    fs.rename(oldFilePath, newFilePath, async (err) => {
                        if (err) throw err;
                        return MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                            .then(async () => {
                                video.cover = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                                video.thumb = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`
                                await videoRepo.save(video);
                                publish({ userId, type: 'file_processing_complete', entityType: 'video', entity: video });
                                // fs.unlinkSync(newFilePath);
                            })
                    })

                    // await videoRepo.save(video);
                }


        } catch (error) {
            console.error('Error while video uploading', error);

            if (newFilePath) {
                fs.unlinkSync(newFilePath);
            }
            if (outputFiles && outputFiles.photo) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.photo}`);
            }
            if (outputFiles && outputFiles.thumb) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.thumb}`);
            }
        }
    }

    static getVideos = async (req, res) => {

        try {
            const {
                user,
                query: { page = 1, filter = "date", limit = 15, order = "desc", draft = false, userId = null },
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            if(userId && +userId !== +user.id) {
                const targetAccount = await getAccountRepository().getFullAccountInfo({ key: 'id', val: userId })
                const target = targetAccount;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({error: 'Private account'});
                }
            }

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            const videoRepo = getRepository(Video);

            const query = AbstractController.buildPhotoVideoArticleListQuery(
                videoRepo,
                'article',
                userId || user.baseBusinessAccount || user.id,
                user.baseBusinessAccount || user.id,
                skip,
                limitNumb,
                filter,
                order,
                draft
            )

            const data = await query.getManyAndCount()

            res.status(200).send({ items: data[0], total: data[1] });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getSingleVideo = async (req, res) => {

        try {
            const {
                user,
                params: { id = 1 }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const videoRepo = getRepository(Video);
            const query = AbstractController.getSinglePhotoVideoArticle(videoRepo, 'video', id, user.baseBusinessAccount || user.id)
            const video: any = await query.getOne()

            if (!video) return res.status(404).send({ error: 'Not Found' })

            if(+video.user.id !== +user.id) {
                const target = video.user;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({error: 'Private account'});
                }

            }

            res.status(200).send(video);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }

    }

    static updateVideo = async (req, res) => {
        const user = req.user;
        const videoRepo = getRepository(Video);
        try {
            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const videoId = req.body.id;
            if (videoId) {
                const videoInfo = {
                    category: req.body.category,
                    description: req.body.description,
                    location: req.body.location,
                    business_address: req.body.business_address,
                    title: req.body.title,
                    cover: req.body.cover,
                    draft: !!req.body.draft,
                    updated: dateTime(),
                };
                await videoRepo.update({ id: videoId }, videoInfo);
                const updatedVideo = await videoRepo.findOne(videoId);
                await getHashtagRepository().updateHashtagsByPostId("video", updatedVideo.id, req.body.hashtags);
                updatedVideo.mentions = await getMentionRepository().addMention("video", updatedVideo, req.body.mentions, "video", user);

                res.status(200).send(updatedVideo);
            } else {
                res.status(400).send({ error: "No video ID" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static deleteVideo = async (req, res) => {
        const user = req.user;
        const videoRepo = getRepository(Video);
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const videoId = req.params.id;
            if (videoId) {
                await videoRepo.update(
                    { id: videoId },
                    { deleted: dateTime() }
                );
                res.status(200).send({ status: "success" });
            } else {
                res.status(400).send({ error: "No ID field found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getVideoDuration = async video => {
        return new Promise((resolve, reject) => {
            ffmpeg.ffprobe(video.video, function (err, metadata) {
                if (err) {
                    reject(err);
                } else {
                    const duration = TimeFormat.fromS(parseInt(metadata.format.duration));
                    resolve(duration);
                }
            });
        });
    };

    static likeVideo = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
            } = req;

            const likesRepo = getRepository(Video_Like);

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere("like.video_ = :id", { id })
                .getOne();

            const respObj = {
                status: "success",
                liked: false,
            };

            let content;

            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
            } else {
                const likeObj = new Video_Like();
                likeObj.user = userID;
                likeObj.video_ = id;
                content = await likesRepo.save(likeObj);
                respObj.liked = true;


                const videoRepo = getRepository(Video);
                const query = AbstractController.getSinglePhotoVideoArticleForNotification(videoRepo, 'video', id, req.user.baseBusinessAccount || req.user.id)
                const video: any = await query.getOne()

                await NotificationController.addNotification(
                    'liked',
                    'video',
                    "video",
                    video,
                    issuer,
                    video.user
                )
            }


            res.status(200).send(respObj);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getVideoLikes = async (req, res) => {
        try {
            const {
                user,
                query: { page = 1, limit = 15 },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Video_Like)
                .createQueryBuilder("like")
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .leftJoinAndMapOne(`like_user.following`, `like_user.followers`, `like_user_followers`, `like_user_followers.account = ${user.baseBusinessAccount || user.id}`)
                .where("like.video_ = :id", { id })
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static addComment = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: { text, replyTo, mentions = [] },
            } = req;

            if (!text) throw Error("Empty text field");

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })


            const commentRepo = getRepository(Video_Comment);
            const data: any = {};

            const videoRepo = getRepository(Video);

            const query = AbstractController.getSinglePhotoVideoArticleForNotification(videoRepo, 'article', id, req.user.baseBusinessAccount || req.user.id)


            const video: any = await query.getOne()

            if (replyTo) {
                const repliesRepo = getRepository(Video_Comment_Replies);
                const comment = await commentRepo
                    .createQueryBuilder()
                    .where("id = :id", { id: replyTo })
                    .getOne();

                if (!comment) {
                    throw Error("Comment not exist");
                }

                const reply = new Video_Comment_Replies();
                reply.user = userID;
                reply.comment_ = replyTo;
                reply.video_ = id;
                reply.text = text;
                data.comment = await repliesRepo.save(reply);

                data.comment.mentions = await getMentionRepository().addMention("video_comment_reply", video, mentions,"video", issuer, data.comment);
            } else {
                const comment = new Video_Comment();
                comment.user = userID;
                comment.video_ = id;
                comment.text = text;
                data.comment = await commentRepo.save(comment);

                data.comment.mentions = await getMentionRepository().addMention("video_comment", video, mentions,"video", issuer, data.comment);
            }

            // const query = replyTo ?
            // AbstractController.getSingleComment(commentRepo, 'video', replyTo, req.user.baseBusinessAccount || req.user.id)
            // :AbstractController.getSinglePhotoVideoArticle(videoRepo, 'video', id, req.user.baseBusinessAccount || req.user.id)
            const action = replyTo ? 'replied' : 'commented'
            const content_display_name = replyTo ? 'comment' : 'video'
            // const video_content_type: string = replyTo ? "video_comment_reply" : "video_comment";

            await NotificationController.addNotification(
                action,
                content_display_name,
                // video_content_type as NotificationContentType,
                "video",
                video,
                issuer,
                video.user
            )

            res.status(200).send(data);
        } catch (error) {
            console.log('add comment error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static editComment = async (req, res) => {
        return await AbstractController.editComment(
            req,
            res,
            Video_Comment,
            Video_Comment_Replies,
            'video_comment_reply',
            'video_comment',
        )
    }

    static getComments = async (req, res) => {
        return await AbstractController.getComments(req, res, Video_Comment, 'video_', 'video');
    };

    static getReplies = async (req, res) => {
        return await AbstractController.getReplies(req, res, Video_Comment_Replies, 'video');
        // try {
        //     const {
        //         user,
        //         params: { id },
        //         query: { page = 1, filterBy = "date", limit = 15, order = "asc" },
        //     } = req;

        //     const replyRepo = getRepository(Video_Comment_Replies);
        //     const filter: Filter = {
        //         by: filterBy.toLowerCase() === "likes" ? "reply_total_likes" : "reply_created",
        //         order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
        //     };
        //     const limitNumb = Number(limit);
        //     const skip = (Number(page) - 1) * limitNumb;

        //     const replies: any = await replyRepo
        //         .createQueryBuilder("reply")
        //         .leftJoinAndSelect("reply.user", "user")
        //         .leftJoin("user.business", "user_business")
        //         .addSelect("user_business.id")
        //         .addSelect("user_business.photo")
        //         .addSelect("user_business.name")
        //         .addSelect("user_business.pageName")
        //         .leftJoin("user.person", "user_person")
        //         .addSelect("user_person.id")
        //         .addSelect("user_person.photo")
        //         .addSelect("user_person.fullName")
        //         .addSelect("user_person.pageName")
        //         .leftJoinAndSelect("reply.mentions", "reply_mention")
        //         .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
        //         .leftJoin("reply_mention_target.business", "reply_mention_target_business")
        //         .addSelect("reply_mention_target_business.id")
        //         .addSelect("reply_mention_target_business.photo")
        //         .addSelect("reply_mention_target_business.name")
        //         .addSelect("reply_mention_target_business.pageName")
        //         .leftJoin("reply_mention_target.person", "reply_mention_target_person")
        //         .addSelect("reply_mention_target_person.id")
        //         .addSelect("reply_mention_target_person.photo")
        //         .addSelect("reply_mention_target_person.fullName")
        //         .addSelect("reply_mention_target_person.pageName")
        //         .addSelect("reply.created", "reply_created")
        //         .leftJoinAndMapOne(`reply.liked`, `reply.likes`, `reply_likes`, `reply_likes.user_id = ${user.baseBusinessAccount || user.id}`)
        //         .andWhere("reply.comment_ = :id", { id })
        //         .skip(skip)
        //         .take(limitNumb)
        //         .orderBy(filter.by, filter.order)
        //         .getMany();

        //     res.status(200).send({ replies });
        // } catch (error) {
        //     res.status(400).send({ error: error.message });
        // }
    };

    static likeComment = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: { reply },
            } = req;

            const likesRepo = getRepository(Video_Comment_Like);
            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const commentContext = reply ? Video_Comment_Replies : Video_Comment
            const videoRepo = getRepository(commentContext);
            const query = AbstractController.getSingleComment(videoRepo, 'video', id, req.user.baseBusinessAccount || req.user.id)
            const comment: any = await query.getOne()

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .getOne();

            let liked: boolean;
            let content;
            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
                liked = false;
            } else {
                const likeObj = new Video_Comment_Like();
                likeObj.user = userID;
                likeObj.video_ = comment.video_
                if (reply) {
                    likeObj.reply_ = comment;
                } else {
                    likeObj.comment_ = comment;
                }
                content = await likesRepo.save(likeObj);
                liked = true;


                await NotificationController.addNotification(
                    'liked',
                    'comment',
                    'video',
                    comment.video_,
                    issuer,
                    comment.user
                )
            }


            res.status(200).send({ liked });

        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getCommentLikes = async (req, res) => {
        try {
            const {
                query: { page = 1, limit = 15, reply = false },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Video_Comment_Like)
                .createQueryBuilder("like")
                .where(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static deleteComment = async (req, res) => {
        try {
            const {
                query: { reply = false },
                params: { id },
                user
            } = req;

            const repo = reply ? getRepository(Video_Comment_Replies) : getRepository(Video_Comment);

            const deleted = await repo.delete({ id });

            if (deleted.affected) {
                res.status(200).send({ status: "success" });
            } else {
                res.status(404).send({ status: "comment not found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };
}
