import { Business } from './../../entities/business';
import { MediaConverter, ConvertedPhoto } from './../../utils/media-converter';
const dateTime = require("date-time");
import uniqid = require("uniqid");
import shortid = require("shortid");
import base64Img = require("base64-img");
import * as fs from 'fs';
import { getRepository } from "typeorm";
import { App } from "../../config/app.config";
import { Photo } from "../../entities/photo";
import { Profile_photo } from "../../entities/profile_photo";
import { Photo_Like } from "../../entities/photo_like";
import { Filter } from "../../interfaces/global/filter.interface";
import { Photo_Comment } from "../../entities/photo_comment";
import { Photo_Comment_Replies } from "../../entities/photo_comment_reply";
import { Photo_Comment_Like } from "../../entities/photo_comment_like";
import getMentionRepository from "../../entities/repositories/mention-repository";
import { Photo_Annotation } from "../../entities/photo_annotation";
import { Helpers } from "../../utils/helpers";
import MinioInstance from "../../utils/minio";
import { tusServer, TUS_EVENTS, decodeMetadata } from "../../utils/tus-server";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import getHashtagRepository from "../../entities/repositories/hashtag-repository";
import { publish } from '../../utils/sse';
import getAccountRepository from '../../entities/repositories/account-repository';
import getFollowingRepository from '../../entities/repositories/following-repository';
import { NotificationController } from '../notifications/controller';
import { NotificationContentType } from '../../entities/notification';
import { Bookmark } from '../../entities/bookmarks';
import { Account } from '../../entities/account';

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata);
    if (metadata.entityType === 'photo') {
        // console.log('upload photo completed metadata', metadata)
        // console.log('upload photo completed event', event)
        const tmpFileId = event.file.id
        await PhotoController.onUploadEnd(tmpFileId, metadata)
    }
});

export class PhotoController {
    static addPhoto = async (req: any, res: any) => {
        const user = req.user;

        if (user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if (!permissions.canWrite) return res.status(403).send({ error: 'Forbidden' });
        }

        const {
            mentions = [],
            hashtags = []
        } = req.body;
        const photoRepo = getRepository(Photo);
        try {
            const savedPhoto = await photoRepo.save({
                user: user.baseBusinessAccount || user.id,
                title: req.body.title,
                description: req.body.description,
                category: req.body.category,
                location: req.body.location,
                business_address: req.body.business_address,
                filterName: req.body.filterName,
                draft: req.body.draft,
            }, { reload: true });
            await getMentionRepository().addMention("photo", savedPhoto, mentions, "photo", user);
            await getHashtagRepository().addHashtag("photo", savedPhoto.id, hashtags);
            await PhotoController.addAnnotations(savedPhoto, req.body.annotations, user);
            res.status(200).send(savedPhoto);
        } catch (error) {
            console.error('error while photo adding', error);
            res.status(400).send({ error: error.message });
        }
    };

    static uploadPhoto = async (req: any, res: any) => {
        if (req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if (!permissions.canWrite) return res.status(403).send({ error: 'Forbidden' });
        }

        return tusServer.handle(req, res);
    };

    static onUploadEnd = async (tmpFileId, metadata) => {
        // console.log('upload end', tmpFileId, metadata)
        let newFilePath;
        let outputFiles;
        const { id, type, userId } = metadata;
        try {
            const fileType = type;
            const photoRepo = getRepository(Photo);
            const photo = await photoRepo.findOne({ id });
            // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type

            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;

            const uniqueFileName = uniqid("photo-");
            const spliType = (fileType as string).split('/');
            const fileFormat = spliType[1];

            const fullFileName = `${uniqueFileName}.${fileFormat}`;
            // const fullFileName = `${uniqueFileName}.${fileFormat === 'jpeg' ? 'jpg' : fileFormat}`;

            newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

            fs.renameSync(oldFilePath, newFilePath);
            console.log('newFilePath', newFilePath)

            MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                .then(async () => {
                    photo.photo = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                    photo.thumb = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`
                    await photoRepo.save(photo);
                    console.log('minio success, publish SSE')
                    publish({ userId, type: 'file_processing_complete', entityType: 'photo', entity: photo });

                })

        } catch (error) {
            console.error('Error while photo uploading', error);

            if (newFilePath) {
                fs.unlinkSync(newFilePath);
            }
            if (outputFiles.photo) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.photo}`);
            }
            if (outputFiles.thumb) {
                fs.unlinkSync(`${App.UPLOAD_FOLDER}/${outputFiles.thumb}`);
            }
        }
    }

    static savePhoto = async (photoData: any, userId: any) => {
        if (!photoData) {
            return {
                photo: null,
                user: userId,
            }
        } else {
            try {
                const { photo } = await PhotoController.base64ToFile(photoData);
                return {
                    photo,
                    user: userId
                };
            } catch (error) {
                throw error;
            }
        }
    };

    static base64ToFile = async (photoData: any, photoSize = 750, thumbSize = 250) => {
        try {
            const url = shortid.generate();
            return new Promise<ConvertedPhoto>(async (resolve, reject) => {
                base64Img.img(photoData, App.UPLOAD_FOLDER, url, async (error: any, filepath: string) => {
                    if (error) {
                        reject(error);
                    } else {
                        await MediaConverter.convertPhotoAndUploadOnMinio(filepath, url, photoSize, thumbSize);

                        const photo = `${App.MINIO_PUBLIC_URL}/photo/${url}-photo.jpg`;
                        const thumb = `${App.MINIO_PUBLIC_URL}/photo/${url}-thumb.jpg`;

                        resolve({ photo, thumb });
                    }
                });

            });
        } catch (error) {
            throw error;
        }
    };

    static getPhotos = async (req: any, res: any) => {
        try {
            const {
                user,
                query: { page = 1, filter = "date", limit = 15, order = "desc", draft = false, userId = null }
            } = req;

            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if (!permissions.canRead) return res.status(403).send({ error: 'Forbidden' });
            }

            if (userId && +userId !== +user.id) {
                const targetAccount = await getAccountRepository().getFullAccountInfo({ key: 'id', val: userId })
                const target = targetAccount;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({ error: 'Private account' });
                }
            }

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            const photoRepo = getRepository(Photo);

            const query = AbstractController.buildPhotoVideoArticleListQuery(
                photoRepo,
                'photo',
                userId || user.baseBusinessAccount || user.id,
                user.baseBusinessAccount || user.id,
                skip,
                limitNumb,
                filter,
                order,
                draft
            )

            const data = await query.getManyAndCount();




            res.status(200).send({ items: data[0], total: data[1] });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getSinglePhoto = async (req: any, res: any) => {
        try {
            const {
                user,
                params: { id = 1 }
            } = req;

            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user);
                if (!permissions.canRead) return res.status(403).send({ error: 'Forbidden' });
            }

            const photoRepo = getRepository(Photo);
            const query = AbstractController.getSinglePhotoVideoArticle(photoRepo, 'photo', id, user.baseBusinessAccount || user.id);

            const photo: any = await query.getOne();

            if (!photo) {
                return res.status(404).send({ error: 'Not Found' });
            }

            if (+photo.user.id !== +user.id) {
                const target = photo.user;
                const account = user.id;

                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);

                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    // No such field as target.person && target.person.privacy
                    return res.status(403).send({ error: 'Private account' });
                }
            }
            res.status(200).send(photo);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getPhotoTags = async (req, res) => {
        try {
            const {
                user,
                query: { page = 1, limit = 15 },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const tagdBy = await getRepository(Photo_Annotation)
                .createQueryBuilder("annotations")
                .leftJoin("annotations.target", "annotations_target")
                .addSelect("annotations_target.id")
                .leftJoin('annotations_target.person', "account_person")
                .addSelect("account_person.id")
                .addSelect("account_person.thumb")
                .addSelect("account_person.fullName")
                .addSelect("account_person.pageName")
                .leftJoin("annotations_target.business", "account_business")
                .addSelect("account_business.id")
                .addSelect("account_business.thumb")
                .addSelect("account_business.name")
                .addSelect("account_business.pageName")


                .where("annotations.photo = :id", { id })
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()

            res.status(200).send(tagdBy);
        } catch (error) {
            console.log('getPhotoTags error',error)
            res.status(400).send({ error: error.message });
        }
    };

    static updatePhoto = async (req: any, res: any) => {
        const user = req.user;

        if (user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if (!permissions.canWrite) return res.status(403).send({ error: 'Forbidden' });
        }

        const photoRepo = getRepository(Photo);
        try {
            const photoId = req.body.id;
            const photoData = req.body.photo;
            if (photoId) {
                let uploadedPhoto;

                if (photoData) {
                    uploadedPhoto = await PhotoController.savePhoto(photoData, user.baseBusinessAccount || user.id);
                }

                const photoInfo = {
                    ...uploadedPhoto,
                    id: photoId,
                    title: req.body.title,
                    description: req.body.description,
                    category: req.body.category,
                    location: req.body.location,
                    business_address: req.body.business_address,
                    filterName: req.body.filterName,
                    draft: req.body.draft,
                    updated: dateTime()
                };

                await photoRepo.update({ id: photoId }, photoInfo);
                await getHashtagRepository().updateHashtagsByPostId("photo", photoId, req.body.hashtags);
                await getMentionRepository().addMention("photo", photoInfo, req.body.mentions, "photo", user);

                const query = AbstractController.getSinglePhotoVideoArticle(photoRepo, 'photo', photoId, user.baseBusinessAccount || user.id);
                let updatedPhoto: any = await query.getOne();

                await PhotoController.addAnnotations(updatedPhoto, req.body.annotations, user);

                updatedPhoto = await query.getOne();

                res.status(200).send(updatedPhoto);
            } else {
                res.status(200).send({ error: "No photo ID" });
            }
        } catch (error) {
            console.error('error while photo updating', error);
            res.status(400).send({ error: error.message });
        }
    };

    static updateProfilePhoto = async (photoData: any, userId: any, isDeleted: boolean) => {
        const profilePhotoRepo = getRepository(Profile_photo);
        try {
            const profilePhoto = await profilePhotoRepo.findOne({ where: { user: userId, deleted: null } });
            if (!photoData && !isDeleted) {
                return { photo: profilePhoto.photoUrl, thumb: profilePhoto.thumbUrl };
            } else if (profilePhoto && isDeleted) {
                await profilePhotoRepo.update(profilePhoto.id, { deleted: dateTime() });
                return null;
            } else if (photoData) {
                const profilePhoto = new Profile_photo();
                const { photo, thumb } = await PhotoController.base64ToFile(photoData, 750, 250);
                profilePhoto.photoUrl = photo;
                profilePhoto.thumbUrl = thumb;
                profilePhoto.user = userId;
                await profilePhotoRepo.save(profilePhoto);
                if (profilePhoto) {
                    await profilePhotoRepo.update(profilePhoto.id, { deleted: dateTime() });
                }
                return { photo, thumb };
            } else {
                return null;
            }
        } catch (error) {
            throw error;
        }
    };

    static deletePhoto = async (req, res) => {
        const user = req.user;

        if (user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
            if (!permissions.canWrite) return res.status(403).send({ error: 'Forbidden' });
        }

        const photoRepo = getRepository(Photo);
        try {
            const photoId = req.params.id;
            if (photoId) {
                await photoRepo.update({ id: photoId }, { deleted: dateTime() });
                res.status(200).send({ status: "success" });
            } else {
                res.status(400).send({ error: "No ID field found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static likePhoto = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
            } = req;

            const likesRepo = getRepository(Photo_Like);

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere("like.photo_ = :id", { id })
                .getOne();

            const respObj = {
                status: "success",
                liked: false,
                comment: null
            };

            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
            } else {
                const likeObj = new Photo_Like();
                likeObj.user = userID;
                likeObj.photo_ = id;
                respObj.comment = await likesRepo.save(likeObj);

                respObj.liked = true;

                const photoRepo = getRepository(Photo);
                const query = AbstractController.getSinglePhotoVideoArticleForNotification(photoRepo, 'photo', id, req.user.baseBusinessAccount || req.user.id)
                const photo: any = await query.getOne()

                await NotificationController.addNotification(
                    'liked',
                    'photo',
                    'photo',
                    photo,
                    issuer,
                    photo.user
                )
            }


            res.status(200).send(respObj);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getPhotoLikes = async (req, res) => {
        try {
            const {
                user,
                query: { page = 1, limit = 15 },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Photo_Like)
                .createQueryBuilder("like")
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .leftJoinAndMapOne(`like_user.following`, `like_user.followers`, `like_user_followers`, `like_user_followers.account = ${user.baseBusinessAccount || user.id}`)
                .where("like.photo_ = :id", { id })
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static addComment = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: {
                    text,
                    replyTo,
                    mentions = [],
                },
            } = req;

            if (!text) throw Error("Empty text field");

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const commentRepo = getRepository(Photo_Comment);
            const data: any = {};


            const photoRepo = getRepository(Photo);
            // const  query = replyTo ?
            // AbstractController.getSingleComment(commentRepo, 'photo', replyTo, req.user.baseBusinessAccount || req.user.id)
            // : AbstractController.getSinglePhotoVideoArticle(photoRepo, 'photo', id, req.user.baseBusinessAccount || req.user.id)
            const query = AbstractController.getSinglePhotoVideoArticleForNotification(photoRepo, 'photo', id, req.user.baseBusinessAccount || req.user.id)
            const photo: any = await query.getOne()

            if (replyTo) {
                const repliesRepo = getRepository(Photo_Comment_Replies);

                const comment = await commentRepo
                    .createQueryBuilder()
                    .where("id = :id", { id: replyTo })
                    .getOne();

                if (!comment) throw Error("Comment not exist");

                const reply = new Photo_Comment_Replies();
                reply.user = userID;
                reply.comment_ = replyTo;
                reply.photo_ = id;
                reply.text = text;
                data.comment = await repliesRepo.save(reply);

                data.comment.mentions = await getMentionRepository().addMention("photo_comment_reply", photo, mentions, "photo", issuer, data.comment);
            } else {
                const comment = new Photo_Comment();
                comment.user = userID;
                comment.photo_ = id;
                comment.text = text;
                data.comment = await commentRepo.save(comment);

                data.comment.mentions = await getMentionRepository().addMention("photo_comment", photo, mentions, "photo", issuer, data.comment);
            }


            // const  photo_content_type : string = replyTo ? "photo_comment_reply" : "photo_comment";

            const action = replyTo ? 'replied' : 'commented'
            const content_display_name = replyTo ? 'comment' : 'photo'

            await NotificationController.addNotification(
                action,
                content_display_name,
                "photo",
                photo,
                issuer,
                photo.user
            )

            res.status(200).send(data);

        } catch (error) {
            console.log('add comment error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static editComment = async (req, res) => {
        return await AbstractController.editComment(
            req,
            res,
            Photo_Comment,
            Photo_Comment_Replies,
            'photo_comment',
            'photo_comment_reply',
        )
    }

    static getComments = async (req, res) => {
        return await AbstractController.getComments(req, res, Photo_Comment, 'photo_', 'photo');
    };

    static getReplies = async (req, res) => {
        return await AbstractController.getReplies(req, res, Photo_Comment_Replies, 'photo');
        // try {
        //     const {
        //         user,
        //         params: { id },
        //         query: { page = 1, filterBy = "date", limit = 15, order = "asc" }
        //     } = req;

        //     const replyRepo = getRepository(Photo_Comment_Replies);

        //     const filter: Filter = {
        //         by: filterBy.toLowerCase() === "likes" ? "reply_total_likes" : "reply_created",
        //         order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
        //     };
        //     const limitNumb = Number(limit);
        //     const skip = (Number(page) - 1) * limitNumb;

        //     const replies: any = await replyRepo
        //         .createQueryBuilder("reply")
        //         .leftJoinAndSelect("reply.user", "user")
        //         .leftJoin("user.business", "user_business")
        //         .addSelect("user_business.id")
        //         .addSelect("user_business.photo")
        //         .addSelect("user_business.name")
        //         .addSelect("user_business.pageName")
        //         .leftJoin("user.person", "user_person")
        //         .addSelect("user_person.id")
        //         .addSelect("user_person.photo")
        //         .addSelect("user_person.fullName")
        //         .addSelect("user_person.pageName")
        //         .leftJoinAndSelect("reply.mentions", "reply_mention")
        //         .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
        //         .leftJoin("reply_mention_target.business", "reply_mention_target_business")
        //         .addSelect("reply_mention_target_business.id")
        //         .addSelect("reply_mention_target_business.photo")
        //         .addSelect("reply_mention_target_business.name")
        //         .addSelect("reply_mention_target_business.pageName")
        //         .leftJoin("reply_mention_target.person", "reply_mention_target_person")
        //         .addSelect("reply_mention_target_person.id")
        //         .addSelect("reply_mention_target_person.photo")
        //         .addSelect("reply_mention_target_person.fullName")
        //         .addSelect("reply_mention_target_person.pageName")
        //         .addSelect("reply.created", "reply_created")
        //         .leftJoinAndMapOne(`reply.liked`, `reply.likes`, `reply_likes`, `reply_likes.user_id = ${user.baseBusinessAccount || user.id}`)
        //         .andWhere("reply.comment_ = :id", { id })
        //         .skip(skip)
        //         .take(limitNumb)
        //         .orderBy(filter.by, filter.order)
        //         .getMany();

        //     res.status(200).send({ replies });

        // } catch (error) {
        //     res.status(400).send({ error: error.message });
        // }
    };

    static likeComment = async (req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: { reply },
            } = req;

            const likesRepo = getRepository(Photo_Comment_Like);

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const commentContext = reply ? Photo_Comment_Replies : Photo_Comment
            const photoRepo = getRepository(commentContext);
            const query = AbstractController.getSingleComment(photoRepo, 'photo', id, req.user.baseBusinessAccount || req.user.id)
            const comment: any = await query.getOne()

            console.log('comment', comment)

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .getOne();

            let liked: boolean;
            let content;
            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
                liked = false;
            } else {
                const likeObj = new Photo_Comment_Like();
                likeObj.user = userID;
                likeObj.photo_ = comment.photo_
                if (reply) {
                    likeObj.reply_ = comment;
                } else {
                    likeObj.comment_ = comment;
                }
                content = await likesRepo.save(likeObj);
                liked = true;


                await NotificationController.addNotification(
                    'liked',
                    'comment',
                    'photo',
                    comment.photo_,
                    issuer,
                    comment.user
                )
            }

            res.status(200).send({ liked });

        } catch (error) {
            console.log('likeComment error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static getCommentLikes = async (req, res) => {
        try {
            const {
                query: { page = 1, limit = 15, reply = false },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Photo_Comment_Like)
                .createQueryBuilder("like")
                .where(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static deleteComment = async (req, res) => {
        try {
            const {
                query: { reply = false },
                params: { id },
                user
            } = req;

            const repo = reply ? getRepository(Photo_Comment_Replies) : getRepository(Photo_Comment);

            const deleted = await repo.delete({ id });

            if (deleted.affected) {
                res.status(200).send({ status: "success" });
            } else {
                res.status(404).send({ status: "comment not found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static checkPhotoOwnership = async (id, userID) => {
        try {
            const photoRepo = getRepository(Photo);

            return await photoRepo.findOne({
                where: {
                    id,
                    user: userID
                }
            });
        } catch (error) {
            throw error;
        }
    };

    static addAnnotation = async (req, res) => {
        try {
            const {
                user,
                params: { photoId }
            } = req;

            const annotations: Array<Photo_Annotation> = req.body.annotations;
            const userID = user.baseBusinessAccount || user.id
            const isOwner = await PhotoController.checkPhotoOwnership(photoId, userID);

            const photoRepo = getRepository(Photo);
            const query = AbstractController.getSinglePhotoVideoArticleForNotification(photoRepo, 'photo', photoId, userID)
            const photo: any = await query.getOne()

            let targetPhotoId;
            if (isOwner) {
                for (const annotation of annotations) {
                    annotation.photo = photoId;
                    targetPhotoId = annotation.target;
                    const owner = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: targetPhotoId })
                    const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })
                    await getRepository(Photo_Annotation).save(annotation);

                    const newNoty = await NotificationController.addNotification(
                        "tagged",
                        null,
                        "photo",
                        photo,
                        issuer,
                        owner
                    )
                }

                res.status(200).send({ status: "success" });
            } else {
                res.status(400).send({ error: "Access denied" });
            }
        } catch (error) {
            console.log('addAnnotation error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static deleteAnnotation = async (req, res) => {
        try {
            const {
                user,
                params: { photoId, annotationId }
            } = req;

            const isOwner = await PhotoController.checkPhotoOwnership(photoId, user.baseBusinessAccount || user.id);
            if (isOwner) {
                await getRepository(Photo_Annotation).delete(annotationId);
                res.status(200).send({ status: "success" });
            } else {
                res.status(400).send({ error: "Access denied" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static async addAnnotations(
        photo: Photo,
        annotations: Array<Photo_Annotation> = [],
        issuer: Account,
    ) {
        try {
            const repo = getRepository(Photo_Annotation);
            const oldAnnotations = await repo.find({ photo });
            const toRemoveMap: Map<number, Photo_Annotation> = new Map(oldAnnotations.map(item => ([item.id, item])));
            const result: Photo_Annotation[] = [];

            for (const annotation of annotations) {
                if (!annotation.id) {
                    const newItem = new Photo_Annotation();

                    newItem.target = annotation.target;
                    newItem.photo = photo;
                    newItem.x = annotation.x;
                    newItem.y = annotation.y;

                    const saved = await repo.save(newItem);

                    const newNotifi = await NotificationController.addNotification(
                        'tagged',
                        'photo',
                        'photo',
                        photo,
                        issuer,
                        annotation.target
                    );

                    const found = await repo.createQueryBuilder('annotation')
                        .leftJoinAndSelect('annotation.target', 'annotation_target')
                        .leftJoinAndSelect('annotation_target.person', 'annotation_target_person')
                        .addSelect("annotation_target_person.id")
                        .addSelect("annotation_target_person.photo")
                        .addSelect("annotation_target_person.fullName")
                        .addSelect("annotation_target_person.pageName")
                        .leftJoinAndSelect('annotation_target.business', 'annotation_target_business')
                        .addSelect("annotation_target_business.id")
                        .addSelect("annotation_target_business.photo")
                        .addSelect("annotation_target_business.name")
                        .addSelect("annotation_target_business.pageName")
                        .whereInIds(saved.id)
                        .getOne();

                    result.push(found);
                } else {
                    toRemoveMap.delete(annotation.id);
                    result.push(annotation);
                }
            }

            const toRemove = Array.from(toRemoveMap.values());
            await repo.remove(toRemove);

            return result;
        } catch (error) {
            console.log('addMention error',error )
            throw error;
        }
    }
}
