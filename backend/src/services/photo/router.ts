const express = require("express");
const router = express.Router();

import passport = require("passport");
import { PhotoController } from "./controller";
import { ViewController } from "../view/controller";
import { Middleware } from "../../utils/middleware";

router.post("/", passport.authenticate("jwt", {session: false}), PhotoController.addPhoto);

router.use("/:id/upload", passport.authenticate("jwt", {session: false}), PhotoController.uploadPhoto);

router.get("/all", passport.authenticate("jwt", {session: false}), PhotoController.getPhotos);

router.get("/:id", passport.authenticate("jwt", {session: false}), Middleware.checkInBlaklist("photo", "id"), PhotoController.getSinglePhoto);

router.get("/drafts", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, PhotoController.getPhotos);

router.put("/", passport.authenticate("jwt", {session: false}), PhotoController.updatePhoto);

router.delete("/:id", passport.authenticate("jwt", {session: false}), PhotoController.deletePhoto);

router.post("/:id/like", passport.authenticate("jwt", {session: false}), PhotoController.likePhoto);

router.get("/:id/like", passport.authenticate("jwt", {session: false}), PhotoController.getPhotoLikes);

router.post("/:id/view", passport.authenticate("jwt", {session: false}), ViewController.view("photo"));

router.post("/:id/comment", passport.authenticate("jwt", {session: false}), PhotoController.addComment);

router.put("/:id/comment", passport.authenticate("jwt", {session: false}), PhotoController.editComment);

router.get("/:id/comment", passport.authenticate("jwt", {session: false}), PhotoController.getComments);

router.get("/comment/:id/replies", passport.authenticate("jwt", {session: false}), PhotoController.getReplies);

router.post("/comment/:id/like", passport.authenticate("jwt", {session: false}), PhotoController.likeComment);

router.get("/comment/:id/like", passport.authenticate("jwt", {session: false}), PhotoController.getCommentLikes);

router.delete("/comment/:id", passport.authenticate("jwt", {session: false}), PhotoController.deleteComment);

router.post("/:photoId/annotation", passport.authenticate("jwt", {session: false}), PhotoController.addAnnotation);

router.get("/:id/annotation", passport.authenticate("jwt", {session: false}), PhotoController.getPhotoTags);



router.delete("/:photoId/annotation/annotationId", passport.authenticate("jwt", {session: false}), PhotoController.deleteAnnotation);

export = router;
