import getBlockRepository from "../../entities/repositories/block-repository";
import AccountRepository from "../../entities/repositories/account-repository";
import getFollowingRepository from "../../entities/repositories/following-repository";
import getFollowingRequestRepository from "../../entities/repositories/following-request-repository";

export class BlockController {
    static toBlock = async (req, res) => {
        try {
            const {
                user,
                params: { blockId },
            } = req;
            // unfollow user
            const targetAccount = await AccountRepository().getFullAccountInfo({ key: 'id', val: blockId })
            const target = targetAccount;
            const account = user.baseBusinessAccount || user.id;

            await getFollowingRepository()
                .unfollow(blockId, user.baseBusinessAccount || user.id);

            if (!(target.business && target.business.id) || (target.person && target.person.privacy !== 'public')) {
                await getFollowingRequestRepository().deleteFollowRequest(target, account)
            }

            const blacklist = await getBlockRepository().toBlocked(blockId, user.baseBusinessAccount || user.id);
            res.status(200).send({ blacklist });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static fromBlock = async (req, res) => {
        try {
            const {
                user,
                params: { blockId },
            } = req;
            const blacklist = await getBlockRepository().fromBlocked(blockId, user.baseBusinessAccount || user.id);
            res.status(200).send({ blacklist });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getBlacklist = async (req, res) => {
        try {
            const {
                user,
                query: { offset = 0, limit = 10 },
            } = req;
            const blocked = await getBlockRepository().getBlacklist(user.baseBusinessAccount || user.id, limit, offset);
            res.status(200).send({ blocked });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };
}
