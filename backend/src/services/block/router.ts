import { BlockController } from "./controller";
import passport = require("passport");
import { Router } from "express";

const router: Router = Router();

router.get("/blocked", passport.authenticate("jwt", {session: false}), BlockController.getBlacklist);

router.post("/blocked/:blockId", passport.authenticate("jwt", {session: false}), BlockController.toBlock);

router.delete("/blocked/:blockId", passport.authenticate("jwt", {session: false}), BlockController.fromBlock);

export default router;
