export const getNotificationsListRawQuery = (userId: string, limit: number, offset: number): string => {
  const rawQuery = `
    SELECT
      "notifications"."id" AS "notifications_id",
      "notifications"."action" AS "notifications_action",
      "notifications"."content_display_name" AS "notifications_content_display_name",
      "notifications"."content_type" AS "notifications_content_type",
      "notifications"."read" AS "notifications_read",
      "notifications"."created" AS "notifications_created",
      "owner"."id" AS "owner_id",
      "owner_person"."id" AS "owner_person_id",
      "owner_person"."thumb" AS "owner_person_thumb",
      "owner_person"."pageName" AS "owner_person_pageName",
      "owner_person"."fullName" AS "owner_person_fullName",
      "owner_business"."id" AS "owner_business_id",
      "owner_business"."thumb" AS "owner_business_thumb",
      "owner_business"."name" AS "owner_business_name",
      "owner_business"."pageName" AS "owner_business_pageName",
      "issuer"."id" AS "issuer_id",
      "issuer_person"."id" AS "issuer_person_id",
      "issuer_person"."thumb" AS "issuer_person_thumb",
      "issuer_person"."pageName" AS "issuer_person_pageName",
      "issuer_person"."fullName" AS "issuer_person_fullName",
      "issuer_business"."id" AS "issuer_business_id",
      "issuer_business"."thumb" AS "issuer_business_thumb",
      "issuer_business"."name" AS "issuer_business_name",
      "issuer_business"."pageName" AS "issuer_business_pageName",
      "photo"."id" AS "photo_id",
      "photo"."thumb" AS "photo_thumb",
      "video"."id" AS "video_id",
      "video"."cover" AS "video_cover",
      "article"."id" AS "article_id",
      "article"."thumb" AS "article_thumb",
      "text"."id" AS "text_id",
      "text"."text" AS "text_text",
      "product"."id" AS "product_id",
      "product"."type" AS "product_type",
      "product"."title" AS "product_title",
      "product_cover"."id" AS "product_cover_id",
      "product_cover"."photoUrl" AS "product_cover_photoUrl",
      "product_cover"."thumbUrl" AS "product_cover_thumbUrl",
      "product_cover"."order" AS "product_cover_order",
      "product_cover"."created" AS "product_cover_created",
      "product_cover"."deleted" AS "product_cover_deleted",
      "product_cover"."product_id" AS "product_cover_product_id",
      "service"."id" AS "service_id",
      "service"."title" AS "service_title",
      "service_photos"."id" AS "service_photos_id",
      "service_photos"."link" AS "service_photos_link",
      "service_photos"."thumb" AS "service_photos_thumb",
      "product_review"."id" AS "product_review_id",
      "product_review"."text" AS "product_review_text",
      "product_review_product"."id" AS "product_review_product_id",
      "product_review_product_cover"."id" AS "product_review_product_cover_id",
      "product_review_product_cover"."photoUrl" AS "product_review_product_cover_photoUrl",
      "product_review_product_cover"."thumbUrl" AS "product_review_product_cover_thumbUrl",
      "product_review_product_cover"."order" AS "product_review_product_cover_order",
      "product_review_product_cover"."created" AS "product_review_product_cover_created",
      "product_review_product_cover"."deleted" AS "product_review_product_cover_deleted",
      "product_review_product_cover"."product_id" AS "product_review_product_cover_product_id",
      "service_review"."id" AS "service_review_id",
      "service_review"."text" AS "service_review_text",
      "service_review_service"."id" AS "service_review_service_id",
      "service_review_service_photos"."id" AS "service_review_service_photos_id",
      "service_review_service_photos"."link" AS "service_review_service_photos_link",
      "service_review_service_photos"."thumb" AS "service_review_service_photos_thumb",
      "service_review_service_photos"."index" AS "service_review_service_photos_index",
      "service_review_service_photos"."created" AS "service_review_service_photos_created",
      "service_review_service_photos"."updated" AS "service_review_service_photos_updated",
      "service_review_service_photos"."deleted" AS "service_review_service_photos_deleted",
      "service_review_service_photos"."service_id" AS "service_review_service_photos_service_id",
      "service_review_service_user"."id" AS "service_review_service_user_id",
      "service_review_service_user_business"."id" AS "service_review_service_user_business_id",
      "service_review_service_user_business"."photo" AS "service_review_service_user_business_photo",
      "service_review_service_user_business"."thumb" AS "service_review_service_user_business_thumb",
      "service_review_service_user_business"."name" AS "service_review_service_user_business_name",
      "service_review_service_user_business"."pageName" AS "service_review_service_user_business_pageName",
      "service_review_service_user_person"."id" AS "service_review_service_user_person_id",
      "service_review_service_user_person"."photo" AS "service_review_service_user_person_photo",
      "service_review_service_user_person"."thumb" AS "service_review_service_user_person_thumb",
      "service_review_service_user_person"."pageName" AS "service_review_service_user_person_pageName",
      "service_review_service_user_person"."fullName" AS "service_review_service_user_person_fullName",
      "seller_review"."id" AS "seller_review_id",
      "seller_review"."text" AS "seller_review_text",
      "service_review_service_user"."businessId",
      "service_review_service_user"."personId"
    FROM
      "t_notifications" "notifications"
    LEFT JOIN "t_account" "owner" ON
      "owner"."id" = "notifications"."ownerId"
    LEFT JOIN "t_person" "owner_person" ON
      "owner_person"."id" = "owner"."personId"
    LEFT JOIN "t_business" "owner_business" ON
      "owner_business"."id" = "owner"."businessId"
    LEFT JOIN "t_account" "issuer" ON
      "issuer"."id" = "notifications"."issuerId"
    LEFT JOIN "t_person" "issuer_person" ON
      "issuer_person"."id" = "issuer"."personId"
    LEFT JOIN "t_business" "issuer_business" ON
      "issuer_business"."id" = "issuer"."businessId"
    LEFT JOIN "t_photo" "photo" ON
      "photo"."id" = "notifications"."photoId"
    LEFT JOIN "t_video" "video" ON
      "video"."id" = "notifications"."videoId"
    LEFT JOIN "t_article" "article" ON
      "article"."id" = "notifications"."articleId"
    LEFT JOIN "t_text" "text" ON
      "text"."id" = "notifications"."textId"
    LEFT JOIN "t_product" "product" ON
      "product"."id" = "notifications"."productId"
    LEFT JOIN "t_product_photo" "product_cover" ON
      "product_cover"."id" = "product"."cover_id"
    LEFT JOIN "t_service" "service" ON
      "service"."id" = "notifications"."serviceId"
    LEFT JOIN "t_service_photo" "service_photos" ON
      "service_photos"."service_id" = "service"."id"
    LEFT JOIN "t_product_review" "product_review" ON
      "product_review"."id" = "notifications"."productReviewId"
    LEFT JOIN "t_product" "product_review_product" ON
      "product_review_product"."id" = "product_review"."product_id"
    LEFT JOIN "t_product_photo" "product_review_product_cover" ON
      "product_review_product_cover"."id" = "product_review_product"."cover_id"
    LEFT JOIN "t_service_review" "service_review" ON
      "service_review"."id" = "notifications"."serviceReviewId"
    LEFT JOIN "t_service" "service_review_service" ON
      "service_review_service"."id" = "service_review"."service_id"
    LEFT JOIN "t_service_photo" "service_review_service_photos" ON
      "service_review_service_photos"."service_id" = "service_review_service"."id"
      AND ("service_review_service_photos"."index" = 0)
    LEFT JOIN "t_account" "service_review_service_user" ON
      "service_review_service_user"."id" = "service_review_service"."user_id"
    LEFT JOIN "t_business" "service_review_service_user_business" ON
      "service_review_service_user_business"."id" = "service_review_service_user"."businessId"
    LEFT JOIN "t_person" "service_review_service_user_person" ON
      "service_review_service_user_person"."id" = "service_review_service_user"."personId"
    LEFT JOIN "t_seller_review" "seller_review" ON
      "seller_review"."id" = "notifications"."sellerReviewId"
    WHERE
      notifications."ownerId" = ${userId}
    ORDER BY
      "notifications"."created" DESC
    limit ${limit}
    offset ${offset}
  `;
  return rawQuery
}
