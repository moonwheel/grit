import * as express from "express";
const router = express.Router();

import passport = require("passport");
import { NotificationController } from "./controller";

 // USE /user GET for getting a list of notifications options(settings)

router.put("/settings", passport.authenticate("jwt", {session: false}), NotificationController.updateNotificationOptionsForAccount);

router.get("/", passport.authenticate("jwt", {session: false}), NotificationController.getNotifications);
router.get("/unread", passport.authenticate("jwt", {session: false}), NotificationController.getUnreadNotificationsCount);

router.put("/", passport.authenticate("jwt", {session: false}), NotificationController.updateNotifications);

export = router;
