import { Product_variant } from './../../entities/product_variant';
import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { App } from "../../config/app.config";
import getAccountRepository from "../../entities/repositories/account-repository";
import { Notification, NotificationContentType, NotificationAction, NotificationContentDisplayName } from '../../entities/notification';
import { Account } from '../../entities/account';
import { ProductReview } from '../../entities/product_review';
import { Text } from '../../entities/text';
import { Photo } from '../../entities/photo';
import { Video } from '../../entities/video';
import { Article } from '../../entities/article';
import { ServiceReview } from '../../entities/service_review';
import { Product } from '../../entities/product';
import { Service } from '../../entities/service';
// import { Order } from '../../entities/order';
import { Booking } from '../../entities/booking';
import { Article_Comment_Like } from '../../entities/article_comment_like';
import { Article_Comment_Replies } from '../../entities/article_comment_reply';
import { Article_Comment } from '../../entities/article_comment';
import { Photo_Comment_Like } from '../../entities/photo_comment_like';
import { Photo_Comment } from '../../entities/photo_comment';
import { Text_Comment_Like } from '../../entities/text_comment_like';
import { Text_Comment_Replies } from '../../entities/text_comment_reply';
import { Text_Comment } from '../../entities/text_comment';
import { Video_Comment_Like } from '../../entities/video_comment_like';
import { Video_Comment_Replies } from '../../entities/video_comment_reply';
import { Video_Comment } from '../../entities/video_comment';
import { publish } from '../../utils/sse';
import { Photo_Comment_Replies } from '../../entities/photo_comment_reply';
import { PageParams } from '../../interfaces/global/page.interface';
import { getNotificationsListRawQuery } from './raw_queries';
import { sanitizeRawData } from '../../utils/helpers';

export type Content = Text |
  Photo |
  Photo_Comment |
  Photo_Comment_Like |
  Photo_Comment_Replies |
  Video_Comment |
  Video_Comment_Replies |
  Text |
  Text_Comment |
  Text_Comment_Like |
  Product |
  Service;

export class NotificationController {

  // USE /user GET


  static updateNotificationOptionsForAccount = async (req: Request, res: Response) => {
    try {
      const updatedAccount = await getAccountRepository().updateAccount({ key: 'id', val: req.user.id }, req.body)
      res.status(200).send(updatedAccount)
    } catch (error) {
      console.log('updateNotificationOptionsForAccount error', error)
      res.status(400).send(error)
    }
  }

  static getNotifications = async (
      req: Request,
      res: Response,
  ) => {
    const {
      query: {
        page = 1,
        limit = 15,
      }

    } = req;

    try {
      const limitNumb = Number(limit);
      const skip = (Number(page) - 1) * limitNumb;

      // const query = NotificationController.createQuery()
      //   .where(`notifications."ownerId" = :ownerId `, { ownerId: req.user.id })
      //   .skip(skip)
      //   .take(limitNumb)
      //   .orderBy(`notifications.created`, `DESC`)

      // console.log('======= getNotifications query.getSql()', query.getSql())

      // const userNotifications = await query
      //   .getManyAndCount();

      // res.status(200).send({ items: userNotifications[0], total: userNotifications[1]})

      const rawQuery = getNotificationsListRawQuery(req.user.id, limitNumb, skip)
      const userNotifications = await getRepository(Notification).query(rawQuery)
      res.status(200).send({items: sanitizeRawData(userNotifications), total: userNotifications.length, raw: true})
    } catch (error) {
      console.log('getNotifications error', error)
      res.status(400).send(error)
    }
  }

  static addNotification = async (
    action: NotificationAction,
    content_display_name: NotificationContentDisplayName,
    content_type: NotificationContentType,
    content: Content,
    issuer: Account,
    owner: Account,
    product_variant?: Product_variant
  ) => {

    const notificationRepo = getRepository(Notification);

    console.log('owner1', owner)
    console.log('issuer', issuer)

    try {
      if (+issuer.id === +owner.id) return;

      // if we have not detailed object of user - we should find him
      if (!('likes_notification' in owner)) {
        const key = 'id';
        const val = (owner as Account).id;
        owner = await getAccountRepository().getAccountNotificationSettings({ key, val });
      }

      // console.log('owner2', owner)


      if ( !owner.likes_notification && action === 'liked' ) return;
      if ( !owner.comments_notification && (action === 'commented' || action === 'replied') ) return
      if ( !owner.tags_notification && (action === 'tagged' || action === 'mentioned') ) return
      if ( !owner.followers_notification && (action === 'follows') ) return

      const newData = {
        action,
        content_display_name,
        content_type,
        read: false,
        created: new Date(),
        issuer,
        owner,
        [content_type]: content,
        product_variant
      }

      const newNotification = await notificationRepo.save(newData);
      newNotification.issuer = issuer

      publish({ userId: owner.id, type: 'new_notification', notification: newNotification });


    } catch (error) {
      console.log('addNotification error', error)
      throw new Error(error)
    }
  }

  // set Read status for the notifications
  static updateNotifications = async (req: Request, res: Response) => {
    const updatedNotifications = req.body.idArray;
    try {
      function generateWhereCondition(ids) {
        // if we have no array of ids - we want to mark all user notifications as read
        if (!ids || !ids.length) {
          return `
            owner = ${req.user.baseBusinessAccount || req.user.id}
            AND
            read IS false
          `;
        }

        return ids.map(id => {
          return `id = ${id}`
        }).join(' or ');
      }
      let query = getRepository(Notification)
        .createQueryBuilder()
        .update(Notification)
        .set({ read: true })
        .where(generateWhereCondition(updatedNotifications))
        .execute();

      res.status(200).send({ succeess: true })
    } catch (error) {
      console.log('updateNotifications error', error)
      throw new Error(error)
    }
  }

  static getUnreadNotificationsCount = async (req: Request, res: Response) => {
    try {
      let query = getRepository(Notification)
        .createQueryBuilder("notifications")
        .leftJoin('notifications.owner', "owner")
        .addSelect("owner.id")
        .where(`notifications."ownerId" = :ownerId `, { ownerId: req.user.id })
        .andWhere("notifications.read = false")

      const count = await query.getCount();

      res.status(200).send({ count })
    } catch (error) {
      res.status(400).send({ error: error.message });
    }
  }

  static createQuery() {
    let query = getRepository(Notification).createQueryBuilder("notifications")
      .leftJoin('notifications.owner', "owner")
      .addSelect("owner.id")
      .leftJoin('owner.person', "owner_person")
      .addSelect("owner_person.id")
      .addSelect("owner_person.thumb")
      .addSelect("owner_person.fullName")
      .addSelect("owner_person.pageName")
      .leftJoin("owner.business", "owner_business")
      .addSelect("owner_business.id")
      .addSelect("owner_business.thumb")
      .addSelect("owner_business.name")
      .addSelect("owner_business.pageName")

      .leftJoin('notifications.issuer', "issuer")
      .addSelect("issuer.id")
      .leftJoin('issuer.person', "issuer_person")
      .addSelect("issuer_person.id")
      .addSelect("issuer_person.thumb")
      .addSelect("issuer_person.fullName")
      .addSelect("issuer_person.pageName")
      .leftJoin("issuer.business", "issuer_business")
      .addSelect("issuer_business.id")
      .addSelect("issuer_business.thumb")
      .addSelect("issuer_business.name")
      .addSelect("issuer_business.pageName")

      .leftJoin('notifications.photo', "photo")
      .addSelect("photo.id")
      .addSelect("photo.thumb")

      .leftJoin('notifications.video', "video")
      .addSelect("video.id")
      .addSelect("video.cover")

      .leftJoin('notifications.article', "article")
      .addSelect("article.id")
      .addSelect("article.thumb")

      .leftJoin('notifications.text', "text")
      .addSelect("text.id")
      .addSelect("text.text")

      .leftJoin('notifications.product', "product")
      .addSelect("product.id")
      .addSelect("product.title")
      .addSelect("product.type")
      .leftJoinAndMapOne("product.cover","product.cover_", 'product_cover')
      // .addSelect(`product_cover."thumbUrl"`)
      // .leftJoinAndSelect(
      //   "product.photos",
      //   "product_photos",
      //   `
      //     product.type = 'singleProduct'
      //     AND
      //     product_photos.deleted IS NULL
      //     AND
      //     product_photos.order = '0'
      //   `
      // )
      // .addSelect(`"product_photos"."thumbUrl"`)

      // .leftJoin("notifications.product_variant", "product_variant", `"product_variant"."deleted" IS NULL`)
      // .addSelect(`product_variant.id`)
      // .leftJoinAndSelect("product_variant.photos", "prod_variants_to_photos", "prod_variants_to_photos.order = '0'")
      // .leftJoinAndSelect("prod_variants_to_photos.photo", "product_variants_photos")
      // .addSelect(
      //   `(
      //     CASE
      //       WHEN "product_photos"."thumbUrl" IS NOT NULL
      //       THEN "product_photos"."thumbUrl"
      //       ELSE "product_variants_photos"."thumbUrl"
      //     END
      //   )`
      //   // ,
      //   // "product.product_photos.thumbUrl"
      // )

      .leftJoin('notifications.service', "service")
      .addSelect("service.id")
      .addSelect("service.title")
      .leftJoin('service.photos', "service_photos")
      .addSelect("service_photos.id")
      .addSelect("service_photos.link")
      .addSelect("service_photos.thumb")


      .leftJoin('notifications.product_review', "product_review")
      .addSelect("product_review.id")
      .addSelect("product_review.text")
      .leftJoin("product_review.product", "product_review_product")
      .addSelect("product_review_product.id")
      .leftJoinAndMapOne("product_review_product.cover","product_review_product.cover_", 'product_review_product_cover')

      .leftJoin('notifications.service_review', "service_review")
      .addSelect("service_review.id")
      .addSelect("service_review.text")
      .leftJoin("service_review.service", "service_review_service")
      .addSelect("service_review_service.id")
      .leftJoinAndMapOne(
        "service_review_service.photoCover",
        "service_review_service.photos",
        "service_review_service_photos",
        "service_review_service_photos.index = 0"
      )
      .leftJoin("service_review_service.user", "service_review_service_user")
      .addSelect("service_review_service_user.id")
      .addSelect("service_review_service_user.business")
      .leftJoin("service_review_service_user.business", "service_review_service_user_business")
      .addSelect("service_review_service_user_business.id")
      .addSelect("service_review_service_user_business.photo")
      .addSelect("service_review_service_user_business.thumb")
      .addSelect("service_review_service_user_business.name")
      .addSelect("service_review_service_user_business.pageName")
      .addSelect("service_review_service_user.person")
      .leftJoin("service_review_service_user.person", "service_review_service_user_person")
      .addSelect("service_review_service_user_person.id")
      .addSelect("service_review_service_user_person.thumb")
      .addSelect("service_review_service_user_person.photo")
      .addSelect("service_review_service_user_person.fullName")
      .addSelect("service_review_service_user_person.pageName")


      .leftJoin('notifications.seller_review', "seller_review")
      .addSelect("seller_review.id")
      .addSelect("seller_review.text")

    return query;
  }
}
