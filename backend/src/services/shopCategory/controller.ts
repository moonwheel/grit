import { getRepository } from "typeorm";
import { Shop_category } from "../../entities/shop_category";

export class ShopCategoryController {
    static addShopCategory = async(req, res) => {
        const user = req.user;
        const shopCategoryRepo = getRepository(Shop_category);
        try {
            const category: Shop_category = req.body;
            category.user = user.baseBusinessAccount || user.id;
            const savedCategory = await shopCategoryRepo.save(category);
            console.log('saved', savedCategory)
            res.status(200).send(savedCategory);
        } catch (error) {
            console.log('error', error)
            console.log('error.message', error.message)
            if(/duplicate key value/.test(error.message)) {
                return res.status(400).send({error: 'Such shopcategory already exists'})
            }
            res.status(400).send({error: error.message});
        }
    };

    static getShopCategories = async(req, res) => {
        const user = req.user;
        const shopCategoryRepo = getRepository(Shop_category);
        try {
            const categories = await shopCategoryRepo.find({user: user.baseBusinessAccount || user.id, deleted: null});
            res.status(200).send(categories);
        } catch (error) {
            res.status(400).send({error: error.message});
        }

    };

    static updateShopCategory = async(req, res) => {
        const user = req.user;
        const shopCategoryRepo = getRepository(Shop_category);
        try {
            await shopCategoryRepo.update({id: req.body.id}, {name: req.body.name, parentId: req.body.parentId});
            const updatedCategory = await shopCategoryRepo.findOne(req.body.id);
            res.status(200).send(updatedCategory);
        } catch (error) {
            if(/duplicate key value/.test(error.message)) {
                return res.status(400).send({error: 'Such shopcategory already exists'})
            }
            res.status(400).send({error: error.message});
        }
    };

    static deleteShopCategory = async(req, res) => {
        const shopCategoryRepo = getRepository(Shop_category);
        try {
            await shopCategoryRepo.delete(req.params.id);
            res.status(200).send({status: "success"});
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };
}
