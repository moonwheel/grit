const express = require("express");
const router = express.Router();

import { ShopCategoryController } from "./controller";
import passport = require("passport");

router.post("/", passport.authenticate("jwt", {session: false}), ShopCategoryController.addShopCategory);
router.get("/all", passport.authenticate("jwt", {session: false}), ShopCategoryController.getShopCategories);
router.put("/", passport.authenticate("jwt", {session: false}), ShopCategoryController.updateShopCategory);
router.delete("/:id", passport.authenticate("jwt", {session: false}), ShopCategoryController.deleteShopCategory);

export = router;