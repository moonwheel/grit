const express = require("express");
const router = express.Router();

import passport = require("passport");
import { ReportController } from "./controller";

router.get("/all", passport.authenticate("jwt", {session: false}), ReportController.getAllReports);

router.post("/:id", passport.authenticate("jwt", {session: false}), ReportController.addReport);

router.get("/categories", passport.authenticate("jwt", {session: false}), ReportController.getReportCategories);

router.get("/types", passport.authenticate("jwt", {session: false}), ReportController.getReportTypes);

router.get("/:id", passport.authenticate("jwt", {session: false}), ReportController.getReportsByTargetId);

router.patch("/:id", passport.authenticate("jwt", {session: false}), ReportController.updateReport);

router.delete("/:id", passport.authenticate("jwt", {session: false}), ReportController.deleteReport);

export = router;
