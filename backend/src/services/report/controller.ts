import getReportRepository from "../../entities/repositories/report-repository";
import {Filter} from "../../interfaces/global/filter.interface";
import getRoleRepository from '../../entities/repositories/role-repository';
import { Response, Request } from "express";

export class ReportController {

    public static addReport = async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: reportData
            } = req;

            const report = await getReportRepository().addNewReport(id, user.baseBusinessAccount || user.id, reportData);

            res.status(200).send({ report });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    public static getReportsByTargetId = async (req, res) => {
        try {
            const {
                params: { id },
                query: {
                    page = 1,
                    limit = 15,
                    search = null,
                    filter = "date",
                    order = "desc"
                },
                user: {
                    role: roleId,
                    business: businessId
                }
            } = req;

            const role = await getRoleRepository().getRoleById(Number(roleId));

            if (role.type !== 'admin') {
                throw new Error('Only admin role can see the reports.')
            }

            if (!["type", "category", "status", "date"].includes(filter)) {
                throw new Error('Your filter is not allowed.');
            }

            const reports = await getReportRepository().getReportsByTargetId(
                id, {
                    page,
                    limit,
                    search,
                    filter,
                    order
                },
                    businessId
                );

            res.status(200).send({ count: reports[1], reports: reports[0] });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    public static getAllReports = async (req, res) => {
        try {
            const {
                params: { id },
                query: {
                    page = 1,
                    limit = 15,
                    search = null,
                    status = null,
                    type = null,
                    category = null,
                },
                user: {
                    role: roleId,
                    business: businessId
                }
            } = req;

            const role = await getRoleRepository().getRoleById(Number(roleId));

            // if (role && role.type !== 'admin') {
                // throw new Error('Only admin role can see the reports.')
            // }

            const reports = await getReportRepository()
                .getAllReports(id, {page, limit, search, status, type, category}, businessId);

            res.status(200).send({ count: reports[1], reports: reports[0] });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getReportCategories = async (req, res) => {
        try {
            const categories = await getReportRepository().getAllReportCategories();
            res.status(200).send(categories);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getReportTypes = async (req, res) => {
        try {
            const types = await getReportRepository().getReportTypes();
            res.status(200).send(types);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    public static updateReport = async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: reportData
            } = req;

            const report = await getReportRepository().updateReportById(id, user.baseBusinessAccount || user.id, reportData);

            res.status(200).send({ report });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    public static deleteReport = async (req: Request, res: Response) => {
        try {
            const {
                params: { id }
            } = req;

            await getReportRepository().deleteReportById(id);

            res.status(200).send({ message: "success" } );
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };
}
