const express = require("express");
const router = express.Router();

import passport = require("passport");
import { AppServices } from "./controller";
import reviewRouter from "../review/router";
import { Middleware } from "../../utils/middleware";

router.post("/", passport.authenticate("jwt", {session: false}), AppServices.addService)

router.use("/:id/upload", passport.authenticate("jwt", {session: false}), AppServices.uploadServicePhotos);

router.get("/all", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, AppServices.getServices);

router.get("/search", passport.authenticate("jwt", {session: false}), AppServices.searchBusinessService);

router.get("/:id", passport.authenticate("jwt", {session: false}),  Middleware.checkInBlaklist("service", "id"), AppServices.getServiceById);

router.put("/", passport.authenticate("jwt", {session: false}),  AppServices.updateService);

router.delete("/:id", passport.authenticate("jwt", {session: false}),  AppServices.deleteService);

// router.post("/:id/photo", passport.authenticate("jwt", {session: false}),  AppServices.addNewPhoto);
// router.patch("/:id/photo/reorder", passport.authenticate("jwt", {session: false}),  AppServices.reorderPhoto);
// router.delete("/:id/photo/:photo", passport.authenticate("jwt", {session: false}), AppServices.deletePhoto);
router.use(reviewRouter("service"));

export = router;
