import { Request, Response } from 'express';
import dateTime = require("date-time");
import uniqid = require("uniqid");
import * as fs from 'fs';
import { getRepository, Repository } from "typeorm";
import { Service } from "../../entities/service";
import { Service_schedule } from "../../entities/service_schedule";
import { ServicePhoto } from "../../entities/service_photo";
import { PhotoController } from "../photo/controller";
import { Filter } from "../../interfaces/global/filter.interface";
import getServiceRepository from "../../entities/repositories/service-repository";
import { ScheduleByDate } from "../../entities/schedule_by_date";
import { ScheduleByDays } from "../../entities/schedule_by_days";
import getReviewRepository from "../../entities/repositories/review-repository";
import { Helpers } from "../../utils/helpers";
import getHashtagRepository from '../../entities/repositories/hashtag-repository';
import { tusServer, TUS_EVENTS, decodeMetadata, UploadedFileMetadata } from '../../utils/tus-server';
import { ConvertedPhoto, MediaConverter } from '../../utils/media-converter';
import { publish } from '../../utils/sse';
import { App } from '../../config/app.config';
import MinioInstance from '../../utils/minio';
import getAccountRepository from '../../entities/repositories/account-repository';
import getMentionRepository from '../../entities/repositories/mention-repository';
import { Mention } from '../../entities/mention';

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata)

    if (metadata.entityType === 'service') {
        // console.log('upload service completed metadata', metadata)
        // console.log('upload service completed event', event)
        const tmpFileId = event.file.id
        await AppServices.onUploadEnd(tmpFileId, metadata)
    }
});

export class AppServices {
    static addService = async(req, res) => {
        try {
            const user = req.user;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const {
                service,
                schedule,
                photosLength = 0,
                byDate = [],
                byDays = [],
                hashtags = [],
                mentions = []
            }: {
                service: Service;
                schedule: Service_schedule;
                photosLength: number;
                byDate: Array<ScheduleByDate>;
                byDays: Array<ScheduleByDays>;
                hashtags: string[];
                mentions: Mention[];
            } = req.body;

            if (
                service.draft
                || (
                    service.title
                    && service.price
                    && service.quantity
                    && service.category
                    && service.performance
                    && service.performance !== "customer" ? service.address : true
                    && photosLength
                    && (
                        byDays.length && schedule.duration && schedule.break
                        || byDate.length
                    )
                )
            ) {

                if(byDate.length > 0 && byDays.length > 0) {
                    throw new Error('Bad request')
                } else if (byDate.length > 0) {
                    schedule.break = null;
                    schedule.duration = null
                }
                service.user = user.baseBusinessAccount || user.id;

                const savedService = await getServiceRepository().createService(service, photosLength, schedule, byDate, byDays, service.user );

                await getHashtagRepository().addHashtag("service", savedService.id, hashtags || []);

                savedService.mentions = await getMentionRepository().addMention("service", savedService, mentions, "service", user);

                res.status(200).send(savedService);
            } else {
                res.status(400).send({message: "Invalid data"});
            }
        } catch (error) {
            console.log('error while service creating', error)
            res.status(400).send({error: error.message});
        }
    };

    static uploadServicePhotos = async (req: Request, res: Response) => {
        if(req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }
        return tusServer.handle(req, res);
    }

    static onUploadEnd = async (tmpFileId: number, metadata: UploadedFileMetadata) => {
        let newFilePath: string;
        let outputFiles: ConvertedPhoto;

        try {
            const servicePhotoId = +metadata['grit-file-entity-id'];
            const { id, type, userId } = metadata;
            const photoRepo = getRepository(ServicePhoto);
            const photo = await photoRepo.findOne({ id: servicePhotoId });

            // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;

            const uniqueFileName = uniqid("photo-");
            const spliType = (type as string).split('/');
            const fileFormat = spliType[1];

            const fullFileName = `${uniqueFileName}.${fileFormat}`;

            newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

            fs.renameSync(oldFilePath, newFilePath);

            MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                .then(async () => {
                    photo.link = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                    photo.thumb = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`

                    await photoRepo.save(photo);

                    const serviceRepo = getRepository(Service);
                    const entity = await getServiceRepository().getOneService(id, null, userId);

                    publish({ userId, type: 'file_processing_complete', entityType: 'service', entity });
                })

        } catch (error) {
            console.error('Error while service photo uploading' ,error);
        }
    }

    static getServices = async(req, res) => {
        try {
            const {
                user,
                query: {
                    page = 1,
                    filter = "date",
                    limit = 15,
                    order = "desc",
                    draft = false,
                    userId = null,
                    search = null
                }
            } = req;

            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;
            const filterObj = AppServices.getFilterParams(filter, order);

            const id = userId || user.baseBusinessAccount || user.id;
            let query = AppServices.createServiceListQuery(getRepository(Service), id, user.baseBusinessAccount || user.id)
                .andWhere("service.draft = :draft", {draft})
                .andWhere("service.active = :active", {active: true})
                .skip(skip)
                .take(limitNumb)
                .orderBy(filterObj.by, filterObj.order)

            if (search) {
                query = query
                    .andWhere(`
                        (LOWER(service.title) SIMILAR TO '%${search.toLowerCase()}%')
                    `)
            }

            // console.log('query.getSql()', query.getSql())

            const data = await query.getManyAndCount();

            // TODO: uncomment this if getMany() is note enough
            // const services: any = await Helpers.beautifyRawData(rawData,{
            //     name: "service",
            //     aggregate: [
            //         "photos",
            //     ]
            // });
            res.status(200).send({ items: data[0], total: data[1] });
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getFilterParams = (filterBy: string, order: string) => {
        const filter: Filter = {
            by: "service.created",
            order: "DESC",
        };
        if(filterBy === "reviews") filter.by = "service_reviews";
        if(filterBy === "price") filter.by ="service.price";
        // if(filterBy === "draft") filter.by ="service.draft";

        if(order.toLowerCase() === "asc" ) filter.order = "ASC";

        return filter;
    };

    static searchBusinessService = async(req, res) => {
        try {
            const {
                user,
                query: {
                    page = 1,
                    limit = 15,
                    value = "",
                }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            if(!user.business) throw Error("Accessible only for a business account");
            if(!value.trim()) throw Error("No value");
            const services = await getRepository(Service)
                .createQueryBuilder("service")
                .leftJoinAndSelect("service.schedule", "schedule")
                .leftJoinAndSelect("schedule.byDate", "byDate")
                .leftJoinAndSelect("service.photos", "photos")
                .leftJoinAndSelect("service.user", "user")
                .leftJoinAndSelect("service.address", "address")
                .leftJoin("user.business", "business")
                .addSelect("business.id")
                .addSelect("business.photo")
                .addSelect("business.name")
                .addSelect("business.pageName")
                .leftJoin("user.person", "person")
                .addSelect("person.id")
                .addSelect("person.photo")
                .addSelect("person.fullName")
                .addSelect("person.pageName")
                .andWhere("service.user_id = :user", {user: user.baseBusinessAccount || user.id})
                .andWhere("service.deleted IS NULL")
                .andWhere(`
                    (LOWER(service.title) SIMILAR TO '%${value}%'
                    OR LOWER(service.category) SIMILAR TO '%${value}%')
                `)
                .skip(skip)
                .take(limitNumb)
                .getMany();

            res.status(200).send(services);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getServiceById = async(req, res) => {
        try {
            const {
                params: { id },
                user,
            } = req;

            // const test = await getServiceRepository().findOne(id)
            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if (!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const service = await getServiceRepository().getOneService(id, null, user.baseBusinessAccount || user.id);

            const rating = await getReviewRepository().getRating('service', id);

            // console.log('service', service)
            res.status(200).send({
                ...service,
                rating,
            });
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    // static checkOwnership = async(req, res, next) => {
    //     try {
    //         const {
    //             user,
    //         } = req;

    //         const id = req.params.id || req.body.id
    //         const service = await getServiceRepository().getOneService(id, user.baseBusinessAccount || user.id);

    //         if (service) {
    //             req.service = service;
    //             next();
    //         } else {
    //             res.status(400).send({ message: "User does not own the service" });
    //         }

    //     } catch(error) {
    //         res.status(400).send({error: error.message});
    //     }
    // };

    static updateService = async(req, res) => {
        try {
            const {
                user,
                body: {
                    id,
                    service,
                    schedule,
                    byDate,
                    byDays,
                    photos
                }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const serviceTmp = await getServiceRepository().updateService(id, service, schedule, byDate, byDays, photos, user.baseBusinessAccount || user.id);
            await getMentionRepository().addMention("service", serviceTmp, req.body.mentions, "service", user);

            if (service.draft != undefined && !service.draft) await AppServices.releaseService(id,  user.baseBusinessAccount || user.id, res);
            const updated = await getServiceRepository().getOneService(id, user.baseBusinessAccount || user.id, user.baseBusinessAccount || user.id);
            await getHashtagRepository().updateHashtagsByPostId("service", updated.id, req.body.hashtags);

            res.status(200).send(updated);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static releaseService = async(id, user, res) => {
        try {
            const serviceRepo = getServiceRepository();
            const service = await serviceRepo.getOneService(id, user, user);

            if (
                service
                && service.title
                && service.price
                && service.category
                && service.quantity
                && service.performance
                && service.performance !== "customer" ? service.address : true
                && service.schedule
                && (service.schedule.byDate || service.schedule.byDays)
            ) {
                await serviceRepo.updateService(id, {draft: false}, {}, null, null, [], user);
                await serviceRepo.generateServiceScheduleIntervals(id, user);
            } else {
                await serviceRepo.updateService(id, {draft: true}, {}, null, null, [], user);
            }
            return;
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteService = async(req, res) => {
        try {
            const { id } = req.params;

            if(req.user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            await getServiceRepository().deleteService(id);
            res.status(200).send({status:"success"});

        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    // static addNewPhoto = async(req, res) => {
    //     try {
    //         const {
    //             params: { id },
    //             body: { photos: photosArr },
    //         } = req;

    //         const servicePhotoRepo = getRepository(ServicePhoto);

    //         const links = [];

    //         for (let i=0; i < photosArr.length; i++ ) {
    //             try {
    //                 const link = await PhotoController.base64ToFile(photosArr[i]);
    //                 links.push(link);
    //             } catch (error) {
    //                 console.log(error);
    //             }
    //         }

    //         const count = await servicePhotoRepo
    //             .createQueryBuilder("photo")
    //             .where("photo.service_id = :id", {id})
    //             .getCount();

    //         for (let index = 0; index < links.length; index++) {

    //             const photo = new ServicePhoto();
    //             photo.link = links[index];
    //             photo.index = count + index;
    //             photo.service_ = id;

    //             await servicePhotoRepo.save(photo);
    //         }

    //         res.status(200).send({status:"success"});
    //     } catch (error) {
    //         res.status(400).send({error: error.message});
    //     }
    // };

    // static deletePhoto = async(req, res) => {
    //     try {
    //         const {
    //             user,
    //             params: { id, photo }
    //         } = req;

    //         if(user.business) {
    //             const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
    //             if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
    //         }

    //         await getRepository(ServicePhoto).delete(photo);

    //         res.status(200).send({status:"success"});
    //     } catch (error) {
    //         res.status(400).send({error: error.message});
    //     }
    // };

    private static createServiceListQuery(repo: Repository<Service>, userId: number, currentUser: number) {
        const query = repo.createQueryBuilder("service")
            .leftJoinAndMapOne(`service.bookmarked`, `service.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${currentUser}`)
            .leftJoin("service.reviews", "reviews")
            .addSelect("COUNT(reviews.id)", "service_reviews")
            .leftJoinAndSelect("service.schedule", "service_schedule")
            .leftJoinAndSelect("service_schedule.byDate", "service_schedule_bydate")
            .leftJoinAndMapOne("service.photoCover", "service.photos", "service_photos", "service_photos.index = 0")
            .leftJoinAndSelect("service.address", "service_address")
            .leftJoin("service.user", "service_user")
            .addSelect("service_user.id")
            .addSelect("service_user.business")
            .leftJoin("service_user.business", "service_user_business")
            .addSelect("service_user_business.id")
            .addSelect("service_user_business.photo")
            .addSelect("service_user_business.thumb")
            .addSelect("service_user_business.name")
            .addSelect("service_user_business.pageName")
            .addSelect("service_user.person")
            .leftJoin("service_user.person", "service_user_person")
            .addSelect("service_user_person.id")
            .addSelect("service_user_person.thumb")
            .addSelect("service_user_person.photo")
            .addSelect("service_user_person.fullName")
            .addSelect("service_user_person.pageName")
            .where("service.user_id = :userId", { userId })
            .andWhere("service.deleted IS NULL")
            .groupBy(`
                service.*,
                bookmarks.*,
                service_schedule.*,
                service_schedule_bydate.*,
                service_photos.*,
                service_address.*,
                service_user.*,
                service_user_business.*,
                service_user_person.*
            `)

        return query;
    }
}
