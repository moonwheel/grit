const express = require("express");
const router = express.Router();

import { BusinessController } from "./controller";
import passport = require("passport");

router.post("/", passport.authenticate("jwt", {session: false}), BusinessController.addBusiness);

router.get("/", passport.authenticate("jwt", {session: false}), BusinessController.getAllBusinesses);

router.get("/categories", BusinessController.getCategories);

router.get("/:id", passport.authenticate("jwt", {session: false}), BusinessController.getBusiness);

router.patch("/:id", passport.authenticate("jwt", {session: false}), BusinessController.updateBusiness);

router.delete("/:id", passport.authenticate("jwt", {session: false}), BusinessController.removeBusiness);
router.get("/reactivate/:token", passport.authenticate("jwt", {session: false}), BusinessController.reactivateBusiness);



router.get("/:id/users", passport.authenticate("jwt", {session: false}), BusinessController.getUsersForBusiness);
router.post("/:id/users", passport.authenticate("jwt", {session: false}), BusinessController.addManyUsersToBusiness);
router.put("/:id/users/:accountId", passport.authenticate("jwt", {session: false}), BusinessController.updateUserRole);
router.delete("/:id/users/:accountId", passport.authenticate("jwt", {session: false}), BusinessController.deleteUserFromBusiness);

router.delete("/:id/leave", passport.authenticate("jwt", {session: false}), BusinessController.leaveBusiness);

router.get("/:id/users/search-possible-members", passport.authenticate("jwt", {session: false}), BusinessController.searchPossibleMembers);

export = router;
