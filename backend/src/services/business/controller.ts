import { App } from './../../config/app.config';
import mangoPay from "../../utils/mango-payment";

const dateTime = require("date-time");

import "reflect-metadata";
import { isEmpty } from "lodash";
import { getRepository, IsNull, Not } from "typeorm";
import { groupBy } from "lodash";
import { Business } from "../../entities/business";
import { Role } from "../../entities/role";
import { Account } from "../../entities/account";
import businessValidation from "../../validationSchemes/business";
import { PhotoController } from "../photo/controller";
import { Category } from "../../entities/category";
import { Hour } from "../../entities/hour";
import getAccountRepository from "../../entities/repositories/account-repository";
import getAddressRepository from "../../entities/repositories/address-repository";
import { Person } from "../../entities/person";
import { UserController } from "../user/controller";
import { AccountToken } from "../../interfaces/global/account-token.interface";
import { Request, Response } from "express";
import { CompletelyRemoveUser } from '../../jobs/remove-user';
import uniqid = require("uniqid");

interface UpdateParams {
    role?: string;
    deleted?: boolean
}

export class BusinessController {
    static getBusiness = async (req, res) => {
        try {
            const {
                user,
                params: { id }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }


            const business = await getRepository(Business).findOne(id, {
                relations: ["hours", "category", "addresses"],
                where: { deleted: null }
            });
            const {language} = await getRepository(Person).findOne(user.person);
            res.status(200).send({...business, language});
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getAllBusinesses = async (req, res) => {
        try {
            const {
                user,
            } = req;
            // This was rewritten from .find() to .createQueryBuilder() because
            // we need proper condition to filter accounts which are removed from business by admin
            // but not removed by Delete Page operation.
            // TODO: Rewrite this in better way if needed
            const businesses = await getAccountRepository()
                .createQueryBuilder('account')
                .leftJoinAndSelect('account.role', 'account_role')
                .leftJoinAndSelect('account.business', 'account_business')
                .leftJoinAndSelect('account.person', 'account_person')
                .loadRelationCountAndMap('account.followersCount', 'account.followers')
                .loadRelationCountAndMap('account.followingsCount', 'account.followings')

                .where(`(
                    (
                        account.person = ${user.person}
                        AND account.deleted IS NULL
                    )
                    OR
                    (
                        account.person = ${user.person}
                        AND account.deleted IS NOT NULL
                        AND account_business.deleted IS NOT NULL
                    )
                )`)
                .getMany();
            res.status(200).send(businesses);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getCategories = async (req, res) => {
        try {
            const categories = await getRepository(Category).find();
            res.status(200).send(groupBy(categories, "type"));
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static addBusiness = async (req, res) => {
        try {
            const {
                user,
                body: {
                    business,
                    business: { address }
                },
            } = req;
            const creatorRole = await getRepository(Role).findOne({ where: { type: "admin" }});
            const businessCount = await getAccountRepository().count({
                where: {
                    person: user.person,
                    role: creatorRole.id,
                    deleted: null
                }
            });

            delete business.address;

            const isValid = await businessValidation(business);

            if (isValid.error) {
                throw isValid.error;
            } else if (businessCount >= App.MAX_BUSINESS_ACCOUNTS_PER_USER) {
                throw Error(`Reached the limit. User can create only ${App.MAX_BUSINESS_ACCOUNTS_PER_USER} business`);
            } else if (!address) {
                throw Error("Address is required");
            }

            address.type = 'businessAddress';

            const chekResult = await getAccountRepository().checkUniquePageName(business.name);
            business.pageName = chekResult.pageName;

            const { person } = await getRepository(Account).findOne(user.id)

            const { data: { Id: mangoUserId } } = await mangoPay.createNaturalUser({
                email: person.email,
                lastName: business.pageName,
                firstName: business.name,
                nationality: "FR",
                countryOfResidence: "FR"
            });

            const { data: { Id: walletId } } = await mangoPay.createWallet(mangoUserId, "EUR");

            const { data: { Id: bankAliasId } } = await mangoPay.createBankAlias(mangoUserId, business.name, walletId);
            business.wallet = Number(walletId);
            business.mangoUserId = Number(mangoUserId);
            business.bankAliasId = Number(bankAliasId);

            const savedBusiness = await getRepository(Business).save(business);

            const account = await getAccountRepository()
                .createBusinessAccount(user.person, creatorRole, savedBusiness.id, null)
                .catch(async (error) => {
                    await getRepository(Business).delete(savedBusiness.id);
                    throw error;
                });

            const newAddress = await getAddressRepository().addAddress({ address, account }).catch(async (error) => {
                await getRepository(Business).delete(savedBusiness.id);
                await getAccountRepository().delete(account.id);
                throw error;
            });

            await getRepository(Business).update(savedBusiness.id, { address: newAddress });

            res.status(200).send({
                status: "success",
                account: account.id,
                business: savedBusiness.id
            });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static removeBusiness = async (req, res) => {
        try {
            const {
                user,
                params: { id }
            } = req;

            console.log('user', user)

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                console.log('permissions', permissions)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }

            const deactivatedTime = dateTime()

            await getRepository(Business).update({ id }, { deleted: deactivatedTime,  reactivationtoken: uniqid() });
            const account = await getAccountRepository().getAccountForToken({ person: user.person, business: id });
            await getAccountRepository().update(account.id, { deleted: deactivatedTime });
            const updateTokenAccount: AccountToken = { ...user, business: null, id: null };
            const newToken = await UserController.createVerifyToken(updateTokenAccount);

            CompletelyRemoveUser.setJobForBusiness(user.business, deactivatedTime)
            res.status(200).send({ status: "success", token: newToken });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static reactivateBusiness = async (req, res) => {
        try {
            const {
                user,
                params: { token }
            } = req;

            console.log('user', user)

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                console.log('permissions', permissions)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }


            if (!token) throw new Error('reactivation token not found')
            const businessRepo = getRepository(Business);

            const foundBusiness = await businessRepo.findOne({reactivationtoken: token})
            const account = await getRepository(Account).findOne({ person: user.person, business: foundBusiness });
            console.log('foundBusiness', foundBusiness)
            console.log('account', account)
            if (!foundBusiness) throw new Error('reactivation token not found')
            // return
            const updated = await businessRepo.update({reactivationtoken: token}, {deleted: null, reactivationtoken: null})
            await getAccountRepository().update(account.id, { deleted: null });
            CompletelyRemoveUser.cancelJobByBusinessId(foundBusiness.id)
            // console.log('updated', updated)
            res.status(200).send({ status: "success" });

        } catch (error) {
            res.status(400).send({message: error.message})
        }
    }

    static updateBusiness = async (req, res) => {
        try {
            const {
                user,
                body: {
                    business,
                    language
                },
                params: { id }
            } = req;

            const personRepo = getRepository(Person)

            if(language){
                const personId = user.person;
                await personRepo.update({id: personId}, {
                    language: language,
                    updated: new Date()
                });
            }


            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }

            if(business.pageName) {
                const checkResult = await getAccountRepository().checkUniquePageName(business.pageName);
                if (!checkResult.unique) throw Error("Page with this name already exist");
                business.pageName = checkResult.pageName;
            }
            if(business.photo) {
                const { photo, thumb } = await PhotoController.base64ToFile(business.photo, 750, 250);
                business.photo = photo;
                business.thumb = thumb;
            }
            if(business.hours) {
                await BusinessController.createOrUpdateHours(business.hours, id);
                delete business.hours;
            }

            if(business.address) {
                await getAddressRepository().update(business.address.id, business.address);
                delete business.address;
            }

            if (!isEmpty(business)) {
                await getRepository(Business).update(id, business);
            }

            res.status(200).send({ status: "success" });
        } catch (error) {
            console.log('error', error);
            res.status(400).send({ error: error.message });
        }
    };

    static createOrUpdateHours = async (hours, businessID) => {
        try {
            const hoursRepo = getRepository(Hour);
            for (let index = 0; index < hours.length; index++) {
                const period = hours[index];
                const newHours = new Hour();

                if (Object.keys(period).length === 1 && period.id) {
                    await hoursRepo.delete(period.id).catch();
                    continue;
                }

                if (period.id) newHours.id = period.id;
                newHours.business = businessID;
                newHours.day = period.day;
                newHours.from = period.from;
                newHours.to = period.to;

                const saved = await hoursRepo.save(newHours);
            }
        } catch (error) {
            throw error;
        }
    };

    // static checkPermission = async (userID, businessID, roles: Array<string> = []) => {
    //     try {
    //         const userBusinesRepo = getRepository(Account);
    //         const relationship = await userBusinesRepo.findOne({
    //             relations: ["role"],
    //             where: {
    //                 user: userID,
    //                 business: businessID
    //             }
    //         });
    //         if (relationship) {
    //             return roles.length
    //                 ? !!(roles.indexOf(relationship.role.type) + 1)
    //                 : true;
    //         } else {
    //             return false;
    //         }
    //     } catch (error) {
    //         throw error;
    //     }
    // };

    static getUsersForBusiness = async (req, res) => {
        try {
            const businessId = req.params.id;
            const { search } = req.query;
            const { user } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const query = getRepository(Account)
                .createQueryBuilder('account')
                .leftJoinAndSelect('account.role', 'role')
                .leftJoinAndSelect("account.person", "person")
                .where("account.business = :businessId", { businessId })
                .orderBy('person.fullName', 'ASC')
                .andWhere("account.deleted is NULL", { businessId })

            if (search) {
                query.andWhere(`(
                    LOWER(person.pageName) SIMILAR TO '%${search}%'
                    OR LOWER(person.fullName) SIMILAR TO '%${search}%'
                )`)
            }

            const usersInBusiness = await query.getManyAndCount();

            res.status(200).send({ items: usersInBusiness[0], total: usersInBusiness[1] })

        } catch (error) {
            console.log('getUsersForBusiness error', error)
            res.status(400).send({ error: error.message });
        }

    }

    // static addUserToBusiness = async (req, res) => {
    //     try {
    //         const businessId = req.params.id;
    //         const { email, role } = req.body;
    //         const { user } = req;

    //         if(!businessId || !email || !role) return res.status(400).send('Bad Request')

    //         // CHECK THAT YOU'RE AN ADMIN OF THE BUSINESS
    //         if(user.business) {
    //             const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
    //             if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
    //         }

    //         // CHECK IF ENTERED EMAIL BELONGS TO EXIST PERSON
    //         const foundPerson = await getRepository(Person).findOne({
    //             where: {
    //                 email
    //             }
    //         })
    //         if(!foundPerson) return res.status(400).send('This email doesn\'t exist')

    //         // CHECK IF ENTERED BUSINESS EXISTS
    //         const foundBusiness = await getRepository(Business).findOne({
    //             where: {
    //                 id: businessId
    //             }
    //         })
    //         if(!foundBusiness) return res.status(400).send('This business doesn\'t exist')


    //         // CHECK THAT USER IS NOT IN THE BUSINESS
    //         const userAlreadyInBusiness = await getRepository(Account)
    //             .createQueryBuilder('account')
    //             .leftJoinAndSelect('account.role', 'role')
    //             .leftJoinAndSelect("account.person", "person")
    //             .where("account.business = :businessId AND person.email = :email", { businessId, email })
    //             addManyUsersToBusiness       console.log('foundPerson', foundPerson)
    //         console.log('foundBusiness', foundBusiness)
    //         console.log('foundRole', foundRole)

    //         const baseBusinessAccount = await getRepository(Account)
    //         .createQueryBuilder('account')
    //         .where("account.business = :businessId", { businessId })
    //         .orderBy('account.created', 'ASC')
    //         .getOne();
    //         addManyUsersToBusiness
    //         await getAccountRepository().createBusinessAccount(foundPerson, foundRole, foundBusiness, baseBusinessAccount)

    //         res.status(200).send({status: "success"})

    //     } catch (error) {
    //         console.log('addUserToBusiness error', error);
    //         res.status(400).send({ error: error.message });
    //     }
    // }

    static addManyUsersToBusiness = async (req, res) => {
        try {
            const businessId = req.params.id;
            const usersToAdd = req.body as { email: string, role: string }[];
            const { user } = req;

            const possibleRoles = new Set(usersToAdd.map(item => item.role));
            const userEmailToRoleMap = new Map<string, string>();

            for (const userToAdd of usersToAdd) {
                userEmailToRoleMap.set(userToAdd.email, userToAdd.role);
            }

            const usersAreValid = usersToAdd.every(item => item.email && item.role);
            if (!businessId || !usersToAdd || !usersToAdd.length || !usersAreValid) return res.status(400).send({ error: 'Bad Request' });

            // CHECK THAT YOU'RE AN ADMIN OF THE BUSINESS
            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }

            // CHECK IF ENTERED EMAIL BELONGS TO EXIST PERSON
            const foundPersons = await getRepository(Person).find({
                where: usersToAdd.map(({ email }) => ({ email }))
            })
            if (foundPersons.length !== usersToAdd.length) return res.status(400).send({ error: 'Some of users doesn\'t exist' })

            // CHECK IF ANY USER HAS MAXIMUM NUMBER OF BUSINESS ACCOUNTS
            await Promise.all(foundPersons.map(async (person) => {
                const count = await getRepository(Account).count({ person, business: Not(IsNull())});
                if (count >= App.MAX_BUSINESS_ACCOUNTS_PER_USER) {
                    throw new Error(`User with email ${person.email} already has maximum number of business accounts (${App.MAX_BUSINESS_ACCOUNTS_PER_USER})`)

                }
            }));

            // CHECK IF ENTERED BUSINESS EXISTS
            const foundBusiness = await getRepository(Business).findOne({
                where: {
                    id: businessId
                }
            })
            if (!foundBusiness) return res.status(400).send({ error: 'This business doesn\'t exist' })

            // CHECK THAT USER IS NOT IN THE BUSINESS
            const query = getRepository(Account)
                .createQueryBuilder('account')
                .select('account.id')
                // .leftJoinAndSelect('account.role', 'role')
                .leftJoin("account.person", "person")
                .leftJoin("account.business", "business")
                .where("account.business = :businessId", { businessId })
                .andWhere("account.deleted is NULL")
                .andWhere(`
                    (${usersToAdd.map(item => `person.email = '${item.email}'`).join(' OR ')})
                `);


            // console.log('========= countOfUsersAlreadyInBusiness query.getSql()', query.getSql())

            const countOfUsersAlreadyInBusiness = await query.getCount();

            if (countOfUsersAlreadyInBusiness > 0) return res.status(400).send({ error: 'Some of users are already in the business' })

            const foundRoles = await getRepository(Role).find({
                where: Array.from(possibleRoles).map(type => ({ type }))
            });

            if (foundRoles.length !== possibleRoles.size) return res.status(400).send({ error: 'Some of given roles doesn\'t exist' })



            const baseBusinessAccount = await getRepository(Account)
                .createQueryBuilder('account')
                .where("account.business = :businessId", { businessId })
                .orderBy('account.created', 'ASC')
                .getOne();

            const promises = usersToAdd.map(userToAdd => {
                const foundPerson = foundPersons.find(person => person.email === userToAdd.email);
                const foundRole = foundRoles.find(role => role.type === userToAdd.role);

                return getAccountRepository().createBusinessAccount(foundPerson, foundRole, foundBusiness, baseBusinessAccount);
            });

            await Promise.all(promises)

            res.status(200).send({ message: 'OK' })
        } catch (error) {
            console.log('addUserToBusiness error', error);
            res.status(400).send({ error: error.message });
        }
    }

    static leaveBusiness = async (req, res) => {
        try {
            const {
                params: {
                    id: businessId
                },
                user
            } = req;

            if(!businessId) return res.status(400).send('Bad Request')

            // CHECK THAT YOU'RE AN USER OF THE BUSINESS
            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            await BusinessController.performUpdatingBusinessAcc(user.id, businessId, {deleted: true})

            res.status(200).send({status: "success"})
        } catch (error) {
            console.log('leave business error', error);
            res.status(400).send({ error: error.message });
        }
    }


    static updateUserRole = async (req, res) => {
        try {

            const businessId = req.params.id;
            const accountId = req.params.accountId;
            const { role } = req.body;
            const { user } = req;

            if(!businessId || !accountId || !role) return res.status(400).send('Bad Request')

            // CHECK THAT YOU'RE AN ADMIN OF THE BUSINESS
            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
            }

            await BusinessController.performUpdatingBusinessAcc(accountId, businessId, {role})

            res.status(200).send({status: "success"})
        } catch (error) {
            console.log('updateUserRole error', error);
            res.status(400).send({ error: error.message });
        }
    }

    static deleteUserFromBusiness = async (req, res) => {
        try {

            const businessId = req.params.id;
            const accountId = req.params.accountId;
            const { user } = req;

            if(!businessId || !accountId) return res.status(400).send('Bad Request')

            // CHECK THAT YOU'RE AN ADMIN OF THE BUSINESS
            if (user.business) {
                // check permissions only if user is trying to remove other user.
                // Allow action if he is removing himself
                if (+user.id !== +accountId) {
                    const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                    if(!permissions.canManage) return res.status(403).send({error: 'Forbidden'});
                }
            }

            await BusinessController.performUpdatingBusinessAcc(accountId, businessId, {deleted: true})

            if (+req.params.accountId !== +req.user.id) {
                // if we removed other account - just send success
                res.status(200).send({status: "success"})
            } else {
                // if we removed own account - we need to generate new tokens,
                // because old ones will have no power anymore
                const account = await getAccountRepository().getAccountForToken({
                    person: user.person,
                    business: null,
                });

                const token = UserController.createVerifyToken(account);
                const refreshToken = UserController.createVerifyToken(account.id, "refresh");

                res.status(200).send({status: "success", token, refreshToken});
            }

        } catch (error) {
            console.log('updateUserRole error', error);
            res.status(400).send({ error: error.message });
        }
    }



    static performUpdatingBusinessAcc = async (accountId: number, businessId: number, updateParams: UpdateParams = {}) => {
        const foundAccount = await getRepository(Account)
            .createQueryBuilder('account')
            .leftJoinAndSelect('account.role', 'role')
            .leftJoinAndSelect("account.business", "business")
            .where("account.id = :accountId", { accountId })
            .getOne();

        if(!foundAccount || !(foundAccount.business && Number(foundAccount.business.id) === Number(businessId))) throw new Error('Bad Request')

        const adminsInTheBusiness = await getRepository(Account)
            .createQueryBuilder('account')
            .leftJoinAndSelect('account.role', 'role')
            .where("account.business = :businessId AND role.type = 'admin'", { businessId })
            .andWhere("account.deleted IS NULL")
            .getMany();

        let updateField;
        if(updateParams.role) {
            // UPDATING USER'S ROLE
            const foundRole = await getRepository(Role).findOne({where: {type: updateParams.role}})
            if(!foundRole) throw new Error('This role doesn\'t exist')

            if(
                (foundAccount.role && foundAccount.role.type === 'admin')
                && (adminsInTheBusiness && adminsInTheBusiness.length < 2)
                && updateParams.role !== 'admin'
            ) {
                throw new Error('You can not devote the last admin in the business')
            }

            updateField = {
                role: foundRole
            };
        } else {
            if(
                (foundAccount.role && foundAccount.role.type === 'admin')
                && (adminsInTheBusiness && adminsInTheBusiness.length < 2)
            ) {
                throw new Error('You are the only admin for some businesses. You have to choose someone else as admin for those businesses before your personal page can be deleted.')
            }
            if (updateParams.deleted) {
                 // DELETING ACCOUNT (SET deleted field)
                updateField = {
                    deleted: dateTime()
                }
            } else {
                // JUST checked that you're not last admin (for deleting personal accounts)
                updateField = {
                    updated: dateTime()
                }
            }
        }

        return await getRepository(Account).update({
            id: foundAccount.id
        }, updateField);

    }

    static searchPossibleMembers = async (req: Request, res: Response) => {
        try {
            const businessId = req.params.id;
            const { search } = req.query;
            const { user } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const query = getRepository(Account)
                .createQueryBuilder('account')
                .leftJoinAndSelect('account.role', 'role')
                .leftJoinAndSelect("account.person", "person")
                .where("account.business is NULL")
                .andWhere("account.deleted is NULL")
                .andWhere("account.id != :userId", { userId: user.id })

            if (search) {
                query.andWhere(`(
                    LOWER(person.pageName) SIMILAR TO '%${search}%'
                    OR LOWER(person.fullName) SIMILAR TO '%${search}%'
                )`)
            }

            const usersInBusiness = await query.getManyAndCount();

            res.status(200).send({ items: usersInBusiness[0], total: usersInBusiness[1] })
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }
}
