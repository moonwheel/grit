export const getProductListRawQuery = (
  userId: string,
  draft: boolean,
  active: boolean,
  currentUserId: string,
  limit: number,
  offset: number,
  order: string,
  sort: string,
  shopCategoryId?: string | number,
  search?: string
): string => {
  // const rawQuery = `
  //   select
	//     "product"."id" as "product_id",
  //     "product"."type" as "product_type",
  //     "product"."title" as "product_title",
  //     "product"."condition" as "product_condition",
  //     "product"."price" as "product_price",
  //     "product"."quantity" as "product_quantity",
  //     "product"."category" as "product_category",
  //     "product"."description" as "product_description",
  //     "product"."variantOptions" as "product_variantOptions",
  //     "product"."deliveryOptions" as "product_deliveryOptions",
  //     "product"."deliveryTime" as "product_deliveryTime",
  //     "product"."shippingCosts" as "product_shippingCosts",
  //     "product"."payerReturn" as "product_payerReturn",
  //     "product"."bank_id" as "product_bank_id",
  //     "product"."active" as "product_active",
  //     "product"."draft" as "product_draft",
  //     "product"."created" as "product_created",
  //     "product"."updated" as "product_updated",
  //     "product"."deleted" as "product_deleted",
  //     "product"."user_id" as "product_user_id",
  //     "product"."business_id" as "product_business_id",
  //     "product"."shopcategory_id" as "product_shopcategory_id",
  //     "product"."address_id" as "product_address_id",
  //     "product"."cover_id" as "product_cover_id",
  //     "product_photos"."id" as "product_photos_id",
  //     "product_photos"."photoUrl" as "product_photos_photoUrl",
  //     "product_photos"."thumbUrl" as "product_photos_thumbUrl",
  //     "product_photos"."order" as "product_photos_order",
  //     "product_photos"."created" as "product_photos_created",
  //     "product_photos"."deleted" as "product_photos_deleted",
  //     "product_photos"."product_id" as "product_photos_product_id",
  //     "product_variants"."id" as "product_variants_id",
  //     "product_variants"."size" as "product_variants_size",
  //     "product_variants"."color" as "product_variants_color",
  //     "product_variants"."flavor" as "product_variants_flavor",
  //     "product_variants"."material" as "product_variants_material",
  //     "product_variants"."price" as "product_variants_price",
  //     "product_variants"."quantity" as "product_variants_quantity",
  //     "product_variants"."enable" as "product_variants_enable",
  //     "product_variants"."created" as "product_variants_created",
  //     "product_variants"."updated" as "product_variants_updated",
  //     "product_variants"."deleted" as "product_variants_deleted",
  //     "product_variants"."product_id" as "product_variants_product_id",
  //     "variants_to_photos"."variantId" as "variants_to_photos_variantId",
  //     "variants_to_photos"."photoId" as "variants_to_photos_photoId",
  //     "variants_to_photos"."order" as "variants_to_photos_order",
  //     "variants_to_photos"."productId" as "variants_to_photos_productId",
  //     "product_variants_photos"."id" as "product_variants_photos_id",
  //     "product_variants_photos"."photoUrl" as "product_variants_photos_photoUrl",
  //     "product_variants_photos"."thumbUrl" as "product_variants_photos_thumbUrl",
  //     "product_variants_photos"."order" as "product_variants_photos_order",
  //     "product_variants_photos"."created" as "product_variants_photos_created",
  //     "product_variants_photos"."deleted" as "product_variants_photos_deleted",
  //     "product_variants_photos"."product_id" as "product_variants_photos_product_id",
  //     "product_shopcategory"."id" as "product_shopcategory_id",
  //     "product_shopcategory"."parent_id" as "product_shopcategory_parent_id",
  //     "product_shopcategory"."name" as "product_shopcategory_name",
  //     "product_shopcategory"."created" as "product_shopcategory_created",
  //     "product_shopcategory"."updated" as "product_shopcategory_updated",
  //     "product_shopcategory"."deleted" as "product_shopcategory_deleted",
  //     "product_shopcategory"."user_id" as "product_shopcategory_user_id",
  //     "product_user"."id" as "product_user_id",
  //     "product_user"."followers_notification" as "product_user_followers_notification",
  //     "product_user"."likes_notification" as "product_user_likes_notification",
  //     "product_user"."comments_notification" as "product_user_comments_notification",
  //     "product_user"."tags_notification" as "product_user_tags_notification",
  //     "product_user"."created" as "product_user_created",
  //     "product_user"."updated" as "product_user_updated",
  //     "product_user"."deleted" as "product_user_deleted",
  //     "product_user"."personId" as "product_user_personId",
  //     "product_user"."businessId" as "product_user_businessId",
  //     "product_user"."role_id" as "product_user_role_id",
  //     "product_user"."baseBusinessAccountId" as "product_user_baseBusinessAccountId",
  //     "bookmarks"."id" as "bookmarks_id",
  //     "bookmarks"."created" as "bookmarks_created",
  //     "bookmarks"."owner_id" as "bookmarks_owner_id",
  //     "bookmarks"."account_id" as "bookmarks_account_id",
  //     "bookmarks"."text_id" as "bookmarks_text_id",
  //     "bookmarks"."photo_id" as "bookmarks_photo_id",
  //     "bookmarks"."video_id" as "bookmarks_video_id",
  //     "bookmarks"."article_id" as "bookmarks_article_id",
  //     "bookmarks"."product_id" as "bookmarks_product_id",
  //     "bookmarks"."service_id" as "bookmarks_service_id",
  //     "product"."shopcategory_id" as "shop_cat_id",
  //     COUNT(distinct "product_reviews"."id") as "product_reviews_count",
  //     (case
  //       when "product"."price" is not null then "product"."price"
  //       else "product_variants"."price"
  //     end) as "product_price_conditional"
  //   from
  //     "t_product" "product"
  //   left join "t_product_review" "product_reviews" on
  //     "product_reviews"."product_id" = "product"."id"
  //   left join "t_product_photo" "product_photos" on
  //     "product_photos"."product_id" = "product"."id"
  //     and ("product"."type" = 'singleProduct'
  //     and "product_photos"."deleted" is null
  //     and "product_photos"."order" = '0')
  //   left join "t_product_variant" "product_variants" on
  //     "product_variants"."id" = (
  //     select
  //       pv.id
  //     from
  //       t_product_variant pv
  //     where
  //       pv.product_id = "product"."id"
  //       and pv.enable = true
  //     order by
  //       pv.price, pv.id
  //     limit 1 )
  //   left join "t_product_variant_t_product_photos" "variants_to_photos" on
  //     "variants_to_photos"."variantId" = "product_variants"."id"
  //     and ("variants_to_photos"."order" = 0)
  //   left join "t_product_photo" "product_variants_photos" on
  //     "product_variants_photos"."id" = "variants_to_photos"."photoId"
  //   left join "t_shop_category" "product_shopcategory" on
  //     "product_shopcategory"."id" = "product"."shopcategory_id"
  //   left join "t_account" "product_user" on
  //     "product_user"."id" = "product"."user_id"
  //   left join "t_bookmarks" "bookmarks" on
  //     "bookmarks"."product_id" = "product"."id"
  //     and ("bookmarks"."owner_id" = ${currentUserId})
  //   where
  //     "product"."user_id" = ${userId}
  //     and "product"."draft" = ${draft}
  //     and "product"."deleted" is null
  //     and "product"."active" = ${active}
  //     ${shopCategoryId ? `AND "product"."shopcategory_id" = ${shopCategoryId}` : ''}
  //     ${search ? `AND (LOWER("product"."title") SIMILAR TO '%${search}%')` : ''}
  //   group by
  //     product.*,
  //     bookmarks.*,
  //     product_photos.*,
  //     product_variants.*,
  //     variants_to_photos.*,
  //     product_variants_photos.*,
  //     product_shopcategory.*,
  //     product_user.*
  //   order by
  //     ${order} ${sort}
  //   limit ${limit}
  //   offset ${offset}
  // `;

  const rawQuery = `
    select
      "product"."id" as "product_id",
      "product"."type" as "product_type",
      "product"."title" as "product_title",
      "product"."condition" as "product_condition",
      "product"."price" as "product_price",
      "product"."quantity" as "product_quantity",
      "product"."category" as "product_category",
      "product"."description" as "product_description",
      "product"."variantOptions" as "product_variantOptions",
      "product"."deliveryOptions" as "product_deliveryOptions",
      "product"."deliveryTime" as "product_deliveryTime",
      "product"."shippingCosts" as "product_shippingCosts",
      "product"."payerReturn" as "product_payerReturn",
      "product"."bank_id" as "product_bank_id",
      "product"."active" as "product_active",
      "product"."draft" as "product_draft",
      "product"."created" as "product_created",
      "product"."updated" as "product_updated",
      "product"."deleted" as "product_deleted",
      "product"."user_id" as "product_user_id",
      "product"."business_id" as "product_business_id",
      "product"."shopcategory_id" as "product_shopcategory_id",
      "product"."address_id" as "product_address_id",
      "product"."cover_id" as "product_cover_id",
      "product_cover"."photoUrl" as "product_cover_photoUrl",
      "product_cover"."thumbUrl" as "product_cover_thumbUrl",
      "product_shopcategory"."id" as "product_shopcategory_id",
      "product_shopcategory"."parent_id" as "product_shopcategory_parent_id",
      "product_shopcategory"."name" as "product_shopcategory_name",
      "product_shopcategory"."created" as "product_shopcategory_created",
      "product_shopcategory"."updated" as "product_shopcategory_updated",
      "product_shopcategory"."deleted" as "product_shopcategory_deleted",
      "product_shopcategory"."user_id" as "product_shopcategory_user_id",
      "product_user"."id" as "product_user_id",
      "product_user"."followers_notification" as "product_user_followers_notification",
      "product_user"."likes_notification" as "product_user_likes_notification",
      "product_user"."comments_notification" as "product_user_comments_notification",
      "product_user"."tags_notification" as "product_user_tags_notification",
      "product_user"."created" as "product_user_created",
      "product_user"."updated" as "product_user_updated",
      "product_user"."deleted" as "product_user_deleted",
      "product_user"."personId" as "product_user_personId",
      "product_user"."businessId" as "product_user_businessId",
      "product_user"."role_id" as "product_user_role_id",
      "product_user"."baseBusinessAccountId" as "product_user_baseBusinessAccountId",
      "bookmarked"."id"::BOOL as "product_bookmarked",

      "product"."shopcategory_id" as "product_shopcategory_id",
      COUNT(distinct "product_reviews"."id") as "product_reviews_count",
      coalesce(avg("product_reviews"."rating"), 0) as "product_reviews_rating"
    from
      "t_product" "product"
    left join "t_product_review" "product_reviews" on
      "product_reviews"."product_id" = "product"."id"
    left join "t_product_photo" "product_cover" on
      "product_cover"."id" = "product"."cover_id"
    left join "t_shop_category" "product_shopcategory" on
      "product_shopcategory"."id" = "product"."shopcategory_id"
    left join "t_account" "product_user" on
      "product_user"."id" = "product"."user_id"
    left join "t_bookmarks" "bookmarked" on
      "bookmarked"."product_id" = "product"."id"
      and ("bookmarked"."owner_id" = ${currentUserId})
    where
      "product"."user_id" = ${userId}
      and "product"."draft" = ${draft}
      and "product"."deleted" is null
      and "product"."active" = ${active}
      ${shopCategoryId ? `AND "product"."shopcategory_id" = ${shopCategoryId}` : ''}
      ${search ? `AND (LOWER("product"."title") SIMILAR TO '%${search}%')` : ''}
    group by
      product.*,
      bookmarked.*,
      product_cover.*,
      product_shopcategory.*,
      product_user.*
    order by
      ${order} ${sort}
    limit ${limit}
    offset ${offset}

  `;
  return rawQuery
}


export const getBusinessProductListRawQuery = (
  userId: string,
  // draft: boolean,
  // active: boolean,
  limit: number,
  offset: number,
  order: string,
  sort: string,
  shopCategoryId?: string | number,
  search?: string
) => {
  const rawQuery = `
  select
	"product_variants"."id" as "product_variants_id",
	"product_variants"."size" as "product_variants_size",
	"product_variants"."color" as "product_variants_color",
	"product_variants"."flavor" as "product_variants_flavor",
	"product_variants"."material" as "product_variants_material",
	"product"."id",
	"product"."title",
	"product"."condition",
	"product"."category",
	"product"."deliveryOptions",
	"product"."deliveryTime",
	"product"."shippingCosts",
	"product"."created",
	"product"."active",
	(case
		when "product"."price" is not null then "product"."price"
		else "product_variants"."price"
	end) as "price",
	coalesce(product_bookmarks.id::bool, false) as "bookmarked",
	(case
		when "product_photos"."photoUrl" is not null then "product_photos"."photoUrl"
		else "product_variants_photos"."photoUrl"
	end) as "photoUrl",
	(case
		when "product_photos"."thumbUrl" is not null then "product_photos"."thumbUrl"
		else "product_variants_photos"."thumbUrl"
	end) as "thumbUrl",
	"product_shopcategory"."name" as "product_shopcategory_name",
	COUNT("product_reviews"."id") as "product_reviews_count",
	coalesce(avg("product_reviews"."rating"), 0) as "rating",
	sum(distinct "order_product"."quantity") as "sales",
	sum(distinct "order_product"."quantity") * avg("order_product"."price") as "revenue",
	(case
		when "product"."quantity" is not null then "product"."quantity"
		else "product_variants"."quantity"
	end) as "quantity"
from
	"t_product" "product"
left join "t_product_photo" "product_photos" on
	"product_photos"."product_id" = "product"."id"
	and ("product"."type" = 'singleProduct'
	and "product_photos"."deleted" is null
	and "product_photos"."order" = '0')
left join "t_bookmarks" "product_bookmarks" on
	"product_bookmarks"."product_id" = "product"."id"
	and ("product_bookmarks"."owner_id" = ${userId})
left join "t_product_variant" "product_variants" on
	"product_variants"."product_id" = "product"."id"
	and ("product_variants"."deleted" is null)
left join "t_product_variant_t_product_photos" "variants_to_photos" on
	"variants_to_photos"."variantId" = "product_variants"."id"
	and ("variants_to_photos"."order" = 0)
left join "t_product_photo" "product_variants_photos" on
	"product_variants_photos"."id" = "variants_to_photos"."photoId"
left join "t_shop_category" "product_shopcategory" on
	"product_shopcategory"."id" = "product"."shopcategory_id"
left join "t_product_review" "product_reviews" on
	"product_reviews"."product_id" = "product"."id"
left join "t_order_product" "order_product" on
	"order_product"."product_id" = "product"."id"
	and (("order_product"."product_variant_id" = "product_variants"."id"
	or "product_variants"."id" is null))
where
"product"."user_id" = ${userId}
and "product"."deleted" is null
and "product"."draft" = false
	and ("product"."type" = 'singleProduct'
  or "product_variants"."enable" is true)
  AND (LOWER("product"."title") SIMILAR TO '%${search}%')
  ${shopCategoryId ? `AND "product"."shopcategory_id" = ${shopCategoryId}` : ''}
  ${search ? `AND (LOWER("product"."title") SIMILAR TO '%${search}%')` : ''}
group by
	product.*,
	product_photos.*,
	product_variants.*,
	product_bookmarks.*,
	variants_to_photos."variantId",
	variants_to_photos."photoId",
	variants_to_photos."order",
	variants_to_photos."productId",
	product_variants_photos.*,
	product_shopcategory.*
  order by
  ${order} ${sort}
  limit ${limit}
  offset ${offset}
  `;
  return rawQuery
}
