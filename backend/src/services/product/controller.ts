import { Product_variant_to_photo } from './../../entities/product_variant_to_product_photo';
const dateTime = require("date-time");
import * as fs from 'fs';
import uniqid = require("uniqid");
import { getRepository, Not, Equal, Repository } from "typeorm";
import { App } from "../../config/app.config";
import { Product } from "../../entities/product";
import { Product_photo } from "../../entities/product_photo";
import { Product_variant } from "../../entities/product_variant";
import { SearchDictionary } from "../../entities/search_dictionary";
import { Shop_category } from "../../entities/shop_category";
import { Filter } from "../../interfaces/global/filter.interface";
import { Helpers, sanitizeRawData } from "../../utils/helpers";
import { Request, Response } from "express";
import MinioInstance from "../../utils/minio";
import getHashtagRepository from '../../entities/repositories/hashtag-repository';
import { tusServer, TUS_EVENTS, decodeMetadata, UploadedFileMetadata } from '../../utils/tus-server';
import { ConvertedPhoto, MediaConverter } from '../../utils/media-converter';
import { publish } from '../../utils/sse';
import getAccountRepository from '../../entities/repositories/account-repository';
import getReviewRepository from '../../entities/repositories/review-repository';
import getMentionRepository from '../../entities/repositories/mention-repository';
import getOrderRepository from '../../entities/repositories/order-repository';
import { OrderProduct } from '../../entities/order_product';
import { getBusinessProductListRawQuery, getProductListRawQuery } from './raw_queries';

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata)

    if (metadata.entityType === 'product') {
        // console.log('upload product completed metadata', metadata)
        // console.log('upload product completed event', event)
        const tmpFileId = event.file.id
        await ProductController.onUploadEnd(tmpFileId, metadata)
    }
});

export class ProductController {
    static addProduct = async (req: Request, res: Response) => {
        const { user, body } = req;
        const productRepo = getRepository(Product);
        // console.log('body', body)
        // console.log('body.variants', body.variants)
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }
            const product = new Product();
            product.address_ = body.address;
            product.category = body.category;
            product.condition = body.condition;
            product.deliveryOptions = body.deliveryOptions;
            product.deliveryTime = body.deliveryTime;
            product.description = body.description;
            product.title = body.title;
            product.type = body.type;
            product.payerReturn = body.payerReturn;
            product.shippingCosts = body.shippingCosts;
            product.variantOptions = body.variantOptions;
            product.draft = body.draft ? true : false;
            product.shopcategory_ = body.shopcategory_;

            product.user = user.baseBusinessAccount || user.id;

            if ("active" in body) product.active = body.active;

            if (user.business) {
                product.business_ = user.business;
            }

            if (product.type === "singleProduct") {
                product.price = body.price;
                product.quantity = body.quantity;

            }

            const savedProduct = await productRepo.save(product);

            if (savedProduct) {
                if (body.photos) {
                    savedProduct.photos = await ProductController.addGeneralPhotos(savedProduct, body.photos);
                    // console.log('savedProduct.photos', savedProduct.photos)

                    savedProduct.cover_ = savedProduct.photos[0];

                  }

                if (body.variants) {
                    savedProduct.variants = await ProductController.addProductVariants(savedProduct, body.variants);

                    const copyVariants = [ ...req.body.variants ]
                    copyVariants.sort((a, b) => +a.price - +b.price)
                    const cheapestVariant = copyVariants[0];

                    savedProduct.price = +cheapestVariant.price;
                    savedProduct.cover_ = savedProduct.photos.find(photo => photo.order === cheapestVariant.photos[0])

                }


                await productRepo.save(savedProduct);

                await getHashtagRepository().addHashtag("product", savedProduct.id, body.hashtags || []);
                await getMentionRepository().addMention("product", savedProduct, body.mentions, "product", user);

                const NewProduct = await ProductController
                    .createProductListQuery(productRepo, user.id, product.user as unknown as number)
                    .andWhere('product.id = :id', { id: savedProduct.id })
                    .getOne();

                NewProduct.photos = savedProduct.photos

                res.status(200).send(NewProduct);
            } else {
                res.status(400).send("Product not saved!");
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static uploadProductPhotos = async (req: Request, res: Response) => {
        if(req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
        }
        return tusServer.handle(req, res);
    }

    static onUploadEnd = async (tmpFileId: number, metadata: UploadedFileMetadata) => {
        let newFilePath: string;
        let outputFiles: ConvertedPhoto;

        try {
            const productPhotoId = +metadata['grit-file-entity-id'];
            const { id, type, userId } = metadata;
            const photoRepo = getRepository(Product_photo);
            const photo = await photoRepo.findOne({ id: productPhotoId });

            // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;

            const uniqueFileName = uniqid("photo-");
            const spliType = (type as string).split('/');
            const fileFormat = spliType[1];

            const fullFileName = `${uniqueFileName}.${fileFormat}`;

            newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

            fs.renameSync(oldFilePath, newFilePath);

            MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                .then(async () => {
                    photo.photoUrl = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`;
                    photo.thumbUrl = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`;

                    await photoRepo.save(photo);

                    const productRepo = getRepository(Product);
                    const entity = await ProductController
                        .createSingleProductQuery(productRepo, userId, id)
                        .getOne();

                    publish({ userId, type: 'file_processing_complete', entityType: 'product', entity });
                })
        } catch (error) {
            console.error('Error while product photo uploading' ,error);
        }
    }

    static getProducts = async (req: Request, res: Response) => {
        try {
            const {
                user,
                query: {
                    page = 1,
                    filter = "date",
                    order = "desc",
                    shopCategoryId = null,
                    limit = 50,
                    draft = false,
                    userId = null,
                    search = null
                }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const productRepo = getRepository(Product);

            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            if(!user.business && filter === 'reviews') {
                throw new Error('You can not apply sorting by reviews for personal account');

            }

            const filterObj = ProductController.getFilterParams(filter, order);

            const id = userId || user.baseBusinessAccount || user.id;

            // let query = ProductController
            //     .createProductListQuery(productRepo, id, user.baseBusinessAccount || user.id)
            //     .where("product.user_id = :userId", { userId: id })
            //     .andWhere("product.draft = :draft", { draft })
            //     .andWhere("product.deleted IS NULL")
            //     .andWhere("product.active = :active", { active: true })
            //     .skip(skip)
            //     .take(limitNumb)
            //     .orderBy(filterObj.by, filterObj.order);

            // if(shopCategoryId) {
            //     query = query
            //         .andWhere("product.shopcategory_ = :shopCategoryId", {shopCategoryId});
            // }

            // if(search) {
            //     query = query
            //         .andWhere(`
            //             (LOWER(product.title) SIMILAR TO '%${search.toLowerCase()}%')
            //         `);
            // }

            // console.log('product query.getSql()', query.getSql())

            // const data = await query.getManyAndCount();
            // res.status(200).send({ items: data[0], total: data[1] });


            const rawQuery = getProductListRawQuery(
              id,
              draft,
              true,
              user.baseBusinessAccount || user.id,
              limitNumb,
              skip,
              filterObj.by,
              filterObj.order,
              shopCategoryId,
              search.toLowerCase()
            )
            const productList = await productRepo.query(rawQuery)
            // console.log('rawQuery', rawQuery)

            // TODO: ENABLE SANITIZER WHEN THE FRONTEND MAPPING IS READY
            // res.status(200).send({items: sanitizeRawData(productList), total: productList.length, raw: true})
            res.status(200).send({items: productList, total: productList.length, raw: true})

        } catch (error) {
            console.log('error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static getFilterParams = (filterBy: string, order: string, extended?: boolean) => {
        const filter: Filter = {
            by: "product.created",
            order: "DESC",
        };

        if (filterBy === "reviews") filter.by = "product_reviews_count";
        else if (filterBy === "price") filter.by = `product_price_conditional`;
        else if (filterBy === "date") filter.by = "product.created";
        // else if (filterBy === "shop_categories") filter.by = "shop_cat_id";
        // if (extended) {
        //     if (filterBy === "status") filter.by = "product.active";
        //     else if (filterBy === "title") filter.by = "product.title";
        //     else if (filterBy === "condition") filter.by = "product.condition";
        //     else if (filterBy === "quantity") filter.by = "product.quantity";
        //     else if (filterBy === "category") filter.by = "product.category";
        //     else if (filterBy === "deliveryTime") filter.by = "product.deliveryTime";
        //     else if (filterBy === "shippingCosts") filter.by = "product.shippingCosts";
        // }

        if (order.toLowerCase() === "asc") filter.order = "ASC";


        return filter;
    };

    static getSingleProduct = async (req, res) => {
        try {
            const {
                user,
                params: {
                    id
                },
                query: {
                }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const productRepo = getRepository(Product);
            const query = ProductController.createSingleProductQuery(productRepo, user.id, id);

            // console.log('DEBUG QUERY: ', query.getSql())

            const product  = await query.getOne();

            if(!product) {
                return res.status(404).send({error: 'Not Found'});
            }

            const sellerRating = await getReviewRepository().getRating('seller', product.user.id);
            const sellerRatingCount = await getReviewRepository().getRatingCount('seller', product.user.id);

            const rating = await getReviewRepository().getRating('product', id);

            const bought = await getRepository(OrderProduct)
                .createQueryBuilder('order_product')
                .leftJoinAndSelect('order_product.order', 'order_product_order')
                .leftJoinAndSelect('order_product_order.buyer', 'order_product_order_buyer')
                .where('order_product.product = :id', { id })
                .andWhere('order_product_order_buyer.id = :user', { user: user.id })
                .getMany();

            res.status(200).send({
                ...product,
                sellerRating,
                sellerRatingCount,
                rating,
                bought,
            })
        } catch (error) {
            console.log('error', error)
            res.status(400).send({ error: error.message });
        }
    }

    // allowed filter values: "date" === "created" (for backward compatibility)
    // "product_variants_id" | "product_variants_size" | "product_variants_color" |
    // "product_variants_flavor" | "product_variants_material" | "id" | "title" |
    // "condition" | "category" | "deliveryOptions" | "deliveryTime" | "shippingCosts" |
    // "created" | "price" | "status" | "photoUrl" | "thumbUrl" | "product_shopcategory_name" |
    // "product_reviews_count" | "rating" | "sales" | "revenue" | "quantity"
    static getBusinessProducts = async (req: Request, res: Response) => {
        try {
            const {
                user,
                query: {
                    page = 1,
                    limit = 50,
                    filter = "created",
                    order = "desc",
                    shopCategoryId = null,
                    search = null
                    // draft = false, // Draft is not necessary IMHO
                }
            } = req;

            if (user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if (!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const sortingField = (filter === "date") ? "created": filter;
            const orderBy = order.toUpperCase();
            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            const productRepo = getRepository(Product);

            const id = user.baseBusinessAccount || user.id;
            // let query = ProductController
            //     .createBisnessProductListQuery(productRepo, id)
            //     .andWhere("product.draft = false")
            //     .orderBy(`"${sortingField}"`, orderBy)
            //     .limit(limitNumb)
            //     .offset(skip);

            // if(shopCategoryId) {
            //     query = query
            //         .andWhere("product.shopcategory_ = :shopCategoryId", {shopCategoryId});
            // }

            // if(search) {
            //     query = query
            //         .andWhere(`
            //             (LOWER(product.title) SIMILAR TO '%${search.toLowerCase()}%')
            //         `);
            // }

            // // console.log('query.getSql()', query.getSql())

            // const items = await query.getRawMany();
            // const total = await query.getCount();
            // res.status(200).send({ items, total });

            const rawQuery = getBusinessProductListRawQuery(
              id,
              limitNumb,
              skip,
              sortingField,
              orderBy,
              shopCategoryId,
              search.toLowerCase()
            )
            const productList = await productRepo.query(rawQuery)
            // console.log('rawQuery', rawQuery)

            // TODO: ENABLE SANITIZER WHEN THE FRONTEND MAPPING IS READY
            // res.status(200).send({items: sanitizeRawData(productList), total: productList.length, raw: true})
            res.status(200).send({items: productList, total: productList.length, raw: true})
        } catch (error) {
            console.log('error', error)
            res.status(400).send({ error: error.message });
        }

    };

    // static searchProducts = async (req: Request, res: Response) => {
    //     console.log('searchProducts')
    //     try {
    //         const {
    //             user,
    //             query: {
    //                 page = 1,
    //                 limit = 15,
    //                 value = "",
    //                 userId = null,
    //             }
    //         } = req;
    //         const productRepo = getRepository(Product);

    //         const limitNumb = Number(limit);
    //         const skip = (Number(page) - 1) * limitNumb;
    //         const id = userId || user.baseBusinessAccount || user.id;


    //         // const query = ProductController
    //         //     .createProductListQuery(productRepo, id)
    //         //     .andWhere("product.draft = :draft", { draft: false })
    //         //     .andWhere("product.deleted IS NULL")
    //         //     .andWhere(`
    //         //         (LOWER(product.title) SIMILAR TO '%${value}%'
    //         //         OR LOWER(product.category) SIMILAR TO '%${value}%'
    //         //         OR LOWER(product.description) SIMILAR TO '%${value}%')
    //         //     `)
    //         //     .skip(skip)
    //         //     .take(limitNumb)
    //         //     .orderBy(filterObj.by, filterObj.order)

    //         // const data = await query.getManyAndCount();
    //         // res.status(200).send({ items: data[0], total: data[1] });

    //         // if (!user.business) throw Error("Accessible only for a business account");
    //         // if (!value.trim()) throw Error("No value");
    //         // const products = await productRepo
    //         //     .createQueryBuilder("product")
    //         //     .leftJoinAndSelect("product.photos", "product_photos", "product_photos.deleted IS NULL")
    //         //     .leftJoinAndSelect("product.variants", "product_variants")
    //         //     .leftJoinAndSelect("product_variants.photos", "product_variants_photo", "product_variants_photo.deleted IS NULL")
    //         //     .leftJoinAndMapOne("product.shopcategory", Shop_category, "product_shopcategory", "product.shopcategory_id = product_shopcategory.id")
    //         //     // .where("product.business_ = :business", { business: user.business })
    //         //     .andWhere("product.deleted IS NULL")
    //         //     .andWhere(`
    //         //         (LOWER(product.title) SIMILAR TO '%${value}%'
    //         //         OR LOWER(product.category) SIMILAR TO '%${value}%'
    //         //         OR LOWER(product.description) SIMILAR TO '%${value}%')
    //         //     `)
    //         //     .offset(skip)
    //         //     .limit(limitNumb)
    //         //     .getMany();

    //         // const beautifyed = await Helpers.beautifyRawData(products, {
    //         //     name: "product",
    //         //     sanitize: [
    //         //         "bank_id",
    //         //         "user_id",
    //         //         "business_id",
    //         //         "address_id",
    //         //     ],
    //         //     aggregate: [
    //         //         "photos"
    //         //     ]
    //         // });

    //         // res.status(200).send(beautifyed);
    //     } catch (error) {
    //         res.status(400).send({ error: error.message });
    //     }
    // };

    static updateProduct = async (req: Request, res: Response) => {

        const { user, body } = req;
        const productRepo = getRepository(Product);
        const variantRepo = getRepository(Product_variant);
        const productPhotoRepo = getRepository(Product_photo);
        const productVariantToPhotoRepo = getRepository(Product_variant_to_photo);

        console.log('update products body', body)
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }
            const productId = body.id;

            if (body.deletedPhotos) {
                for (const photo of body.deletedPhotos) {
                    // TODO: delete from minio and DB
                    const splitten = photo.photoUrl.split('/');
                    const photoName = splitten[splitten.length -1]
                    await MinioInstance.deleteFile('photo', photoName);
                    await productVariantToPhotoRepo.delete({productId: productId})
                    await productPhotoRepo.delete({ id: photo.id });
                }
            }

            const product: any = {
                address_: body.address,
                type: body.type,
                category: body.category,
                condition: body.condition,
                deliveryOptions: body.deliveryOptions,
                deliveryTime: body.deliveryTime,
                description: body.description,
                shopcategory_: body.shopcategory_,
                title: body.title,
                payerReturn: body.payerReturn,
                shippingCosts: body.shippingCosts,
                draft: body.draft ? true : false,

                user: user.baseBusinessAccount || user.id,
            };

            if (body.type === "singleProduct") {
                body.variants = [];
                product.variantOptions = null;
                product.price = body.price;
                product.quantity = body.quantity;
            } else {
                product.variantOptions = body.variantOptions;
                product.quantity = null;
                product.price = null;
            }

            if ("active" in body) product.active = body.active;

            await productRepo.update({ id: productId }, product);

            const savedProduct = await productRepo.findOne({ id: productId }, { relations: ["address_"] });

            if (savedProduct) {
                if (body.photos) {
                    savedProduct.photos = await ProductController.addGeneralPhotos(savedProduct, body.photos);

                    savedProduct.cover_ = savedProduct.photos[0];
                }

                if (body.variants && body.variants[0]) {
                    savedProduct.variants = await ProductController.updateProductVariants(savedProduct, body.variants);

                    const copyVariants = [ ...req.body.variants ]
                    copyVariants.sort((a, b) => +a.price - +b.price)
                    const cheapestVariant = copyVariants[0];

                    savedProduct.price = +cheapestVariant.price;
                    savedProduct.cover_ = savedProduct.photos.find(photo => photo.order === cheapestVariant.photos[0])

                }

                await productRepo.save(savedProduct);

                await getHashtagRepository().updateHashtagsByPostId("product", savedProduct.id, req.body.hashtags);
                const mentions = await getMentionRepository().addMention("product", savedProduct, req.body.mentions, "product", user);

                const result = {
                    ...savedProduct,
                    address: savedProduct.address_,
                    mentions
                }

                delete result.address_;

                res.status(200).send(result);
            } else {
                res.status(400).send("Product not saved!");
            }
        } catch (error) {
            console.log('error', error);
            res.status(400).send({ error: error.message });
        }
    };

    static deleteProduct = async (req: Request, res: Response) => {
        const user = req.user;
        const productRepo = getRepository(Product);
        try {
            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const productId = req.params.id;

            if (productId) {
                await productRepo.update({ id: productId }, { deleted: dateTime() });
                res.status(200).send({ status: "success" });
            } else {
                res.status(400).send({ error: "No ID field found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static addGeneralPhotos = async (
        product: Product,
        orderedPhotos: { id: number }[],
    ) => {

        try {
            const productPhotoRepo = getRepository(Product_photo);
            let foundPhotos = await productPhotoRepo.find({ product_: product, deleted: null });

            const savedProductPhotos = (foundPhotos || []).map(item => ({
                ...item,
                product_: null
            }));

            const result = [];

            if (orderedPhotos) {
                for (let index = 0; index < orderedPhotos.length; index++) {
                    const photo = orderedPhotos[index];
                    const existingPhoto = savedProductPhotos.find(item => +item.id === +photo.id);

                    if (existingPhoto) {
                        existingPhoto.order = index;
                        await productPhotoRepo.update(existingPhoto.id, { order: index });
                        result.push(existingPhoto);
                    } else {
                        const productPhoto = new Product_photo();

                        productPhoto.product_ = product;
                        productPhoto.order = index;
                        const savedProductPhoto = await productPhotoRepo.save(productPhoto);
                        result.push({
                            ...savedProductPhoto,
                            product_: null,
                        });
                    }
                }
            }

            return result;
        } catch (error) {
            throw error;
        }
    };

    // static addCover = async (product: Product, files) => {
    //     try {
    //         const productPhotoRepo = getRepository(Product_photo);
    //         const file = files.cover;

    //         if (file) {
    //             if (product.cover_) {
    //                 await productPhotoRepo.update({ product_: product, id: product.cover_.id }, { deleted: dateTime() });
    //             }
    //             const productPhoto = new Product_photo();
    //             productPhoto.photoUrl = file.path;
    //             productPhoto.product_ = product;
    //             const savedProductPhoto = await productPhotoRepo.save(productPhoto);
    //             return {
    //                 ...savedProductPhoto,
    //                 product_: null,
    //             }
    //         } else {
    //             if (product.type === 'singleProduct') {
    //                 if (product.cover_) {
    //                     await productPhotoRepo.update({ product_: product, id: product.cover_.id }, { deleted: dateTime() });
    //                 }
    //                 return null;
    //             } else {
    //                 return product.cover_;
    //             }
    //         }
    //     } catch (error) {
    //         throw error;
    //     }
    // };

    /////////////////////////////////////////////////

    static addProductVariants = async (product: Product, variants) => {
        try {
            const productVariantRepo = getRepository(Product_variant);
            const productVariantToPhotoRepo = getRepository(Product_variant_to_photo);
            const searchDictionaryRepo = getRepository(SearchDictionary);
            const savedVariants = [];
            for (let i = 0; i < variants.length; ++i) {
                try {
                    const productVariant = new Product_variant();

                    productVariant.color = variants[i].color;
                    productVariant.flavor = variants[i].flavor;
                    productVariant.material = variants[i].material;
                    productVariant.price = variants[i].price;
                    productVariant.quantity = variants[i].quantity;
                    productVariant.size = variants[i].size;
                    productVariant.enable = variants[i].enable;
                    productVariant.product_ = product;

                    // save variant's COLOR, MATERIAL or FLAVOR to dictionary table for elasticsearch filtering purposes
                    if (productVariant.color) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.color,
                                type: 'color'
                            })
                        } catch (error) {
                            console.log(`color ${productVariant.color} already exists`)
                        }
                    }

                    if (productVariant.flavor) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.flavor,
                                type: 'flavor'
                            })

                        } catch (error) {
                            console.log(`flavor ${productVariant.flavor} already exists`)
                        }
                    }

                    if (productVariant.material) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.material,
                                type: 'material'
                            })

                        } catch (error) {
                            console.log(`material ${productVariant.material} already exists`)
                        }
                    }
                    const savedVariant = await productVariantRepo.save(productVariant);
                    const variantPhotos = variants[i].photos;
                    const variantPhotosForSave = [];
                    const variantPhotosForResponse = [];
                    if (variantPhotos && variantPhotos.length) {
                        variantPhotos.forEach((photoIndex, index) => {
                            const photo = product.photos[photoIndex];
                            variantPhotosForSave.push({
                                variantId: savedVariant.id,
                                photoId: photo.id,
                                productId: product.id,
                                order: index
                            })
                            variantPhotosForResponse.push(photo)
                        });
                    }
                    savedVariant.photos = variantPhotosForResponse;

                    // await productVariantToPhotoRepo.delete(variantPhotosForSave)
                    await productVariantToPhotoRepo.save(variantPhotosForSave)

                    // const order = variantPhotosForSave.map(item => item.id);
                    // productVariant.photosOrder = JSON.stringify(order);
                    // TODO: SAVE VARIANT'S PHOTO-ORDER INSIDE CUSTOM MANY-TO-MANY TABLE INSTEAD

                    if (savedVariant) {
                        savedVariants.push({
                            id: savedVariant.id,
                            color: savedVariant.color,
                            flavor: savedVariant.flavor,
                            material: savedVariant.material,
                            price: savedVariant.price,
                            quantity: savedVariant.quantity,
                            size: savedVariant.size,
                            photos: savedVariant.photos,
                            // photosOrder: productVariant.photosOrder,
                            // TODO: SAVE VARIANT'S PHOTO-ORDER INSIDE CUSTOM MANY-TO-MANY TABLE INSTEAD
                        });
                    }
                } catch (error) {
                    throw error;
                }
            }
            return savedVariants;
        } catch (error) {
            throw error;
        }
    };

    static updateProductVariants = async (product: Product, variants) => {
        // const variants = new Set(variantsArr);
        console.log('variants', variants)
        try {
            const productVariantRepo = getRepository(Product_variant);
            const productPhotoRepo = getRepository(Product_photo);
            const productVariantToPhotoRepo = getRepository(Product_variant_to_photo);
            const searchDictionaryRepo = getRepository(SearchDictionary);
            const savedVariants = [];

            // productVariantToPhotoRepo
            await productVariantToPhotoRepo.delete({productId: product.id})
            await productVariantRepo.delete({product_: product})
            // return;

            for (let i = 0; i < variants.length; ++i) {
                try {
                    // const productVariant = new Product_variant();
                    const productVariant = {
                        color: variants[i].color,
                        flavor: variants[i].flavor,
                        material: variants[i].material,
                        price: variants[i].price,
                        quantity: variants[i].quantity,
                        size: variants[i].size,
                        enable: variants[i].enable,
                        product_: product
                    };

                    // save variant's COLOR, MATERIAL or FLAVOR to dictionary table for elasticsearch filtering purposes
                    if (productVariant.color) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.color,
                                type: 'color'
                            })
                        } catch (error) {
                            console.log(`color ${productVariant.color} already exists`)
                        }
                    }

                    if (productVariant.flavor) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.flavor,
                                type: 'flavor'
                            })

                        } catch (error) {
                            console.log(`flavor ${productVariant.flavor} already exists`)
                        }
                    }

                    if (productVariant.material) {
                        try {
                            await searchDictionaryRepo.save({
                                word: productVariant.material,
                                type: 'material'
                            })

                        } catch (error) {
                            console.log(`material ${productVariant.material} already exists`)
                        }
                    }

                    // productVariant.photos = productVariant.photos.filter(photoIndex => photoIndex < product.photos.length);

                    const savedVariant = await productVariantRepo.save(productVariant);

                    const variantPhotos = variants[i].photos;
                    const variantPhotosForSave = [];
                    const variantPhotosForResponse = [];
                    if (variantPhotos && variantPhotos.length) {
                        variantPhotos.forEach((photoIndex, index) => {
                            const photo = product.photos[photoIndex];
                            variantPhotosForSave.push({
                                variantId: savedVariant.id,
                                photoId: photo.id,
                                productId: product.id,
                                order: index
                            })
                            variantPhotosForResponse.push(photo)
                        });
                    }
                    savedVariant.photos = variantPhotosForResponse;

                    await productVariantToPhotoRepo.save(variantPhotosForSave)


                    if (savedVariant) {
                        savedVariants.push({
                            id: savedVariant.id,
                            color: savedVariant.color,
                            flavor: savedVariant.flavor,
                            material: savedVariant.material,
                            price: savedVariant.price,
                            quantity: savedVariant.quantity,
                            size: savedVariant.size,
                            photos: savedVariant.photos,
                            enable: savedVariant.enable,
                            // photosOrder: savedVariant.photosOrder,
                            // TODO: SAVE VARIANT'S PHOTO-ORDER INSIDE CUSTOM MANY-TO-MANY TABLE INSTEAD
                        });
                    }
                } catch (error) {
                    throw error;
                }
            }
            return savedVariants;

        } catch (error) {
            throw error;
        }
    };

    /////////////////////////////////////////////////

    static switchActive = async (req: Request, res: Response) => {
        try {
            const {
                user,
                params: { id }
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }
            const product = await getRepository(Product).findOne({ where: { id } });
            if (product) await getRepository(Product).update(id, { active: !product.active });
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    private static createProductListQuery(productRepo: Repository<Product>, userId: number, currentUser: number) {
        const query = productRepo
            .createQueryBuilder("product")
            .addSelect('"product"."shopcategory_id"', 'shop_cat_id')
            .leftJoin(`product.reviews`, `product_reviews`)
            .addSelect(`COUNT(DISTINCT product_reviews.id)`, `product_reviews_count`)
            // .addSelect(`(CASE WHEN "product"."price" IS NOT NULL THEN "product"."price"
            //     ELSE "product_variants"."price"
            //     END)`, "product_price_conditional"
            // )
            // .leftJoinAndSelect("product.photos", "product_photos", `product.type = 'singleProduct' AND product_photos.deleted IS NULL AND product_photos.order = '0'`)
            // .leftJoinAndMapOne("product.cheapestVariant", "t_product_variant", "product_variants", `
            //     product_variants.id = (
            //         SELECT pv.id
            //         FROM t_product_variant pv
            //         WHERE pv.product_id = product.id AND pv.enable = true
            //         ORDER BY pv.price, pv.id
            //         LIMIT 1
            //     )
            // `)
            // .leftJoinAndMapOne("product_variants.cover","product_variants.photos", "variants_to_photos", "variants_to_photos.order = 0")
            // .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos")
            .leftJoin('product.cover_', 'product_cover')
            .addSelect(`product_cover.photoUrl`)
            .addSelect(`product_cover.thumbUrl`)
            .leftJoinAndSelect("product.shopcategory_", "product_shopcategory")
            .leftJoinAndSelect("product.user", "product_user")
            .leftJoinAndMapOne(`product.bookmarked`, `product.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${currentUser}`)

            .groupBy(`
                product.*,
                bookmarks.*,
                product_cover.*,
                product_shopcategory.*,
                product_user.*
            `);

        return query;
    }

    private static createBisnessProductListQuery(productRepo: Repository<Product>, userId: number) {
        const query = productRepo
            .createQueryBuilder("product")
            .select(`"product"."id"`)
            .addSelect(`"product"."title"`)
            .addSelect(`"product"."condition"`)
            .addSelect(`"product"."category"`)
            .addSelect(`"product"."deliveryOptions"`)
            .addSelect(`"product"."deliveryTime"`)
            .addSelect(`"product"."shippingCosts"`)
            .addSelect(`"product"."created"`)
            .addSelect(`"product"."active"`)
            .addSelect(`(CASE
                WHEN "product"."price" IS NOT NULL THEN "product"."price"
                ELSE "product_variants"."price"
                END)`,"price")
            .leftJoin("product.photos", "product_photos", `product.type = 'singleProduct' AND product_photos.deleted IS NULL AND product_photos.order = '0'`)
            // .addSelect(`"product_photos"."id"`, `product_photos_id`)
            // .addSelect(`"product_photos"."photoUrl"`, `product_photos_photoUrl`)
            // .addSelect(`"product_photos"."thumbUrl"`, `product_photos_thumbUrl`)
            .leftJoin(`product.bookmarks`, `product_bookmarks`, `product_bookmarks.owner_id = ${userId}`)
            .addSelect(`COALESCE(product_bookmarks.id::BOOL, false) as "bookmarked"`)

            .leftJoin("product.variants", "product_variants", `"product_variants"."deleted" IS NULL`)
            .addSelect(`product_variants.id`, `product_variants_id`)
            .addSelect(`product_variants.size`, `product_variants_size`)
            .addSelect(`product_variants.color`, `product_variants_color`)
            .addSelect(`product_variants.flavor`, `product_variants_flavor`)
            .addSelect(`product_variants.material`, `product_variants_material`)
            .leftJoin("product_variants.photos", "variants_to_photos", "variants_to_photos.order = 0")

            .leftJoin("variants_to_photos.photo", "product_variants_photos")
            // .addSelect(`"product_variants_photos"."id"`, `product_variants_photos_id`)
            // .addSelect(`"product_variants_photos"."photoUrl"`, `product_variants_photos_photoUrl`)
            // .addSelect(`"product_variants_photos"."thumbUrl"`, `product_variants_photos_thumbUrl`)

            .addSelect(`(CASE
                WHEN "product_photos"."photoUrl" IS NOT NULL THEN "product_photos"."photoUrl"
                ELSE "product_variants_photos"."photoUrl"
                END)`, "photoUrl")
            .addSelect(`(CASE
                WHEN "product_photos"."thumbUrl" IS NOT NULL THEN "product_photos"."thumbUrl"
                ELSE "product_variants_photos"."thumbUrl"
                END)`, "thumbUrl")

            .leftJoin("product.shopcategory_", "product_shopcategory")
            .addSelect(`"product_shopcategory"."name"`, `product_shopcategory_name`)

            .leftJoin(`product.reviews`, `product_reviews`)
            .addSelect(`COUNT("product_reviews"."id")`, `product_reviews_count`)
            .addSelect(`COALESCE(avg("product_reviews"."rating"), 0)`, `rating`)


            .leftJoin(`product.order_item_products`, `order_product`, `("order_product"."product_variant_id" = "product_variants"."id" OR "product_variants"."id" IS NULL)`)

            .addSelect(`sum(DISTINCT "order_product"."quantity")`, "sales") // TODO: how many items sold (sum of amount for all orders)
            // If I have 2 order 2 items and 7 items sold for the same product sales will be 9
            .addSelect(`sum(DISTINCT "order_product"."quantity") * avg("order_product"."price")`, "revenue") // sales * price
            .addSelect(`(CASE
                WHEN "product"."quantity" IS NOT NULL THEN "product"."quantity"
                ELSE "product_variants"."quantity"
                END)`, "quantity")
                // It shows "product"."quantity" for single or "product_variants"."quantity" for product variants
                // TODO: should we calculate quantity as current quantity - sales
                // or we will update current quantity after shipping automatically?

            .where("product.user_id = :userId", { userId })
            .andWhere("product.deleted IS NULL")
            .andWhere(`
                (product.type = 'singleProduct'
                OR product_variants.enable IS TRUE)
            `)
            .groupBy(`
                product.*,
                product_photos.*,
                product_variants.*,
                product_bookmarks.*,
                variants_to_photos."variantId",
                variants_to_photos."photoId",
                variants_to_photos."order",
                variants_to_photos."productId",
                product_variants_photos.*,
                product_shopcategory.*
            `);


        return query;
        /**
            SELECT
                "product"."id",
                (CASE
                    WHEN "product"."active" = TRUE THEN 'Active'
                    ELSE 'Inacive'
                END) AS "status",
                "product_photos"."id" AS "product_photos_id",
                "product_photos"."photoUrl" AS "product_photos_photoUrl",
                "product_photos"."thumbUrl" AS "product_photos_thumbUrl",
                "product"."title",
                "product_variants"."id" AS "product_variants_id",
                "product_variants"."size" AS "product_variants_size",
                "product_variants"."color" AS "product_variants_color",
                "product_variants"."flavor" AS "product_variants_flavor",
                "product_variants"."material" AS "product_variants_material",
                "product_variants"."enable" AS "product_variants_enable",
                "product"."condition",
                (CASE
                    WHEN "product"."price" IS NOT NULL THEN "product"."price"
                    ELSE "product_variants"."price"
                END) AS "price",
                (CASE
                    WHEN "product"."quantity" IS NOT NULL THEN "product"."quantity"
                    ELSE "product_variants"."quantity"
                END) AS "quantity",
                "product"."category",
                "product_shopcategory"."name" AS "product_shopcategory_name",
                "product"."deliveryOptions",
                "product"."deliveryTime",
                "product"."shippingCosts",
                0 AS "sales",
                0 AS "revenue",
                count(DISTINCT "product_reviews"."id") AS "product_reviews_count",
                COALESCE(avg("product_reviews"."rating"), 0) AS "rating",
                "product"."created"
            FROM
                "t_product" "product"
            LEFT JOIN "t_product_photo" "product_photos" ON
                "product_photos"."product_id" = "product"."id"
                AND ("product"."type" = 'singleProduct'
                AND "product_photos"."deleted" IS NULL
                AND "product_photos"."order" = '0')
            LEFT JOIN "t_product_variant" "product_variants" ON
                "product_variants"."product_id" = "product"."id"
                AND "product_variants"."deleted" IS NULL
            LEFT JOIN "t_product_variant_t_product_photos" "variants_to_photos" ON
                "variants_to_photos"."variantId" = "product_variants"."id"
                AND ("variants_to_photos"."order" = 0)
            LEFT JOIN "t_product_photo" "product_variants_photos" ON
                "product_variants_photos"."id" = "variants_to_photos"."photoId"
            LEFT JOIN "t_shop_category" "product_shopcategory" ON
                "product_shopcategory"."id" = "product"."shopcategory_id"
            LEFT JOIN "t_product_review" "product_reviews" ON
                "product_reviews"."product_id" = "product"."id"
            WHERE
                "product"."user_id" = $1
                AND "product"."deleted" IS NULL
                AND "product"."draft" = FALSE
            GROUP BY
                product.*,
                product_photos.*,
                product_variants.*,
                variants_to_photos.*,
                product_variants_photos.*,
                product_shopcategory.*
            ORDER BY
                "product"."created" DESC

         */
    }

    static createSingleProductQuery(productRepo: Repository<Product>, userId: number, productId: number) {
        const query = productRepo
            .createQueryBuilder("product")
            .leftJoinAndMapOne(`product.bookmarked`, `product.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${userId}`)
            .leftJoinAndSelect("product.photos", "product_photos", `product_photos.deleted IS NULL`)
            .leftJoinAndSelect("product.address_", "product_address")
            .leftJoinAndSelect("product.variants", "product_variants")
            .leftJoinAndSelect("product_variants.photos", "variants_to_photos")
            .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos")
            .leftJoinAndSelect("product.shopcategory_", "product_shopcategory")
            .leftJoinAndSelect("product.user", "product_user")
            .leftJoin("product_user.business", "product_user_business")
            .addSelect("product_user_business.id")
            .addSelect("product_user_business.photo")
            .addSelect("product_user_business.name")
            .addSelect("product_user_business.pageName")
            .leftJoin("product_user.person", "product_user_person")
            .addSelect("product_user_person.id")
            .addSelect("product_user_person.photo")
            .addSelect("product_user_person.fullName")
            .addSelect("product_user_person.pageName")
            .leftJoinAndSelect("product.mentions", "product_mention")
            .leftJoinAndSelect("product_mention.target", "product_mention_target")
            .leftJoinAndSelect("product_mention_target.business", "product_mention_target_business")
            .addSelect("product_mention_target_business.id")
            .addSelect("product_mention_target_business.photo")
            .addSelect("product_mention_target_business.name")
            .addSelect("product_mention_target_business.pageName")
            .leftJoinAndSelect("product_mention_target.person", "product_mention_target_person")
            .addSelect("product_mention_target_person.id")
            .addSelect("product_mention_target_person.photo")
            .addSelect("product_mention_target_person.fullName")
            .addSelect("product_mention_target_person.pageName")
            .where("product.id = :productId", { productId })
            .andWhere("product.deleted IS NULL")
            .groupBy(`
                product.*,
                bookmarks.*,
                product_photos.*,
                product_address.*,
                variants_to_photos.*,
                product_variants.*,
                product_variants_photos.*,
                product_shopcategory.*,
                product_user.*,
                product_user_business.*,
                product_user_person.*,
                product_mention.*,
                product_mention_target.*,
                product_mention_target_business.*,
                product_mention_target_person.*
            `);

        return query;
    }

}
