const express = require("express");
const router = express.Router();

import passport = require("passport");
import { ProductController } from "./controller";
import reviewRouter from "../review/router";
import { Middleware } from "../../utils/middleware";

router.post("/", passport.authenticate("jwt", {session: false}), ProductController.addProduct);

router.use("/:id/upload", passport.authenticate("jwt", {session: false}), ProductController.uploadProductPhotos);

router.get("/all", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, ProductController.getProducts);

// BUSINESS LIST
router.get("/businesslist", passport.authenticate("jwt", {session: false}), ProductController.getBusinessProducts);

// router.get("/search", passport.authenticate("jwt", {session: false}), ProductController.searchProducts);

router.get("/:id", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, Middleware.checkInBlaklist("product", "id"), ProductController.getSingleProduct);

router.put("/", passport.authenticate("jwt", {session: false}), ProductController.updateProduct);

router.delete("/:id", passport.authenticate("jwt", {session: false}), ProductController.deleteProduct);

router.patch("/active/:id", passport.authenticate("jwt", {session: false}), ProductController.switchActive);

router.use(reviewRouter("product"));

router.use("/seller/", reviewRouter("seller"));

export = router;
