const express = require("express");
const router = express.Router();

import { BookmarksController } from "./controller";
import passport = require("passport");

router.post("/", passport.authenticate("jwt", {session: false}), BookmarksController.addBookmark);

router.delete("/:type/:id", passport.authenticate("jwt", {session: false}), BookmarksController.removeBookmark);

router.get("/", passport.authenticate("jwt", {session: false}), BookmarksController.getBookmarks);

export = router;
