export const getBookmarkListRawQuery = (userId: string, limit: number, offset: number, search?: string): string => {
  // const rawQuery = `
  //   select
  //   "bookmarks"."id" as "bookmarks_id",
  //   "bookmarks"."created" as "bookmarks_created",
  //   "bookmarks"."owner_id" as "bookmarks_owner_id",
  //   "bookmarks"."account_id" as "bookmarks_account_id",
  //   "bookmarks"."text_id" as "bookmarks_text_id",
  //   "bookmarks"."photo_id" as "bookmarks_photo_id",
  //   "bookmarks"."video_id" as "bookmarks_video_id",
  //   "bookmarks"."article_id" as "bookmarks_article_id",
  //   "bookmarks"."product_id" as "bookmarks_product_id",
  //   "bookmarks"."service_id" as "bookmarks_service_id",
  //   "owner"."id" as "owner_id",
  //   "account"."id" as "account_id",
  //   "account_person"."id" as "account_person_id",
  //   "account_person"."thumb" as "account_person_thumb",
  //   "account_person"."pageName" as "account_person_pageName",
  //   "account_person"."fullName" as "account_person_fullName",
  //   "account_business"."id" as "account_business_id",
  //   "account_business"."thumb" as "account_business_thumb",
  //   "account_business"."name" as "account_business_name",
  //   "account_business"."pageName" as "account_business_pageName",
  //   "photo"."id" as "photo_id",
  //   "photo"."thumb" as "photo_thumb",
  //   "photo"."title" as "photo_title",
  //   "photo_person"."id" as "photo_person_id",
  //   "photo_account_person"."id" as "photo_account_person_id",
  //   "photo_account_person"."thumb" as "photo_account_person_thumb",
  //   "photo_account_person"."pageName" as "photo_account_person_pageName",
  //   "photo_account_person"."fullName" as "photo_account_person_fullName",
  //   "photo_account_business"."id" as "photo_account_business_id",
  //   "photo_account_business"."thumb" as "photo_account_business_thumb",
  //   "photo_account_business"."name" as "photo_account_business_name",
  //   "photo_account_business"."pageName" as "photo_account_business_pageName",
  //   "photo_likes"."id" as "photo_likes_id",
  //   "photo_likes"."created" as "photo_likes_created",
  //   "photo_likes"."updated" as "photo_likes_updated",
  //   "photo_likes"."user_id" as "photo_likes_user_id",
  //   "photo_likes"."photo_id" as "photo_likes_photo_id",
  //   "video"."id" as "video_id",
  //   "video"."thumb" as "video_thumb",
  //   "video"."title" as "video_title",
  //   "video_person"."id" as "video_person_id",
  //   "video_account_person"."id" as "video_account_person_id",
  //   "video_account_person"."thumb" as "video_account_person_thumb",
  //   "video_account_person"."pageName" as "video_account_person_pageName",
  //   "video_account_person"."fullName" as "video_account_person_fullName",
  //   "video_account_business"."id" as "video_account_business_id",
  //   "video_account_business"."thumb" as "video_account_business_thumb",
  //   "video_account_business"."name" as "video_account_business_name",
  //   "video_account_business"."pageName" as "video_account_business_pageName",
  //   "video_likes"."id" as "video_likes_id",
  //   "video_likes"."created" as "video_likes_created",
  //   "video_likes"."updated" as "video_likes_updated",
  //   "video_likes"."user_id" as "video_likes_user_id",
  //   "video_likes"."video_id" as "video_likes_video_id",
  //   "article"."id" as "article_id",
  //   "article"."thumb" as "article_thumb",
  //   "article"."title" as "article_title",
  //   "article_person"."id" as "article_person_id",
  //   "article_account_person"."id" as "article_account_person_id",
  //   "article_account_person"."thumb" as "article_account_person_thumb",
  //   "article_account_person"."pageName" as "article_account_person_pageName",
  //   "article_account_person"."fullName" as "article_account_person_fullName",
  //   "article_account_business"."id" as "article_account_business_id",
  //   "article_account_business"."thumb" as "article_account_business_thumb",
  //   "article_account_business"."name" as "article_account_business_name",
  //   "article_account_business"."pageName" as "article_account_business_pageName",
  //   "article_likes"."id" as "article_likes_id",
  //   "article_likes"."created" as "article_likes_created",
  //   "article_likes"."user_id" as "article_likes_user_id",
  //   "article_likes"."article_id" as "article_likes_article_id",
  //   "text"."id" as "text_id",
  //   "text"."text" as "text_text",
  //   "text_person"."id" as "text_person_id",
  //   "text_account_person"."id" as "text_account_person_id",
  //   "text_account_person"."thumb" as "text_account_person_thumb",
  //   "text_account_person"."pageName" as "text_account_person_pageName",
  //   "text_account_person"."fullName" as "text_account_person_fullName",
  //   "text_account_business"."id" as "text_account_business_id",
  //   "text_account_business"."thumb" as "text_account_business_thumb",
  //   "text_account_business"."name" as "text_account_business_name",
  //   "text_account_business"."pageName" as "text_account_business_pageName",
  //   "text_likes"."id" as "text_likes_id",
  //   "text_likes"."created" as "text_likes_created",
  //   "text_likes"."updated" as "text_likes_updated",
  //   "text_likes"."user_id" as "text_likes_user_id",
  //   "text_likes"."text_id" as "text_likes_text_id",
  //   "product"."id" as "product_id",
  //   "product"."type" as "product_type",
  //   "product"."title" as "product_title",
  //   "product"."price" as "product_price",
  //   "product"."active" as "product_active",
  //   "product"."created" as "product_created",
  //   "product_photos"."id" as "product_photos_id",
  //   "product_photos"."photoUrl" as "product_photos_photoUrl",
  //   "product_photos"."thumbUrl" as "product_photos_thumbUrl",
  //   "product_photos"."order" as "product_photos_order",
  //   "product_photos"."created" as "product_photos_created",
  //   "product_photos"."deleted" as "product_photos_deleted",
  //   "product_photos"."product_id" as "product_photos_product_id",
  //   "product_variants"."id" as "product_variants_id",
  //   "product_variants"."size" as "product_variants_size",
  //   "product_variants"."color" as "product_variants_color",
  //   "product_variants"."flavor" as "product_variants_flavor",
  //   "product_variants"."material" as "product_variants_material",
  //   "product_variants"."price" as "product_variants_price",
  //   "product_variants"."quantity" as "product_variants_quantity",
  //   "product_variants"."enable" as "product_variants_enable",
  //   "product_variants"."created" as "product_variants_created",
  //   "product_variants"."updated" as "product_variants_updated",
  //   "product_variants"."deleted" as "product_variants_deleted",
  //   "product_variants"."product_id" as "product_variants_product_id",
  //   "variants_to_photos"."variantId" as "variants_to_photos_variantId",
  //   "variants_to_photos"."photoId" as "variants_to_photos_photoId",
  //   "variants_to_photos"."order" as "variants_to_photos_order",
  //   "variants_to_photos"."productId" as "variants_to_photos_productId",
  //   "product_variants_photos"."id" as "product_variants_photos_id",
  //   "product_variants_photos"."photoUrl" as "product_variants_photos_photoUrl",
  //   "product_variants_photos"."thumbUrl" as "product_variants_photos_thumbUrl",
  //   "product_variants_photos"."order" as "product_variants_photos_order",
  //   "product_variants_photos"."created" as "product_variants_photos_created",
  //   "product_variants_photos"."deleted" as "product_variants_photos_deleted",
  //   "product_variants_photos"."product_id" as "product_variants_photos_product_id",
  //   "product_person"."id" as "product_person_id",
  //   "product_account_person"."id" as "product_account_person_id",
  //   "product_account_person"."thumb" as "product_account_person_thumb",
  //   "product_account_person"."pageName" as "product_account_person_pageName",
  //   "product_account_person"."fullName" as "product_account_person_fullName",
  //   "product_account_business"."id" as "product_account_business_id",
  //   "product_account_business"."thumb" as "product_account_business_thumb",
  //   "product_account_business"."name" as "product_account_business_name",
  //   "product_account_business"."pageName" as "product_account_business_pageName",
  //   "service"."id" as "service_id",
  //   "service"."active" as "service_active",
  //   "service"."title" as "service_title",
  //   "service"."price" as "service_price",
  //   "service"."quantity" as "service_quantity",
  //   "service"."hourlyPrice" as "service_hourlyPrice",
  //   "service_schedule"."id" as "service_schedule_id",
  //   "service_schedule"."duration" as "service_schedule_duration",
  //   "service_schedule"."break" as "service_schedule_break",
  //   "service_schedule"."created" as "service_schedule_created",
  //   "service_schedule"."updated" as "service_schedule_updated",
  //   "service_schedule"."deleted" as "service_schedule_deleted",
  //   "service_schedule"."service_id" as "service_schedule_service_id",
  //   "service_schedule_bydate"."id" as "service_schedule_bydate_id",
  //   "service_schedule_bydate"."from" as "service_schedule_bydate_from",
  //   "service_schedule_bydate"."to" as "service_schedule_bydate_to",
  //   "service_schedule_bydate"."bookings" as "service_schedule_bydate_bookings",
  //   "service_schedule_bydate"."created" as "service_schedule_bydate_created",
  //   "service_schedule_bydate"."updated" as "service_schedule_bydate_updated",
  //   "service_schedule_bydate"."deleted" as "service_schedule_bydate_deleted",
  //   "service_schedule_bydate"."scheduleId" as "service_schedule_bydate_scheduleId",
  //   "service_photos"."id" as "service_photos_id",
  //   "service_photos"."link" as "service_photos_link",
  //   "service_photos"."thumb" as "service_photos_thumb",
  //   "service_photos"."index" as "service_photos_index",
  //   "service_photos"."created" as "service_photos_created",
  //   "service_photos"."updated" as "service_photos_updated",
  //   "service_photos"."deleted" as "service_photos_deleted",
  //   "service_photos"."service_id" as "service_photos_service_id",
  //   "service_person"."id" as "service_person_id",
  //   "service_account_person"."id" as "service_account_person_id",
  //   "service_account_person"."thumb" as "service_account_person_thumb",
  //   "service_account_person"."pageName" as "service_account_person_pageName",
  //   "service_account_person"."fullName" as "service_account_person_fullName",
  //   "service_account_business"."id" as "service_account_business_id",
  //   "service_account_business"."thumb" as "service_account_business_thumb",
  //   "service_account_business"."name" as "service_account_business_name",
  //   "service_account_business"."pageName" as "service_account_business_pageName",
  //   (case
  //     when "product"."price" is not null then "product"."price"
  //     else "product_variants"."price"
  //   end) as "product_price_conditional"
  // from
  //   "t_bookmarks" "bookmarks"
  // left join "t_account" "owner" on
  //   "owner"."id" = "bookmarks"."owner_id"
  // left join "t_account" "account" on
  //   "account"."id" = "bookmarks"."account_id"
  // left join "t_person" "account_person" on
  //   "account_person"."id" = "account"."personId"
  // left join "t_business" "account_business" on
  //   "account_business"."id" = "account"."businessId"
  // left join "t_photo" "photo" on
  //   "photo"."id" = "bookmarks"."photo_id"
  // left join "t_account" "photo_person" on
  //   "photo_person"."id" = "photo"."user_id"
  // left join "t_person" "photo_account_person" on
  //   "photo_account_person"."id" = "photo_person"."personId"
  // left join "t_business" "photo_account_business" on
  //   "photo_account_business"."id" = "photo_person"."businessId"
  // left join "t_photo_like" "photo_likes" on
  //   "photo_likes"."photo_id" = "photo"."id"
  //   and ("photo_likes"."user_id" = ${userId})
  // left join "t_video" "video" on
  //   "video"."id" = "bookmarks"."video_id"
  // left join "t_account" "video_person" on
  //   "video_person"."id" = "video"."user_id"
  // left join "t_person" "video_account_person" on
  //   "video_account_person"."id" = "video_person"."personId"
  // left join "t_business" "video_account_business" on
  //   "video_account_business"."id" = "video_person"."businessId"
  // left join "t_video_like" "video_likes" on
  //   "video_likes"."video_id" = "video"."id"
  //   and ("video_likes"."user_id" = ${userId})
  // left join "t_article" "article" on
  //   "article"."id" = "bookmarks"."article_id"
  // left join "t_account" "article_person" on
  //   "article_person"."id" = "article"."user_id"
  // left join "t_person" "article_account_person" on
  //   "article_account_person"."id" = "article_person"."personId"
  // left join "t_business" "article_account_business" on
  //   "article_account_business"."id" = "article_person"."businessId"
  // left join "t_article_like" "article_likes" on
  //   "article_likes"."article_id" = "article"."id"
  //   and ("article_likes"."user_id" = ${userId})
  // left join "t_text" "text" on
  //   "text"."id" = "bookmarks"."text_id"
  // left join "t_account" "text_person" on
  //   "text_person"."id" = "text"."user_id"
  // left join "t_person" "text_account_person" on
  //   "text_account_person"."id" = "text_person"."personId"
  // left join "t_business" "text_account_business" on
  //   "text_account_business"."id" = "text_person"."businessId"
  // left join "t_text_like" "text_likes" on
  //   "text_likes"."text_id" = "text"."id"
  //   and ("text_likes"."user_id" = ${userId})
  // left join "t_product" "product" on
  //   "product"."id" = "bookmarks"."product_id"
  // left join "t_product_photo" "product_photos" on
  //   "product_photos"."product_id" = "product"."id"
  //   and ("product"."type" = 'singleProduct'
  //   and "product_photos"."deleted" is null
  //   and "product_photos"."order" = '0')
  // left join "t_product_variant" "product_variants" on
  //   "product_variants"."id" = (
  //   select
  //     pv.id
  //   from
  //     t_product_variant pv
  //   where
  //     pv.product_id = "product"."id"
  //     and pv.enable = true
  //   order by
  //     pv.price, pv.id
  //   limit 1 )
  // left join "t_product_variant_t_product_photos" "variants_to_photos" on
  //   "variants_to_photos"."variantId" = "product_variants"."id"
  //   and ("variants_to_photos"."order" = 0)
  // left join "t_product_photo" "product_variants_photos" on
  //   "product_variants_photos"."id" = "variants_to_photos"."photoId"
  // left join "t_account" "product_person" on
  //   "product_person"."id" = "product"."user_id"
  // left join "t_person" "product_account_person" on
  //   "product_account_person"."id" = "product_person"."personId"
  // left join "t_business" "product_account_business" on
  //   "product_account_business"."id" = "product_person"."businessId"
  // left join "t_service" "service" on
  //   "service"."id" = "bookmarks"."service_id"
  // left join "t_service_schedule" "service_schedule" on
  //   "service_schedule"."service_id" = "service"."id"
  // left join "t_schedule_date" "service_schedule_bydate" on
  //   "service_schedule_bydate"."scheduleId" = "service_schedule"."id"
  // left join "t_service_photo" "service_photos" on
  //   "service_photos"."service_id" = "service"."id"
  //   and ("service_photos"."index" = 0)
  // left join "t_account" "service_person" on
  //   "service_person"."id" = "service"."user_id"
  // left join "t_person" "service_account_person" on
  //   "service_account_person"."id" = "service_person"."personId"
  // left join "t_business" "service_account_business" on
  //   "service_account_business"."id" = "service_person"."businessId"
  // where
  //   bookmarks."owner_id" = ${userId}
  //   ${search ? `and (
  //   LOWER ("photo"."title") SIMILAR TO  '%${search}%'
  //   OR LOWER ("video"."title") SIMILAR TO  '%${search}%'
  //   OR LOWER ("article"."title") SIMILAR TO  '%${search}%'
  //   OR LOWER ("product"."title") SIMILAR TO  '%${search}%'
  //   OR LOWER ("service"."title") SIMILAR TO  '%${search}%'
  //   OR LOWER ("account_person"."pageName") SIMILAR TO  '%${search}%'
  //   OR LOWER ("account_business"."pageName") SIMILAR TO  '%${search}%'
  //   )` : ''}
  // order by
  //   "bookmarks"."created" desc
  // limit ${limit}
  // offset ${offset}
  // `;
  const rawQuery = `
    select
    "bookmarks"."id" as "bookmarks_id",
    "bookmarks"."created" as "bookmarks_created",
    "bookmarks"."owner_id" as "bookmarks_owner_id",
    "bookmarks"."account_id" as "bookmarks_account_id",
    "bookmarks"."text_id" as "bookmarks_text_id",
    "bookmarks"."photo_id" as "bookmarks_photo_id",
    "bookmarks"."video_id" as "bookmarks_video_id",
    "bookmarks"."article_id" as "bookmarks_article_id",
    "bookmarks"."product_id" as "bookmarks_product_id",
    "bookmarks"."service_id" as "bookmarks_service_id",
    "owner"."id" as "owner_id",
    "account"."id" as "account_id",
    "account_person"."id" as "account_person_id",
    "account_person"."thumb" as "account_person_thumb",
    "account_person"."pageName" as "account_person_pageName",
    "account_person"."fullName" as "account_person_fullName",
    "account_business"."id" as "account_business_id",
    "account_business"."thumb" as "account_business_thumb",
    "account_business"."name" as "account_business_name",
    "account_business"."pageName" as "account_business_pageName",
    "photo"."id" as "photo_id",
    "photo"."thumb" as "photo_thumb",
    "photo"."title" as "photo_title",
    "photo_person"."id" as "photo_person_id",
    "photo_account_person"."id" as "photo_account_person_id",
    "photo_account_person"."thumb" as "photo_account_person_thumb",
    "photo_account_person"."pageName" as "photo_account_person_pageName",
    "photo_account_person"."fullName" as "photo_account_person_fullName",
    "photo_account_business"."id" as "photo_account_business_id",
    "photo_account_business"."thumb" as "photo_account_business_thumb",
    "photo_account_business"."name" as "photo_account_business_name",
    "photo_account_business"."pageName" as "photo_account_business_pageName",
    "photo_likes"."id" as "photo_likes_id",
    "photo_likes"."created" as "photo_likes_created",
    "photo_likes"."updated" as "photo_likes_updated",
    "photo_likes"."user_id" as "photo_likes_user_id",
    "photo_likes"."photo_id" as "photo_likes_photo_id",
    "video"."id" as "video_id",
    "video"."thumb" as "video_thumb",
    "video"."cover" as "video_cover",
    "video"."duration" as "video_duration",
    "video"."title" as "video_title",
    "video_person"."id" as "video_person_id",
    "video_account_person"."id" as "video_account_person_id",
    "video_account_person"."thumb" as "video_account_person_thumb",
    "video_account_person"."pageName" as "video_account_person_pageName",
    "video_account_person"."fullName" as "video_account_person_fullName",
    "video_account_business"."id" as "video_account_business_id",
    "video_account_business"."thumb" as "video_account_business_thumb",
    "video_account_business"."name" as "video_account_business_name",
    "video_account_business"."pageName" as "video_account_business_pageName",
    "video_likes"."id" as "video_likes_id",
    "video_likes"."created" as "video_likes_created",
    "video_likes"."updated" as "video_likes_updated",
    "video_likes"."user_id" as "video_likes_user_id",
    "video_likes"."video_id" as "video_likes_video_id",
    "article"."id" as "article_id",
    "article"."thumb" as "article_thumb",
    "article"."title" as "article_title",
    "article_person"."id" as "article_person_id",
    "article_account_person"."id" as "article_account_person_id",
    "article_account_person"."thumb" as "article_account_person_thumb",
    "article_account_person"."pageName" as "article_account_person_pageName",
    "article_account_person"."fullName" as "article_account_person_fullName",
    "article_account_business"."id" as "article_account_business_id",
    "article_account_business"."thumb" as "article_account_business_thumb",
    "article_account_business"."name" as "article_account_business_name",
    "article_account_business"."pageName" as "article_account_business_pageName",
    "article_likes"."id" as "article_likes_id",
    "article_likes"."created" as "article_likes_created",
    "article_likes"."user_id" as "article_likes_user_id",
    "article_likes"."article_id" as "article_likes_article_id",
    "text"."id" as "text_id",
    "text"."text" as "text_text",
    "text_person"."id" as "text_person_id",
    "text_account_person"."id" as "text_account_person_id",
    "text_account_person"."thumb" as "text_account_person_thumb",
    "text_account_person"."pageName" as "text_account_person_pageName",
    "text_account_person"."fullName" as "text_account_person_fullName",
    "text_account_business"."id" as "text_account_business_id",
    "text_account_business"."thumb" as "text_account_business_thumb",
    "text_account_business"."name" as "text_account_business_name",
    "text_account_business"."pageName" as "text_account_business_pageName",
    "text_likes"."id" as "text_likes_id",
    "text_likes"."created" as "text_likes_created",
    "text_likes"."updated" as "text_likes_updated",
    "text_likes"."user_id" as "text_likes_user_id",
    "text_likes"."text_id" as "text_likes_text_id",

    "product"."id" as "product_id",
    "product"."type" as "product_type",
    "product"."title" as "product_title",
    "product"."price" as "product_price",
    "product"."active" as "product_active",
    "product"."created" as "product_created",
    "product_cover"."photoUrl" as "product_cover_photoUrl",
	  "product_cover"."thumbUrl" as "product_cover_thumbUrl",
    "product_person"."id" as "product_person_id",
    "product_account_person"."id" as "product_account_person_id",
    "product_account_person"."thumb" as "product_account_person_thumb",
    "product_account_person"."pageName" as "product_account_person_pageName",
    "product_account_person"."fullName" as "product_account_person_fullName",
    "product_account_business"."id" as "product_account_business_id",
    "product_account_business"."thumb" as "product_account_business_thumb",
    "product_account_business"."name" as "product_account_business_name",
    "product_account_business"."pageName" as "product_account_business_pageName",

    "service"."id" as "service_id",
    "service"."active" as "service_active",
    "service"."title" as "service_title",
    "service"."price" as "service_price",
    "service"."quantity" as "service_quantity",
    "service"."hourlyPrice" as "service_hourlyPrice",
    "service_schedule"."id" as "service_schedule_id",
    "service_schedule"."duration" as "service_schedule_duration",
    "service_schedule"."break" as "service_schedule_break",
    "service_schedule"."created" as "service_schedule_created",
    "service_schedule"."updated" as "service_schedule_updated",
    "service_schedule"."deleted" as "service_schedule_deleted",
    "service_schedule"."service_id" as "service_schedule_service_id",
    "service_schedule_bydate"."id" as "service_schedule_bydate_id",
    "service_schedule_bydate"."from" as "service_schedule_bydate_from",
    "service_schedule_bydate"."to" as "service_schedule_bydate_to",
    "service_schedule_bydate"."bookings" as "service_schedule_bydate_bookings",
    "service_schedule_bydate"."created" as "service_schedule_bydate_created",
    "service_schedule_bydate"."updated" as "service_schedule_bydate_updated",
    "service_schedule_bydate"."deleted" as "service_schedule_bydate_deleted",
    "service_schedule_bydate"."scheduleId" as "service_schedule_bydate_scheduleId",
    "service_photos"."id" as "service_photos_id",
    "service_photos"."link" as "service_photos_link",
    "service_photos"."thumb" as "service_photos_thumb",
    "service_photos"."index" as "service_photos_index",
    "service_photos"."created" as "service_photos_created",
    "service_photos"."updated" as "service_photos_updated",
    "service_photos"."deleted" as "service_photos_deleted",
    "service_photos"."service_id" as "service_photos_service_id",
    "service_person"."id" as "service_person_id",
    "service_account_person"."id" as "service_account_person_id",
    "service_account_person"."thumb" as "service_account_person_thumb",
    "service_account_person"."pageName" as "service_account_person_pageName",
    "service_account_person"."fullName" as "service_account_person_fullName",
    "service_account_business"."id" as "service_account_business_id",
    "service_account_business"."thumb" as "service_account_business_thumb",
    "service_account_business"."name" as "service_account_business_name",
    "service_account_business"."pageName" as "service_account_business_pageName"
  from
    "t_bookmarks" "bookmarks"
  left join "t_account" "owner" on
    "owner"."id" = "bookmarks"."owner_id"
  left join "t_account" "account" on
    "account"."id" = "bookmarks"."account_id"
  left join "t_person" "account_person" on
    "account_person"."id" = "account"."personId"
  left join "t_business" "account_business" on
    "account_business"."id" = "account"."businessId"
  left join "t_photo" "photo" on
    "photo"."id" = "bookmarks"."photo_id"
  left join "t_account" "photo_person" on
    "photo_person"."id" = "photo"."user_id"
  left join "t_person" "photo_account_person" on
    "photo_account_person"."id" = "photo_person"."personId"
  left join "t_business" "photo_account_business" on
    "photo_account_business"."id" = "photo_person"."businessId"
  left join "t_photo_like" "photo_likes" on
    "photo_likes"."photo_id" = "photo"."id"
    and ("photo_likes"."user_id" = ${userId})
  left join "t_video" "video" on
    "video"."id" = "bookmarks"."video_id"
  left join "t_account" "video_person" on
    "video_person"."id" = "video"."user_id"
  left join "t_person" "video_account_person" on
    "video_account_person"."id" = "video_person"."personId"
  left join "t_business" "video_account_business" on
    "video_account_business"."id" = "video_person"."businessId"
  left join "t_video_like" "video_likes" on
    "video_likes"."video_id" = "video"."id"
    and ("video_likes"."user_id" = ${userId})
  left join "t_article" "article" on
    "article"."id" = "bookmarks"."article_id"
  left join "t_account" "article_person" on
    "article_person"."id" = "article"."user_id"
  left join "t_person" "article_account_person" on
    "article_account_person"."id" = "article_person"."personId"
  left join "t_business" "article_account_business" on
    "article_account_business"."id" = "article_person"."businessId"
  left join "t_article_like" "article_likes" on
    "article_likes"."article_id" = "article"."id"
    and ("article_likes"."user_id" = ${userId})
  left join "t_text" "text" on
    "text"."id" = "bookmarks"."text_id"
  left join "t_account" "text_person" on
    "text_person"."id" = "text"."user_id"
  left join "t_person" "text_account_person" on
    "text_account_person"."id" = "text_person"."personId"
  left join "t_business" "text_account_business" on
    "text_account_business"."id" = "text_person"."businessId"
  left join "t_text_like" "text_likes" on
    "text_likes"."text_id" = "text"."id"
    and ("text_likes"."user_id" = ${userId})
  left join "t_product" "product" on
    "product"."id" = "bookmarks"."product_id"
  left join "t_product_photo" "product_cover" on
    "product_cover"."id" = "product"."cover_id"
  left join "t_account" "product_person" on
    "product_person"."id" = "product"."user_id"
  left join "t_person" "product_account_person" on
    "product_account_person"."id" = "product_person"."personId"
  left join "t_business" "product_account_business" on
    "product_account_business"."id" = "product_person"."businessId"

    left join "t_service" "service" on
    "service"."id" = "bookmarks"."service_id"
  left join "t_service_schedule" "service_schedule" on
    "service_schedule"."service_id" = "service"."id"
  left join "t_schedule_date" "service_schedule_bydate" on
    "service_schedule_bydate"."scheduleId" = "service_schedule"."id"
  left join "t_service_photo" "service_photos" on
    "service_photos"."service_id" = "service"."id"
    and ("service_photos"."index" = 0)
  left join "t_account" "service_person" on
    "service_person"."id" = "service"."user_id"
  left join "t_person" "service_account_person" on
    "service_account_person"."id" = "service_person"."personId"
  left join "t_business" "service_account_business" on
    "service_account_business"."id" = "service_person"."businessId"
  where
    bookmarks."owner_id" = ${userId}
    ${search ? `and (
    LOWER ("photo"."title") SIMILAR TO  '%${search}%'
    OR LOWER ("video"."title") SIMILAR TO  '%${search}%'
    OR LOWER ("article"."title") SIMILAR TO  '%${search}%'
    OR LOWER ("product"."title") SIMILAR TO  '%${search}%'
    OR LOWER ("service"."title") SIMILAR TO  '%${search}%'
    OR LOWER ("account_person"."pageName") SIMILAR TO  '%${search}%'
    OR LOWER ("account_business"."pageName") SIMILAR TO  '%${search}%'
    )` : ''}
  order by
    "bookmarks"."created" desc
  limit ${limit}
  offset ${offset}
  `;
  return rawQuery
}
