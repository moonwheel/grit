import { getConnection, getRepository } from "typeorm";
import { Request, Response } from 'express'
import { BookmarkContentType } from '../../entities/bookmarks';
import { Text } from "../../entities/text";
import { Photo } from "../../entities/photo";
import { Video } from "../../entities/video";
import { Article } from "../../entities/article";
import { Product } from "../../entities/product";
import { Service } from "../../entities/service";
import getAccountRepository from "../../entities/repositories/account-repository";
import { Account } from "../../entities/account";
import { Bookmark } from "../../entities/bookmarks";
import { getBookmarkListRawQuery } from "./raw_queries";
import { sanitizeRawData } from "../../utils/helpers";


export type Content = Text |
    Photo |
    Text |
    Article |
    Video

export class BookmarksController {

    // static addBookmark = async(req, res) => {
    //     try {
    //         const {
    //             body: {
    //                 type,
    //                 id,
    //             },
    //             user,
    //         } = req;
    //         const bookmarks_type = await BookmarksController.checkExistence(type, id);
    //         if (bookmarks_type) {
    //             await getAccountRepository()
    //                 .createQueryBuilder()
    //                 .relation(Account, bookmarks_type)
    //                 .of(user.baseBusinessAccount || user.id)
    //                 .add(id);

    //             return res.status(200).send({status:"success"});
    //         }
    //         throw Error(`${type} not found`);
    //     } catch (error) {
    //         res.status(400).send({error: error.message});
    //     }
    // };


    static addBookmark = async (
        req: Request,
        res: Response,
    ) => {
        const {
            body: {
                type,
                id,
            },
            user,
        } = req;
        const bookmarkRepo = getRepository(Bookmark);
        try {
            const bookmarks_type = await BookmarksController.checkExistence(type, id);

            const bookmarkRepo = getRepository(Bookmark);

            const duplicateCheck = await bookmarkRepo.findOne({
                owner: user.baseBusinessAccount || user.id,
                [bookmarks_type]: id
            })

            if(duplicateCheck) return res.status(400).send('This bookmark already exists')


            if (bookmarks_type) {
                const newBookmark = {
                    created: new Date(),
                    // account: user.id,
                    owner: user.baseBusinessAccount || user.id,
                    [bookmarks_type]: id
                }

                const newNotification = await bookmarkRepo.save(newBookmark)
                return res.status(200).send({ status: "success" });

            }
            throw Error(`${type} not found`);

        } catch (error) {
            console.log('addBookmark  error', error)
            res.status(400).send({ error: error.message });
        }
    }


    static removeBookmark = async (req, res) => {
        try {
            const {
                params: {
                    type,
                    id,
                },
                user,
            } = req;
            const bookmarks_type = await BookmarksController.checkExistence(type, id);

            const bookmarkRepo = getRepository(Bookmark);

            const searchBookmark = await bookmarkRepo.findOne({
                owner: user.baseBusinessAccount || user.id,
                [bookmarks_type]: id
            })

            const deleted = await bookmarkRepo.delete(searchBookmark);

            if (deleted.affected) {
                res.status(200).send({ status: "success" });
            } else {
                res.status(404).send({ status: "bookmark not found" });
            }

        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };


    // static getBookmarks = async (req, res) => {
    //     try {
    //         const bookmarkList: any = ["bookmarks_user", "bookmarks_text", "bookmarks_photo", "bookmarks_article", "bookmarks_video", "bookmarks_product", "bookmarks_service"];
    //         const user = await getAccountRepository().findOne(
    //             req.user.baseBusinessAccount || req.user.id,
    //             {
    //                 relations: bookmarkList,
    //             }
    //         );
    //         res.status(200).send({
    //             users: user.bookmarks_user,
    //             texts: user.bookmarks_text,
    //             photos: user.bookmarks_photo,
    //             articles: user.bookmarks_article,
    //             videos: user.bookmarks_video,
    //             products: user.bookmarks_product,
    //             services: user.bookmarks_service,
    //         });
    //     } catch (error) {
    //         res.status(400).send({ error: error.message });
    //     }
    // };

    static getBookmarks = async (
        req: Request,
        res: Response,
    ) => {
        const {
            query: {
                page = 1,
                limit = 15,
                search = null
            }

        } = req;

        const userId = req.user.baseBusinessAccount || req.user.id

        try {
            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;

            // let query = getRepository(Bookmark)
            //     .createQueryBuilder("bookmarks")

            //     .leftJoin('bookmarks.owner', "owner")
            //     .addSelect("owner.id")

            //     .leftJoin('bookmarks.account', "account")
            //     .addSelect("account.id")
            //     .leftJoin('account.person', "account_person")
            //     .addSelect("account_person.id")
            //     .addSelect("account_person.thumb")
            //     .addSelect("account_person.fullName")
            //     .addSelect("account_person.pageName")
            //     .leftJoin("account.business", "account_business")
            //     .addSelect("account_business.id")
            //     .addSelect("account_business.thumb")
            //     .addSelect("account_business.name")
            //     .addSelect("account_business.pageName")

            //     .leftJoin('bookmarks.photo', "photo")
            //     .addSelect("photo.thumb")
            //     .addSelect("photo.title")
            //     .addSelect("photo.id")
            //     .leftJoin('photo.user', "photo_person")
            //     .addSelect("photo_person.id")
            //     .leftJoin('photo_person.person', "photo_account_person")
            //     .addSelect("photo_account_person.id")
            //     .addSelect("photo_account_person.thumb")
            //     .addSelect("photo_account_person.fullName")
            //     .addSelect("photo_account_person.pageName")
            //     .leftJoin('photo_person.business', "photo_account_business")
            //     .addSelect("photo_account_business.id")
            //     .addSelect("photo_account_business.thumb")
            //     .addSelect("photo_account_business.name")
            //     .addSelect("photo_account_business.pageName")
            //     .leftJoinAndMapOne(`photo.liked`, `photo.likes`, `photo_likes`, `photo_likes.user_id = :user`, {user: req.user.id})


            //     .leftJoin('bookmarks.video', "video")
            //     .addSelect("video.id")
            //     .addSelect("video.thumb")
            //     .addSelect("video.title")
            //     .leftJoin('video.user', "video_person")
            //     .addSelect("video_person.id")
            //     .leftJoin('video_person.person', "video_account_person")
            //     .addSelect("video_account_person.id")
            //     .addSelect("video_account_person.thumb")
            //     .addSelect("video_account_person.fullName")
            //     .addSelect("video_account_person.pageName")
            //     .leftJoin('video_person.business', "video_account_business")
            //     .addSelect("video_account_business.id")
            //     .addSelect("video_account_business.thumb")
            //     .addSelect("video_account_business.name")
            //     .addSelect("video_account_business.pageName")
            //     .leftJoinAndMapOne(`video.liked`, `video.likes`, `video_likes`, `video_likes.user_id = :user`, {user: req.user.id})

            //     .leftJoin('bookmarks.article', "article")
            //     .addSelect("article.id")
            //     .addSelect("article.thumb")
            //     .addSelect("article.title")
            //     .leftJoin('article.user', "article_person")
            //     .addSelect("article_person.id")
            //     .leftJoin('article_person.person', "article_account_person")
            //     .addSelect("article_account_person.id")
            //     .addSelect("article_account_person.thumb")
            //     .addSelect("article_account_person.fullName")
            //     .addSelect("article_account_person.pageName")
            //     .leftJoin('article_person.business', "article_account_business")
            //     .addSelect("article_account_business.id")
            //     .addSelect("article_account_business.thumb")
            //     .addSelect("article_account_business.name")
            //     .addSelect("article_account_business.pageName")
            //     .leftJoinAndMapOne(`article.liked`, `article.likes`, `article_likes`, `article_likes.user_id = :user`, {user: req.user.id})


            //     .leftJoin('bookmarks.text', "text")
            //     .addSelect("text.id")
            //     .addSelect("text.text")
            //     .leftJoin('text.user', "text_person")
            //     .addSelect("text_person.id")
            //     .leftJoin('text_person.person', "text_account_person")
            //     .addSelect("text_account_person.id")
            //     .addSelect("text_account_person.thumb")
            //     .addSelect("text_account_person.fullName")
            //     .addSelect("text_account_person.pageName")
            //     .leftJoin('text_person.business', "text_account_business")
            //     .addSelect("text_account_business.id")
            //     .addSelect("text_account_business.thumb")
            //     .addSelect("text_account_business.name")
            //     .addSelect("text_account_business.pageName")
            //     .leftJoinAndMapOne(`text.liked`, `text.likes`, `text_likes`, `text_likes.user_id = :user`, {user: req.user.id})


            //     .leftJoin('bookmarks.product', "product")
            //     .addSelect("product.id")
            //     .addSelect("product.title")
            //     .addSelect("product.active")
            //     .addSelect("product.type")
            //     .addSelect("product.price")
            //     .addSelect("product.created")
            //     // .addSelect(`(CASE WHEN "product"."price" IS NOT NULL THEN "product"."price"
            //     //     ELSE "product_variants"."price"
            //     //     END)`, "product_price_conditional"
            //     // )
            //     // .leftJoinAndSelect("product.photos", "product_photos", `product.type = 'singleProduct' AND product_photos.deleted IS NULL AND product_photos.order = '0'`)
            //     // .leftJoinAndMapOne("product.cheapestVariant", "t_product_variant", "product_variants", `
            //     //     product_variants.id = (
            //     //         SELECT pv.id
            //     //         FROM t_product_variant pv
            //     //         WHERE pv.product_id = product.id AND pv.enable = true
            //     //         ORDER BY pv.price, pv.id
            //     //         LIMIT 1
            //     //     )
            //     // `)
            //     // .leftJoinAndMapOne("product_variants.cover","product_variants.photos", "variants_to_photos", "variants_to_photos.order = 0")
            //     // .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos")
            //     .leftJoin('product.cover_', 'product_cover')
            //     .addSelect(`product_cover.photoUrl`)
            //     .addSelect(`product_cover.thumbUrl`)


            //     .leftJoin('product.user', "product_person")
            //     .addSelect("product_person.id")
            //     .leftJoin('product_person.person', "product_account_person")
            //     .addSelect("product_account_person.id")
            //     .addSelect("product_account_person.thumb")
            //     .addSelect("product_account_person.fullName")
            //     .addSelect("product_account_person.pageName")
            //     .leftJoin('product_person.business', "product_account_business")
            //     .addSelect("product_account_business.id")
            //     .addSelect("product_account_business.thumb")
            //     .addSelect("product_account_business.name")
            //     .addSelect("product_account_business.pageName")


            //     .leftJoin('bookmarks.service', "service")
            //     .addSelect("service.id")
            //     .addSelect("service.active")
            //     .addSelect("service.title")
            //     .addSelect("service.price")
            //     .addSelect("service.quantity")
            //     .addSelect("service.hourlyPrice")
            //     .leftJoinAndSelect("service.schedule", "service_schedule")
            //     .leftJoinAndSelect("service_schedule.byDate", "service_schedule_bydate")
            //     .leftJoinAndMapOne("service.photoCover", "service.photos", "service_photos", "service_photos.index = 0")
            //     .leftJoin('service.user', "service_person")
            //     .addSelect("service_person.id")
            //     .leftJoin('service_person.person', "service_account_person")
            //     .addSelect("service_account_person.id")
            //     .addSelect("service_account_person.thumb")
            //     .addSelect("service_account_person.fullName")
            //     .addSelect("service_account_person.pageName")
            //     .leftJoin('service_person.business', "service_account_business")
            //     .addSelect("service_account_business.id")
            //     .addSelect("service_account_business.thumb")
            //     .addSelect("service_account_business.name")
            //     .addSelect("service_account_business.pageName")


            //     .where(`bookmarks."owner_id" = :owner_id `, { owner_id: req.user.id })

            //     .skip(skip)
            //     .take(limitNumb)
            //     .orderBy(`bookmarks.created`, `DESC`)

            // if (search) {
            //     query = query
            //         .andWhere(`
            //                 (LOWER (photo.title) SIMILAR TO  '%${search}%'
            //                 OR LOWER (video.title) SIMILAR TO  '%${search}%'
            //                 OR LOWER (article.title) SIMILAR TO  '%${search}%'
            //                 OR LOWER (product.title) SIMILAR TO  '%${search}%'
            //                 OR LOWER (service.title) SIMILAR TO  '%${search}%'
            //                 OR LOWER (account_person.pageName) SIMILAR TO  '%${search}%'
            //                 OR LOWER (account_business.pageName) SIMILAR TO  '%${search}%'
            //                 )
            //             `)

            // }

            // console.log('bookmarks query.getSql()', query.getSql() )

            // const userBookmarks = await query
            //     .getMany();
            // res.status(200).send(userBookmarks)

            const rawQuery = getBookmarkListRawQuery(userId, limitNumb, skip, search.toLowerCase())
            const userBookmarks = await getRepository(Bookmark).query(rawQuery)
            res.status(200).send({userBookmarks: sanitizeRawData(userBookmarks), raw: true})
        } catch (error) {
            console.log('getBookmarks error', error)
            res.status(400).send(error)
        }

    }


    static checkExistence = async (type, id) => {
        try {
            switch (type.toLowerCase()) {
                case "user": {
                    const repo = await getRepository(Account).findOne(id);
                    return repo ? "account" : null;
                }
                case "text": {
                    const repo = await getRepository(Text).findOne(id);
                    return repo ? type : null;
                }
                case "photo": {
                    const repo = await getRepository(Photo).findOne(id);
                    return repo ? type : null;
                }
                case "video": {
                    const repo = await getRepository(Video).findOne(id);
                    return repo ? type : null;
                }
                case "article": {
                    const repo = await getRepository(Article).findOne(id);
                    return repo ? type : null;
                }
                case "product": {
                    const repo = await getRepository(Product).findOne(id);
                    return repo ? type : null;
                }
                case "service": {
                    const repo = await getRepository(Service).findOne(id);
                    return repo ? type : null;
                }
                default: return null;
            }
        } catch (error) {
            throw error;
        }
    };
}
