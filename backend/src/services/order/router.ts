const express = require("express");
const router = express.Router();

import passport = require("passport");

import { OrderController } from "./controller";

router.get("/purchases", passport.authenticate("jwt", {session: false}), OrderController.getPurchasesList);

router.get("/sales", passport.authenticate("jwt", {session: false}), OrderController.getSalesList);

router.get("/:id", passport.authenticate("jwt", {session: false}), OrderController.getOrder);

router.post("/:id/cancel", passport.authenticate("jwt", {session: false}), OrderController.cancel);

router.post("/:id/ship", passport.authenticate("jwt", {session: false}), OrderController.ship);

router.post("/:id/return", passport.authenticate("jwt", {session: false}), OrderController.return);

router.post("/:id/refund", passport.authenticate("jwt", {session: false}), OrderController.refund);

export = router;
