import { getRepository } from 'typeorm';
import getOrderRepository from "../../entities/repositories/order-repository";
import getBookingRepository from "../../entities/repositories/booking-repository";
import { Product } from "../../entities/product";
import { ProductController } from "../product/controller";
import { NotificationController } from "../notifications/controller";
import { OrderProduct } from '../../entities/order_product';

export class OrderController {

    static getPurchasesList = async (req, res) => {
        try {
            const {
                user: {
                    id: userId
                },
                query: {
                    page = 1,
                    filter = 'all',
                    limit = 15,
                    search = '',
                },
            } = req;

            const orders = await getOrderRepository()
                .getOrders(userId, 'purchases', { page, limit, search, filter });

            const bookings = await getBookingRepository()
                .getBookings(userId, 'purchases', { page, limit, search, filter });

            const result = [
                ...orders[0].map(order => ({ type: 'order', ...order })),
                ...bookings[0].map(booking => ({ type: 'booking', ...booking })),
            ].sort((a ,b) => a.created < b.created ? 1 : -1);

            res.status(200).send({
                items: result,
                total: orders[1] + bookings[1],
            });
        } catch (error) {
            console.log('addToBookingList', error);
            res.status(400).send({ error: error.message });
        }
    }

    static getSalesList = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                query: {
                    page = 1,
                    filter = 'all',
                    limit = 15,
                    search = null,
                }
            } = req;
            const orders = await getOrderRepository()
                .getOrders(userId, 'sales', { page, limit, search, filter });

            const bookings = await getBookingRepository()
                .getBookings(userId, 'sales', { page, limit, search, filter });

            const result = [
                ...orders[0].map(order => ({ type: 'order', ...order })),
                ...bookings[0].map(booking => ({ type: 'booking', ...booking })),
            ].sort((a ,b) => a.created < b.created ? 1 : -1);

            res.status(200).send({
                items: result,
                total: orders[1] + bookings[1],
            });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getOrderList = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
            } = req;

            const orders = await getOrderRepository()
                .getOrders(userId);

            res.status(200).json({
                orders: orders[0],
                count: orders[1]
            });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getOrder = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: orderId
                },
            } = req;

            const order = await getOrderRepository().getOrder(userId, orderId);

            if (!order) {
                return res.status(404).json({ message: 'Not Found' });
            }

            res.status(200).json(order);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static ship = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: orderId,
                },
                body: {
                    items,
                },
            } = req;
            console.log('userId, orderId, items', userId, orderId, items)

            const data = await getOrderRepository()
                .ship(userId, orderId, items);

            const orderProductRepo = getRepository(OrderProduct);

            items.forEach(async (orderItemId) => {
                const orderProduct = await orderProductRepo.findOne(orderItemId, {
                    relations: ['product', 'product_variant', 'order', 'order.buyer', 'order.seller']
                })
                // console.log('orderProduct', orderProduct)
                await NotificationController.addNotification(
                    'shipped',
                    'product',
                    'product',
                    orderProduct.product,
                    orderProduct.order.seller,
                    orderProduct.order.buyer,
                    orderProduct.product_variant
                )
            });

            res.status(200).send(data);

        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    }

    static cancel = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: orderId,
                },
                body: {
                    items,
                    reason,
                    description,
                },
            } = req;
            console.log('cancel order ', userId, orderId, items, reason, description)

            const order = await getOrderRepository()
                .cancel(userId, orderId, items, reason, description);

            const orderProductRepo = getRepository(OrderProduct);

            items.forEach(async (orderItemId) => {
                const orderProduct = await orderProductRepo.findOne(orderItemId, {
                    relations: ['product', 'product_variant', 'order', 'order.buyer', 'order.seller']
                })
                console.log('orderProduct', orderProduct)
                await NotificationController.addNotification(
                    'cancelled',
                    'order',
                    'product',
                    orderProduct.product,
                    orderProduct.order.seller.id === userId ? orderProduct.order.seller : orderProduct.order.buyer,
                    orderProduct.order.seller.id === userId ? orderProduct.order.buyer : orderProduct.order.seller,
                    orderProduct.product_variant
                )
            });
            res.status(200).send(order);
        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    }

    static return = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: orderId,
                },
                body: {
                    items,
                    reason,
                    description,
                }
            } = req;

            const order = await getOrderRepository()
                .returnOrder(userId, orderId, items, reason, description);

            res.status(200).send(order);
        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    };

    static refund = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: orderId,
                },
                body: {
                    items,
                    payment = '',
                },
            } = req;

            const order = await getOrderRepository()
                .refund(userId, orderId, items, payment);

            res.status(200).send(order);
        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    }
}
