const express = require("express");
const router = express.Router();
import passport = require("passport");

import { CartController } from "./controller";

router.get("/", passport.authenticate("jwt", {session: false}), CartController.getCartProducts);

router.post("/", passport.authenticate("jwt", {session: false}), CartController.addProductToCart);

router.put("/:id", passport.authenticate("jwt", {session: false}), CartController.updateCartProduct);

router.delete("/:id", passport.authenticate("jwt", {session: false}), CartController.deleteCartProduct);

export = router;