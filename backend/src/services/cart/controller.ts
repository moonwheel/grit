import {getRepository, createQueryBuilder, getConnection} from "typeorm";
import {CartProduct} from "../../entities/cart_product";
import {Product} from "../../entities/product";
import {ProductController} from "../product/controller";
import {calculateTotalPrice} from "../../utils/helpers";

export class CartController {
    static getCartProducts = async (req, res) => {
        try {
            const { user: { id: userId } } = req;

            const query = getRepository(CartProduct)
                .createQueryBuilder("cart_product")
                .leftJoinAndSelect(`cart_product.product`, `product`)
                .addSelect('product.shopcategory_id', 'shop_cat_id')
                .leftJoin(`product.reviews`, `product_reviews`)
                .addSelect(`COUNT(DISTINCT product_reviews.id)`, `product_reviews_count`)
                .addSelect(`
                    (CASE
                        WHEN "product"."price" IS NOT NULL THEN "product"."price"
                        ELSE "product_variants"."price"
                    END)`,
                    "product_price_conditional"
                )
                .leftJoinAndSelect(
                    "product.photos",
                    "product_photos",
                    `product.type = 'singleProduct' AND product_photos.deleted IS NULL AND product_photos.order = '0'`,
                )
                .leftJoinAndMapOne(
                    "product.selectedVariant",
                    "t_product_variant",
                    "product_variants",
                    `product_variants.id = cart_product.product_variant_id`
                )
                .leftJoinAndMapOne(
                    "product_variants.cover",
                    "product_variants.photos",
                    "variants_to_photos",
                    "variants_to_photos.order = 0"
                )
                .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos")
                .leftJoinAndSelect("product.shopcategory_", "product_shopcategory")
                .leftJoinAndSelect("product.user", "product_user")
                .leftJoinAndSelect("product_user.business", "product_user_business")
                .leftJoinAndSelect("product_user.person", "product_user_person")
                .where("cart_product.user_id = :userId", { userId })
                .andWhere("product.deleted IS NULL")
                .groupBy(`
                    cart_product.*,
                    product.*,
                    product_photos.*,
                    product_variants.*,
                    variants_to_photos.*,
                    product_variants_photos.*,
                    product_shopcategory.*,
                    product_user.*,
                    product_user_business.*,
                    product_user_person.*
                `);

            const data = await query
                .andWhere("cart_product.user_id = :user_id", {user_id: userId})
                .orderBy("cart_product.id", "ASC")
                .getManyAndCount();

            const productsPrice = data[0].reduce((sum, item) => {
                const product = item.product as any;
                if (product.selectedVariant) {
                    return sum + (item.quantity * product.selectedVariant.price);
                }
                return sum + (item.quantity * product.price);
            }, 0);

            const shipping = await getConnection().query(`
                SELECT SUM(shipping)
                    FROM (
                        SELECT t_product.user_id,
                            MAX(CASE
                                WHEN t_cart_product.collected = false THEN t_product."shippingCosts"
                                ELSE 0
                        END) AS shipping
                    FROM t_cart_product
                        LEFT JOIN t_product ON t_cart_product.product_id = t_product.id
                    WHERE t_cart_product.user_id = ${userId}
                    GROUP BY t_product.user_id
                ) AS shippingCost;
            `);

            const shippingPrice = shipping.length ? Number(shipping[0].sum) : 0

            res.status(200).send({
                productsPrice,
                shippingPrice,
                totalPrice: productsPrice + shippingPrice,
                count: data[1],
                products: data[0]
            });
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static addProductToCart = async (req, res) => {
        try {
            const {
                user: {
                    id: userId
                },
                body: {
                    quantity = 1,
                    product_id,
                    product_variant_id
                }
            } = req;
            // TODO check if user can add to cart this product
            const productRepo = getRepository(Product);
            const product = await productRepo.findOne(product_id);
            // TODO check product variant
            if (!product) {
                return res.status(404).send({error: 'Not Found'});
            }

            const cartProductRepo = getRepository(CartProduct);
            const query = {
                where: {
                    user: userId,
                    product: product_id,
                    product_variant: null
                },
            };

            if (product_variant_id) {
                query.where.product_variant = product_variant_id;
            }

            let cartProduct = await cartProductRepo.findOne(query);

            if (!cartProduct) {
                await cartProductRepo.save({
                    user: userId,
                    quantity: quantity,
                    collected: product.deliveryOptions === 'onlyCollection',
                    product: product_id,
                    product_variant: product_variant_id || null
                }, {reload: true});
            } else {
                cartProduct.quantity = Number(cartProduct.quantity) + Number(quantity);
                await cartProductRepo.save(cartProduct);
            }
            return CartController.getCartProducts(req, res);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static updateCartProduct = async (req, res) => {
        try {
            const {body: {quantity, collected}} = req;
            const cartProductRepo = getRepository(CartProduct);
            const cartProduct = await cartProductRepo.findOne(Number(req.params.id));

            if (!cartProduct) {
                return res.status(404).send({error: 'Not Found'});
            }

            if (collected !== undefined) {
                cartProduct.collected = collected;
            }

            if (quantity > 0) {
                cartProduct.quantity = Number(quantity);
            }

            await cartProductRepo.save(cartProduct);

            return CartController.getCartProducts(req, res);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteCartProduct = async (req, res) => {
        try {
            const cartProductRepo = getRepository(CartProduct);
            await cartProductRepo.delete(req.params.id);
            return CartController.getCartProducts(req, res);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };
}
