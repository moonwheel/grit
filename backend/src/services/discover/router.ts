import * as express from "express";
const router = express.Router();

import passport = require("passport");
import { DiscoverController } from "./controller";

// TODO these routes are temporary unauthorized (add  passport.authenticate("jwt", {session: false}) to authenticate their)
// FOR EXAMPLE:
// router.get("/photos", passport.authenticate("jwt", {session: false}), DiscoverController.getPhotos);
router.get("/photos", passport.authenticate("jwt", {session: false}), DiscoverController.getPhotos);
router.get("/videos", passport.authenticate("jwt", {session: false}), DiscoverController.getVideos);
router.get("/articles", passport.authenticate("jwt", {session: false}), DiscoverController.getArticles);
router.get("/products", passport.authenticate("jwt", {session: false}), DiscoverController.getProducts);
router.get("/services", passport.authenticate("jwt", {session: false}), DiscoverController.getServices);

export = router;
