import { getRepository, Between } from 'typeorm';
import { App } from "../../config/app.config";
import { Photo } from '../../entities/photo';
import { Video } from '../../entities/video';
import { Article } from '../../entities/article';
import { Product } from '../../entities/product';
import { Service } from '../../entities/service';
import { Account } from '../../entities/account';
import { Address } from '../../entities/address';

export class DiscoverController {
    /**
     * Allowed types: account, article, photo, product, service, video, all
     */
    static getPhotos = async (req, res) => {
        const {
            category,
            limit = 30,
            page = 1
        } = req.query;
        try {
            const { limitNumb, skip } = DiscoverController._getPageParams(page, limit);
            const photoRepo = getRepository(Photo);
            let q1 = photoRepo
                .createQueryBuilder("photo")
                .leftJoin("photo.user", "photo_user")
                .addSelect("photo_user.id")
                .addSelect("photo_user.business")
                .leftJoin("photo_user.business", "photo_user_business")
                .addSelect("photo_user_business.id")
                .addSelect("photo_user_business.photo")
                .addSelect("photo_user_business.name")
                .addSelect("photo_user_business.pageName")
                .addSelect("photo_user.person")
                .leftJoin("photo_user.person", "photo_user_person")
                .addSelect("photo_user_person.id")
                .addSelect("photo_user_person.photo")
                .addSelect("photo_user_person.fullName")
                .addSelect("photo_user_person.pageName")
                .leftJoinAndSelect("photo.mentions", "photo_mention", `
                    photo_mention.photo_comment IS NULL
                    AND
                    photo_mention.photo_comment_reply IS NULL
                `)
                .leftJoinAndSelect("photo_mention.target", "photo_mention_target")
                .leftJoin("photo_mention_target.business", "photo_mention_target_business")
                .addSelect("photo_mention_target_business.id")
                .addSelect("photo_mention_target_business.photo")
                .addSelect("photo_mention_target_business.name")
                .addSelect("photo_mention_target_business.pageName")
                .leftJoin("photo_mention_target.person", "photo_mention_target_person")
                .addSelect("photo_mention_target_person.id")
                .addSelect("photo_mention_target_person.photo")
                .addSelect("photo_mention_target_person.fullName")
                .addSelect("photo_mention_target_person.pageName")
                .leftJoinAndSelect("photo.annotations", "photo_annotations")
                .leftJoinAndSelect("photo_annotations.target", "photo_annotations_target")
                .leftJoin("photo_annotations_target.business", "photo_annotations_target_business")
                .addSelect("photo_annotations_target_business.id")
                .addSelect("photo_annotations_target_business.photo")
                .addSelect("photo_annotations_target_business.name")
                .addSelect("photo_annotations_target_business.pageName")
                .leftJoin("photo_annotations_target.person", "photo_annotations_target_person")
                .addSelect("photo_annotations_target_person.id")
                .addSelect("photo_annotations_target_person.photo")
                .addSelect("photo_annotations_target_person.fullName")
                .addSelect("photo_annotations_target_person.pageName")
                .leftJoinAndMapOne(`photo.bookmarked`, `photo.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${req.user.baseBusinessAccount || req.user.id}`)
                .andWhere("photo.deleted IS NULL") // Don't show deleted photos
                .andWhere("photo.draft = FALSE") // Don't show drafts
                .andWhere("photo.user_id != :user", {user: (req.user.baseBusinessAccount || req.user.id)}) // Don't show your own photos
                .andWhere(qb => { // Don't show photos from your followings because it should be shown in feed
                    const subQuery = qb.subQuery()
                        .select("followings.followedAccount") // select all followed accounts
                        .from("t_followings", "followings")
                        .where("followings.account = :userId", { userId: req.user.id })
                        .getQuery();
                    return "photo.user_id NOT IN " + subQuery
                })

            if(category && category !== 'all') {
                q1.andWhere("LOWER(photo.category) = :category", {category: category.toLowerCase()})
            };

            const photos = await q1
                .orderBy("photo.created", "DESC")
                .take(limitNumb)
                .skip(skip)
                .getManyAndCount();
                // .getSql();
            // console.log('photos', photos)
            res.status(200).send({ items: photos[0], total: photos[1]})
        } catch (error) {
            console.log('error', error)
            res.status(400).send(error.message)
        }
    }

    static getVideos = async (req, res) => {
        const {
            category,
            limit = 30,
            page = 1
        } = req.query;
        try {
            const { limitNumb, skip } = DiscoverController._getPageParams(page, limit);
            const videoRepo = getRepository(Video);

            let q1 = videoRepo
                .createQueryBuilder("video")
                .leftJoin("video.user", "video_user")
                .addSelect("video_user.id")
                .addSelect("video_user.business")
                .leftJoin("video_user.business", "video_user_business")
                .addSelect("video_user_business.id")
                .addSelect("video_user_business.photo")
                .addSelect("video_user_business.name")
                .addSelect("video_user_business.pageName")
                .addSelect("video_user.person")
                .leftJoin("video_user.person", "video_user_person")
                .addSelect("video_user_person.id")
                .addSelect("video_user_person.photo")
                .addSelect("video_user_person.fullName")
                .addSelect("video_user_person.pageName")
                .leftJoinAndSelect("video.mentions", "video_mention", `
                    video_mention.video_comment IS NULL
                    AND
                    video_mention.video_comment_reply IS NULL
                `)
                .leftJoinAndSelect("video_mention.target", "video_mention_target")
                .leftJoin("video_mention_target.business", "video_mention_target_business")
                .addSelect("video_mention_target_business.id")
                .addSelect("video_mention_target_business.photo")
                .addSelect("video_mention_target_business.name")
                .addSelect("video_mention_target_business.pageName")
                .leftJoin("video_mention_target.person", "video_mention_target_person")
                .addSelect("video_mention_target_person.id")
                .addSelect("video_mention_target_person.photo")
                .addSelect("video_mention_target_person.fullName")
                .addSelect("video_mention_target_person.pageName")
                .leftJoinAndMapOne(`video.bookmarked`, `video.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${req.user.baseBusinessAccount || req.user.id}`)
                .andWhere("video.deleted IS NULL")
                .andWhere("video.draft = FALSE")
                .andWhere("video.user_id != :user", {user: (req.user.baseBusinessAccount || req.user.id)})
                .andWhere(qb => { // Don't show videos from your followings because it should be shown in feed
                    const subQuery = qb.subQuery()
                        .select("followings.followedAccount") // select all followed accounts
                        .from("t_followings", "followings")
                        .where("followings.account = :userId", { userId: req.user.id })
                        .getQuery();
                    return "video.user_id NOT IN " + subQuery
                })

            if(category && category !== 'all') {
                q1.andWhere("LOWER(video.category) = :category", {category: category.toLowerCase()})
            };
            const videos = await q1
                .orderBy("video.created", "DESC")
                .take(limitNumb)
                .skip(skip)
                .getManyAndCount();
            // .getSql();
            // console.log('videos', videos)
            res.status(200).send({ items: videos[0], total: videos[1]})
        } catch (error) {
            console.log('error', error)
            res.status(400).send(error)
        }
    }

    static getArticles = async (req, res) => {
        const {
            category,
            limit = 30,
            page = 1
        } = req.query;
        try {
            const { limitNumb, skip } = DiscoverController._getPageParams(page, limit);
            const articleRepo = getRepository(Article);

            let q1 = articleRepo
                .createQueryBuilder("article")
                .leftJoin("article.user", "article_user")
                .addSelect("article_user.id")
                .addSelect("article_user.business")
                .leftJoin("article_user.business", "article_user_business")
                .addSelect("article_user_business.id")
                .addSelect("article_user_business.photo")
                .addSelect("article_user_business.name")
                .addSelect("article_user_business.pageName")
                .addSelect("article_user.person")
                .leftJoin("article_user.person", "article_user_person")
                .addSelect("article_user_person.id")
                .addSelect("article_user_person.photo")
                .addSelect("article_user_person.fullName")
                .addSelect("article_user_person.pageName")
                .leftJoinAndSelect("article.mentions", "article_mention", `
                    article_mention.article_comment IS NULL
                    AND
                    article_mention.article_comment_reply IS NULL
                `)
                .leftJoinAndSelect("article_mention.target", "article_mention_target")
                .leftJoin("article_mention_target.business", "article_mention_target_business")
                .addSelect("article_mention_target_business.id")
                .addSelect("article_mention_target_business.photo")
                .addSelect("article_mention_target_business.name")
                .addSelect("article_mention_target_business.pageName")
                .leftJoin("article_mention_target.person", "article_mention_target_person")
                .addSelect("article_mention_target_person.id")
                .addSelect("article_mention_target_person.photo")
                .addSelect("article_mention_target_person.fullName")
                .addSelect("article_mention_target_person.pageName")

                .leftJoinAndMapOne(`article.bookmarked`, `article.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${req.user.baseBusinessAccount || req.user.id}`)
                .andWhere("article.deleted IS NULL")
                .andWhere("article.draft = FALSE")
                .andWhere("article.user_id != :user", {user: (req.user.baseBusinessAccount || req.user.id)})
                .andWhere(qb => { // Don't show articles from your followings because it should be shown in feed
                    const subQuery = qb.subQuery()
                        .select("followings.followedAccount") // select all followed accounts
                        .from("t_followings", "followings")
                        .where("followings.account = :userId", { userId: req.user.id })
                        .getQuery();
                    return "article.user_id NOT IN " + subQuery
                })
                .orderBy("article.created", "DESC")
                .take(limitNumb)
                .skip(skip)

            if(category && category !== 'all') {
                q1.andWhere("LOWER(article.category) = :category", {category: category.toLowerCase()})
            }

            // console.log('q1.getSql()', q1.getSql())
            const articles = await q1
                .getManyAndCount();
            // .getSql();
            // console.log('articles', articles)
            res.status(200).send({ items: articles[0], total: articles[1]})
        } catch (error) {
            console.log('error', error)
            res.status(400).send(error)
        }
    }

    static getProducts = async (req, res) => {
        const distanceUnit = 111.045; // kilometers in 1 degree
        const {
            category,
            limit = 30,
            page = 1,
            radius
        } = req.query;
        try {
            const { limitNumb, skip } = DiscoverController._getPageParams(page, limit);
            const currentUser =  await getRepository(Account)
                .createQueryBuilder("account")
                .leftJoin(Address, "user_address", "user_address.id = account.\"personId\" OR user_address.id = account.\"businessId\" ")
                .addSelect("user_address.lat")
                .addSelect("user_address.lng")
                .addSelect("user_address.id", 'address_id')
                .where("account.id = :user", {user: (req.user.baseBusinessAccount || req.user.id)})
                .getRawOne()

            const lat = req.query.lat || currentUser.user_address_lat || null;
            const lng = req.query.lng || currentUser.user_address_lng || null;
            const productRepo = getRepository(Product);

            const q1 = await productRepo
            .createQueryBuilder("product")
            .setParameter("radius", radius)
            .setParameter("distance_unit", distanceUnit)
            .setParameter("user_lat", lat)
            .setParameter("user_lng", lng)
            .leftJoin("product.user", "product_user")
            .addSelect("product_user.id")
            .addSelect("product_user.business")
            .leftJoin("product_user.business", "product_user_business")
            .addSelect("product_user_business.id")
            .addSelect("product_user_business.photo")
            .addSelect("product_user_business.name")
            .addSelect("product_user_business.pageName")
            .addSelect("product_user.person")
            .leftJoin("product_user.person", "product_user_person")
            .addSelect("product_user_person.id")
            .addSelect("product_user_person.photo")
            .addSelect("product_user_person.fullName")
            .addSelect("product_user_person.pageName")
            .leftJoinAndSelect("product.photos", "product_photos", `product_photos.deleted IS NULL`)
            .leftJoinAndSelect("product.address_", "product_address")
            .leftJoinAndSelect("product.variants", "product_variants")
            .leftJoinAndMapOne("product_variants.cover","product_variants.photos", "variants_to_photos", "variants_to_photos.order = 0")
            .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos", "product_variants_photos.deleted IS NULL")
            .leftJoinAndSelect("product.shopcategory_", "product_shopcategory")
            .leftJoinAndMapOne(`product.bookmarked`, `product.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${req.user.baseBusinessAccount || req.user.id}`)
            .andWhere("product.deleted IS NULL")
            .andWhere("product.draft = FALSE")
            .andWhere("product.user_id != :user", {user: (req.user.baseBusinessAccount || req.user.id)})

            // Don't show photos from your followings because it should be shown in feed
            .andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select("followings.followedAccount") // select all followed accounts
                    .from("t_followings", "followings")
                    .where("followings.account = :userId", { userId: req.user.id })
                    .getQuery();
                return "product.user_id NOT IN " + subQuery
            })

            // Filter by category
            if(category && category !== 'all') {
                q1.andWhere("LOWER(product.category) = :category", {category: category.toLowerCase()})
            };

            // IMPLEMENTING HAVERSINE FORMULA BELOW TO SELECT PRODUCTS WITHIN RADIUS
            if (radius && lat && lng) {
                // console.log('radius', radius)
                // console.log('User lat', lat)
                // console.log('User lng', lng)
                q1
                    .addSelect(":distance_unit * DEGREES(ACOS(COS(RADIANS(:user_lat)) * COS(RADIANS(product_address.lat)) * COS(RADIANS(:user_lng - product_address.lng)) + SIN(RADIANS(:user_lat))* SIN(RADIANS(product_address.lat))))", "distance")
                    .andWhere("product_address.lat >= :user_lat  - (:radius::FLOAT / :distance_unit)")
                    .andWhere( "product_address.lat <= :user_lat  + (:radius::FLOAT / :distance_unit)")
                    .andWhere( "product_address.lng >= :user_lng - (:radius::FLOAT / (:distance_unit * COS(RADIANS(:user_lat))))")
                    .andWhere( "product_address.lng <= :user_lng + (:radius::FLOAT / (:distance_unit * COS(RADIANS(:user_lat))))" )
            }
            const products = await q1
            .groupBy(`
                product.*,
                product_user.*,
                product_user_person.*,
                product_user_business.*,
                product_photos.*,
                product_address.*,
                product_variants.*,
                variants_to_photos.*,
                product_variants_photos.*,
                product_shopcategory.*,
                bookmarks.*
            `)
            .orderBy("product.created", "DESC")
            .take(limitNumb)
            .skip(skip)
            // .getRawAndEntities()
            .getManyAndCount();
            // .getRawMany();
            // .getSql();

            res.status(200).send({ items: products[0], total: products[1]})
        } catch (error) {
            console.log('error', error)
            res.status(400).send(error)
        }
    }

    static getServices = async (req, res) => {
        const distanceUnit = 111.045; // kilometers in 1 degree
        const {
            category,
            limit = 30,
            page = 1,
            radius
        } = req.query;


        try {
            const { limitNumb, skip } = DiscoverController._getPageParams(page, limit);
            const currentUser =  await getRepository(Account)
                .createQueryBuilder("account")
                .leftJoin(Address, "user_address", "user_address.id = account.\"personId\" OR user_address.id = account.\"businessId\" ")
                .addSelect("user_address.lat")
                .addSelect("user_address.lng")
                .addSelect("user_address.id", 'address_id')
                .where("account.id = :user", {user: (req.user.baseBusinessAccount || req.user.id)})
                .getRawOne()

            const lat = req.query.lat || currentUser.user_address_lat || null;
            const lng = req.query.lng || currentUser.user_address_lng || null;

            const q1 = getRepository(Service)
                .createQueryBuilder("service")
                .leftJoin("service.reviews", "reviews")
                .addSelect("COUNT(reviews.id)", "service_reviews")
                .leftJoinAndSelect("service.schedule", "service_schedule")
                .leftJoinAndSelect("service_schedule.byDate", "service_schedule_bydate")
                .leftJoinAndSelect("service.photos", "service_photos")
                .leftJoinAndSelect("service.address", "service_address")
                .leftJoin("service.user", "service_user")
                .addSelect("service_user.id")
                .addSelect("service_user.business")
                .leftJoin("service_user.business", "service_user_business")
                .addSelect("service_user_business.id")
                .addSelect("service_user_business.photo")
                .addSelect("service_user_business.name")
                .addSelect("service_user_business.pageName")
                .addSelect("service_user.person")
                .leftJoin("service_user.person", "service_user_person")
                .addSelect("service_user_person.id")
                .addSelect("service_user_person.photo")
                .addSelect("service_user_person.fullName")
                .addSelect("service_user_person.pageName")
                .leftJoinAndMapOne(`service.bookmarked`, `service.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${req.user.baseBusinessAccount || req.user.id}`)
                .andWhere("service.deleted IS NULL")
                .andWhere("service.user_id != :user", {user: (req.user.baseBusinessAccount || req.user.id)})

                // Don't show photos from your followings because it should be shown in feed
                .andWhere(qb => {
                    const subQuery = qb.subQuery()
                        .select("followings.followedAccount") // select all followed accounts
                        .from("t_followings", "followings")
                        .where("followings.account = :userId", { userId: req.user.id })
                        .getQuery();
                    return "service.user_id NOT IN " + subQuery
                })
                // Filter by category
            if(category && category !== 'all') {
                q1.andWhere("LOWER(service.category) = :category", {category: category.toLowerCase()})
            };

            // IMPLEMENTING HAVERSINE FORMULA BELOW TO SELECT PRODUCTS WITHIN RADIUS
            if (radius && lat && lng) {
                // console.log('radius && lat && lng', radius, lat, lng)
                q1
                    .addSelect(":distance_unit * DEGREES(ACOS(COS(RADIANS(:user_lat)) * COS(RADIANS(service_address.lat)) * COS(RADIANS(:user_lng - service_address.lng)) + SIN(RADIANS(:user_lat))* SIN(RADIANS(service_address.lat))))", "distance")
                    .andWhere("service_address.lat >= :user_lat  - (:radius::FLOAT / :distance_unit)")
                    .andWhere( "service_address.lat <= :user_lat  + (:radius::FLOAT / :distance_unit)")
                    .andWhere( "service_address.lng >= :user_lng - (:radius::FLOAT / (:distance_unit * COS(RADIANS(:user_lat))))")
                    .andWhere( "service_address.lng <= :user_lng + (:radius::FLOAT / (:distance_unit * COS(RADIANS(:user_lat))))" )
            }

            const services = await q1
                .setParameter("radius", radius)
                .setParameter("distance_unit", distanceUnit)
                .setParameter("user_lat", lat)
                .setParameter("user_lng", lng)
                .groupBy(`
                    service.*,
                    service_schedule.*,
                    service_schedule_bydate.*,
                    service_photos.*,
                    service_address.*,
                    service_user.*,
                    service_user_business.*,
                    service_user_person.*,
                    bookmarks.*
                `)
                .orderBy("service.created", "DESC")
                .take(limitNumb)
                .skip(skip)
                .getManyAndCount();
            // .getRawMany();

            res.status(200).send({ items: services[0], total: services[1]})
        } catch (error) {
            console.log('error', error)
            res.status(400).send(error)
        }
    }

    static _getPageParams(
        page: number|string,
        limit: number|string
    ) {
        const limitNumb = Number(limit);
        const skip = (Number(page) - 1) * limitNumb;

        return { limitNumb, skip };
    }
}