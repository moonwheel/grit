const express = require("express");
const router = express.Router();
import passport = require("passport");

import { PaymentController } from "./controller";

router.post("/bank", passport.authenticate("jwt", {session: false}), PaymentController.createBankAccount);
router.get("/all", passport.authenticate("jwt", {session: false}), PaymentController.getCardAndBankAlias);
router.put("/bank", passport.authenticate("jwt", {session: false}), PaymentController.deactivateBankAccount);
router.post("/cards", passport.authenticate("jwt", {session: false}), PaymentController.createCard);
router.get("/cards", passport.authenticate("jwt", {session: false}), PaymentController.getCardRegistration);
router.put("/cards", passport.authenticate("jwt", {session: false}), PaymentController.deactivateCard);
router.post("/process", passport.authenticate("jwt", {session: false}), PaymentController.processBuy);
router.post("/payout", passport.authenticate("jwt", {session: false}), PaymentController.payOut);
router.get("/wallet", passport.authenticate("jwt", {session: false}), PaymentController.getWallet);
router.post("/payins/card", passport.authenticate("jwt", {session: false}), PaymentController.cardDirectPayIn);
router.post("/payins/bankwire", passport.authenticate("jwt", {session: false}), PaymentController.bankwireDirectPayIn);
router.get('/transactions', passport.authenticate("jwt", {session: false}), PaymentController.getWalletTransactions);

export = router;
