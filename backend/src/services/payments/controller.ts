import { Product_variant } from './../../entities/product_variant';
import { Product } from './../../entities/product';
import { Response, Request } from "express";
import { getConnection, getRepository } from "typeorm";

import { IBankAllias } from "../../interfaces/mango-pay/mango.interface";

import { App } from "../../config/app.config";

import { Business } from "../../entities/business";
import { Account } from "../../entities/account";
import { Person } from "../../entities/person";
import { Bank } from "../../entities/bank";
import { OrderProduct } from "../../entities/order_product";
import { Order, PaymentType } from "../../entities/order";

import getCartProductsRepository from "../../entities/repositories/buy-repository";
import getOrderRepository from "../../entities/repositories/order-repository";

import paymentProcess from "../../utils/payment-process";
import { getCalculatedSellers } from "../../utils/helpers";
import mangoPay from "../../utils/mango-payment";
import mangoPayment from "../../utils/mango-payment";
import { NotificationController } from "../notifications/controller";
import { ProductController } from '../product/controller';

interface IRequest extends Request {
    business: any;
    user: any;
}

export class PaymentController {

    /**
     * GET ALL cards and bank accounts
     * @param {Object} req
     * @param {Object} res
     */
    static getCardAndBankAlias = async (req: IRequest, res: Response) => {
        console.log('user', req.user);
        try {
            const account = await getRepository(Account).findOne(req.user.id);

            const mangoUserId = account.business ? account.business.mangoUserId : account.person.mangoUserId;
            const bankAliasId = account.business ? account.business.bankAliasId : account.person.bankAliasId;

            const [
                cards,
                bankAlias,
                bankAccounts
            ] = await Promise.all([
                mangoPay.getCardsList(Number(mangoUserId)),
                mangoPay.getBankAlias(Number(bankAliasId)),
                mangoPay.getBankAccountsList(Number(mangoUserId))
            ]);

            const creditCardsList = cards.map(card => {
                return {
                    id: card.Id,
                    userId: card.UserId,
                    alias: card.Alias,
                    cardProvider: card.CardProvider,
                    expiration: card.ExpirationDate,
                    currency: card.Currency,
                    type: card.CardType,
                    active: card.Active
                }
            });

            const bankAcountsList = bankAccounts.map(account => ({
                id: account.Id,
                iban: account.IBAN,
                name: account.OwnerName,
                address: account.OwnerAddress,
                userId: account.UserId,
                active: account.Active,
                created: account.CreationDate,
                type: account.Type,
            }));

            const activeCards = creditCardsList.filter(card => card.active === true);
            const activeBankAccounts = bankAcountsList.filter(bankAccount => bankAccount.active === true);

            let activeBankAlias = {} as IBankAllias;

            if (bankAlias.Active) {
                activeBankAlias.id = bankAlias.Id;
                activeBankAlias.bic = bankAlias.BIC;
                activeBankAlias.iban = bankAlias.IBAN;
                activeBankAlias.walletId = bankAlias.WalletId;
                activeBankAlias.creditedUserId = bankAlias.CreditedUserId;
            }

            const result = {
                cards: activeCards,
                bankAlias: activeBankAlias,
                bankAccounts: activeBankAccounts
            }

            res.status(200).json(result);
        } catch (error) {
            console.log('getCardAndBankAccounts', error);
            res.status(400).json({ error: error.message });
        }
    }

    static createBankAccount = async (req, res) => {
        try {
            const { business, person } = req.user;
            // let city;
            // let country;
            // let street;

            let userData = null;

            if (business) {
                userData = await getRepository(Business).findOne(business.id);
                // const { address } = userData;
                // city = address.city;
                // country = address.city;
                // street = address.street;
            } else {
                userData = await getRepository(Person).findOne(person.id);
                // const { address } = userData;
                // city = address.city;
                // country = address.country;
                // street = address.street;
            }

            const { data } = await mangoPay.createBankAccount({
                name: req.body.name,
                city: 'Berlin',
                country: 'DE',
                address: 'Test street',
                iban: req.body.iban,
                userId: Number(userData.mangoUserId),
                postalCode: '75001',
            });

            res.status(200).json({
                id: data.Id,
                iban: data.IBAN,
                name: data.OwnerName,
                address: data.OwnerAddress,
                userId: data.UserId,
                active: data.Active,
                created: data.CreationDate,
                type: data.Type,
            });
        } catch (error) {
            console.log('createBankAccount', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * CREATE Card
     * @param {Object} req
     * @param {Object} res
     */
    static createCard = async (req: IRequest, res: Response) => {
        try {
            const account = await getRepository(Account).findOne(req.user.id);

            let mangoUserId;
            if (account.business) {
                mangoUserId = account.business.mangoUserId;
            } else {
                mangoUserId = account.person.mangoUserId;
            }

            const {
                data: {
                    Id,
                    CardRegistrationURL,
                    PreregistrationData,
                    AccessKey
                }
            } = await mangoPay.cardRegistration(Number(mangoUserId));

            const { data } = await mangoPay.cardInfo({
                cardRegistrationURL: CardRegistrationURL,
                data: PreregistrationData,
                accessKeyRef: AccessKey,
                cardNumber: req.body.number,
                cardExpirationDate: String(req.body.month) + String(req.body.year),
                cardCvx: req.body.cvc
            });

            const cardData = await mangoPay.updateTokenDatas(Id, data);

            const createdCard = await mangoPay.viewCard(cardData.data.CardId);

            const card = {
                id: createdCard.Id,
                userId: createdCard.UserId,
                alias: createdCard.Alias,
                cardProvider: createdCard.CardProvider,
                expiration: createdCard.ExpirationDate,
                currency: createdCard.Currency,
                type: createdCard.CardType,
                active: createdCard.Active
            }

            res.status(200).json(card);
        } catch (error) {
            console.log('createCard', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * GET card registration
     * @param {Object} req
     * @param {Object} res
     */
    static getCardRegistration = async (req: Request, res: Response) => {
        try {
            const {data} = await mangoPay.getCardRegistraition(req.body.id);

            res.status(200).json(data);
        } catch (error) {
            console.log('getCardRegistration', error);
            res.status(400).json({ error: error.message });
        }
    }

    static getWallet = async (req, res) => {
        try {
            const {
                user: {
                    wallet: walletId,
                },
            } = req;

            const { data } = await mangoPay.getWalletForUser(walletId);

            const wallet = {
                id: data.Id,
                balance: data.Balance.Amount / 100,
                currency: data.Balance.Currency,
                created: data.CreationDate,
            };

            res.status(200).json(wallet);
        } catch (error) {
            console.log('getCardRegistration', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * CREATE Hook
     * @param {Object} req
     * @param {Object} res
     */
    static createHook = async (req: Request, res: Response) => {
        try {
            const {data} = await mangoPay.createHook();

            res.status(200).json(data);
        } catch (error) {
            console.log('createHook', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * GET Hook by id
     * @param {Object} req
     * @param {Object} res
     */
    static getHookById = async (req: Request, res: Response) => {
        try {
            const {data} = await mangoPay.getHookById(Number(req.params.id));

            res.status(200).json(data);
        } catch (error) {
            console.log('getHookById', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * GET Hook list
     * @param {Object} req
     * @param {Object} res
     */
    static getHooksList = async (req, res) => {
        try {
            const {data} = await mangoPay.getHooks();

            res.status(200).json(data);
        } catch (error) {
            console.log('getHooksList', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * UPDATE Hook
     * @param {Object} req
     * @param {Object} res
     */
    static updateHook = async (req, res) => {
        try {
            const {data} = await mangoPay.updateHook(req.params.id);

            res.status(200).json(data);
        } catch (error) {
            console.log('updateHook', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * UPDATE Bank Account
     * @param {Object} req
     * @param {Object} res
     */
    static deactivateBankAccount = async (req, res) => {
        try {
            if (!req.user.business) {
                throw new Error("You are not business account.");
            }

            const { business: {mangoUserId} } = await getRepository(Account).findOne(req.user.id);
            const {data} = await mangoPay.deactivateBankAccount(Number(mangoUserId), req.body.id);

            const bankAccount = {
                ibank: data.IBAN,
                id: data.Id,
                date: data.CreationDate,
                active: data.Active,
                owner: data.OwnerName
            }

            res.status(200).json(bankAccount);
        } catch (error) {
            console.log('deactivateBankAccount', error);
            res.status(400).json({ error: error.message });
        }
    }

    /**
     * Deactivate Card
     * @param {Object} req
     * @param {Object} res
     */
    static deactivateCard = async (req: Request, res: Response) => {
        try {
            const {data} = await mangoPay.deactivateCard(req.body.id);

            const card = {
                alias: data.Alias,
                type: data.CardType,
                country: data.Country,
                active: data.Active,
                currency: data.Currency,
                userId: data.UserId,
                creationDate: data.CreationDate
            };

            res.status(200).json(card);
        } catch (error) {
            console.log('updateCard', error);
            res.status(400).json({ error: error.message });
        }
    }

    static processBuy = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                body: {
                    delivery_address,
                    billing_address,
                    payment,
                    grit_wallet,
                },
            } = req;

            console.log('req body', req.body);

            const cartProducts = await getCartProductsRepository().getCartProducts(userId);

            if (!cartProducts[0].length) {
                throw new Error('Cart is empty');
            }

            const { sellers, totalPrice } = await getCalculatedSellers(userId);

            // console.log('sellers, totalPrice', sellers, totalPrice)

            let paymentType: PaymentType = '';

            try {
                const card = await mangoPay.viewCard(payment);
                if (card) {
                    paymentType = 'card';
                }
            } catch (e) {
                const bank = await mangoPay.getSingleBankAccount(userId, payment);
                if (bank) {
                    paymentType = 'bank';
                } else {
                    throw e;
                }
            }

            // create buyer orders for each seller
            const orderSellersIds: Map<number, number> = new Map<number, number>();
            const orderSellers: Map<number, any> = new Map<number, any>();

            let buyerData = await paymentProcess.createPayInToWallet(
                userId,
                payment,
                totalPrice,
                grit_wallet,
                'order',
            );

            // console.log('buyerData', buyerData)

            for (let seller of sellers) {
                const orderId = await getOrderRepository().createNewOrder({
                    buyerId: Number(userId),
                    sellerId: Number(seller.seller_id),
                    shippingPrice: seller.shippingprice,
                    totalPrice: seller.totalsum,
                    payment,
                    deliveryAddressId: delivery_address,
                    billingAddressId: billing_address || delivery_address,
                    payInId: buyerData ? Number(buyerData.transactionId) : undefined,
                    amount: buyerData ? buyerData.amount : undefined,
                    is_wallet: grit_wallet,
                    payment_type: paymentType,
                });

                // get unique Order-ID for each seller
                orderSellersIds.set(Number(seller.seller_id), orderId);

                // add unique seller object
                orderSellers.set(Number(seller.seller_id), seller);
            }

            const orderProducts = cartProducts[0].map(cartProduct => ({
                quantity: cartProduct.quantity,
                collect: cartProduct.collected,
                price: cartProduct.product.type === 'productVariants' ? cartProduct.product_variant.price : cartProduct.product.price,
                shipping_price: cartProduct.collected ? 0 : cartProduct.product.shippingCosts,
                payment_status: "pending",
                order: orderSellersIds.get(cartProduct.product.user.id),
                product: cartProduct.product.id,
                seller_id: cartProduct.product.user.id,
                product_variant: cartProduct.product_variant ? cartProduct.product_variant.id : null
            }));

            const orderProduct = getRepository(OrderProduct);
            const productRepo = getRepository(Product);
            const productVariantRepo = getRepository(Product_variant);

            for (let product of orderProducts) {

                if (product.product_variant) {
                    const item = await productVariantRepo.findOne(product.product_variant);
                    await productVariantRepo.update(product.product_variant, {
                        quantity: item.quantity - product.quantity,
                    });
                } else {
                    const item = await productRepo.findOne(product.product);
                    await productRepo.update(product.product, {
                        quantity: item.quantity - product.quantity,
                    });
                }

                // @ts-ignore
                await orderProduct.save({
                    ...product,
                });


                // console.log(' product of orderProducts ', product)

                const productItself = await ProductController
                    .createSingleProductQuery(productRepo, product.seller_id, product.product)
                    .getOne();

                const productVariantItself = await productVariantRepo.findOne(product.product_variant, {
                    relations: ['photos']
                })

                // console.log('productItself', productItself)
                await NotificationController.addNotification(
                    'ordered',
                    'product',
                    'product',
                    productItself,
                    userId,
                    productItself.user,
                    productVariantItself
                )
            }


            await getCartProductsRepository().deleteProductsFromCart(userId);

            res.status(200).json({ message: 'success' });
        } catch (error) {
            console.log('error: ', error);
            res.status(400).send({ error: error.message });
        }
    }

    static cardDirectPayIn = async (req, res) => {
        try {
            const {
                user: {
                    mangoUserId,
                    wallet: walletId
                },
                body: {
                    card: cardId,
                    amount,
                },
            } = req;
            const { data } = await mangoPayment.createPayIn({
                mangoUserId,
                walletId,
                cardId,
                amount,
            });

            res.status(200).send(data);
        } catch (error) {
            console.log('error: ', error.response.data);
            res.status(400).send({ error: error.message });
        }
    }

    static bankwireDirectPayIn = async (req, res) => {
        try {
            res.status(200).send({ message: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static payOut = async (req: Request, res: Response) => {
        try {
            // await mangoPay.payOut();

            res.status(200).send({ message: "success" });
        } catch (error) {
            console.log('payOut', error);
            res.status(400).send({ message: error.message });
        }
    }

    static getWalletTransactions = async (req: Request, res: Response) => {
        try {
            const {wallet} = req.user;
            const transactions = await mangoPay.getWalletTransactions(wallet);

            return transactions;

        }catch(error) {
            res.status(400).send({message: error.message})
        }
    }
}
