import getFollowingRepository from "../../entities/repositories/following-repository";
import getFollowingRequestRepository from "../../entities/repositories/following-request-repository";
import AccountRepository from "../../entities/repositories/account-repository";
import { Followings_request, RequestStatus } from "../../entities/followings_request";
import { App } from "../../config/app.config";
import { getRepository } from "typeorm";
import { publish } from "../../utils/sse";
import { NotificationController } from "../notifications/controller";
import getAccountRepository from "../../entities/repositories/account-repository";
import {Account} from "../../entities/account";

export class FollowingController {
    static follow = async (req, res) => {
        try {
            const {
                user,
                params: { followId },
            } = req;

            const targetAccount = await AccountRepository().getFullAccountInfo({ key: 'id', val: followId });
            const target = targetAccount.baseBusinessAccount ? targetAccount.baseBusinessAccount : targetAccount;
            const account = user.baseBusinessAccount || user.id;
            const maxCount = App.MAX_FOLLOW_COUNT;

            if (target == account) {
                throw new Error("Self follow");
            }

            if (account.followingsCount <= maxCount) {
                throw new Error("Follow quota exceeded");
            }

            if ((target.business && target.business.id) || (target.person && target.person.privacy === "public")) {
                // FOLLOWING PUBLIC OR BUSINESS ACCOUNT
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);

                if (!!alreadyFollowed) {
                    throw new Error("The user already follows this user");
                }

                await getFollowingRepository().follow(target, account);

                const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: account });
                    // add notification controller
                    await NotificationController.addNotification(
                        'follows',
                        null,
                        null,
                        null,
                        issuer, // issuer
                        targetAccount // owner
                    );
                res.status(200).send({ status: "success" });
            } else {
                // FOLLOWING PRIVATE ACCOUNT
                const alreadyRequested = await getFollowingRequestRepository().isAccountAlreadyRequestedFollowingsOfTarget(account, target.id);

                if (!!alreadyRequested) {
                    // throw new Error("The user already requested followings of this user");
                    const followingRequest = await getFollowingRequestRepository().updateFollowingRequest(alreadyRequested.id, target, 'pending');
                    publish({ userId: followId, type: 'following_request', entityType: 'followingRequest', entity: followingRequest });
                    res.status(200).send({ status: "pending" });
                } else {
                    const followingRequestObject = await getFollowingRequestRepository().createFollowRequest(target, account);
                    const followingRequest = await getFollowingRequestRepository().findOne({ id: followingRequestObject.id }, { relations: ["account"] });

                    publish({ userId: followId, type: 'following_request', entityType: 'followingRequest', entity: followingRequest });
                    res.status(200).send({ status: "pending" });
                }
            }

        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static unfollow = async (req, res) => {
        try {
            const {
                user,
                params: { followId },
            } = req;
            const targetAccount = await AccountRepository().getFullAccountInfo({ key: 'id', val: followId })
            const target = targetAccount.baseBusinessAccount ? targetAccount.baseBusinessAccount : targetAccount;
            const account = user.baseBusinessAccount || user.id;
            if ( (target.business && target.business.id) || (target.person && target.person.privacy === 'public')) {
                await getFollowingRepository().unfollow(followId, user.baseBusinessAccount || user.id);
            } else {
                const followingRequest = await getFollowingRequestRepository().isAccountAlreadyRequestedFollowingsOfTarget(account, target.id);
                await getFollowingRepository().unfollow(followId, user.baseBusinessAccount || user.id);
                await getFollowingRequestRepository().deleteFollowRequest(target, account);
                publish({
                    userId: target.id,
                    type: 'following_request',
                    entityType: 'followingRequestChanged',
                    entity: {
                        userId: user.id,
                        followId: followingRequest && followingRequest.id,
                        status: followingRequest ? 'canceled' : 'unfollowed',
                    },
                });
            }

            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getIncomingFollowingsRequests = async (req, res) => {
        try {
            const {
                user,
                query: { offset = 0, limit = 10 },
            } = req;

            const incomingRequests = await getFollowingRequestRepository().getIncomingFollowingsRequests(user.baseBusinessAccount || user.id, limit, offset)
            res.status(200).send({ incomingRequests });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getOutgoingFollowingsRequests = async (req, res) => {
        try {
            const {
                user,
            } = req;

            const outgoingRequest = await getFollowingRequestRepository().getOutgoingFollowingsRequests(user.baseBusinessAccount || user.id)
            res.status(200).send({ outgoingRequest });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };


    static approveIncomingFollowingRequest = async (req, res) => {
        try {
            const {
                user,
                params: { requestId }
            } = req;

            const { account } = await getFollowingRequestRepository().updateFollowingRequest(requestId, user, 'approved');
            await getFollowingRequestRepository().deleteFollowRequest(user.baseBusinessAccount || user.id, account);

            await getFollowingRepository().follow(user, account);

            publish({
                userId: account.baseBusinessAccount ? account.baseBusinessAccount.id : account.id,
                type: 'following_request',
                entityType: 'followingRequestChanged',
                entity: {
                    userId: user.baseBusinessAccount || user.id,
                    status: 'approved',
                },
            });
            res.status(200).send({ status: 'approved' });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static rejectIncomingFollowingRequest = async (req, res) => {
        try {
            const {
                user,
                params: { requestId }
            } = req;

            const { account } = await getFollowingRequestRepository().updateFollowingRequest(requestId, user, 'approved');
            await getFollowingRequestRepository().deleteFollowRequest(user.baseBusinessAccount || user.id, account);

            console.log('account', account)
            publish({
                userId: account.baseBusinessAccount ? account.baseBusinessAccount.id : account.id,
                type: 'following_request',
                entityType: 'followingRequestChanged',
                entity: {
                    userId: user.baseBusinessAccount || user.id,
                    status: 'rejected',
                },
            });
            res.status(200).send({ status: 'rejected' });
        } catch (error) {
          console.log('error', error)
            res.status(400).send({ error: error.message });
        }
    };


    static getFollowings = async (req, res) => {
        try {
            const {
                user,
                params: { userId },
                query: {
                    page,
                    limit,
                    search,
                }
            } = req;

            const id = userId ? userId : user.baseBusinessAccount || user.id;
            const followings = await getFollowingRepository().getFollowings(id, { page, limit, search });

            res.status(200).send(followings);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getFollowers = async (req, res) => {
        try {
            const {
                user,
                params: { targetUserId },
                query: {
                    page,
                    limit,
                    search,
                }
            } = req;

            const currentUserId = user.baseBusinessAccount || user.id;
            const id = targetUserId || currentUserId;
            const followers = await getFollowingRepository().getFollowers2(id, currentUserId, { page, limit, search });

            res.status(200).send(followers);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };
}
