import {FollowingController} from "./controller";
import {authenticate} from "passport";
import {Router} from "express";

const router: Router = Router();

router.get("/following", authenticate("jwt", {session: false}), FollowingController.getFollowings);

// router.get("/following/requests", authenticate("jwt", {session: false}), FollowingController.getOutgoingFollowingsRequests);

router.get("/followers", authenticate("jwt", {session: false}), FollowingController.getFollowers);

router.get("/followers/requests", authenticate("jwt", {session: false}), FollowingController.getIncomingFollowingsRequests);

router.put("/followers/requests/approve/:requestId", authenticate("jwt", {session: false}), FollowingController.approveIncomingFollowingRequest);

router.put("/followers/requests/reject/:requestId", authenticate("jwt", {session: false}), FollowingController.rejectIncomingFollowingRequest);

router.get("/:targetUserId/followers", authenticate("jwt", {session: false}), FollowingController.getFollowers);

router.post("/following/:followId", authenticate("jwt", {session: false}), FollowingController.follow);

router.delete("/following/:followId", authenticate("jwt", {session: false}), FollowingController.unfollow);

export default router;

