import * as express from "express";
const router = express.Router();

import passport = require("passport");
import { SearchController } from "./controller";

// TODO this route is temporary unauthorized
router.get("/", passport.authenticate("jwt", {session: false}), SearchController.doSearch);
router.get("/autosuggest", passport.authenticate("jwt", {session: false}), SearchController.autosuggest);
router.get("/location", passport.authenticate("jwt", {session: false}), SearchController.locationSearch);
router.get("/location/autosuggest", passport.authenticate("jwt", {session: false}), SearchController.locationAutosuggest);
// router.get("/", passport.authenticate("jwt", {session: false}), SearchController.doSearch);

export = router;
