import { Hashtag } from './../../entities/hashtag';
import { getRepository } from 'typeorm';
import axios from 'axios';
import { App } from "../../config/app.config";
import { SearchDictionary } from '../../entities/search_dictionary';
import getHashtagRepository from '../../entities/repositories/hashtag-repository';
import { Request, Response } from 'express';
import { Geocoding } from '../../utils/geocoding';
import getBlockRepository from "../../entities/repositories/block-repository";

export class SearchController {
    static autosuggest = async (req, res) => {
        try {
            const { text } = req.query;
            const tag = text.substr(1);
            const isHashtag = text.search(new RegExp('#\S*')) === -1 ? false : true;

            // console.log('isHashtag', isHashtag)

            // TODO: Investigate how to sort suggests by count for hashtags
            const hashtagQuery = {
                "suggest": {
                    "suggest" : {
                        "prefix" : isHashtag ? tag : text,
                        "completion" : {
                            "field" : isHashtag ? "hashtag": "title",
                            "skip_duplicates": false,
                            "size": 5,
                        }
                    }
                }
            };
            const indexUrl = isHashtag ? App.ELASTIC_HASHTAGS_SUGGEST : App.ELASTIC_TITLE_SUGGEST;
            const response = await axios({
                method: 'post',
                url: `${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/${indexUrl}/_search`,
                headers: { "Content-Type": "application/json" },
                data: hashtagQuery
            });

            // console.log('response', response.data);
            res.status(200).send(response.data.suggest.suggest);
        } catch (error) {
            console.log('autosuggest error', error);
            res.status(400).send(error);
        }
    }


    static locationAutosuggest = async (req, res) => {
        try {
            const { text, limit } = req.query;

            // TODO: Investigate how to sort suggests by count for hashtags
            const locationQuery = {
                "suggest": {
                    "suggest" : {
                        "prefix" : text,
                        "completion" : {
                            "field": "location",
                            "skip_duplicates": false,
                            "size": limit || 5,
                        }
                    }
                }
            }
            const locationElasticSearch = await axios({
                method: 'post',
                url: `${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/${App.ELASTIC_LOCATION_SUGGEST}/_search`,
                headers: { "Content-Type": "application/json" },
                data: locationQuery
            })

            const businessQuery = {
                "suggest": {
                    "text_business_name" : {
                        "prefix" : text,
                        "completion" : {
                            "field" :  "text_business_name",
                            "skip_duplicates": false,
                            "size": 5,
                        }
                    },
                    "address" : {
                        "prefix" : text,
                        "completion" : {
                            "field" :  "address",
                            "skip_duplicates": false,
                            "size": 5,
                        }
                    }
                }
            };
            const businessSuggest = await axios({
                method: 'post',
                url: `${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/${App.ELASTIC_BUSINESS_SUGGEST}/_search`,
                headers: { "Content-Type": "application/json" },
                data: businessQuery
            });

            const tomTom = await Geocoding.tomTomAutocomplete(text, limit);
            res.status(200).send({
                locations: locationElasticSearch.data, // locations of other photos/videos/articles entered in the system
                business: businessSuggest.data, // business names autosuggest
                tomTom // tom-tom
            })
        } catch (error) {
            console.log('autosuggest error', error)
            res.status(400).send(error)
        }
    }


    /**
     * Allowed types: account, article, photo, product, service, video, all
     */
    static doSearch = async (req: Request, res: Response) => {
        const { text, type, page, limit } = req.query;
        const userId = req.user.id;
        const fields = ["text_*"];

        try {
            const response = await SearchController.elasticSearch(text, [type], fields, page, limit);

            const [
                blockedBy,
                blocked
            ] = await Promise.all([
                getBlockRepository().blockedBy(userId),
                getBlockRepository().getBlacklist(userId)
            ]);

            const blockedIds: Array<number> = blocked.map(obj => obj.id);
            const nowToShowIds: Array<number> = [...blockedIds, ...blockedBy];

            const { buckets } = response.data.aggregations.by_id;

            let bucketWithOutBlockedAccounts = buckets;

            if (buckets.length && nowToShowIds.length) {
                nowToShowIds.forEach((id) => {
                    bucketWithOutBlockedAccounts = buckets.filter(obj => obj.key !== `account-${id}`);
                });
            }
            response.data.aggregations.by_id.buckets = bucketWithOutBlockedAccounts;

            res.status(200).send(response.data);
        } catch (error) {
            res.status(400).send(error);
        }
    }

    static elasticSearch = async (text: string, type: string[], fields: string[], page = 1, limit = 15) => {
        const filter = await SearchController.buildFilter(text, type);

        const startsWithText = `${text}*`
        const containsText = `*${text}*`

        const size = Number(limit);
        const from = (Number(page) - 1) * size;

        const isHashtag = text.search(new RegExp('#\S*')) === -1 ? false : true;
        const tag = text.substr(1);

        const searchQuery =  {
            "bool": {
                "should": [{
                    "query_string": {
                        "query": text,
                        "fields": fields,
                        "boost": 4
                    }
                }, {
                    "query_string": {
                        "query": startsWithText,
                        "fields": fields,
                        "boost": 3
                    }
                }, {
                    "query_string": {
                        "query": containsText,
                        "fields": fields,
                        "boost": 2
                    }
                }],
                "filter": filter
            }
        }

        const hashtagQuery = {
            "bool": {
                "must" : {
                    "term" : { "hashtags" : tag }
                },
                "filter": filter
            }
        }

        const aggs = {
            "by_id" : {
                "terms": {
                    "field": "aggregation_id.keyword",
                    "size": size * 30 // 30 is approximate number of page which user never reach
                },
                "aggs" : {
                    "_max_score" : {
                        "max": {
                            "script": {
                                "source": "_score"
                            }
                        }
                    },
                    "_amount_followers": { "min" : { "field" : "amount_followers" } },
                    "_amount_likes": { "min" : { "field" : "amount_likes" } },
                    "_amount_views": { "min" : { "field" : "amount_views" } },
                    "_amount_reviews": { "min" : { "field" : "amount_reviews" } },
                    "min_price_hits": {
                        "top_hits": {
                            "_source": {
                                "exclude": ['min_price', 'type_int', 'tags', '@version']
                            },
                            "sort": [
                                // // for Products
                                // TODO: DECIDE HOW TO DEAL WHEN THERE ARE NO INDEXED PRODUCTS (POSSIBLE FAKE PRICE FIELD)!!!!!!!!!
                                {
                                    "min_price": {
                                        "order": "asc" // sort product variants of the same product by lowest price
                                    }
                                }
                            ],
                            "size" : 1
                        }
                    },
                    "inner_bucket_sort": {
                        "bucket_sort": {
                            "from": from,
                            "size": size,
                            "sort": [
                                { "_max_score": "desc" },
                                // for Accounts
                                { "_amount_followers": {
                                    "order": "desc",
                                    "unmapped_type" : "long"
                                }},
                                // for Photos, Videos, Articles
                                { "_amount_likes": {
                                    "order": "desc",
                                    "unmapped_type" : "long"
                                } },
                                { "_amount_views": {
                                    "order": "desc",
                                    "unmapped_type" : "long"
                                } },
                                // // for Products & Services
                                { "_amount_reviews": {
                                    "order": "desc",
                                    "unmapped_type" : "long"
                                } },
                            ],
                        }
                    }
                }
            },
        }

        // const aggForTypeProduct

        const query = {
            "min_score": 0.0000000000001, // remove objects with 0 score from the response
            "size": 10,
            "query": !isHashtag ? searchQuery : hashtagQuery,
            "aggs" : aggs,
            "sort": [
                // common sorting by score
                { "_score": "desc" },
                // for Accounts
                { "amount_followers": {
                    "order": "desc",
                    "unmapped_type" : "long"
                }},
                // for Photos, Videos, Articles
                { "amount_likes": {
                    "order": "desc",
                    "unmapped_type" : "long"
                } },
                { "amount_views": {
                    "order": "desc",
                    "unmapped_type" : "long"
                } },
                // // for Products & Services
                { "amount_reviews": {
                    "order": "desc",
                    "unmapped_type" : "long"
                } }
            ],
        }

        const response: any = await axios({
            method: 'post',
            url: `${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/${App.ELASTIC_INDEX}/_search`,
            headers: { "Content-Type": "application/json" },
            data: query
            // data: query1
        })
        return response;
    }

    static buildFilter = async (text: string, searchType: string[]) => {
        const searchDictionaryRepo = getRepository(SearchDictionary);
        const searchTerms = text.split(' ');

        let colorFilter = [];
        let materialFilter = [];
        let flavorFilter = [];

        const promises = []
        try {
            for (const value of searchTerms) {
                const result = await searchDictionaryRepo.find({ where: { word: value } });
                if (result[0] && result[0].type === 'color') {
                    // promises.push(JSON.parse(JSON.stringify(result[0])))
                    colorFilter.push(value)
                }
                if (result[0] && result[0].type === 'material') {
                    // promises.push(JSON.parse(JSON.stringify(result[0])))
                    materialFilter.push(value)
                }
                if (result[0] && result[0].type === 'flavor') {
                    flavorFilter.push(value)
                }
                promises.push(JSON.parse(JSON.stringify(value)))
            }
            return Promise.all(promises)
                .then((response) => {
                    const filter: any = [{
                        "match": {
                            "tags": searchType.join(' ') // apply filter by type (it returns not relevant results with the same type but with 0 score)
                            // "tags": ["photo", "video", "article", "service"].join(' ') // apply filter by type (it returns not relevant results with the same type but with 0 score)
                        },
                    }];

                    if (colorFilter && colorFilter.length > 0) {
                        filter.push({
                            "match": {
                                "variant_color": colorFilter.join(' ')
                            },
                        })
                    };
                    if (materialFilter && materialFilter.length > 0) {
                        filter.push({
                            "match": {
                                "variant_material": materialFilter.join(' ')
                            },
                        })
                    };
                    if (flavorFilter && materialFilter.length > 0) {
                        filter.push({
                            "match": {
                                "variant_flavor": flavorFilter.join(' ')
                            },
                        })
                    };

                    return filter;
                })
        } catch (error) {
            console.log('smth wrong on building a filter', error)
        }
    }

    static locationSearch = async (req: Request, res: Response) => {
        try {
            const { text, page, limit, type } = req.query;
            let lat, lng;

            const fields = ['location']

            // console.log('addr', text)


            const response = await SearchController.elasticSearch(text, [type], fields, page, limit)

            res.status(200).send(response.data)
        } catch (error) {
            res.status(400).send(error)
        }
    }
}