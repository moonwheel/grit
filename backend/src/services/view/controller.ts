import { getRepository } from "typeorm";
import { Photo } from "../../entities/photo";
import { Article } from "../../entities/article";
import { Video } from "../../entities/video";
import { Text } from "../../entities/text";
import { Article_View } from "../../entities/article_view";
import { Photo_View } from "../../entities/photo_view";
import { Video_View } from "../../entities/video_view";
import { Text_View } from "../../entities/text_view";

export class ViewController {
    static view = entityName => async (req, res) => {
        try {
            const {
                params: { id },
                user,
            } = req;
            const entity = ViewController.getEntity(entityName);
            if (!entity) throw Error("No entity");

            const nameRepo = getRepository(entity);

            const foundView = await nameRepo.find({
                user: user.baseBusinessAccount || user.id,
                [`${entityName}_`]: id,
            });

            if (!foundView.length) {
                
                await nameRepo.save({
                    user: user.baseBusinessAccount || user.id,
                    [`${entityName}_`]: id,
                }, { reload: true })
            }
                

            res.status(200).send({ status: "success" });
        } catch (error) {
            console.log('Addview error', error)
            res.status(400).send({ error: error.message });
        }
    };
    static getEntity = entityName => {
        switch (entityName) {
            case "article": return Article_View;
            case "photo": return Photo_View;
            case "video": return Video_View;
            case "text": return Text_View;
            default: return null;
        }
    };
}
