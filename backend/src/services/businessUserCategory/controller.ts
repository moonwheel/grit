import { getRepository } from "typeorm";
import { BusinessUserCategory } from "../../entities/business_user_categoty";
import { User_BusinessUserCategory } from "../../entities/user_business_user_category";

export class BusinessUserCategoryController { 
    static addCategory = () => {
        
    };

    static getCategories = async(req, res) => {
        try {
            const categories = await getRepository(BusinessUserCategory).find();
            const sortCategories = [];
            categories.forEach((item: BusinessUserCategory) => {
                if (!item.parentId) {
                    sortCategories.push({id: item.id, name:item.name, category: []});
                }
            });
            sortCategories.forEach((item: any) => {
                const filtered = categories.filter((obj: BusinessUserCategory) => { return obj.parentId === item.id.toString();});
                if (filtered.length > 0) {
                    item.category = filtered;
                } 
            });
            res.status(200).send(sortCategories);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static updateUserCategory = async (userId, categotyId) => {
        const repo = getRepository(User_BusinessUserCategory);
        try {
            const data: User_BusinessUserCategory = await repo.findOne({where: {user: userId}});
            if (data) {
                await repo.update(data.id, {category_: categotyId});
            } else {
                await repo.save({user: userId, category_: categotyId});
            } 
        } catch (error) {
            throw error;
        }
    };
}
