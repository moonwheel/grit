const express = require("express");
const router = express.Router();

import { BusinessUserCategoryController } from "./controller";
import passport = require("passport");

router.get("/", BusinessUserCategoryController.getCategories);

export = router;