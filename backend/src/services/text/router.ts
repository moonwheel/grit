const express = require("express");
const router = express.Router();

import passport = require("passport");
import { TextController } from "./controller";
import { ViewController } from "../view/controller";
import { Middleware } from "../../utils/middleware";

router.post("/", passport.authenticate("jwt", {session: false}), TextController.addText);
router.get("/all", passport.authenticate("jwt", {session: false}), Middleware.checkDraftAccess, TextController.getAllTexts);
router.get("/:id", passport.authenticate("jwt", {session: false}), Middleware.checkInBlaklist("text", "id"), Middleware.checkDraftAccess, TextController.getSingleText);
router.put("/", passport.authenticate("jwt", {session: false}), TextController.updateText);
router.delete("/:id", passport.authenticate("jwt", {session: false}), TextController.deleteText);

router.post("/:id/like", passport.authenticate("jwt", {session: false}), TextController.likeText);

router.get("/:id/like", passport.authenticate("jwt", {session: false}), TextController.getTextLikes);

router.post("/:id/view", passport.authenticate("jwt", {session: false}), Middleware.checkInBlaklist("text", "id"), ViewController.view("text"));

router.post("/:id/comment", passport.authenticate("jwt", {session: false}), TextController.addComment);
router.put("/:id/comment", passport.authenticate("jwt", {session: false}), TextController.editComment);
router.get("/:id/comment", passport.authenticate("jwt", {session: false}), TextController.getComments);

router.get("/comment/:id/replies", passport.authenticate("jwt", {session: false}), TextController.getReplies);

router.post("/comment/:id/like", passport.authenticate("jwt", {session: false}), TextController.likeComment);

router.get("/comment/:id/like", passport.authenticate("jwt", {session: false}), TextController.getCommentLikes);

router.delete("/comment/:id", passport.authenticate("jwt", {session: false}), TextController.deleteComment);

export = router;
