const dateTime = require("date-time");
import { getRepository } from "typeorm";
import { Text } from "../../entities/text";
import { Text_Like } from "../../entities/text_like";
import { Text_Comment } from "../../entities/text_comment";
import { Text_Comment_Replies } from "../../entities/text_comment_reply";
import { Filter } from "../../interfaces/global/filter.interface";
import { Text_Comment_Like } from "../../entities/text_comment_like";
import getMentionRepository from "../../entities/repositories/mention-repository";
import { Helpers } from "../../utils/helpers";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import getHashtagRepository from "../../entities/repositories/hashtag-repository";
import getAccountRepository from "../../entities/repositories/account-repository";
import { NotificationController } from '../notifications/controller'
import { NotificationContentType } from "../../entities/notification";
import getFollowingRepository from "../../entities/repositories/following-repository";

export class TextController {

    static addText = async(req, res) => {
        try {
            const user = req.user;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const textRepo = getRepository(Text);
            const {
                text,
                draft = false,
                mentions = [],
                hashtags = []
            } = req.body;



            const savedData = await textRepo.save({user: user.baseBusinessAccount || user.id, text, draft: draft ? true : false });
            savedData.mentions = await getMentionRepository().addMention("text", savedData, mentions,'text',user);
            await getHashtagRepository().addHashtag("text", savedData.id, hashtags);
            res.status(200).send(savedData);
        } catch (error) {
            console.log('addText error',error)
            res.status(400).send({error: error.message});
        }
    };

    static getAllTexts = async(req, res) => {
        const {
            user,
            query: { draft = false, userId = null }
        } = req;
        const textRepo = getRepository(Text);
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) return res.status(403).send({error: 'Forbidden'});
            }

            const data: any = await textRepo
                .createQueryBuilder("text")
                .leftJoin("text.user", "text_user")
                .addSelect("text_user.id")
                .addSelect("text_user.business")
                .leftJoin("text_user.business", "text_user_business")
                .addSelect("text_user_business.id")
                .addSelect("text_user_business.photo")
                .addSelect("text_user_business.name")
                .addSelect("text_user_business.pageName")
                .addSelect("text_user.person")
                .leftJoin("text_user.person", "text_user_person")
                .addSelect("text_user_person.photo")
                .addSelect("text_user_person.fullName")
                .addSelect("text_user_person.pageName")
                .leftJoinAndSelect("text.mentions", "text_mention", `
                    text_mention.text_comment IS NULL
                    AND
                    text_mention.text_comment_reply IS NULL
                `)
                .leftJoinAndSelect("text_mention.target", "text_mention_target")
                .leftJoin("text_mention_target.business", "text_mention_target_business")
                .addSelect("text_mention_target_business.id")
                .addSelect("text_mention_target_business.photo")
                .addSelect("text_mention_target_business.name")
                .addSelect("text_mention_target_business.pageName")
                .leftJoin("text_mention_target.person", "text_mention_target_person")
                .addSelect("text_mention_target_person.id")
                .addSelect("text_mention_target_person.photo")
                .addSelect("text_mention_target_person.fullName")
                .addSelect("text_mention_target_person.pageName")
                .leftJoinAndMapOne(`text.liked`, `text.likes`, `text_likes`, `text_likes.user_id = ${userId || user.baseBusinessAccount || user.id}`)
                .andWhere("text.user = :user", {user: userId || user.baseBusinessAccount || user.id  })
                .andWhere("text.deleted IS NULL")
                .andWhere("text.draft = :draft", { draft })
                .orderBy("text.created", "DESC")
                .getManyAndCount();

            res.status(200).send({ items: data[0], total: data[1] });
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getSingleText = async (req: any, res: any) => {
        try {
            const {
                user,
                params: { id }
            } = req;

            if (!id) {
                return res.status(400).send({error: `Id wasn't provided`});
            }

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canRead) {
                    return res.status(403).send({error: 'Forbidden'});
                }
            }

            const textRepo = getRepository(Text);
            const query = AbstractController.getSinglePhotoVideoArticle(textRepo, 'plain_text', id, user.baseBusinessAccount || user.id);

            console.log('TEXT: ');

            const text: any = await query.getOne();

            if(!text) {
                return res.status(404).send({error: 'Not Found'});
            }

            if(+text.user.id !== +user.id) {
                const target = text.user;
                const account = user.id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    return res.status(403).send({error: 'Private account'});
                }
            }

            res.status(200).send(text);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    }

    static deleteText = async(req, res) => {
        const user = req.user;
        const textRepo = getRepository(Text);
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const textId = req.params.id;
            if (textId) {
                await textRepo.update({id: textId }, {deleted: dateTime()});
                res.status(200).send({status: "success"});
            } else {
                res.status(400).send({error: "No ID field found"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static updateText = async(req, res) => {
        const user = req.user;
        const textRepo = getRepository(Text);
        try {

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const textObj = req.body as Text;
            if (textObj.id) {
                await textRepo.update({
                    id: textObj.id,
                }, {
                    text: textObj.text,
                    draft: textObj.draft,
                    updated: dateTime()
                });
                const updatedText = await textRepo.findOne(textObj.id);
                await getHashtagRepository().updateHashtagsByPostId("text", updatedText.id, req.body.hashtags);
                updatedText.mentions = await getMentionRepository().addMention("text", updatedText, req.body.mentions,'text', user);

                res.status(200).send(updatedText);
            } else {
                res.status(400).send({error: "No ID field found"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static likeText = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
            } = req;

            const likesRepo = getRepository(Text_Like);

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere("like.text_ = :id", { id })
                .getOne();

            const respObj = {
                status: "success",
                liked: false,
                content: null
            };

            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
            } else {
                const likeObj = new Text_Like();
                likeObj.user = userID;
                likeObj.text_ = id;
                respObj.content = await likesRepo.save(likeObj);

                respObj.liked = true;

                const textRepo = getRepository(Text);
                const query = AbstractController.getSinglePhotoVideoArticleForNotification(textRepo, 'plain_text', id, req.user.baseBusinessAccount || req.user.id)
                const text: any = await query.getOne()

                await NotificationController.addNotification(
                    'liked',
                    'text',
                    'text',
                    text,
                    issuer,
                    text.user
                )
            }




            res.status(200).send(respObj);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getTextLikes = async(req, res) => {
        try {
            const {
                user,
                query: { page = 1, limit = 15 },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Text_Like)
                .createQueryBuilder("like")
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .leftJoinAndMapOne(`like_user.following`, `like_user.followers`, `like_user_followers`, `like_user_followers.account = ${user.baseBusinessAccount || user.id}`)
                .where("like.text_ = :id", { id })
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static addComment = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: {
                    text,
                    replyTo,
                    mentions = []
                },
            } = req;

            if (!text) throw Error("Empty text field");

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const commentRepo = getRepository(Text_Comment);
            const data: any = {};

            const textRepo = getRepository(Text);
            // const query = replyTo ?
            // AbstractController.getSingleComment(commentRepo, 'plain_text', replyTo, req.user.baseBusinessAccount || req.user.id)
            //  : AbstractController.getSinglePhotoVideoArticle(textRepo, 'plain_text', id, req.user.baseBusinessAccount || req.user.id)
            const query = AbstractController.getSinglePhotoVideoArticleForNotification(textRepo, 'plain_text', id, req.user.baseBusinessAccount || req.user.id)

            const textSearch: any = await query.getOne()


            if (replyTo) {
                const repliesRepo = getRepository(Text_Comment_Replies);

                const comment = await commentRepo
                    .createQueryBuilder()
                    .where("id = :id", { id: replyTo })
                    .getOne();

                if (!comment) throw Error("Comment not exist");

                const reply = new Text_Comment_Replies();
                reply.user = userID;
                reply.comment_ = replyTo;
                reply.plain_text_ = id;
                reply.text = text;
                data.comment = await repliesRepo.save(reply);
                data.comment.mentions = await getMentionRepository().addMention("text_comment_reply",textSearch, mentions, 'text', issuer, data.comment);
            } else {
                const comment = new Text_Comment();
                comment.user = userID;
                comment.plain_text_ = id;
                comment.text = text;
                data.comment = await commentRepo.save(comment);
                data.comment.mentions = await getMentionRepository().addMention("text_comment", textSearch, mentions,'text', issuer, data.comment);
            }


            const action = replyTo ? 'replied' : 'commented'
            const content_display_name = replyTo ? 'comment' : 'text'
            // const text_content_type: string = replyTo ? "text_comment_reply" : "text_comment";


            await NotificationController.addNotification(
                action,
                content_display_name,
                'text',
                textSearch,
                issuer,
                textSearch.user
            )


            res.status(200).send(data);

        } catch (error) {
            console.log('text_addComment error', error)
            res.status(400).send({error: error.message});
        }
    };

    static editComment = async (req, res) => {
        return await AbstractController.editComment(
            req,
            res,
            Text_Comment,
            Text_Comment_Replies,
            'text_comment_reply',
            'text_comment',
        )
    }

    static getComments = async(req, res) => {
        return await AbstractController.getComments(req, res, Text_Comment, 'plain_text_', 'text');
    };

    static getReplies = async(req, res) => {
        return await AbstractController.getReplies(req, res, Text_Comment_Replies, 'text');
        // try {
        //     const {
        //         user,
        //         params: { id },
        //         query: { page = 1, filterBy = "date", limit = 15, order = "asc" }
        //     } = req;

        //     const replyRepo = getRepository(Text_Comment_Replies);

        //     const filter: Filter = {
        //         by: filterBy.toLowerCase() === "likes" ? "reply_total_likes" : "reply_created",
        //         order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
        //     };
        //     const limitNumb = Number(limit);
        //     const skip = (Number(page) - 1) * limitNumb;

        //     const replies: any = await replyRepo
        //         .createQueryBuilder("reply")
        //         .leftJoinAndSelect("reply.user", "user")
        //         .leftJoin("user.business", "user_business")
        //         .addSelect("user_business.id")
        //         .addSelect("user_business.photo")
        //         .addSelect("user_business.name")
        //         .addSelect("user_business.pageName")
        //         .leftJoin("user.person", "user_person")
        //         .addSelect("user_person.id")
        //         .addSelect("user_person.photo")
        //         .addSelect("user_person.fullName")
        //         .addSelect("user_person.pageName")
        //         .leftJoinAndSelect("reply.mentions", "reply_mention")
        //         .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
        //         .leftJoin("reply_mention_target.business", "reply_mention_target_business")
        //         .addSelect("reply_mention_target_business.id")
        //         .addSelect("reply_mention_target_business.photo")
        //         .addSelect("reply_mention_target_business.name")
        //         .addSelect("reply_mention_target_business.pageName")
        //         .leftJoin("reply_mention_target.person", "reply_mention_target_person")
        //         .addSelect("reply_mention_target_person.id")
        //         .addSelect("reply_mention_target_person.photo")
        //         .addSelect("reply_mention_target_person.fullName")
        //         .addSelect("reply_mention_target_person.pageName")
        //         .addSelect("reply.created", "reply_created")
        //         .leftJoinAndMapOne(`reply.liked`, `reply.likes`, `reply_likes`, `reply_likes.user_id = ${user.baseBusinessAccount || user.id}`)
        //         .andWhere("reply.comment_ = :id", { id })
        //         .skip(skip)
        //         .take(limitNumb)
        //         .orderBy( filter.by, filter.order)
        //         .getMany();

        //     res.status(200).send({ replies });

        // } catch (error) {
        //     res.status(400).send({error: error.message});
        // }
    };

    static likeComment = async(req, res) => {
        try {
            const {
                user: { id: userID },
                params: { id },
                body: { reply },
            } = req;

            const likesRepo = getRepository(Text_Comment_Like);

            const commentContext = reply ? Text_Comment_Replies : Text_Comment
            const textRepo = getRepository(commentContext);
            const query = AbstractController.getSingleComment(textRepo, 'plain_text', id, req.user.baseBusinessAccount || req.user.id)
            const comment: any = await query.getOne()

            const issuer = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: userID })

            const like = await likesRepo
                .createQueryBuilder("like")
                .where("like.user = :userID", { userID })
                .andWhere(`like.${reply ?"reply_" : "comment_"} = :id`, { id })
                .getOne();

            let liked: boolean;
            let content;
            if (like) {
                await likesRepo
                    .createQueryBuilder()
                    .delete()
                    .where("id = :likeID", { likeID: like.id })
                    .execute();
                liked = false;
            } else {
                const likeObj = new Text_Comment_Like();
                likeObj.user = userID;
                likeObj.plain_text_ = comment.plain_text_
                if (reply) {
                    likeObj.reply_ = comment;
                } else {
                    likeObj.comment_ = comment;
                }
                content = await likesRepo.save(likeObj);
                liked = true;


                await NotificationController.addNotification(
                    'liked',
                    'comment',
                    'text',
                    comment.plain_text_,
                    issuer,
                    comment.user
                )
            }

            res.status(200).send({liked});

        } catch (error) {
            console.log('text_comment_like error', error)
            res.status(400).send({error: error.message});
        }
    };

    static getCommentLikes = async(req, res) => {
        try {
            const {
                query: { page = 1, limit = 15, reply = false },
                params: { id }
            } = req;

            const pageParams: any = {};
            pageParams.limit = Number(limit);
            pageParams.offset = (Number(page) - 1) * pageParams.limit;

            const likedBy = await getRepository(Text_Comment_Like)
                .createQueryBuilder("like")
                .where(`like.${reply ? "reply_" : "comment_"} = :id`, { id })
                .leftJoinAndSelect("like.user", "like_user")
                .leftJoin("like_user.business", "like_user_business")
                .addSelect("like_user_business.id")
                .addSelect("like_user_business.photo")
                .addSelect("like_user_business.name")
                .addSelect("like_user_business.pageName")
                .leftJoin("like_user.person", "like_user_person")
                .addSelect("like_user_person.id")
                .addSelect("like_user_person.photo")
                .addSelect("like_user_person.fullName")
                .addSelect("like_user_person.pageName")
                .skip(pageParams.offset)
                .take(pageParams.limit)
                .getMany()
                .then(likes => likes.map(like => like.user));

            res.status(200).send(likedBy);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteComment = async(req, res) => {
        try {
            const {
                query: { reply = false },
                params: { id },
                user
            } = req;

            if(user.business) {
                const permissions = await getAccountRepository().checkPermissionsForBusiness(user)
                if(!permissions.canWrite) return res.status(403).send({error: 'Forbidden'});
            }

            const repo = reply ? getRepository(Text_Comment_Replies) : getRepository(Text_Comment);

            const deleted = await repo.delete({ id });

            if (deleted.affected) {
                res.status(200).send({ status: "success" });
            } else {
                res.status(404).send({ status: "comment not found" });
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };
}
