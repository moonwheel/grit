const express = require("express");
const router = express.Router();

import {BookingController} from "./controller";
import passport = require("passport");

router.post("/", passport.authenticate("jwt", {session: false}), BookingController.addBooking);

router.get("/", passport.authenticate("jwt", {session: false}), BookingController.getBookings);

router.post("/:id/cancel", passport.authenticate("jwt", {session: false}), BookingController.cancel);

router.post("/:id/refund", passport.authenticate("jwt", {session: false}), BookingController.refund);

export = router;
