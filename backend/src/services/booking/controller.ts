import getBookingRepository from "../../entities/repositories/booking-repository";
import getServiceRepository from "../../entities/repositories/service-repository";

import { calculateBookingHours } from "../../utils/booking-helper";
import { NotificationController } from "../notifications/controller";
import { Service } from "../../entities/service";

export class BookingController {

    static addBooking = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                body: {
                    serviceId,
                    payment,
                    appointments,
                    grit_wallet,
                },
            } = req;

            const service = await getServiceRepository().getServiceById(serviceId);

            let totalPrice = service.price;

            const totalTime = calculateBookingHours(appointments);

            if (service.schedule) {
                totalPrice = totalTime * Number(service.price);
            }

            const data = await getBookingRepository()
                .addBooking(userId, service, payment, totalPrice, totalTime, appointments, grit_wallet);

            await NotificationController.addNotification(
                'booked',
                'service',
                'service',
                service,
                userId,
                service.user // owner (who receive the notification)
            )

            res.status(201).json({ data });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getBookings = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
            } = req;

            const bookings = await getBookingRepository().getBookings(userId);

            res.status(200).send(bookings);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static cancel = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: bookingId,
                },
                body: {
                    reason,
                    description,
                },
            } = req;

            // console.log('cancel booking', bookingId, userId, reason, description)
            const updatedBooking = await getBookingRepository()
                .cancel(bookingId, userId, reason, description);

            const booking = await getBookingRepository().getBookingById( bookingId, userId)

            await NotificationController.addNotification(
                'cancelled',
                'booking',
                'service',
                booking.service as Service,
                userId,
                booking.seller.id === userId ? booking.buyer : booking.seller  // owner (who receive the notification)
            )

            res.status(200).send(updatedBooking);
        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    }

    static refund = async (req, res) => {
        try {
            const {
                user: {
                    id: userId,
                },
                params: {
                    id: bookingId,
                },
                body: {
                    bankAccountId,
                },
            } = req;

            // await mangoPayment.refundTranfer(transaction);
            // const { walletId } = await paymentProcess.getMangoCreatentialsByUserId(seller.id);
            // if can refund from wallet id
            // KYC validation
            // await mangoPayment.payOut({
            //     userId,
            //     walletId,
            //     bankAccountId,
            //     amount: total_price,
            // });

            // TODO CHECK hook PAYOUT_REFUND_SUCCEEDED

            const updatedBooking = await getBookingRepository()
                .refund(userId, bookingId);

            res.status(200).send(updatedBooking);
        } catch (error) {
            console.log('ERROR: ', error);
            res.status(400).send({ error: error.message });
        }
    }
}
