const dateTime = require("date-time");
import { Address } from "../../entities/address";
import getAddressRepository from "../../entities/repositories/address-repository";
import { Geocoding } from "../../utils/geocoding";
import { Request, Response } from "express";

export class AddressController {

    static addAddress = async (req: Request, res: Response) => {
        const account = req.user;
        const address = req.body as Address;
        const concatenatedAdddess =  encodeURI(`${address.street}, ${address.city}, ${address.zip}`);

        try {
            const geocodedAddress = await Geocoding.tomTomGeocode(concatenatedAdddess);
            // additional geocoring API
            // const result = await Geocoding.locationIq(req.query.text || 'Some str, Some City');

            if(geocodedAddress && geocodedAddress.results && geocodedAddress.results[0]) {
                console.log('geocoded addr', geocodedAddress.results[0] )
                address.lat = geocodedAddress.results[0].position.lat;
                address.lng = geocodedAddress.results[0].position.lon;
            }
            const newAddress = await getAddressRepository().addAddress({address, account});
            res.status(200).send(newAddress);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getAddresses = async (req: Request, res: Response) => {
        const account = req.user;
        let addrType = null;

        if (Object.keys(req.query)) {
            addrType = req.query.type
        }

        try {
            const savedAddresses = await getAddressRepository().getAccountAddresses(account, addrType);
            res.status(200).send(savedAddresses);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static updateAddress = async(req, res) => {
        try {
            const {
                user: account,
                params: { id },
                body: address,
            } = req;
            const includes = getAddressRepository().checkOwnership(id, account);
            if (includes) {
                await getAddressRepository().update(id, address);
                const newAddress = await getAddressRepository().findOne(id);
                res.status(200).send(newAddress);
            } else {
                res.status(400).send({error: "Address not found"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static deleteAddress = async(req, res) => {
        try {
            const {
                params: { id },
                user: account
            } = req;
            const includes = await getAddressRepository().checkOwnership(id, account);
            if (includes) {
                await getAddressRepository().update(id, {deleted: dateTime()});
                res.status(200).send({status:"success"});
            } else {
                res.status(404).send({error: "address not found"});
            }
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };
}
