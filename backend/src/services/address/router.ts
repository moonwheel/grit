const express = require("express");
const router = express.Router();

import { AddressController } from "./controller";
import passport = require("passport");

router.post("/", passport.authenticate("jwt", {session: false}), AddressController.addAddress);

router.get("/", passport.authenticate("jwt", {session: false}), AddressController.getAddresses);

router.patch("/:id", passport.authenticate("jwt", {session: false}), AddressController.updateAddress);

router.delete("/:id", passport.authenticate("jwt", {session: false}), AddressController.deleteAddress);

export = router;
