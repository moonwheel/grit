import uniqid = require("uniqid");
import { IncomingForm } from "formidable";
import { App } from "../../config/app.config";
import * as fs from 'fs';
import getReviewRepository from "../../entities/repositories/review-repository";
import { VideoController } from "../video/controller";
import { tusServer, TUS_EVENTS, decodeMetadata, UploadedFileMetadata } from '../../utils/tus-server';
import getMentionRepository from "../../entities/repositories/mention-repository";
import getAccountRepository from "../../entities/repositories/account-repository";
import { ConvertedPhoto, MediaConverter } from "../../utils/media-converter";
import { getRepository } from "typeorm";
import { publish } from "../../utils/sse";
import getServiceRepository from "../../entities/repositories/service-repository";
import { ReviewPhoto } from "../../entities/review_photo";
import { NotificationController } from "../notifications/controller";
import { NotificationContentType } from "../../entities/notification";

tusServer.on(TUS_EVENTS.EVENT_UPLOAD_COMPLETE, async (event) => {
    const metadata = decodeMetadata(event.file.upload_metadata);

    if (
        metadata.entityType === 'service_review'
        || metadata.entityType === 'seller_review'
        || metadata.entityType === 'product_review'
    ) {
        console.log(`upload ${metadata.entityType} completed metadata`, metadata)
        console.log(`upload ${metadata.entityType} completed event`, event)
        const tmpFileId = event.file.id
        await ReviewController.onUploadEnd(tmpFileId, metadata, metadata.entityType)
    }
});

export class ReviewController {
    static addReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: reviewData
            } = req;

            const mentions = reviewData.mentions || [];

            delete reviewData.mentions;

            const review = await getReviewRepository().addReview(entity, id, user, reviewData);

            review.mentions = await getMentionRepository().addMention(`${entity}review`, review, mentions,`${entity}review`, user);

          //   await NotificationController.addNotification(
          //     'commented',
          //     'review',
          //     `${entity}_review` as NotificationContentType,
          //     reply.review_,
          //     user,
          //     reply.review_.user
          // );

            res.status(200).send(review);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static uploadReviewPhotos = entity => async (req, res) => {
        if(req.user.business) {
            const permissions = await getAccountRepository().checkPermissionsForBusiness(req.user)
            if(!permissions.canWrite) {
                return res.status(403).send({error: 'You have no power here'});
            }
        }
        return tusServer.handle(req, res);
    }

    static onUploadEnd = async (
        tmpFileId: number,
        metadata: UploadedFileMetadata,
        entity: string,
    ) => {
        let newFilePath: string;
        let outputFiles: ConvertedPhoto;

        try {
            const reviewPhotoId = +metadata['grit-file-entity-id'];
            const { id, type, userId } = metadata;
            const photoRepo = getRepository(ReviewPhoto);
            const photo = new ReviewPhoto();

            let entityType = '';

            if (entity === 'seller_review') {
                entityType = 'seller';
            } else if (entity === 'product_review') {
                entityType = 'product';
            } else if (entity === 'service_review') {
                entityType = 'service';
            }

            // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
            const oldFilePath = `${App.UPLOAD_FOLDER}/${tmpFileId}`;

            const uniqueFileName = uniqid("photo-");
            const spliType = (type as string).split('/');
            const fileFormat = spliType[1];

            const fullFileName = `${uniqueFileName}.${fileFormat}`;

            newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;

            fs.renameSync(oldFilePath, newFilePath);

            MediaConverter.convertPhotoAndUploadOnMinio(newFilePath, uniqueFileName, 750, 250)
                .then(async () => {
                    photo.link = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-photo.jpg`
                    photo.thumb = `${App.MINIO_PUBLIC_URL}/photo/${uniqueFileName}-thumb.jpg`

                    photo[`${entityType}Review`] = id;

                    const data = await photoRepo.save(photo);

                    const ReviewEntity = await getReviewRepository().getEntity(entityType);
                    const entityRepo = getRepository(ReviewEntity);
                    const entityData = await entityRepo.findOne(id);

                    publish({ userId, type: 'file_processing_complete', entityType: 'review', entity: entityData });
                })

        } catch (error) {
            console.error('Error while service photo uploading' ,error);
        }
    }

    static addReviewReply = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: replyData,
            } = req;

            const mentions = replyData.mentions || [];

            delete replyData.mentions;

            const reply = await getReviewRepository().addReply(entity, user, replyData);

            reply.mentions = await getMentionRepository().addMention(`${entity}_review_reply`, reply, mentions,`${entity}review`, user, reply);

            await NotificationController.addNotification(
                'commented',
                'review',
                `${entity}_review` as NotificationContentType,
                reply.review_,
                user,
                reply.review_.user
            );

            res.status(200).send(reply);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            const review = await getReviewRepository().getById(entity, id, user);
            res.status(200).send(review);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getReplies = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            const review = await getReviewRepository().getReplies(entity, id, user);
            res.status(200).send(review);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getAllReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
                query: {
                    page = 1,
                    filter = '',
                    filterBy: orderBy = 'date',
                    limit = 15,
                    order = 'desc',
                    rating = 'all',
                },
            } = req;
            const reviews = await getReviewRepository().getAll(
                entity, id, user, {
                    page,
                    limit,
                    order,
                    orderBy,
                    filter,
                    rating,
                });
            res.status(200).send(reviews);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static removeReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            const reviews = await getReviewRepository().removeReview(entity, id, user);
            res.status(200).send(reviews);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static updateReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: reviewData
            } = req;
            const mentions = reviewData.mentions || [];
            const review = await getReviewRepository().updateReview(entity, id, user, reviewData);
            review.mentions = await getMentionRepository().addMention(`${entity}_review`, review, mentions,`${entity}_review`, user);
            res.status(200).send(review);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static updateReviewReply = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
                body: replyData
            } = req;
            const mentions = replyData.mentions || [];
            const reply = await getReviewRepository().updateReviewReply(entity, id, user, replyData);
            reply.mentions = await getMentionRepository().addMention(`${entity}_review_reply`, reply, mentions,`${entity}_review`, user, reply);
            res.status(200).send(reply);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static removeReviewReply = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            await getReviewRepository().removeReviewReply(entity, id, user);
            res.status(204).send({});
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static addReviewVideo = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            await getReviewRepository().checkOwnership(entity, id, user);
            const form = new IncomingForm();
            const videoData: any = {};

            form.parse(req, async function(err, fields, files) {
                try {
                    if (fields.title) videoData.title = fields.title;

                    if (!videoData.cover) {
                        // videoData.cover = await VideoController.getVideoCover(videoData);
                    }
                    // videoData.duration  = await VideoController.getVideoDuration(videoData) as string;
                    const review = await getReviewRepository().saveReviewVideos(entity, id, videoData, user);
                    res.status(200).send(review);
                } catch (error) {
                    throw error;
                }
            });

            form.on("fileBegin", function (name, file){
                const fileType = file.type.split("/");
                if (fileType[0] === "image") {
                    const url = uniqid("cover-");
                    const coverUrl = App.UPLOAD_FOLDER + "/" + url + "." + fileType[1];
                    file.path = coverUrl;
                    videoData.cover = coverUrl.substring(2, coverUrl.length);
                }
                if (fileType[0] === "video") {
                    const url = uniqid("video-");
                    const videoFormat = App.VIDEO_FORMATS.get(fileType[1]);
                    const videoUrl = App.UPLOAD_FOLDER + "/" + url + videoFormat;
                    file.path = videoUrl;
                    videoData.video = videoUrl.substring(2, videoUrl.length);
                }
            });

            form.on("file", function (name, file){

            });
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static removeReviewPhoto = async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            await getReviewRepository().removeReviewPhoto(id, user);
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static removeReviewVideo = async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;
            await getReviewRepository().removeReviewVideo(id, user);
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static likeReview = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;

            const liked = await getReviewRepository().likeReview(entity, id, user);
            const review = await getReviewRepository().getById(entity, id, user);

            await NotificationController.addNotification(
                'liked',
                'review',
                `${entity}_review` as NotificationContentType,
                review,
                user,
                review.user
            );

            res.status(200).send({ status: "success", liked });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static likeReviewReply = entity => async (req, res) => {
        try {
            const {
                user,
                params: { id },
            } = req;

            const liked = await getReviewRepository().likeReviewReply(entity, id, user);
            const reply = await getReviewRepository().getReplyById(entity, id);

            await NotificationController.addNotification(
                'liked',
                'review_reply',
                `${entity}_review` as NotificationContentType,
                reply.review_,
                user,
                reply.user
            );

            res.status(200).send({ status: "success", liked});
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getLikes = entity => async (req, res) => {
        try {
            const {
                params: { id },
                query: { page = 1, limit = 15 }
            } = req;

            const likes = await getReviewRepository().getLikes(entity, id, { page, limit});
            res.status(200).send(likes);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static getReplyLikes = entity => async (req, res) => {
        try {
            const {
                params: { id },
                query: { page = 1, limit = 15 }
            } = req;

            const likes = await getReviewRepository().getReplyLikes(entity, id);
            res.status(200).send(likes);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };
}
