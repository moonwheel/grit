const express = require("express");
import passport = require("passport");
import { ReviewController } from "./controller";

export default entity => {
    const router = express.Router();

    router.post("/:id/review", passport.authenticate("jwt", {session: false}), ReviewController.addReview(entity));
    router.get("/:id/review", passport.authenticate("jwt", {session: false}), ReviewController.getAllReview(entity));
    router.use("/review/:id/upload", passport.authenticate("jwt", {session: false}), ReviewController.uploadReviewPhotos(entity));
    router.post("/review/:id/reply", passport.authenticate("jwt", {session: false}), ReviewController.addReviewReply(entity));
    router.patch("/review/reply/:id", passport.authenticate("jwt", {session: false}), ReviewController.updateReviewReply(entity));
    router.delete("/review/reply/:id", passport.authenticate("jwt", {session: false}), ReviewController.removeReviewReply(entity));
    router.post("/review/reply/:id/like", passport.authenticate("jwt", {session: false}), ReviewController.likeReviewReply(entity));
    router.get("/review/reply/:id/like", passport.authenticate("jwt", {session: false}), ReviewController.getReplyLikes(entity));
    router.get("/review/:id", passport.authenticate("jwt", {session: false}), ReviewController.getReview(entity));
    router.get("/review/:id/replies", passport.authenticate("jwt", {session: false}), ReviewController.getReplies(entity));
    router.patch("/review/:id", passport.authenticate("jwt", {session: false}), ReviewController.updateReview(entity));
    router.post("/review/:id/like", passport.authenticate("jwt", {session: false}), ReviewController.likeReview(entity));
    router.get("/review/:id/like", passport.authenticate("jwt", {session: false}), ReviewController.getLikes(entity));
    router.delete("/review/:id", passport.authenticate("jwt", {session: false}), ReviewController.removeReview(entity));
    router.patch("/review/:id/video", passport.authenticate("jwt", {session: false}), ReviewController.addReviewVideo(entity));
    router.delete("/review/photo/:id", passport.authenticate("jwt", {session: false}), ReviewController.removeReviewPhoto);
    router.delete("/review/video/:id", passport.authenticate("jwt", {session: false}), ReviewController.removeReviewVideo);

    return router;
};
