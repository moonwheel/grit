import * as express from "express";
const router = express.Router();

import passport = require("passport");
import { UserController } from "./controller";
import blockRouter from "../block/router";
import followingRouter from "../following/router";
import feedRouter from "../feed/router";

router.post("/login", UserController.userLogin);

router.get("/token", passport.authenticate("jwt", {session: false}), UserController.tokenInfo);

router.post("/token", UserController.renewToken);

router.post("/register", UserController.userRegister);

router.get("/forgotpass", passport.authenticate("jwt", {session: false}), UserController.forgotPassword);

router.post("/forgotpassnoauth", UserController.forgotPasswordWithoutAuth);

router.post("/changepass", passport.authenticate("jwt", {session: false}), UserController.changePassword);

router.post("/changepassnoauth", UserController.resetPassword);

router.get("/resend/mverify", passport.authenticate("jwt", {session: false}), UserController.resendEmail);

router.get("/mverify", UserController.mailVerification);

router.get("/pverify", UserController.passwordVerification);

router.get("/", passport.authenticate("jwt", {session: false}), UserController.getUser);

router.get("/timeline", passport.authenticate("jwt", {session: false}), UserController.getTimeLine);

router.use(blockRouter);
router.use(followingRouter);
router.use(feedRouter);

router.patch("/switch/:id", passport.authenticate("jwt", {session: false}), UserController.switchAccount);

router.patch("/language", passport.authenticate("jwt", {session: false}), UserController.switchLanguage);

router.put("/", passport.authenticate("jwt", {session: false}), UserController.updateUser);

router.post("/deactivate", passport.authenticate("jwt", {session: false}), UserController.deactivatePerson);

router.get("/reactivate/:token", UserController.reactivatePerson);

router.get("/search", passport.authenticate("jwt", {session: false}), UserController.findUser);

router.get("/username/:username", passport.authenticate("jwt", {session: false}), UserController.getUserByUsername);
router.get("/:id", passport.authenticate("jwt", {session: false}), UserController.getUserById);

router.post("/kyc/validate", passport.authenticate("jwt", { session: false }), UserController.validateKycDocument);

export = router;
