import { CompletelyRemoveUser } from './../../jobs/remove-user';
const dateTime = require("date-time");

import "reflect-metadata";
import passport = require("passport");
import argon2 = require('argon2');
import jwt = require("jsonwebtoken");
import emailValidator = require("email-validator");
import { App } from "../../config/app.config";
import { Keys } from "../../utils/keys";
import { Person } from "../../entities/person";
import { getRepository, Like, IsNull, getConnection, getTreeRepository } from "typeorm";
import { PhotoController } from "../photo/controller";
import { Account } from "../../entities/account";
import { Helpers } from "../../utils/helpers";
import getAccountRepository from "../../entities/repositories/account-repository";
import mailer from "../../utils/mailer";
import uniqid = require("uniqid");
import { Request, Response } from "express";
import { Token } from "../../interfaces/global/token.interface";
import { AccountToken } from "../../interfaces/global/account-token.interface";
import { BusinessController } from "../business/controller";
import getFollowingRepository from '../../entities/repositories/following-repository';
import mangoPay from '../../utils/mango-payment';
import paymentProcess from '../../utils/payment-process';
import { Business } from '../../entities/business';
import { emailTexts } from '../../config/email-texts';

interface UserAccountWithBusiness extends Account {
    businessBaseAccount?: Account[]
}

export class UserController {
    static userRegister = async (req, res) => {
        try {
            const {
                user,
            }: {
                user: Person;
            } = req.body;

            if (user.email) {
                user.email = user.email.toLowerCase();
            }

            if (
                UserController.userValidation(user) &&
                emailValidator.validate(user.email)
            ) {
                if (await Helpers.getUserByEmail(user.email))
                    throw Error("User with this email already exists in database");

                const passwordHash = await argon2.hash(user.password);
                const mailVerifyToken = UserController.createVerifyToken(
                    user.email,
                    "email"
                );

                const { pageName } = await getAccountRepository().checkUniquePageName(user.fullName);

                // console.log('pageName', pageName)

                const { data: { Id: mangoUserId } } = await mangoPay.createNaturalUser({
                    email: user.email,
                    lastName: user.fullName,
                    firstName: user.fullName,
                    nationality: "GE",
                    countryOfResidence: "GE"
                });

                const { data: { Id: walletId } } = await mangoPay.createWallet(mangoUserId, "EUR");
                const { data: { Id: bankAliasId } } = await mangoPay.createBankAlias(mangoUserId, user.fullName, walletId);

                user.pageName = pageName;
                user.password = passwordHash;
                user.token = mailVerifyToken;
                user.wallet = Number(walletId);
                user.mangoUserId = Number(mangoUserId);
                user.bankAliasId = Number(bankAliasId);

                const savedUser = await getRepository(Person).save(user);
                console.log('savedUser', savedUser)
                await getAccountRepository().createUserAccount(savedUser);

                try {
                    const link = `http://${req.get("host")}/user/mverify?token=${
                        savedUser.token
                    }`;
                    mailer({
                        to: savedUser.email,
                        // subject: "Mail verification!",
                        // html: `Hello,<br> Please Click on the link to verify your email.<br><a href="${link}">${link}</a>`,
                        subject: emailTexts('confirmEmail', savedUser.language, 'subject'),
                        html: emailTexts('confirmEmail', savedUser.language, 'html', link),
                    });
                } catch (error) {}

                res.status(200).send({ status: "success" });
            } else {
                throw Error("User info is not valid");
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static resendEmail = async (req, res) => {
        try {
            const user: AccountToken = req.user;
            const person = await getRepository(Person).findOne(user.person);
            if (!person) throw Error("User not found");
            const canSend = new Date().getTime() - new Date(person.tokenAttemptsStamp).getTime() > App.MALL_ATTEMPTS_TIME_LIMIT_MS
            if (person.tokenAttempts === 3 && !canSend)
                throw Error("Maximum retries reached");
            const mailVerifyToken = await UserController.createVerifyToken(
                person.email,
                "email"
            );
            await getRepository(Person).update(
                { id: user.person },
                {
                    token: mailVerifyToken,
                    tokenAttempts: canSend ? 1 : person.tokenAttempts + 1,
                    tokenAttemptsStamp: canSend ? new Date() : person.tokenAttemptsStamp,
                }
            );
            const link = `http://${req.get("host")}/user/mverify?token=${mailVerifyToken}`;
            await mailer({
                to: person.email,
                // subject: "Mail verification!",
                // html: `Hello,<br> Please Click on the link to verify your email.<br><a href="${link}">${link}</a>`,
                subject: emailTexts('confirmEmail', person.language, 'subject'),
                html: emailTexts('confirmEmail', person.language, 'html', link),
            });
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static switchAccount = async (req, res) => {
        try {
            const {
                params: { id },
                user,
            } = req;

            const account = await getAccountRepository().getAccountForToken({
                id,
                person: user.person,
            });
            if (!account) throw Error("User not related to the company");

            const token = UserController.createVerifyToken(account);
            const refreshToken = UserController.createVerifyToken(account.id, "refresh");
            res.status(200).send({ token, refreshToken });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static switchLanguage = async (req, res) => {
        try {
            const {
                user,
                body: { language }
            } = req;

            if (!language) throw Error("language field is empty");
            await getRepository(Person).update(user.person, { language });

            res.status(200).send({ status: "success", language });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static userValidation = (user: Person) => {
        return (
            user.email &&
            user.password &&
            user.fullName &&
            user.birthday &&
            user.gender
        );
    };

    static mailVerification = async (req, res) => {
        const data: Token = jwt.decode(req.query.token) as any;

        try {
            if (data.expires && data.expires >= Date.now()) {
                if (data.identifier) {
                    const user = await Helpers.getUserByEmail(data.identifier as string);
                    await getRepository(Person).update({ id: user.id }, { active: true });
                    res.redirect(App.FRONTEND_HOST + "/feed");
                } else {
                    res.set("Content-Type", "text/html");
                    res.send(new Buffer("<h2>Sorry, your token is invalid</h2>"));
                }
            } else {
                res.set("Content-Type", "text/html");
                res.send(new Buffer("<h2>Sorry, token expired</h2>"));
            }
        } catch (error) {
            res.set("Content-Type", "text/html");
            res.send(new Buffer("<h2>Sorry, your token is invalid</h2>"));
        }
    };

    static userLogin = (req, res) => {
        passport.authenticate(
            "local",
            { session: false },
            (error, user: AccountToken) => {
                if (error || !user) {
                    if (
                        error.message === "Cannot read property 'password' of undefined"
                    ) {
                        res.status(400).send({ error: "Incorrect Username / Password" });
                    } else {
                        res.status(400).send({ error: error });
                    }
                    return;
                }
                UserController.performLogin(user, req, res);
            }
        )(req, res);
    };

    static renewToken = async (req, res) => {
        try {
            const { headers: {refreshtoken: refreshToken} } = req;
            if(!refreshToken) {
                throw new Error("Refresh token must be provided");
            }
            const jwtPayload = jwt.verify(refreshToken, Keys.publicKey(), {algorithms: ["RS256"]});
            if (!jwtPayload || jwtPayload["type"] !== "refresh" || !jwtPayload["identifier"]) {
                throw new Error("Incorrect refresh token");
            }
            if (!jwtPayload["expires"] || Date.now() > jwtPayload["expires"]) {
                throw new Error("jwt expired");
            }
            const user = await getAccountRepository().getAccountForToken({ id: jwtPayload["identifier"] });
            if (!user) {
                throw new Error("Incorrect refresh token");
            }
            UserController.performLogin(user, req, res);
        } catch (e) {
            res.status(400).send({error: `Can't renew token`});
        }
    };

    static performLogin = (user: AccountToken, req, res) => {
        req.login(user, { session: false }, (error: any) => {
            // console.log('performLogin user', user)
            if (error) {
                res.status(400).send({ error });
            }
            const token = UserController.createVerifyToken(user);
            const refreshToken = UserController.createVerifyToken(user.id, "refresh");
            res.status(200).send({
                token,
                refreshToken,
                deactivated: user.deactivated || undefined,
                reactivationtoken: user.reactivationtoken
             });
        });
    };

    static tokenInfo = (req, res) => {
        const { user } = req;
        res.status(200).send({user});
    };

    static getUser = async (req, res) => {
        const { id } = req.user;

        const [responseUser, cartProductsCount] = await Promise.all([
            getAccountRepository().getFullAccountInfo({key: 'id', val: id}),
            getAccountRepository().getCountOfCardProducts(id)
        ])

        res.status(200).send({
            ...responseUser,
            cartProductsCount
        });
    };

    static updateUser = async (req, res) => {
        const user: AccountToken = req.user;
        const newUser = req.body;
        const personRepo = getRepository(Person);

        try {
            if (newUser.password)
                throw Error("You can't update the password in this case");
            if (newUser.email && await Helpers.getUserByEmail(newUser.email))
                throw Error("User with this email already exists in database");
            if (newUser.photoData || newUser.isDeleted) {
                const photoAndThumbData = await PhotoController.updateProfilePhoto(
                    newUser.photoData,
                    user.person,
                    newUser.isDeleted
                );
                if (photoAndThumbData) {
                    newUser.photo = photoAndThumbData.photo;
                    newUser.thumb = photoAndThumbData.thumb;
                } else {
                    newUser.photo = null;
                    newUser.thumb = null;
                }
            }

            delete newUser.photoData;
            delete newUser.isDeleted;
            const updateObj: Person = newUser;
            updateObj.updated = dateTime();
            if (newUser.pageName) {
                const checkResult = await getAccountRepository().checkUniquePageName(newUser.pageName);
                if (!checkResult.unique) throw Error("Page with this name already exist");
                updateObj.pageName = checkResult.pageName;
            }
            if (newUser.email) {
                updateObj.active = false;
                updateObj.tokenAttempts = null;
                const mailVerifyToken = UserController.createVerifyToken(
                    newUser.email,
                    "email"
                );
                updateObj.token = mailVerifyToken;
                const link = `http://${req.get("host")}/user/mverify?token=${mailVerifyToken}`;
                mailer({
                    to: newUser.email,
                    subject: emailTexts('confirmEmail', updateObj.language, 'subject'),
                    html: emailTexts('confirmEmail', updateObj.language, 'html', link),
                    // subject: "Mail verification!",
                    // html: `Hello,<br> You updated your email address,<br> Please Click on the link to verify your email.<br><a href="${link}">${link}</a>`,
                });
            }

            await personRepo.update(user.person, updateObj);

            UserController.getUser(req, res);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static createVerifyToken = (payload: AccountToken|string|number, type?: string) => {
        let resultPayloadBody: Token = {
            type: type,
            expires: Date.now() + UserController.getTokenLifetime(type),
        };

        if (typeof payload !== "object") {
            resultPayloadBody = {
                ...resultPayloadBody,
                identifier: payload,
            };
        } else {
            resultPayloadBody = {
                ...resultPayloadBody,
                ...payload
            }
        }

        return jwt.sign(
            JSON.stringify(resultPayloadBody),
            Keys.privateKey(),
            {
                algorithm: "RS256",
            }
        );
    };

    static getTokenLifetime(type?: string): number {
        if (type === "email") {
            return App.MAIL_EXPIRATION_MS;
        }
        if (type === "password") {
            return App.PASS_EXPIRATION_MS;
        }
        if (type === "refresh") {
            return App.REFRESH_EXPIRATION_MS;
        }
        return App.JWT_EXPIRATION_MS;
    }

    static resetPassword = async (req, res) => {
        try {
            const passtoken = req.body.token;
            const user = await getRepository(Person).findOne({ where: { passtoken } });
            const decodedToken: any = jwt.decode(passtoken);

            if (!user)
                throw Error("User not found");
            if (!decodedToken)
                throw Error("Invalid token");
            if (!decodedToken.expires || decodedToken.expires < Date.now())
                throw Error("Sorry, token expired");
            if (!req.body.password)
                throw Error("New password is empty");

            const newPasswordHash = await argon2.hash(req.body.password);
            await getRepository(Person).update(
                user.id, {
                    password: newPasswordHash,
                    passtoken: null,
                    updated: dateTime()
                }
            );
            res.status(200).send({ status: "success", type: "forgot" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static changePassword = async (req, res) => {
        try {
            const {
                user,
                body: {
                    oldPassword,
                    password,
                }
            } = req;

            const personRepo = getRepository(Person);
            const person = await personRepo.createQueryBuilder('person')
                .where('person.id = :id', { id: user.person })
                .addSelect('person.password')
                .getOne();

            const passwordsMatch = await argon2.verify(person.password, oldPassword);

            if (!passwordsMatch)
                throw Error("Old password not correct");
            if (!password)
                throw Error("New password is empty");


            const newPasswordHash = await argon2.hash(password);

            await personRepo.update(
                { id: person.id },
                { password: newPasswordHash, updated: dateTime() }
            );
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static forgotPasswordWithoutAuth = async (req, res) => {
        try {
            const email = req.body.email;
            const user = await Helpers.getUserByEmail(email);
            if (user) {
                UserController.sendPassVerifyMail(user, res);
            } else {
                res.status(400).send({ error: "User not found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    public static forgotPassword = async (req, res) => {
        try {
            const user = await Helpers.getUserById(req.user.person);
            if (user) {
                UserController.sendPassVerifyMail(user, res);
            } else {
                res.status(400).send({ error: "User not found" });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    public static findUser = async (req, res) => {
        try {
            const {
                query: { search, page = 1, limit = 20, except },
                user
            } = req;
            const searchResult = await getAccountRepository().searchAccount(search, user.id, { page, limit, except });
            res.status(200).send(searchResult);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    };

    static sendPassVerifyMail = async (user, res) => {
        try {
            const token = UserController.createVerifyToken(user.id, "password");
            const link = `${App.FRONTEND_HOST}/changepass?token=${token}`;
            await mailer({
                to: user.email,
                subject: emailTexts('resetPass', user.language, 'subject'),
                html: emailTexts('resetPass', user.language, 'html', link),
                // subject: "Password change!",
                // html: `Hello,<br> Please Click on the link to change your password.<br><a href="${link}">${link}</a>`,
            });
            await getRepository(Person).update(user.id, { passtoken: token });
            res.status(200).send({ status: "success" });
        } catch (error) {
            res.status(400).send({ message: "E-mail message not sent", error });
        }
    };

    static passwordVerification = async (req, res) => {
        const data: any = jwt.decode(req.query.token);
        if (data.expires) {
            if (data.expires >= Date.now()) {
                if (data.identifier) {
                    res.redirect(App.FRONTEND_HOST + "/homepage");
                }
            } else {
                res.set("Content-Type", "text/html");
                res.send(new Buffer("<h2>Sorry, token expired</h2>"));
            }
        }
    };

    static deactivatePerson = async (req, res) => {
        const {
            user,
            body: {
                password
            }
        } = req;
        try {
            const personRepo = getRepository(Person);

            const person = await getRepository(Person)
                .createQueryBuilder('person')
                .addSelect("person.password")
                .where("person.id = :id", { id: user.person })
                .getOne();

            const passwordsMatch = await argon2.verify(person.password, password);

            if (!passwordsMatch) {
                return res.status(400).send({ error: 'Wrong password' });
            }

            const allUserAccounts = await getAccountRepository().getAccountsOfThePerson(user.person);
            const businessAccounts = allUserAccounts.filter(acc => acc.business !== null);

            await Promise.all(
                businessAccounts.map(async (acc) => {
                    // This method is checking that you're not the single admin in the assigned businesses
                    await BusinessController.performUpdatingBusinessAcc(acc.id, acc.business.id)
                })
            )
            // await getAccountRepository().update({ id }, { deleted: dateTime() });

            const deactivatedTime = dateTime()

            await personRepo.update({id: user.person}, {deactivated: deactivatedTime, reactivationtoken: uniqid()})
            CompletelyRemoveUser.setJobForPerson(user.person, deactivatedTime)
            res.status(200).send({ status: "success" });
        } catch (error) {
            console.log(' deactivatePerson error', error)
            res.status(400).send({ error: error.message });
        }
    };

    static reactivatePerson = async (req, res) => {
        try {
            const token = req.params['token']
            if (!token) throw new Error('reactivation token not found')
            const personRepo = getRepository(Person);
            const foundPerson = await personRepo.findOne({reactivationtoken: token})
            console.log('foundPerson', foundPerson)
            if (!foundPerson) throw new Error('reactivation token not found')
            const updated = await personRepo.update({reactivationtoken: token}, {deactivated: null, reactivationtoken: null})

            CompletelyRemoveUser.cancelJobByPersonId(foundPerson.id)
            console.log('updated', updated)
            res.status(200).send({ status: "success" });

        } catch (error) {
            res.status(400).send({message: error.message})
        }
    }

    static getTimeLine = async (req, res) => {
        try {
            const {
                query: {
                    page,
                    limit,
                    userId
                }
            } = req;

            const id = req.user.baseBusinessAccount || req.user.id

            let query = await Helpers.generateTimelineQuery(userId || id, id, { page, limit, isPrivateAcc: false }); // public acc
            if(userId && +userId !== +id) {
                const targetAccount = await getAccountRepository().getFullAccountInfo({ key: 'id', val: userId })
                const target = targetAccount;
                const account = id;
                const alreadyFollowed = await getFollowingRepository().isAccountFollowsTarget(account, target.id);
                if (!((target.business && target.business.id) || (target.person && target.person.privacy === "public")|| !!alreadyFollowed)) {
                    query = await Helpers.generateTimelineQuery(userId || id, id, { page, limit, isPrivateAcc: true }); // private acc
                }
            }

            const data = await getConnection().query(query);
            res.status(200).send(data);
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getUserByUsername = async (req: Request, res: Response) => {
        try {
            const {
                params: { username }
            } = req;

            const searchResult = await getAccountRepository().getAccountByUsername(username, req.user.id);
            if (searchResult) {
                res.status(200).send(searchResult);
            } else {
                res.status(404).send({ error: 'User not found' });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static getUserById = async (req: Request, res: Response) => {
        try {
            const {
                params: { id }
            } = req;

            const responseUser = await getAccountRepository().getFullAccountInfo({ key: 'id', val: id });

            if (responseUser) {
                res.status(200).send(responseUser);
            } else {
                res.status(404).send({ error: 'User not found' });
            }
        } catch (error) {
            res.status(400).send({ error: error.message });
        }
    }

    static validateKycDocument = async (req, res) => {
        try {
            const {
                user,
                body: {
                    file,
                },
            } = req;

            const {
                data: {
                    Id: KYCDocumentId,
                    Status,
                }
            } = await mangoPay.createKycDocument(user.mangoUserId, 'IDENTITY_PROOF');

            if (Status !== 'CREATED') {
                throw new Error('Documents have to be in "CREATED" Status');
            }

            await mangoPay.createKycPage(user.mangoUserId, KYCDocumentId, file);

            const { data } = await mangoPay.submitKycDocument(user.mangoUserId, KYCDocumentId, 'VALIDATION_ASKED');

            if (!user.business) {
                await getRepository(Person).update(user.person, {
                    kycDocumentId: data.Id,
                    verificationStatus: data.Status,
                });
            } else {
                await getRepository(Business).update(user.business, {
                    kycDocumentId: data.Id,
                    verificationStatus: data.Status,
                });
            }

            res.status(200).send(data);
        } catch (error) {
            console.log('ERROR: ', error.response.data);
            res.status(400).send({ error: error.message });
        }
    };
}
