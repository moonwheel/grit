import { Request, Response } from "express";
import getBookingRepository from "../../entities/repositories/booking-repository";
import getOrderRepository from "../../entities/repositories/order-repository";
import { getRepository } from "typeorm";
import { Person } from "../../entities/person";
import { publish } from "../../utils/sse";

export class HookController {
    static hookMangopayValidation = async (req, res) => {

        const {
            query: {
                RessourceId,
                EventType,
            },
        } = req;

        const personRepository = getRepository(Person);

        const foundUser = await personRepository.find({ kycDocumentId: RessourceId });

        if (!foundUser) {
            throw new Error('RessourseId not found');
        }

        await personRepository.update({
            kycDocumentId: RessourceId,
        }, {
            verificationStatus: EventType,
        });

        res.status(200).send({ message: "success" });
    };
}
