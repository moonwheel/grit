const express = require("express");
const router = express.Router();

import { HookController } from "./controller";

router.get("/mangopay/validation", HookController.hookMangopayValidation);

export = router;
