require("dotenv").config();

const ELASTIC_INDEX_PREFIX = process.env.ENVIRONMENT ? `${process.env.ENVIRONMENT}_` : ``;

export class App {
    static FRONTEND_HOST: string = process.env.FRONTEND_HOST || "https://grit-testing.com";

    //--- Server settings
    static PORT: number = Number(process.env.PORT) || 4000;
    static FILE_SIZE_LIMIT = Number(process.env.FILE_SIZE_LIMIT) || 300;
    static MSG_FILE_SIZE_LIMIT = Number(process.env.MSG_FILE_SIZE_LIMIT) || 100;

    //--- Messaging settings
    static MSG_ATTACHMENT_EXPIRY_DAYS = 10;

    //---User settings
    static HASH_COST = 10;
    static MAX_FOLLOW_COUNT = 1000;
    static MAX_BUSINESS_ACCOUNTS_PER_USER = 5;

    static COMPLETELY_REMOVE_USER_DELAY_MS = 1000 * 60 * 60 * 24 * 30 // 30 days
    // static COMPLETELY_REMOVE_USER_DELAY_MS = 1000 * 60 * 10 // 10 min

    //---Token settings
    static JWT_EXPIRATION_MS: number = Number(process.env.JWT_EXPIRATION_MS) || 1200000; // login expiration time
    static JWT_PRIVATE_KEY: string = process.env.JWT_PRIVATE_KEY ? process.env.JWT_PRIVATE_KEY.replace(/\\n/g, '\n') : null; // login expiration time
    static JWT_PUBLIC_KEY: string = process.env.JWT_PUBLIC_KEY ? process.env.JWT_PUBLIC_KEY.replace(/\\n/g, '\n') : null; // login expiration time


    static REFRESH_EXPIRATION_MS: number = Number(process.env.REFRESH_EXPIRATION_MS) || 600000000; // refresh token expiration time
    static PASS_EXPIRATION_MS: number = Number(process.env.PASS_EXPIRATION_MS) || 600000000; // change password expiration time
    static MAIL_EXPIRATION_MS: number = Number(process.env.MAIL_EXPIRATION_MS) || 600000000; // mail confirm expiration time
    static MALL_ATTEMPTS_TIME_LIMIT_MS: number = Number(process.env.MALL_ATTEMPTS_TIME_LIMIT_MS) || 86400000; // time limit for new mail resend

    //---Mail settings
    static MAILER_FROM = process.env.MAILER_FROM || "";
    static MAILJET_APIKEY_PUBLIC = process.env.MAILER_APIKEY_PUBLIC || "";
    static MAILJET_APIKEY_PRIVATE = process.env.MAILER_APIKEY_PRIVATE || "";

    //---Upload settings
    static UPLOAD_FOLDER: string = process.env.UPLOAD_FOLDER || "./upload";
    static VIDEO_FORMATS: Map<string, string> = new Map(
        [
            ["quicktime", ".mov"],
            ["avi", ".avi"],
            ["mp4", ".mp4"],
            ["webm", ".webm"],
        ]
    );
    static TEMP_FOLDER_PREFIX: string = 'temp/';
    //---Database settings
    static DB_HOST = process.env.DB_HOST || "localhost";
    static DB_PORT = process.env.DB_PORT || 26257;
    static DB_USER = process.env.DB_USER || "grit";
    static DB_PASSWORD = process.env.DB_PASSWORD || "grit";
    static DB_DATABASE = process.env.DB_DATABASE || "grit";
    static DB_SSL: boolean = process.env.DB_SSL === "TRUE";
    static DB_LOGGING: boolean = process.env.DB_LOGGING === "TRUE";

    static MINIO_HOST: string = process.env.MINIO_HOST || "localhost";
    static MINIO_PUBLIC_URL: string = process.env.MINIO_PUBLIC_URL || null;
    static MINIO_PORT: number = +process.env.MINIO_PORT || 9000;
    static MINIO_SSL: boolean = process.env.MINIO_SSL === "true";
    static MINIO_ACCESS_KEY: string = process.env.MINIO_ACCESS_KEY || "testtest";
    static MINIO_SECRET_KEY: string = process.env.MINIO_SECRET_KEY || "asdasdasdasd:";
    static MINIO_BUCKETS: string[] = ['photo', 'video']

    static ELASTIC_HOST: string = process.env.ELASTIC_HOST || 'http://elasticsearch-master';
    static ELASTIC_PORT: number = +process.env.ELASTIC_PORT || 9200;
    static ELASTIC_INDEX: string = process.env.ELASTIC_INDEX || `${ELASTIC_INDEX_PREFIX}grit`;
    static ELASTIC_HASHTAGS_SUGGEST: string = `${ELASTIC_INDEX_PREFIX}hashtags_suggest`;
    static ELASTIC_TITLE_SUGGEST: string = `${ELASTIC_INDEX_PREFIX}title_suggest`;
    static ELASTIC_LOCATION_SUGGEST: string = `${ELASTIC_INDEX_PREFIX}location_suggest`;
    static ELASTIC_BUSINESS_SUGGEST: string = `${ELASTIC_INDEX_PREFIX}business_suggest`;

    static TOMTOM_GEOCODER_APIKEY: string = process.env.TOMTOM_GEOCODER_APIKEY;

    static SHAKA_SERVER_HOST: string = process.env.SHAKA_SERVER_HOST || "shaka";
    static SHAKA_SERVER_PORT: number = +process.env.SHAKA_SERVER_PORT || 5000;
    static MANGO_CLIENT_ID: string = process.env.MANGO_CLIENT_ID || "thomaslohmann"
    static MANGO_CLIENT_API_KEY: string = process.env.MANGO_CLIENT_API_KEY || "v7iYx8RCVjxT0XwUr0Zwvw5gsnVgXXdaoAMeeS0reGtHdJ2fY2"
    static MANGO_BASE_URL: string = process.env.MANGO_BASE_URL || "https://api.mangopay.com"
    static MANGO_API_VERSION: string = process.env.MANGO_API_VERSION || "v2.01"


    static APM_SERVICE_NAME: string = process.env.APM_SERVICE_NAME || "node"
    static APM_SERVER_URL: string = process.env.APM_SERVER_URL || "http://tracing-apm-server.kube-system:8200"


    static NATS_TOKEN: string = process.env.NATS_TOKEN || "some token"
    static NATS_URL: string = process.env.NATS_URL || "nats"

}
