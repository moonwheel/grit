export const emailTexts = (type: string, lang: string, field: string, link?: string) => {
  const allTexts = {
    confirmEmail: {
      english: {
        subject: 'Confirm email',
        html: `Click on the following link to confirm your email:<br><a href="${link}">${link}</a>`
      },
      deutsch: {
        subject: 'E-Mail bestätigen',
        html: `Klicke auf den folgenden Link, um deine E-Mail zu bestätigen:<br><a href="${link}">${link}</a>`
      }
    },
    resetPass: {
      english: {
        subject: 'Reset password',
        html: `Click on the following link to reset your password:<br><a href="${link}">${link}</a>`
      },
      deutsch: {
        subject: 'Passwort zurücksetzen',
        html: `Klicke auf den folgenden Link, um dein Passwort zu erneuern:<br><a href="${link}">${link}</a>`
      }
    }
  }


  return allTexts[type][lang][field]
}