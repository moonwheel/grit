// import { minioClient } from './minio';
import * as Minio from 'minio';
import { App } from "../config/app.config";
import { downloadPolicy } from './minio-bucket-policy';

export default class MinioInstance {
  static client: any = new Minio.Client({
    endPoint: App.MINIO_HOST,
    port: App.MINIO_PORT,
    useSSL: App.MINIO_SSL,
    accessKey: App.MINIO_ACCESS_KEY,
    secretKey: App.MINIO_SECRET_KEY
  });

  // static createBucket = (): void => {
  //   const minioClient = MinioInstance.client;
  //   App.MINIO_BUCKETS.forEach(bucketName => {
  //     minioClient.bucketExists(bucketName, (err, exists) => {
  //       if (err) {
  //         return console.log(err)
  //       } else if (!exists) {
  //         console.log(`Bucket ${bucketName} does not exist`)
  //         minioClient.makeBucket(bucketName, '', (err) => {
  //           if (err) return console.log('Error creating bucket.', err)
  //           const policy = downloadPolicy(bucketName);
  //           minioClient.setBucketPolicy(bucketName, policy, (err) => {
  //             if (err) throw err
  //             console.log(`Bucket ${bucketName} created successfully with policy download.`)
  //           })
  //         })
  //       } else {
  //         console.log(`Bucket ${bucketName} already exists`)
  //       }
  //     })

  //   });
  // }

  static uploadFile = (type, name, url): Promise<string> => {
    return new Promise((resolve, reject) => {
      console.log('uploading object', type, name, url);
      MinioInstance.client.fPutObject(type, name, url, {}, async function(err, etag) {
        if(err) return reject(err)
        resolve(etag)
      })
    });
  }
  static deleteFile = (bucket, name): Promise<string> => {
    return new Promise((resolve, reject) => {
      MinioInstance.client.removeObject(bucket, name, function(err) {
        if(err) return reject(err)
        resolve()
      })
    });
  }
  static uploadStream = (type, name, stream, size, meta): Promise<string> => {
    return new Promise((resolve, reject) => {
      MinioInstance.client.putObject(type, name, stream, size, meta, function(err, etag) {
        if(err) return reject(err)
        resolve(etag)
      })
    });
  }

}