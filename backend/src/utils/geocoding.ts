import Axios from 'axios';
import { App } from './../config/app.config';

export class Geocoding {
  static tomTomGeocode = async (text) => {
    try {
      const response = await Axios({
        method: 'get',
        url: `https://api.tomtom.com/search/2/geocode/${text}.JSON`,
        params: {
          key: App.TOMTOM_GEOCODER_APIKEY
        },
      })
      return response.data;

    } catch (error) {
      console.log('error', error);
      return error
    }
  }

  static tomTomAutocomplete = async (text, limit) => {
    try {
      const response = await Axios({
        method: 'get',
        url: `https://api.tomtom.com/search/2/search/${text}.json`,
        params: {
          key: App.TOMTOM_GEOCODER_APIKEY,
          language: 'en-US',
          limit: limit,
          typeahead: true
        },
      })
      return response.data;

    } catch (error) {
      console.log('error', error);
      return error
    }
  }
  // static locationIq = async (text) => {
  //   try {
  //     const response = await Axios({
  //       method: 'get',
  //       url: `shttps://eu1.locationiq.com/v1/search.php`,
  //       params: {
  //         key: 'SOMEAPIKEY',
  //         q: text,
  //         format: 'json'
  //       },
  //     })
  //     return response;
  //   } catch (error) {
  //     console.log('error', error);
  //     return error
  //   }
  // }
  // static mapBox = async (text) => {
  //   try {
  //     const response = await Axios({
  //       method: 'get',
  //       url: `https://api.mapbox.com/geocoding/v5/mapbox.places/${text}.json`,
  //       params: {
  //         access_token: 'SOMEAPIKEY',
  //       },
  //     })
  //     return response;
  //   } catch (error) {
  //     console.log('error', error);
  //     return error
  //   }
  // }
}