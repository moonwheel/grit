import { App } from './../config/app.config';
import http = require('http');
import socketIo = require('socket.io');
import socketIoJwt = require('socketio-jwt');
import { Keys } from './keys';
var nats = require('nats');
var natsAdapter = require('socket.io-2-nats');

const nc = nats.connect({
  json: true,
  url: `nats://${App.NATS_URL}:4222`,
  token: App.NATS_TOKEN
})

export let socketIoServer: socketIo.Server;

export const initWebsocketServer = (server: http.Server) => {
    socketIoServer = socketIo(server, {
      transports: ['websocket',  'polling']
    });
    socketIoServer.adapter(natsAdapter({ nc: nc }));

  socketIoServer.use(socketIoJwt.authorize({
    secret: Keys.publicKey(),
    handshake: true
  }));

  socketIoServer.on('connection', function (socket) {
    const user = socket['decoded_token'];
    console.log('Socket connected user.id', user.id)

    socket.join(`user_${user.id}`);
    socket.emit('joined', {room: `user_${user.id}`})
  })

  return socketIoServer;
}

// export socketIoServer;