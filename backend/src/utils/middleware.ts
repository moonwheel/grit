import { Account } from "../entities/account";
import { Article } from "../entities/article";
import { Photo } from "../entities/photo";
import { Product } from "../entities/product";
import { Service } from "../entities/service";
import { Text } from "../entities/text";
import { Video } from "../entities/video";
import { getRepository } from "typeorm";
import getBlockRepository from "../entities/repositories/block-repository";
import { Request, Response, NextFunction } from "express";

export class Middleware {
    static checkInBlaklist(
        entity: any,
        paramField: string,
    ) {
        return async (req, res, next) => {
            let pageName = null;
            try {
                const Entity = Middleware.getEntity(entity);
                if (req.params && req.params[paramField]) {
                    const entityID = req.params[paramField];
                    const result: any = await getRepository(Entity).findOne(entityID,{ relations: ["user"] });
                    const ownerId = (entity === "account")
                        ? result.id
                        : result.user
                            ? result.user.id
                            : null;
                    const blockedBy = await getBlockRepository().blockedBy(req.user.id);
                    pageName = result.user.person ? result.user.person.pageName : result.user.business.pageName;
                    if (blockedBy.some(id => id == ownerId) || !ownerId) throw Error("Page is blocked");
                }
                next();
            } catch (error) {
                res.status(400).send({error: error.message, isBlocked: true, pageName: pageName});
            }
        };
    }

    static getEntity(
        entity: string
    ) {
        switch (entity) {
            case "account": return Account;
            case "article": return Article;
            case "photo": return Photo;
            case "product": return Product;
            case "service": return Service;
            case "text": return Text;
            case "video": return Video;
            default: throw Error("Invalid entity name");
        }
    }

    static checkDraftAccess(req: Request, res: Response, next: NextFunction) {
        const {
            user,
            query: {
                draft = 'false',
                userId = null,
            }
        } = req;

        const isDraft = draft === 'true';
        const hasUserIdInQuery = userId !== null;
        const isUserIdDifferent = Number(userId) !== Number(user.id);

        if (isDraft && hasUserIdInQuery && isUserIdDifferent) {
            return res.status(403).send({error: `You can't access to other user drafts`});
        }

        next();
    }
}
