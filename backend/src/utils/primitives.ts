export const isFloat = (value: any) => {
    if(parseFloat(value as any) % 1 !== 0) return true;

    return false;
}