import * as tus from 'tus-node-server';
import * as fs from 'fs';
import uniqid = require("uniqid");
import { Request } from 'express';
import { App } from '../config/app.config';
import atob = require("atob");

export const tusServer = new tus.Server();
export const TUS_EVENTS = tus.EVENTS;

tusServer.datastore = new tus.FileStore({
  path: '/upload',
});

export interface UploadedFileMetadata {
  id: number;
  userId: number;
  type: string;
  entityType: 'photo' |
    'video' |
    'article' |
    'product' |
    'service' |
    'photo-attachment' |
    'video-attachment' |
    'document-attachment' |
    'seller_review' |
    'product_review' |
    'service_review';
}


export const decodeMetadata = (meta): UploadedFileMetadata => {
  const result: any = {};

  if (!meta) {
    return result;
  }

  meta.split(',').forEach(part => {
    const [key, value] = part.split(' ');
    result[key] = atob(value);
  });

  if ('id' in result) {
    result.id = Number(result.id);
  }

  if ('userId' in result) {
    result.userId = Number(result.userId);
  }

  return result;
}


// export const isUploadingFinished = (req: Request): boolean => {
//   const fileSize = +req.headers['grit-file-size'];
//   const chunkSize = +req.headers['content-length'];
//   const offset = +req.headers['upload-offset'];

//   return chunkSize + offset === fileSize;
// }

export const generatePaths = async (
  req: Request,
  featureType: 'photo' | 'video' | 'cover',
  bucketName: string,
) => {
  // calculate oldFilePath because the file is uploaded, but it has encoded filename and no file type
  const splitUrl = req.url.split('/');
  const key = splitUrl[splitUrl.length - 1];
  const oldFilePath = `${App.UPLOAD_FOLDER}/${key}`;

  const uniqueFileName = uniqid(`${featureType}-`);
  const fileType = req.headers['grit-file-type'];
  const spliType = (fileType as string).split('/');
  const fileFormat = spliType[1];
  const fullFileName = `${uniqueFileName}.${fileFormat}`;

  const newFilePath = `${App.UPLOAD_FOLDER}/${fullFileName}`;
  const minioPath = `${App.MINIO_PUBLIC_URL}/${bucketName}/${fullFileName}`;

  fs.renameSync(oldFilePath, newFilePath);

  return { newFilePath, minioPath };
}
