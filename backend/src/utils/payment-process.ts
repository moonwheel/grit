import { getRepository } from "typeorm";
import * as cron from "node-cron";

import { Account } from "../entities/account";
import { PaymentType } from "../entities/order";

import { IMangoUserData } from "../interfaces/payment/payment.interface";

import getBookingRepository from "../entities/repositories/booking-repository";

import mangoPayment from '../utils/mango-payment';

type typeAccount = "business" | "person";
type typePayIn = "booking" | "order";

class PaymentProcess {

    public async createPayInToWallet(
        userId: number,
        cardId: number,
        price: number,
        gritWallet: boolean,
        type: typePayIn
    ) {
        const { walletId, mangoUserId } = await this.getMangoCreatentialsByUserId(userId);

        let amount = 0;
        let amountWallet = 0;
        let totalAmount = price * 100;

        if (gritWallet) {
            const { amount: walletBalance } = await mangoPayment.getWalletBalance(walletId);
            if (totalAmount > walletBalance) {
                amount = totalAmount - walletBalance;
                amountWallet = walletBalance;
            } else {
                amount = 0;
                amountWallet = totalAmount;
            }
        } else {
            amount = totalAmount;
            amountWallet = 0;
        }

        const {
            data: {
                Id: transactionId,
                Status: errorStatus,
                ResultCode: errorCode,
                ResultMessage: errorMessage
            }
        } = await mangoPayment.createPayIn({
            mangoUserId,
            walletId,
            cardId,
            amount,
        });

        if (errorStatus === "FAILED" && errorCode === "001001") {
            throw new Error("Not enough funds on your balance.");
        } else if (errorStatus === "FAILED") {
            throw new Error(errorMessage);
        }

        return {
            walletId,
            mangoUserId,
            transactionId,
            amount,
            amountWallet,
        };
    }

    public async managePayments(
        buyer: Account,
        seller: Account,
        amount: number,
        paymentType: PaymentType = 'card',
    ): Promise<string> {
        // await this.kycValidation(sellerWalletId);

        const { feesAmount, debitedAmount } = this.calculateFeesAndDebitAmount(amount);

        const buyerMangopayId = buyer.business ? buyer.business.mangoUserId : buyer.person.mangoUserId;
        const buyerWalletId = buyer.business ? buyer.business.wallet : buyer.person.wallet;
        const sellerWalletId = seller.business ? seller.business.wallet : seller.person.wallet;

        const { Id: transferId } = await mangoPayment.createTransfer(
            buyerMangopayId,
            buyerWalletId,
            sellerWalletId,
            debitedAmount,
            feesAmount
        );

        return transferId;
    }

    /**
     * KYC Validation
     * @param {Number} mangoUserId
     */
    private async kycValidation(mangoUserId: number | string, file: string) {
        const { data: { Id: kycDocumentId } } = await mangoPayment.createKycDocument(Number(mangoUserId));
        await mangoPayment.createKycPage(Number(mangoUserId), kycDocumentId, file);
        const { data } = await mangoPayment.submitKycDocument(Number(mangoUserId), kycDocumentId);

        return data;
    }

    /**
     * Get Mango creadentials
     * @param {Number|String} userId
     */
    public async getMangoCreatentialsByUserId(userId: number | string): Promise<IMangoUserData> {

        const account = await getRepository(Account).findOne(Number(userId));

        const walletId = account.business ? account.business.wallet : account.person.wallet;
        const mangoUserId = account.business ? account.business.mangoUserId : account.person.mangoUserId;
        const name = account.business ? account.business.name : account.person.pageName;
        const bankAliasId = account.business ? account.business.bankAliasId : account.person.bankAliasId;
        const accountType: typeAccount = account.business ? 'business' : 'person';

        return {
            walletId,
            mangoUserId,
            name,
            bankAliasId,
            accountType,
        };
    }

    /**
     * Calculate fees and debit amount
     * @param {Number} sum
     */
    private calculateFeesAndDebitAmount(sum: number) {
        const debitedAmount = Number(sum) * 100;
        const minimumFeesAmount = 1667;

        let feesAmount = 0;

        if (debitedAmount > minimumFeesAmount) {
            feesAmount = debitedAmount * 0.03;
        } else if (debitedAmount < minimumFeesAmount) {
            feesAmount = 50;
        }

        return { feesAmount, debitedAmount };
    }

    /**
     * Pay money for booking and change status
     */
    public async payMoneyAndChangeStatus() {
        cron.schedule('* * * * *', async () => {
            const appointments = await getBookingRepository().getNotPaidBookings();

            for (let appointment of appointments) {
                const totalPrice = appointment.booking['total_price'];
                const bookingOwner = appointment.booking['user'].id;
                const serviceOwner = appointment.booking['service_owner'].id;
                const bookingId = appointment.booking['id'];

                await this.payMoneyForBooking(bookingId, serviceOwner, bookingOwner, Number(totalPrice));

                // TODO Check Hook for
                await getBookingRepository().changeBookingStatus();
            }

            console.log('change status every minute.');
        });
    }

    /**
     * Pay money for booking
     * @param {Number} bookingId
     * @param {Number} sellerId
     * @param {Number} buyerId
     * @param {Number} totalPrice
     */
    private async payMoneyForBooking(
        bookingId: number,
        sellerId: number,
        buyerId: number,
        totalPrice: number
    ): Promise<void> {
        const {
            mangoUserId: sellerMangoUserId
        } = await this.getMangoCreatentialsByUserId(sellerId);

        const {
            walletId: buyerWalletId,
            mangoUserId: buyerMangoUserId,
        } = await this.getMangoCreatentialsByUserId(buyerId);

        const { feesAmount, debitedAmount } = this.calculateFeesAndDebitAmount(totalPrice);

        const { Id: transactionId } = await mangoPayment.createTransfer(
            buyerMangoUserId,
            buyerWalletId,
            sellerMangoUserId,
            debitedAmount,
            feesAmount
        );

        await getBookingRepository().saveTransactionData(
            bookingId,
            Number(transactionId),
            debitedAmount,
            feesAmount
        )
    }
}

export default new PaymentProcess();
