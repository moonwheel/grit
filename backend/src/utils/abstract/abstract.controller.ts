import { Photo } from '../../entities/photo';
import { Video } from '../../entities/video';
import { Article } from '../../entities/article';
import { EntitySchema, ObjectLiteral, getRepository, Repository } from "typeorm";
import { Request, Response, response } from "express";

import getMentionRepository, { MentionRepositoryEntityKey } from "../../entities/repositories/mention-repository";
import { Account } from "../../entities/account";
import { Filter } from "../../interfaces/global/filter.interface";
import { Video_Comment } from '../../entities/video_comment';
import { Article_Comment } from '../../entities/article_comment';
import { Photo_Comment } from '../../entities/photo_comment';
import { Text } from '../../entities/text';
import { Text_Comment } from '../../entities/text_comment';
import { Photo_Comment_Replies } from '../../entities/photo_comment_reply';
import { Video_Comment_Replies } from '../../entities/video_comment_reply';
import { Article_Comment_Replies } from '../../entities/article_comment_reply';
import { Text_Comment_Replies } from '../../entities/text_comment_reply';
import { getCommentsRawQuery, getRepliesRawQuery } from './raw_queries';

type AbstractComment = ObjectLiteral & {
    user: Account;
    text: string;
}

type AbstractReply = AbstractComment & {
    comment_: AbstractComment;
}

type Resource = 'photo' | 'video' | 'article' | 'plain_text' | 'photo_comment' | 'video_comment' | 'article_comment' | 'text_comment';


/**
 * @author ws.kadirmaev
 * Created for non-repeating of simmilar code in controllers.
 * Maybe not the best solution.
 * TODO: develop better solution to keep simmilar code in one place.
 *
 * NOTE: started from 'edit comment' functionality, because other methods are already exists
 * in other controllers and moving them here can cause issues.
 * Later all simmilar CRUD (and CRUD-like) methods can be moved in this abstract controller.
 */
export class AbstractController {
    static editComment = async <C extends AbstractComment, R extends AbstractReply>(
        req: Request,
        res: Response,

        CommentEntity: new () => C,
        ReplyEntity: new () => R,

        mentionCommentsKey: MentionRepositoryEntityKey,
        mentionRepliesKey: MentionRepositoryEntityKey,
    ) => {
        try {
            const {
                params: { id },
                body: {
                    text,
                    replyTo,
                    mentions = [],
                },
            } = req;

            if (!text) throw Error("Empty text field");

            const response = { comment: null };
            const commentRepo = getRepository(CommentEntity);
            const repliesRepo = getRepository(ReplyEntity);

            if (replyTo) {
                const comment = await commentRepo.findOne(replyTo);
                const reply = await repliesRepo.findOne(id);

                if (!comment) throw Error("Comment not exist");
                if (!reply) throw Error("Reply not exist");

                reply.text = text;

                response.comment = await repliesRepo.save(reply);
                response.comment.mentions = await getMentionRepository().addMention(mentionRepliesKey, response.comment, mentions, mentionRepliesKey, req.user);
            } else {
                const comment = await commentRepo.findOne(id);

                if (!comment) throw Error("Comment not exist");

                comment.text = text;
                response.comment = await commentRepo.save(comment);
                response.comment.mentions = await getMentionRepository().addMention(mentionCommentsKey, response.comment, mentions,mentionRepliesKey, req.user);
            }

            res.status(200).send(response);
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };

    static getComments = async <C, K extends keyof C>(
        req: Request,
        res: Response,
        CommentEntity: new () => C,
        relationColumnName: K,
        entityName: string
    ) => {
        try {
            const {
                params: { id: entityId },
                user,
                query: { page = 1, filterBy = "date", limit = 15, order = "asc" }
            } = req;

            const commentRepo = getRepository(CommentEntity);

            const filter: Filter = {
                by: filterBy.toLowerCase() === "relevance" ? "comment_relevance" : "comment_created",
                order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
            };
            const limitNumb = Number(limit);
            const skip = (Number(page) - 1) * limitNumb;
            const userId = user.baseBusinessAccount || user.id

            // const query = commentRepo
            //     .createQueryBuilder("comment")
            //     .leftJoinAndSelect("comment.user", "comment_user")
            //     .leftJoin("comment_user.business", "comment_user_business")
            //     .addSelect("comment_user_business.id")
            //     .addSelect("comment_user_business.photo")
            //     .addSelect("comment_user_business.name")
            //     .addSelect("comment_user_business.pageName")
            //     .leftJoin("comment_user.person", "comment_user_person")
            //     .addSelect("comment_user_person.id")
            //     .addSelect("comment_user_person.photo")
            //     .addSelect("comment_user_person.fullName")
            //     .addSelect("comment_user_person.pageName")
            //     .leftJoinAndMapOne(`comment_user.following`, `comment_user.followers`, `comment_user_followers`, `comment_user_followers.account = ${userId}`)
            //     .leftJoinAndSelect("comment.mentions", "comment_mention")
            //     .leftJoinAndSelect("comment_mention.target", "comment_mention_target")
            //     .leftJoin("comment_mention_target.business", "comment_mention_target_business")
            //     .addSelect("comment_mention_target_business.id")
            //     .addSelect("comment_mention_target_business.photo")
            //     .addSelect("comment_mention_target_business.name")
            //     .addSelect("comment_mention_target_business.pageName")
            //     .leftJoin("comment_mention_target.person", "comment_mention_target_person")
            //     .addSelect("comment_mention_target_person.id")
            //     .addSelect("comment_mention_target_person.photo")
            //     .addSelect("comment_mention_target_person.fullName")
            //     .addSelect("comment_mention_target_person.pageName")
            //     .leftJoin("comment.replies", "comment_replies")
            //     .addSelect("comment.created", "comment_created")
            //     .addSelect("COUNT(DISTINCT(comment_likes.id))", "comment_likesCount")
            //     .addSelect("COUNT(DISTINCT(comment_replies.id))", "comment_repliesCount")
            //     .addSelect("COUNT(DISTINCT(comment_replies.id)) + COUNT(DISTINCT(comment_likes.id))", "comment_relevance")
            //     .leftJoinAndMapOne(
            //         `comment.liked`,
            //         `comment.likes`,
            //         `comment_likes`,
            //         `comment_likes.user_id = :userId`,
            //         { userId }
            //     )
            //     .andWhere(`comment.${relationColumnName} = :entityId`, { entityId })
            //     .groupBy(`
            //         comment.*,
            //         comment_likes.*,
            //         comment_user.*,
            //         comment_mention.*,
            //         comment_mention_target.*,
            //         comment_mention_target_person.*,
            //         comment_mention_target_business.*,
            //         comment_user_person.*,
            //         comment_user_business.*,
            //         comment_user_followers.*
            //     `)
            //     .skip(skip)
            //     .take(limitNumb)
            //     .orderBy(filter.by, filter.order)

            //   // console.log('getComments query.getSql()', query.getSql())

            //   const comments = await query.getMany();

            // res.status(200).send(comments);
          const rawQuery = getCommentsRawQuery(
            entityName,
            entityId,
            userId,
            limitNumb,
            skip,
            filter.by,
            filter.order,
          )
          const comments = await commentRepo.query(rawQuery)
          // console.log('rawQuery', rawQuery)

          // TODO: ENABLE SANITIZER WHEN THE FRONTEND MAPPING IS READY
          // res.status(200).send({comments: sanitizeRawData(comments), raw: true})
          res.status(200).send({comments, raw: true})
        } catch (error) {
            res.status(400).send({error: error.message});
        }
    };


    static getReplies = async <C>(
      req: Request,
      res: Response,
      replyEntity: new () => C,
      entityName: string
  ) => {
      try {
          const {
              user,
              params: { id },
              query: { page = 1, filterBy = "date", limit = 15, order = "asc" }
          } = req;

          const replyRepo = getRepository(replyEntity);

          const filter: Filter = {
              by: filterBy.toLowerCase() === "likes" ? "reply_total_likes" : "reply_created",
              order: order.toLowerCase() === "asc" ? "ASC" : "DESC",
          };
          const limitNumb = Number(limit);
          const skip = (Number(page) - 1) * limitNumb;

          // const query = replyRepo
          //     .createQueryBuilder("reply")
          //     .leftJoinAndSelect("reply.user", "user")
          //     .leftJoin("user.business", "user_business")
          //     .addSelect("user_business.id")
          //     .addSelect("user_business.photo")
          //     .addSelect("user_business.name")
          //     .addSelect("user_business.pageName")
          //     .leftJoin("user.person", "user_person")
          //     .addSelect("user_person.id")
          //     .addSelect("user_person.photo")
          //     .addSelect("user_person.fullName")
          //     .addSelect("user_person.pageName")
          //     .leftJoinAndSelect("reply.mentions", "reply_mention")
          //     .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
          //     .leftJoin("reply_mention_target.business", "reply_mention_target_business")
          //     .addSelect("reply_mention_target_business.id")
          //     .addSelect("reply_mention_target_business.photo")
          //     .addSelect("reply_mention_target_business.name")
          //     .addSelect("reply_mention_target_business.pageName")
          //     .leftJoin("reply_mention_target.person", "reply_mention_target_person")
          //     .addSelect("reply_mention_target_person.id")
          //     .addSelect("reply_mention_target_person.photo")
          //     .addSelect("reply_mention_target_person.fullName")
          //     .addSelect("reply_mention_target_person.pageName")
          //     .addSelect("reply.created", "reply_created")
          //     .leftJoinAndMapOne(`reply.liked`, `reply.likes`, `reply_likes`, `reply_likes.user_id = ${user.baseBusinessAccount || user.id}`)
          //     .andWhere("reply.comment_ = :id", { id })
          //     .skip(skip)
          //     .take(limitNumb)
          //     .orderBy( filter.by, filter.order)

          //     console.log('query.getSql()', query.getSql())
          //     const replies: any = await query.getMany();

          // res.status(200).send({ replies });

          const rawQuery = getRepliesRawQuery(
            entityName,
            id,
            user.baseBusinessAccount || user.id,
            limitNumb,
            skip,
            filter.by,
            filter.order,
          )
          const replies = await replyRepo.query(rawQuery)
          // console.log('rawQuery', rawQuery)

          // TODO: ENABLE SANITIZER WHEN THE FRONTEND MAPPING IS READY
          // res.status(200).send({replies: sanitizeRawData(replies), raw: true})
          res.status(200).send({replies, raw: true})
      } catch (error) {
          res.status(400).send({error: error.message});
      }
  };



    static buildPhotoVideoArticleListQuery = (
        repository: Repository<Video> | Repository<Photo> | Repository<Article>,
        resource: Resource,
        userId: number, // user - target
        currentUser: number, // user who's doing a request
        skip: number,
        limitNumb: number,
        filterBy: string,
        order: string,
        draft: boolean
    ) => {
        const filter: Filter = {
            by: `${resource}.created`,
            order: "DESC",
        };
        if(filterBy === "views") filter.by = `${resource}_views_count`;
        if(filterBy === "likes") filter.by =`${resource}_likes_count`;
        if(filterBy === "date") filter.by =`${resource}.created`;

        if(order.toLowerCase() === "asc" ) filter.order = "ASC";


        const query = repository
            .createQueryBuilder(`${resource}`)
            .leftJoin(`${resource}.likes`, `${resource}_likes`)
            .addSelect(`COUNT(DISTINCT ${resource}_likes.id)`, `${resource}_likes_count`) // this field just for filtering the list (invisible in the response)
            .leftJoin(`${resource}.views`, `${resource}_view`)
            .addSelect(`COUNT(DISTINCT ${resource}_view."user_id")`, `${resource}_views_count`)
            .leftJoin(`${resource}.user`, `${resource}_user`)
            .addSelect(`${resource}_user`, `user_id`)
                // Joining likes 2nd time for detecting if user liked this ${resource} and not affecting likes count filter
            .leftJoinAndMapOne(`${resource}.liked`, `${resource}.likes`, `likes`, `likes.user_id = :user`, {user: userId})
            .leftJoinAndMapOne(`${resource}.viewed`, `${resource}.views`, `views`, `views.user_id = :user`, {user: userId})
            .leftJoinAndMapOne(`${resource}.bookmarked`, `${resource}.bookmarks`, `bookmarks`, `bookmarks.owner_id = :owner`, {owner: currentUser})

            .andWhere(`${resource}.user_id = :user`, {user: userId})
            .andWhere(`${resource}.deleted IS NULL`)
            .andWhere(`${resource}.draft = :draft`, {draft})
            .groupBy(`
                ${resource}.*,
                likes.*,
                views.*,
                bookmarks.*,
                ${resource}_user.*
            `)
            .skip(skip)
            .take(limitNumb)
            .orderBy( filter.by, filter.order)

        // console.log('DEBUG: QUERY ', query.getSql())
        return query
    }


    static getSinglePhotoVideoArticle = (
        repository: Repository<Video> | Repository<Photo> | Repository<Article> | Repository<Text>  ,
        resource: Resource,
        entityId: number,
        userId: number
    ) => {

        const resourseType = resource === 'plain_text' ? 'text' : resource;

        const query: any = repository
            .createQueryBuilder("entity")
            .leftJoin("entity.user", "entity_user")
            .addSelect("entity_user.id")
            .addSelect("entity_user.business")
            .leftJoin("entity_user.business", "entity_user_business")
            .addSelect("entity_user_business.id")
            .addSelect("entity_user_business.photo")
            .addSelect("entity_user_business.name")
            .addSelect("entity_user_business.pageName")
            .addSelect("entity_user.person")
            .leftJoin("entity_user.person", "entity_user_person")
            .addSelect("entity_user_person.photo")
            .addSelect("entity_user_person.fullName")
            .addSelect("entity_user_person.pageName")
            .addSelect("entity_user_person.privacy")
            .leftJoinAndSelect("entity.mentions","entity_mention", `
                entity_mention.${resourseType}_comment IS NULL
                AND
                entity_mention.${resourseType}_comment_reply IS NULL
            `)
            .leftJoinAndSelect("entity_mention.target", "entity_mention_target")
            .leftJoin("entity_mention_target.business", "entity_mention_target_business")
            .addSelect("entity_mention_target_business.id")
            .addSelect("entity_mention_target_business.photo")
            .addSelect("entity_mention_target_business.name")
            .addSelect("entity_mention_target_business.pageName")
            .leftJoin("entity_mention_target.person", "entity_mention_target_person")
            .addSelect("entity_mention_target_person.id")
            .addSelect("entity_mention_target_person.photo")
            .addSelect("entity_mention_target_person.fullName")
            .addSelect("entity_mention_target_person.pageName")
            .leftJoinAndMapOne(`entity.liked`, `entity.likes`, `entity_likes`, `entity_likes.user_id = ${userId}`)
            .leftJoinAndMapOne(`entity.viewed`, `entity.views`, `entity_viewes`, `entity_viewes.user_id = ${userId}`)
            .leftJoinAndMapOne(`entity.bookmarked`, `entity.bookmarks`, `entity_bookmarks`, `entity_bookmarks.owner_id = ${userId}`)
            .leftJoinAndMapOne(`entity_user.following`, `entity_user.followers`, `entity_user_followers`, `entity_user_followers.account = ${userId}`)

        if (resource !== 'plain_text') {
            query.leftJoinAndMapOne("entity.entity_business_address","entity.business_address", "entity_business_address")
        }

        query
            .where("entity.id = :id", { id: entityId })
            .andWhere("entity.deleted IS NULL")

        // console.log('DEBUG: QUERY ', query.getSql())
        return query;
    }

    static getSinglePhotoVideoArticleForNotification = (
        repository: Repository<Video> | Repository<Photo> | Repository<Article> | Repository<Text>  ,
        resource: Resource,
        entityId: number,
        userId: number
    ) => {
        const query: any = repository
        .createQueryBuilder("entity")
        // .addSelect("entity.cover")
        // .addSelect("entity.text")
        .leftJoin("entity.user", "entity_user")
        .addSelect("entity_user.id")
        .addSelect('entity_user.followers_notification')
        .addSelect('entity_user.likes_notification')
        .addSelect('entity_user.comments_notification')
        .addSelect('entity_user.tags_notification')
        .addSelect("entity_user.business")
        // .leftJoin("entity_user.business", "entity_user_business")
        // .addSelect("entity_user_business.id")
        // .addSelect("entity_user_business.thumb")
        // .addSelect("entity_user_business.name")
        // .addSelect("entity_user_business.pageName")
        .addSelect("entity_user.person")
        .addSelect("entity_user.baseBusinessAccount")
        // .leftJoin("entity_user.person", "entity_user_person")
        // .addSelect("entity_user_person.thumb")
        // .addSelect("entity_user_person.fullName")
        // .addSelect("entity_user_person.pageName")
        // .leftJoin("entity_user.baseBusinessAccount", "baseBusinessAccount")
        // .addSelect("baseBusinessAccount.id")



        // .leftJoinAndSelect("entity.mentions", "entity_mention")
        // .leftJoinAndSelect("entity_mention.target", "entity_mention_target")
        // .leftJoin("entity_mention_target.business", "entity_mention_target_business")
        // .addSelect("entity_mention_target_business.id")
        // .addSelect("entity_mention_target_business.photo")
        // .addSelect("entity_mention_target_business.name")
        // .addSelect("entity_mention_target_business.pageName")
        // .leftJoin("entity_mention_target.person", "entity_mention_target_person")
        // .addSelect("entity_mention_target_person.id")
        // .addSelect("entity_mention_target_person.photo")
        // .addSelect("entity_mention_target_person.fullName")
        // .addSelect("entity_mention_target_person.pageName")
        // .leftJoinAndMapOne(`entity.liked`, `entity.likes`, `entity_likes`, `entity_likes.user_id = ${userId}`)
        // .leftJoinAndMapOne(`entity_user.following`, `entity_user.followers`, `entity_user_followers`, `entity_user_followers.account = ${userId}`)
        .where("entity.id = :id", { id: entityId })
        // .andWhere("entity.deleted IS NULL")

        // console.log('DEBUG: QUERY ', query.getSql())
        return query
    }



    static getSingleComment = (
        repository: Repository<Video_Comment> |
        Repository<Video_Comment_Replies> |
        Repository<Photo_Comment> |
        Repository<Photo_Comment_Replies> |
        Repository<Article_Comment> |
        Repository<Article_Comment_Replies> |
        Repository<Text_Comment> |
        Repository<Text_Comment_Replies>,
        resource: Resource,
        entityId: number,
        userId: number
    ) => {
        const query: any = repository
        .createQueryBuilder("entity")
        .leftJoin(`entity.${resource}_`, "entity_resource")
        .addSelect("entity_resource.id")
        .addSelect(`entity_resource.${ resource == "plain_text" ? "text" : "thumb"}`)
        .leftJoin("entity.user", "entity_user")
        .addSelect("entity_user.id")
        .addSelect("entity_user.business")
        .leftJoin("entity_user.business", "entity_user_business")
        .addSelect("entity_user_business.id")
        .addSelect("entity_user_business.photo")
        .addSelect("entity_user_business.name")
        .addSelect("entity_user_business.pageName")
        .addSelect("entity_user.person")
        .leftJoin("entity_user.person", "entity_user_person")
        .addSelect("entity_user_person.photo")
        .addSelect("entity_user_person.fullName")
        .addSelect("entity_user_person.pageName")
        .leftJoinAndSelect("entity.mentions", "entity_mention")
        .leftJoinAndSelect("entity_mention.target", "entity_mention_target")
        .leftJoin("entity_mention_target.business", "entity_mention_target_business")
        .addSelect("entity_mention_target_business.id")
        .addSelect("entity_mention_target_business.photo")
        .addSelect("entity_mention_target_business.name")
        .addSelect("entity_mention_target_business.pageName")
        .leftJoin("entity_mention_target.person", "entity_mention_target_person")
        .addSelect("entity_mention_target_person.id")
        .addSelect("entity_mention_target_person.photo")
        .addSelect("entity_mention_target_person.fullName")
        .addSelect("entity_mention_target_person.pageName")
        .leftJoinAndMapOne(`entity.liked`, `entity.likes`, `entity_likes`, `entity_likes.user_id = ${userId}`)
        .leftJoinAndMapOne(`entity_user.following`, `entity_user.followers`, `entity_user_followers`, `entity_user_followers.account = ${userId}`)
        .where("entity.id = :id", { id: entityId })
        // .andWhere("entity.deleted IS NULL")

        return query
    }
}










