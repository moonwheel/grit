export const getCommentsRawQuery = (
  entityName: string,
  entityId: string | number,
  userId: string | number,
  limit: number,
  offset: number,
  order: string,
  sort: string,
): string => {
  const rawQuery = `
    select
      "comment"."id" as "comment_id",
      "comment"."text" as "comment_text",
      "comment"."created" as "comment_created",
      "comment"."updated" as "comment_updated",
      "comment"."user_id" as "comment_user_id",
      "comment"."created" as "comment_created",
      "comment_user"."id" as "comment_user_id",
      "comment_user"."followers_notification" as "comment_user_followers_notification",
      "comment_user"."likes_notification" as "comment_user_likes_notification",
      "comment_user"."comments_notification" as "comment_user_comments_notification",
      "comment_user"."tags_notification" as "comment_user_tags_notification",
      "comment_user"."created" as "comment_user_created",
      "comment_user"."updated" as "comment_user_updated",
      "comment_user"."deleted" as "comment_user_deleted",
      "comment_user"."personId" as "comment_user_personId",
      "comment_user"."businessId" as "comment_user_businessId",
      "comment_user"."role_id" as "comment_user_role_id",
      "comment_user"."baseBusinessAccountId" as "comment_user_baseBusinessAccountId",
      "comment_user_business"."id" as "comment_user_business_id",
      "comment_user_business"."photo" as "comment_user_business_photo",
      "comment_user_business"."name" as "comment_user_business_name",
      "comment_user_business"."pageName" as "comment_user_business_pageName",
      "comment_user_person"."id" as "comment_user_person_id",
      "comment_user_person"."photo" as "comment_user_person_photo",
      "comment_user_person"."pageName" as "comment_user_person_pageName",
      "comment_user_person"."fullName" as "comment_user_person_fullName",
      "comment_user_followers"."id" as "comment_user_followers_id",
      "comment_user_followers"."created" as "comment_user_followers_created",
      "comment_user_followers"."accountId" as "comment_user_followers_accountId",
      "comment_user_followers"."followedAccountId" as "comment_user_followers_followedAccountId",
      "comment_mention"."id" as "comment_mention_id",
      "comment_mention"."mention_text" as "comment_mention_mention_text",
      "comment_mention"."created" as "comment_mention_created",
      "comment_mention"."targetId" as "comment_mention_targetId",
      "comment_mention"."${entityName}Id" as "comment_mention_${entityName}Id",
      "comment_mention"."photoId" as "comment_mention_photoId",
      "comment_mention"."textId" as "comment_mention_textId",
      "comment_mention"."videoId" as "comment_mention_videoId",
      "comment_mention"."sellerreviewId" as "comment_mention_sellerreviewId",
      "comment_mention"."productId" as "comment_mention_productId",
      "comment_mention"."productReviewId" as "comment_mention_productReviewId",
      "comment_mention"."serviceReviewId" as "comment_mention_serviceReviewId",
      "comment_mention"."serviceId" as "comment_mention_serviceId",
      "comment_mention"."${entityName}CommentId" as "comment_mention_${entityName}CommentId",
      "comment_mention"."${entityName}CommentReplyId" as "comment_mention_${entityName}CommentReplyId",
      "comment_mention"."photoCommentId" as "comment_mention_photoCommentId",
      "comment_mention"."photoCommentReplyId" as "comment_mention_photoCommentReplyId",
      "comment_mention"."textCommentId" as "comment_mention_textCommentId",
      "comment_mention"."textCommentReplyId" as "comment_mention_textCommentReplyId",
      "comment_mention"."videoCommentId" as "comment_mention_videoCommentId",
      "comment_mention"."videoCommentReplyId" as "comment_mention_videoCommentReplyId",
      "comment_mention"."productReviewReplyId" as "comment_mention_productReviewReplyId",
      "comment_mention"."serviceReviewReplyId" as "comment_mention_serviceReviewReplyId",
      "comment_mention"."sellerReviewReplyId" as "comment_mention_sellerReviewReplyId",
      "comment_mention_target"."id" as "comment_mention_target_id",
      "comment_mention_target"."followers_notification" as "comment_mention_target_followers_notification",
      "comment_mention_target"."likes_notification" as "comment_mention_target_likes_notification",
      "comment_mention_target"."comments_notification" as "comment_mention_target_comments_notification",
      "comment_mention_target"."tags_notification" as "comment_mention_target_tags_notification",
      "comment_mention_target"."created" as "comment_mention_target_created",
      "comment_mention_target"."updated" as "comment_mention_target_updated",
      "comment_mention_target"."deleted" as "comment_mention_target_deleted",
      "comment_mention_target"."personId" as "comment_mention_target_personId",
      "comment_mention_target"."businessId" as "comment_mention_target_businessId",
      "comment_mention_target"."role_id" as "comment_mention_target_role_id",
      "comment_mention_target"."baseBusinessAccountId" as "comment_mention_target_baseBusinessAccountId",
      "comment_mention_target_business"."id" as "comment_mention_target_business_id",
      "comment_mention_target_business"."photo" as "comment_mention_target_business_photo",
      "comment_mention_target_business"."name" as "comment_mention_target_business_name",
      "comment_mention_target_business"."pageName" as "comment_mention_target_business_pageName",
      "comment_mention_target_person"."id" as "comment_mention_target_person_id",
      "comment_mention_target_person"."photo" as "comment_mention_target_person_photo",
      "comment_mention_target_person"."pageName" as "comment_mention_target_person_pageName",
      "comment_mention_target_person"."fullName" as "comment_mention_target_person_fullName",
      "comment_liked"."id"::BOOL as "comment_liked",
      "comment"."created" as "comment_created",
      COUNT(distinct("comment_likes"."id")) as "comment_likesCount",
      COUNT(distinct("comment_replies"."id")) as "comment_repliesCount",
      COUNT(distinct("comment_replies"."id")) + COUNT(distinct("comment_likes"."id")) as "comment_relevance"
    from
      "t_${entityName}_comment" "comment"
    left join "t_account" "comment_user" on
      "comment_user"."id" = "comment"."user_id"
    left join "t_business" "comment_user_business" on
      "comment_user_business"."id" = "comment_user"."businessId"
    left join "t_person" "comment_user_person" on
      "comment_user_person"."id" = "comment_user"."personId"
    left join "t_followings" "comment_user_followers" on
      "comment_user_followers"."followedAccountId" = "comment_user"."id"
      and ("comment_user_followers"."accountId" = ${userId})
    left join "t_mentions" "comment_mention" on
      "comment_mention"."${entityName}CommentId" = "comment"."id"
    left join "t_account" "comment_mention_target" on
      "comment_mention_target"."id" = "comment_mention"."targetId"
    left join "t_business" "comment_mention_target_business" on
      "comment_mention_target_business"."id" = "comment_mention_target"."businessId"
    left join "t_person" "comment_mention_target_person" on
      "comment_mention_target_person"."id" = "comment_mention_target"."personId"
    left join "t_${entityName}_comment_replies" "comment_replies" on
      "comment_replies"."comment_id" = "comment"."id"
    left join "t_${entityName}_comment_like" "comment_likes" on
      "comment_likes"."comment_id" = "comment"."id"
    left join "t_${entityName}_comment_like" "comment_liked" on
      "comment_liked"."comment_id" = "comment"."id"
      and ("comment_liked"."user_id" = ${userId})
    where
      "comment"."${entityName}_id" = ${entityId}
    group by
      comment.*,
      comment_likes.*,
      comment_liked.*,
      comment_user.*,
      comment_mention.*,
      comment_mention_target.*,
      comment_mention_target_person.*,
      comment_mention_target_business.*,
      comment_user_person.*,
      comment_user_business.*,
      comment_user_followers.*
    order by
      ${order} ${sort}
    limit ${limit}
    offset ${offset}
  `;
  return rawQuery
};

export const getRepliesRawQuery = (
  entityName: string,
  commentId: string | number,
  currentUserId: string | number,
  limit: number,
  offset: number,
  order: string,
  sort: string,
): string => {
  const rawQuery = `
    select
      "reply"."id" as "reply_id",
      "reply"."text" as "reply_text",
      "reply"."created" as "reply_created",
      "reply"."updated" as "reply_updated",
      "reply"."comment_id" as "reply_comment_id",
      "reply"."user_id" as "reply_user_id",
      "reply"."created" as "reply_created",
      "user"."id" as "user_id",
      "user"."followers_notification" as "user_followers_notification",
      "user"."likes_notification" as "user_likes_notification",
      "user"."comments_notification" as "user_comments_notification",
      "user"."tags_notification" as "user_tags_notification",
      "user"."created" as "user_created",
      "user"."updated" as "user_updated",
      "user"."deleted" as "user_deleted",
      "user"."personId" as "user_personId",
      "user"."businessId" as "user_businessId",
      "user"."role_id" as "user_role_id",
      "user"."baseBusinessAccountId" as "user_baseBusinessAccountId",
      "user_business"."id" as "user_business_id",
      "user_business"."photo" as "user_business_photo",
      "user_business"."name" as "user_business_name",
      "user_business"."pageName" as "user_business_pageName",
      "user_person"."id" as "user_person_id",
      "user_person"."photo" as "user_person_photo",
      "user_person"."pageName" as "user_person_pageName",
      "user_person"."fullName" as "user_person_fullName",
      "reply_mention"."id" as "reply_mention_id",
      "reply_mention"."mention_text" as "reply_mention_mention_text",
      "reply_mention"."created" as "reply_mention_created",
      "reply_mention"."targetId" as "reply_mention_targetId",
      "reply_mention"."articleId" as "reply_mention_articleId",
      "reply_mention"."photoId" as "reply_mention_photoId",
      "reply_mention"."textId" as "reply_mention_textId",
      "reply_mention"."videoId" as "reply_mention_videoId",
      "reply_mention"."sellerreviewId" as "reply_mention_sellerreviewId",
      "reply_mention"."productId" as "reply_mention_productId",
      "reply_mention"."productReviewId" as "reply_mention_productReviewId",
      "reply_mention"."serviceReviewId" as "reply_mention_serviceReviewId",
      "reply_mention"."serviceId" as "reply_mention_serviceId",
      "reply_mention"."articleCommentId" as "reply_mention_articleCommentId",
      "reply_mention"."articleCommentReplyId" as "reply_mention_articleCommentReplyId",
      "reply_mention"."photoCommentId" as "reply_mention_photoCommentId",
      "reply_mention"."photoCommentReplyId" as "reply_mention_photoCommentReplyId",
      "reply_mention"."textCommentId" as "reply_mention_textCommentId",
      "reply_mention"."textCommentReplyId" as "reply_mention_textCommentReplyId",
      "reply_mention"."videoCommentId" as "reply_mention_videoCommentId",
      "reply_mention"."videoCommentReplyId" as "reply_mention_videoCommentReplyId",
      "reply_mention"."productReviewReplyId" as "reply_mention_productReviewReplyId",
      "reply_mention"."serviceReviewReplyId" as "reply_mention_serviceReviewReplyId",
      "reply_mention"."sellerReviewReplyId" as "reply_mention_sellerReviewReplyId",
      "reply_mention_target"."id" as "reply_mention_target_id",
      "reply_mention_target"."followers_notification" as "reply_mention_target_followers_notification",
      "reply_mention_target"."likes_notification" as "reply_mention_target_likes_notification",
      "reply_mention_target"."comments_notification" as "reply_mention_target_comments_notification",
      "reply_mention_target"."tags_notification" as "reply_mention_target_tags_notification",
      "reply_mention_target"."created" as "reply_mention_target_created",
      "reply_mention_target"."updated" as "reply_mention_target_updated",
      "reply_mention_target"."deleted" as "reply_mention_target_deleted",
      "reply_mention_target"."personId" as "reply_mention_target_personId",
      "reply_mention_target"."businessId" as "reply_mention_target_businessId",
      "reply_mention_target"."role_id" as "reply_mention_target_role_id",
      "reply_mention_target"."baseBusinessAccountId" as "reply_mention_target_baseBusinessAccountId",
      "reply_mention_target_business"."id" as "reply_mention_target_business_id",
      "reply_mention_target_business"."photo" as "reply_mention_target_business_photo",
      "reply_mention_target_business"."name" as "reply_mention_target_business_name",
      "reply_mention_target_business"."pageName" as "reply_mention_target_business_pageName",
      "reply_mention_target_person"."id" as "reply_mention_target_person_id",
      "reply_mention_target_person"."photo" as "reply_mention_target_person_photo",
      "reply_mention_target_person"."pageName" as "reply_mention_target_person_pageName",
      "reply_mention_target_person"."fullName" as "reply_mention_target_person_fullName",
      "reply_liked"."id"::BOOL as "reply_liked",
      (select count(id) from t_${entityName}_comment_like reply_likes where "reply_likes"."reply_id" = "reply"."id") as "likesCount"
    from
      "t_${entityName}_comment_replies" "reply"
    left join "t_account" "user" on
      "user"."id" = "reply"."user_id"
    left join "t_business" "user_business" on
      "user_business"."id" = "user"."businessId"
    left join "t_person" "user_person" on
      "user_person"."id" = "user"."personId"
    left join "t_mentions" "reply_mention" on
      "reply_mention"."${entityName}CommentReplyId" = "reply"."id"
    left join "t_account" "reply_mention_target" on
      "reply_mention_target"."id" = "reply_mention"."targetId"
    left join "t_business" "reply_mention_target_business" on
      "reply_mention_target_business"."id" = "reply_mention_target"."businessId"
    left join "t_person" "reply_mention_target_person" on
      "reply_mention_target_person"."id" = "reply_mention_target"."personId"
    left join "t_${entityName}_comment_like" "reply_likes" on
      "reply_likes"."reply_id" = "reply"."id"
    left join "t_${entityName}_comment_like" "reply_liked" on
      "reply_liked"."reply_id" = "reply"."id"
      and ("reply_liked"."user_id" = ${currentUserId})
    where
      "reply"."comment_id" = ${commentId}
    order by
      ${order} ${sort}
    limit ${limit}
    offset ${offset}
  `;
  return rawQuery
};