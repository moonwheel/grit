import * as EventEmitter from 'events';
import { Request, Response } from 'express';
import { Notification } from '../entities/notification';


export type ServerEventType = 'file_processing_complete' | 'following_request' | 'new_notification' | 'attachment_file_processing_complete';

export interface ServerEvent {
    userId: number,
    type: ServerEventType,
    notification?: Notification
    entityType?: string,
    entity?: any,
    videoId?: string,
    photoId?: string,
    documentId?: string,
    temporaryId?: string,
}

export const subscribe = (req: Request, res: Response) => {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        "Access-Control-Allow-Origin": "*",
        Connection: 'keep-alive'
    });

    // res.write('\n');
    // res.flushHeaders()

    // console.log('subscribe')

    // This listener receives message from cluster's master and sends SSE when somebody calls publish({...})
    // Passing to master and back to worker necessary for EventEmitter work with Cluster
    process.on('message', function (msg) {
        if (msg.type === 'masterToWorkerSSE') {
            // console.log('Worker ' + process.pid + ' received message from master.', msg.data);
            res.write(`id: ${msg.data.userId}\n`)
            res.write(`event: message\n`);
            res.write(`data: ${JSON.stringify(msg.data)}\n\n`);
            res.flushHeaders()

        }
    });


    const heartbit = setInterval(() => {
        // console.log('heartbit')
        res.write('id: 1')
        res.write(`event: message\n`);
        res.write(`data: ${JSON.stringify({ status: 'heartbit' })}\n\n`)
        res.flushHeaders()
    }, 15000);


    process.send({
        type: 'createListenerSSE',
        data: {
            userId: req.user.id
        }
    });

    req.on('close', () => {
        clearInterval(heartbit);
        process.send({ type: 'closeSSE', data: {} });
        // emitter.removeListener('event', onEvent);
        res.end()
    });
}

// export const fakePublish = (req, res) => {
//     const eventData = {
//         foo: 'bar',
//         userId: 1
//     }
//     // console.log('fakePublish', eventData)

//     // Send to master due to EventSurce don't work correct in clusters' workers
//     process.send({ type: 'emitSSE', data: { ...eventData } });

//     res.send({})
// }

export const publish = (eventData: ServerEvent) => {
    // console.log('publish', eventData)
    // const stringData = JSON.stringify(eventData)
    // emitter.emit('event', eventData);

    // Send to master due to EventSurce don't work correct in clusters' workers
    process.send({ type: 'emitSSE', data: { ...eventData } });

}

