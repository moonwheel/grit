import { getRepository, getConnection, Repository } from "typeorm";
import { Person } from "../entities/person";
import { Business } from "../entities/business";
import { PageParams } from "../interfaces/global/page.interface";
import { set, camelCase, merge, mergeWith, isArray, flatMap } from "lodash";
import { Followings } from "../entities/followings";
import {ISeller} from "../interfaces/payment/payment.interface";

function customizer(objValue: any, srcValue: any) {
    if (isArray(objValue)) {
        const concated = objValue.concat(srcValue);
        const everyItemHasId = concated.every(item => item && 'id' in item);

        if (!everyItemHasId) {
            return concated;
        }

        const withSameIds: { [id: string]: any[] } = {};

        for (const item of concated) {
            if ('id' in item) {
                if (item.id in withSameIds) {
                    withSameIds[item.id].push(item);
                } else {
                    withSameIds[item.id] = [item];
                }
            }
        }

        const result = Object.keys(withSameIds).map(id => {
            const itemsWithSameId = withSameIds[id];

            return itemsWithSameId.reduce(
                (acc, item) => mergeWith(item, acc, customizer),
                {}
            );
        });

        return result;
    }
}

export class Helpers {
    static getUserById = async (id: number) => {
        try {
            const user = await getRepository(Person).findOne({
                where: { id },
            });
            return user;
        } catch (error) {
            throw error;
        }
    };

    static getUserByEmail = async (email: string) => {
        try {
            const user = await getRepository(Person).findOne({
                where: { email },
            });
            return user;
        } catch (error) {
            throw error;
        }
    };

    static beautifyRawData  = async (
        rawArray: Record<string, any>[] = [],
        options: {
            name?: string;
            sanitize?: string[];
            aggregate?: string[];
        }
    ) => {
        try {
            const beautifyedData = rawArray.map((rawObject: Record<string, any>) => {
                const properties = Object.keys(rawObject);
                return properties.reduce(
                    (aggr: Record<string, any>, item: string) => {
                        let property: string = item;
                        if (options.sanitize) property = Helpers.propertySanitizer(property, options.sanitize);
                        const chain = property.replace(new RegExp("^" + options.name + "_"),"").replace(/_/g,".");
                        return set(aggr, chain, rawObject[item]);
                    },
                    {}
                );
            });
            return options.aggregate
                ? await Helpers.aggregateData(beautifyedData, options.aggregate)
                : beautifyedData;
        } catch (error) {
            throw error;
        }
    };

    static propertySanitizer = (property: string, sanitize: string[]) => {
        try {
            let newProperty = property;
            for (let index = 0; index < sanitize.length; index++) {
                const template = sanitize[index];
                newProperty = newProperty.replace(template, camelCase(template));
            }
            return newProperty;
        } catch (error) {
            throw error;
        }
    };

    static aggregateData = (data: Record<string, any>[], aggregate: string[] = []) => {
        try {
            const wrap = (data: Record<string, any>[], aggregate: string[] = []) => {
                return data.map((item: Record<string, any>[]) => {
                    aggregate.forEach(field => {
                        /**
                         * Here we split argument of type 'someKey.someOtherKey.oneMoreKey'
                         * to 'someKey' - which is key of array-like property
                         * and 'someOtherKey' is a key of propery in this array items
                         */
                        const [subArrayKey, ...subItemsKeys] = field.split('.');

                        // if item is array and we have some porperties to aggregate
                        if (subItemsKeys.length && isArray(item[subArrayKey])) {
                            if (subItemsKeys.length === 1) {
                                // if we have only one level depth -
                                // we transform these properies to arrays to correct merge in future
                                const key = subItemsKeys[0];
                                for (const subItem of item[subArrayKey]) {
                                    subItem[key] = [subItem[key]];
                                }
                            } else {
                                // if we have 2+ level depth - do apply this function recursively
                                item[subArrayKey] = wrap(item[subArrayKey], [subItemsKeys.join('.')])
                            }
                        } else {
                            item[field] = [item[field]];
                        }
                    });
                    return item;
                });
            };

            const wraped = wrap(data, aggregate);

            return wraped.reduce(
                (aggr: any[], item: any) => {
                    const indexOf = aggr.findIndex(elem => elem.id === item.id );
                    if ( indexOf + 1) {
                        aggr[indexOf] = mergeWith(aggr[indexOf], item, customizer);
                    } else {
                        aggr.push(item);
                    }
                    return aggr;
                },
                []
            );

        } catch (error) {
            throw error;
        }
    };

    public static getCompanyByName = async (name: string) => {
        try {
            return await getRepository(Business).findOne({ where: { name } });
        } catch (error) {
            throw error;
        }
    };

    public static nullCheck = async (value: any) => {
        try {
            const duplicate = (n) => typeof n === "object" && n != null ? flatMap(n, duplicate) : n;
            return duplicate(value).join("") ? value : null;
        } catch (error) {
            throw error;
        }
    };

    static generateFeedQuery = async(
        users: Followings[],
        currentUserId: number,
        options: {
            page?: number|string;
            limit?: number|string;
        }
    ) => {
        // console.log('users', users);
        // console.log('currentUserId', currentUserId);
        const params: PageParams = {
            limit: 50,
            offset: 0
        };
        if(options.limit) params.limit = Number(options.limit);
        if(options.page) params.offset = (Number(options.page) - 1) * params.limit;
        const tables = ['article', 'photo', 'text', 'video', 'service']
        const columns = await Promise.all(tables.map(async table => await getConnection().query(
            `SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS where Table_name like 't_${table}'`
        )));
        const filtered = columns.map(nested => nested.map(arr => arr.column_name ));
        const uniqResults = Array.from(new Set(filtered.flat()));
        const eachSelect = await Promise.all(tables.map((table, key) => {
            return `
                select '${table}' as "tableName",

                (CASE WHEN COUNT("entity_mention"."id") > 0 THEN
                    jsonb_agg(
                        jsonb_build_object(
                            'id', "entity_mention"."id",
                            'mention_text', "entity_mention"."mention_text",
                            'created', "entity_mention"."created",
                            'target', jsonb_build_object(
                                'id', "entity_mention_target"."id",
                                'created', "entity_mention_target"."created",
                                'baseBusinessAccountId', "entity_mention_target"."baseBusinessAccountId",
                                'person', jsonb_build_object(
                                    'fullName', "entity_mention_target_person"."fullName",
                                    'pageName', "entity_mention_target_person"."pageName",
                                    'photo', "entity_mention_target_person".photo,
                                    'thumb', "entity_mention_target_person".thumb
                                ),
                                'business', (CASE WHEN "entity_mention_target_business"."id" IS NOT NULL
                                    THEN jsonb_build_object(
                                            'name', "entity_mention_target_business"."name",
                                            'pageName', "entity_mention_target_business"."pageName",
                                            'photo', "entity_mention_target_business".photo,
                                            'thumb', "entity_mention_target_business".thumb
                                        )
                                    ELSE NULL
                                    END
                                )
                            )
                        )
                    )
                ELSE '[]'
                END
                ) as "mentions",

                    ${table !== 'service' ? `COUNT(DISTINCT likes.id)` : 'null'} AS "likesCount",
                    ${table !== 'service' ? `COUNT(DISTINCT (views."user_id"))`: 'null'} AS "viewsCount",
                    ${table !== 'service' ? `COUNT(DISTINCT comments.id)` : 'null'} AS "commentsCount",
                    ${table !== 'service' ? `COALESCE(liked.id::BOOL, false)` : 'null'} as "liked",
                    ${table !== 'service' ? `COALESCE(viewed.id::BOOL, false)` : 'null'} as "viewed",
                    COALESCE(bookmarked.id::BOOL, false) as "bookmarked",
                    COUNT(DISTINCT bookmarks.id) as "bookmarksCount",

                    ${table === 'service' ? `MIN("date"."from")` : 'null'} AS "date_from",
                    ${table === 'service' ? `MIN("date"."to")`: 'null'} AS "date_to",
                    ${table === 'service' ? `AVG(review.rating)` : 'null'} AS "average_rating",
                    ${table === 'service' ? `COUNT(DISTINCT review.id)` : 'null'} as "amount_reviews",
                    ${table === 'service' ? `json_agg(JSON_BUILD_OBJECT(
                        'id', photos.id,
                        'link', photos.link,
                        'thumb', photos.thumb
                    ))` : 'null'} as "photos",
                    ${table === 'service' ? `JSON_BUILD_OBJECT(
                        'id', schedule.id,
                        'duration', schedule.duration,
                        'byDate', json_agg(JSON_BUILD_OBJECT(
                            'id', date.id,
                            'from', date.from,
                            'to', date.to
                        ))
                    )` : 'null'} as "schedule",

                    ${table !== 'text' && table !== 'service' ? `
                    CASE WHEN entity_business_address.id IS NOT NULL
                        THEN jsonb_build_object(
                                'id', entity_business_address.id,
                                'country', entity_business_address.country,
                                'street', entity_business_address.street,
                                'addrType', entity_business_address."addrType",
                                'appendix', entity_business_address.appendix,
                                'careOf', entity_business_address."careOf",
                                'city', entity_business_address.city,
                                'created', entity_business_address.created,
                                'deleted', entity_business_address.deleted,
                                'fullName', entity_business_address."fullName",
                                'lat', entity_business_address.lat,
                                'lng', entity_business_address.lng,
                                'selected', entity_business_address.selected,
                                'state', entity_business_address.state,
                                'type', entity_business_address.type,
                                'updated', entity_business_address.updated,
                                'zip', entity_business_address.zip
                        )
                        ELSE
                            NULL
                        END
                    `
                    : 'null'} as "entity_business_address",
                    ${uniqResults.map(column => {
                        if (filtered[key].includes(column)) {
                            return `${table}."${column}" AS "${column}"`;
                        } else {
                            if(table === 'service' && (column === 'photo' || column === 'thumb' )) {
                                return `photo."${column === 'photo' ? 'link' : column}" AS "${column}"`
                            }
                            if (table === 'service' && column === 'duration') {
                                return 'schedule.duration as "duration"'
                            }
                            return `null as "${column}"`;
                        }
                    }).join(', ')},
                    (
                        select
                            jsonb_build_object(
                                'id', acc.id,
                                'person', jsonb_build_object(
                                    'fullName', person."fullName",
                                    'pageName', person."pageName",
                                    'photo', person.photo,
                                    'thumb', person.thumb
                                ),
                                'business', (CASE WHEN acc."businessId" IS NOT NULL
                                    THEN jsonb_build_object(
                                            'name', business."name",
                                            'pageName', business."pageName",
                                            'photo', business.photo,
                                            'thumb', business.thumb
                                        )
                                    ELSE NULL
                                    END
                                )
                            )
                            FROM t_account as acc
                            LEFT OUTER JOIN t_person AS person ON
                                acc."personId" = person.id
                            LEFT OUTER JOIN t_business AS business ON
                                acc."businessId" = business.id
                            WHERE acc.id = ${table}."user_id"
                            GROUP BY
                                acc.*,
                                person.*,
                                business.*
                            LIMIT 1
                    )  as "user"
                from t_${table} AS ${table}
                LEFT JOIN t_bookmarks AS bookmarks ON bookmarks.${table}_id = ${table}.id
                LEFT JOIN t_bookmarks AS bookmarked ON bookmarked.${table}_id = ${table}.id AND bookmarked."owner_id" = ${currentUserId}

                LEFT JOIN "t_mentions" "entity_mention" ON
                    "entity_mention"."${table}Id" = ${table}.id
                LEFT JOIN "t_account" "entity_mention_target" ON
                    "entity_mention_target"."id" = "entity_mention"."targetId"
                LEFT JOIN "t_business" "entity_mention_target_business" ON
                    "entity_mention_target_business"."id" = "entity_mention_target"."businessId"
                LEFT JOIN "t_person" "entity_mention_target_person" ON
                    "entity_mention_target_person"."id" = "entity_mention_target"."personId"

                ${table !== 'service'
                ?
                `LEFT JOIN t_${table}_like AS likes ON likes.${table}_id = ${table}.id
                LEFT JOIN t_${table}_view AS views ON views."${table}_id" = ${table}.id
                LEFT JOIN t_${table}_comment AS comments ON comments.${table}_id = ${table}.id
                LEFT JOIN t_${table}_like AS liked ON liked.${table}_id = ${table}.id AND liked."user_id" = ${currentUserId}
                LEFT JOIN t_${table}_view AS viewed ON viewed.${table}_id = ${table}.id AND viewed."user_id" = ${currentUserId}
                ${table !== 'text' ?
                    'LEFT JOIN t_address AS entity_business_address ON entity_business_address.id = ' + table + '.business_address'
                    : ''}
                `
                :
                `LEFT JOIN t_service_review AS review ON
                review.service_id = service.id
                    LEFT JOIN t_service_photo AS photo ON
                        photo.service_id = service.id AND photo."index" = 0
                    LEFT JOIN t_service_photo AS photos ON
                        photos.service_id = service.id
                    LEFT JOIN t_service_schedule AS schedule ON
                        schedule.service_id = service.id
                    LEFT JOIN t_address AS address ON
                        service.address_id = address.id
                    LEFT JOIN t_schedule_date AS "date" ON
                        "date"."scheduleId" = schedule.id
                `}
                where
                    ${
                        users.length
                            ? `(${
                                    users.map(user => `
                                        (${table}."user_id" = ${user.followedAccount.id} and (${table}."created"::timestamp) > ${Math.round(new Date(user.created).getTime()/1000)}::timestamp)
                                    `).join(' or ')
                                } or `
                            : '('
                    }
                    ${table}."user_id" = ${currentUserId})
                    and ${table}.draft = false
                    and ${table}.deleted is null
                GROUP BY
                ${table !== 'service'
                ?
                    `liked.id,
                    viewed.id,
                    ${table !== 'text' ? 'entity_business_address.*,' : ''}
                    `
                : `
                    photo.*,
                    schedule.*,
                `}
                bookmarked.id,
                ${table}.*
            `;
        }));

        const queryString = eachSelect.join(' union all ') + ` order by created desc offset ${params.offset || 0} rows fetch next ${params.limit || 50} rows only`;
        // console.log('===============feed queryString', queryString)
        return queryString;
    };


    static generateTimelineQuery = async(
        targetUserId: number,
        currentUserId: number,
        options: {
            page?: number|string;
            limit?: number|string;
            isPrivateAcc: boolean;
        },
    ) => {
        const params: PageParams = {
            limit: 50,
            offset: 0
        };

        if (options.limit) params.limit = Number(options.limit);
        if (options.page) params.offset = (Number(options.page) - 1) * params.limit;

        let tables: string[];

        if (options.isPrivateAcc) {
            tables = ['service'];
        } else {
            tables = ['article', 'photo', 'text', 'video', 'service'];
        }

        const columns = await Promise.all(tables.map(async table => await getConnection().query(
            `SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS where Table_name like 't_${table}'`
        )));

        const filtered = columns.map(nested => nested.map(arr => arr.column_name ));
        const uniqResults = Array.from(new Set(filtered.flat()));

        const eachSelect = await Promise.all(tables.map((table, key) => {
            return `
                select '${table}' as "tableName",
                (CASE WHEN COUNT("entity_mention"."id") > 0 THEN
                    jsonb_agg(
                        jsonb_build_object(
                            'id', "entity_mention"."id",
                            'mention_text', "entity_mention"."mention_text",
                            'created', "entity_mention"."created",
                            'target', jsonb_build_object(
                                'id', "entity_mention_target"."id",
                                'created', "entity_mention_target"."created",
                                'baseBusinessAccountId', "entity_mention_target"."baseBusinessAccountId",
                                'person', jsonb_build_object(
                                    'fullName', "entity_mention_target_person"."fullName",
                                    'pageName', "entity_mention_target_person"."pageName",
                                    'photo', "entity_mention_target_person".photo,
                                    'thumb', "entity_mention_target_person".thumb
                                ),
                                'business', (CASE WHEN "entity_mention_target_business"."id" IS NOT NULL
                                    THEN jsonb_build_object(
                                            'name', "entity_mention_target_business"."name",
                                            'pageName', "entity_mention_target_business"."pageName",
                                            'photo', "entity_mention_target_business".photo,
                                            'thumb', "entity_mention_target_business".thumb
                                        )
                                    ELSE NULL
                                    END
                                )
                            )
                        )
                    )
                ELSE '[]'
                END
                ) as "mentions",
                ${table !== 'service' ? `COUNT(DISTINCT likes.id)` : 'null'} AS "likesCount",
                ${table !== 'service' ? `COUNT(DISTINCT (views."user_id"))`: 'null'} AS "viewsCount",
                ${table !== 'service' ? `COUNT(DISTINCT comments.id)` : 'null'} AS "commentsCount",
                ${table !== 'service' ? `COALESCE(liked.id::BOOL, false)` : 'null'} as "liked",
                ${table !== 'service' ? `COALESCE(viewed.id::BOOL, false)` : 'null'} as "viewed",
                COALESCE(bookmarked.id::BOOL, false) as "bookmarked",
                COUNT(DISTINCT bookmarks.id) as "bookmarksCount",

                ${table === 'service' ? `MIN("date"."from")` : 'null'} AS "date_from",
                ${table === 'service' ? `MIN("date"."to")`: 'null'} AS "date_to",
                ${table === 'service' ? `AVG(review.rating)` : 'null'} AS "average_rating",
                ${table === 'service' ? `COUNT(DISTINCT review.id)` : 'null'} as "amount_reviews",

                ${uniqResults.map(column => {
                    if (filtered[key].includes(column)) {
                        return `${table}."${column}" AS "${column}"`;
                    } else {
                        if (table === 'service' && (column === 'photo' || column === 'thumb' )) {
                            return `photo."${column === 'photo' ? 'link' : column}" AS "${column}"`
                        }
                        if (table === 'service' && column === 'duration') {
                            return 'schedule.duration as "duration"'
                        }
                        return `null as "${column}"`;
                    }
                }).join(', ')}
                from t_${table} AS ${table}
                LEFT JOIN t_bookmarks AS bookmarks ON bookmarks.${table}_id = ${table}.id
                LEFT JOIN t_bookmarks AS bookmarked ON bookmarked.${table}_id = ${table}.id AND bookmarked."owner_id" = ${currentUserId}
                LEFT JOIN "t_mentions" "entity_mention" ON
                    "entity_mention"."${table}Id" = ${table}.id
                LEFT JOIN "t_account" "entity_mention_target" ON
                    "entity_mention_target"."id" = "entity_mention"."targetId"
                LEFT JOIN "t_business" "entity_mention_target_business" ON
                    "entity_mention_target_business"."id" = "entity_mention_target"."businessId"
                LEFT JOIN "t_person" "entity_mention_target_person" ON
                    "entity_mention_target_person"."id" = "entity_mention_target"."personId"
                ${table !== 'service'
                ?
                    `LEFT JOIN t_${table}_like AS likes ON likes.${table}_id = ${table}.id
                    LEFT JOIN t_${table}_view AS views ON views."${table}_id" = ${table}.id
                    LEFT JOIN t_${table}_comment AS comments ON comments.${table}_id = ${table}.id
                    LEFT JOIN t_${table}_like AS liked ON liked.${table}_id = ${table}.id AND liked."user_id" = ${currentUserId}
                    LEFT JOIN t_${table}_view AS viewed ON viewed.${table}_id = ${table}.id AND viewed."user_id" = ${currentUserId}
                    `
                :
                    `LEFT JOIN t_service_review AS review ON
                        review.service_id = service.id
                    LEFT JOIN t_service_photo AS photo ON
                        photo.service_id = service.id AND photo."index" = 0
                    LEFT JOIN t_service_schedule AS schedule ON
                        schedule.service_id = service.id
                    LEFT JOIN t_address AS address ON
                        service.address_id = address.id
                    LEFT JOIN t_schedule_date AS "date" ON
                        "date"."scheduleId" = schedule.id AND "date"."from" > NOW()
                `}
                where
                    ${table}."user_id" = ${targetUserId}
                    ${table === 'service' ? `and service.active = true` : ''}
                    and ${table}.draft = false
                    and ${table}.deleted is null

                GROUP BY
                ${table !== 'service'
                ?
                    `liked.id,
                    viewed.id,
                    `
                    : `
                    photo.*,
                    schedule.*,
                    `}
                    bookmarked.id,
                ${table}.*
            `;
        }));

        const queryString = eachSelect.join(' union all ') + ` order by created desc offset ${params.offset || 0} rows fetch next ${params.limit || 50} rows only`;

        // console.log('timeline queryString', queryString);
        return queryString;
    };

    static getLikedStatus = async <T>(repo: Repository<T>, entityName: string, entityId: number, userId: number) => {
        const liked = await repo
            .createQueryBuilder(`${entityName}`)
            .leftJoin(`${entityName}.likes`, "likes")
            .addSelect("likes.user_id")
            .where(`${entityName}.id = :id`, { id: entityId })
            .andWhere("likes.user_id = :account", { account: userId })
            .getOne();

        return !!liked;
    }
}

export function calculateTotalPrice(data): number {
    const cost = [];

    // TODO: bad calculation for shippingCost (we need to count all shippings from unique user, not max value from all!!!)
    data.forEach(item => cost.push(item.product.shippingCosts));
    const shippingCost = Math.max(...cost);

    const price = data.reduce((sum, item) => {
        const product = item.product as any;

        if (product.selectedVariant) {
            return sum + (item.quantity * product.selectedVariant.price);
        }

        return sum + (item.quantity * product.price);
    }, 0);

    const totalPrice = price + shippingCost;
    return totalPrice;
}

export async function getCalculatedSellers(userId: number): Promise<{
    sellers: Array<any>,
    totalPrice: number,
}> {
    const sellers = await getConnection().query(`
        SELECT
            t_product.user_id AS seller_id,
            t_person."mango_user_id" AS personMangoPayId,
            t_person."wallet" AS personWalletId,
            t_business."mango_user_id" AS businessMangoPayId,
            t_business."wallet" AS businessWalletId,
            SUM(CASE
                WHEN t_product.price IS NOT NULL THEN t_product.price * t_cart_product.quantity
                WHEN t_product_variant.price IS NOT NULL THEN t_product_variant.price * t_cart_product.quantity
                ELSE 0
            END) + MAX(CASE
                    WHEN t_cart_product.collected = false THEN t_product."shippingCosts"
                    ELSE 0
                    END) AS totalSum,
            MAX(CASE
                WHEN t_cart_product.collected = false THEN t_product."shippingCosts"
                ELSE 0
                END) as shippingPrice
        FROM t_cart_product
            LEFT JOIN t_product ON t_cart_product.product_id = t_product.id
            LEFT JOIN t_product_variant ON t_cart_product.product_variant_id = t_product_variant.id
            LEFT JOIN t_account ON t_product.user_id = t_account.id
            LEFT JOIN t_business ON t_business.id = t_account."businessId"
            LEFT JOIN t_person ON t_person.id = t_account."personId"
        WHERE t_cart_product.user_id = ${userId}
        GROUP BY seller_id, personMangoPayId, personWalletId, businessMangoPayId, businessWalletId;
    `);

    const totalPrice = sellers
        .reduce((sum: number, item: any) => sum + Number(item.totalsum), 0);

    return { sellers, totalPrice };
}

// REMOVE NULL VALUES FROM RAW DATA
export const sanitizeRawData = (rawData: any[]) => {
  rawData.map(object => {
    for (const key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        const element = object[key];
        // console.log('element', element)
        if (element === null) delete object[key];
      }
    }
  })
  return rawData

}
