import * as moment from 'moment';

export interface IAppointment {
    id: number;
    created?: number;
    updated?: number;
    start: string | number;
    end: string | number;
    booking?: number;
}

export function calculateBookingHours(apointments: IAppointment[]): number {
    if (!apointments.length) {
        throw new Error("Appointment time is not specified.");
    }

    return apointments.reduce((sum, date) => sum + moment(new Date(date.end)).diff(moment(new Date(date.start)), 'hour'), 0);
}
