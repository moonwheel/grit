import MinioInstance from "./minio";
import * as fs from "fs";
import { App } from '../config/app.config'
import { publish } from "./sse";
const sharp = require('sharp');

// const { Worker, isMainThread } = require('worker_threads');
const path = require('path');

export interface ConvertedVideo {
  cover?: string;
  video?: string;
}

export interface ConvertedPhoto {
  thumb?: string;
  photo?: string;
}

export class MediaConverter {
  static convertPhotoAndUploadOnMinio = async (
    filepath: string,
    uniqueFileName: string,
    photoSize: number,
    thumbSize?: number,
    tempFile?: boolean,
  ): Promise<void> => {
    try {
      const image = sharp(filepath)
        .flatten({
          background: 'white'
        })
        .rotate();

      const metadata = await image.metadata()
      let outputFiles: any = {};

      if (metadata.width > 750 || metadata.height > 750) {
        await image
          .resize(photoSize, photoSize, {
            fit: 'inside',
          })
          .toFile(`${App.UPLOAD_FOLDER}/${uniqueFileName}-photo.jpg`);
      } else {
        await image
          .toFile(`${App.UPLOAD_FOLDER}/${uniqueFileName}-photo.jpg`);
      }

      outputFiles.photo = `${uniqueFileName}-photo.jpg`

      if (thumbSize) {
        await image
          .resize(thumbSize, thumbSize, {
            fit: 'cover',
          })
          .toFile(`${App.UPLOAD_FOLDER}/${uniqueFileName}-thumb.jpg`);

        outputFiles.thumb = `${uniqueFileName}-thumb.jpg`
      }

      await Promise.all(
        ['photo', 'thumb'].map(async key => {
          const originName = outputFiles[key];
          const path = `${App.UPLOAD_FOLDER}/${originName}`;
          const name = tempFile ? `${App.TEMP_FOLDER_PREFIX}${originName}` : originName;

          await MinioInstance.uploadFile('photo', name, path);

          fs.unlinkSync(path);

          return `ok`;
        })
      );
      // remove original image from our server (not miinio)
      fs.unlinkSync(path.join('/app', filepath));

    } catch (error) {
      console.log('media converter error', error)
    }
  }
}