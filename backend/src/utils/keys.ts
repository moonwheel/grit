import fs  = require ("fs");
import { App } from "../config/app.config";

export class Keys {
    static privateKey = () => {
        // return fs.readFileSync("/tmp/private-key.pem", "utf8");
        return App.JWT_PRIVATE_KEY || fs.readFileSync("/tmp/private-key.pem", "utf8");;
    };

    static publicKey = () => {
        // return fs.readFileSync("/tmp/public-key.pem", "utf8");
        return App.JWT_PUBLIC_KEY || fs.readFileSync("/tmp/public-key.pem", "utf8");
    };
}