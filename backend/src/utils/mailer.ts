import { App } from "../config/app.config";
const mailjet = require ("node-mailjet").connect(App.MAILJET_APIKEY_PUBLIC, App.MAILJET_APIKEY_PRIVATE);

export default (
    options: {
        from?: string;
        to: string;
        subject: string;
        text?: string;
        html?: string;
    }) => mailjet
    .post("send", {"version": "v3.1"})
    .request({
        Messages: [{
            From: {
                Email: options.from || App.MAILER_FROM,
                Name: "GRIT"
            },
            To: [{
                Email: options.to,
            }],
            Subject: options.subject,
            TextPart: options.text || "",
            HTMLPart: options.html || ""
        }]
    });
