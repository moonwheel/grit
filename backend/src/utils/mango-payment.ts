import axios from 'axios';
import { App } from "../config/app.config";
import * as qs from 'qs';
import {
    IOpts,
    Currency,
    INaturalUser,
    IWalletResponse,
    IBankAccountResponse,
    INaturalUserResponse,
    IKycDocument,
    ISubmitedValidation,
    ICardRegistrationResponse,
    ICardInfo,
    IPutTokenResponse,
    IPayInRequest,
    IPayInOptions,
    ITransferOptions,
    IPayOutResponse,
    IPayOut,
    IRefundOptions,
    IBankAccount,
    ICard,
    IWallet,
    IBankAllias,
    IBankAlliasResponse, IWalletInfo, ITransferResponse, IWalletTransaction
} from "../interfaces/mango-pay/mango.interface";
class MangoPay {

    /**
     * Return Id
     * @param {Object} user
     */
    public async createNaturalUser(user?: INaturalUser): Promise<{  data: INaturalUserResponse }> {
        const {
            email,
            lastName,
            firstName,
            nationality,
            countryOfResidence
        } = user;

        return axios.post('/users/natural', {
            FirstName: firstName,
            LastName: lastName,
            Email: email,
            Birthday: Date.parse(String(new Date())) / 1000,
            Nationality: nationality,
            CountryOfResidence: countryOfResidence
        }, this.config())
    }

    /**
     * Return Id
     * @param {String} userId
     * @param {String} currency
     */
    public async createWallet(userId: string, currency: Currency): Promise<{ data: IWalletResponse }> {
        return axios.post('/wallets', {
            Owners: [`${userId}`],
            Description: "Grit Wallet",
            Currency: currency
        }, this.config());
    }

    /**
     * Get wallet balance
     * @param {Number} walletId
     */
    public async getWalletBalance(walletId: number): Promise<{amount: number, currency: string}> {
        const {
            data: {
                Balance,
                Owners,
            }
        } = await axios.get(`/wallets/${walletId}`, this.config());
        return {
            amount: Balance.Amount,
            currency: Balance.Currency
        };
    }

    /**
     * GET wallets for user
     */
    public async getWalletForUser(walletId: number | string): Promise<{ data: IWalletInfo }> {
        return axios.get(`/wallets/${walletId}`, this.config());
    }

    /**
     * Create bank account
     * @param {Object} options
     */
    public async createBankAccount(options: IBankAccount): Promise<{ data: IBankAccountResponse }> {
        const {
            city,
            iban,
            userId,
            country,
            name,
            postalCode,
            address
        } = options;

        return axios.post(`/users/${userId}/bankaccounts/iban`, {
            OwnerAddress: {
                AddressLine1: address,
                City: city,
                PostalCode: postalCode,
                Country: country
            },
            OwnerName: name,
            IBAN: iban
        }, this.config());
    }

    /**
     * Deactivate users bank account
     * @param {Number} userId
     * @param {Number} bankAccountId
     */
    public async deactivateBankAccount(userId: number, bankAccountId: number) {
        return axios.put(`/users/${userId}/bankaccounts/${bankAccountId}/`, {
            Active: false
        }, this.config());
    }

    /**
     * Create kyc Document
     * @param {Number} userId
     * @param {Number} Type
     */
    public async createKycDocument(userId: number, Type = 'IDENTITY_PROOF'): Promise<{ data: IKycDocument }> {
        return axios.post(`/users/${userId}/kyc/documents/`,{ Type }, this.config());
    }

    /**
     * Create Kyc Page
     * @param {Number} userId
     * @param {Number} kycDocumentId
     * @param {String} File base64 encoded file
     */
    public async createKycPage(userId: number, kycDocumentId: number, File: string) {
        return axios.post(`/users/${userId}/kyc/documents/${kycDocumentId}/pages/`, { File }, this.config());
    }

    /**
     * Create Kyc Document
     * @param {Number} userId
     * @param {Number} kycDocumentId
     */
    public async submitKycDocument(
        userId: number,
        kycDocumentId: number,
        Status: string = 'VALIDATION_ASKED'
    ): Promise<{ data: ISubmitedValidation }> {
        return axios.put(`/users/${userId}/kyc/documents/${kycDocumentId}`, { Status }, this.config());
    }

    /**
     * Create card
     * @param {Number} userId
     */
    public async cardRegistration(userId: number): Promise<{ data: ICardRegistrationResponse }> {
        return axios.post(`/CardRegistrations`, {
            UserId: `${userId}`,
            Currency : "EUR",
            CardType: "CB_VISA_MASTERCARD"
        }, this.config());
    }

    /**
     * GET card registartion
     * @param {Number} id
     */
    public async getCardRegistraition(id: number) {
        return axios.get(`/cardregistrations/${id}/`, this.config());
    }

    /**
     * Deactivate card
     * @param {Number} id
     */
    public async deactivateCard(id: number) {
        return axios.put(`/cards/${id}/`, {
            Active: false
        }, this.config());
    }

    /**
     * Card info
     * @param {Object} options
     * @constructor
     */
    public async cardInfo(options: ICardInfo): Promise<{ data: string }> {
        const {
            cardRegistrationURL,
            data,
            accessKeyRef,
            cardNumber,
            cardExpirationDate,
            cardCvx
        } = options;

        return axios({
            method: 'post',
            url: cardRegistrationURL,
            data: qs.stringify({
                data,
                accessKeyRef,
                cardNumber,
                cardExpirationDate,
                cardCvx,
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            },
            auth: {
                username: App.MANGO_CLIENT_ID,
                password: App.MANGO_CLIENT_API_KEY
            }
        });
    }

    /**
     * GET cards list
     * @param {Number} userId
     */
    public async getCardsList(userId: number): Promise<ICard[]> {
        try {
            const { data } = await axios.get(`/users/${userId}/cards/`, this.config());
            return data;
        } catch (e) {
            return []
        }
    }

    public async viewCard(id: string): Promise<ICard> {
        const { data } = await axios.get(`/cards/${id}/`, this.config());
        return data;
    }

    /**
     * GET Bank Account list
     * @param {Number} userId
     */
    public async getBankAccountsList(userId: number): Promise<IBankAccountResponse[]> {
        try {
            const { data } = await axios.get(`/users/${userId}/bankaccounts/`, this.config());
            return data;
        } catch (e) {
            return []
        }
    }

    /**
     * Get single bank account
     * @param {Number} userId
     * @param {Number} bankAccountId
     */
    public async getSingleBankAccount(userId: number, bankAccountId: number) {
        const { data } = await axios.get(`/users/${userId}/bankaccounts/${bankAccountId}`, this.config());
        return data;
    }

    /**
     * Put token datas
     * @param {Number} cardId
     * @param {String} dataString
     */
    public async updateTokenDatas(cardId: string, dataString: string): Promise<{ data: IPutTokenResponse }> {
        return axios.put(`/cardregistrations/${cardId}`, {
            RegistrationData: dataString
        }, this.config());
    }

    /**
     * Create Pay In
     * @param {Object} options
     */
    public async createPayIn(options: IPayInOptions): Promise<{ data: IPayInRequest }> {
        const {
            mangoUserId,
            walletId,
            cardId,
            amount
        } = options

        return axios.post(`/payins/card/direct`, {
            AuthorId: mangoUserId,
            DebitedFunds: {
                "Currency": "EUR",
                "Amount": Number(amount)
            },
            Fees: {
                "Currency": "EUR",
                "Amount": 0
            },
            CreditedWalletId: walletId,
            SecureModeReturnURL: "http://my_url_redirection_after_payment",
            SecureMode :"DEFAULT",
            CardID: cardId
        }, this.config());
    }

    /**
     *
     * @param {Number} mangoUserId
     * @param {Number} debitedWalletId
     * @param {Number} creditedWalletId
     * @param {Number} amount
     * @param {Number} feesAmount
     */
    public async createTransfer(
        mangoUserId: string | number,
        debitedWalletId: string | number,
        creditedWalletId: string | number,
        amount: string | number,
        feesAmount: string | number
    ): Promise<ITransferResponse> {
        const { data } = await axios.post(`/transfers`,{
            AuthorId: String(mangoUserId),
            DebitedFunds: {
                Currency: "EUR",
                Amount: amount
            },
            Fees: {
                Currency: "EUR",
                Amount: feesAmount
            },
            DebitedWalletId: String(debitedWalletId),
            CreditedWalletId: String(creditedWalletId)
        }, this.config());

        return data;
    }

    /**
     * Pay Out
     * @param {Object} options
     */
    public async payOut(options: IPayOut): Promise<{ data: IPayOutResponse }> {
        const {
            userId,
            walletId,
            bankAccountId,
            amount
        } = options;

        // TODO Check response
        return axios.post(`/payouts/bankwire`,{
            AuthorId: userId,
            DebitedFunds: {
                Currency: "EUR",
                Amount: amount
            },
            Fees: {
                Currency: "EUR",
                Amount: 0
            },
            DebitedWalletId: walletId,
            BankAccountId: bankAccountId,
            BankWireRef: ""
        }, this.config());
    }

    /**
     * Create refund to card
     * @param {Number} paInId
     * @param {Number} userId
     * @param {Number} amount
     * @param {Number} fees
     */
    public async refundToCard(
        payInId: number,
        userId: number,
        amount: number,
        fees?: number
    ) {
        const { data } = await axios.post(`/payins/${payInId}/refunds`, {
            AuthorId: userId,
            DebitedFunds: {
                Currency: "EUR",
                Amount: amount
            },
            Fees: {
                Currency: "EUR",
                Amount: fees | 0
            }
        }, this.config());

        return data;
    }


    public async refundTransfer(transferId: number, userId: number) {
        const { data } = await axios.post(`/transfers/${transferId}/refunds`, {
            AuthorId: userId,
        }, this.config());
        return data;
    }

    /**
     * CREATE a hook
     */
    public async createHook() {
        return axios.post(`/hooks/`, {
            EventType: "PAYIN_NORMAL_CREATED",
            Url: ""
        }, this.config());
    }

    /**
     * GET Hooks
     */
    public async getHooks() {
        return axios.get(`/hooks/`, this.config());
    }

    /**
     * GET Hook by id
     * @param {Number} id
     */
    public async getHookById(id: number) {
        return axios.get(`/hooks/${id}/`, this.config());
    }

    /**
     * Update Hook by id
     * @param {Number} id
     */
    public async updateHook(id: number): Promise<{ data: {} }> {
        return axios.put(`/hooks/${id}/`, {
            Status: "ENABLED",
            Url: ""
        }, this.config());
    }

    /**
     * GET wallet
     * @param {String} fundsType
     * @param {String} currency
     */
    public async getWallet(fundsType: string = "FEES", currency: string = "EUR"): Promise<{ data: IWallet }> {
        return axios.get(`/clients/wallets/${fundsType}/${currency}/`, this.config())
    }

    /**
     * GET wallet transactions
     * @param {string} walletId
     */
    public async getWalletTransactions(walletId: string): Promise<IWalletTransaction[]> {
        return axios.get(`/wallets/${walletId}/transactions`, this.config());
    }

    /**
     * Create bank alias
     * @param {Number|String} creditedUserId
     * @param {Number|String} ownerName
     * @param {Number|String} walletId
     */
    public async createBankAlias(
        creditedUserId: number | string,
        ownerName: number | string,
        walletId: string | number
    ): Promise<{ data: IBankAlliasResponse }> {

        return axios.post(`/wallets/${walletId}/bankingaliases/iban/`, {
            CreditedUserId: creditedUserId,
            OwnerName: ownerName,
            Country: "LU"
        }, this.config());
    }

    /**
     * Deactivate Bank Alias
     * @param {Number} bankAliasId
     */
    public async deactivateBankAlias(bankAliasId: number) {
        return axios.put(`/bankingaliases/${bankAliasId}/`, {
            Active: true
        }, this.config());
    }

    /**
     * GET bank alias
     * @param {Number} bankAliasId
     */
    public async getBankAlias(bankAliasId: number) {
        const { data } = await axios.get(`/bankingaliases/${bankAliasId}/`, this.config());
        return data;
    }

    /**
     * GET alias for wallet
     * @param {Number} walletId
     */
    public async getAliasForWallet(walletId: number) {
        const { data } = await axios.get(`/wallets/${walletId}/bankingaliases/`);
        return data;
    }

    /**
     * Config
     */
    private config(): IOpts {
        return {
            baseURL: `https://api.sandbox.mangopay.com/v2.01/${App.MANGO_CLIENT_ID}`,
            timeout: 10000,
            auth: {
                username: App.MANGO_CLIENT_ID,
                password: App.MANGO_CLIENT_API_KEY
            }
        }

    }

}

export default new MangoPay();
