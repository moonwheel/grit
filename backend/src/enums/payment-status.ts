export enum PaymentStatus {
  Rejected = 'rejected',
  Paid = 'paid',
  Pending = 'pending',
}
