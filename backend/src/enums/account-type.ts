export enum AccountType {
  Business = 'business',
  Person = 'person',
}
