export enum AccountRole {
  Buyer = 'buyer',
  Seller = 'seller',
}