export enum PaymentType {
  Card = 'card',
  BankAccount = 'bank_account',
}
