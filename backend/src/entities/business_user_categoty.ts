import { Column, Entity, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { User_BusinessUserCategory } from "./user_business_user_category";

@Entity("t_business_user_category")
export class BusinessUserCategory {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("int", {
        nullable: true,
        name: "parent_id"
    })
    parentId: number | null;


    @Column("varchar", {
        nullable: true,
        name: "name"
    })
    name: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToMany(type => User_BusinessUserCategory, t_user_business_user_category => t_user_business_user_category.category_)
    user_business_user_categories: User_BusinessUserCategory[];
}
