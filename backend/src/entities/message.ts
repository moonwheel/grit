import { MessageAttachment } from './message_attachment';
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Chatroom } from "./chatroom";
import { Account } from "./account";
import { Text } from './text';
import { Photo } from './photo';
import { Video } from './video';
import { Article } from './article';
import { Product } from './product';
import { Service } from './service';

@Entity("t_message")
// @Index("booking_service_id_idx", ["service_",])
// @Index("booking_user_id_idx", ["user",])
@Index("message_created_desc_idx", { synchronize: false })
export class Message {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Index('messages_text_idx')
    @Column("varchar", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @OneToMany(type => Message, message => message.reply_to)
    replies: Message[];

    @ManyToOne(type => Message, message => message.replies)
    @JoinColumn({ name: "reply_to" })
    reply_to: Message;

    @Column("boolean", {
        nullable: false,
        default: () => "FALSE",
        name: "is_forwarded"
    })
    is_forwarded: boolean;

    @Column("boolean", {
        nullable: false,
        default: () => "FALSE",
        name: "is_service_message"
    })
    is_service_message: boolean;

    @OneToOne(type => MessageAttachment, t_message_attachment => t_message_attachment.message, { onDelete: 'CASCADE' })
    attachment: MessageAttachment

    @ManyToOne(type => Chatroom, t_chatroom => t_chatroom.chatroom_users, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "chatroom_id" })
    chatroom: Chatroom;

    @ManyToOne(type => Account, t_user => t_user.messages, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Index('chatroom_deleted_idx')
    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @ManyToOne(type => Account)
    @JoinColumn({ name: "shared_user_id" })
    shared_user: Account;

    @ManyToOne(type => Text)
    @JoinColumn({ name: "shared_text_id" })
    shared_text: Text;

    @ManyToOne(type => Photo)
    @JoinColumn({ name: "shared_photo_id" })
    shared_photo: Photo;

    @ManyToOne(type => Video)
    @JoinColumn({ name: "shared_video_id" })
    shared_video: Video;

    @ManyToOne(type => Article)
    @JoinColumn({ name: "shared_article_id" })
    shared_article: Article;

    @ManyToOne(type => Product)
    @JoinColumn({ name: "shared_product_id" })
    shared_product: Product;

    @ManyToOne(type => Service)
    @JoinColumn({ name: "shared_service_id" })
    shared_service: Service;

}
