import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Photo } from "./photo";
import { Account } from "./account";
import { Notification } from "./notification";


@Entity("t_photo_like")
@Index("like_user_id_idx", ["user",])
@Index("like_photo_id_idx", ["photo_",])
export class Photo_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.photos_likes)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Photo, t_photo => t_photo.likes)
    @JoinColumn({ name: "photo_id" })
    photo_: Photo | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.photo_like)
    // notifications: Notification[]
}
