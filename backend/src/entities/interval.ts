import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { ScheduleByDays } from "./schedule_by_days";

@Entity("t_schedule_days_intervals")
export class Interval {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => ScheduleByDays, t_schedule_days => t_schedule_days.intervals, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    scheduleByDay: ScheduleByDays | null;

    @Column("time", {
        nullable: true,
        name: "from"
    })
    from: string | null;

    @Column("time", {
        nullable: true,
        name: "to"
    })
    to: string | null;
}
