import { Column, Entity, PrimaryGeneratedColumn, BeforeUpdate, JoinColumn, ManyToOne, OneToOne, OneToMany, Index } from "typeorm";
import { Person } from "./person";
import { Business } from "./business";
import { Product } from "./product";
import { Service } from "./service";
import { Order } from "./order";

import {addrType} from '../interfaces/address/address';
import {Service_schedule} from "./service_schedule";
import { Article } from './article';
import { Photo } from './photo';
import { Video } from './video';

@Entity("t_address")
export class Address {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: true,
        name: "type"
    })
    type: string | null;

    @Column("varchar", {
        nullable: true,
        name: "addrType"
    })
    addrType: addrType | null;

    @Column("varchar", {
        nullable: true,
        name: "fullName"
    })
    fullName: string | null;

    @Column("varchar", {
        nullable: true,
        name: "careOf"
    })
    careOf: string | null;

    @Column("varchar", {
        nullable: true,
        name: "street"
    })
    street: string | null;

    @Column("varchar", {
        nullable: true,
        name: "appendix"
    })
    appendix: string | null;

    @Column("int", {
        nullable: true,
        name: "zip"
    })
    zip: number | null;

    @Column("varchar", {
        nullable: true,
        name: "city"
    })
    city: string | null;

    @Column("varchar", {
        nullable: true,
        name: "state"
    })
    state: string | null;

    @Column("varchar", {
        nullable: true,
        name: "country"
    })
    country: string | null;

    @Column("bool", {
        nullable: false,
        name: "selected",
        default: () => false,
    })
    selected: boolean | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @Column("float", {
        nullable: true,
        name: "lat"
    })
    lat: number | null;
    @Column("float", {
        nullable: true,
        name: "lng"
    })
    lng: number | null;

    @ManyToOne(type => Person, t_person => t_person.addresses, { onDelete: 'CASCADE' })
    @JoinColumn()
    person: Person;

    @ManyToOne(type => Business, t_business => t_business.addresses, { onDelete: 'CASCADE' })
    @JoinColumn()
    business: Business;

    @OneToOne(type => Person, t_person => t_person.address)
    person_address: Person;

    @OneToMany(type => Product, t_product => t_product.address_, { onDelete: 'CASCADE' })
    @JoinColumn()
    products: Product[];

    @OneToMany(type => Service, t_product => t_product.address, { onDelete: 'CASCADE' })
    @JoinColumn()
    services: Service[];

    @OneToMany(type => Order, t_cart_order => t_cart_order.delivery_address, { onDelete: 'CASCADE' })
    delivery_address: Order[];

    @OneToMany(type => Order, t_cart_order => t_cart_order.billing_address, { onDelete: 'CASCADE' })
    billing_address: Order[];

    @OneToMany(type => Article, article => article.business_address)
    article_location: Article[];

    @OneToMany(type => Photo, photo => photo.business_address)
    photo_location: Photo[];

    @OneToMany(type => Video, video => video.business_address)
    video_location: Video[];

}
