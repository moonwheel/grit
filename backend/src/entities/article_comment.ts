import { Column, Entity, Index, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Article } from "./article";
import { Article_Comment_Like } from "./article_comment_like";
import { Article_Comment_Replies } from "./article_comment_reply";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Report } from "./report";


@Entity("t_article_comment")
@Index("article_comment_user_id_idx", ["user",])
export class Article_Comment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @ManyToOne(type => Account, t_user => t_user.article_comments)
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @ManyToOne(type => Article, t_photo => t_photo.comments, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Article_Comment_Like, t_article_comment_like => t_article_comment_like.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Article_Comment_Like[];

    @OneToMany(type => Article_Comment_Replies, t_article_comment_replies => t_article_comment_replies.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: Article_Comment_Replies[];

    @RelationCount((t_article_comment: Article_Comment) => t_article_comment.likes)
    likesCount: number;

    @RelationCount((t_article_comment: Article_Comment) => t_article_comment.replies)
    repliesCount: number;

    @OneToMany(type => Report, t_report_articlecomment => t_report_articlecomment.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.article_comment)
    @JoinTable()
    mentions: Mention[];

    // @OneToMany(type => Notification, t_notification => t_notification.article_comment)
    // notifications: Notification[];
}
