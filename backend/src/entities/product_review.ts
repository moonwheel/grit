import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, ManyToMany, OneToOne, RelationCount } from "typeorm";
import { Account } from "./account";
import { Product } from "./product";
import { ReviewVideo } from "./review_video";
import { ReviewPhoto } from "./review_photo";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { ProductReviewReplies } from "./product_review_reply";
import { Report } from "./report";

@Entity("t_product_review")
// @Index("product_review_product_id_idx", ["product",])
// @Index("product_review_user_id_idx", ["user",])
export class ProductReview {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.product_reviews, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Product, t_product => t_product.reviews, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product: Product | null;

    @Column("int", {
        nullable: true,
        name: "rating"
    })
    rating: number | null;

    @Column("varchar", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToOne(type => ReviewVideo)
    @JoinColumn()
    video: ReviewVideo;

    @OneToMany(type => ReviewPhoto, t_review_photo => t_review_photo.productReview)
    @JoinColumn()
    photo: ReviewPhoto[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @RelationCount((product_review: ProductReview) => product_review.likes)
    likesCount: number;

    @OneToMany(type => Mention, t_mentions => t_mentions.product_review)
    @JoinTable()
    mentions: Mention[];

    @OneToMany(type => ProductReviewReplies, t_product_review_replies => t_product_review_replies.review_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: ProductReviewReplies[];

    @OneToMany(type => Notification, t_notification => t_notification.product_review)
    notifications: Notification[];
}
