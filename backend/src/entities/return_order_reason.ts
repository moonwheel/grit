import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";

@Entity("t_return_order_reason")
@Unique(["name"])
export class ReturnOrderReason {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "name"
    })
    name: string;
}
