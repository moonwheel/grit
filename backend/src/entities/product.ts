import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, OneToOne, RelationCount, JoinTable } from "typeorm";
import { Order } from "./order";
import { Product_variant } from "./product_variant";
import { Product_photo } from "./product_photo";
import { Share } from "./share";
import { Shop_category } from "./shop_category";
import { View } from "./view";
import { Business } from "./business";
import { Account } from "./account";
import { ProductReview } from "./product_review";
import { Address } from "./address";
import { Hashtag } from "./hashtag";
import { Notification } from "./notification";
import { CartProduct } from "./cart_product";
import { Bookmark } from "./bookmarks";
import { Report } from "./report";
import { OrderProduct } from "./order_product";
import { Mention } from "./mention";

@Entity("t_product")
// @Index("product_user_id_idx", ["user",])
// @Index("product_business_id_idx", ["business_",])
// @Index("product_shopcategory_id_idx", ["shopcategory_",])
// @Index("product_address_id_idx", ["address_",])
@Index("product_bank_id_idx", ["bank_id",])
@Index('product-draft-deleted-idx', ['draft', 'deleted', 'active'])
export class Product {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Business, t_business => t_business.products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "business_id" })
    business_: Business | null;

    @ManyToOne(type => Shop_category, t_shop_category => t_shop_category.products, { onDelete: 'RESTRICT' })
    @JoinColumn({ name: "shopcategory_id" })
    shopcategory_: Shop_category | null;

    @ManyToOne(type => Address, t_address => t_address.products, { onDelete: 'SET NULL' })
    @JoinColumn({ name: "address_id" })
    address_: Address | null;

    @Index('product_type_idx')
    @Column("varchar", {
        nullable: true,
        name: "type"
    })
    type: string | null;

    @Index('product_title_idx')
    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Index('product_condition_idx')
    @Column("varchar", {
        nullable: true,
        name: "condition"
    })
    condition: string | null;

    @Index('product_price_idx')
    @Column("decimal", {
        nullable: true,
        name: "price"
    })
    price: number | null;

    @Column("int", {
        nullable: true,
        name: "quantity"
    })
    quantity: number | null;

    @Column("varchar", {
        nullable: true,
        name: "category"
    })
    category: string | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Index('product_variantOptions_idx')
    @Column("varchar", {
        nullable: true,
        name: "variantOptions"
    })
    variantOptions: string | null;

    @Column("varchar", {
        nullable: true,
        name: "deliveryOptions"
    })
    deliveryOptions: string | null;

    @Column("int", {
        nullable: true,
        name: "deliveryTime"
    })
    deliveryTime: number | null;

    @Column("int", {
        nullable: true,
        name: "shippingCosts"
    })
    shippingCosts: number | null;

    @Column("varchar", {
        nullable: true,
        name: "payerReturn"
    })
    payerReturn: string | null;

    @Column("int", {
        nullable: true,
        name: "bank_id"
    })
    bank_id: number | null;

    @Column("boolean", {
        nullable: false,
        name: "active",
        default: true
    })
    active: boolean | null;

    @Column("boolean", {
        nullable: true,
        name: "draft",
        default: false,
    })
    draft: boolean | null;


    @Index('produt_created_idx')
    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    // @OneToMany(type => Order, t_order => t_order.product_)
    // orders: Order[];

    // TODO: check proper onDelete strategy
    @OneToOne(type => Product_photo, t_product_photo => t_product_photo.cover_for_product_)
    @JoinColumn({ name: "cover_id" })
    cover_: Product_photo | null;

    // @Column("varchar", {
    //   nullable: true,
    //   name: "cheapestVariantCover"
    // })
    // cheapestVariantCover: string | null;
    // @Column("varchar", {
    //   nullable: true,
    //   name: "cheapestVariantPrice"
    // })
    // cheapestVariantPrice: string | null;

    @OneToMany(type => Product_photo, t_product_photo => t_product_photo.product_)
    photos: Product_photo[];

    @OneToMany(type => Product_variant, t_product_variant => t_product_variant.product_)
    variants: Product_variant[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => ProductReview, t_review => t_review.product)
    reviews: ProductReview[];

    @RelationCount((product: Product) => product.reviews)
    reviewsCount: number;

    @OneToMany(type => Share, t_share => t_share.product_)
    shares: Share[];

    @OneToMany(type => View, t_view => t_view.product_)
    views: View[];

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.product_)
    hashtags: Hashtag[];

    @OneToMany(type => CartProduct, t_cart_product => t_cart_product.product)
    cart_products: CartProduct[];

    @OneToMany(type => Bookmark, t_product_bookmarks => t_product_bookmarks.product)
    bookmarks: Bookmark;

    @OneToMany(type => OrderProduct, t_order_item => t_order_item.product)
    order_item_products: OrderProduct[];

    @RelationCount((product: Product) => product.bookmarks)
    bookmarksCount: number;

    @OneToMany(type => Mention, t_mentions => t_mentions.product)
    @JoinTable()
    mentions: Mention[];

}
