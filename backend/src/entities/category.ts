import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity("t_category")
@Unique(["name", "type"])
export class Category {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "name"
    })
    name: string;

    @Column("varchar", {
        nullable: true,
        name: "type"
    })
    type: string;
}
