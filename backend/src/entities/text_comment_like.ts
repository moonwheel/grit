import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Text_Comment } from "./text_comment";
import { Text_Comment_Replies } from "./text_comment_reply";
import { Account } from "./account";
import { Notification } from "./notification";
import { Text } from "./text";


@Entity("t_text_comment_like")
export class Text_Comment_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.text_comments_likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @ManyToOne(type => Text_Comment, t_text_comment => t_text_comment.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Text_Comment | null;


    @ManyToOne(type => Text_Comment_Replies, t_text_comment_reply => t_text_comment_reply.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "reply_id" })
    reply_: Text_Comment_Replies | null;

    @ManyToOne(type => Text, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    plain_text_: Text;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.text_comment_like)
    // notifications: Notification[];
}
