import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Text } from "./text";
import { Photo } from "./photo";
import { Video } from "./video";
import { Article } from "./article";
import { Product } from "./product";
import { Account } from "./account";


@Entity("t_share")
@Index("share_product_id_idx", ["product_",])
@Index("share_article_id_idx", ["article_",])
@Index("share_video_id_idx", ["video_",])
@Index("share_photo_id_idx", ["photo_",])
@Index("share_text_id_idx", ["text_",])
@Index("share_user_id_idx", ["user",])
export class Share {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.shares)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: true,
        name: "type"
    })
    type: string | null;

    @Column("int", {
        nullable: true,
        name: "total"
    })
    total: number | null;

    @ManyToOne(type => Text, t_text => t_text.shares, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    text_: Text | null;

    @ManyToOne(type => Photo, t_photo => t_photo.shares, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "photo_id" })
    photo_: Photo | null;

    @ManyToOne(type => Video, t_video => t_video.shares, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "video_id" })
    video_: Video | null;

    @ManyToOne(type => Article, t_article => t_article.shares, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;

    @ManyToOne(type => Product, t_product => t_product.shares, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product_: Product | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
