import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, JoinTable } from "typeorm";
import { Account } from "./account";
import { Article } from "./article";
import { Photo } from "./photo";
import { Text } from "./text";
import { Video } from "./video";
import { SellerReview } from "./seller_review";
import { SellerReviewReplies } from "./seller_review_reply";
import { ProductReview } from "./product_review";
import { ProductReviewReplies } from "./product_review_reply";
import { ServiceReview } from "./service_review";
import { ServiceReviewReplies } from "./service_review_reply";
import { Article_Comment } from "./article_comment";
import { Article_Comment_Replies } from "./article_comment_reply";
import { Photo_Comment } from "./photo_comment";
import { Photo_Comment_Replies } from "./photo_comment_reply";
import { Text_Comment } from "./text_comment";
import { Text_Comment_Replies } from "./text_comment_reply";
import { Video_Comment } from "./video_comment";
import { Video_Comment_Replies } from "./video_comment_reply";
import { Product } from "./product";
import { Service } from "./service";

@Entity("t_mentions")
export class Mention {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "mention_text"
    })
    mention_text: string;

    @ManyToOne(type => Account, t_account => t_account.mentioned, { onDelete: 'CASCADE' })
    @JoinTable()
    target: Account;

    @ManyToOne(type => Article, { onDelete: 'CASCADE' })
    @JoinColumn()
    article: Article | null;

    @ManyToOne(type => Photo, { onDelete: 'CASCADE' })
    @JoinColumn()
    photo: Photo | null;

    @ManyToOne(type => Text, { onDelete: 'CASCADE' })
    @JoinColumn()
    text: Text | null;

    @ManyToOne(type => Video, { onDelete: 'CASCADE' })
    @JoinColumn()
    video: Video | null;

    @ManyToOne(type => SellerReview, { onDelete: 'CASCADE' })
    @JoinColumn()
    sellerreview: SellerReview | null;

    @ManyToOne(type => Product, { onDelete: 'CASCADE' })
    @JoinColumn()
    product: Product | null;

    @ManyToOne(type => ProductReview, { onDelete: 'CASCADE' })
    @JoinColumn()
    product_review: ProductReview | null;

    @ManyToOne(type => ServiceReview, { onDelete: 'CASCADE' })
    @JoinColumn()
    service_review: ServiceReview | null;

    @ManyToOne(type => Service, { onDelete: 'CASCADE' })
    @JoinColumn()
    service: Service | null;

    @ManyToOne(type => Article_Comment, { onDelete: 'CASCADE' })
    @JoinColumn()
    article_comment: Article_Comment | null;

    @ManyToOne(type => Article_Comment_Replies, { onDelete: 'CASCADE' })
    @JoinColumn()
    article_comment_reply: Article_Comment_Replies | null;

    @ManyToOne(type => Photo_Comment, { onDelete: 'CASCADE' })
    @JoinColumn()
    photo_comment: Photo_Comment | null;

    @ManyToOne(type => Photo_Comment_Replies, { onDelete: 'CASCADE' })
    @JoinColumn()
    photo_comment_reply: Photo_Comment_Replies | null;

    @ManyToOne(type => Text_Comment, { onDelete: 'CASCADE' })
    @JoinColumn()
    text_comment: Text_Comment | null;

    @ManyToOne(type => Text_Comment_Replies, { onDelete: 'CASCADE' })
    @JoinColumn()
    text_comment_reply: Text_Comment_Replies | null;

    @ManyToOne(type => Video_Comment, { onDelete: 'CASCADE' })
    @JoinColumn()
    video_comment: Video_Comment | null;

    @ManyToOne(type => Video_Comment_Replies, { onDelete: 'CASCADE' })
    @JoinColumn()
    video_comment_reply: Video_Comment_Replies | null;

    @ManyToOne(type => ProductReviewReplies, { onDelete: 'CASCADE' })
    @JoinColumn()
    product_review_reply: ProductReviewReplies | null;

    @ManyToOne(type => ServiceReviewReplies, { onDelete: 'CASCADE' })
    @JoinColumn()
    service_review_reply: ServiceReviewReplies | null;

    @ManyToOne(type => SellerReviewReplies, { onDelete: 'CASCADE' })
    @JoinColumn()
    seller_review_reply: SellerReviewReplies | null;

    @CreateDateColumn({type: "timestamp"})
    created: Date;
}
