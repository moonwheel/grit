import { Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Share } from "./share";
import { Hashtag } from "./hashtag";
import { Photo_Like } from "./photo_like";
import { Photo_Comment } from "./photo_comment";
import { Account } from "./account";
import { Mention } from "./mention";
import { Photo_Annotation } from "./photo_annotation";
import { Notification } from "./notification";
import { Bookmark } from "./bookmarks";
import { Photo_View } from "./photo_view";
import { Report } from "./report";
import { Address } from "./address";


@Entity("t_photo")
@Index('photo-draft-deleted-idx', ['draft', 'deleted'])
export class Photo {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.photos, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @Column("varchar", {
        nullable: true,
        name: "photo"
    })
    photo: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;


    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;


    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;


    @Column("varchar", {
        nullable: true,
        name: "category"
    })
    category: string | null;


    @Column("varchar", {
        nullable: true,
        name: "location"
    })
    location: string | null;

    @ManyToOne(type => Address, addr => addr.photo_location)
    @JoinColumn({ name: "business_address" })
    business_address: Address;


    @Column("bool", {
        nullable: false,
        name: "draft",
        default: () => false,
    })
    draft: boolean | null;



    @Column("varchar", {
        nullable: true,
        name: "filterName"
    })
    filterName: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;


    @OneToMany(type => Photo_Comment, t_photo_comment => t_photo_comment.photo_)
    comments: Photo_Comment[];

    @OneToMany(type => Photo_Like, t_photo_like => t_photo_like.photo_)
    likes: Photo_Like[];

    @OneToMany(type => Photo_View, t_photo_view => t_photo_view.photo_)
    views: Photo_View[];

    @OneToMany(type => Bookmark, t_photo_bookmarks => t_photo_bookmarks.photo)
    bookmarks: Bookmark[];

    @RelationCount((photo: Photo) => photo.likes)
    likesCount: number;

    @RelationCount((photo: Photo) => photo.bookmarks)
    bookmarksCount: number;

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Share, t_share => t_share.photo_)
    shares: Share[];

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.photo_)
    hashtags: Hashtag[];

    // @ManyToMany(type => Account, t_user => t_user.views_photo, { onDelete: 'CASCADE' })
    // @JoinTable()
    // views: Account[];

    @RelationCount((photo: Photo) => photo.views)
    viewsCount: number;

    @OneToMany(type => Mention, t_mentions => t_mentions.photo)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((photo: Photo) => photo.comments)
    commentsCount: number;

    @OneToMany(type => Photo_Annotation, t_photo_annotation => t_photo_annotation.photo)
    annotations: Photo_Annotation[];

}
