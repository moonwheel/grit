import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Service_schedule } from "./service_schedule";
import { Day } from "./day";
import { Interval } from "./interval";

@Entity("t_schedule_days")
export class ScheduleByDays {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Service_schedule, t_service => t_service.byDays, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    schedule: Service_schedule | null;

    @Column("time", {
        nullable: true,
        name: "from"
    })
    from: string | null;

    @Column("time", {
        nullable: true,
        name: "to"
    })
    to: string | null;

    @ManyToOne(type => Day, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    day: Day | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Interval, t_schedule_days_intervals => t_schedule_days_intervals.scheduleByDay, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    intervals: Interval[];

}
