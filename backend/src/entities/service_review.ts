import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, ManyToMany, OneToOne, RelationCount } from "typeorm";
import { Account } from "./account";
import { Service } from "./service";
import { ReviewVideo } from "./review_video";
import { ReviewPhoto } from "./review_photo";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Report } from "./report";
import { ServiceReviewReplies } from "./service_review_reply";

@Entity("t_service_review")
@Index("service_review_service_id_idx", ["service",])
@Index("service_review_user_id_idx", ["user",])
export class ServiceReview {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.service_reviews)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Service, t_service => t_service.reviews)
    @JoinColumn({ name: "service_id" })
    service: Service | null;

    @Column("int", {
        nullable: true,
        name: "rating"
    })
    rating: number | null;

    @Column("varchar", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToOne(type => ReviewVideo)
    @JoinColumn()
    video: ReviewVideo;

    @OneToMany(type => ReviewPhoto, t_review_photo => t_review_photo.serviceReview)
    @JoinColumn()
    photo: ReviewPhoto[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @RelationCount((service_review: ServiceReview) => service_review.likes)
    likesCount: number;

    @OneToMany(type => Mention, t_mentions => t_mentions.service_review)
    @JoinTable()
    mentions: Mention[];

    @OneToMany(type => ServiceReviewReplies, t_service_review_replies => t_service_review_replies.review_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: ServiceReviewReplies[];

    @OneToMany(type => Notification, t_notification => t_notification.service_review)
    notifications: Notification[];
}
