import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  BeforeUpdate,
  JoinTable,
  RelationCount,
  ManyToMany,
} from "typeorm";

import { Account } from "./account";
import { Mention } from "./mention";
import { SellerReview } from "./seller_review";


@Entity("t_seller_review_replies")
export class SellerReviewReplies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => SellerReview, t_seller_review => t_seller_review.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "review_id" })
    review_: SellerReview;

    @ManyToOne(type => Account, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "seller_id" })
    seller_: Account;

    @ManyToOne(type => Account, t_user => t_user.seller_reviews_replies, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @OneToMany(type => Mention, t_mentions => t_mentions.seller_review_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((seller_replies: SellerReviewReplies) => seller_replies.likes)
    likesCount: number;
}
