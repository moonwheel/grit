import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate, Index, OneToMany} from "typeorm";
import {Order, PaymentStatus} from "./order";
import {Product} from "./product";
import {Product_variant} from "./product_variant";
import {Account} from "./account";
import { AccountRole } from "../enums";

type PaymentType = "bank" | "card";
export type Status = "shipped" | "collected" | "canceled";

@Entity("t_order_product")
@Index("order_id_idx", ["order",])
@Index("product_id_idx", ["product",])
@Index("product_variant_id_idx", ["product_variant",])
export class OrderProduct {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("int", {
        nullable: false,
        name: "quantity"
    })
    quantity: number;

    @Column("boolean", {
        nullable: true,
        name: "collect"
    })
    collect: boolean | null;

    @Column("decimal", {
        nullable: false,
        default: () => 0,
        name: "price"
    })
    price: number;

    @Column("decimal", {
        nullable: false,
        default: () => 0,
        name: "shipping_price"
    })
    shipping_price: number;

    @Column("varchar", {
        nullable: true,
        name: "payment_id"
    })
    payment: string | null;

    @Column("varchar", {
        nullable: true,
        name: "reason"
    })
    reason: string | null;

    @ManyToOne(type => Order, t_order => t_order.order_items, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "order_id" })
    order: Order | null;

    @ManyToOne(type => Product, t_product => t_product.order_item_products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product: Product | null;

    @ManyToOne(type => Product_variant, t_product_variant => t_product_variant.order_items, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_variant_id" })
    product_variant: Product_variant | null;

    @Column("varchar", {
        nullable: true,
        name: "who_cancelled"
    })
    who_cancelled: AccountRole | null;

    @Column("varchar", {
        nullable: true,
        name: "when_cancelled"
    })
    when_cancelled: 'before' | 'after' | null;

    @Column("varchar", {
        nullable: true,
        length: 255,
        name: "cancel_reason"
    })
    cancel_reason: string;

    @Column("varchar", {
        nullable: true,
        length: 500,
        name: "cancel_description"
    })
    cancel_description: string;

    @Column("varchar", {
        nullable: true,
        length: 255,
        name: "return_reason"
    })
    return_reason: string;

    @Column("varchar", {
        nullable: true,
        length: 500,
        name: "return_description"
    })
    return_description: string;

    @Column("decimal", {
        nullable: true,
        name: "return_shipping_cost",
    })
    return_shipping_cost: number;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "shipped"
    })
    shipped: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "collected"
    })
    collected: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "canceled"
    })
    cancelled: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "returned"
    })
    returned: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "received"
    })
    received: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "refunded"
    })
    refunded: Date;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @Column("int", {
        nullable: true,
        name: "transfer_id",
    })
    transfer_id: number;

    @Column("varchar", {
        nullable: true,
        name: "transfer_status"
    })
    transfer_status: "success" | "failed" | null;

    @Column("timestamp", {
        nullable: true,
        name: "transfer_date"
    })
    transfer_date: Date | null;

    @Column("int", {
        nullable: true,
        name: "pay_in_id",
    })
    pay_in_id: number;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "reviewed"
    })
    reviewed: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }
}
