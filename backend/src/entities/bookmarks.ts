import { Column, Entity, PrimaryGeneratedColumn, Index, ManyToOne, JoinColumn, Unique } from "typeorm";
import { Account } from "./account";
import { Text } from "./text";
import { Photo } from "./photo";
import { Video } from "./video";
import { Article } from "./article";
import { Product } from "./product";
import { Service } from "./service";

export type BookmarkContentType =
"text" |
"photo" |
"video" |
"article" |
"product" |
"service";

@Entity("t_bookmarks")
@Unique(["owner", "account", "text", "photo", "video", "article", "product", "service"])
@Index("bookmarks_created_desc_idx", { synchronize: false })
export class Bookmark {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    // @ManyToOne(type => Account, t_user => t_user.bookmark_account, { onDelete: 'CASCADE', eager: true })
    // @JoinColumn()
    // account: Account | null;

    @ManyToOne(type => Account, t_user => t_user.bookmark_owner, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "owner_id" })
    owner: Account | null;

    @ManyToOne(type => Account, t_user => t_user.bookmark_account, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "account_id" })
    account: Account;

    @ManyToOne(type => Text, t_text => t_text.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "text_id" })
    text: Text;

    @ManyToOne(type => Photo, t_photo => t_photo.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "photo_id" })
    photo: Photo | null;

    @ManyToOne(type => Video, t_viceo => t_viceo.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "video_id" })
    video: Video | null;

    @ManyToOne(type => Article, t_article => t_article.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "article_id" })
    article: Article | null;

    @ManyToOne(type => Product, t_product => t_product.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "product_id" })
    product: Product;

    @ManyToOne(type => Service, t_service => t_service.bookmarks, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "service_id" })
    service: Service;

    @Column("timestamp", {
      nullable: false,
      default: () => "CURRENT_TIMESTAMP",
      name: "created"
    })
    created: Date;
}
