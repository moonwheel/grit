import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, RelationCount } from "typeorm";
import { Text } from "./text";
import { Text_Comment_Like } from "./text_comment_like";
import { Text_Comment_Replies } from "./text_comment_reply";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Report } from "./report";

@Entity("t_text_comment")
@Index("text_comment_user_id_idx", ["user",])
export class Text_Comment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @ManyToOne(type => Account, t_user => t_user.text_comments, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Text, t_text => t_text.comments, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    plain_text_: Text | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Text_Comment_Like, t_text_comment_like => t_text_comment_like.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Text_Comment_Like[];

    @OneToMany(type => Text_Comment_Replies, t_text_comment_replies => t_text_comment_replies.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: Text_Comment_Replies[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.text_comment)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((t_text_comment: Text_Comment) => t_text_comment.likes)
    likesCount: number;

    @RelationCount((t_text_comment: Text_Comment) => t_text_comment.replies)
    repliesCount: number;

    // @OneToMany(type => Notification, t_notification => t_notification.text_comment)
    // notifications: Notification[];
}
