import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Service_schedule } from "./service_schedule";

@Entity("t_schedule_date")
export class ScheduleByDate {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Service_schedule, t_service => t_service.byDate, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    schedule: Service_schedule | null;

    @Column("timestamp", {
        nullable: true,
        name: "from"
    })
    from: string | null;

    @Column("timestamp", {
        nullable: true,
        name: "to"
    })
    to: string | null;

    @Column("int", {
        nullable: true,
        name: "bookings"
    })
    bookings: number;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }
}
