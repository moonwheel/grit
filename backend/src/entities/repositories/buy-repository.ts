import {EntityRepository, Repository, getCustomRepository, IsNull, getRepository, getConnection} from "typeorm";
import {CartProduct} from "../cart_product";

@EntityRepository(CartProduct)
export class CartProductRepository extends Repository<CartProduct> {

    public async getCartProducts(userId: number): Promise<[CartProduct[], number]> {
        try {
            const query = this.createQueryBuilder(`cart_product`)
                .leftJoinAndSelect(`cart_product.product`, `product`)
                .leftJoin(`product.user`, `product_user`)
                .addSelect(`product_user.id`)
                .leftJoinAndSelect(`cart_product.product_variant`, `product_variant`)
                .leftJoin(`product.address_`, `product_address`)
                .addSelect(`product_address.id`)
                .where(`cart_product.user_id = :userId`, { userId })
                .andWhere(`product.deleted IS NULL`)
                .orderBy(`cart_product.id`, `ASC`)

            return query.getManyAndCount()
        } catch (error) {
            console.log('getCartProducts error', error)
            throw error;
        }
    }

    public async deleteProductsFromCart(userId: number): Promise<void> {
        await this.createQueryBuilder()
            .delete()
            .from(CartProduct)
            .where("user_id = :userId", { userId })
            .execute()
    }
}

export default function getCartProductsRepository() {
    return getCustomRepository(CartProductRepository);
}
