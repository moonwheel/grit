import { EntityRepository, Repository, getCustomRepository, getRepository } from "typeorm";
import { Mention } from "../mention";
import { Text } from "../../entities/text";
import { Photo } from "../../entities/photo";
import { Video } from "../../entities/video";
import { Article } from "../../entities/article";

import { NotificationController } from "../../services/notifications/controller";
import { NotificationContentDisplayName, NotificationContentType } from "../notification";
import { AbstractController } from "../../utils/abstract/abstract.controller";
import { Photo_Comment } from "../photo_comment";
import { Text_Comment } from "../text_comment";
import { Video_Comment } from "../video_comment";
import { Article_Comment } from "../article_comment";
import { Photo_Comment_Replies } from "../photo_comment_reply";
import { Text_Comment_Replies } from "../text_comment_reply";
import { Video_Comment_Replies } from "../video_comment_reply";
import { Article_Comment_Replies } from "../article_comment_reply";
import getAccountRepository from "./account-repository";
import { Account } from "../account";

export type MentionRepositoryEntityKey = (
    "photo"
    | "text"
    | "article"
    | "video"
    | "sellerreview"
    | "product_review"
    | "service_review"
    | "article_comment"
    | "article_comment_reply"
    | "photo_comment"
    | "photo_comment_reply"
    | "text_comment"
    | "text_comment_reply"
    | "video_comment"
    | "video_comment_reply"
    | string

);

@EntityRepository(Mention)
export class MentionsRepository extends Repository<Mention> {

    async addMention(
        entity: MentionRepositoryEntityKey,
        entityData: any,
        mentions: Array<any> = [],
        contentType: string,
        issuer: Account,
        commentData? : Photo_Comment | Text_Comment | Video_Comment | Article_Comment | Photo_Comment_Replies | Text_Comment_Replies | Video_Comment_Replies | Article_Comment_Replies
    ) {
        try {

            const oldMentions = await this.getMentionByContent(entity, entityData, contentType, commentData)

            console.log('oldMentions', oldMentions)
            const toRemoveMap: Map<number, Mention> = new Map(oldMentions.map(item => ([item.id, item])));
            const result: Mention[] = [];

            for (const mention of mentions) {
                if (!mention.id) {
                    const newMention = new Mention();

                    newMention.target = mention.target.id;
                    newMention.mention_text = mention.mention_text;
                    newMention[entity] = commentData ? commentData.id : null;
                    newMention[contentType] = entityData.id

                    const savedM = await this.save(newMention);
                    const owner = await getAccountRepository().getSmallAccountInfo({ key: 'id', val: mention.target.id })
                    const newEntity = entity.includes("comment") ? "comment" : entity

                    const newNotifi = await NotificationController.addNotification(
                        'mentioned',
                        newEntity as NotificationContentDisplayName ,
                        contentType as NotificationContentType,
                        entityData,
                        issuer, // issuer
                        owner // Owner
                    );

                    const query = this.createQueryBuilder('mention')
                        .leftJoinAndSelect('mention.target', 'mention_target')
                        .leftJoinAndSelect('mention_target.person', 'mention_target_person')
                        .addSelect("mention_target_person.id")
                        .addSelect("mention_target_person.photo")
                        .addSelect("mention_target_person.fullName")
                        .addSelect("mention_target_person.pageName")
                        .leftJoinAndSelect('mention_target.business', 'mention_target_business')
                        .addSelect("mention_target_business.id")
                        .addSelect("mention_target_business.photo")
                        .addSelect("mention_target_business.name")
                        .addSelect("mention_target_business.pageName")
                        .whereInIds(savedM.id)

                    // console.log('query.getQuery()', query.getQuery());
                    const found = await query.getOne();

                    result.push(found);
                } else {
                    toRemoveMap.delete(mention.id);
                    result.push(mention);
                }
            }


            const toRemove = Array.from(toRemoveMap.values());

            await this.remove(toRemove);

            return result;
        } catch (error) {
            console.log('addMention error',error )
            throw error;
        }
    }

    async getMentionByContent(
        entity: MentionRepositoryEntityKey,
        entityData: any,
        contentType: string,
        commentData? : Photo_Comment | Text_Comment | Video_Comment | Article_Comment | Photo_Comment_Replies | Text_Comment_Replies | Video_Comment_Replies | Article_Comment_Replies
    ) {
        let query = this.createQueryBuilder('mention')

        if (entity !== contentType) {
            query = query
                .addSelect('mention.id')
                .leftJoin(`mention.${contentType}`, `mention_content_type`)
                .addSelect(`mention_content_type.id`)
                .leftJoin(`mention.${entity}`, `mention_entity`)
                .addSelect(`mention_entity.id`)
                .where(`mention_content_type.id = ${entityData.id}`)
                .andWhere(
                    commentData
                    ? `mention_entity.id = ${commentData.id}`
                    : `mention_entity.id IS NULL`
                );
        } else {
            query = query
                .addSelect('mention.id')
                .leftJoin(`mention.${contentType}`, `mention_content_type`)

            switch (contentType) {
                case 'product':
                case 'service':
                    query = query
                        .leftJoin(`mention.${contentType}_review`, `mention_content_type_review`)
                        .leftJoin(`mention.${contentType}_review_reply`, `mention_content_type_review_reply`)
                        .addSelect(`mention_content_type_review.id`)
                        .addSelect(`mention_content_type_review_reply.id`)
                        .andWhere(`mention_content_type_review.id IS NULL`)
                        .andWhere(`mention_content_type_review_reply.id IS NULL`);
                    break;

                case 'product_review':
                case 'service_review':
                case 'sellerreview':
                    query = query
                        .leftJoin(`mention.seller_review_reply`, `mention_content_type_reply`)
                        .addSelect(`mention_content_type_reply.id`)
                        .andWhere(`mention_content_type_reply.id IS NULL`)
                    break;

                default:
                    query = query
                        .leftJoin(`mention.${contentType}_comment`, `mention_content_type_comment`)
                        .leftJoin(`mention.${contentType}_comment_reply`, `mention_content_type_comment_reply`)
                        .addSelect(`mention_content_type_comment.id`)
                        .addSelect(`mention_content_type_comment_reply.id`)
                        .andWhere(`mention_content_type_comment.id IS NULL`)
                        .andWhere(`mention_content_type_comment_reply.id IS NULL`);
                    break;
            }
        }

        const mentions = await query.getMany();

        // console.log('mentions', mentions);
        // console.log('query.getQuery()', query.getQuery());

        return mentions;
    }
}

export default function getMentionRepository() {
    return getCustomRepository(MentionsRepository);
}
