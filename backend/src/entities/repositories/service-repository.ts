import { Account } from './../account';
const dateTime = require("date-time");
import {EntityRepository, Repository, getCustomRepository, getRepository, IsNull} from "typeorm";
import { Service } from "../service";
import { ServicePhoto } from "../service_photo";
import { Service_schedule } from "../service_schedule";
import { ScheduleByDate } from "../schedule_by_date";
import { ScheduleByDays } from "../schedule_by_days";
import { DateTime, Duration, Interval as TimeInterval } from "luxon";
import { Interval } from "../interval";
import { isEmpty } from "lodash";
import { PhotoController } from "../../services/photo/controller";


@EntityRepository(Service)
export class ServiceRepository extends Repository<Service> {
    async createService(
        service: Service,
        photosLength: number,
        schedule: Service_schedule,
        byDate: Array<ScheduleByDate>,
        byDays: Array<ScheduleByDays>,
        user: Account
    ){
        const savedService = await this.save(service);

        const photoRepo = getRepository(ServicePhoto);
        const byDateRepo = getRepository(ScheduleByDate);
        const byDaysRepo = getRepository(ScheduleByDays);

        for (let index = 0; index < photosLength; index++) {
            const photo = new ServicePhoto();
            photo.index = index;
            photo.service_ = savedService;
            await photoRepo.save(photo);
        }

        schedule.service_ = savedService;
        const savedSchedule = await getRepository(Service_schedule).save(schedule);

        for (const element of byDate) {
            element.schedule = savedSchedule;
            await byDateRepo.save(element);
        }
        for (const element of byDays) {
            element.schedule = savedSchedule;
            await byDaysRepo.save(element);
        }
        if (!service.draft) await this.generateServiceScheduleIntervals(savedService.id, user);

        return await this.getOneService(service.id, user, user);
    }

    async getOneService(
        id: number,
        user: number | Account,
        currentUser: number | Account
    ) {

        const where: any = { id };

        console.log('user', user)
        if (user) where.user = user;
        return await this.createQueryBuilder("service")
            .leftJoinAndMapOne(`service.bookmarked`, `service.bookmarks`, `bookmarks`, `bookmarks.owner_id = ${currentUser}`)

            .leftJoinAndSelect("service.schedule", "schedule")
            .leftJoinAndSelect("schedule.byDate", "byDate")
            .leftJoinAndSelect("schedule.byDays", "byDays")
            .leftJoinAndSelect("byDays.intervals", "intervals")
            .leftJoinAndSelect("byDays.day", "day")
            .leftJoinAndSelect("service.photos", "photos")
            .leftJoinAndSelect("service.user", "user")
            .leftJoinAndSelect("service.address", "address")
            .leftJoin("user.business", "business")
            .addSelect("business.id")
            .addSelect("business.thumb")
            .addSelect("business.photo")
            .addSelect("business.name")
            .addSelect("business.pageName")
            .leftJoin("user.person", "person")
            .addSelect("person.id")
            .addSelect("person.thumb")
            .addSelect("person.photo")
            .addSelect("person.fullName")
            .addSelect("person.pageName")
            .leftJoinAndSelect("service.mentions", "service_mention")
            .leftJoinAndSelect("service_mention.target", "service_mention_target")
            .leftJoinAndSelect("service_mention_target.business", "service_mention_target_business")
            .addSelect("service_mention_target_business.id")
            .addSelect("service_mention_target_business.photo")
            .addSelect("service_mention_target_business.name")
            .addSelect("service_mention_target_business.pageName")
            .leftJoinAndSelect("service_mention_target.person", "service_mention_target_person")
            .addSelect("service_mention_target_person.id")
            .addSelect("service_mention_target_person.photo")
            .addSelect("service_mention_target_person.fullName")
            .addSelect("service_mention_target_person.pageName")
            // .where(` service.id = :id
            //     ${where.user ? "AND service.user_id = :user" : ""}
            // `, where)
            .where(" service.id = :id", where)
            .andWhere("service.deleted IS NULL")
            .andWhere("service.active = :active", { active: true })
            .getOne();
    }

    async updateService(
        id: number,
        service: any = {},
        schedule: any = {},
        byDate: Array<any> = null,
        byDays: Array<any> = null,
        photos: Array<any> = [],
        user: number | Account
    ) {
        try {
            if (!isEmpty(service)) await this.update(id, service);

            const byDateRepo = getRepository(ScheduleByDate);
            const byDaysRepo = getRepository(ScheduleByDays);
            const serviceSchedule = await getRepository(Service_schedule).findOne({
                where: {
                    service_: id
                },
                relations: ["byDate", "byDays"]
            });

            if (byDate && !byDate.length) {
                for (const item of serviceSchedule.byDate) {
                    byDateRepo.remove(item)
                }
            }

            if (byDays && !byDays.length) {
                for (const item of serviceSchedule.byDays) {
                    byDaysRepo.remove(item)
                }
            }

            if (!isEmpty(schedule)) {
                if (!isEmpty(byDate)) {
                    schedule.duration = null;
                    schedule.break = null;
                }
                await getRepository(Service_schedule).update(serviceSchedule.id, schedule)
            };

            if (!isEmpty(byDate)) {
                for (let index = 0; index < byDate.length; index++) {
                    const element = byDate[index];
                    if (element.id && serviceSchedule.byDate.some(date => date.id == element.id)) {
                        if (Object.keys(element).length > 1) {
                            await byDateRepo.update(element.id, element);
                        } else {
                            await byDateRepo.delete(element.id);
                        }
                    } else if(Object.keys(element).length) {
                        const scheduleByDate = new ScheduleByDate();
                        if (element.from) scheduleByDate.from = element.from;
                        if (element.to) scheduleByDate.to = element.to;
                        scheduleByDate.schedule = serviceSchedule;
                        await byDateRepo.save(scheduleByDate);
                    }
                }
            } else if (!isEmpty(byDays)){
                for (let index = 0; index < byDays.length; index++) {
                    const element = byDays[index];
                    if (element.id && serviceSchedule.byDays.some(day => day.id == element.id)) {
                        if (Object.keys(element).length > 1) {
                            await byDaysRepo.update(element.id, element);
                        } else {
                            await byDaysRepo.delete(element.id);
                        }
                    } else if(!element.id && Object.keys(element).length) {
                        const dayTime = new ScheduleByDays();
                        if (element.from) dayTime.from = element.from;
                        if (element.to) dayTime.to = element.to;
                        if (element.day) dayTime.day = element.day;
                        dayTime.schedule = serviceSchedule;
                        await byDaysRepo.save(dayTime);
                    }
                }
            }

            if (!isEmpty(photos)) {
                const servicePhotoRepo = getRepository(ServicePhoto);
                const serviceInDB = await this.findOne(id);

                const existingPhotos = await servicePhotoRepo
                    .createQueryBuilder("photo")
                    .where("photo.service_id = :id", {id})
                    .getMany();

                for (const existingPhoto of existingPhotos) {
                    const found = photos.find(item => item.id === existingPhoto.id);
                    if (!found) {
                        await servicePhotoRepo.delete(existingPhoto.id);
                    }
                }

                for (const photo of photos) {
                    if (photo.id) {
                        await servicePhotoRepo.update(photo.id, { index: photo.index });
                    } else {
                        const newPhoto = new ServicePhoto();
                        newPhoto.index = photo.index;
                        newPhoto.service_ = serviceInDB;
                        await servicePhotoRepo.save(newPhoto);
                    }
                }
            }

            return await this.getOneService(id, user, user);
        } catch (error) {
            throw error;
        }
    }

    async generateServiceScheduleIntervals(
        serviceID: number,
        user: Account | number
    ) {
        try {
            const {
                schedule: {
                    id,
                    byDays
                },
                schedule,
            } = await this.getOneService(serviceID, user, user);

            if(schedule.duration && schedule.break) {

              const durationObject = DateTime.fromFormat(schedule.duration, "HH:mm").toObject();
              const breakObject = DateTime.fromFormat(schedule.break, "HH:mm").toObject();
              const duration = Duration.fromObject({
                  hour: durationObject.hour,
                  minute: durationObject.minute,
              });
              const breakDuration = Duration.fromObject({
                  hour: breakObject.hour,
                  minute: breakObject.minute,
              });
              const repeatTime = duration.plus(breakDuration);

              for (let index = 0; index < byDays.length; index++) {
                  const element = byDays[index];
                  const existingIds = element.intervals.map(interval => interval.id);
                  if (existingIds.length) {
                      await getRepository(Interval).delete(existingIds);
                  }
                  const intervals = await TimeInterval.fromDateTimes(DateTime.fromISO(element.from), DateTime.fromISO(element.to)).splitBy(repeatTime);
                  const last = intervals[intervals.length - 1];
                  const lastDuration = Duration.fromObject({
                      hour: last.end.hour - last.start.hour,
                      minute: last.end.minute - last.start.minute,
                  });
                  const diff = lastDuration.minus(duration);
                  if (diff.minutes < 0 || diff.hours < 0) await intervals.pop();
                  for (let index = 0; index < intervals.length; index++) {
                      const time = intervals[index].set({end: intervals[index].start.plus(duration)});
                      const interval = new Interval();
                      interval.scheduleByDay = element;
                      interval.from = time.start.toSQLTime();
                      interval.to = time.end.toSQLTime();

                      await getRepository(Interval).save(interval);
                  }
              }
            }
            return this;
        } catch (error) {
            throw error;
        }

    }

    async deleteService(id) {
        try {
            return this.update(id, {deleted: dateTime()});
        } catch (error) {
            throw error;
        }
    }

    async getServiceById(id: number) {
        return this.createQueryBuilder("service")
            .leftJoin("service.user", "user")
            .addSelect("user.id")
            .where(" service.id = :id", { id })
            .andWhere("service.deleted IS NULL")
            .andWhere("service.active = :active", { active: true })
            .getOne();
    }

}

export default function getServiceRepository() {
    return getCustomRepository(ServiceRepository);
}
