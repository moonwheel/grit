import { EntityRepository, Repository, getCustomRepository, Brackets } from "typeorm";
import { Account } from "../account";
import { PageParams } from "../../interfaces/global/page.interface";
import { Followings } from "../followings";

@EntityRepository(Followings)
export class FollowingRepository extends Repository<Followings> {
    async follow(
        target: Account,
        account: any
    ){
        try {
            console.log('target', target)
                await this.save({
                    account: account.baseBusinessAccount || account,
                    followedAccount: target.baseBusinessAccount || target,
                    created: new Date()
                });
            return;
        } catch (error) {
            console.log('save followings error', error)
            throw error;
        }
    }

    async unfollow(
        target: any,
        account: any
    ){
        try {
            await this.delete({
              account: account.baseBusinessAccount || account,
              followedAccount: target.baseBusinessAccount || target,
            })
            return;
        } catch (error) {
            throw error;
        }
    }

    async getFollowings(
        account: any,
        options: {
            page?: number|string;
            limit?: number|string;
            search?: string;
        } = {
            page: null,
            limit: null,
            search: null,
        }
    ){
        // console.log('getFollowings')
        try {
            const pageParams = this.getPageParams(options.page, options.limit);
            // console.log('followings', account)

            const followingsQuery = this.createQueryBuilder("followings")
                // .leftJoin('followings.account', "account")
                // .addSelect("account.id")
                .leftJoin('followings.followedAccount', "followed")
                .addSelect("followed.id")
                .leftJoin("followed.business", "business")
                .addSelect("business.id")
                .addSelect("business.photo")
                .addSelect("business.thumb")
                .addSelect("business.name")
                .addSelect("business.pageName")

                .leftJoin("followed.person", "person")
                .addSelect("person.id")
                .addSelect("person.photo")
                .addSelect("person.thumb")
                .addSelect("person.fullName")
                .addSelect("person.pageName")
                .where(
                    `followings.account = :account ${
                        options.search && options.search.trim()
                        ? `AND (${this.getSearchQuery(options.search, ["person.fullName", "person.pageName", "business.name", "business.pageName"])})`
                        : ""
                    }`, { account: account.baseBusinessAccount || account })
                .addOrderBy('business.name', 'ASC')
                .addOrderBy('person.fullName', 'ASC')
                .offset(pageParams.offset)
                .limit(pageParams.limit)

            // console.log('followingsQuery.getSql()', followingsQuery.getSql())
            const followings = await followingsQuery
                .getMany();

            return followings ? followings : [];
        } catch (error) {
            console.log('error', error)
            throw error;
        }
    }

    /**
     * Other version of previous method.
     * @param targetUserId Here we search in all accounts and select only accounts
     * which are following account with given
     * @param currentUserId also we extend every found account with property 'following'
     * which means that our current account following found one
     */
    async getFollowers2(
        targetUserId: number,
        currentUserId: number,
        options: {
            page?: number|string;
            limit?: number|string;
            search?: string;
        } = {
            page: null,
            limit: null,
            search: null,
        }
    ){
        console.log('getFollowers2', targetUserId)
        console.log('getFollowers2', currentUserId)
        try {
            const pageParams = this.getPageParams(options.page, options.limit);

            const followersQuery = this.createQueryBuilder("followings")
                .leftJoin('followings.account', "follower")
                .addSelect("follower.id")
                .leftJoin("follower.business", "business")
                .addSelect("business.id")
                .addSelect("business.photo")
                .addSelect("business.thumb")
                .addSelect("business.name")
                .addSelect("business.pageName")

                // detect if current user follows somebody's follower (need for "following/follow" button on the UI)
                .leftJoinAndMapOne(`follower.following`, `follower.followers`, `account_followers`, `account_followers.account = ${currentUserId}`)

                .leftJoin("follower.person", "person")
                .addSelect("person.id")
                .addSelect("person.photo")
                .addSelect("person.thumb")
                .addSelect("person.fullName")
                .addSelect("person.pageName")
                .where(
                    `followings.followedAccount = :targetUserId ${
                        options.search && options.search.trim()
                        ? `AND (${this.getSearchQuery(options.search, ["person.fullName", "person.pageName", "business.name", "business.pageName"])})`
                        : ""
                    }`, { targetUserId })
                .offset(pageParams.offset)
                .limit(pageParams.limit)

            console.log('followersQuery.getSql()', followersQuery.getSql())
            const followers = await followersQuery
                .getMany();

            return followers ? followers : [];
        } catch (error) {
            throw error;
        }
    }

    async isAccountFollowsTarget(account: Account, target: number) {
        try {
            const alreadyFollows = await this.createQueryBuilder("followings")
                .where("followings.followedAccount = :target", {
                    target
                })
                .andWhere("followings.account = :account", {
                    account
                })
                .getOne();
            return alreadyFollows;

        } catch (error) {
            throw error;
        }
    }

    private getPageParams(
        page: number|string,
        limit: number|string
    ) {
        const params: PageParams = {
            limit: 15,
            offset: 0
        };
        if(limit) params.limit = Number(limit);
        if(page) params.offset = (Number(page) - 1) * params.limit;

        return params;
    }

    private getSearchQuery(
        searchString: string,
        fields: Array<string>,
    ) {
        const searchArray = searchString.split(" ");
        const searchQuery = fields.reduce((query, field, fieldIndx) => {
            let fieldSearch = query;
            searchArray.forEach((searchPart, indx) => {
                fieldSearch += `
                    LOWER(${field}) SIMILAR TO '%${searchPart.trim().toLocaleLowerCase()}%'
                    ${(fieldIndx == (fields.length - 1) && indx == (searchArray.length - 1)) ? "" : "OR"}`;
            });
            return fieldSearch;
        }, "");
        return searchQuery;
    }
}

export default function getFollowingRepository() {
    return getCustomRepository(FollowingRepository);
}
