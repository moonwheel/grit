import { Permissions } from './../../interfaces/global/permissions.interface';
import { Role } from './../role';
import {EntityRepository, Repository, getCustomRepository, IsNull, getRepository, getConnection} from "typeorm";
import { Account } from "../account";
import { camelCase } from "lodash";
import { PageParams } from "../../interfaces/global/page.interface";
import { AccountToken } from "../../interfaces/global/account-token.interface";
import {CartProduct} from "../cart_product";

@EntityRepository(Account)
export class AccountRepository extends Repository<Account> {
    async createUserAccount(person) {
        try {
            const account = new Account();
            account.person = person;
            return await this.save(account);
        } catch (error) {
            console.log('error', error)
            throw error;
        }
    }
    async createBusinessAccount(person, role, business, baseBusinessAccount) {
        try {
            const account = new Account();
            account.person = person;
            account.role = role;
            account.business = business;

            if (baseBusinessAccount) {
                account.baseBusinessAccount = baseBusinessAccount
            }
            return await this.save(account);
        } catch (error) {
            throw error;
        }
    }
    async getAccountForToken(account: { id?: number; person?: number; business?: number }): Promise<AccountToken> {
        try {
                let query = this.createQueryBuilder("account")
                    .addSelect('account.id')
                    .leftJoin("account.baseBusinessAccount", "base_busines_account")
                    .addSelect('base_busines_account.id')
                    .leftJoin("account.person", "person")
                    .addSelect('person.id')
                    .addSelect('person.deactivated')
                    .addSelect('person.reactivationtoken')
                    .addSelect('person.wallet')
                    .addSelect('person.mangoUserId')
                    .leftJoin("account.business", "business")
                    .addSelect('business.id')
                    .addSelect('business.wallet')
                    .addSelect('business.mangoUserId')
                    .leftJoin("account.role", "role")
                    .addSelect('role.id')
                    .where('account.deleted is NULL');


                    if (account.id) query = query.andWhere('account.id = :accountId', {accountId: account.id});
                    if (account.person) query = query.andWhere('account.person = :person', {person: account.person});
                    if (account.business) query = query.andWhere('account.business = :business', {business: account.business});

                // query = query.cache(60000);

                const user = await query.getOne();

                if (user) {
                    return {
                        id: user.id,
                        person: user.person && user.person.id,
                        business: user.business && user.business.id,
                        role: user.role && user.role.id,
                        baseBusinessAccount: user.baseBusinessAccount ? user.baseBusinessAccount.id : null,
                        deactivated: user.person.deactivated,
                        wallet: user.business ? user.business.wallet : user.person.wallet,
                        mangoUserId: user.business ? user.business.mangoUserId : user.person.mangoUserId,
                        reactivationtoken: user.person.reactivationtoken
                    };
                } else {
                    throw Error("Account Not found. Maybe it was deleted");
                }
            // } else {
            //     throw Error("Not enough data for search");
            // }
        } catch (error) {
            console.log('getAccountForToken error', error)
            throw error;
        }
    }

    public async getFullAccountInfo({key, val}: {key: keyof Account, val: number | string}): Promise<Account> {
        try {
            const query = this.createQueryBuilder("account")
                .leftJoinAndSelect("account.baseBusinessAccount", "base_busines_account")
                .leftJoinAndSelect("account.person", "person")
                .leftJoinAndSelect("person.delivery_address", "person_delivery_address")
                .leftJoinAndSelect("account.business", "business")
                .leftJoinAndSelect("account.role", "role")
                .loadRelationCountAndMap('account.followersCount', 'account.followers')
                .loadRelationCountAndMap('account.followingsCount', 'account.followings')

                .leftJoin("account.followings", "followings")
                .addSelect("followings.id")
                .loadRelationCountAndMap(`account.followingRequestCount`, `account.input_following_requests`,
                    "frc", qb => qb.andWhere("frc.status = :status", { status: 'pending' })
                    )
                .leftJoin("followings.followedAccount", "followedAccounts")
                .addSelect("followedAccounts.id")
                .where(`account.${key} = ${val}`)

                // console.log('query.getSql()', query.getSql())

                const responseUser: any = await query.getOne()
                return responseUser;

        } catch (error) {
            console.log('getFullAccountInfo error', error)
            throw error;
        }
    }

    public async getCountOfCardProducts(userId: number) {
        try {
            const productsCount = await getRepository(CartProduct)
                .createQueryBuilder("cart_product")
                .where("cart_product.user_id = :userId", { userId })
                .getCount()

            return productsCount;
        } catch (error) {
            console.log('getCountOfCardProducts error', error)
            throw error;
        }
    }

    public async getSmallAccountInfo({key, val}: {key: keyof Account, val: number | string}): Promise<Account> {
        try {
            const query = this.createQueryBuilder("account")
                .addSelect("account.id")
                .leftJoin('account.person', "account_person")
                .addSelect("account_person.id")
                .addSelect("account_person.thumb")
                .addSelect("account_person.fullName")
                .addSelect("account_person.pageName")
                .leftJoin("account.business", "account_business")
                .addSelect("account_business.id")
                .addSelect("account_business.thumb")
                .addSelect("account_business.name")
                .addSelect("account_business.pageName")
                .where(`account.${key} = ${val}`)

            // console.log('query.getSql()', query.getSql())

            const responseUser: any = await query.getOne()
            return responseUser;

        } catch (error) {
            console.log('getSmallAccountInfo error', error)
            throw error;
        }
    }

    public async getAccountNotificationSettings({key, val}: {key: keyof Account, val: number | string}): Promise<Account> {
      try {
          const query = this.createQueryBuilder("account")
              .addSelect("account.id")
              .addSelect("account.likes_notification")
              .addSelect("account.comments_notification")
              .addSelect("account.tags_notification")
              .addSelect("account.followers_notification")
              .where(`account.${key} = ${val}`)

          // console.log('query.getSql()', query.getSql())

          const responseUser: any = await query.getOne()
          return responseUser;

      } catch (error) {
          console.log('getSmallAccountInfo error', error)
          throw error;
      }
  }


    public async updateAccount({key, val}: {key: keyof Account, val: number | string}, data: Account): Promise<Account> {
        try {
            await this.update({
                [key]: val
            }, {
                ...data,
                updated: new Date()
            })

            return this.findOne({
                [key]: val
            })
        } catch (error) {
            console.log('updateAccount error', error)
            throw error;
        }

    }

    public async getAccountsOfThePerson(val: number | string): Promise<Account[]> {
        const query = this.createQueryBuilder("account")
            .leftJoinAndSelect("account.baseBusinessAccount", "base_busines_account")
            .leftJoinAndSelect("account.person", "person")
            .leftJoinAndSelect("account.business", "business")
            .leftJoinAndSelect("account.role", "role")
            .where(`account.person = ${val}`)

        // console.log('query.getSql()', query.getSql())

        const response = await query.getMany()
        return response;
    }

    async  checkUniquePageName(name: string) {
        try {
            const pageName = name.replace(" ", "").toLowerCase();

            console.log('pageName', pageName)

            const results = await getConnection().query(`
                SELECT "pageName" from t_person person WHERE LOWER(person."pageName") SIMILAR TO '%${pageName}%'
                UNION
                SELECT "pageName" from t_business business WHERE LOWER(business."pageName") SIMILAR TO '%${pageName}%'
            `)
            interface Results {
                pageName: string
            }

            const generateUniqueName = (results: Results[], pageName: string) => {
                const exactResult = results.find(r => r.pageName === pageName)

                if(!exactResult) return { pageName, unique: true }

                const joinedResults = results.map(r => `${r.pageName}`).join(', ')
                const regexp = new RegExp(`(${pageName})(\\d{1,})`, 'g')
                let exec = regexp.exec(joinedResults)
                var indexes = []
                while (exec != null) {
                  // console.log('exec', exec)
                  indexes.push(+exec[2])
                  exec = regexp.exec(joinedResults)
                }
                const sortedIndexes = indexes.sort((a, b) => a - b)
                const newIndex = +sortedIndexes[sortedIndexes.length - 1] + 1 || 1
                return {
                    pageName: pageName + newIndex,
                    unique: false
                }
            }
            const generatedNameObj = generateUniqueName(results, pageName)
            console.log('generatedNameObj', generatedNameObj)
            return generatedNameObj
        } catch (error) {
            throw error;
        }
    }

    public async searchAccount(
        value: string,
        currentUserId: number,
        options?: {
            page: number | string;
            limit: number | string;
            except?: string // should be parsed to array
        }
    ) {
        try {
            const pageParams = this.getPageParams(options.page, options.limit);
            const except = options.except && JSON.parse(options.except);

            console.log('except', except)

            if (!value) {
                return [];
            }

            let request = this.createQueryBuilder("account")
                .leftJoin("account.person", "account_person")
                .addSelect("account_person.photo")
                .addSelect("account_person.fullName")
                .addSelect("account_person.pageName")
                .addSelect("account_person.email")
                .leftJoin("account.business", "account_business")
                .addSelect("account_business.id")
                .addSelect("account_business.photo")
                .addSelect("account_business.name")
                .addSelect("account_business.pageName")
                .leftJoinAndMapOne(`account.following`, `account.followers`, `account_followers`, `account_followers.account = ${currentUserId}`)
                .leftJoinAndMapOne(`account.followingRequest`, `account.input_following_requests`, `input_following_requests`, `input_following_requests.account = ${currentUserId}`)
                .where("account.deleted IS NULL")
                .andWhere(`
                    (
                        account.business IS NOT NULL
                        AND (
                            LOWER(account_business.pageName) SIMILAR TO '%${value.toLowerCase()}%'
                            OR LOWER(account_business.name) SIMILAR TO '%${value.toLowerCase()}%'
                        )
                        AND account.baseBusinessAccount IS NULL
                    )
                    OR (
                        account.business IS NULL
                        AND (
                            LOWER(account_person.pageName) SIMILAR TO '%${value.toLowerCase()}%'
                            OR LOWER(account_person.fullName) SIMILAR TO '%${value.toLowerCase()}%'
                        )
                    )
                `)
                .offset(pageParams.offset)
                .limit(pageParams.limit)

            if (except) {
                for (const id of except) {
                    request = request.andWhere(`account.id != ${id}`)
                }
            }

            console.log('request.getSql()', request.getSql())

            const result = await request.getMany() as any[];

            console.log('result', result)

            return result.filter(item => !item.followingRequest);
        } catch (error) {
            throw error;
        }
    }

    private getPageParams(
        page: number | string,
        limit: number | string
    ) {
        const params: PageParams = {
            limit: 15,
            offset: 0
        };
        if (limit) {
            params.limit = Number(limit);
        }
        if (page) {
            params.offset = (Number(page) - 1) * params.limit;
        }

        return params;
    }

    public async getAccountByUsername(value: string, currentUserId: number) {
        try {
            if (value) {
                const query = this.createQueryBuilder("account")
                    .leftJoinAndSelect("account.person", "account_person")
                    .leftJoinAndSelect("account.business", "account_business")
                    .loadRelationCountAndMap('account.followingsCount', 'account.followings')
                    .loadRelationCountAndMap('account.followersCount', 'account.followers')

                    .leftJoinAndMapOne(`account.following`, `account.followers`, `account_followers`, `account_followers.account = ${currentUserId}`)
                    .leftJoinAndMapOne(`account.followingRequest`, `account.input_following_requests`, `input_following_requests`, `input_following_requests.account = ${currentUserId}`)

                     // YOU_BLOCK - current user added the account to blacklist
                    .leftJoinAndMapOne(`account.isBlocked`, 'account.blocked', `account_blocked`, `account_blocked."ownerId" = ${currentUserId}`)

                     // YOU_ARE_BLOCKED - current user in the blacklist of account
                    .leftJoinAndMapOne(`account.isBlocking`, 'account.blacklist', `account_blacklist`, `account_blacklist."blockedAccountId" = ${currentUserId}`)
                    .where("account.deleted IS NULL")
                    .andWhere(`
                        (
                            (
                                account.business IS NULL
                                AND LOWER(account_person.pageName) = :value
                            )
                            OR (
                              LOWER(account_business.pageName) = :value
                              AND account.baseBusinessAccount IS NULL
                            )
                        )
                    `, { value })
                    // .groupBy(`
                    //   account_followings_list.id,
                    //   account_followers_list.id,
                    //   account.id,
                    //   account_person.id,
                    //   account_business.id,
                    //   account_followers.id,
                    //   input_following_requests.id,
                    //   account_blocked.id,
                    //   account_blacklist.id
                    // `)

                    console.log('query.getSql()', query.getSql())

                    const result: any = await query.getOne();
                // convert isBlocked value to boolean
                result.isBlocked = !!result.isBlocked;
                result.isBlocking = !!result.isBlocking;
                return result;
            } else {
                return null;
            }
        } catch (error) {
            throw error;
        }
    }

    public async checkPermissionsForBusiness(token: AccountToken): Promise<Permissions> {
        try {
            const permissions = {
                canRead: false,
                canWrite: false,
                canManage: false
            };

            let query = this.createQueryBuilder("account")
                .addSelect('account.id')
                .leftJoin("account.role", "role")
                .addSelect('role.id')
                .addSelect('role.type')
                .where('account.id = :accountId', {accountId: token.id});

            const account = await query.getOne();

            // console.log('account', account)
            if (account.role && account.role.type === 'admin') {
                permissions.canRead = true;
                permissions.canWrite = true;
                permissions.canManage = true;
            } else if (account.role && account.role.type === 'creator') {
                permissions.canRead = true;
                permissions.canWrite = true;
                permissions.canManage = false;
            } else if (account.role && account.role.type === 'viewer') {
                permissions.canRead = true;
                permissions.canWrite = false;
                permissions.canManage = false;
            }


            // console.log('checkPermissionsForBusiness permissions', permissions)
            return permissions;
        } catch (error) {
            throw error;
        }
    }
}

export default function getAccountRepository() {
    return getCustomRepository(AccountRepository);
}
