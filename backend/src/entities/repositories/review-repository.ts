import { EntityRepository, getCustomRepository, EntityManager, getRepository } from "typeorm";
import { isEmpty } from "lodash";
import { ReviewInterface } from "../../interfaces/review/review.interface";
import { ProductReview } from "../product_review";
import { ServiceReview } from "../service_review";
import { Filter } from "../../interfaces/global/filter.interface";
import { PageParams } from "../../interfaces/global/page.interface";
import { PhotoController } from "../../services/photo/controller";
import { ReviewPhoto } from "../review_photo";
import { ReviewVideo } from "../review_video";
import { SellerReview } from "../seller_review";
import { ProductReviewReplies } from "../product_review_reply";
import { ServiceReviewReplies } from "../service_review_reply";
import { SellerReviewReplies } from "../seller_review_reply";
import getOrderRepository from "./order-repository";
import { OrderProduct } from "../order_product";

@EntityRepository()
export class ReviewRepository {

    constructor(private manager: EntityManager) {}

    async addReview(
        entity: string,
        id: number|string,
        account: any,
        reviewData: ReviewInterface
    ) {
        try {
            const Review = this.getEntity(entity);
            const review = new Review();
            review.rating = reviewData.rating;
            review.text = reviewData.text || null;
            review.user =  account.id;
            review[entity] = id;

            const savedReview = await this.manager.save(review);

            const photos = await this.saveReviewPhotos(reviewData.photos, account);

            for (let index = 0; index < photos.length; index++) {
                const photo = photos[index];
                await this.manager
                    .createQueryBuilder()
                    .relation(Review, "photo")
                    .of(savedReview)
                    .add(photo);
            }

            if (reviewData.itemId) {
                await getRepository(OrderProduct).update(reviewData.itemId, {
                    reviewed: new Date(),
                });
            }

            return this.getById(entity, savedReview.id, account);
        } catch (error) {
            throw error;
        }
    }

    async addReply(
        entity: string,
        account: any,
        reviewData: ReviewInterface
    ) {
        const Review = this.getEntity(entity);
        const Reply = this.getReplyEntity(entity);

        const review = await this.manager
            .getRepository(Review)
            .createQueryBuilder()
            .where("id = :id", { id: reviewData.replyTo })
            .getOne();

        if (!review) {
            throw Error("Review does not exist");
        }

        const reply = new Reply();
        reply.user = account.id;
        reply.review_ = reviewData.replyTo as any;
        reply.text = reviewData.text;

        const saved = await this.manager.getRepository(Reply).save(reply);

        return this.getReplyById(entity, saved.id);
    }

    async getById(
        entity: string,
        id: number|string,
        account: any
    ) {
        try {
            const Review = this.getEntity(entity);

            const review: any = await this.manager
                .getRepository(Review)
                .createQueryBuilder("review")
                .leftJoin("review.user", "review_user")
                .addSelect("review_user.id")
                .leftJoin("review_user.business", "review_user_business")
                .addSelect("review_user_business.id")
                .addSelect("review_user_business.photo")
                .addSelect("review_user_business.name")
                .addSelect("review_user_business.pageName")
                .leftJoin("review_user.person", "review_user_person")
                .addSelect("review_user_person.id")
                .addSelect("review_user_person.photo")
                .addSelect("review_user_person.fullName")
                .addSelect("review_user_person.pageName")
                .leftJoinAndSelect("review.mentions", "review_mention", `review_mention.${entity}_review_reply IS NULL`)
                .leftJoinAndSelect("review_mention.target", "review_mention_target")
                .leftJoin("review_mention_target.business", "review_mention_target_business")
                .addSelect("review_mention_target_business.id")
                .addSelect("review_mention_target_business.photo")
                .addSelect("review_mention_target_business.name")
                .addSelect("review_mention_target_business.pageName")
                .leftJoin("review_mention_target.person", "review_mention_target_person")
                .addSelect("review_mention_target_person.id")
                .addSelect("review_mention_target_person.photo")
                .addSelect("review_mention_target_person.fullName")
                .addSelect("review_mention_target_person.pageName")
                .leftJoin("review.photo", "review_photo")
                .addSelect("review_photo.id")
                .addSelect("review_photo.link")
                .leftJoin("review.video", "review_video")
                .addSelect("review_video.id")
                .addSelect("review_video.link")
                .where("review.id = :id", { id })
                .getOne();

            if (!review) {
                throw Error("Review not found");
            }

            const liked = await this.manager
                .getRepository(Review)
                .createQueryBuilder("review")
                .leftJoin("review.likes", "likes")
                .addSelect("likes.id")
                .where("review.id = :id", { id })
                .andWhere("likes.id = :account", { account: account.id})
                .getOne();

            review.liked = liked ? true : false;

            return review;
        } catch (error) {
            throw error;
        }
    }

    async getReplyById(
        entity: string,
        id: number|string,
    ) {
        try {
            const Reply = this.getReplyEntity(entity);

            const reply: any = await this.manager
                .getRepository(Reply)
                .createQueryBuilder("reply")
                .leftJoin('reply.review_', 'reply_review')
                .addSelect('reply_review.id')
                .leftJoinAndSelect("reply_review.user", "reply_review_user")
                .leftJoinAndSelect("reply_review_user.business", "reply_review_user_business")
                .leftJoinAndSelect("reply_review_user.person", "reply_review_user_person")
                .leftJoinAndSelect("reply.user", "reply_user")
                .leftJoinAndSelect("reply_user.business", "reply_user_business")
                .leftJoinAndSelect("reply_user.person", "reply_user_person")
                .where("reply.id = :id", { id })
                .getOne();

            if (!reply) {
                throw Error("reply not found");
            }

            return reply;
        } catch (error) {
            throw error;
        }
    }

    async getAll(
        entity: string,
        id: number | string = null,
        account: any,
        options: {
            page: number|string;
            limit: number|string;
            orderBy: string;
            order: string;
            rating: string;
            filter: string;
        }
    ) {
        try {
            const where: any = {};
            if (id) {
                where.id = id;
            }

            const pageParams = this.getPageParams(options.page, options.limit);
            const filter = this.getFilterParams(options.orderBy, options.order);

            const Review = this.getEntity(entity);

            const query: any = await this.manager
                .getRepository(Review)
                .createQueryBuilder("review")
                .leftJoinAndSelect("review.user", "review_user")
                .leftJoinAndSelect("review_user.person", "review_person")
                .leftJoinAndSelect("review_user.business", "review_business")
                .leftJoinAndMapOne(
                    `review_user.following`,
                    `review_user.followers`,
                    `review_user_followers`,
                    `review_user_followers.account = ${account.baseBusinessAccount || account.id}`,
                )
                .leftJoinAndSelect("review.mentions", "review_mention", `review_mention.${entity}_review_reply IS NULL`)
                .leftJoinAndSelect("review_mention.target", "review_mention_target")
                .leftJoinAndSelect("review_mention_target.business", "review_mention_target_business")
                .leftJoinAndSelect("review_mention_target.person", "review_mention_target_person")
                .leftJoinAndSelect("review.photo", "review_photo")
                .leftJoinAndSelect("review.video", "review_video")
                .leftJoinAndSelect("review.replies", "review_replies")
                .leftJoinAndSelect("review_replies.user", "review_replies_user")
                .leftJoinAndSelect("review_replies_user.business", "review_replies_user_business")
                .leftJoinAndSelect("review_replies_user.person", "review_replies_user_person")
                .leftJoinAndSelect(`review.${entity}`, `review_${entity}`)
                .leftJoinAndSelect(`review_${entity}.photos`, `review_${entity}_photos`)
                .where(`review.${entity} = :id`, where)

            if (options.rating && options.rating !== 'all') {
                query.andWhere('review.rating = :rating', { rating: options.rating });
            }

            if (filter) {
                query.orderBy( filter.by, filter.order)
            }

            query.orderBy(`review.user_id = ${account.id}`, 'DESC')
                .offset(pageParams.offset)
                .limit(pageParams.limit)

            const reviews: any = await query.getManyAndCount();

            for (let i = 0; i < reviews[0].length; i++) {
                const liked = await this.manager
                    .getRepository(Review)
                    .createQueryBuilder("review")
                    .leftJoinAndSelect("review.likes", "likes")
                    .addSelect("likes.id")
                    .where("review.id = :id", { id: reviews[0][i].id })
                    .andWhere("likes.id = :account", { account: account.id})
                    .getOne();

                reviews[0][i].liked = (liked) ? true : false;
            }

            const ratingsTotal = await this.manager
                .getRepository(Review)
                .createQueryBuilder('review')
                .select('rating')
                .addSelect('COUNT(*)', 'count')
                .where(`review.${entity} = :id`, { id })
                .groupBy('rating')
                .orderBy('rating')
                .getRawMany();

            const reviewed = await this.manager
                .getRepository(Review)
                .createQueryBuilder('review')
                .leftJoinAndSelect("review.user", "review_user")
                .where(`review.${entity} = :id`, { id })
                .andWhere(`review.user = :account`, { account: account.id })
                .getOne();

            return {
                items: reviews[0],
                total: ratingsTotal.reduce((sum, item) => sum + Number(item.count), 0),
                ratingsTotal,
                reviewed,
            };
        } catch (error) {
            throw error;
        }
    }

    async getReplies(
        entity: string,
        id: number | string = null,
        account: any,
    ) {
        try {
            const Reply = this.getReplyEntity(entity);
            const replies: any = await this.manager
                .getRepository(Reply)
                .createQueryBuilder("reply")
                .leftJoinAndSelect("reply.user", "reply_user")
                .leftJoinAndSelect("reply_user.business", "reply_user_business")
                .leftJoinAndSelect("reply_user.person", "reply_user_person")

                .leftJoinAndSelect("reply.mentions", "reply_mention")
                .leftJoinAndSelect("reply_mention.target", "reply_mention_target")
                .leftJoin("reply_mention_target.business", "reply_mention_target_business")
                .addSelect("reply_mention_target_business.id")
                .addSelect("reply_mention_target_business.photo")
                .addSelect("reply_mention_target_business.name")
                .addSelect("reply_mention_target_business.pageName")
                .leftJoin("reply_mention_target.person", "reply_mention_target_person")
                .addSelect("reply_mention_target_person.id")
                .addSelect("reply_mention_target_person.photo")
                .addSelect("reply_mention_target_person.fullName")
                .addSelect("reply_mention_target_person.pageName")

                .where(`reply.review_id = :id`, { id })
                .getManyAndCount();

            for (let i = 0; i < replies[0].length; i++) {
                const liked = await this.manager
                    .getRepository(Reply)
                    .createQueryBuilder("reply")
                    .leftJoinAndSelect("reply.likes", "likes")
                    .addSelect("likes.id")
                    .where("reply.id = :id", { id: replies[0][i].id })
                    .andWhere("likes.id = :account", { account: account.id})
                    .getOne();

                replies[0][i].liked = (liked) ? true : false;
            }

            return {
                items: replies[0],
                total: replies[1],
            };
        } catch (error) {
            throw error;
        }
    }

    async getRating(
        entity: string,
        entityId: number | string,
    ) {
        const Review = this.getEntity(entity);

        const data = await this.manager
            .getRepository(Review)
            .createQueryBuilder('review')
            .select('AVG(review.rating)', 'rating')
            .where(`review.${entity}_id = :entity`, { entity: entityId })
            .getRawOne();

        return data.rating;
    }

    async getRatingCount(
        entity: string,
        entityId: number | string,
    ) {
        const Review = this.getEntity(entity);

        const data = await this.manager
            .getRepository(Review)
            .createQueryBuilder('review')
            .select('COUNT(*)', 'count')
            .where(`review.${entity}_id = :entity`, { entity: entityId })
            .getRawOne();

        return data.count;
    }

    async checkOwnership(
        entity: string,
        id: number|string,
        account: any
    ) {
        try {
            const Rewiew = this.getEntity(entity);
            const review = await this.manager
                .getRepository(Rewiew)
                .createQueryBuilder("review")
                .where("review.id = :id", { id })
                .andWhere("review.user = :account", { account: account.id })
                .getOne();
            if (review) return true;
            else throw Error("Access denied");
        } catch (error) {
            throw error;
        }
    }

    async checkReplyOwnership(
        entity: string,
        id: number|string,
        account: any
    ) {
        try {
            const Reply = this.getReplyEntity(entity);
            const reply = await this.manager
                .getRepository(Reply)
                .createQueryBuilder("reply")
                .where("reply.id = :id", { id })
                .andWhere("reply.user = :account", { account: account.id })
                .getOne();
            if (reply) {
                return true;
            } else {
                throw Error("Access denied");
            }
        } catch (error) {
            throw error;
        }
    }

    async updateReview(
        entity: string,
        id: number|string,
        account: any,
        reviewData: ReviewInterface,
    ) {
        try {
            const Review = this.getEntity(entity);
            if (await this.checkOwnership(entity, id, account)) {
                const newData: any = {};
                if (reviewData.rating) newData.rating = reviewData.rating;
                if (reviewData.text) newData.text = reviewData.text;

                if(reviewData.photos && reviewData.photos.length) {
                    const [review] = await this.getById(entity, id, account);
                    const photoCount = review.photo && review.photo.length || 0;
                    const photos = await this.saveReviewPhotos(reviewData.photos, account, 6 - photoCount);
                    for (let index = 0; index < photos.length; index++) {
                        const photo = photos[index];
                        await this.manager
                            .createQueryBuilder()
                            .relation(Review, "photo")
                            .of(id)
                            .add(photo);
                    }
                }
                if (!isEmpty(newData)) await this.manager.getRepository(Review).update(id, newData);
                return await this.getById(entity, id, account);
            } else {
                throw Error("Access denied");
            }
        } catch (error) {
            throw error;
        }
    }

    async updateReviewReply(
        entity: string,
        id: number|string,
        account: any,
        reviewData: ReviewInterface,
    ) {
        try {
            const Reply = this.getReplyEntity(entity);
            if (await this.checkReplyOwnership(entity, id, account)) {
                const newData: any = {};

                if (reviewData.text) {
                    newData.text = reviewData.text;
                }

                if (!isEmpty(newData)) {
                    await this.manager.getRepository(Reply).update(id, newData);
                }

                return await this.getReplyById(entity, id);
            } else {
                throw Error("Access denied");
            }
        } catch (error) {
            throw error;
        }
    }

    async removeReviewReply(
        entity: string,
        id: number|string,
        account: any
    ) {
        try {
            const Reply = this.getEntity(entity);
            if (await this.checkReplyOwnership(entity, id, account)){
                await this.manager.getRepository(Reply).delete(id);
            }
            return;
        } catch (error) {
            throw error;
        }
    }

    async removeReview(
        entity: string,
        id: number|string,
        account: any
    ) {
        try {
            const Review = this.getEntity(entity);
            if (await this.checkOwnership(entity, id, account)){
                await this.manager.getRepository(Review).delete(id);
            }
            return;
        } catch (error) {
            throw error;
        }
    }

    async likeReview(
        entity: string,
        id: number|string,
        account: any,
    ){
        try {
            const Review = this.getEntity(entity);

            let liked = true;

            await this.manager
                .createQueryBuilder()
                .relation(Review, "likes")
                .of(id)
                .add(account.id)
                .catch(async (error: Error) => {
                    if (error.message.includes("duplicate key value")){
                        await this.manager
                            .createQueryBuilder()
                            .relation(Review, "likes")
                            .of(id)
                            .remove(account.id);

                        liked = false;
                    } else {
                        throw error;
                    }
                });

            return liked;
        } catch (error) {
            throw error;
        }
    }

    async likeReviewReply(entity: string, id: number | string, account: any) {
        try {
            const Reply = this.getEntity(entity);

            let liked = true;

            const res = await this.manager
                .createQueryBuilder()
                .relation(Reply, "likes")
                .of(id)
                .add(account.id)
                .catch(async (error: Error) => {
                    if (error.message.includes("duplicate key value")){
                        await this.manager
                            .createQueryBuilder()
                            .relation(Reply, "likes")
                            .of(id)
                            .remove(account.id);

                        liked = false;
                    } else {
                        throw error;
                    }
                });

            console.log('liked: ', liked);

            return liked;
        } catch (error) {
            throw error;
        }
    }

    async getLikes(
        entity: string,
        id: number|string,
        options: {
            page: number|string;
            limit: number|string;
        }
    ){
        try {
            const Review = this.getEntity(entity);
            const pageParams = this.getPageParams(options.page, options.limit);

            const likedBy = await this.manager
                .getRepository(Review)
                .createQueryBuilder("review")
                .leftJoinAndSelect("review.likes", "user")
                .leftJoin("user.business", "user_business")
                .addSelect("user_business.id")
                .addSelect("user_business.photo")
                .addSelect("user_business.name")
                .addSelect("user_business.pageName")
                .leftJoin("user.person", "user_person")
                .addSelect("user_person.id")
                .addSelect("user_person.photo")
                .addSelect("user_person.fullName")
                .addSelect("user_person.pageName")
                .where("review.id = :id", { id })
                .offset(pageParams.offset)
                .limit(pageParams.limit)
                .getOne()
                .then(review => (review && review.likes) ? review.likes.map(likedBy => likedBy) : []);

            return likedBy;
        } catch (error) {
            throw error;
        }
    }

    async getReplyLikes(entity: string, id: number | string) {
        try {
            const Reply = this.getReplyEntity(entity);
            return this.manager
                .getRepository(Reply)
                .createQueryBuilder("reply")
                .leftJoinAndSelect("reply.likes", "user")
                .leftJoin("user.business", "user_business")
                .addSelect("user_business.id")
                .addSelect("user_business.photo")
                .addSelect("user_business.name")
                .addSelect("user_business.pageName")
                .leftJoin("user.person", "user_person")
                .addSelect("user_person.id")
                .addSelect("user_person.photo")
                .addSelect("user_person.fullName")
                .addSelect("user_person.pageName")
                .where("reply.id = :id", { id })
                .getOne()
                .then(reply => (reply && reply.likes) ? reply.likes.map(likedBy => likedBy) : []);
        } catch (error) {
            throw error;
        }
    }

    public getEntity(entity: string) {
        switch (entity) {
            case "product":
                return ProductReview;
            case "service":
                return ServiceReview;
            case "seller":
                return SellerReview;
            default:
                throw Error("Invalid entity name");
        }
    }

    public getReplyEntity(entity: string) {
        switch (entity) {
            case "product":
                return ProductReviewReplies;
            case "service":
                return ServiceReviewReplies;
            case "seller":
                return SellerReviewReplies;
            default:
                throw Error("Invalid entity name");
        }
    }

    private getFilterParams(
        filterBy: string,
        order: string
    ) {
        const filter: Filter = {
            by: "review.created",
            order: "DESC",
        };
        if(filterBy === "date") filter.by ="review.created";

        if(order.toLocaleUpperCase() === "ASC" ) filter.order = "ASC";
        return filter;
    }

    private getPageParams(
        page: number|string,
        limit: number|string
    ) {
        const params: PageParams  ={
            limit: 15,
            offset: 0
        };
        if(limit) params.limit = Number(limit);
        if(page) params.offset = (Number(page) - 1) * params.limit;

        return params;
    }

    private async saveReviewPhotos(
        photos: Array<any> = [],
        account: any,
        limit = 6,
    ) {
        try {
            const saved = [];
            for (let index = 0; index < photos.length && limit > 0; index++) {
                const reviewPhoto = new ReviewPhoto();
                const { photo } = await PhotoController.base64ToFile(photos[index]);
                reviewPhoto.link = photo;
                reviewPhoto.user = account.id;
                saved.push(await this.manager.getRepository(ReviewPhoto).save(reviewPhoto));
                limit--;
            }
            return saved;
        } catch (error) {
            throw error;
        }
    }

    async removeReviewPhoto(
        id: number|string,
        account: any
    ) {
        try {
            const reviewPhoto = await this.manager
                .getRepository(ReviewPhoto)
                .findOne({
                    where: {
                        id,
                        user: account.id
                    }
                });

            if (reviewPhoto){
                await this.manager.getRepository(ReviewPhoto).delete(reviewPhoto.id);
            }
            return reviewPhoto;
        } catch (error) {
            throw error;
        }
    }

    async saveReviewVideos(
        entity: string,
        id: number|string,
        video: ReviewVideo,
        account: any,
    ) {
        try {
            const Review = this.getEntity(entity);
            const savedVideo = await this.manager.getRepository(ReviewVideo).save(video);
            await this.manager.getRepository(Review).update(id, { video: savedVideo });
            return await this.getById(entity, id, account);
        } catch (error) {
            throw error;
        }
    }

    async removeReviewVideo(
        id: number|string,
        account: any
    ) {
        try {
            const reviewVideo = await this.manager
                .getRepository(ReviewVideo)
                .findOne({
                    where: {
                        id,
                        user: account.id
                    }
                });

            if (reviewVideo){
                await this.manager.getRepository(ReviewVideo).delete(reviewVideo.id);
            }
            return;
        } catch (error) {
            throw error;
        }
    }

}

export default function getReviewRepository() {
    return getCustomRepository(ReviewRepository);
}
