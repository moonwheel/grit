import { EntityRepository, Repository, getCustomRepository } from "typeorm";
import { Hashtag } from "../hashtag";

export type HashtagRepositoryTypeKey = (
    "photo"
    | "text"
    | "article"
    | "video"
    | "product"
    | "service"
);

@EntityRepository(Hashtag)
export class HashtagsRepository extends Repository<Hashtag> {

    async addHashtag(
      postType: HashtagRepositoryTypeKey,
      postId: number,
      tags: string[]
    ) {
      try {
          const hashtagsToInsert = []

          for (const tag of tags) {
            hashtagsToInsert.push({
              tag,
              [`${postType}_`]: postId
            })
          }
          await this.save(hashtagsToInsert);
          return;
      } catch (error) {
          console.log('addHashtag error', error)
          throw error;
      }
    }


    async updateHashtagsByPostId(
      postType: HashtagRepositoryTypeKey,
      postId: number,
      tags: string[]
    ) {
      try {

          const hashtagsInCurrentPost = await this.find({
            [postType + '_']: postId
          })
          await this.remove(hashtagsInCurrentPost)

          const hashtagsToInsert = []
          for (const tag of tags) {
            hashtagsToInsert.push({
              tag,
              [`${postType}_`]: postId
            })
          }
          await this.save(hashtagsToInsert);
          return;
      } catch (error) {
          throw error;
      }
    }

}

export default function getHashtagRepository() {
    return getCustomRepository(HashtagsRepository);
}
