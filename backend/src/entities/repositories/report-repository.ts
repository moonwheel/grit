import {
    EntityRepository,
    getCustomRepository,
    EntityManager,
    getRepository,
    getConnection,
} from "typeorm";
import { isEmpty } from "lodash";

import { ReportInterface } from "../../interfaces/report/report.interface";
import { PageParams } from "../../interfaces/global/page.interface";

import { Account } from "../account";
import { Article } from "../article";
import { Photo } from "../photo";
import { Product } from "../product";
import { Service } from "../service";
import { Text } from "../text";
import { Video } from "../video";
import { SellerReview } from "../seller_review";
import { ProductReview } from "../product_review";
import { ServiceReview } from "../service_review";
import { Article_Comment } from "../article_comment";
import { Article_Comment_Replies } from "../article_comment_reply";
import { Photo_Comment } from "../photo_comment";
import { Photo_Comment_Replies } from "../photo_comment_reply";
import { Text_Comment } from "../text_comment";
import { Text_Comment_Replies } from "../text_comment_reply";
import { Video_Comment } from "../video_comment";
import { Video_Comment_Replies } from "../video_comment_reply";
import { Helpers } from "../../utils/helpers";
import { ReportType } from "../report_type";
import { Report } from "../report";
import { ReportCategory } from "../report_category";

import { ReportController } from "../../services/report/controller";

interface Filter {
    page: number | string;
    limit: number | string;
    search: string | null;
    status?: string | null;
    type?: number | null;
    category?: number | null;
}

@EntityRepository()
export class ReportRepository {

    constructor(private manager: EntityManager) {}

    public async addNewReport(
        targetId: number,
        account: any,
        reportData: any
    ) {
        try {
            const report: Report = new Report();
            report.user = account;
            report.target = targetId;
            report.type = reportData.type;
            report.category = reportData.category;
            report.description = reportData.description;
            report.status = "open";

            const { id } = await this.manager.save(report);
            return this.getReportById(id);
        } catch (error) {
            throw error;
        }
    }

    public async getReportById(id: number | string) {
        try {
            return getRepository(Report)
                .createQueryBuilder("report")
                .leftJoinAndSelect("report.user", "report_user")
                .leftJoin("report_user.business", "report_user_business")
                .addSelect("report_user_business.id")
                .addSelect("report_user_business.photo")
                .addSelect("report_user_business.name")
                .addSelect("report_user_business.pageName")
                .leftJoin("report_user.person", "report_user_person")
                .addSelect("report_user_person.id")
                .addSelect("report_user_person.photo")
                .addSelect("report_user_person.fullName")
                .addSelect("report_user_person.pageName")
                .leftJoinAndSelect("report.target", "target")
                .where("report.id = :id", { id })
                .getOne();
        } catch (error) {
            throw error;
        }
    }

    public async deleteReportById(reportId: number | string) {
        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Report)
                .where("id = :reportId", { reportId })
                .execute();
        } catch (error) {
            throw error;
        }
    }

    public async updateReportById(
        reportId: number,
        account: any,
        reportData: ReportInterface
    ) {
        try {
            await this.checkOwnership(reportId, account);
            const newData: any = {};

            if (reportData.description) newData.description = reportData.description;
            if (reportData.status) newData.status = reportData.status;

            if (!isEmpty(newData)) {
                await this.manager.getRepository(Report).update(reportId, newData);
            }

            return this.getReportById(reportId);
        } catch (error) {
            throw error;
        }
    }

    public async addReport(
        entity: string,
        id: any,
        account: any,
        reportData: ReportInterface
    ) {
        try {
            await this.checkSelfReport(entity, id, account);
            const report = new Report();
            report['user'] = account;
            report['target'] = id;
            report['reportType'] = reportData.reportType;
            report['type'] = reportData.type;
            report['report'] = reportData.report;
            report['status'] = "open";

            const saved = await this.manager.save(report);
            return await this.getOne(entity, saved.id);
        } catch (error) {
            throw error;
        }
    }

    public async updateReport(
        entity: string,
        id: any,
        account: any,
        reportData: ReportInterface
    ) {
        try {
            // const report = getRepository(Report);
            // await this.checkOwnership(entity, id, account);
            const newData: any = {};
            if (reportData.report) newData.report = reportData.report;
            if (reportData.type) newData.type = reportData.type;
            if (reportData.reportType) newData.reportType = reportData.reportType;
            if (reportData.status) newData.status = reportData.status;
            if (!isEmpty(newData)) await this.manager.getRepository(Report).update(id, newData);
            return await this.getOne(entity, id);
        } catch (error) {
            throw error;
        }
    }

    public async getOne(
        entity: string,
        id: number|string,
    ) {
        try {
            const report = await this.manager.getRepository(Report)
                .createQueryBuilder("report")
                .leftJoinAndSelect("report.user", "report_user")
                .leftJoin("report_user.business", "report_user_business")
                .addSelect("report_user_business.id")
                .addSelect("report_user_business.photo")
                .addSelect("report_user_business.name")
                .addSelect("report_user_business.pageName")
                .leftJoin("report_user.person", "report_user_person")
                .addSelect("report_user_person.id")
                .addSelect("report_user_person.photo")
                .addSelect("report_user_person.fullName")
                .addSelect("report_user_person.pageName")
                .leftJoinAndSelect("report.target", "target")
                .where("report.id = :id", { id })
                .getOne();

            return report;
        } catch (error) {
            throw error;
        }
    }

    public async getAll(
        entity: string, 
        id: number|string,
        options: {
            page: number|string;
            limit: number|string;
        }
    ) {
        try {
            const pageParams = this.getPageParams(options.page, options.limit);

            const reports = await this.manager.getRepository(Report)
                .createQueryBuilder("report")
                .leftJoinAndSelect("report.user", "report_user")
                .leftJoin("report_user.business", "report_user_business")
                .addSelect("report_user_business.id")
                .addSelect("report_user_business.photo")
                .addSelect("report_user_business.name")
                .addSelect("report_user_business.pageName")
                .leftJoin("report_user.person", "report_user_person")
                .addSelect("report_user_person.id")
                .addSelect("report_user_person.photo")
                .addSelect("report_user_person.fullName")
                .addSelect("report_user_person.pageName")
                .leftJoinAndSelect("report.target", "target")
                .where("target.id = :id", { id })
                .offset(pageParams.offset)
                .limit(pageParams.limit)
                .getMany();

            return reports;
        } catch (error) {
            throw error;
        }
    }

    public async getAllReports(
        id: number,
        options: Filter,
        businessId: number
    ) {
        const { page, limit, type, category, status } = options;
        try {
            const pageParams = this.getPageParams(page, limit);

            const reportsRepo = getRepository(Report);
            let query = reportsRepo
                .createQueryBuilder("report")
                .leftJoinAndSelect("report.user", "report_user")
                .leftJoinAndSelect("report_user.business", "report_user_business")
                .leftJoinAndSelect("report_user.person", "report_user_person")
                .leftJoinAndSelect("report.category", "report_category")
                .leftJoinAndSelect("report.type", "report_type")

            if (options.search) {
                query = query
                    .andWhere(`
                            LOWER(report_user_business.name) SIMILAR TO '%${options.search.toLowerCase()}%'
                            OR LOWER(report_user_person.fullName) SIMILAR TO '%${options.search.toLowerCase()}%'
                        `);
            }

            if (status && status !== 'all') {
                query = query.andWhere('report.status = :status', { status });
            }

            if (category && category != -1) {
                query = query.andWhere('report.category = :category', { category });
            }

            if (type && type != -1) {
                query = query.andWhere('report.category = :category', { category });
            }

            const reports = await query
                .offset(pageParams.offset)
                .limit(pageParams.limit)
                .getManyAndCount();

            return reports;
        } catch (error) {
            throw error;
        }
    }

    public async getReportsByTargetId(
        id: number,
        options: any,
        businessId: number
    ) {
        const { page, limit } = options;
        try {
            const pageParams = this.getPageParams(page, limit);

            const reportsRepo = getRepository(Report);
            let query = reportsRepo
                .createQueryBuilder("report")
                .leftJoinAndSelect("report.user", "report_user")
                .leftJoin("report_user.business", "report_user_business")
                .addSelect("report_user_business.id")
                .addSelect("report_user_business.photo")
                .addSelect("report_user_business.name")
                .addSelect("report_user_business.pageName")
                .leftJoin("report_user.person", "report_user_person")
                .addSelect("report_user_person.id")
                .addSelect("report_user_person.photo")
                .addSelect("report_user_person.fullName")
                .addSelect("report_user_person.pageName")
                .leftJoin("report.reportType", "report_type")
                .addSelect("report_type.name")
                .leftJoinAndSelect("report.target", "target")
                .where("target.id = :id", { id })
                .andWhere("report_user_business.id = :businessId", { businessId })

            if (options.search) {
                query = query
                    .andWhere(`
                            LOWER(report_user_business.name) SIMILAR TO '%${options.search.toLowerCase()}%'
                            OR LOWER(report_user_person.fullName) SIMILAR TO '%${options.search.toLowerCase()}%'
                        `);
            }

            const reports = await query
                .offset(pageParams.offset)
                .limit(pageParams.limit)
                .getManyAndCount();

            return reports;
        } catch (error) {
            throw error;
        }
    }

    public async getReportList(
        options: {
            page: number|string;
            limit: number|string;
        }
    ) {
        try {
            const pageParams = this.getPageParams(options.page, options.limit);

            const query = this.generateReportListQuery([
                {name: "account", table: "t_report_account"},
                {name: "article_comment", table: "t_report_articlecomment"},
                {name: "article_reply", table: "t_report_articlereply"},
                {name: "article", table: "t_report_article"},
                {name: "photo_comment", table: "t_report_photocomment"},
                {name: "photo_reply", table: "t_report_photoreply"},
                {name: "photo", table: "t_report_photo"},
                {name: "product_review", table: "t_report_productreview"},
                {name: "product", table: "t_report_product"},
                {name: "seller_review", table: "t_report_sellerreview"},
                {name: "service_review", table: "t_report_servicereview"},
                {name: "service", table: "t_report_service"},
                {name: "text_comment", table: "t_report_textcomment"},
                {name: "text_reply", table: "t_report_textreply"},
                {name: "text", table: "t_report_text"},
                {name: "video_comment", table: "t_report_videocomment"},
                {name: "video_reply", table: "t_report_videoreply"},
                {name: "video", table: "t_report_video"},
            ], pageParams);

            const rawReports = await this.manager.query(query);

            const reports = await Helpers.beautifyRawData(rawReports, { name: "report"});

            for (const report of reports) {
                report.user.business = await Helpers.nullCheck(report.user.business);
            }
            return reports;
        } catch (error) {
            throw error;
        }
    }

    public async removeReport(
        entity: string, 
        id: number|string,
        account: any
    ) {
        try {
            await this.checkOwnership(id, account);
            await this.manager.getRepository(Report).delete(id);
            return;
        } catch (error) {
            throw error;
        }
    }

    public async checkOwnership(
        reportId: number | string,
        account: any
    ) {
        try {
            const report = await this.manager
                .getRepository(Report)
                .createQueryBuilder("report")
                .where("report.id = :reportId", { reportId })
                .andWhere("report.user = :account", { account })
                .getOne();

            if (report) return true;
            else throw Error("Access denied");
        } catch (error) {
            throw error;
        }
    }

    async checkSelfReport(
        entity: string,
        target: number|string,
        account: any
    ) {
        try {
            if (entity === "account") {
                if (target == account) throw Error("Self reporting");
            } else {
                const targetObject: any = await this.manager
                    .getRepository(Report)
                    .findOne({ where: { id: target }, relations: ["user"]});
                if (!targetObject) throw Error(`${entity} not found`);
                if (targetObject.user.id == account) throw Error("Self reporting");
                return true;
            }
        } catch (error) {
            throw error;
        }
    }

    public async getAllReportCategories() {
        try {
            const reportCategories = await this.manager
                .getRepository(ReportCategory)
                .createQueryBuilder("report_category")
                .getMany();

            return reportCategories;
        } catch (error) {
            throw error;
        }
    }

    public async getReportTypes() {
        try {
            const reportTypes = await this.manager
                .getRepository(ReportType)
                .createQueryBuilder("report_types")
                .getMany();

            return reportTypes;
        } catch (error) {
            throw error;
        }
    }

    private getEntity(entity: string) {
        switch (entity) {
            case "account":
                return { parent: Account };
            case "article":
                return { parent: Article };
            case "photo":
                return { parent: Photo };
            case "product":
                return { parent: Product };
            case "service":
                return { parent: Service };
            case "text":
                return { parent: Text };
            case "video":
                return { parent: Video };
            case "seller_review":
                return { parent: SellerReview };
            case "product_review":
                return { parent: ProductReview };
            case "service_review":
                return { parent: ServiceReview };
            case "article_comment":
                return { parent: Article_Comment };
            case "article_reply":
                return { parent: Article_Comment_Replies };
            case "photo_comment":
                return { parent: Photo_Comment };
            case "photo_reply":
                return { parent: Photo_Comment_Replies };
            case "text_comment":
                return { parent: Text_Comment };
            case "text_reply":
                return { parent: Text_Comment_Replies };
            case "video_comment":
                return { parent: Video_Comment };
            case "video_reply":
                return { parent: Video_Comment_Replies };
            default:
                throw Error("Invalid entity name");
        }
    }

    private getPageParams(
        page: number|string,
        limit: number|string
    ) {
        const params: PageParams = {
            limit: 15,
            offset: 0
        };
        if(limit) params.limit = Number(limit);
        if(page) params.offset = (Number(page) - 1) * params.limit;

        return params;
    }

    private generateReportListQuery(
        entities: Array<{
            name: string;
            table: string;
        }>,
        options: {
            offset: number;
            limit: number;
        }
    ) {
        const lastIndex = entities.length - 1;
        const resultQuery = entities.reduce( (query, {name, table}, index) => {
            return query + `${(index !== 0) ? "UNION ALL" : ""}
            SELECT 
                '${name}' AS "report_entity",
                "report"."id" AS "report_id",
                "report"."report" AS "report_report",
                "report"."target_id" AS "report_target",
                "report"."created" AS "report_created",
                "report"."typeId" AS "report_type_id",
                "report_type"."name" AS "report_type_name",
                "report"."user_id" AS "report_user_id",
                "report_user_person"."id" AS "report_user_person_id",
                "report_user_person"."fullName" AS "report_user_person_fullName",
                "report_user_person"."pageName" AS "report_user_person_pageName",
                "report_user_person"."photo" AS "report_user_person_photo",
                "report_user_business"."id" AS "report_user_business_id",
                "report_user_business"."name" AS "report_user_business_name",
                "report_user_business"."pageName" AS "report_user_business_pageName",
                "report_user_business"."photo" AS "report_user_business_photo"
            FROM 
                "${table}" AS "report"
            LEFT JOIN "t_report_type" AS "report_type"
                ON "report_type"."id"="report"."typeId"
            LEFT JOIN "t_account" AS "report_user"
                ON "report_user"."id"="report"."user_id"
            LEFT JOIN "t_business" "report_user_business"
                ON "report_user_business"."id"="report_user"."businessId"
            LEFT JOIN "t_person" "report_user_person"
                ON "report_user_person"."id"="report_user"."personId"
            ${(index === lastIndex) ? `ORDER BY "report_created" DESC LIMIT ${options.limit} OFFSET ${options.offset}` : ""}
        `;
        } ,"");
        return resultQuery;
    }
}

export default function getReportRepository() {
    return getCustomRepository(ReportRepository);
}
