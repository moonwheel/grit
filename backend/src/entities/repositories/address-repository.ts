import { EntityRepository, Repository, getCustomRepository, getConnection, IsNull, Not } from "typeorm";
import { Address } from "../address";
import { Account } from "../account";
import { Business } from "../business";
import { Person } from "../person";

import { addrType } from '../../interfaces/address/address';

@EntityRepository(Address)
export class AddressRepository extends Repository<Address> {
    async addAddress({ address, account }: { address: Address; account: Account}) {
        const savedAddress = await this.save(address);
        const {relation, of} = this.getRelationOptions(account);
        await getConnection()
            .createQueryBuilder()
            .relation(relation, "addresses")
            .of(of)
            .add(savedAddress);
        return savedAddress;
    }

    async getAccountAddresses(account: Account, addrType?: string | null) {
        const where: any = {
            deleted: IsNull(),
            type: Not('businessAddress'),
        };

        if (addrType) {
            where.addrType = addrType;
        }

        if (account.business) {
            where.business = account.business;
        } else if (account.person) {
            where.person = account.person;
            where.business = IsNull();
        } else {
            return [];
        }

        return await this.find({ where });
    }

    async checkOwnership(id, account) {
        const addresses = await this.getAccountAddresses(account);
        return addresses.some(item => Number(id) === item.id );
    }

    private getRelationOptions(account: Account) {
        return {
            relation: account.business ? Business : Person,
            of: account.business ? account.business : account.person
        };
    }

}

export default function getAddressRepository() {
    return getCustomRepository(AddressRepository);
}
