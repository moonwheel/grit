import { EntityRepository, Repository, getCustomRepository } from "typeorm";
import { Role } from "../role";

interface IRole {
    id: number,
    type: string;
}

@EntityRepository(Role)

export class RoleRepository extends Repository<Role> {

    async getRoleById(roleId: number): Promise<IRole> {
        try {
            return this.createQueryBuilder("role")
                .where("role.id = :roleId", { roleId })
                .getOne();
        } catch (error) {
           throw new Error(error);
        }
    }

}

export default function getRoleRepository() {
    return getCustomRepository(RoleRepository);
}