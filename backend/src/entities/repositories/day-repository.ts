import { EntityRepository, Repository, getCustomRepository } from "typeorm";
import { Day } from "../day";

@EntityRepository(Day)
export class DaysRepository extends Repository<Day> {
    async findByDay(day: string){
        return await this.createQueryBuilder().where(`LOWER(day) SIMILAR TO '%${day.toLowerCase()}%'`).getOne();
    }
}

export default function getDaysRepository() {
    return getCustomRepository(DaysRepository);
}
