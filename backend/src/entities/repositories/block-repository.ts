import { EntityRepository, Repository, getCustomRepository, getConnection, getRepository } from "typeorm";
import { Account } from "../account";
import { Blacklist } from "../blacklist";

@EntityRepository(Blacklist)
export class BlockRepository extends Repository<Blacklist> {
    async toBlocked(
        target: any,
        account: any
    ){
        try {
            if (target == account) throw Error("Self block");
            const insertQuery = await this.createQueryBuilder()
                .insert()
                .into(Blacklist)
                .values([
                    { owner: account, blockedAccount: target }
                ])
                .execute();
            // console.log('insertQuery', insertQuery)
            return insertQuery.generatedMaps;

            // return await this.getBlacklist(account);
        } catch (error) {
            throw error;
        }
    }

    async fromBlocked(
        target: any,
        account: any
    ){
        try {
            const deleteQuery = await this.createQueryBuilder()
                .delete()
                .from(Blacklist)
                .where(`"ownerId" = :account and "blockedAccountId" = :target`, { account, target })
                .execute();

            // console.log('deleteQuery', deleteQuery)
            return deleteQuery
            // return await this.getBlacklist(account);
        } catch (error) {
            throw error;
        }
    }

    async getBlacklist(
        account: any,
        limit: number = 10,
        offset: number = 0
    ){
        try {
            let query2 = getRepository(Account)
                .createQueryBuilder("account")
                .leftJoin("account.business", "business")
                .addSelect("business.id")
                .addSelect("business.photo")
                .addSelect("business.name")
                .addSelect("business.pageName")
                .leftJoin("account.person", "person")
                .addSelect("person.id")
                .addSelect("person.photo")
                .addSelect("person.fullName")
                .addSelect("person.pageName")
                .leftJoinAndSelect("account.followings", "followings")
                .leftJoinAndMapOne(`account.following`, `account.followers`, `account_followers`, `account_followers.account = ${account}`)
                .where(qb => {
                    const subQuery = qb.subQuery()
                        .select(`"blacklist"."blockedAccountId"`)
                        .from(Blacklist, "blacklist")
                        .where(`"blacklist"."ownerId" = :account`, { account })
                        .getQuery();
                    return "account.id IN " + subQuery;
                })
                .offset(Number(offset))
                .limit(Number(limit))

            const blacklist2 = await query2.getMany();
            return blacklist2
        } catch (error) {
            throw error;
        }
    }

    async blockedBy(
        account: any
    ){
        try {
            // return await this.createQueryBuilder()
            //     .relation(Account, "blocked")
            //     .of(account)
            //     .loadMany()
            //     .then(result => result.map(blocked => blocked.id));


            // console.log('account', account)
            let query = getRepository(Account)
                .createQueryBuilder("account")
                .select('account.id')
                .where(qb => {
                    const subQuery = qb.subQuery()
                        .select(`"blacklist"."ownerId"`)
                        .from(Blacklist, "blacklist")
                        .where(`"blacklist"."blockedAccountId" = :account`, { account })
                        .getQuery();
                    return "account.id IN " + subQuery;
                })
            // console.log('query.getSql', query.getSql())
            const result = await query.getMany();
            // console.log('blockedBy result', result)
            return result.map(account => account.id);

        } catch (error) {
            throw error;
        }
    }
}

export default function getBlockRepository() {
    return getCustomRepository(BlockRepository);
}
