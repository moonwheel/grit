import { Followings_request, RequestStatus } from '../followings_request';
import { EntityRepository, Repository, getCustomRepository, Brackets } from "typeorm";
import { Account } from "../account";

@EntityRepository(Followings_request)
export class FollowingRequestRepository extends Repository<Followings_request> {
    async createFollowRequest(
        target: Account,
        account: any,
    ) {

        try {
            console.log('target', target);

            const saved = await this.save({
                account: account,
                followedAccount: target,
                created: new Date(),
                status: 'pending'
            });

            return saved;
        } catch (error) {
            console.log('save followings error', error)
            throw error;
        }
    }

    async getFollowingRequest(account: Account, target: number) {
        try {
            const followsRequest = await this.createQueryBuilder('request')
                .where('request.followedAccount = :target', {
                    target
                })
                .andWhere('request.account = :account', {
                    account
                })
                .getOne();

            if (!followsRequest) {
                throw new Error('Following request not found');
            }
            return followsRequest;
        } catch (error) {
            throw error;
        }
    }

    async deleteFollowRequest(
        target: any,
        account: any
    ) {
        try {
            await this.delete({
                account: account,
                followedAccount: target,
            });
            return;
        } catch (error) {
            throw error;
        }
    }

    async updateFollowingRequest(
        requestId: number,
        target: Account,
        status: RequestStatus
    ) {
        try {
            const followsRequest = await this.findOne({
                id: requestId,
                followedAccount: target.baseBusinessAccount || target,
                // status: 'pending'
            }, {relations: ['account']});

            if (!followsRequest) throw new Error('Following request not found');

            await this.update({
                id: requestId,
                followedAccount: target.baseBusinessAccount || target,
                // status: 'pending'
            }, {
                status: status,
                updated: new Date()
            })
            return followsRequest;
        } catch (error) {
            throw error;
        }
    }

    async isAccountAlreadyRequestedFollowingsOfTarget(account: Account, target: Number) {
        console.log('account', account)
        console.log('target', target)
        try {
            const alreadyRequested = await this.createQueryBuilder('request')
                .where('request.followedAccount = :target', {
                    target
                })
                .andWhere('request.account = :account', {
                    account
                })
                .getOne();
            console.log('alreadyRequested', alreadyRequested)
            return alreadyRequested
        } catch (error) {
            throw error;
        }
    }

    async getIncomingFollowingsRequests (
        followedAccount: Account,
        limit: number = 10,
        offset: number = 0
    ) {
        try {
            return this.createQueryBuilder('request')
                .leftJoinAndSelect("request.account", "request_account")
                .leftJoinAndSelect("request.followedAccount", "request_followedAccount")
                .leftJoinAndSelect("request_account.person", "request_account_person")
                .leftJoinAndSelect("request_account.business", "request_account_business")
                .where("request.followedAccount = :followedAccount", { followedAccount })
                .andWhere("request.status = 'pending'")
                .offset(Number(offset))
                .limit(Number(limit))
                .getMany();
        } catch (error) {
            throw error;
        }
    };

    async getOutgoingFollowingsRequests (account: Account){
        try {
            const outgoingRequests = await this.find({ account })
            console.log('outgoingRequests', outgoingRequests)
            return outgoingRequests
        } catch (error) {
            throw error
        }
    };

}

export default function getFollowingRequestRepository() {
    return getCustomRepository(FollowingRequestRepository);
}
