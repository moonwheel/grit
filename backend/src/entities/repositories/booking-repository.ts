import {
  EntityRepository,
  Repository,
  getCustomRepository,
  getConnection,
  getRepository,
} from "typeorm";
import * as moment from "moment";

import { Booking } from "../booking";
import { Service } from "../service";
import { BookingAppointment } from "../booking_appointments";

import { AccountRole } from "../../enums";
import paymentProcess from "../../utils/payment-process";
import mangoPayment from "../../utils/mango-payment";
import { isFloat } from "../../utils/primitives";

interface BookingQueryOptions {
  page?: number | string;
  limit?: number | string;
  search?: string;
  filter?: string;
}

@EntityRepository(Booking)
export class BookingRepository extends Repository<Booking> {
  public async getBookings(
    userId: number,
    type: "purchases" | "sales" = "purchases",
    options: BookingQueryOptions = {
      page: 1,
      limit: 15,
      search: "",
      filter: "all",
    }
  ) {
    const limitNumb = Number(options.limit);
    const skip = (Number(options.page) - 1) * limitNumb;

    const query = this.createQueryBuilder("booking")
      .leftJoinAndSelect("booking.seller", "booking_seller")
      .leftJoinAndSelect("booking_seller.person", "seller_person")
      .leftJoinAndSelect("booking_seller.business", "seller_business")
      .leftJoinAndSelect("booking.buyer", "booking_buyer")
      .leftJoinAndSelect("booking_buyer.person", "buyer_person")
      .leftJoinAndSelect("booking_buyer.business", "buyer_business")
      .leftJoinAndSelect("booking.service", "booking_service")
      .leftJoinAndSelect("booking.delivery_address", "booking_delivery_address")
      .leftJoinAndSelect("booking.billing_address", "booking_billing_address")
      .leftJoinAndSelect("booking.appointments", "booking_appointments")
      .leftJoinAndSelect("booking_service.photos", "booking_photos")
      .leftJoinAndMapOne(
        "booking_service.photoCover",
        "booking_service.photos",
        "booking_service_photos",
        "booking_service_photos.index = 0"
      )
      .where("booking_buyer.id = :userId", { userId })
      .andWhere(
        `${
          type === "purchases" ? "booking_buyer" : "booking_seller"
        }.id = :userId`,
        { userId }
      )
      .skip(skip)
      .take(limitNumb);

    if (options.search) {
      query.andWhere(
        `
                (booking.id = :id
                OR LOWER(seller_person.fullName) LIKE :search
                OR LOWER(seller_person.pageName) LIKE :search
                OR LOWER(seller_business.name) LIKE :search
                OR LOWER(seller_business.pageName) LIKE :search
                OR LOWER(booking_service.title) LIKE :search) 
                AND ${
                  type === "purchases" ? "booking_buyer" : "booking_seller"
                }.id = :userId
            `,
        {
          id: isFloat(options.search as any)
            ? null
            : Number(options.search) || null,
          search: `%${options.search.toLowerCase()}%`,
          userId,
        }
      );
    }

    if (options.filter === "open") {
      query
        .andWhere("booking.cancelled IS NULL")
        .andWhere("booking.refunded IS NULL")
        .andWhere("booking.is_completed IS NOT TRUE");
    } else if (options.filter === "cancelled") {
      query.andWhere("booking.cancelled IS NOT NULL");
    } else if (options.filter === "refunded") {
      query.andWhere("booking.refunded IS NOT NULL");
    }

    return query.getManyAndCount();
  }

  public async addBooking(
    buyerId: any,
    service: Service,
    payment: string | number,
    totalPrice: number,
    totalTime: number,
    appointments: any[],
    gritWallet: boolean
  ): Promise<Booking> {
    const bookingAppointments = [];

    for (let app of appointments) {
      const appointment = new BookingAppointment();

      appointment.start = app.start;
      appointment.end = app.end;

      await getRepository(BookingAppointment).save(appointment);
      bookingAppointments.push(appointment);
    }

    const { transactionId } = await paymentProcess.createPayInToWallet(
      buyerId,
      Number(payment),
      totalPrice,
      gritWallet,
      "booking"
    );

    const booking = new Booking();

    booking.buyer = buyerId;
    booking.seller = service.user.id as any;
    booking.service = service.id;
    booking.total_price = totalPrice;
    booking.total_time = totalTime;
    booking.paid = new Date();
    booking.payment_id = Number(payment);
    booking.appointments = bookingAppointments;
    booking.pay_in_id = Number(transactionId);

    return this.save(booking);
  }

  public async getNotPaidBookings() {
    return getRepository(BookingAppointment)
      .createQueryBuilder("booking_appointments")
      .leftJoinAndSelect(`booking_appointments.booking`, `booking`)
      .leftJoinAndSelect("booking.seller", "booking_seller")
      .leftJoinAndSelect("booking_seller.person", "seller_person")
      .leftJoinAndSelect("booking_seller.business", "seller_business")
      .leftJoinAndSelect("booking.buyer", "booking_buyer")
      .leftJoinAndSelect("booking_buyer.person", "buyer_person")
      .leftJoinAndSelect("booking_buyer.business", "buyer_business")
      .addSelect(`booking_seller.id`)
      .where("booking_appointments.end < NOW()")
      .groupBy(
        `
                booking_seller.*,
                booking_buyer.*,
                booking_appointments.*,
                booking.*,
                booking_appointments.booking_id,
                booking_appointments.end
            `
      )
      .orderBy("booking_appointments.end", "DESC")
      .getMany();
  }

  public async getNotPaidBookingsById(bookingId: number) {
    return getRepository(BookingAppointment)
      .createQueryBuilder("booking_appointments")
      .leftJoinAndSelect(`booking_appointments.booking`, `booking`)
      .leftJoinAndSelect("booking.seller", "booking_seller")
      .leftJoinAndSelect("booking_seller.person", "seller_person")
      .leftJoinAndSelect("booking_seller.business", "seller_business")
      .leftJoinAndSelect("booking.buyer", "booking_buyer")
      .leftJoinAndSelect("booking_buyer.person", "buyer_person")
      .leftJoinAndSelect("booking_buyer.business", "buyer_business")
      .where("booking_appointments.end < NOW()")
      .andWhere("booking.id = :bookingId", { bookingId })
      .groupBy(
        `
                booking_seller.*,
                booking_buyer.*,
                booking_appointments.*,
                booking.*,
                booking_appointments.booking_id,
                booking_appointments.end
            `
      )
      .getMany();
  }

  public async changeBookingStatus(): Promise<void> {
    await getConnection()
      .createQueryBuilder()
      .update(Booking)
      .set({
        // sold: true
      })
      .execute();
  }

  public async getBookingAppointmentsByUserId(userId: number) {
    return getRepository(BookingAppointment)
      .createQueryBuilder("booking_appointments")
      .leftJoinAndSelect(`booking_appointments.booking`, `booking`)
      .where("booking.user_id = :userId", { userId })
      .getMany();
  }

  public async cancel(
    bookingId: number,
    userId: number,
    cancelReason: string,
    cancelDescription: string
  ): Promise<Booking> {
    const booking = await this.findOne(bookingId, {
      relations: ["buyer", "seller", "appointments"],
      // order: {
      // appointments: 'ASC',
      // },
    });

    if (!booking) {
      throw new Error(`Booking doesn't exist`);
    }

    if (booking.cancelled) {
      throw new Error(`Booking has already been cancelled`);
    }

    let whoCancelled: AccountRole = AccountRole.Buyer;

    if (booking.buyer.id === userId) {
      whoCancelled = AccountRole.Buyer;
    } else if (booking.seller.id === userId) {
      whoCancelled = AccountRole.Seller;
    } else {
      throw new Error(`You don't have permissions to cancel`);
    }

    if (
      booking.appointments.length &&
      moment().diff(moment(booking.appointments[0].start), "hour") > 24
    ) {
      throw new Error(`You can't cancel booking`);
    }

    if (!booking.pay_in_id) {
      throw new Error(`PayIn ID not found.`);
    }

    const cancelledDate = new Date();
    let refundedDate = undefined;

    // pay out for seller with orderPrice
    if (!booking.is_completed) {
      const mangoUserId = booking.buyer.business
        ? booking.buyer.business.mangoUserId
        : booking.buyer.person.mangoUserId;
      await mangoPayment.refundToCard(
        booking.pay_in_id,
        mangoUserId,
        booking.total_price * 100
      );
      refundedDate = new Date();
    }

    await this.save({
      ...booking,
      cancelled: cancelledDate,
      who_cancelled: whoCancelled,
      when_cancelled: booking.is_completed ? "after" : "before",
      cancel_reason: cancelReason,
      cancel_description: cancelDescription,
    });

    return this.getBookingById(bookingId, userId);
  }

  public async refund(userId: number, bookingId: number): Promise<Booking> {
    const booking = await this.findOne(bookingId, {
      relations: ["seller", "buyer"],
    });

    if (!booking) {
      throw new Error("Booking does not exist");
    }

    if (booking.seller.id !== userId) {
      throw new Error(`You don't have permissions`);
    }

    if (!booking.cancelled) {
      throw new Error(`You can't refund money.`);
    }

    if (!booking.transfer_id) {
      throw new Error(`Transfer ID not found.`);
    }

    const mangopayUserId = booking.seller.business
      ? booking.seller.business.mangoUserId
      : booking.seller.person.mangoUserId;

    await mangoPayment.refundTransfer(booking.transfer_id, mangopayUserId);

    await this.save({
      ...booking,
      refunded: new Date(),
    });

    return this.getBookingById(bookingId, userId);
  }

  public async saveTransactionData(
    bookingId: number,
    transactionId: number,
    total_price: number,
    fee: number
  ): Promise<void> {
    await getConnection()
      .createQueryBuilder()
      .update(Booking)
      .set({
        total_price,
        transaction: transactionId,
        fee,
      })
      .where("id = :bookingId", { bookingId })
      .execute();
  }

  public async getBookingById(id: number, userId: number) {
    return getRepository(Booking)
      .createQueryBuilder("booking")
      .leftJoinAndSelect("booking.seller", "booking_seller")
      .leftJoinAndSelect("booking_seller.person", "seller_person")
      .leftJoinAndSelect("booking_seller.business", "seller_business")
      .leftJoinAndSelect("booking.buyer", "booking_buyer")
      .leftJoinAndSelect("booking_buyer.person", "buyer_person")
      .leftJoinAndSelect("booking_buyer.business", "buyer_business")
      .leftJoinAndSelect("booking.service", "booking_service")
      .leftJoinAndSelect("booking.delivery_address", "booking_delivery_address")
      .leftJoinAndSelect("booking.billing_address", "booking_billing_address")
      .leftJoinAndSelect("booking.appointments", "booking_appointments")
      .leftJoinAndSelect("booking_service.photos", "booking_photos")
      .leftJoinAndMapOne(
        "booking_service.photoCover",
        "booking_service.photos",
        "booking_service_photos",
        "booking_service_photos.index = 0"
      )
      .where("booking.id = :id", { id })
      .andWhere("booking_buyer.id = :userId OR booking_seller.id = :userId", {
        userId,
      })
      .getOne();
  }

  public async savePayInAmount(
    userId: number,
    transactionId: number,
    amount: number
  ) {
    await this.update(
      {
        buyer: {
          id: userId,
        },
      },
      {
        transaction: transactionId,
        amount_card: amount,
      }
    );
  }

  public async changeBookingByTransactionId(id: string, eventType: string) {
    const now = new Date().toISOString();

    let body = {};

    switch (eventType) {
      case "PAYIN_NORMAL_SUCCEEDED":
        body = { pain_status: "success", pain_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYIN_NORMAL_FAILED":
        body = { pain_status: "failed", pain_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYOUT_NORMAL_SUCCEEDED":
        body = { payout_status: "success", payout_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYOUT_NORMAL_FAILED":
        body = { payout_status: "failed", payout_date: now };
        await this.changeBooking(id, body);
        break;
      case "TRANSFER_NORMAL_SUCCEEDED":
        body = { transfer_status: "success", transfer_date: now };
        await this.changeBooking(id, body);
        break;
      case "TRANSFER_NORMAL_FAILED":
        body = { transfer_status: "failed", transfer_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYIN_REFUND_SUCCEEDED":
        body = { payin_refund_status: "success", payin_refund_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYIN_REFUND_FAILED":
        body = { payin_refund_status: "failed", payin_refund_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYOUT_REFUND_SUCCEEDED":
        body = { payout_refund_status: "success", payout_refund_date: now };
        await this.changeBooking(id, body);
        break;
      case "PAYOUT_REFUND_FAILED":
        body = { payout_refund_status: "failed", payout_refund_date: now };
        await this.changeBooking(id, body);
        break;
    }
  }

  private async changeBooking(id, body) {
    await getConnection()
      .createQueryBuilder()
      .update(Booking)
      .set(body)
      .where("transaction = :id", { id })
      .execute();
  }
}

export default function getBookingRepository() {
  return getCustomRepository(BookingRepository);
}
