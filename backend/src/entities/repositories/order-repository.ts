import {
  EntityRepository,
  Repository,
  getCustomRepository,
  getConnection,
  getRepository,
} from "typeorm";
import * as moment from "moment";

import { Order, PaymentStatus, PaymentType } from "../order";
import { OrderProduct } from "../order_product";

import { AccountRole } from "../../enums";
import mangoPayment from "../../utils/mango-payment";
import paymentProcess from "../../utils/payment-process";
import { isFloat } from "../../utils/primitives";

interface OrderQueryOptions {
  page?: number | string;
  limit?: number | string;
  filter?: string;
  search?: string;
}

interface OrderData {
  buyerId: any;
  sellerId: any;
  payment: number;
  deliveryAddressId: any;
  billingAddressId: any;
  transaction?: number;
  shippingTransferId?: number;
  payInId?: number;
  amount?: number;
  totalPrice?: number;
  shippingPrice?: number;
  shipped?: Date;
  cancelReason?: string;
  cancelDescription?: string;
  whoCancelled?: string;
  whenCancelled?: string;
  cancelled?: Date;
  returned?: Date;
  refunded?: Date;
  is_wallet?: boolean;
  payment_type: PaymentType;
}

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  public async getOrders(
    userId: number,
    type: "purchases" | "sales" = "purchases",
    options: OrderQueryOptions = {
      page: 1,
      limit: 15,
      filter: "all",
      search: "",
    }
  ) {
    const limitNumb = Number(options.limit);
    const skip = (Number(options.page) - 1) * limitNumb;

    const query = this.createQueryBuilder("order")
      .leftJoinAndSelect("order.seller", "order_seller")
      .leftJoinAndSelect("order_seller.person", "seller_person")
      .leftJoinAndSelect("order_seller.business", "seller_business")
      .leftJoinAndSelect("order.buyer", "order_buyer")
      .leftJoinAndSelect("order_buyer.person", "buyer_person")
      .leftJoinAndSelect("order_buyer.business", "buyer_business")
      .leftJoinAndSelect("order.order_items", "order_items")
      .leftJoinAndSelect("order_items.product", "order_items_product")
      .leftJoinAndMapOne("order_items_product.cover","order_items_product.cover_", 'order_items_product_cover')
      .leftJoinAndSelect("order.delivery_address", "order_delivery_address")
      .leftJoinAndSelect("order.billing_address", "order_billing_address")
      .where(
        `${type === "purchases" ? "order_buyer" : "order_seller"}.id = :userId`,
        { userId }
      )
      .skip(skip)
      .take(limitNumb);

    if (options.search) {
      query.andWhere(
        `
                (order.id = :id
                OR LOWER(seller_person.fullName) LIKE :search
                OR LOWER(seller_person.pageName) LIKE :search
                OR LOWER(seller_business.name) LIKE :search
                OR LOWER(seller_business.pageName) LIKE :search
                OR order.id IN (
                    SELECT order_id
                    FROM t_order_product
                        JOIN t_product ON t_order_product.product_id = t_product.id
                    WHERE LOWER(t_product.title) LIKE :search
                )) AND ${
                  type === "purchases" ? "order_buyer" : "order_seller"
                }.id = :userId
            `,
        {
          id: isFloat(options.search as any)
            ? null
            : Number(options.search) || null,
          search: `%${options.search.toLowerCase()}%`,
          userId,
        }
      );
    }

    if (options.filter === "open") {
      query.andWhere(`order.id IN (
                SELECT order_id
                FROM t_order_product
                WHERE t_order_product.shipped IS NULL
                AND t_order_product.refunded IS NULL)
            `);
    } else if (options.filter === "cancelled") {
      query.andWhere(`order.id IN (
                SELECT order_id
                FROM t_order_product
                WHERE t_order_product.canceled IS NOT NULL)
            `);
    } else if (options.filter === "refunded") {
      query.andWhere(`order.id IN (
                SELECT order_id
                FROM t_order_product
                WHERE t_order_product.refunded IS NOT NULL)
            `);
    }

    return query.getManyAndCount();
  }

  public async getOrder(userId: number, orderId: number) {
    return await this.createQueryBuilder("order")
      .leftJoinAndSelect("order.seller", "order_seller")
      .leftJoinAndSelect("order_seller.person", "seller_person")
      .leftJoinAndSelect("order_seller.business", "seller_business")
      .leftJoinAndSelect("order.buyer", "order_buyer")
      .leftJoinAndSelect("order_buyer.person", "buyer_person")
      .leftJoinAndSelect("order_buyer.business", "buyer_business")
      .leftJoinAndSelect("order.order_items", "order_items")
      .leftJoinAndSelect("order_items.product", "order_items_product")
      .leftJoinAndSelect(
        "order_items_product.photos",
        "product_photos",
        `order_items_product.type = 'singleProduct' AND product_photos.deleted IS NULL AND product_photos.order = '0'`
      )
      .leftJoinAndMapOne(
        "order_items_product.selectedVariant",
        "t_product_variant",
        "product_variants",
        "product_variants.id = order_items.product_variant_id"
      )
      .leftJoinAndMapOne(
        "product_variants.cover",
        "product_variants.photos",
        "variants_to_photos",
        "variants_to_photos.order = 0"
      )
      .leftJoinAndSelect("variants_to_photos.photo", "product_variants_photos")
      .leftJoinAndSelect("order.delivery_address", "order_delivery_address")
      .leftJoinAndSelect("order.billing_address", "order_billing_address")
      .where("order.id = :orderId", { orderId })
      .andWhere("(order_buyer.id = :userId OR order_seller.id = :userId)", {
        userId,
      })
      .getOne();
  }

  public async createNewOrder(data: OrderData): Promise<number> {
    const order = new Order();

    order.buyer = data.buyerId;
    order.seller = data.sellerId;
    order.payment_status = "pending";
    order.delivery_address = data.deliveryAddressId;
    order.billing_address = data.billingAddressId;
    order.paid = new Date();
    order.payment_id = data.payment;
    order.total_price = Number(data.totalPrice);
    order.shipping_price = Number(data.shippingPrice);
    order.fee = Number(data.totalPrice) * 0.03;
    order.amount_card = data.amount;
    order.transaction = data.transaction;
    order.shipping_transfer_id = Number(data.shippingTransferId) || undefined;
    order.pay_in_id = Number(data.payInId) || undefined;
    order.is_wallet = data.is_wallet || undefined;
    order.payment_type = data.payment_type;

    const { id } = await this.save(order);
    return id;
  }

  public async ship(
    userId: number,
    orderId: number,
    items: number[]
  ): Promise<Order> {
      const order = await this.findOne(orderId, {
        relations: ["seller", "buyer", "delivery_address", "billing_address"],
      });

      if (!order) {
        throw new Error("Order does not exist");
      }

      if (order.seller.id !== userId) {
        throw new Error(`You don't have permissions`);
      }

      const shippedItems = await this.getOrderItems(orderId, items);

      if (shippedItems.some((item) => item.cancelled)) {
        throw new Error(
            `You can't ship products which have already been cancelled`
        );
      }

      if (shippedItems.every(item => item.shipped)) {
        throw new Error(`All selected products have already been shipped`);
      }
    if (!order.shipped && parseInt(String(order.shipping_price), 10) !== 0) {
        const shippingTransactionId = await paymentProcess.managePayments(
            order.buyer,
            order.seller,
            order.shipping_price,
        );

        await this.update(order.id, {
          shipping_transfer_id: Number(shippingTransactionId),
        });
      }

      const shippingDate = new Date();

      for (let item of shippedItems) {
        if (!item.shipped && !item.cancelled) {
          const productAmount = item.product_price * item.quantity;

          const transactionId = await paymentProcess.managePayments(
              order.buyer,
              order.seller,
              productAmount
          );

          await getRepository(OrderProduct).update(item.id, {
            shipped: shippingDate,
            transfer_id: Number(transactionId),
          });
        }
      }

      await this.save({
        ...order,
        shipped: new Date(),
      });

      return this.getOrder(userId, orderId);
  }

  public async cancel(
    userId: number,
    orderId: number,
    items: number[],
    reason: string,
    description: string
  ): Promise<Order> {
    const order = await this.findOne(orderId, {
      relations: ["seller", "buyer", "delivery_address", "billing_address"],
    });

    if (!order) {
      throw new Error("Order does not exist");
    }

    let whoCancelled: AccountRole = AccountRole.Buyer;

    if (order.buyer.id === userId) {
      whoCancelled = AccountRole.Buyer;
    } else if (order.seller.id === userId) {
      whoCancelled = AccountRole.Seller;
    } else {
      throw new Error(`You don't have permissions to cancel`);
    }

    if (order.shipped && moment().diff(moment(order.shipped), "day") > 30) {
      throw new Error(`You can't cancel order`);
    }

    const cancelledItems = await this.getOrderItems(orderId, items);

    if (cancelledItems.every((item) => item.cancelled)) {
      throw new Error(`All selected products have already been cancelled`);
    }

    const cancelledDate = new Date();
    const refundedDate = new Date();

    for (const item of cancelledItems) {
      if (!item.shipped) {
        const orderProductPrice = item.product_price * item.quantity * 100;
        const mangopayUserId = order.buyer.business
          ? order.buyer.business.mangoUserId
          : order.buyer.person.mangoUserId;
        await mangoPayment.refundToCard(
          order.pay_in_id,
          mangopayUserId,
          orderProductPrice
        );
      }

      await getRepository(OrderProduct).update(item.id, {
        cancelled: cancelledDate,
        cancel_reason: reason,
        cancel_description: description,
        who_cancelled: whoCancelled,
        when_cancelled: item.shipped ? "after" : "before",
        refunded: !item.shipped ? refundedDate : undefined,
      });
    }

    await this.save({
      ...order,
      cancelled: cancelledDate,
      cancel_reason: reason,
      cancel_description: description,
      who_cancelled: whoCancelled,
      when_cancelled: order.shipped ? "after" : "before",
    });

    return this.getOrder(userId, orderId);
  }

  public async returnOrder(
    userId: number,
    orderId: number,
    items: number[],
    reason: string,
    description: string
  ): Promise<Order> {
    const order = await this.findOne(orderId, {
      relations: ["seller", "buyer", "delivery_address", "billing_address"],
    });

    if (!order) {
      throw new Error("Order does not exist");
    }

    if (order.seller.id === userId) {
      throw new Error(`You don't have permissions to return`);
    }

    const returnedItems = await this.getOrderItems(orderId, items);
    console.log(returnedItems)

    if (!returnedItems.some((item) => item.shipped)) {
      throw new Error(`All selected products must be shipped before returning`);
    }

    // if (!returnedItems.some((item) => item.cancelled)) {
    //   throw new Error(
    //     `You need to cancel all selected products before returning`
    //   );
    // }

    if (returnedItems.every((item) => item.returned)) {
      throw new Error(`All selected products have already been returned`);
    }

    for (let item of returnedItems) {
      if (item.shipped && !item.returned) {
        await getRepository(OrderProduct).update(item.id, {
          returned: new Date(),
          return_reason: reason,
          return_description: description,
        });
      }
    }

    if (!order.returned) {
      await this.save({
        ...order,
        returned: new Date(),
      });
    }

    return this.getOrder(userId, orderId);
  }

  public async refund(
    userId: number,
    orderId: number,
    items: number[],
    payment = ""
  ): Promise<Order> {
    const order = await this.findOne(orderId, {
      relations: ["seller", "buyer", "delivery_address", "billing_address"],
    });

    if (!order) {
      throw new Error("Order does not exist");
    }

    if (order.buyer.id === userId) {
      throw new Error(`You don't have permissions to refund`);
    }

    const refundedItems = await this.getOrderItems(orderId, items);

    if (refundedItems.every((item) => item.refunded)) {
      console.log("item", refundedItems);
      throw new Error(`All selected products have already been refunded`);
    }

    for (let item of refundedItems) {
      if (item.cancelled && item.returned && !item.refunded) {
        const mangopayUserId = order.seller.business
          ? order.seller.business.mangoUserId
          : order.seller.person.mangoUserId;

        await mangoPayment.refundTransfer(item.transfer_id, mangopayUserId);

        await getRepository(OrderProduct).update(item.id, {
          refunded: new Date(),
        });
      }
    }

    await this.save({
      ...order,
      refunded: new Date(),
    });

    return await this.getOrder(userId, orderId);
  }

  public async getOrderItems(orderId: number, items: number[]) {
    return getRepository(OrderProduct)
      .createQueryBuilder("order_product")
      .select("order_product.id", "id")
      .addSelect("order_product.price", "product_price")
      .addSelect(
        `
                CASE
                    WHEN order_product.collect = false THEN order_product.shipping_price
                    ELSE 0
                END`,
        "shipping_price"
      )
      .addSelect("order_product.quantity", "quantity")
      .addSelect("order_product.transfer_id", "transfer_id")
      .addSelect("order_product.shipped", "shipped")
      .addSelect("order_product.cancelled", "cancelled")
      .addSelect("order_product.returned", "returned")
      .addSelect("order_product.refunded", "refunded")
      .where("order_product.order_id = :orderId", { orderId })
      .andWhere(`order_product.id IN (:...items)`, { items })
      .groupBy("order_product.id")
      .getRawMany();
  }

  public calculateOrderPrice(orderItems: any[]) {
    const shippingPrice = orderItems.reduce((max, item) => {
      return Number(item.shipping_price) > max
        ? Number(item.shipping_price)
        : max;
    }, Number(orderItems[0].shipping_price));

    const totalPrice =
      orderItems.reduce((sum, item) => {
        return sum + Number(item.product_price) * Number(item.quantity);
      }, 0) + shippingPrice;

    return { shippingPrice, totalPrice };
  }

  public async changePaymentStatus(
    userId: number,
    orderId: number,
    paymentStatus: PaymentStatus
  ): Promise<void> {
    await getConnection()
      .createQueryBuilder()
      .update(Order)
      .set({
        payment_status: paymentStatus,
      })
      .where("user_id = :userId", { userId })
      .andWhere("id = :orderId", { orderId })
      .execute();
  }

  public async changePaymentDate(
    userId: number,
    orderId: number
  ): Promise<void> {
    await getConnection()
      .createQueryBuilder()
      .update(Order)
      .set({
        payment_date: new Date().toISOString(),
      })
      .where("user_id = :userId", { userId })
      .andWhere("id = :orderId", { orderId })
      .execute();
  }

  public async savePayInAmount(
    userId: number,
    transactionId: number,
    amount: number
  ) {
    await getConnection()
      .createQueryBuilder()
      .update(Order)
      .set({
        transaction: transactionId,
        amount_card: amount,
      })
      .where("user_id = :userId", { userId })
      .execute();
  }

  public async changeOrderByTransactionId(id: string, eventType: string) {
    const now = new Date().toISOString();

    let body = {};

    switch (eventType) {
      case "PAYIN_NORMAL_SUCCEEDED":
        body = { pain_status: "success", pain_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYIN_NORMAL_FAILED":
        body = { pain_status: "failed", pain_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYOUT_NORMAL_SUCCEEDED":
        body = { payout_status: "success", payout_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYOUT_NORMAL_FAILED":
        body = { payout_status: "failed", payout_date: now };
        await this.changeOrder(id, body);
        break;
      case "TRANSFER_NORMAL_SUCCEEDED":
        body = { transfer_status: "success", transfer_date: now };
        await this.changeOrder(id, body);
        break;
      case "TRANSFER_NORMAL_FAILED":
        body = { transfer_status: "failed", transfer_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYIN_REFUND_SUCCEEDED":
        body = { payin_refund_status: "success", payin_refund_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYIN_REFUND_FAILED":
        body = { payin_refund_status: "failed", payin_refund_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYOUT_REFUND_SUCCEEDED":
        body = { payout_refund_status: "success", payout_refund_date: now };
        await this.changeOrder(id, body);
        break;
      case "PAYOUT_REFUND_FAILED":
        body = { payout_refund_status: "failed", payout_refund_date: now };
        await this.changeOrder(id, body);
        break;
    }
  }

  /**
   * Change Order status
   * @param {Number} id
   * @param {Object} body
   */
  private async changeOrder(id, body) {
    await getConnection()
      .createQueryBuilder()
      .update(Order)
      .set(body)
      .where("transaction = :id", { id })
      .execute();
  }
}

export default function getOrderRepository() {
  return getCustomRepository(OrderRepository);
}
