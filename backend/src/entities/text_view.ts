import { BaseEntity, Column, Entity,Unique, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Text } from "./text";
import { Account } from "./account";

@Entity("t_text_view")
@Index("t_text_view_user_idx", ["user",])
@Index("t_text_view_text_idx", ["text_",])
@Unique(["user", "text_"])
export class Text_View {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.views_text)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Text, t_text => t_text.views)
    @JoinColumn({ name: "text_id" })
    text_: Text | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

}