import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ProductReview } from "./product_review";
import { ServiceReview } from "./service_review";
import { Account } from "./account";
import { SellerReview } from "./seller_review";

@Entity("t_review_photo")
export class ReviewPhoto {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.review_photo, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: true,
        name: "link"
    })
    link: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @ManyToOne(type => ProductReview, t_product_review => t_product_review.photo ,{ onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    productReview: ProductReview;

    @ManyToOne(type => ServiceReview, t_service_review => t_service_review.photo ,{ onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    serviceReview: ServiceReview;

    @ManyToOne(type => SellerReview, t_seller_review => t_seller_review.photo ,{ onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    sellerReview: SellerReview;
}
