import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Text } from "./text";
import { Photo } from "./photo";
import { Video } from "./video";
import { Article } from "./article";
import { Product } from "./product";
import { Account } from "./account";

@Entity("t_view")
@Index("view_user_id_idx", ["user",])
@Index("view_text_id_idx", ["text_",])
@Index("view_photo_id_idx", ["photo_",])
@Index("view_video_id_idx", ["video_",])
@Index("view_article_id_idx", ["article_",])
@Index("view_product_id_idx", ["product_",])
export class View {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("int", {
        nullable: true,
        width: 1,
        name: "view"
    })
    view: boolean | null;

    @Column("int", {
        nullable: true,
        name: "total"
    })
    total: number | null;

    @ManyToOne(type => Text, t_text => t_text.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    text_: Text | null;

    @ManyToOne(type => Photo, t_photo => t_photo.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "photo_id" })
    photo_: Photo | null;

    @ManyToOne(type => Video, t_video => t_video.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "video_id" })
    video_: Video | null;

    @ManyToOne(type => Article, t_article => t_article.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;

    @ManyToOne(type => Product, t_product => t_product.views, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product_: Product | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
