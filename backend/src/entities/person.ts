import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, OneToOne, Index } from "typeorm";
import { Address } from "./address";
import { Account } from "./account";
import { Profile_photo } from "./profile_photo";


@Entity("t_person")
// @Index("person_lower_pagename_idx", { synchronize: false })
export class Person {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: true,
        name: "photo"
    })
    photo: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Index("person_pagename_idx")
    @Column("varchar", {
        nullable: true,
        name: "pageName",
        unique: true
    })
    pageName: string | null;

    @Index('person_fullname_idx')
    @Column("varchar", {
        nullable: true,
        name: "fullName"
    })
    fullName: string | null;

    @Index('person_email_idx')
    @Column("varchar", {
        nullable: true,
        name: "email"
    })
    email: string | null;

    @Column("varchar", {
        nullable: true,
        select: false,
        name: "password"
    })
    password: string | null;

    @Column("int", {
        nullable: true,
        name: "mango_user_id"
    })
    mangoUserId: number | null;

    @Column("int", {
        nullable: true,
        name: "wallet"
    })
    wallet: number | null;

    @Column("int", {
        nullable: true,
        name: "bank_alias_id"
    })
    bankAliasId: number | null;

    @Column("varchar", {
        nullable: true,
        name: 'kyc_document_id',
    })
    kycDocumentId: string | null;

    @Column("varchar", {
        nullable: true,
        name: "verification_status",
    })
    verificationStatus: string | null;

    @Column("varchar", {
        nullable: true,
        name: "gender"
    })
    gender: string | null;

    @Column("varchar", {
        nullable: true,
        name: "birthday"
    })
    birthday: string | null;

    @Column("varchar", {
        nullable: true,
        name: "residence"
    })
    residence: string | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Column("varchar", {
        nullable: true,
        default: "deutsch",
        name: "language"
    })
    language: string | null;

    @Column("varchar", {
        nullable: true,
        default: "public",
        name: "privacy"
    })
    privacy: string | null;

    @Column("boolean", {
        nullable: false,
        default: () => false,
        name: "active"
    })
    active: boolean | null;

    @Column("varchar", {
        nullable: true,
        select: false,
        name: "token"
    })
    token: string | null;

    @Column("varchar", {
        nullable: true,
        select: false,
        name: "passtoken"
    })
    passtoken: string | null;

    @Column("int", {
        nullable: true,
        name: "sellerRating"
    })
    sellerRating: string | null;

    @Column("int", {
        nullable: true,
        name: "sellerReviews"
    })
    sellerReviews: string | null;

    @Column({
        nullable: true,
        name: "tokenAttempts"
    })
    tokenAttempts: number;

    @Column("timestamp", {
        nullable: true,
        default: () => "CURRENT_TIMESTAMP",
        name: "tokenAttemptsStamp"
    })
    tokenAttemptsStamp: Date;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deactivated"
    })
    deactivated: Date;

    @Column("varchar", {
        nullable: true,
        default: () => "NULL",
        name: "reactivationtoken"
    })
    reactivationtoken: string | null;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Account, t_account => t_account.person, { onDelete: 'CASCADE' })
    companies: Account[];

    @OneToMany(type => Address, t_address => t_address.person, { onDelete: 'SET NULL' })
    @JoinColumn()
    addresses: Address[];

    @OneToOne(type => Address, t_address => t_address.person_address, { onDelete: 'SET NULL', eager: true })
    @JoinColumn({ name: "address_id" })
    address: Address;

    @OneToOne(type => Address, t_address => t_address.person, { onDelete: 'SET NULL', eager: true })
    @JoinColumn()
    delivery_address: Address;

    @OneToMany(type => Profile_photo, t_profile_photo => t_profile_photo.user)
    profile_photos: Profile_photo[];
}
