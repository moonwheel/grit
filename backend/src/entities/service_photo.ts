import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate, Index } from "typeorm";
import { Service } from "./service";


@Entity("t_service_photo")
export class ServicePhoto {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Service, t_service => t_service.photos, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "service_id" })
    service_: Service | null;


    @Column("varchar", {
        nullable: true,
        name: "link"
    })
    link: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Index()
    @Column("int", {
        nullable: true,
        name: "index"
    })
    index: number | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

}
