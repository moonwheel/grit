import { Notification } from './notification';
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount, TreeChildren, TreeParent, TreeLevelColumn, Tree, Index } from "typeorm";
import { Person } from "./person";
import { Role } from "./role";
import { Business } from "./business";
import { Article } from "./article";
import { Bank } from "./bank";
import { Booking } from "./booking";
import { Text } from "./text";
import { Photo } from "./photo";
import { Video } from "./video";
import { Product } from "./product";
import { CartProduct } from "./cart_product";
import { Order } from "./order";
import { Service } from "./service";
import { Photo_Comment } from "./photo_comment";
import { Photo_Comment_Like } from "./photo_comment_like";
import { Article_Comment } from "./article_comment";
import { Article_Like } from "./article_like";
import { Article_Comment_Like } from "./article_comment_like";
import { Text_Comment } from "./text_comment";
import { Text_Comment_Like } from "./text_comment_like";
import { Text_Comment_Replies } from "./text_comment_reply";
import { Video_Comment } from "./video_comment";
import { Video_Comment_Replies } from "./video_comment_reply";
import { Video_Comment_Like } from "./video_comment_like";
import { Contact } from "./contact";
import { Photo_Like } from "./photo_like";
import { Share } from "./share";
import { Shop_category } from "./shop_category";
// import { Tag } from "./tag";
import { Text_Like } from "./text_like";
import { Video_Like } from "./video_like";
import { View } from "./view";
import { ProductReview } from "./product_review";
import { ServiceReview } from "./service_review";
import { ReviewPhoto } from "./review_photo";
import { SellerReview } from "./seller_review";
import { Mention } from "./mention";
import { Photo_Annotation } from "./photo_annotation";
import { Followings } from "./followings";
import { Followings_request } from "./followings_request";
import { Bookmark } from './bookmarks';
import { Photo_View } from './photo_view';
import { Text_View } from './text_view';
import { Video_View } from './video_view';
import { Article_View } from './article_view';
import { Report } from './report';
import { ChatroomUser } from './chatroom_user';
import { Message } from './message';
import { ProductReviewReplies } from './product_review_reply';
import { ServiceReviewReplies } from './service_review_reply';
import { SellerReviewReplies } from './seller_review_reply';
import { Blacklist } from './blacklist';

@Entity("t_account")
// @Entity()
// @Tree("adjacency-list")
// @Tree("closure-table")
export class Account {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    // @Index("account-person-fk-idx")
    @ManyToOne(type => Person, t_person => t_person.companies, { onDelete: "SET NULL", onUpdate: "SET NULL", eager: true })
    @JoinColumn()
    person: Person;

    // @Index("account-business-fk-idx")
    @ManyToOne(type => Business, t_business => t_business.members, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    business: Business;

    // @Index("account-role-fk-idx")
    @ManyToOne(type => Role, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    @JoinColumn({ name: "role_id" })
    role: Role;

    @Column("boolean", {
        nullable: false,
        default: () => "TRUE",
        name: "followers_notification"
    })
    followers_notification: boolean;

    @Column("boolean", {
        nullable: false,
        default: () => "TRUE",
        name: "likes_notification"
    })
    likes_notification: boolean;

    @Column("boolean", {
        nullable: false,
        default: () => "TRUE",
        name: "comments_notification"
    })
    comments_notification: boolean;

    @Column("boolean", {
        nullable: false,
        default: () => "TRUE",
        name: "tags_notification"
    })
    tags_notification: boolean;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @Index("account-deleted-idx")
    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Account, account => account.baseBusinessAccount)
    children: Account[];

    @Index("account-basebusinessaccount-fk-idx")
    @ManyToOne(type => Account, account => account.children)
    baseBusinessAccount: Account;


    // @TreeChildren()
    // children: Account[];

    // @TreeParent()
    // baseBusinesAccount: Account;


    @OneToMany(type => Message, t_message => t_message.user)
    messages: Message[];

    @OneToMany(type => ChatroomUser, t_chatroom_user => t_chatroom_user.user)
    chatroom_users: ChatroomUser[];

    @OneToMany(type => Article, t_article => t_article.user)
    articles: Article[];

    @OneToMany(type => Article_Like, t_article_like => t_article_like.user)
    article_likes: Article_Like[];

    @OneToMany(type => Bank, t_bank => t_bank.user)
    banks: Bank[];

    @OneToMany(type => Blacklist, blacklist => blacklist.owner)
    blacklist: Blacklist[]

    @OneToMany(type => Blacklist, blacklist => blacklist.blockedAccount)
    blocked: Blacklist[]

    // @ManyToMany(type => Account, t_user => t_user.blocked)
    // @JoinTable()
    // blacklist: Account[];

    // @ManyToMany(type => Account, t_user => t_user.blacklist)
    // blocked: Account[];

    @OneToMany(type => Followings, t_followings => t_followings.account)
    followings: Followings[];

    @OneToMany(type => Followings, t_followings => t_followings.followedAccount)
    followers: Followings[];

    @OneToMany(type => Followings_request, t_followings => t_followings.account)
    output_following_requests: Followings[];

    @OneToMany(type => Followings_request, t_followings => t_followings.followedAccount)
    input_following_requests: Followings[];

    // RelationCount is deprecated
    // @RelationCount((account: Account) => account.followings)
    // followingsCount: number;

    // @RelationCount((account: Account) => account.followers)
    // followersCount: number;

    @OneToMany(type => Report, t_report_account => t_report_account.target)
    reports: Report[];

    // @RelationCount((account: Account) => account.blacklist)
    // blacklistCount: number;

    // @RelationCount((account: Account) => account.blocked)
    // blockedCount: number;

    @OneToMany(type => Booking, t_booking => t_booking.seller)
    booking_seller: Booking[];

    @OneToMany(type => Order, t_booking => t_booking.buyer)
    booking_buyer: Booking[];

    // @ManyToMany(type => Account)
    // @JoinTable()
    // bookmarks_user: Account[];

    // @ManyToMany(type => Text)
    // @JoinTable()
    // bookmarks_text: Text[];

    // @ManyToMany(type => Photo)
    // @JoinTable()
    // bookmarks_photo: Photo[];

    // @ManyToMany(type => Article)
    // @JoinTable()
    // bookmarks_article: Article[];

    // @ManyToMany(type => Video)
    // @JoinTable()
    // bookmarks_video: Video[];

    // @ManyToMany(type => Product)
    // @JoinTable()
    // bookmarks_product: Product[];

    // @ManyToMany(type => Service)
    // @JoinTable()
    // bookmarks_service: Service[];


    // @ManyToMany(type => Photo, t_photo => t_photo.views)
    // // @JoinTable()
    // views_photo: Photo[];
    @OneToMany(type => Photo_View, t_photo_view => t_photo_view.user)
    views_photo: Photo_View[];

    @OneToMany(type => Text_View, t_text_view => t_text_view.user)
    views_text: Text_View[];


    @OneToMany(type => Video_View, t_video_view => t_video_view.user)
    views_video: Video_View[];


    @OneToMany(type => Article_View, t_article_view => t_article_view.user)
    views_article: Article_View[];


    @OneToMany(type => Photo_Comment, t_photo_comment => t_photo_comment.user)
    photo_comments: Photo_Comment[];

    @OneToMany(type => Photo_Comment_Like, t_photo_comment_like => t_photo_comment_like.user)
    photo_comments_likes: Photo_Comment_Like[];

    @OneToMany(type => Article_Comment, t_article_comment => t_article_comment.user)
    article_comments: Article_Comment[];

    @OneToMany(type => Photo_Comment_Like, t_article_comment_like => t_article_comment_like.user)
    article_comments_likes: Article_Comment_Like[];

    @OneToMany(type => Text_Comment, t_text_comment => t_text_comment.user)
    text_comments: Text_Comment[];

    @OneToMany(type => Text_Comment_Like, t_text_comment_like => t_text_comment_like.user)
    text_comments_likes: Text_Comment_Like[];

    @OneToMany(type => Text_Comment_Replies, t_text_comment_reply => t_text_comment_reply.user)
    text_comments_replies: Text_Comment_Replies[];

    @OneToMany(type => Video_Comment, t_video_comment => t_video_comment.user)
    video_comments: Video_Comment[];

    @OneToMany(type => Video_Comment_Like, t_video_comment_like => t_video_comment_like.user)
    video_comments_likes: Video_Comment_Like[];

    @OneToMany(type => Video_Comment_Replies, t_video_comment_reply => t_video_comment_reply.user)
    video_comments_replies: Video_Comment_Replies[];

    @OneToMany(type => Contact, t_contact => t_contact.user)
    contacts: Contact[];

    @OneToMany(type => Photo, t_photo => t_photo.user)
    photos: Photo[];

    @OneToMany(type => Photo_Like, t_photo_like => t_photo_like.user)
    photos_likes: Photo_Like[];

    @OneToMany(type => Photo_Annotation, t_photo_annotation => t_photo_annotation.target)
    annotated: Photo_Annotation;

    @OneToMany(type => Product, t_product => t_product.user)
    products: Product[];

    @OneToMany(type => CartProduct, t_cart_product => t_cart_product.user)
    cart_products: CartProduct[];

    @OneToMany(type => ReviewPhoto, t_review_photo => t_review_photo.user)
    review_photo: ReviewPhoto[];

    @OneToMany(type => ProductReview, t_product_review => t_product_review.user)
    product_reviews: ProductReview[];

    @ManyToMany(type => ProductReview)
    @JoinTable()
    product_reviews_likes: ProductReview[];

    @ManyToMany(type => ProductReviewReplies)
    @JoinTable()
    product_reviews_replies: ProductReviewReplies[];

    @ManyToMany(type => ProductReviewReplies)
    @JoinTable()
    product_reviews_replies_likes: ProductReviewReplies[];

    @OneToMany(type => ServiceReview, t_service_review => t_service_review.user)
    service_reviews: ServiceReview[];

    @ManyToMany(type => ServiceReview)
    @JoinTable()
    service_reviews_likes: ServiceReview[];

    @ManyToMany(type => ServiceReviewReplies)
    @JoinTable()
    service_reviews_replies: ServiceReviewReplies[];

    @ManyToMany(type => ServiceReviewReplies)
    @JoinTable()
    service_reviews_replies_likes: ServiceReviewReplies[];

    @OneToMany(type => SellerReview, t_seller_review => t_seller_review.user)
    seller_reviews: SellerReview[];

    @OneToMany(type => SellerReview, t_seller_review => t_seller_review.seller)
    reviews: SellerReview[];

    @ManyToMany(type => SellerReview)
    @JoinTable()
    seller_reviews_likes: SellerReview[];

    @ManyToMany(type => SellerReviewReplies)
    @JoinTable()
    seller_reviews_replies: SellerReviewReplies[];

    @ManyToMany(type => SellerReviewReplies)
    @JoinTable()
    seller_reviews_replies_likes: SellerReviewReplies[];

    @OneToMany(type => Service, t_service => t_service.user)
    services: Service[];

    @OneToMany(type => Share, t_share => t_share.user)
    shares: Share[];

    @OneToMany(type => Shop_category, t_shop_category => t_shop_category.user)
    shop_categories: Shop_category[];

    @OneToMany(type => Text, t_text => t_text.user)
    texts: Text[];

    @OneToMany(type => Text_Like, t_text_like => t_text_like.user)
    text_likes: Text_Like[];

    @OneToMany(type => Video, t_video => t_video.user)
    videos: Video[];

    @OneToMany(type => Video_Like, t_video_like => t_video_like.user)
    video_likes: Video_Like[];

    @OneToMany(type => View, t_view => t_view.user)
    views: View[];

    @OneToMany(type => Mention, t_mentions => t_mentions.target)
    @JoinTable()
    mentioned: Mention[];

    @OneToMany(type => Notification, t_notifications => t_notifications.issuer)
    notification_issuer: Notification[];

    @OneToMany(type => Notification, t_notifications => t_notifications.owner)
    notification_owner: Notification[];

    @OneToMany(type => Bookmark, t_bookmarks => t_bookmarks.account)
    bookmark_account: Bookmark[];

    @OneToMany(type => Bookmark, t_bookmarks => t_bookmarks.owner)
    bookmark_owner: Bookmark[];

    @OneToMany(type => Order, t_order => t_order.seller)
    order_seller: Order[];

    @OneToMany(type => Order, t_order => t_order.buyer)
    order_buyer: Order[];
}
