import { Column, Entity, PrimaryGeneratedColumn, Unique, ManyToOne, JoinColumn, Index } from "typeorm";
import { ProductReview } from './product_review';
import { Text } from './text';
import { Photo } from './photo';
import { Video } from './video';
import { Article } from './article';
import { Account } from './account';
import { ServiceReview } from './service_review';
import { Product } from './product';
import { Service } from './service';
// import { Order } from './order';
import { Booking } from './booking';
import { Article_Comment_Like } from './article_comment_like';
import { Article_Comment_Replies } from './article_comment_reply';
import { Article_Comment } from './article_comment';
import { Photo_Comment_Like } from './photo_comment_like';
import { Photo_Comment } from './photo_comment';
import { Text_Comment_Like } from './text_comment_like';
import { Text_Comment_Replies } from './text_comment_reply';
import { Text_Comment } from './text_comment';
import { Video_Comment_Like } from './video_comment_like';
import { Video_Comment_Replies } from './video_comment_reply';
import { Video_Comment } from './video_comment';
import { Photo_Comment_Replies } from "./photo_comment_reply";
import { Photo_Like } from "./photo_like";
import { Article_Like } from "./article_like";
import { Text_Like } from "./text_like";
import { Video_Like } from "./video_like";
import { SellerReview } from "./seller_review";
import { Product_variant } from "./product_variant";

export type NotificationAction = "follows" | "liked" | "commented" | "tagged" | "ordered" | "shipped" | "booked" | "cancelled" | 'replied' | 'mentioned' | 'tagged';
export type NotificationContentDisplayName = "text" | "photo" | "video" | "article" | "review" | "product" | "service" | "comment" | "order" | "booking" | "review_reply";
export type NotificationContentType =
"text" |
"photo" |
"video" |
"article" |
"product" |
"service" |
"seller_review" |
"product_review" |
"service_review";

@Entity("t_notifications")
@Index(['owner', 'read'])
@Index("notifications_created_desc_idx", { synchronize: false })

export class Notification {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "action"
    })
    action: NotificationAction;


    @Column("varchar", {
      nullable: true,
      name: "content_display_name"
    })
    content_display_name: NotificationContentDisplayName | null;


    @Column("varchar", {
      nullable: true,
      name: "content_type"
    })
    content_type: NotificationContentType | null;



    @ManyToOne(type => Text, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    text: Text;

    @ManyToOne(type => Photo, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    photo: Photo;

    @ManyToOne(type => Video, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    video: Video;

    @ManyToOne(type => Article, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    article: Article;

    @ManyToOne(type => Product, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    product: Product;

    @ManyToOne(type => Product_variant, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    product_variant: Product_variant;

    @ManyToOne(type => Service, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    service: Service;




    @ManyToOne(type => ProductReview, product_review => product_review.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    product_review: ProductReview;

    @ManyToOne(type => ServiceReview, service_review => service_review.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    service_review: ServiceReview;

    @ManyToOne(type => SellerReview, seller_review => seller_review.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn()
    seller_review: SellerReview;



    // @ManyToOne(type => Order, order => order.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // order: Order;

    // @ManyToOne(type => Booking, booking => booking.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // booking: Booking;

    // @ManyToOne(type => Article_Comment_Like, article_comment_like => article_comment_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // article_comment_like: Article_Comment_Like;

    // @ManyToOne(type => Article_Comment_Replies, article_comment_reply => article_comment_reply.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // article_comment_reply: Article_Comment_Replies;

    // @ManyToOne(type => Article_Comment, article_comment => article_comment.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // article_comment: Article_Comment;

    // @ManyToOne(type => Article_Like, article_like => article_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // article_like: Article_Like;

    // @ManyToOne(type => Photo_Comment_Like, photo_comment_like => photo_comment_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // photo_comment_like: Photo_Comment_Like;

    // @ManyToOne(type => Photo_Comment_Replies, photo_comment_reply => photo_comment_reply.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // photo_comment_reply: Photo_Comment_Replies;

    // @ManyToOne(type => Photo_Comment, photo_comment => photo_comment.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // photo_comment: Photo_Comment;

    // @ManyToOne(type => Photo_Like, photo_like => photo_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // photo_like: Photo_Like;

    // @ManyToOne(type => Text_Comment_Like, text_comment_like => text_comment_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // text_comment_like: Text_Comment_Like;

    // @ManyToOne(type => Text_Comment_Replies, text_comment_reply => text_comment_reply.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // text_comment_reply: Text_Comment_Replies;

    // @ManyToOne(type => Text_Comment, text_comment => text_comment.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // text_comment: Text_Comment;

    // @ManyToOne(type => Text_Like, text_like => text_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // text_like: Text_Like;

    // @ManyToOne(type => Video_Comment_Like, video_comment_like => video_comment_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // video_comment_like: Video_Comment_Like;

    // @ManyToOne(type => Video_Comment_Replies, video_comment_reply => video_comment_reply.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // video_comment_reply: Video_Comment_Replies;

    // @ManyToOne(type => Video_Comment, video_comment => video_comment.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // video_comment: Video_Comment;

    // @ManyToOne(type => Video_Like, video_like => video_like.notifications, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    // @JoinColumn()
    // video_like: Video_Like;

    @ManyToOne(type => Account, t_user => t_user.notification_issuer, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    issuer: Account | null;

    @ManyToOne(type => Account, t_user => t_user.notification_owner, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    owner: Account | null;

    @Column("boolean", {
      nullable: false,
      default: () => "FALSE",
      name: "read"
    })
    read: boolean;

    @Column("timestamp", {
      nullable: false,
      default: () => "CURRENT_TIMESTAMP",
      name: "created"
    })
    created: Date;
}
