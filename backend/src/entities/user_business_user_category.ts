import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BusinessUserCategory } from "./business_user_categoty";

@Entity("t_user_business_user_category")
export class User_BusinessUserCategory {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => BusinessUserCategory, t_business_user_category => t_business_user_category.user_business_user_categories, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "category_id" })
    category_: BusinessUserCategory | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;
}

