import { Column, Entity, PrimaryGeneratedColumn, Index, ManyToOne, JoinColumn, Unique } from "typeorm";
import { Account } from "./account";


@Entity("t_blacklist")
export class Blacklist {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.blacklist)
    @JoinColumn()
    owner: Account

    @ManyToOne(type => Account, t_user => t_user.blocked)
    @JoinColumn()
    blockedAccount: Account

    @Column("timestamp", {
      nullable: false,
      default: () => "CURRENT_TIMESTAMP",
      name: "created"
    })
    created: Date;
}
