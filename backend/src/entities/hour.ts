import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Business } from "./business";
import { Day } from "./day";


@Entity("t_hours")
@Index("hours_business_id_idx", ["business",])

export class Hour {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Business, t_business => t_business.hours)
    @JoinColumn({ name: "business_id" })
    business: Business | null;

    @ManyToOne(type => Day, { onDelete: "CASCADE", onUpdate: "CASCADE", eager: true })
    @JoinColumn({ name: "day_id" })
    day: Day | null;

    @Column("time", {
        nullable: true,
        name: "from_h"
    })
    from: string | null;


    @Column("time", {
        nullable: true,
        name: "to_h"
    })
    to: string | null;

    @CreateDateColumn({type: "timestamp"})
    created: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated: Date;

}
