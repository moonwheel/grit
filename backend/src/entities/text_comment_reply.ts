import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, RelationCount } from "typeorm";
import { Text_Comment } from "./text_comment";
import { Text_Comment_Like } from "./text_comment_like";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Text } from "./text";
import { Report } from "./report";


@Entity("t_text_comment_replies")
export class Text_Comment_Replies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Text_Comment, t_text_comment => t_text_comment.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Text_Comment;

    @ManyToOne(type => Text, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    plain_text_: Text;


    @ManyToOne(type => Account, t_user => t_user.text_comments_replies, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Text_Comment_Like, t_text_comment_like => t_text_comment_like.reply_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Text_Comment_Like[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.text_comment_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((text_replies: Text_Comment_Replies) => text_replies.likes)
    likesCount: number;

    // @OneToMany(type => Notification, t_notification => t_notification.text_comment_reply)
    // notifications: Notification[];
}
