import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { Article } from "./article";


@Entity("t_article_photo")
@Index("article_photo_article_id_idx", ["article_",])
export class Article_photo {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Article, t_article => t_article.article_photos, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;

    @Column("varchar", {
        nullable: true,
        name: "photo"
    })
    photo: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
