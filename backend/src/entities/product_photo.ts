import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { Product } from "./product";
import { Product_variant_to_photo } from './product_variant_to_product_photo';
//import {Product_variant_photo} from "./Product_variant_photo"

@Entity("t_product_photo")
@Index("product_photo_deleted_order_idx", ["deleted", "order", 'photoUrl', 'thumbUrl'])
export class Product_photo {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Product, t_product => t_product.photos, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product_: Product | null;

    @OneToOne(type => Product, t_product => t_product.cover_, { onDelete: 'CASCADE' })
    cover_for_product_: Product | null;

    @OneToMany(type => Product_variant_to_photo, variantToPhoto => variantToPhoto.photo)
    variant: Product_variant_to_photo[]

    @Column("varchar", {
        nullable: true,
        name: "photoUrl"
    })
    photoUrl: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumbUrl"
    })
    thumbUrl: string | null;

    @Column("int", {
        nullable: true,
        name: "order"
    })
    order: number | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
