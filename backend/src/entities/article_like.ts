import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { Article } from "./article";
import { Account } from "./account";
import { Notification } from "./notification";


@Entity("t_article_like")
@Index("like_user_id_idx", ["user",])
@Index("like_article_id_idx", ["article_",])
export class Article_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.article_likes)
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @ManyToOne(type => Article, t_article => t_article.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    // @OneToMany(type => Notification, t_notification => t_notification.article_like)
    // notifications: Notification[]
}
