import { BaseEntity, Column, Entity, Index,Unique, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Article } from "./article";
import { Account } from "./account";

@Entity("t_article_view")
// @Index("t_article_view_user_idx", ["user",])
// @Index("t_article_view_article_idx", ["article_",])
@Unique(["user", "article_"])
export class Article_View {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.views_article, { onDelete: "CASCADE" })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Article, t_article => t_article.views,  { onDelete: "CASCADE" })
    @JoinColumn({ name: "article_id" })
    article_: Article | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

}