import { Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Text_Like } from "./text_like";
import { Share } from "./share";
import { Hashtag } from "./hashtag";
import { Text_Comment } from "./text_comment";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Bookmark } from "./bookmarks";
import { Text_View } from "./text_view";
import { Report } from "./report";

@Entity("t_text")
@Index('text-draft-deleted-idx', ['draft', 'deleted'])
export class Text {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.texts, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("text", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @Column("bool", {
        nullable: false,
        name: "draft",
        default: () => false,
    })
    draft: boolean | null;



    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToMany(type => Text_Comment, t_text_comment => t_text_comment.plain_text_)
    comments: Text_Comment[];

    @OneToMany(type => Text_Like, t_text_like => t_text_like.text_)
    likes: Text_Like[];

    @OneToMany(type => Text_View, t_text_view => t_text_view.text_)
    views: Text_View[];


    @OneToMany(type => Share, t_share => t_share.text_)
    shares: Share[];

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.text_)
    hashtags: Hashtag[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.text)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((text: Text) => text.likes)
    likesCount: number;

    @RelationCount((text: Text) => text.bookmarks)
    bookmarksCount: number;

    @RelationCount((text: Text) => text.views)
    viewsCount: number;

    @RelationCount((text: Text) => text.comments)
    commentsCount: number;

    @OneToMany(type => Bookmark, t_text_bookmarks => t_text_bookmarks.text)
    bookmarks: Bookmark;

}
