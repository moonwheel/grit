import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Account } from "./account";
import { Product } from "./product";
import { Product_variant } from "./product_variant";

@Entity("t_cart_product")
export class CartProduct {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.cart_products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Product, t_product => t_product.cart_products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product: Product | null;

    @ManyToOne(type => Product_variant, t_product_variant => t_product_variant.cart_products, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_variant_id" })
    product_variant: Product_variant | null;

    @Column("int", {
        nullable: true,
        name: "quantity"
    })
    quantity: number | null;

    @Column("boolean", {
        nullable: false,
        default: () => "FALSE",
        name: "collected"
    })
    collected: boolean;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }
}
