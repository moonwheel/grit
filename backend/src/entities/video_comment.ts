import { Column, Entity, Index, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Video } from "./video";
import { Video_Comment_Like } from "./video_comment_like";
import { Video_Comment_Replies } from "./video_comment_reply";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Report } from "./report";

@Entity("t_video_comment")
@Index("video_comment_user_id_idx", ["user",])
export class Video_Comment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @ManyToOne(type => Account, t_user => t_user.video_comments, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Video, t_video => t_video.comments, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "video_id" })
    video_: Video | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Video_Comment_Like, t_video_comment_like => t_video_comment_like.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Video_Comment_Like[];

    @OneToMany(type => Video_Comment_Replies, t_video_comment_replies => t_video_comment_replies.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: Video_Comment_Replies[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.video_comment)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((t_video_comment: Video_Comment) => t_video_comment.likes)
    likesCount: number;

    @RelationCount((t_video_comment: Video_Comment) => t_video_comment.replies)
    repliesCount: number;

    // @OneToMany(type => Notification, t_notification => t_notification.video_comment)
    // notifications: Notification[];
}
