import { Address } from './address';
import { Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Article_photo } from "./article_photo";
import { Article_Like } from "./article_like";
import { Share } from "./share";
import { Hashtag } from "./hashtag";
import { Article_Comment } from "./article_comment";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Bookmark } from "./bookmarks";
import { Photo_View } from "./photo_view";
import { Article_View } from "./article_view";
import { Report } from "./report";

@Entity("t_article")
@Index('article-draft-deleted-idx', ['draft', 'deleted'])
export class Article {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.articles, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: true,
        name: "cover"
    })
    cover: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Column("text", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @Column("varchar", {
        nullable: true,
        name: "category"
    })
    category: string | null;

    @Column("varchar", {
        nullable: true,
        name: "location"
    })
    location: string | null;

    @ManyToOne(type => Address, addr => addr.article_location)
    @JoinColumn({ name: "business_address" })
    business_address: Address;

    @Column("boolean", {
        nullable: true,
        name: "draft"
    })
    draft: boolean | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;


    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Article_photo, t_article_photo => t_article_photo.article_)
    article_photos: Article_photo[];

    @OneToMany(type => Article_Comment, t_article_comment => t_article_comment.article_)
    comments: Article_Comment[];

    @OneToMany(type => Article_Like, t_article_like => t_article_like.article_)
    likes: Article_Like[];

    @OneToMany(type => Article_View, t_article_view => t_article_view.article_)
    views: Article_View[];

    @OneToMany(type => Bookmark, t_article_bookmarks => t_article_bookmarks.article)
    bookmarks: Bookmark;

    @OneToMany(type => Report, t_report_article => t_report_article.target)
    reports: Report[];

    @OneToMany(type => Share, t_share => t_share.article_)
    shares: Share[];

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.article_)
    hashtags: Hashtag[];

    @OneToMany(type => Mention, t_mentions => t_mentions.article)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((article: Article) => article.likes)
    likesCount: number;

    @RelationCount((article: Article) => article.bookmarks)
    bookmarksCount: number;

    @RelationCount((article: Article) => article.views)
    viewsCount: number;

    @RelationCount((article: Article) => article.comments)
    commentsCount: number;

}
