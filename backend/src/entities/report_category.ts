import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity("t_report_category")
@Unique(["name"])

export class ReportCategory {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "name"
    })
    name: string;
}