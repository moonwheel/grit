import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Account } from "./account";
import { ReportType } from "./report_type";
import { ReportCategory } from "./report_category";

type ReportStatus = "open" | "checked";

@Entity("t_report")
@Index("report_product_user_id_idx", ["user",])
@Index("report_product_target_id_idx", ["target",])
@Index("report_product_type_id_idx", ["type",])
@Index("report_product_category_id_idx", ["category",])
export class Report {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.reports, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "target_id" })
    target: number | null;

    @ManyToOne(type => Account, t_user => t_user.reports, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => ReportType, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    @JoinColumn({ name: "type_id" })
    type: ReportType | null;

    @ManyToOne(type => ReportCategory, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    @JoinColumn({ name: "category_id" })
    category: ReportCategory | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Column("varchar", {
        nullable: true,
        name: "status"
    })
    status: ReportStatus | null;

    @CreateDateColumn({type: "timestamp"})
    created: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated: Date;
}
