import {Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Service} from "./service";
import {Booking} from "./booking";

@Entity("t_booking_appointments")
@Index("booking_id_idx", ["booking",])
export class BookingAppointment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Booking, t_booking => t_booking.appointments, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "booking_id" })
    booking: Booking | number;

    @Column("timestamp", {
        nullable: true,
        name: "start"
    })
    start: Date | null;

    @Column("timestamp", {
        nullable: true,
        name: "end"
    })
    end: Date | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

}