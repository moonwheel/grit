import { Column, Entity, PrimaryGeneratedColumn, BeforeUpdate, JoinColumn, ManyToOne, OneToOne, OneToMany } from "typeorm";
import { Business } from "./business";
import { Product } from "./product";
import { Service } from "./service";
import { Photo } from "./photo";
import { Article } from "./article";
import { Video } from "./video";
import { Text } from "./text";

@Entity("t_hashtag")
export class Hashtag {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @Column("varchar", {
        nullable: false,
        name: "tag"
    })
    tag: string;

    @ManyToOne(type => Photo, t_photo => t_photo.hashtags)
    @JoinColumn()
    photo_: Photo;

    @ManyToOne(type => Video, t_video => t_video.hashtags)
    @JoinColumn()
    video_: Video;

    @ManyToOne(type => Article, t_article => t_article.hashtags)
    @JoinColumn()
    article_: Article;

    @ManyToOne(type => Text, t_text => t_text.hashtags)
    @JoinColumn()
    text_: Text;

    @ManyToOne(type => Product, t_product => t_product.hashtags)
    @JoinColumn()
    product_: Product;

    @ManyToOne(type => Service, t_product => t_product.hashtags)
    @JoinColumn()
    service_: Service;

}
