import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Article_Comment } from "./article_comment";
import { Article_Comment_Replies } from "./article_comment_reply";
import { Account } from "./account";
import { Notification } from "./notification";
import { Article } from "./article";


@Entity("t_article_comment_like")
export class Article_Comment_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.article_comments_likes)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Article_Comment, t_article_comment => t_article_comment.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Article_Comment | null;

    @ManyToOne(type => Article_Comment_Replies, t_article_comment_replies => t_article_comment_replies.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "reply_id" })
    reply_: Article_Comment_Replies | null;

    @ManyToOne(type => Article, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "article_id" })
    article_: Article;

    @Index()
    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.article_comment_like)
    // notifications: Notification[];
}
