import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Photo } from "./photo";
import { Account } from "./account";


@Entity("t_photo_annotation")
export class Photo_Annotation {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.annotated, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "target_id" })
    target: Account | null;

    @ManyToOne(type => Photo, t_photo => t_photo.annotations, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "photo_id" })
    photo: Photo | null;

    @Column("int", {
        nullable: false,
        name: "x"
    })
    x: string | null;

    @Column("int", {
        nullable: false,
        name: "y"
    })
    y: string | null;
}
