import { BaseEntity, Column,Unique, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Video } from "./video";
import { Account } from "./account";

@Entity("t_video_view")
@Index("t_video_view_user_idx", ["user",])
@Index("t_video_view_video_idx", ["video_",])
@Unique(["user", "video_"])
export class Video_View {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.views_video)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Video, t_video => t_video.views)
    @JoinColumn({ name: "video_id" })
    video_: Video | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

}