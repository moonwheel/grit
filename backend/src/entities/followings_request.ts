import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, OneToOne, ManyToOne, Unique } from "typeorm";
import { Account } from "./account";

export type RequestStatus = "pending" | 'approved' | 'rejected';

@Entity("t_followings_request")
@Unique(["account", "followedAccount"])
export class Followings_request {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_account => t_account.output_following_requests, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    account: Account;

    @ManyToOne(type => Account, t_account => t_account.input_following_requests, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    followedAccount: Account;

    @Column("varchar", {
        nullable: false,
        name: "status"
    })
    status: RequestStatus;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @Column("timestamp", {
    //     nullable: true,
    //     default: () => "NULL",
    //     name: "deleted"
    // })
    // deleted: Date;

}
