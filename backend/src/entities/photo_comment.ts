import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, RelationCount } from "typeorm";
import { Photo } from "./photo";
import { Photo_Comment_Like } from "./photo_comment_like";
import { Photo_Comment_Replies } from "./photo_comment_reply";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Report } from "./report";

@Entity("t_photo_comment")
// @Index("comment_user_id_idx", ["user",])
// @Index("comment_photo_id_idx", ["photo_",])
export class Photo_Comment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @ManyToOne(type => Account, t_user => t_user.photo_comments)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Photo, t_photo => t_photo.comments)
    @JoinColumn({ name: "photo_id" })
    photo_: Photo | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Photo_Comment_Like, t_comment_like => t_comment_like.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Photo_Comment_Like[];

    @RelationCount((photo_comment: Photo_Comment) => photo_comment.likes)
    likesCount: number;

    @OneToMany(type => Photo_Comment_Replies, t_comment_like => t_comment_like.comment_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: Photo_Comment_Replies[];

    @RelationCount((photo_comment: Photo_Comment) => photo_comment.replies)
    repliesCount: number;

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.photo_comment)
    @JoinTable()
    mentions: Mention[];

    // @OneToMany(type => Notification, t_notification => t_notification.photo_comment)
    // notifications: Notification[]
}
