import { Column, Entity, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Article_Comment } from "./article_comment";
import { Article_Comment_Like } from "./article_comment_like";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Article } from "./article";
import { Report } from "./report";


@Entity("t_article_comment_replies")
export class Article_Comment_Replies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Article_Comment, t_article_comment => t_article_comment.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Article_Comment;

    @ManyToOne(type => Account, t_user => t_user.article_comments)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @ManyToOne(type => Article, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "article_id" })
    article_: Article;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Article_Comment_Like, t_article_comment_like => t_article_comment_like.reply_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Article_Comment_Like[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.article_comment_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((t_article_replies: Article_Comment_Replies) => t_article_replies.likes)
    likesCount: number;

    // @OneToMany(type => Notification, t_notification => t_notification.article_comment_reply)
    // notifications: Notification[];
}
