import { Column, Entity, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Photo_Comment } from "./photo_comment";
import { Photo_Comment_Like } from "./photo_comment_like";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Photo } from "./photo";
import { Report } from "./report";

@Entity("t_photo_comment_replies")
export class Photo_Comment_Replies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Photo_Comment, t_photo_comment => t_photo_comment.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Photo_Comment;

    @ManyToOne(type => Account, t_user => t_user.photo_comments)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Photo, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "photo_id" })
    photo_: Photo;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Photo_Comment_Like, t_comment_like => t_comment_like.reply_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Photo_Comment_Like[];

    @RelationCount((photo_reply: Photo_Comment_Replies) => photo_reply.likes)
    likesCount: number;

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.photo_comment_reply)
    @JoinTable()
    mentions: Mention[];

    // @OneToMany(type => Notification, t_notification => t_notification.photo_comment_reply)
    // notifications: Notification[];
}
