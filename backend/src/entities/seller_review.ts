import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, ManyToMany, OneToOne, RelationCount } from "typeorm";
import { Account } from "./account";
import { ReviewPhoto } from "./review_photo";
import { ReviewVideo } from "./review_video";
import { Mention } from "./mention";
import { Report } from "./report";
import { SellerReviewReplies } from "./seller_review_reply";
import { Notification } from "./notification";

@Entity("t_seller_review")
@Index("seller_review_seller_id_idx", ["seller",])
@Index("seller_review_user_id_idx", ["user",])
export class SellerReview {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.seller_reviews, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Account, t_account => t_account.reviews, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "seller_id" })
    seller: Account | null;

    @Column("int", {
        nullable: true,
        name: "rating"
    })
    rating: number | null;

    @Column("varchar", {
        nullable: true,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToOne(type => ReviewVideo)
    @JoinColumn()
    video: ReviewVideo;

    @OneToMany(type => ReviewPhoto, t_review_photo => t_review_photo.sellerReview)
    @JoinColumn()
    photo: ReviewPhoto[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @RelationCount((seller_review: SellerReview) => seller_review.likes)
    likesCount: number;

    @OneToMany(type => Mention, t_mentions => t_mentions.sellerreview)
    @JoinTable()
    mentions: Mention[];

    @OneToMany(type => SellerReviewReplies, t_seller_review_replies => t_seller_review_replies.review_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    replies: SellerReviewReplies[];

    @OneToMany(type => Notification, t_notification => t_notification.seller_review)
    notifications: Notification[];
}
