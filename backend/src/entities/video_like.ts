import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Video } from "./video";
import { Account } from "./account";
import { Notification } from "./notification";

@Entity("t_video_like")
@Index("like_user_id_idx", ["user",])
@Index("like_video_id_idx", ["video_",])
export class Video_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.video_likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Video, t_video => t_video.likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "video_id" })
    video_: Video | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.text_like)
    // notifications: Notification
}
