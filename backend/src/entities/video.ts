import { Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, RelationCount } from "typeorm";
import { Video_Like } from "./video_like";
import { Share } from "./share";
import { Hashtag } from "./hashtag";
import { Video_Comment } from "./video_comment";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Bookmark } from "./bookmarks";
import { Video_View } from "./video_view";
import { Report } from "./report";
import { Address } from "./address";

@Entity("t_video")
@Index('video-draft-deleted-idx', ['draft', 'deleted'])
export class Video {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.videos, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: true,
        name: "video"
    })
    video: string | null;

    @Column("varchar", {
        nullable: true,
        name: "cover"
    })
    cover: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Column("varchar", {
        nullable: true,
        name: "duration"
    })
    duration: string | null;

    @Column("varchar", {
        nullable: true,
        name: "category"
    })
    category: string | null;

    @Column("varchar", {
        nullable: true,
        name: "location"
    })
    location: string | null;

    @ManyToOne(type => Address, addr => addr.video_location)
    @JoinColumn({ name: "business_address" })
    business_address: Address;

    @Column("boolean", {
        nullable: true,
        name: "draft"
    })
    draft: boolean | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToMany(type => Video_Comment, t_video_comment => t_video_comment.video_)
    comments: Video_Comment[];

    @OneToMany(type => Video_Like, t_video_like => t_video_like.video_)
    likes: Video_Like[];

    @OneToMany(type => Video_View, t_text_view => t_text_view.video_)
    views: Video_View[];

    @OneToMany(type => Bookmark,  t_video_bookmarks =>  t_video_bookmarks.video)
    bookmarks: Bookmark;


    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Share, t_share => t_share.video_)
    shares: Share[];

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.video_)
    hashtags: Hashtag[];



    @OneToMany(type => Mention, t_mentions => t_mentions.video)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((video: Video) => video.likes)
    likesCount: number;

    @RelationCount((video: Video) => video.bookmarks)
    bookmarksCount: number;

    @RelationCount((video: Video) => video.views)
    viewsCount: number;

    @RelationCount((video: Video) => video.comments)
    commentsCount: number;

}
