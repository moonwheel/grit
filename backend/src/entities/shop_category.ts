import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, Unique } from "typeorm";
import { Product } from "./product";
import { Account } from "./account";


@Entity("t_shop_category")
@Index("shop_category_product_id_idx", ["user",])
@Unique(["user", "name"])
export class Shop_category {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.shop_categories, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @Column("int", {
        nullable: true,
        name: "parent_id"
    })
    parentId: number | null;


    @Column("varchar", {
        nullable: true,
        name: "name"
    })
    name: string | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToMany(type => Product, t_product => t_product.shopcategory_)
    products: Product[];

}
