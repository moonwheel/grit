import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  BeforeUpdate,
  JoinTable,
  RelationCount,
  ManyToMany,
} from "typeorm";

import { Account } from "./account";
import { Mention } from "./mention";
import { ServiceReview } from "./service_review";
import { Service } from "./service";


@Entity("t_service_review_replies")
export class ServiceReviewReplies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => ServiceReview, t_service_review => t_service_review.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "review_id" })
    review_: ServiceReview;

    @ManyToOne(type => Service, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "service_id" })
    service_: Service;

    @ManyToOne(type => Account, t_user => t_user.service_reviews_replies, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @OneToMany(type => Mention, t_mentions => t_mentions.service_review_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((service_replies: ServiceReviewReplies) => service_replies.likes)
    likesCount: number;
}
