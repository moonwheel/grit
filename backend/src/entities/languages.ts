import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, JoinTable } from "typeorm";

@Entity("t_languages")
export class Languages {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "value"
    })
    value: string ;

    @Column("varchar", {
        nullable: false,
        name: "display_name"
    })
    display_name: string ;

    @Column("varchar", {
        nullable: true,
        name: "default"
    })
    default: boolean | null


}