import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, OneToOne, ManyToOne, Unique } from "typeorm";
import { Account } from "./account";

@Entity("t_followings")
@Unique(["account", "followedAccount"])
export class Followings {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_account => t_account.followings, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    account: Account;

    @ManyToOne(type => Account, t_account => t_account.followers, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn()
    followedAccount: Account;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    // @Column("timestamp", {
    //     nullable: true,
    //     default: () => "NULL",
    //     name: "deleted"
    // })
    // deleted: Date;

}
