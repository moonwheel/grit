import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    BeforeUpdate,
} from "typeorm";

import { Account } from "./account";
import { Address } from "./address";
import { Service } from "./service";
import { BookingAppointment } from "./booking_appointments";

import { AccountRole } from "../enums";

export type PaymentStatus = "rejected" | "paid" | "pending";
export type PaymentType = "bank" | "card";

@Entity("t_booking")
@Index("buyer_id_idx", ["buyer",])
@Index("seller_id_idx", ["seller",])
@Index("delivery_address_id_idx", ["delivery_address",])
@Index("billing_address_id_idx", ["billing_address",])
export class Booking {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("decimal", {
        name: "total_price"
    })
    total_price: number;

    @Column("decimal", {
        name: "total_time"
    })
    total_time: number;

    @Column("decimal", {
        nullable: true,
        name: "fee"
    })
    fee: number | null;

    @Column("decimal", {
        nullable: true,
        name: "refund_amount"
    })
    refund_amount: number | null;

    @Column("decimal", {
        nullable: true,
        name: "amount_card"
    })
    amount_card: number | null;

    @Column("int", {
        nullable: true,
        name: "payment_id"
    })
    payment_id: number | null;

    @Column("varchar", {
        nullable: true,
        name: "payment_status"
    })
    payment_status: PaymentStatus;

    @Column("varchar", {
        nullable: true,
        name: "payment_type"
    })
    payment_type: PaymentType;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "payment_date"
    })
    payment_date: Date | null;

    @Column("varchar", {
        nullable: true,
        name: "pay_in_id"
    })
    pay_in_id: number;

    @Column("varchar", {
        nullable: true,
        name: "pay_in_status"
    })
    pay_in_status: "success" | "failed";

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "pay_in_date"
    })
    pay_in_date: Date;

    @Column("varchar", {
        nullable: true,
        name: "pay_in_refund_status"
    })
    pay_in_refund_status: "success" | "failed";

    @Column("timestamp", {
        nullable: true,
        name: "pay_in_refund_date"
    })
    pay_in_refund_date: Date | null;

    @Column("varchar", {
        nullable: true,
        name: "pay_out_status"
    })
    pay_out_status: "success" | "failed";

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "pay_out_date"
    })
    pay_out_date: Date;

    @Column("varchar", {
        nullable: true,
        name: "pay_out_refund_status"
    })
    pay_out_refund_status: "success" | "failed" | null;

    @Column("timestamp", {
        nullable: true,
        name: "pay_out_refund_date"
    })
    pay_out_refund_date: Date | null;

    @Column("int", {
        nullable: true,
        name: "transaction"
    })
    transaction: number | null;

    @Column("int", {
        nullable: true,
        name: "transfer_id"
    })
    transfer_id: number;

    @Column("varchar", {
        nullable: true,
        name: "transfer_status"
    })
    transfer_status: "success" | "failed" | null;

    @Column("timestamp", {
        nullable: true,
        name: "transfer_date"
    })
    transfer_date: Date | null;

    @ManyToOne(type => Account, t_account => t_account.order_buyer, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "buyer_id" })
    buyer: Account;

    @ManyToOne(type => Account, t_account => t_account.order_seller, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "seller_id" })
    seller: Account;

    @ManyToOne(type => Address, t_address => t_address.delivery_address, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "delivery_address_id" })
    delivery_address: Address;

    @ManyToOne(type => Address, t_address => t_address.billing_address, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "billing_address_id" })
    billing_address: Address;

    @ManyToOne(type => Service, t_service => t_service.bookings, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "service_id" })
    service: Service | number;

    @OneToMany(type => BookingAppointment, t_booking_appointments => t_booking_appointments.booking)
    appointments: BookingAppointment[];

    @Column("varchar", {
        nullable: true,
        name: "who_cancelled"
    })
    who_cancelled: AccountRole | null;

    @Column("varchar", {
        nullable: true,
        name: "when_cancelled"
    })
    when_cancelled: 'before' | 'after' | null;

    @Column("varchar", {
        nullable: true,
        length: 255,
        name: "cancel_reason"
    })
    cancel_reason: string;

    @Column("varchar", {
        nullable: true,
        length: 500,
        name: "cancel_description"
    })
    cancel_description: string;

    @Column("timestamp", {
        nullable: true,
        name: "paid"
    })
    paid: Date;

    @Column("boolean", {
        nullable: false,
        default: () => false,
        name: "is_completed"
    })
    is_completed: boolean;

    @Column("timestamp", {
        nullable: true,
        name: "cancelled"
    })
    cancelled: Date;

    @Column("timestamp", {
        nullable: true,
        name: "refunded"
    })
    refunded: Date;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }
}
