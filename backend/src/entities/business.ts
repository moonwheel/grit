import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, Index } from "typeorm";
import { Account } from "./account";
import { Category } from "./category";
import { Hour } from "./hour";
import { Address } from "./address";
import { Product } from "./product";

@Entity("t_business")
// @Index("business_lower_pagename_idx", { synchronize: false })
export class Business {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: true,
        name: "photo"
    })
    photo: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Index("business_name_idx")
    @Column("varchar", {
        nullable: true,
        name: "name"
    })
    name: string | null;

    @Index("business_pagename_idx")
    @Column("varchar", {
        nullable: true,
        name: "pageName",
        unique: true
    })
    pageName: string | null;

    @Column("varchar", {
        nullable: true,
        name: "phone"
    })
    phone: string | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Column("varchar", {
        nullable: true,
        name: "legal_notice"
    })
    legal_notice: string | null;

    @Column("varchar", {
        nullable: true,
        name: "privacy"
    })
    privacy: string | null;

    @Column("int", {
        nullable: true,
        name: "mango_user_id"
    })
    mangoUserId: number | null;

    @Column("int", {
        nullable: true,
        name: "wallet"
    })
    wallet: number | null;

    @Column("int", {
        nullable: true,
        name: "bank_alias_id"
    })
    bankAliasId: number | null;

    @Column("varchar", {
        nullable: true,
        name: 'kyc_document_id',
    })
    kycDocumentId: string | null

    @Column("varchar", {
        nullable: true,
        name: "verification_status",
    })
    verificationStatus: string | null;

    @CreateDateColumn({type: "timestamp"})
    created: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated: Date;

    @Index()
    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @Column("varchar", {
        nullable: true,
        default: () => "NULL",
        name: "reactivationtoken"
    })
    reactivationtoken: string | null;

    @OneToMany(type => Account, t_account => t_account.business, { onDelete: 'CASCADE' })
    @JoinColumn()
    members: Account[];

    @OneToMany(type => Product, t_product => t_product.business_)
    @JoinColumn()
    products: Product[];

    @ManyToOne(type => Category, { onDelete: 'RESTRICT'})
    @JoinColumn()
    category: Category;

    @OneToMany(type => Hour, t_hours => t_hours.business)
    hours: Hour[];

    @OneToMany(type => Address, t_address => t_address.business, {onDelete: 'SET NULL'})
    @JoinColumn()
    addresses: Address[];

    @OneToOne(type => Address, t_address => t_address.business, { onDelete: 'SET NULL', eager: true })
    @JoinColumn()
    delivery_address: Address;

    @OneToOne(type => Address, t_address => t_address.business, { onDelete: 'SET NULL', eager: true })
    @JoinColumn()
    address: Address;

}
