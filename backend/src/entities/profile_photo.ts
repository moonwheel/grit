import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Person } from "./person";


@Entity("t_profile_photo")
@Index("profile_photo_user_id_idx", ["user",])
export class Profile_photo {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Person, t_user => t_user.profile_photos,{ onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Person | null;

    @Column("varchar", {
        nullable: true,
        name: "photoUrl"
    })
    photoUrl: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumbUrl"
    })
    thumbUrl: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
