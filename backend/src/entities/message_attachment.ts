import { Column, Entity, Index, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Chatroom } from "./chatroom";
import { Account } from "./account";
import { Message } from "./message";
import { App } from "../config/app.config";

export type AttachmentType = "photo" | "video" | "document";
@Entity("t_message_attachment")
// @Index("booking_service_id_idx", ["service_",])
// @Index("booking_user_id_idx", ["user",])
export class MessageAttachment {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: true,
        name: "link"
    })
    link: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Column("varchar", {
        nullable: true,
        name: "duration"
    })
    duration: string | null;

    @Column("varchar", {
        nullable: true,
        name: "file_name"
    })
    file_name: string | null;

    @Index()
    @Column("varchar", {
        nullable: true,
        name: "type"
    })
    type: AttachmentType;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToOne(type => Message, t_message => t_message.attachment,  { onDelete: 'CASCADE' })
    @JoinColumn({ name: "message_id" })
    message: Message

    @ManyToOne(type => Chatroom, t_chatroom => t_chatroom.chatroom_users, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "chatroom_id" })
    chatroom: Chatroom;

    @ManyToOne(type => Account, t_user => t_user.messages, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account;

}
