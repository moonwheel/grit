import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("t_review_video")
export class ReviewVideo {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: true,
        name: "link"
    })
    link: string | null;

    @Column("varchar", {
        nullable: true,
        name: "cover"
    })
    cover: string | null;

    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Column("varchar", {
        nullable: true,
        name: "duration"
    })
    duration: string | null;

}
