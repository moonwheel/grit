import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Account } from "./account";
import { Chatroom } from "./chatroom";
import { Message } from "./message";

@Entity("t_chatroom_user")
@Index(["chatroom", "user"], {unique: true})
@Index(['deleted', 'is_blocked'])
export class ChatroomUser {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("boolean", {
        nullable: false,
        default: () => "FALSE",
        name: "is_muted"
    })
    is_muted: boolean;

    @Column("boolean", {
        nullable: false,
        default: () => "FALSE",
        name: "is_blocked"
    })
    is_blocked: boolean;

    @Index('chatroom_user_created_idx')
    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @ManyToOne(type => Chatroom, t_chatroom => t_chatroom.chatroom_users, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "chatroom_id" })
    chatroom: Chatroom;

    @ManyToOne(type => Account, t_user => t_user.chatroom_users,  { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Message)
    @JoinColumn({ name: "last_read_message" })
    last_read_message: Message | null;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

}
