import { BaseEntity, Column, Entity,Unique, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Photo } from "./photo";
import { Account } from "./account";

@Entity("t_photo_view")
@Index("t_photo_view_user_idx", ["user",])
@Index("t_photo_view_photo_idx", ["photo_",])
@Unique(["user", "photo_"])
export class Photo_View {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.views_photo)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Photo, t_photo => t_photo.views)
    @JoinColumn({ name: "photo_id" })
    photo_: Photo | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

}
