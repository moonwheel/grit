import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Text } from "./text";
import { Account } from "./account";
import { Notification } from "./notification";


@Entity("t_text_like")
@Index("like_user_id_idx", ["user",])
@Index("like_text_id_idx", ["text_",])
export class Text_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.text_likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @ManyToOne(type => Text, t_text => t_text.likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "text_id" })
    text_: Text | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.text_like)
    // notifications: Notification[]
}
