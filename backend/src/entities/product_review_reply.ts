import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  BeforeUpdate,
  JoinTable,
  RelationCount,
  ManyToMany,
  Index,
} from "typeorm";

import { ProductReview } from "./product_review";
import { Account } from "./account";
import { Mention } from "./mention";
import { Product } from "./product";


@Entity("t_product_review_replies")
export class ProductReviewReplies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => ProductReview, t_product_review => t_product_review.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "review_id" })
    review_: ProductReview;

    @ManyToOne(type => Product, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product_: Product;

    @ManyToOne(type => Account, t_user => t_user.product_reviews_replies, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @Index()
    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @ManyToMany(type => Account)
    @JoinTable()
    likes: Account[];

    @OneToMany(type => Mention, t_mentions => t_mentions.product_review_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((product_replies: ProductReviewReplies) => product_replies.likes)
    likesCount: number;
}
