import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Product } from "./product";
import { Product_variant_to_photo } from './product_variant_to_product_photo';
import {CartProduct} from "./cart_product";
// import { Product_photo } from "./product_photo";


@Entity("t_product_variant")
@Index("product_variant_product_id_idx", ["product_",])
export class Product_variant {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Product, t_product => t_product.variants, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "product_id" })
    product_: Product | null;

    @OneToMany(type => Product_variant_to_photo, variantToPhoto => variantToPhoto.variant, {onDelete: "CASCADE", onUpdate: "RESTRICT"})
    // @JoinTable()
    photos: Product_variant_to_photo[];

    // @Column("varchar", {
    //     nullable: true,
    //     name: "photo"
    // })
    // photo: string | null;
    @Index('pv_size_idx')
    @Column("varchar", {
        nullable: true,
        name: "size"
    })
    size: string | null;

    @Index('pv_color_idx')
    @Column("varchar", {
        nullable: true,
        name: "color"
    })
    color: string | null;

    @Index('pv_flavor_idx')
    @Column("varchar", {
        nullable: true,
        name: "flavor"
    })
    flavor: string | null;

    @Index('material')
    @Column("varchar", {
        nullable: true,
        name: "material"
    })
    material: string | null;

    @Index('pv_price_idx')
    @Column("decimal", {
        nullable: true,
        name: "price"
    })
    price: number | null;

    @Column("int", {
        nullable: true,
        name: "quantity"
    })
    quantity: number | null;

    @Index('pv_enable_idx')
    @Column("boolean", {
        nullable: true,
        name: "enable"
    })
    enable: boolean | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Index()
    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    // @OneToMany(type=>Product_variant_photo, t_product_variant_photo=>t_product_variant_photo.product_photo_,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    // photos:Product_variant_photo[];

    @OneToMany(type => CartProduct, t_cart_product => t_cart_product.product_variant)
    cart_products: CartProduct[];

    @OneToMany(type => CartProduct, t_order_item => t_order_item.product_variant)
    order_items: CartProduct[];
}
