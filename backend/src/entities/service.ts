import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, RelationCount, JoinTable } from "typeorm";
import { Booking } from "./booking";
import { Service_schedule } from "./service_schedule";
import { ServicePhoto } from "./service_photo";
import { Account } from "./account";
import { ServiceReview } from "./service_review";
import { Address } from "./address";
import { Hashtag } from "./hashtag";
import { Notification } from "./notification";
import { Bookmark } from "./bookmarks";
import { Report } from "./report";
import { Mention } from "./mention";

@Entity("t_service")
@Index("service_bank_id_idx", ["bank_id",])
@Index('service-draft-deleted-idx', ['active', 'draft', 'deleted'])
export class Service {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.services, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("boolean", {
        nullable: true,
        name: "active",
        default: true,
    })
    active: boolean | null;

    @Column("boolean", {
        nullable: true,
        name: "draft",
        default: false,
    })
    draft: boolean | null;

    @OneToMany(type => Bookmark, t_service_bookmarks => t_service_bookmarks.service)
    bookmarks: Bookmark;

    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Column("decimal", {
        nullable: true,
        name: "price"
    })
    price: number | null;

    @Column("int", {
        nullable: true,
        name: "quantity"
    })
    quantity: number | null;

    @Column("boolean", {
        nullable: true,
        name: "hourlyPrice"
    })
    hourlyPrice: boolean | null;

    @Column("varchar", {
        nullable: true,
        name: "description"
    })
    description: string | null;

    @Column("varchar", {
        nullable: true,
        name: "performance"
    })
    performance: string | null;

    @Column("varchar", {
        nullable: true,
        name: "category"
    })
    category: string | null;

    @Column("int", {
        nullable: true,
        name: "bank_id"
    })
    bank_id: number | null;

    @CreateDateColumn({type: "timestamp"})
    created: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated: Date;

    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

    @OneToMany(type => Booking, t_booking => t_booking.service)
    bookings: Booking[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => ServiceReview, t_service_review => t_service_review.service)
    reviews: ServiceReview[];

    @RelationCount((service: Service) => service.reviews)
    reviewsCount: number;

    @RelationCount((service: Service) => service.bookmarks)
    bookmarksCount: number;

    @OneToOne(type => Service_schedule, t_service_schedule => t_service_schedule.service_)
    schedule: Service_schedule;

    @OneToMany(type => ServicePhoto, t_service_photo => t_service_photo.service_)
    photos: ServicePhoto[];

    @ManyToOne(type => Address, t_address => t_address.services, { onDelete: 'SET NULL' })
    @JoinColumn({ name: "address_id" })
    address: Address | null;

    @OneToMany(type => Hashtag, t_hashtag => t_hashtag.service_)
    hashtags: Hashtag[];

    @OneToMany(type => Mention, t_mentions => t_mentions.service)
    @JoinTable()
    mentions: Mention[];
}
