import { Column, Entity, Index, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Service } from "./service";
import { ScheduleByDate } from "./schedule_by_date";
import { ScheduleByDays } from "./schedule_by_days";

@Entity("t_service_schedule")
@Index("service_schedule_service_id_idx", ["service_",])
export class Service_schedule {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @OneToOne(type => Service, t_service => t_service.schedule, { onDelete: "CASCADE", onUpdate: "CASCADE" })
    @JoinColumn({ name: "service_id" })
    service_: Service | null;

    @Column("string", {
        nullable: true,
        name: "duration"
    })
    duration: string | null;

    @Column("string", {
        nullable: true,
        name: "break"
    })
    break: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @Column("timestamp", {
        nullable: true,
        name: "deleted"
    })
    deleted: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => ScheduleByDate, t_schedule_date => t_schedule_date.schedule, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    @JoinColumn()
    byDate: ScheduleByDate[];

    @OneToMany(type => ScheduleByDays, t_schedule_days => t_schedule_days.schedule, { onDelete: "RESTRICT", onUpdate: "RESTRICT", eager: true })
    @JoinColumn()
    byDays: ScheduleByDays[];
}
