import { Entity, Index, ManyToOne, Unique, PrimaryColumn } from "typeorm";
import { Product_variant } from './product_variant';
import { Product_photo } from './product_photo';
import { Product } from './product';

@Entity("t_product_variant_t_product_photos")
// @Index("product_variant_t_product_photos_idx", ["variant", photo])
@Unique(["variantId", "photoId", "order"])
export class Product_variant_to_photo {
    @PrimaryColumn()
    public variantId!: number;

    @PrimaryColumn()
    public photoId!: number;

    @PrimaryColumn()
    public order!: number;

    @PrimaryColumn()
    public productId!: number;

    @ManyToOne(type => Product_variant, variant => variant.photos, { onDelete: 'CASCADE' })
    public variant!: Product_variant;

    @ManyToOne(type => Product_photo, photo => photo.variant, { onDelete: 'CASCADE' })
    public photo!: Product_photo;
}