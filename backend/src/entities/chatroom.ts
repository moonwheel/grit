import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { ChatroomUser } from "./chatroom_user";
import { MessageAttachment } from "./message_attachment";
import { Message } from "./message";

@Entity("t_chatroom")
// @Index("booking_service_id_idx", ["service_",])
// @Index("booking_user_id_idx", ["user",])
export class Chatroom {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Index('chatroom_title_idx')
    @Column("varchar", {
        nullable: true,
        name: "title"
    })
    title: string | null;

    @Column("varchar", {
        nullable: true,
        name: "cover"
    })
    cover: string | null;

    @Column("varchar", {
        nullable: true,
        name: "thumb"
    })
    thumb: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

    @OneToMany(type => ChatroomUser, t_chatroom_user => t_chatroom_user.chatroom)
    chatroom_users: ChatroomUser[];

    @OneToMany(type => Message, t_message => t_message.chatroom)
    messages: Message[];

    @OneToMany(type => MessageAttachment, t_attachment => t_attachment.chatroom)
    attachments: MessageAttachment[];

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }
}
