import { Column, Entity, Unique, PrimaryGeneratedColumn } from "typeorm";

@Entity("t_search_dictionary")
@Unique(["word", "type"])
export class SearchDictionary {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "word"
    })
    word: string;


    @Column("varchar", {
        nullable: false,
        name: "type"
    })
    type: "color" | "flavor" | "material";
}
