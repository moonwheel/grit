import {Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BeforeUpdate, OneToMany} from "typeorm";
import { Account } from "./account";
import {Order} from "./order";

@Entity("t_bank")
@Index("bank_user_id_idx", ["user",])
export class Bank {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.banks, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @Column("varchar", {
        nullable: true,
        name: "holder"
    })
    holder: string | null;


    @Column("varchar", {
        nullable: true,
        name: "iban"
    })
    iban: string | null;


    @Column("varchar", {
        nullable: true,
        name: "bic"
    })
    bic: string | null;


    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "updated"
    })
    updated: Date;

    // @OneToMany(type => Order, t_order => t_order.bank)
    // orders: Order[];

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @Column("timestamp", {
        nullable: true,
        default: () => "NULL",
        name: "deleted"
    })
    deleted: Date;

}
