import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, BeforeUpdate, JoinTable, RelationCount } from "typeorm";
import { Video_Comment } from "./video_comment";
import { Video_Comment_Like } from "./video_comment_like";
import { Account } from "./account";
import { Mention } from "./mention";
import { Notification } from "./notification";
import { Video } from "./video";
import { Report } from "./report";

@Entity("t_video_comment_replies")
export class Video_Comment_Replies {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Video_Comment, t_video_comment => t_video_comment.replies, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Video_Comment;

    @ManyToOne(type => Video, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "video_id" })
    video_: Video;

    @ManyToOne(type => Account, t_user => t_user.video_comments_replies, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @Column("varchar", {
        nullable: false,
        name: "text"
    })
    text: string | null;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    @OneToMany(type => Video_Comment_Like, t_video_comment_like => t_video_comment_like.reply_, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    likes: Video_Comment_Like[];

    @OneToMany(type => Report, t_report => t_report.target)
    reports: Report[];

    @OneToMany(type => Mention, t_mentions => t_mentions.video_comment_reply)
    @JoinTable()
    mentions: Mention[];

    @RelationCount((t_video_comment_replies: Video_Comment_Replies) => t_video_comment_replies.likes)
    likesCount: number;

    // @OneToMany(type => Notification, t_notification => t_notification.video_comment_reply)
    // notifications: Notification[];
}
