import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity("t_days")
@Unique(["day"])
export class Day {
    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @Column("varchar", {
        nullable: false,
        name: "day"
    })
    day: string;
}
