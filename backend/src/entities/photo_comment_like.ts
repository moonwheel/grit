import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, BeforeUpdate } from "typeorm";
import { Photo_Comment } from "./photo_comment";
import { Photo_Comment_Replies } from "./photo_comment_reply";
import { Account } from "./account";
import { Notification } from "./notification";
import { Photo } from "./photo";

@Entity("t_photo_comment_like")
export class Photo_Comment_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;

    @ManyToOne(type => Account, t_user => t_user.photo_comments_likes)
    @JoinColumn({ name: "user_id" })
    user: Account | null;

    @ManyToOne(type => Photo_Comment, t_photo_comment => t_photo_comment.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Photo_Comment | null;

    @ManyToOne(type => Photo_Comment_Replies, t_photo_comment => t_photo_comment.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "reply_id" })
    reply_: Photo_Comment_Replies | null;

    @ManyToOne(type => Photo, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "photo_id" })
    photo_: Photo;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;

    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.photo_comment_like)
    // notifications: Notification[];


}
