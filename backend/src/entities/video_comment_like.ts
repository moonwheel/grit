import { Column, Entity, JoinColumn, ManyToOne,OneToMany, PrimaryGeneratedColumn, BeforeUpdate } from "typeorm";
import { Video_Comment } from "./video_comment";
import { Video_Comment_Replies } from "./video_comment_reply";
import { Account } from "./account";
import { Notification } from "./notification";
import { Video } from "./video";

@Entity("t_video_comment_like")
export class Video_Comment_Like {

    @PrimaryGeneratedColumn()
    @Column("int", {
        nullable: false,
        primary: true,
        name: "id"
    })
    id: number;


    @ManyToOne(type => Account, t_user => t_user.video_comments_likes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "user_id" })
    user: Account | null;


    @ManyToOne(type => Video_Comment, t_video_comment => t_video_comment.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "comment_id" })
    comment_: Video_Comment | null;

    @ManyToOne(type => Video_Comment_Replies, t_video_comment_replies => t_video_comment_replies.likes, { onDelete: "CASCADE", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "reply_id" })
    reply_: Video_Comment_Replies | null;

    @ManyToOne(type => Video, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "video_id" })
    video_: Video;

    @Column("timestamp", {
        nullable: false,
        default: () => "CURRENT_TIMESTAMP",
        name: "created"
    })
    created: Date;


    @Column("timestamp", {
        nullable: true,
        name: "updated"
    })
    updated: Date | null;

    @BeforeUpdate()
    updateDates() {
        this.updated = new Date();
    }

    // @OneToMany(type => Notification, t_notification => t_notification.video_comment_like)
    // notifications: Notification[];
}
