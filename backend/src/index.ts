require("dotenv").config();
import { App } from "./config/app.config";

import express = require("express");
import http = require('http');
import compression = require('compression');
// import cluster = require('cluster');
import * as cluster from 'cluster'

import cors = require("cors");
import bodyParser = require("body-parser");
import passport = require("passport");
import * as fs from 'fs';
import * as EventEmitter from 'events';

import { createConnection, getConnection } from "typeorm";

import elasticsearchRouter = require("./services/search/router");
import discoverRouter = require("./services/discover/router");
import userRouter = require("./services/user/router");
import photoRouter = require("./services/photo/router");
import addressRouter = require("./services/address/router");
import viewRouter = require("./services/address/router");
import businessCategoryRouter = require("./services/businessUserCategory/router");
import textRouter = require("./services/text/router");
import videoRouter = require("./services/video/router");
import articleRouter = require("./services/article/router");
import productRouter = require("./services/product/router");
import reportRouter = require("./services/report/router");
import shopCategoryRouter = require("./services/shopCategory/router");
import serviceRouter = require("./services/service/router");
import bookMarksRouter = require("./services/bookmarks/router");
import businessRouter = require("./services/business/router");
import notificationRouter = require("./services/notifications/router");
import cartRouter = require("./services/cart/router");
import paymentsRouter = require("./services/payments/router");
import bookingRouter = require("./services/booking/router");
import orderRouter = require("./services/order/router");
import languagesRouter = require("./services/languages/router");
import messagingRouter = require("./services/messaging/router");
import hooksRouter = require("./services/hooks/router");

import passportStrategy = require("./passport");
import MinioInstance from './utils/minio';
import { ReportController } from "./services/report/controller";
import { CompletelyRemoveUser } from './jobs/remove-user';
import { ChangeAppointmentStatus } from './jobs/change-appointment-status';
import { ServerEvent, subscribe } from "./utils/sse";
import { initWebsocketServer } from "./utils/websocket-server";

import axios from 'axios';
import { Response } from "express";

interface NeedSSEWorkerMsg {
    type: string
    data: {
        stringData: string
        resWrite: Function
        resFlushHeaders: Function
        userId: string | number
    }
}


const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    const emitter = new EventEmitter();
    console.log(`Master ${process.pid} is running`);
    console.log('App.NATS_TOKEN', App.NATS_TOKEN)
    console.log('App.NATS_URL', App.NATS_URL)
    console.log('App.APM_SERVICE_NAME', App.APM_SERVICE_NAME)
    console.log('App.APM_SERVER_URL', App.APM_SERVER_URL)
    console.log('process.env.IS_LOCALHOST: ', process.env.IS_LOCALHOST )
    console.log('process.env.IS_LOCALHOST: ', !!process.env.IS_LOCALHOST )
    console.log('process.env.NODE_ENV: ', process.env.NODE_ENV )

     // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        var worker = cluster.fork();
        worker.on('message', function(message: NeedSSEWorkerMsg) {
            // console.log('cluster message.type', message.type)
            // console.log('cluster message.data', message.data)

            const workerPid = this.process.pid;
            // console.log('message from workerPid ', workerPid)
            // console.log('this', this)

            if (message.type === 'createListenerSSE') {
                var onEvent = (event: ServerEvent) => {
                    if (+event.userId === +message.data.userId) {
                        // res.write('retry: 500\n');

                        // setTimeout(() => {
                            this.send({ type: 'masterToWorkerSSE', data: { ...event }});
                        // }, 5000);
                    }
                }

                emitter.on('event', onEvent);
            } else if (message.type === 'emitSSE') {
                // console.log('emitSSE', message.data)
                emitter.emit('event', message.data);
            } else if (message.type === 'closeSSE') {
                if (emitter.eventNames().indexOf('event') !== -1) {
                    emitter.removeAllListeners('event');
                }
            }
        })
    }




    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    // if(!process.env.IS_LOCALHOST) {

      // wait 3 mins for APM server successfully bootstraped
      const apm = require('elastic-apm-node').start({
          // Override service name from package.json
          // Allowed characters: a-z, A-Z, 0-9, -, _, and space
          serviceName: App.APM_SERVICE_NAME,

          // Use if APM Server requires a token
          // secretToken: '',

          // Set custom APM Server URL (default: http://localhost:8200)
          serverUrl: App.APM_SERVER_URL,
      })

    // }
    createConnection()
        .then(async () => {
            const connection = getConnection();

            console.log('connection.options:', connection.options)
            const app = express();
            // app.use(compression());
            app.use(/\/((?!events).)*/, compression()); //exclude SSE route /events from applying compression due to it is not working


            var server = new http.Server(app);

            // MinioInstance.createBucket();
            if (!fs.existsSync(App.UPLOAD_FOLDER)){
                fs.mkdirSync(App.UPLOAD_FOLDER);
            }

            initWebsocketServer(server)

            // const elasticSuggestMapping = {
            //     // "mappings": {
            //         "properties" : {
            //             // "title" : {
            //             //     "type" : "completion"
            //             // },
            //             "hashtags" : {
            //                 "type" : "completion"
            //             }
            //         }
            //     // }
            //     // "mappings": {
            //     //     "properties": {
            //     //       "title": {
            //     //         "type": "text",
            //     //         "fields": {
            //     //           "trigram": {
            //     //             "type": "text",
            //     //             "analyzer": "trigram"
            //     //           },
            //     //           "reverse": {
            //     //             "type": "text",
            //     //             "analyzer": "reverse"
            //     //           }
            //     //         }
            //     //       }
            //     //     }
            //     //   }
            // }
            // try {

            //     await axios.put(`${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/typeahead`)
            //     const result = await axios({
            //         method: 'put',
            //         url: `${App.ELASTIC_HOST}:${App.ELASTIC_PORT}/typeahead/locality/_mapping`,
            //         // url: `http://localhost:9200/grit`,
            //         headers: { "Content-Type": "application/json" },
            //         data: elasticSuggestMapping
            //         // data: query1
            //     })
            //     console.log('result', result.data)
            // } catch (error) {
            //     console.log('elasticSuggestMapping error', error)
            // }

            app.use(cors());

            app.use(bodyParser.raw({ limit: App.FILE_SIZE_LIMIT + "mb" }));
            app.use(bodyParser.json({ limit: App.FILE_SIZE_LIMIT + "mb" }));
            app.use(bodyParser.urlencoded({ extended: false }));

            app.use(passport.initialize());
            passportStrategy.inititialize();

            CompletelyRemoveUser.scheduleRemovingForAllDeactivatedPersons();
            CompletelyRemoveUser.scheduleRemovingForAllDeactivatedBusiness();
            // Pay money for booking and change status
            // ChangeAppointmentStatus.changeStatus();

            app.use("/upload", express.static('upload'))

            app.use("/events", passport.authenticate('jwt', { session: false }), subscribe);
            app.use("/search", elasticsearchRouter);
            app.use("/discover", discoverRouter);
            app.use("/user", userRouter);
            app.use("/photo", photoRouter);
            app.use("/address", addressRouter);
            app.use("/view", viewRouter);
            app.use("/businesscategory", businessCategoryRouter);
            app.use("/text", textRouter);
            app.use("/notification", notificationRouter);
            app.use("/video", videoRouter);
            app.use("/article", articleRouter);
            app.use("/product", productRouter);
            app.use("/report", reportRouter);
            app.use("/shopcategory", shopCategoryRouter);
            app.use("/service", serviceRouter);
            app.use("/bookmark", bookMarksRouter);
            app.use("/business", businessRouter);
            app.use("/cart", cartRouter);
            app.use("/languages", languagesRouter);
            app.use("/messaging", messagingRouter)
            app.use("/payments", paymentsRouter);
            app.use("/bookings", bookingRouter);
            app.use("/orders", orderRouter);
            app.use("/hooks", hooksRouter);
            app.post("/utility/drop", async (req, res) => {
                if(req.body.password === 'jdhfjsihgsdjgshdghsdkgnlsdglsdjsdlkjg') {
                    const connection = getConnection();
                    const queryRunner = connection.createQueryRunner();
                    try {
                        const result = await queryRunner.dropDatabase("grit");
                        const hasDatabase = await queryRunner.hasDatabase("grit");
                        res.status(200).send({result, hasDatabase})
                    } catch (error) {
                        res.status(500).send(error)
                    }
                }
                else {
                    res.status(400).send('wrong password')
                }
            });
            app.get('/', (req, res) => res.status(200).send('Hello world'));
            server.listen(App.PORT, () => {
                console.log(`server started at http://localhost:${App.PORT}`);
            });
        })
        .catch(error => console.log(error));
}

