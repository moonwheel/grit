export interface Filter {
    by: string;
    order: "DESC" | "ASC";
}