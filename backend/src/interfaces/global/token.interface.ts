export interface Token {
    id?: number;
    person?: number;
    business?: number;
    role?: number;
    identifier?: string | number;
    expires: number;
    type: string;
    deactivated?: Date | null
    reactivationtoken?: string
}