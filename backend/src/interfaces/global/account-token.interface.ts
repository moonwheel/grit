export interface AccountToken {
  id: number;
  person: number;
  business: number;
  role: number;
  wallet: number;
  mangoUserId: number;
  baseBusinessAccount: number;
  deactivated?: Date | null
  reactivationtoken?: string
}