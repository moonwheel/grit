export interface Permissions {
  canRead: Boolean;
  canWrite: Boolean;
  canManage: Boolean;
}