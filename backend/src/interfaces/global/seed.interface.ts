import { EntitySchema } from "typeorm";

export interface Seed {
    entity: Function;
    data: Array<Record<string, any>>;
}