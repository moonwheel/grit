export interface ReportInterface {
    id?: number|string;
    user?:  number|string;
    target?:  number|string;
    type?: any;
    category?: any;
    description?: string;
    status?: string;
    reportType?: string;
    report?: string;
}