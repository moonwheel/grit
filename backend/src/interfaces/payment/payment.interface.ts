export interface ISeller {
    sellerid: string;
    businessmangopayid: string | null;
    totalsum: string;
    personmangopayid: string;
    personwalletid: string;
    businesswalletid: string | null;
}

export interface IBuyer {
    walletId: number;
    mangoUserId: number;
}

export interface IMangoUserData {
    walletId: number;
    mangoUserId: number;
    name: string;
    bankAliasId: number;
    accountType: string;
}