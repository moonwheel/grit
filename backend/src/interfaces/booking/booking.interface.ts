export interface BookingInterface {
  id: number;
  type: 'order' | 'booking';

  total_price: number;
  shipping_price: number;
  fee: number;
  refund_amount: number;
  amount_card: number;

  payment_id: number;
  payment_status: string;
  payment_type: string;
  payment_date: Date;

  pay_in_status: string;
  pay_in_date: Date;
  pay_in_refund_status: string;
  pay_in_refund_date: Date;

  pay_out_status: string;
  pay_out_date: Date;
  pay_out_refund_status: string;
  pay_out_refund_date: Date;

  tranfer_status: string;
  transfer_date: Date;

  transaction: any;

  created: Date;
  updated: Date;
  paid: Date;
  cancelled: Date;
  refunded: Date;
  shipped: Date;
  received: Date;

  who_cancelled: 'buyer' | 'seller';
  cancel_reason: string;
  cancel_description: string;

  seller: any;
  buyer: any;

  service: any;

  delivery_address: any;
  billing_address: any;
}