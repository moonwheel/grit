export interface ReviewInterface {
    id?: number;
    rating?: number;
    text?: string;
    user?: number;
    product_?: number;
    photos?: Array<any>;
    replyTo: number | string;
    itemId?: number;
}