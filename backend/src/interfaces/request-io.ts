// import { socketIo } from '@types/socket.io';
import { Request } from "express"
import { Server, Socket } from "socket.io"

export interface ReqIO extends Request {
  io: Server
}