type CardType = "CB_VISA_MASTERCARD";

export interface IOpts {
    baseURL: string;
    timeout: number;
    auth: {
        username: string;
        password: string;
    }
}

export interface INaturalUser {
    address?: {
        addressLine1: string,
        addressLine2: string,
        city: string,
        region: string,
        postalCode: string,
        country: string
    },
    firstName: string;
    lastName: string;
    birthday?: number;
    nationality: string;
    countryOfResidence: string;
    email: string;
}

export interface INaturalUserResponse {
    Address?: {
        AddressLine1: string,
        AddressLine2: string,
        City: string,
        Region: string,
        PostalCode: string,
        Country: string
    },
    FirstName: string;
    LastName: string;
    Birthday: number;
    Nationality: string;
    CountryOfResidence: string;
    Occupation?: null;
    IncomeRange?: null;
    ProofOfIdentity?: null;
    ProofOfAddress?: null;
    Capacity: string;
    PersonType?: string;
    Email: string;
    KYCLevel: string;
    Id: string; // User's id
    Tag: string;
    CreationDate?: number;
}

export interface IWalletResponse {
    Description: string,
    Owners: Array<string[]>
    Balance: {
        Currency: string;
        Amount: number;
    },
    Currency: string;
    FundsType: string;
    Id: string;
    Tag: string;
    CreationDate: number;
}

export interface IBankAccountResponse {
    OwnerAddress?: {
        AddressLine1: string;
        AddressLine2: string;
        City: string;
        Region: string;
        PostalCode: string;
        Country: string;
    },
    IBAN: string;
    BIC: string;
    UserId: string;
    OwnerName: string;
    Type: string;
    Id: string;
    Tag: string;
    CreationDate: number;
    Active: true;
}

export interface IKycDocument {
    Type: string;
    UserId: string;
    Id: number; // Id for Kyc Page
    Tag: string;
    CreationDate: number;
    ProcessedDate: null;
    Status: string;
    RefusedReasonType: null;
    RefusedReasonMessage: null
}

export interface ISubmitedValidation {
    Type: string;
    UserId: string;
    Id: string;
    Tag: string;
    CreationDate: number;
    ProcessedDate: null;
    Status: string;
    RefusedReasonType: null;
    RefusedReasonMessage: null
}

export interface ICardRegistrationResponse {
    Id: string; // Registration id
    Tag: null;
    CreationDate: number;
    UserId: string;
    AccessKey: string;
    PreregistrationData: string;
    RegistrationData: null;
    CardId: null;
    CardType: CardType;
    CardRegistrationURL: string; // Submit url
    ResultCode: null;
    ResultMessage: null;
    Currency: string;
    Status: string;
}

export interface ICardInfo {
    cardRegistrationURL: string;
    data: string;
    accessKeyRef: string;
    returnURL?: string;
    cardNumber: string;
    cardExpirationDate: string;
    cardCvx: string
}

export interface IPutTokenResponse {
    Id: string;
    Tag: null;
    CreationDate: number;
    UserId: string;
    AccessKey: string;
    PreregistrationData: string;
    RegistrationData: string;
    CardId: string;
    CardType: string;
    CardRegistrationURL: string;
    ResultCode: string;
    ResultMessage: string;
    Currency: string;
    Status: string;
}

export interface IPayInOptions {
    mangoUserId: number;
    walletId: number;
    cardId: number;
    amount: number;
}

export interface IPayInRequest {
    Id: string;
    Tag: string;
    CreationDate: number;
    AuthorId: string;
    CreditedUserId: string;
    DebitedFunds: {
        Currency: string;
        "Amount": number
    },
    CreditedFunds: {
        Currency: string;
        Amount: number
    },
    Fees: {
        Currency: string;
        Amount: number;
    },
    Status: string;
    ResultCode: string;
    ResultMessage: string;
    ExecutionDate: number;
    Type: string;
    Nature: string;
    CreditedWalletId: string;
    DebitedWalletId: null;
    PaymentType: string;
    ExecutionType: string;
    SecureMode: string;
    CardId: string;
    SecureModeReturnURL: null;
    SecureModeRedirectURL: null;
    SecureModeNeeded: false;
    Billing: {
        Address: {
            AddressLine1: null;
            AddressLine2: null;
            City: null;
            Region: null;
            PostalCode: null;
            Country: null
        }
    },
    Culture: string;
    SecurityInfo: {
        AVSResult: string
    },
    StatementDescriptor?: string
}

export interface ITransferOptions {
    mangoUserId: string | number;
    walletId: string | number;
    creditedWalletId: string | number;
    amount: number | string;
}

export interface IPayOut {
    userId: number;
    walletId: number;
    bankAccountId: number;
    amount: number;
}

export interface IPayOutResponse {
    Id: string;
    Tag: null;
    CreationDate: number;
    AuthorId: string;
    CreditedUserId: null;
    DebitedFunds: {
        Currency: string;
        Amount: number;
    },
    CreditedFunds: {
        Currency: string;
        Amount: number;
    },
    Fees: {
        Currency: string;
        Amount: number;
    },
    Status: string;
    ResultCode: null;
    ResultMessage: null;
    ExecutionDate: null;
    Type: string;
    Nature: string;
    CreditedWalletId: null;
    DebitedWalletId: string;
    PaymentType: string;
    BankAccountId: string;
    BankWireRef: string;
}

export interface IRefundOptions {
    paInId: number;
    userId: number;
    amount: number;
    fees: number
}

export interface ICard {
    ExpirationDate: string;
    Alias: string;
    CardType: string;
    CardProvider: string;
    Country: string;
    Product: string;
    BankCode: string;
    Active: true;
    Currency: string;
    Validity: string;
    UserId: string;
    Id: string;
    Tag: null;
    CreationDate: number;
    Fingerprint: string
}

export interface IBankAccount {
    bic?: string;
    city: string;
    iban: string;
    region?: string;
    userId: number;
    country: string;
    lastName?: string;
    firstName?: string;
    name: string;
    postalCode: string;
    address: string;
    address2?: string
}

export interface IWallet {
    Balance: {
        Currency: string,
            Amount: number
    },
    Currency: string,
        FundsType: string,
        Id: string,
        Tag: null,
        CreationDate: number
}

export interface IWalletInfo {
    Description: string;
    Owners: [ string ];
    Balance: { Currency: string, Amount: number };
    Currency: string;
    FundsType: string;
    Id: string;
    Tag: string;
    CreationDate: number
}

export interface IBankAllias {
    walletId: string;
    creditedUserId: string;
    ownerName: string;
    country: string;
    id: string;
    bic: string;
    iban: string;
}

export interface IBankAlliasResponse {
    OwnerName: string;
    IBAN: string;
    BIC: string;
    CreditedUserId: string;
    Country: string;
    Tag: null;
    CreationDate: number;
    Active: true;
    Type: string;
    Id: string;
    WalletId: string;
}

export interface ITransferResponse {
    Id: string,
    Tag: null,
    CreationDate: number,
    AuthorId: string,
    CreditedUserId: string,
    DebitedFunds: {
        Currency: string,
        Amount: number
    },
    CreditedFunds: {
        Currency: string,
        Amount: number
    },
    Fees: {
        Currency: "EUR",
        Amount: number
    },
    Status: string,
    ResultCode: string,
    ResultMessage: string,
    ExecutionDate: number,
    Type: string,
    Nature: string,
    CreditedWalletId: string,
    DebitedWalletId: null,
    PaymentType: string,
    ExecutionType: string,
    SecureMode: string,
    CardId: string,
    SecureModeReturnURL: null,
    SecureModeRedirectURL: null,
    SecureModeNeeded: false,
    Billing: {
        Address: {
            AddressLine1: null,
            AddressLine2: null,
            City: null,
            Region: null,
            PostalCode: null,
            Country: null
        }
    },
    Culture: string,
    SecurityInfo: {
        AVSResult: string
    },
    StatementDescriptor: null
}

export type Currency = "EUR";

export interface IWalletTransaction {
    Id: string;
    Tag: null | string;
    CreatingDate: number;
    AuthorId: string;
    CreditedUserId: string;
    DebitedFunds: {
        Cureency: Currency;
        Amount: number;
    };
    CreditedFunds: {
        Currency: Currency;
        Amount: number;
    };
    Fees: {
        Currency: Currency;
        Amount: number;
    };
    Status: string;
    ResultCode: string;
    ResultMEssage: string;
    ExecutionDateR: number;
    Type: string;
    Nature: string;
    CreditedWalletId: string;
    DebitedWalletId: string;
}
