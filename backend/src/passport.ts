import passport = require("passport");
import argon2 = require('argon2');
import { Strategy as LocalStrategy} from "passport-local";
import { Strategy as JWTStrategy} from "passport-jwt";
import { ExtractJwt } from "passport-jwt";
import { Person } from "./entities/person";
import { getRepository } from "typeorm";
import { Keys } from "./utils/keys";
import getAccountRepository from "./entities/repositories/account-repository";
import { Token } from "./interfaces/global/token.interface";


function inititialize() {
    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
    }, async (email: string, password: string, done: any) => {
        email = email.toLowerCase();
        try {
            // Added query builder for getting hidden 'password' field
            const person = await getRepository(Person)
                .createQueryBuilder('person')
                .addSelect("person.password")
                .where("person.email = :email", { email })
                .getOne();

            const passwordsMatch = await argon2.verify(person.password, password);
            if (passwordsMatch) {

                const user = await getAccountRepository().getAccountForToken({person: person.id});
                if (user) {
                    return done(null, user);
                } else {
                    throw Error ("Account not found");
                }
            } else {
                throw Error("Incorrect Username / Password");
            }
        } catch (error) {
            done(error);
        }
    }));


    passport.use(new JWTStrategy(
        {
            jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: Keys.publicKey(),
        },
        async (jwtPayload: Token, done) => {
            // console.log('jwtPayload', jwtPayload)
            try {
                if (Date.now() > jwtPayload.expires) {
                    return done(null, false, { message: "jwt expired" });
                }
                const user = await getAccountRepository().getAccountForToken({
                    id: jwtPayload.id,
                    person: jwtPayload.person,
                    business: jwtPayload.business
                });
                if (user) {
                    // console.log('user', user)
                    if( user.deactivated ) {
                        throw Error("Account deactivated. Please log in and reactivate");
                    }
                    return done(null, jwtPayload);
                } else {
                    return done(null, false, { message: "jwt invalid" });
                }
            } catch (error) {
                return done(error);
            }
        }
    ));
}

export = {inititialize};
