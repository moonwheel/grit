# Grit Backend

## Installation


* Install [docker][DOCKER-LINK]/[docker-compose][DOCKERCOMPOSE-LINK] (latest version).



* Switch user `sudo usermod -aG docker $USER`. Otherwise, all docker commands must be executed by `sudo` 

* Init Node.js:

  * Ask admin for `.env` file and add it to the root level of the working directory (edit `.env` file if needed)

* In the root folder execute `docker-compose up -d --build` to build and run containers locally and execute `docker-compose down` to stop the contaiers.


[DOCKER-LINK]: https://docs.docker.com/install/
[DOCKERCOMPOSE-LINK]: https://docs.docker.com/compose/install/
