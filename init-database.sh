#!/bin/bash
echo Wait for servers to be up
HOSTPARAMS="--host db --insecure"
SQL="/cockroach/cockroach.sh sql $HOSTPARAMS"

until $SQL; do
  >&2 echo "cockroach is unavailable - sleeping"
  sleep 1
done


>&2 echo "cockroach is up - executing command"

$SQL -e "CREATE DATABASE grit;"
$SQL -e "use grit;"
$SQL -e "create user grit;"
$SQL -e "grant all on database grit to grit;"