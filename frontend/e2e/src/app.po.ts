import { browser, by, element } from 'protractor';
import { Injectable } from '@angular/core';

@Injectable()
export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }
}
