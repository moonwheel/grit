const webpack = require('webpack');

console.log('-----------------------------------------')
console.log('-----------------------------------------')
console.log('process.env.API_URL', process.env.API_URL)
console.log('-----------------------------------------')
console.log('-----------------------------------------')

module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      $ENV: {
        ENVIRONMENT: JSON.stringify(process.env.ENVIRONMENT),
        API_URL: JSON.stringify(process.env.API_URL),
      }
    })
  ]
};
