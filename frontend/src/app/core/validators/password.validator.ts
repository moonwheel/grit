import { AbstractControl } from '@angular/forms';

export function ValidatePassword(control: AbstractControl): null | { [key: string]: boolean } {
  const password = control.value;
  return (password.trim().length > 5) ? null : { password: true };
}
