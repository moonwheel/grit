import { Injectable, Optional, InjectionToken, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { map } from 'rxjs/operators';
import { NavigationService } from 'src/app/shared/services/navigation.service';

export const UploadingGuardStateToken = new InjectionToken<typeof AbstractState>('UploadingGuardStateToken');

@Injectable()
export class UploadingGuard implements CanActivate {

  constructor(
    private navService: NavigationService,
    private store: Store,
    @Optional()
    @Inject(UploadingGuardStateToken)
    private state: typeof AbstractState,
  ) {
    if (!state) {
      console.warn(`State is not found for UploadingGuard. Please use UploadingGuardStateToken to inject it`);
    }
  }

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | boolean {
    const id = route.paramMap.get('id');

    if (!id || !this.state) {
      this.navService.nativeGoBack();
      return false;
    }

    return this.store.selectOnce(
      AbstractState.getOneSelector(this.state, Number(id))
    ).pipe(
      map(item => {
        if (!item) {
          return true;
        }

        if (item.processing || item.uploading) {
          this.navService.nativeGoBack();
          return false;
        } else {
          return true;
        }
      })
    );
  }
}


