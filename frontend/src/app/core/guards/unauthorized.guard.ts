import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AuthState } from 'src/app/store/state/auth.state';

@Injectable({
  providedIn: 'root'
})
export class UnauthorizedGuard implements CanActivate {

  constructor(
    private router: Router,
    private store: Store,
  ) {
  }

  public canActivate(): Observable<boolean> | boolean {
    const token = this.store.selectSnapshot(AuthState.token);
    if (!token) {
      return true;
    } else {
      this.router.navigate(['/feed']);
      return false;
    }
  }
}
