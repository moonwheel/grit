import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(
      private router: Router,
      private store: Store,
  ) {
  }

  public canActivate(): Observable<boolean> | boolean {
    const token = this.store.selectSnapshot<string>(state => state.auth.token);
    if (token) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
