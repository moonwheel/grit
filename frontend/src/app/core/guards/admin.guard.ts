import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { Utils } from 'src/app/shared/utils/utils';

@Injectable({
  providedIn: 'root',
})

export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store,
  ) { }

  public canActivate(): Observable<boolean> | boolean {
    const roleType = this.store.selectSnapshot(state => {
      return Utils.path(state, ['user', 'loggedInAccount', 'role', 'type']);
    });

    if (roleType === 'admin') {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
