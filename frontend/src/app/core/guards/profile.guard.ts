import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map, filter, take } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';
import { Utils } from 'src/app/shared/utils/utils';

@Injectable({
  providedIn: 'root'
})

export class ProfileGuard implements CanActivate {

  constructor(
    private router: Router,
    private store: Store,
  ) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const profileUrl = Utils.getParentRouteParam(route, 'username');

    return this.store.select(UserState.loggedInAccountPageName).pipe(
      filter(user => !!user),
      take(1),
      map(pageName => {
        if (profileUrl && pageName.toLowerCase() === profileUrl.toLowerCase()) {
          return true;
        } else {
          this.router.navigate(['/']);
          return false;
        }
      })
    );
  }
}
