import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { Utils } from 'src/app/shared/utils/utils';

@Injectable({
  providedIn: 'root',
})
export class ViewerGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store,
  ) { }

  public canActivate(): Observable<boolean> | boolean {
    const roleType = this.store.selectSnapshot(state => {
      return Utils.path(state, ['user', 'loggedInAccount', 'role', 'type']);
    });

    if (roleType === 'viewer') {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
