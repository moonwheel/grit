import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) { }

  handleError(error: any) {
    const router = this.injector.get(Router);

    if (error instanceof HttpErrorResponse) {
      // The backend returned an unsuccessful response code
      console.error('Backend returned status code:', error.status);
      console.error('Response body:', error.message);
    } else {
      // A client-side or network error occurred
      console.error('An error occurred:', error.message);
    }

    router.navigate(['error']);

  }

}
