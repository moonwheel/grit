import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class PhotoFileResolver implements Resolve<File> {

  accept = 'image/*';

  constructor(protected router: Router, protected location: Location) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): File | Observable<File> | Promise<File> {
    const currentUrl = this.location.path();
    const targetUrl = state.url;

    return null;
    // TODO: bring this back if customer need this. It was disabled because of unclear situation with drafts.

    // if (currentUrl === targetUrl || 'id' in route.queryParams) {
    //   return null;
    // }

    // const input = document.createElement('input');

    // input.type = 'file';
    // input.accept = this.accept;
    // input.style.display = 'none';

    // return new Observable(observer => {
    //   input.oninput = (event) => {
    //     const target = event.target as HTMLInputElement;

    //     if (target.files.length) {
    //       const file = target.files[0];

    //       observer.next(file);
    //       observer.complete();
    //     } else {
    //       this.router.navigateByUrl(this.location.path());
    //       observer.complete();
    //     }
    //   };

    //   input.click();
    //   input.remove();
    // });
  }
}
