import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { filter, take, tap } from 'rxjs/operators';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';
/**
 * This resolver used to wait until account change completed
 * before navigation to main tabs (feed, messages, etc...)
 */
@Injectable({
  providedIn: 'root'
})
export class UserLoadingResolver implements Resolve<any> {
  constructor(private store: Store) { }

  resolve(): Observable<any> {
    this.store.dispatch(new SetGlobalLoading(true));
    return this.store.select(UserState.loading).pipe(
      filter(loading => !loading),
      take(1),
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      })
    );
  }
}
