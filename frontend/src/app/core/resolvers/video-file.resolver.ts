import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PhotoFileResolver } from './photo-file.resolver';


@Injectable({
  providedIn: 'root'
})
export class VideoFileResolver extends PhotoFileResolver  {

  accept = 'video/*';

  constructor(protected router: Router, protected location: Location) {
    super(router, location);
  }
}
