import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { ServiceActions } from '../../store/actions/service.actions';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServiceEditResolver implements Resolve<any> {
  constructor(private store: Store) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    this.store.dispatch(new SetGlobalLoading(true));

    const actions: any[] = [ new ServiceActions.LoadDrafts() ];

    if (route.queryParams.id) {
      actions.push(new ServiceActions.GetOne(+route.queryParams.id));
    }

    return this.store.dispatch(actions).pipe(
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      })
    );
  }
}
