import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';
import { InitBusiness } from '../../store/actions/business.actions';
import { take, switchMap, filter, tap } from 'rxjs/operators';
import { BusinessState } from 'src/app/store/state/business.state';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';


@Injectable({
  providedIn: 'root'
})
export class BusinessListResolver implements Resolve<any> {
  constructor(private store: Store) { }

  resolve(): Observable<any> {
    return this.store.select(BusinessState.businessList).pipe(
      filter(item => !!item),
      take(1),
      switchMap(list => {
        if (list.length) {
          return of(list);
        } else {
          this.store.dispatch(new SetGlobalLoading(true));
          return this.store.dispatch(new InitBusiness()).pipe(
            switchMap(() => this.store.select(BusinessState.businessList)),
            take(1),
            tap(() => {
              this.store.dispatch(new SetGlobalLoading(false));
            })
          );
        }
      })
    );
  }
}
