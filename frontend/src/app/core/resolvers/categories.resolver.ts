import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { GetBusinessCategories } from '../../store/actions/business.actions';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BusinessCategoriesResolver implements Resolve<any> {
  constructor(private store: Store) {
  }
  resolve(): Observable<any> {
    this.store.dispatch(new SetGlobalLoading(true));

    return this.store.dispatch(new GetBusinessCategories()).pipe(
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      })
    );
  }
}
