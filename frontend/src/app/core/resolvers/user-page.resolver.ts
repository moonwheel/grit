import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';
import { ResetViewableUserStore, SetViewableUser } from 'src/app/store/actions/user.actions';
import { UserState } from 'src/app/store/state/user.state';
import { filter, take, switchMap, catchError, mapTo, tap, finalize } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';

@Injectable({
  providedIn: 'root'
})
export class UserPageResolver implements Resolve<any> {
  constructor(private store: Store,
              private router: Router,
              private userService: UserService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
    const username = (route.params.username as string || '').toLowerCase();

    if (username) {
      return this.store.select(UserState.viewableAccountPageName).pipe(
        filter(item => !!item),
        take(1),
        switchMap(viewableAccountPageName => {
          if (viewableAccountPageName === username) {
            return of(true);
          } else {
            this.store.dispatch(new SetGlobalLoading(true));
            return this.userService.getByUsername(username).pipe(
              tap(user => {
                this.store.dispatch(new ResetViewableUserStore());
                this.store.dispatch(new SetViewableUser(user));
              }),
              mapTo(true),
              catchError(error => {
                this.router.navigateByUrl('/404');
                return of(false);
              }),
              finalize(() => {
                this.store.dispatch(new SetGlobalLoading(false));
              })
            );
          }
        })
      );
    } else {
      this.router.navigateByUrl('/feed');
      return false;
    }
  }
}
