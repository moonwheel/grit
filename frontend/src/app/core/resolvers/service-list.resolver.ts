import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { ServiceActions } from '../../store/actions/service.actions';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServiceListResolver implements Resolve<any> {
  constructor(private store: Store) { }

  resolve(): Observable<any> {
    this.store.dispatch(new SetGlobalLoading(true));
    return this.store.dispatch(new ServiceActions.Init()).pipe(
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      })
    );
  }
}
