import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { InitBusiness } from '../../store/actions/business.actions';
import { InitUser } from 'src/app/store/actions/user.actions';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserListResolver implements Resolve<any> {
  constructor(private store: Store) { }

  resolve(): Observable<any> {
    this.store.dispatch(new SetGlobalLoading(true));
    return this.store.dispatch([
      new InitBusiness(),
      new InitUser()
    ]).pipe(
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      })
    );
  }
}
