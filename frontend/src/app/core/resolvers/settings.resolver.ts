import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';
import { UserState } from '../../store/state/user.state';
import { GetBusiness } from '../../store/actions/business.actions';
import { take, switchMap, filter, tap } from 'rxjs/operators';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';


@Injectable({
  providedIn: 'root'
})
export class SettingsResolver implements Resolve<any> {
  constructor(private store: Store) { }

  resolve(): Observable<any> {
    return this.store.select(UserState.loggedInAccount).pipe(
      filter(account => !!account),
      take(1),
      switchMap(account => {
        if (account.business) {
          this.store.dispatch(new SetGlobalLoading(true));
          return this.store.dispatch(new GetBusiness(account.business.id)).pipe(
            tap(() => {
              this.store.dispatch(new SetGlobalLoading(false));
            })
          );
        } else {
          const snapshot = this.store.snapshot();
          return of(snapshot);
        }
      })
    );
  }
}
