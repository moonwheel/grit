import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpHeaders
} from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { catchError, flatMap, map } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { AuthState } from 'src/app/store/state/auth.state';
import { UpdateTokens, Logout } from '../../store/actions/auth.actions';
import { ApiService } from '../../shared/services/api.service';
import { environment } from '../../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )
export class CustomHttpInterceptor implements HttpInterceptor {
  constructor(
    private store: Store,
    private http: HttpClient,
    private router: Router,
  ) { }

  intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
    if (request.url.startsWith('/assets/i18n')) {
      return next.handle(request);
    }

    // default empty string values provided, because you can't write undefined in http headers, it should be string
    const token = this.store.selectSnapshot( AuthState.token ) || '';
    const refreshToken = this.store.selectSnapshot( AuthState.refreshToken ) || '';

    if ( token ) {
      request = request.clone( {headers: request.headers.set( 'Authorization', 'Bearer ' + token )} );
    }

    if ( !request.headers.has( 'Content-Type' ) ) {
      request = request.clone( {headers: request.headers.set( 'Content-Type', 'application/json' )} );
    } else {
      request = request.clone( {headers: request.headers.delete( 'Content-Type' )} );
    }

    request = request.clone( {headers: request.headers.set( 'Accept', 'application/json' )} );

    return next.handle( request ).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (
          typeof error.error === 'object' && error.error.error === `Can't renew token` ||
          typeof error.error === 'string' && error.error.includes('deactivated')
        ) {
          this.store.dispatch(new Logout());
        }

        if (error.status === 401) {
          return this.http.post( environment.baseUrl + '/user/token', {}, {headers: {refreshToken}} ).pipe(
            flatMap(
              ( data: any ) => {
                request = request.clone( {headers: request.headers.set( 'Authorization', 'Bearer ' + data.token )} );
                this.store.dispatch( new UpdateTokens({ token: data.token, refreshToken: data.refreshToken }) );
                return next.handle( request );
              }
            ),
          );
        } else if (error.status === 400) {
          if (error.error.isBlocked && error.error.pageName) {
            this.router.navigate([`/${error.error.pageName}/home`]);
          }
        }
        return throwError( error );
      })
    );
  }
}
