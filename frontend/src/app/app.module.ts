import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { registerLocaleData } from '@angular/common';
import { UrlSerializer } from '@angular/router';
import localeDeAt from '@angular/common/locales/de-AT';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';

// Main Modules
import { SharedModule } from './shared/shared.module';

// Interceptor
import { CustomHttpInterceptor } from 'src/app/core/interceptors/http.interceptor';

// State
import { AuthState, TOKEN_STORAGE_KEY, REFRESH_TOKEN_STORAGE_KEY } from './store/state/auth.state';
import { UserState, LANG_STORAGE_KEY } from './store/state/user.state';
import { BusinessState } from './store/state/business.state';
import { AddressState } from './store/state/address.state';
import { TextState } from './store/state/text.state';
import { PhotoState } from './store/state/photo.state';
import { VideoState } from './store/state/video.state';
import { ArticleState } from './store/state/article.state';
import { ProductState } from './store/state/product.state';
import { ShopCategoryState } from './store/state/shop-category.state';
import { ServiceState } from './store/state/service.state';
import { VisibleState } from './store/state/visible.state';
import { ReportState } from './store/state/report.state';
import { BookmarkState } from './store/state/bookmark.state';
import { AnnotationState } from './store/state/annotation.state';
import { SearchState } from './store/state/search.state';
import { TimelineState } from './store/state/timeline.state';
import { FeedState } from './store/state/feed.state';
import { TeamState } from './store/state/team.state';
import { CartState } from './store/state/cart.state';
import { ChatState } from './store/state/chat.state';
import { PaymentsState } from './store/state/payment.state';
import { OrderState } from './store/state/order.state';
import { BookingState } from './store/state/booking.state';
import { DiscoverState } from './store/state/discover.state';
import { NotificationState } from './store/state/notifications.state';
import { ReviewState } from './store/state/review.state';

// Providers
import { CustomUrlSerializer } from './shared/utils/custom-url-serializer';
import { WalletState } from './store/state/wallet.state';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

registerLocaleData(localeDeAt, 'de-AT');

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // Main Modules
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRouting,
    // Library Modules
    SharedModule,
    NgxsModule.forRoot([
      AuthState,
      UserState,
      AddressState,
      BusinessState,
      TextState,
      PhotoState,
      VideoState,
      ArticleState,
      ProductState,
      ShopCategoryState,
      ServiceState,
      VisibleState,
      ReportState,
      BookmarkState,
      AnnotationState,
      SearchState,
      TimelineState,
      FeedState,
      TeamState,
      CartState,
      ChatState,
      PaymentsState,
      OrderState,
      BookingState,
      DiscoverState,
      ReviewState,
      NotificationState,
      WalletState,
    ], {
      developmentMode: !environment.production
    }),
    NgxsStoragePluginModule.forRoot({
      key: [
        TOKEN_STORAGE_KEY,
        REFRESH_TOKEN_STORAGE_KEY,
        LANG_STORAGE_KEY
      ],
    }),
    TranslateModule.forRoot({
      defaultLanguage: 'deutsch',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true
    },
    { provide: UrlSerializer, useClass: CustomUrlSerializer }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
