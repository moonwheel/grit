import { AccountModel } from 'src/app/shared/models';

// tslint:disable-next-line:no-namespace
export namespace TeamActions {
  export class Reset {
    static readonly type = '[Team] Reset';
    constructor() {}
  }

  export class Load {
    static readonly type = '[Team] Load';
    constructor(public search = '', public page: number = 1, public limit: number = 30, public lazy = false) {}
  }

  export class GetAutosuggestions {
    static readonly type = '[Team] Get Autosuggestions';
    constructor(public text: string) {}
  }

  export class Add {
    static readonly type = '[Team] Add';
    constructor(public payload: any) {}
  }

  export class AddMany {
    static readonly type = '[Team] Add Many';
    constructor(public accounts: AccountModel[]) {}
  }

  export class Delete {
    static readonly type = '[Team] Delete';
    constructor(public id: number) {}
  }

  export class UpdateRole {
    static readonly type = '[Team] Update Role';
    constructor(public userId: number, public role: string) {}
  }

}
