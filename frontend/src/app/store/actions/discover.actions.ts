import { constants } from 'src/app/core/constants/constants';

export type DiscoverEntityType = 'photo' | 'video' | 'product' | 'service' | 'article';
export const discoverEntityTypeSet = new Set<DiscoverEntityType>(['photo', 'video', 'product', 'service', 'article']);

// tslint:disable-next-line:no-namespace
export namespace DiscoverActions {
  export class Load {
    static readonly type = '[Discover] Load';
    constructor(
      public entityType: DiscoverEntityType = 'photo',
      public categoryName: string = constants.DEFAULT_MEDIA_CATEGORY,
      public page = 1,
      public limit = 15,
      public lazy = false,
      public lat = null,
      public lng = null,
      public radius = null,
      public address = '',
    ) {}
  }

  export class GetLocationAutosuggestions {
    static readonly type = '[Discover] GetLocationAutosuggestions';
    constructor(public text: string) {}
  }

  export class Reset {
    static readonly type = '[Discover] Reset';
  }
}
