import { AbstractEntityType, AbstractEntityModel, NotificationModel } from 'src/app/shared/models';

// tslint:disable-next-line:no-namespace
export namespace NotificationActions {
  export class Load {
    static readonly type = '[Notification] Load';
    constructor(public page: number = 1, public limit: number = 15, public lazy: boolean = false) {}
  }

  export class Reset {
    static readonly type = '[Notification] Reset';
  }

  export class DeleteItem {
    static readonly type = '[Notification] Delete Item';
    constructor(public id: number) {}
  }

  export class AddItem {
    static readonly type = '[Notification] Add Item';
    constructor(public item: NotificationModel) {}
  }

  export class GetUnreadCount {
    static readonly type = '[Notification] Get Unread Count';
  }

  export class MarkAsRead {
    static readonly type = '[Notification] Mark As Read';
    constructor(public ids: number[]) {}
  }

  export class MarkAllAsRead {
    static readonly type = '[Notification] Mark All As Read';
  }
}
