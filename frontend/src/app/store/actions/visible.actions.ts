import { VisibleStateModel } from '../state/visible.state';

export class ToggleVisible {
  static readonly type = '[Visible] Toggle Visible';
  constructor(public payload: VisibleStateModel) {}
}

export class SetGlobalLoading {
  static readonly type = '[Visible] Set Global Loading';
  constructor(public globalLoading: boolean) {}
}
