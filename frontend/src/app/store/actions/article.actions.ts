import { AbstractActions, AbstarctOrder } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace ArticleActions {
    export class Init extends AbstractActions.Init {
        static readonly type = '[Article] Init';
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Article] Load';

        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public lazy = false
        ) {
            super()
        }
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Article] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Article] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Article] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Article] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Article] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Article] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Article] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Article] Edit';
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Article] Delete';
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Article] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Article] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Article] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Article] Edit Comment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Article] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Article] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Article] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Article] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Article] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Article] Insert Items';
    }

    export class View extends AbstractActions.View {
      static readonly type = '[Article] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
      static readonly type = '[Article] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
      static readonly type = '[Article] Delete From Bookmark';
    }
}

