export class AddBooking {
    static readonly type = '[Booking] Add';
    constructor(public payload: object) { }
}

export class CancelBooking {
    static readonly type = '[Booking] Cancel';
    constructor(public id: number) { }
}

export class RefundBooking {
    static readonly type = '[Booking] Refund';
    constructor(public id: number) { }
}

export class ChangeBooking {
  static readonly type = '[Booking] Change';
  constructor(public id: number, public booking: object, public type: 'purchases' | 'sales' = 'purchases') { }
}
