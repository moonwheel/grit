export type SearchEntityType = 'all' | 'photo' | 'account' | 'video' | 'product' | 'service' | 'article';

// tslint:disable-next-line:no-namespace
export namespace SearchActions {
  export class Search {
    static readonly type = '[Search] Search';
    constructor(
      public text: string,
      public searchType: SearchEntityType = 'all',
      public page = 1,
      public limit = 15,
      public forAllList = false,
      public location = false,
      public lazy = false,
    ) {}
  }

  export class SearchPhotos {
    static readonly type = '[Search] Search Photos';
    constructor(public text: string) {}
  }

  export class SearchAccounts {
    static readonly type = '[Search] Search Accounts';
    constructor(public text: string) {}
  }

  export class GetAutoSuggestions {
    static readonly type = '[Search] Get Autosuggestions';
    constructor(public text: string, public searchType: string = 'all') {}
  }
}

