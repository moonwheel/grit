import { AbstractEntityType } from 'src/app/shared/models';

// tslint:disable-next-line:no-namespace
export namespace TimelineActions {
  export class Load {
    static readonly type = '[Timeline] Load';
    constructor(public page: number = 1, public limit: number = 30, public lazy = false) {}
  }

  export class DeleteItem {
    static readonly type = '[Timeline] Delete Item';
    constructor(public id: number, public tableName: AbstractEntityType) {}
  }

  export class Reset {
    static readonly type = '[Timeline] Reset';
    constructor() {}
  }
}
