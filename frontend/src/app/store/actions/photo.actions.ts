import { AbstractActions, AbstractAction, AbstarctOrder } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace PhotoActions {
    export class Init extends AbstractActions.Init {
        static readonly type = '[Photo] Init';
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Photo] Load';

        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public lazy = false,
        ) {
            super()
        }
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Photo] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Photo] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Photo] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Photo] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Photo] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Photo] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Photo] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Photo] Edit';
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Photo] Delete';
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Photo] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Photo] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Photo] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Photo] EditComment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Photo] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Photo] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Photo] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Photo] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Photo] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Photo] Insert Items';
    }

    export class View extends AbstractActions.View {
        static readonly type = '[Photo] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
        static readonly type = '[Photo] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
        static readonly type = '[Photo] Delete From Bookmark';
    }
}
