export class InitPurchases {
  static readonly type = '[Order] Init Purchases';
  constructor(public limit = 15, public filter = 'all', public search = '') { }
}

export class LoadPurchases {
  static readonly type = '[Order] Loading Purchases';
  constructor(public page = 1, public limit = 15, public filter = 'all', public search = '', public lazy = false) { }
}

export class GetOnePurchase {
  static readonly type = '[Order] Load Purchase'
  constructor(public id: string, public type: 'product' | 'service') {}
}

export class InitSales {
  static readonly type = '[Order] Init Sales';
  constructor(public limit = 15, public filter = 'all', public search = '') { }
}

export class LoadSales {
  static readonly type = '[Order] Loading Sales';
  constructor(public page = 1, public limit = 15, public filter = 'all', public search = '', public lazy = false) { }
}

export class GetOneSale {
  static readonly type = '[Order] Load Sale';
  constructor(public id: string, public type: 'product' | 'service') {}
}

export class ResetOrders {
  static readonly type = '[Order] Reset';
}

export class ShipOrder {
  static readonly type = '[Order] Ship';
  constructor(public id: number, public items: number[]) { }
}

export class CancelOrder {
  static readonly type = '[Order] Cancel Order';
  constructor(public id: number, public items: number[]) { }
}

export class ReturnOrder {
  static readonly type = '[Order] Return Order';
  constructor(public id: number, public items: number[], public reason: string, public description= '') { }
}

export class RefundOrder {
  static readonly type = '[Order] Refund Order';
  constructor(public id: number, public items: number[], public payment = '') { }
}
