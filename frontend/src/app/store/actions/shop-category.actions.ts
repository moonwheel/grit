import { ShopCategoryModel } from 'src/app/shared/models/shop-category.model';

export class InitShopCategory {
    static readonly type = '[ShopCategory] Init';
    constructor() { }
}

export class LoadShopCategories {
    static readonly type = '[ShopCategory] Loading';
    constructor(public status?: string) { }
}

export class ResetShopCategories {
    static readonly type = '[ShopCategory] Reset';
    constructor() { }
}

export class AddShopCategory {
    static readonly type = '[ShopCategory] Add';
    constructor(public payload: ShopCategoryModel) { }
}

export class EditShopCategory {
    static readonly type = '[ShopCategory] Edit';
    constructor(public payload: ShopCategoryModel, public index: number) { }
}

export class DeleteShopCategory {
    static readonly type = '[ShopCategory] Delete';
    constructor(public id: number) { }
}
