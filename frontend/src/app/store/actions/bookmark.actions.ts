export class InitBookmark {
  static readonly type = '[Bookmark] Init';
  constructor() { }
}

export class LoadBookmark {
  static readonly type = '[Bookmark] Load';
  constructor(public page = 1, public limit = 15, public search = '', public lazy = false) { }
}

export class AddBookmark {
  static readonly type = '[Bookmark] Add';
  constructor(public payload: object) { }
}

export class DeleteBookmark {
  static readonly type = '[Bookmark] Delete';
  constructor(public payload: object) { }
}
