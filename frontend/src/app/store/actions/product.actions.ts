
import { AbstractActions, AbstarctOrder } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace ProductActions {
    export class Init extends AbstractActions.Init {
        static readonly type = '[Product] Init';
        constructor(
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public categoryId?: number,
        ) {
            super();
        }
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Product] Load';
        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public categoryId?: number,
            public lazy = false,
        ) {
            super();
        }
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Product] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Product] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Product] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Product] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Product] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Product] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Product] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Product] Edit';
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Product] Delete';
    }

    export class ChangeActiveState {
        static readonly type = '[Product] Change State';
        constructor(public payload: object) { }
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Product] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Product] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Product] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Product] EditComment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Product] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Product] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Product] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Product] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Product] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Product] Insert Items';
    }

    export class View extends AbstractActions.View {
      static readonly type = '[Product] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
      static readonly type = '[Product] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
      static readonly type = '[Product] Delete From Bookmark';
    }

    export class GetDetailedList {
        static readonly type = '[Product] Get Detailed List';
        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public categoryId?: number,
            public lazy = false,
        ) {}
    }
}
