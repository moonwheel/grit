export class InitReviews {
  static readonly type = '[Review] Init Reviews';
  constructor(public entity: string, public id: number, public limit = 15, public rating = 'all', public orderBy = 'relevance', public order = 'asc') { }
}

export class LoadReviews {
  static readonly type = '[Review] Load Reviews';
  constructor(public entity: string, public id: number, public page = 1, public limit = 15, public rating = 'all', public orderBy = 'relevance', public order = 'asc') { }
}

export class ResetReviews {
  static readonly type = '[Review] Reset Reviews';
}

export class GetReview {
  static readonly type = '[Review] Get Review';
  constructor(public entity: string, public id: number) { }
}

export class AddReview {
  static readonly type = '[Review] Add Review';
  constructor(public entity: string, public id: number, public review: any, public dataToUpload?: any) { }
}

export class Upload {
  static readonly type = '[Review] Upload';
  constructor(public id: number, public entity: string, public dataToUpload: any, public fileEntites: any[] = []) { }
}

export class UpdateUploadingProgress {
  static readonly type = '[Review] Update Uploading Progress';
  constructor(public id: number, public progress: number) { }
}

export class FinishProcessing {
  static readonly type = '[Review] Finish Processing';
  constructor(public id: number, public payload: any) { }
}

export class EditReview {
  static readonly type = '[Review] Edit Review';
  constructor(public entity: string, public review: any, public dataToUpload?: any) { }
}

export class DeleteReview {
  static readonly type = '[Review] Delete Review';
  constructor(public entity: string, public id: number) { }
}

export class LikeReview {
  static readonly type = '[Review] Like Review';
  constructor(public entity: string, public id: number) { }
}

export class LikeByReview {
  static readonly type = '[Review] Like By Review';
  constructor(public entity: string, public id: number) { }
}

export class AddReviewVideo {
  static readonly type = '[Review] Add Review Video';
  constructor(public entity: string, public id: number) { }
}

export class DeleteReviewVideo {
  static readonly type = '[Review] Delete Review Video';
  constructor(public entity: string, public id: number) { }
}

export class DeleteReviewPhoto {
  static readonly type = '[Review] Delete Review Photo';
  constructor(public entity: string, public id: number) { }
}

export class AddReplyReview {
  static readonly type = '[Review] Add Reply Review';
  constructor(public entity: string, public id: number, public review: any) { }
}

export class EditReplyReview {
  static readonly type = '[Review] Edit Reply Review';
  constructor(public entity: string, public id: number, public reply: any) { }
}

export class DeleteReplyReview {
  static readonly type = '[Review] Delete Reply Review';
  constructor(public entity: string, public reviewId: number, public id: number) { }
}

export class LoadReviewReplies {
  static readonly type = '[Review] Load Replies';
  constructor(public entity: string, public id: number, public loadMore: boolean = false) { }
}

export class LoadReviewLikes {
  static readonly type = '[Review] Load Likes';
  constructor(public entity: string, public id: number, public reply: boolean = false, public loadMore: boolean = false) { }
}

export class LikeReplyReview {
  static readonly type = '[Review] Like Reply';
  constructor(public entity: string, public id: number) { }
}

export class LoadReviewReplyLikes {
  static readonly type = '[Review] Load Reply Likes';
  constructor(public entity: string, public id: number) { }
}
