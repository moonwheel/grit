import { AbstractActions } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace TextActions {
    export class Init extends AbstractActions.Init {
        static readonly type = '[Text] Init';
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Text] Loading';
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Text] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Text] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Text] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Text] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Text] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Text] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Text] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Text] Edit';
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Text] Delete';
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Text] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Text] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Text] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Text] Edit Comment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Text] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Text] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Text] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Text] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Text] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Text] Insert Items';
    }

    export class View extends AbstractActions.View {
      static readonly type = '[Text] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
      static readonly type = '[Text] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
      static readonly type = '[Text] Delete From Bookmark';
    }
}
