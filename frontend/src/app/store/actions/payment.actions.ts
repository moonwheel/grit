import { BankAccountModel } from '../../shared/models/user/user.model';
import { CreditCardRequestModel } from '../../shared/models/credit-card.model';

export class InitPayment {
  static readonly type = '[Payment] Init';
  constructor() {}
}

export class LoadPayments {
  static readonly type = '[Payment] Loading';
  constructor() {}
}

export class ResetPayments {
  static readonly type = '[Payment] Reset';
  constructor() {}
}

export class AddBankAccount {
  static readonly type = '[Payment] Add Bank Account';
  constructor(public payload: BankAccountModel) {}
}

export class EditBankAccount {
  static readonly type = '[Payment] Edit Bank Account';
  constructor(public payload: BankAccountModel) {}
}

export class DeleteBankAccount {
  static readonly type = '[Payment] Delete Bank Account';
  constructor(public id: number) {}
}

export class AddCreditCard {
  static readonly type = '[Payment] Add Credit Card';
  constructor(public payload: CreditCardRequestModel) {}
}

export class EditCreditCard {
  static readonly type = '[Payment] Edit Credit Card';
  constructor(public payload: CreditCardRequestModel) {}
}

export class DeleteCreditCard {
  static readonly type = '[Payment] Delete Credit Card';
  constructor(public id: number) {}
}

export class GetWallet {
  static readonly type = '[Payment] Get Wallet';
}

export class CardDirectPayIn {
  static readonly type = '[Payment] Card Direct PayIn';
  constructor(public card: number, public amount: number) {}
}

export class BankwireDirectPayIn {
  static readonly type = '[Payment] Bankwire Direct PayIn';
  constructor(public bank: number, public amount: number) {}
}
