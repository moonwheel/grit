export class InitCart {
  static readonly type = '[Cart] Init';
  constructor(public limit: number = 15) { }
}

export class LoadCart {
  static readonly type = '[Cart] Loading';
  constructor(public page: number = 1, public limit: number = 15, public lazy: boolean = false) { }
}

export class ResetCart {
  static readonly type = '[Card] Reset';
}

export class AddToCart {
  static readonly type = '[Cart] Add';
  constructor(public payload: any) { }
}

export class EditInCart {
  static readonly type = '[Cart] Edit';
  constructor(public payload: any) { }
}

export class DeleteFromCart {
  static readonly type = '[Cart] Delete';
  constructor(public id: number) { }
}

export class CheckoutCart {
  static readonly type = '[Cart] Checkout';
  constructor(public payload: object) { }
}

export class SetCartTotal {
  static readonly type = '[Cart] Set Total';
  constructor(public total: number = 0) { }
}
