import { AbstractEntityType, AbstractEntityModel } from 'src/app/shared/models';

// tslint:disable-next-line:no-namespace
export namespace FeedActions {
  export class Load {
    static readonly type = '[Feed] Load';
    constructor(public page: number = 1, public limit: number = 30, public lazy: boolean = false) {}
  }

  export class Reset {
    static readonly type = '[Feed] Reset';
  }

  export class DeleteItem {
    static readonly type = '[Feed] Delete Item';
    constructor(public id: number, public tableName: AbstractEntityType) {}
  }

  export class AddItem {
    static readonly type = '[Feed] Add Item';
    constructor(public item: AbstractEntityModel, public tableName: AbstractEntityType) {}
  }

  export class Unfollow {
    static readonly type = '[Feed] Unfollow';
    constructor(public userId: number) {}
  }

  export class ViewItem {
    static readonly type = '[Feed] View Item';
    constructor(public id: number, public tableName: AbstractEntityType) {}
  }
}
