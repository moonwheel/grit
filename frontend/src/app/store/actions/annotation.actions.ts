import { Annotation } from '../../shared/models/annotation.model';

export class InitAnnotations {
  static readonly type = '[Annotation] Init';
  constructor(public targetPhotoId: number, public filter?: string, public order?: string) { }
}

export class LoadAnnotations {
  static readonly type = '[Annotation] Loading';
  constructor(public targetPhotoId: number, public filter?: string, public order?: string) { }
}

export class ResetAnnotations {
  static readonly type = '[Annotation] Reset';
  constructor() { }
}

export class AddAnnotation {
  static readonly type = '[Annotation] Add';

  constructor(public payload: Annotation) { }
}

export class RemoveAnnotation {
  static readonly type = '[Annotation] Remove';

  constructor(public payload: Annotation, public targetPhotoId: number) { }
}

