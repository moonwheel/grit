import { AbstractActions, AbstarctOrder } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace VideoActions {

    export class Init extends AbstractActions.Init {
        static readonly type = '[Video] Init';
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Video] Load';

        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public lazy = false,
        ) {
            super();
        }
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Video] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Video] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Video] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Video] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Video] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Video] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Video] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Video] Edit';
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Video] Delete';
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Video] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Video] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Video] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Video] Edit Comment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Video] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Video] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Video] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Video] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Video] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Video] Insert Items';
    }

    export class View extends AbstractActions.View {
        static readonly type = '[Video] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
        static readonly type = '[Video] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
        static readonly type = '[Video] Delete From Bookmark';
    }
}
