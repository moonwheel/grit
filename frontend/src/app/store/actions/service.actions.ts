import { AbstractActions, AbstarctOrder } from './abstract.actions';

// tslint:disable-next-line:no-namespace
export namespace ServiceActions {

    export class Init extends AbstractActions.Init {
        static readonly type = '[Service] Init';
    }

    export class Load extends AbstractActions.Load {
        static readonly type = '[Service] Load';

        constructor(
            public page: number = 1,
            public limit: number = 15,
            public filter?: string,
            public order?: AbstarctOrder,
            public search?: string,
            public lazy = false
            ) {
            super();
        }
    }

    export class LoadDrafts extends AbstractActions.LoadDrafts {
        static readonly type = '[Service] Load Drafts';
    }

    export class GetOne extends AbstractActions.GetOne {
        static readonly type = '[Service] Get';
    }

    export class Reset extends AbstractActions.Reset {
        static readonly type = '[Service] Reset';
    }

    export class Add extends AbstractActions.Add {
        static readonly type = '[Service] Add';
    }

    export class Upload extends AbstractActions.Upload {
        static readonly type = '[Service] Upload';
    }

    export class UpdateUploadingProgress extends AbstractActions.UpdateUploadingProgress {
        static readonly type = '[Service] UpdateUploadingProgress';
    }

    export class FinishProcessing extends AbstractActions.FinishProcessing {
        static readonly type = '[Service] FinishProcessing';
    }

    export class Edit extends AbstractActions.Edit {
        static readonly type = '[Service] Edit';
    }

    export class ChangeActiveState {
        static readonly type = '[Service] Change Active State';
        constructor(public payload: object) { }
    }

    export class Delete extends AbstractActions.Delete {
        static readonly type = '[Service] Delete';
    }

    export class Like extends AbstractActions.Like {
        static readonly type = '[Service] Like';
    }

    export class LoadLikes extends AbstractActions.LoadLikes {
        static readonly type = '[Service] Load Likes';
    }

    export class Comment extends AbstractActions.Comment {
        static readonly type = '[Service] Comment';
    }

    export class EditComment extends AbstractActions.EditComment {
        static readonly type = '[Service] Edit Comment';
    }

    export class LoadComments extends AbstractActions.LoadComments {
        static readonly type = '[Service] Load Comments';
    }

    export class LoadCommentReplies extends AbstractActions.LoadCommentReplies {
        static readonly type = '[Service] Load Replies';
    }

    export class LoadCommentLikes extends AbstractActions.LoadCommentLikes {
        static readonly type = '[Service] Load Comment Likes';
    }

    export class LikeComment extends AbstractActions.LikeComment {
        static readonly type = '[Service] Like Comment';
    }

    export class DeleteComment extends AbstractActions.DeleteComment {
        static readonly type = '[Service] Delete Comment';
    }

    export class InsertItems extends AbstractActions.InsertItems {
        static readonly type = '[Service] Insert Items';
    }

    export class View extends AbstractActions.View {
      static readonly type = '[Service] View';
    }

    export class AddToBookmark extends AbstractActions.AddToBookmark {
      static readonly type = '[Service] Add To Bookmark';
    }

    export class DeleteFromBookmark extends AbstractActions.DeleteFromBookmark {
      static readonly type = '[Service] Delete From Bookmark';
    }
}
