import { MessageModel, ChatroomUserModel } from 'src/app/shared/models/chat';
import { constants } from 'src/app/core/constants/constants';
import { AbstractEntityType } from 'src/app/shared/models';

// tslint:disable-next-line:no-namespace
export namespace ChatActions {
  export class Load {
    static readonly type = '[Chat] Load';
    constructor(public page: number = 1, public limit: number = 30, public lazy = false) {}
  }

  export class LoadUnreadCount {
    static readonly type = '[Chat] LoadUnreadCount';
  }

  export class Reset {
    static readonly type = '[Chat] Reset';
  }

  export class DeleteChatInit {
    static readonly type = '[Chat] DeleteChatInit';
    constructor(public chatroomId: number) {}
  }

  export class DeleteChat {
    static readonly type = '[Chat] DeleteChat';
    constructor(public chatroomId: number) {}
  }

  export class TryToStartChat {
    static readonly type = '[Chat] TryToStartChat';
    constructor(public accountId: number) {}
  }

  export class CreateNewLocalChat {
    static readonly type = '[Chat] CreateNewLocalChat';
    constructor(public accountId: number) {}
  }

  export class CreateNewRemoteChat {
    static readonly type = '[Chat] CreateNewRemoteChat';
    constructor(public accountId: number, public message: string) {}
  }

  export class AddChat {
    static readonly type = '[Chat] AddChat';
    constructor(public chatroomUser: ChatroomUserModel) {}
  }

  export class LoadCurrentChat {
    static readonly type = '[Chat] LoadCurrentChat';
    constructor(public id: number) {}
  }

  export class LoadCurrentChatMessages {
    static readonly type = '[Chat] LoadCurrentChatMessages';
    constructor(public page = 1, public limit = constants.CHAT_MESSAGES_PAGE_SIZE, public lazy = false) {}
  }

  export class SendMessageToChat {
    static readonly type = '[Chat] SendMessageToChat';
    constructor(public chatroomId: number, public text: string) {}
  }

  export class ReplaceTemporalMessage {
    static readonly type = '[Chat] ReplaceTemporalMessage';
    constructor(public chatroomId: number, public temporaryId: string, public message: MessageModel) {}
  }

  export class SendAttachmentToChat {
    static readonly type = '[Chat] SendAttachmentToChat';
    constructor(public chatroomId: number, public file: File) {}
  }

  export class AddMessageToChat {
    static readonly type = '[Chat] AddMessageToChat';
    constructor(public chatroomId: number, public message: MessageModel) {}
  }

  export class ClearCurrentChat {
    static readonly type = '[Chat] ClearCurrentChat';
    constructor() {}
  }

  export class ReadMessage {
    static readonly type = '[Chat] ReadMessage';
    constructor(public chatroomId: number, public messageId = null) {}
  }

  export class ToggleMutedStatus {
    static readonly type = '[Chat] ToggleMutedStatus';
    constructor(public chatroomId: number) {}
  }

  export class UpdateUploadingProgress {
    static readonly type = '[Chat] UpdateUploadingProgress';
    constructor(public chatroomId: number, public temporaryId: string, public progress: number) {}
  }

  export class FinishProcessing {
    static readonly type = '[Chat] FinishProcessing';
    constructor(public chatroomId: number, public temporaryId: string) {}
  }

  export class UpdateScroll {
    // this action has no reducer. It is used in action handler of component to trigger scroll update
    static readonly type = '[Chat] UpdateScroll';
  }

  export class SearchUsersForNewChat {
    static readonly type = '[Chat] SearchUsersForNewChat';
    constructor(public search = '', public page: number = 1, public limit: number = 15) { }
  }

  export class ShareInMessage {
    static readonly type = '[Chat] ShareInMessage';
    constructor(public users: number[],
                public message: string,
                public entityType: AbstractEntityType,
                public entityId: number,
    ) { }
  }
}
