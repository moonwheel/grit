import { AccountModel } from 'src/app/shared/models';
import { RegisterModel } from 'src/app/shared/models/register.model';

export class Login {
    static readonly type = '[Auth] Login';
    constructor(public payload: { username: string, password: string }) { }
}

export class UpdateTokens {
  static readonly type = '[Auth] Update Token';
  constructor(public payload: { token: string, refreshToken?: string }) { }
}

export class Register {
    static readonly type = '[Auth] Register';
    constructor(public payload: RegisterModel) { }
}

export class Logout {
    static readonly type = '[Auth] Logout';
}

export class ChangeAccount {
    static readonly type = '[Auth] Change Active Account';
    constructor(public account: number | AccountModel) { }
}

export class ChangePassword {
    static readonly type = '[Auth] ChangePassword';
    constructor(public payload: any) { }
}

export class ChangeForgottenPassword {
    static readonly type = '[Auth] Change Forgotten Password';
    constructor(public payload: any) { }
}

export class VerifyPasswordWithAuth {
    static readonly type = '[Auth] VerifyPasswordWithAuth';
    constructor() { }
}

export class VerifyPasswordWithoutAuth {
    static readonly type = '[Auth] VerifyPasswordWithoutAuth';
    constructor(public payload: any) { }
}

export class SendEmailVerification {
  static readonly type = '[Auth] Send Email Verification';
  constructor() { }
}

export class SetAuthLoading {
  static readonly type = '[Auth] SetAuthLoading';
  constructor(public loading: boolean) { }
}
