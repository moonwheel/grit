import { AccountModel } from 'src/app/shared/models';

export class InitUser {
  static readonly type = '[User] Init';
  constructor() { }
}

export class LoadViewableUser {
  static readonly type = '[User] Load Viewable';
  constructor(public username: string) { }
}

export class SetViewableUser {
  static readonly type = '[User] Set Viewable';
  constructor(public user: AccountModel) { }
}

export class ResetViewableUserStore {
  static readonly type = '[User] Reset Viewable User Store';
  constructor() { }
}

export class ResetLoggedInUserStore {
  static readonly type = '[User] Reset Logged In User Store';
  constructor() { }
}

export class UpdateUser {
  static readonly type = '[User] Update';
  constructor(public payload: any) { }
}

export class ChangeUserLanguage {
  static readonly type = '[User] Change Language';
  constructor(public payload: any) { }
}

export class FollowUser {
  static readonly type = '[User] Follow';
  constructor(public id: number) { }
}

export class UnfollowUser {
  static readonly type = '[User] Unfollow';
  constructor(public id: number) { }
}

export class SetViewableAccountFollowingStatus {
  static readonly type = '[User] SetViewableAccountFollowingStatus';
  constructor(public status: string) { }
}

export class GetFollowings {
  static readonly type = '[User] Get Followings';
  constructor(public search = '', public page: number = 1, public limit: number = 15, public lazy: boolean = false) { }
}

export class GetFollowers {
  static readonly type = '[User] Get Followers';
  constructor(public userId: number = null, public search = '', public page: number = 1, public limit: number = 15) { }
}

export class BlockUser {
  static readonly type = '[User] Block';
  constructor(public id: number) { }
}

export class UnblockUser {
  static readonly type = '[User] Unblock';
  constructor(public id: number) { }
}

export class BlockedList {
  static readonly type = '[User] Blocked List';
  constructor() { }
}

export class GetFollowRequests {
  static readonly type = '[User] Get Follow Requests';
  constructor(public offset: number = 0, public limit: number = 3) { }
}

export class AddFollowRequest {
  static readonly type = '[User] Add Follow Request';
  constructor(public payload: any) { }
}

export class ApproveFollowingRequest {
  static readonly type = '[User] Approve Following Request';
  constructor(public followId: number) { }
}

export class RejectFollowingRequest {
  static readonly type = '[User] Reject Following Request';
  constructor(public followId: number) { }
}

export class ChangeUserFollowingRequest {
  static readonly type = '[User] Change User Following Request';
  constructor(public userId: number, public status: string, public followId: number) { }
}

export class ToggleNotificationSetting {
  static readonly type = '[User] ToggleNotificationSetting';
  constructor(public key: 'followers_notification' | 'likes_notification' | 'comments_notification' | 'tags_notification') { }
}

export class ValidateKycDocument {
  static readonly type = '[User] Validate KYC Document';
  constructor(public file: string) { }
}

export class SetLocalLanguage {
  static readonly type = '[User] ChangeLocalLanguage';
  constructor(public language: string) { }
}
