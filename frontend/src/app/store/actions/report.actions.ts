import { AbstarctOrder } from './abstract.actions';

export class InitReport {
  static readonly type = '[Report] Init';
  constructor(
    public limit: number = 15,
    public filter?: object,
    public search?: string,
  ) { }
}

export class LoadReports {
  static readonly type = '[Report] Loading';
  constructor(
    public page: number = 1,
    public limit: number = 15,
    public filter?: object,
    public search?: string,
  ) { }
}

export class ResetReports {
  static readonly type = '[Report] Reset';
  constructor() { }
}

export class AddReport {
  static readonly type = '[Report] Add';
  constructor(public contentType: string, public id: number, public payload: object) { }
}

export class EditReport {
  static readonly type = '[Report] Edit';
  constructor(public id: number, public payload: object) { }
}

export class DeleteReport {
  static readonly type = '[Report] Delete';
  constructor(public id: number) { }
}

export class LoadReportCategories {
  static readonly type = '[Report] Categories';
}

export class LoadReportTypes {
  static readonly type = '[Report] Types';
}
