
export class InitBusiness {
    static readonly type = '[Business] Init';
    constructor() { }
}

export class GetBusinessCategories {
    static readonly type = '[Business] Get Business Categories';
    constructor() { }
}

export class CreateNewBusiness {
    static readonly type = '[Business] Create New Business';
    constructor(public payload: object) { }
}

export class GetBusiness {
    static readonly type = '[Business] Get Business';
    constructor(public id: number) { }
}

export class UpdateBusiness {
    static readonly type = '[Business] Update Business';
    constructor(public payload: object, public id: number, public oldRoute?: string) { }
}

export class RestoreBusiness {
    static readonly type = '[Business] Restore Business';
    constructor(public reactivationToken: string) { }
}

export class DeleteLocalBusiness {
    static readonly type = '[Business] Delete Local Business';
    constructor(public id: number) { }
}
