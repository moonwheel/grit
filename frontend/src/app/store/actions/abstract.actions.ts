import { AbstractEntityModel } from 'src/app/shared/models';
import { CommentModel } from 'src/app/shared/components/actions/comments/comment.model';

export class AbstractAction {
  static readonly type: string;
}

export type AbstarctOrder = 'asc' | 'desc';

// tslint:disable-next-line:no-namespace
export namespace AbstractActions {
  export class Init extends AbstractAction {
    constructor(public limit: number = 15, public filter?: string, public order?: AbstarctOrder) { super(); }
  }

  export class Load extends AbstractAction {
    constructor(public page: number = 1,
                public limit: number = 15,
                public filter?: string,
                public order?: AbstarctOrder,
                public search?: string,
    ) { super(); }
  }

  export class LoadDrafts extends AbstractAction {
    constructor(public page: number = 1, public limit: number = 15, public filter?: string, public order?: AbstarctOrder) { super(); }
  }

  export class GetOne extends AbstractAction {
    constructor(public id: number, public enableLoading = true) { super(); }
  }

  export class Reset extends AbstractAction {
    constructor() { super(); }
  }

  export class Add extends AbstractAction {
    constructor(public payload: any, public dataToUpload?: any) { super(); }
  }

  export class Upload extends AbstractAction {
    constructor(public id: number, public dataToUpload: any) { super(); }
  }

  export class UpdateUploadingProgress extends AbstractAction {
    constructor(public id: number, public progress: number) { super(); }
  }

  export class FinishProcessing extends AbstractAction {
    constructor(public id: number, public payload: any) { super(); }
  }

  export class Edit extends AbstractAction {
    constructor(public payload: any, public dataToUpload?: any) { super(); }
  }

  export class Delete extends AbstractAction {
    constructor(public id: number) { super(); }
  }

  export class LoadLikes extends AbstractAction {
    constructor(public id: number,
                public page: number = 1,
                public limit: number = 15,
                public filter?: string,
                public order?: AbstarctOrder
    ) { super(); }
  }

  export class Like extends AbstractAction {
    constructor(public id: number) { super(); }
  }

  export class LoadComments extends AbstractAction {
    constructor(public id: number,
                public page: number = 1,
                public limit: number = 15,
                public filter?: string,
                public order?: AbstarctOrder
    ) { super(); }
  }

  export class Comment extends AbstractAction {
    constructor(public id: number, public newCommentData: Partial<CommentModel>, public replyTo?: number) { super(); }
  }

  export class EditComment extends AbstractAction {
    constructor(public id: number, public comment: Partial<CommentModel>, public replyTo?: number) { super(); }
  }

  export class LoadCommentReplies extends AbstractAction {
    constructor(public id: number,
                public page: number = 1,
                public limit: number = 15,
                public filter?: string,
                public order?: AbstarctOrder
    ) { super(); }
  }

  export class LoadCommentLikes extends AbstractAction {
    constructor(public id: number,
                public reply: boolean = false,
                public page: number = 1,
                public limit: number = 15,
                public filter?: string,
                public order?: AbstarctOrder
    ) { super(); }
  }

  export class LikeComment extends AbstractAction {
    constructor(public id: number, public reply: boolean = false) { super(); }
  }

  export class DeleteComment extends AbstractAction {
    constructor(public id: number, public reply: boolean = false) { super(); }
  }

  export class InsertItems extends AbstractAction {
    constructor(public newItems: AbstractEntityModel[]) { super(); }
  }

  export class View extends AbstractAction {
    constructor(public id: number) { super(); }
  }

  export class AddToBookmark extends AbstractAction {
    constructor(public id: number, type: string) { super(); }
  }

  export class DeleteFromBookmark extends AbstractAction {
    constructor(public id: number, type: string) { super(); }
  }
}
