import { WalletTransactionType } from "../state/wallet.state";

export namespace WalletActions {
  export class LoadTransactions {
    static readonly type = "[Wallet] Load";
    constructor(
      public page = 1,
      public limit = 15,
      public type: WalletTransactionType | "all",
      public search = "",
      public lazy: boolean = false,
      public filter?: string,
    ) {}
  }

  export class ResetWallet {
      static readonly type = "[Wallet] Reset Wallet";
  }
}
