import { AddressModel } from 'src/app/shared/models/user/user.model';

export class InitAddress {
    static readonly type = '[Address] Init';
    constructor() { }
}

export class LoadAddresses {
    static readonly type = '[Address] Loading';
    constructor() { }
}

export class ResetAddresses {
    static readonly type = '[Address] Reset';
    constructor() { }
}

export class AddAddress {
    static readonly type = '[Address] Add';
    constructor(public payload: AddressModel) { }
}

export class EditAddress {
    static readonly type = '[Address] Edit';
    constructor(public payload: AddressModel, public index?: number) { }
}

export class DeleteAddress {
    static readonly type = '[Address] Delete';
    constructor(public id: number) { }
}
