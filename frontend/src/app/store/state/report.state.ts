import { Router } from '@angular/router';
import { NgZone, Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, Actions, ofActionSuccessful, Store } from '@ngxs/store';

import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { ReportModel } from '../../shared/components/actions/reports/report.model';
import {
  AddReport,
  InitReport,
  LoadReports,
  ResetReports,
  EditReport,
  DeleteReport,
  LoadReportCategories,
  LoadReportTypes,
} from '../actions/report.actions';
import { ReportService } from '../../shared/services/report.service';
import { ResetViewableUserStore } from '../actions/user.actions';


export class ReportStateModel {
  reports: ReportModel[];
  categories: any;
  types: any;
  total: number;
  loading: boolean;
  fetched: boolean;
}

@State<ReportStateModel>({
  name: 'reports',
  defaults: {
    reports: [],
    categories: [],
    types: [],
    total: 0,
    loading: false,
    fetched: false,
  }
})

@Injectable()
export class ReportState {

  @Selector()
  static reports(state: ReportStateModel) {
    return state.reports;
  }

  @Selector()
  static total(state: ReportStateModel) {
    return state.total;
  }

  @Selector()
  static categories(state: ReportStateModel) {
    return state.categories;
  }

  @Selector()
  static types(state: ReportStateModel) {
    return state.types;
  }

  @Selector()
  static loading(state: ReportStateModel) {
    return state.loading;
  }

  constructor(
    private reportService: ReportService,
    private store: Store,
    private actions$: Actions,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new ResetReports());
    });
  }

  @Action(InitReport)
  initReport({ getState, patchState, dispatch }: StateContext<ReportStateModel>, { limit, filter, search }: InitReport) {
    const state = getState();

    if (state.loading) {
      return;
    }

    if (!state.fetched) {
      patchState({ loading: true });
    }

    return dispatch(new LoadReports(1, limit, filter, search));
  }

  @Action(LoadReports)
  load({ patchState }: StateContext<ReportStateModel>, { page, limit, filter, search }: LoadReports) {
    return this.reportService.getReporstList(page, limit, filter, search).pipe(
      tap(data => patchState({
        reports: data.reports,
        total: data.count,
        loading: false,
        fetched: true,
      })),
      catchError(error => {
        console.error(LoadReports.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    );
  }

  @Action(ResetReports)
  reset(ctx: StateContext<ReportStateModel>) {
    ctx.patchState({
      reports: [],
    });
  }

  @Action(AddReport)
  addReport({ getState, setState }: StateContext<ReportStateModel>, { contentType, id, payload }) {
    return this.reportService.addReport(id, payload)
      .pipe(tap((data) => {
          const state = getState();
          setState({
            ...state,
            reports: [
              data,
              ...state.reports,
            ]
          });
        },
        error => {
          console.error('Report Add Action', error);
        }
      ));
  }

  @Action(EditReport)
  editReport({ getState, patchState }: StateContext<ReportStateModel>, { id, payload }) {
    return this.reportService.editReport(id, payload).pipe(
      tap(data => {
        const state = getState();
        const reports = [ ...state.reports ];
        const index = reports.findIndex(item => +item.id === +id);
        if (index > -1) {
          reports[index] = data.report;
          patchState({ reports });
        }
      }, error => {
        console.error('Report Edit Action', error);
      }),
    );
  }

  @Action(DeleteReport)
  deleteReport({ getState, patchState }: StateContext<ReportStateModel>, { id }) {
    return this.reportService.deleteReport(id).pipe(
      tap(() => {
        const state = getState();
        patchState({
          reports: state.reports.filter(item => +item.id !== +id),
          total: state.total - 1,
        });
      }, error => {
        console.log('Report Delete Action', error);
      }),
    );
  }

  @Action(LoadReportCategories)
  loadCategories({ patchState }: StateContext<ReportStateModel>) {
    return this.reportService.getCategories().pipe(
      tap(data => {
        const categories = [{
          id: -1,
          name: 'All',
        }, ...data];
        patchState({ categories });
      }, error => {
        console.log('Report Categories Action', error);
      }),
    );
  }

  @Action(LoadReportTypes)
  loadTypes({ patchState }: StateContext<ReportStateModel>) {
    return this.reportService.getTypes().pipe(
      tap(data => {
        const types = [{
          id: -1,
          name: 'All',
        }, ...data];
        patchState({ types });
      }, error => {
        console.log('Report Types Action', error);
      }),
    );
  }
}
