import { State, Action, StateContext, createSelector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { InitAnnotations, LoadAnnotations, ResetAnnotations } from '../actions/annotation.actions';
import { AnnotationService } from '../../shared/services/annotation.service';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { PhotoAnnotationModel } from 'src/app/shared/models';
import { Utils } from 'src/app/shared/utils/utils';
import { Injectable } from '@angular/core';
import { ResetViewableUserStore } from '../actions/user.actions';

export class AnnotationStateModel {
  annotations: Record<number, PhotoAnnotationModel[]>;
  loading: boolean;
}

@State<AnnotationStateModel>({
  name: 'annotations',
  defaults: {
    annotations: {},
    loading: false,
  }
})

@Injectable()
export class AnnotationState {

  static photoAnnotations(photoId: number) {
    return createSelector([AnnotationState], (state: AnnotationStateModel) => {
      if (photoId in state.annotations) {
        return state.annotations[photoId];
      } else {
        return [];
      }
    });
  }

  constructor(private service: AnnotationService,
              private store: Store,
              private actions$: Actions,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new ResetAnnotations());
    });
  }

  @Action(InitAnnotations)
  init({ patchState, dispatch, getState }: StateContext<AnnotationStateModel>, { targetPhotoId, filter, order }: InitAnnotations) {

    const state = getState();
    if (targetPhotoId in state.annotations) {
      return;
    }

    if (state.loading) {
      return;
    }

    patchState({ loading: true });

    return dispatch(new LoadAnnotations(targetPhotoId, filter, order));
  }

  @Action(LoadAnnotations)
  load({ patchState, getState }: StateContext<AnnotationStateModel>, { targetPhotoId }: LoadAnnotations) {
    return this.service.getAllForPhoto(targetPhotoId).pipe(
      tap(items => {
        const state = getState();
        items = items.map(item => ({
          ...item,
          x: +item.x,
          y: +item.y,
          target: Utils.extendAccountWithBaseInfo(item.target),
        }));
        patchState({
          annotations: {
            ...state.annotations,
            [targetPhotoId]: items
          },
          loading: false,
        });
      }),
      catchError(error => {
        console.error(LoadAnnotations.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    );
  }

  @Action(ResetAnnotations)
  reset(ctx: StateContext<AnnotationStateModel>) {
    ctx.patchState({
      annotations: {},
    });
  }
}
