import { State, Action, StateContext, Selector, Store, ofActionSuccessful, Actions } from '@ngxs/store';
import { tap, map, catchError, switchMap, filter, distinctUntilChanged } from 'rxjs/operators';
import { nanoid } from 'nanoid';

import { AccountModel } from 'src/app/shared/models/user/account.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ChatService } from 'src/app/shared/services';
import { ChatActions } from '../actions/chat.actions';
import { ChatroomModel, ChatroomUserModel, MessageModel } from 'src/app/shared/models/chat';
import { UserState } from './user.state';
import { UserService } from 'src/app/shared/services/user.service';
import { Utils } from 'src/app/shared/utils/utils';
import { throwError, forkJoin, EMPTY, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { NgZone, Injectable } from '@angular/core';
import { InitUser, ResetViewableUserStore } from '../actions/user.actions';
import { AbstractEntityType } from 'src/app/shared/models';
import { UploadStatusMessage } from 'src/app/shared/services/abstract.service';
import { constants } from 'src/app/core/constants/constants';
import { SseService } from 'src/app/shared/services/sse.service';
import { SetGlobalLoading } from '../actions/visible.actions';

export interface ChatStateModel {
  chatroomsIds: number[];
  chatrooms: Record<number, ChatroomModel>;
  loading: boolean;
  fetched: boolean;
  accountForNewChat: AccountModel;
  currentChatroomId: number;
  total: number;
  unreadChatsCount: number;
  searchResults: AccountModel[];
  searchLoading: boolean;
  searchFetched: boolean;
  lazyLoading: boolean;
}

const defaultState: ChatStateModel = {
  chatroomsIds: [],
  chatrooms: {},
  loading: false,
  fetched: false,
  accountForNewChat: null,
  currentChatroomId: 0,
  total: 0,
  unreadChatsCount: 0,
  searchResults: [],
  searchLoading: false,
  searchFetched: false,
  lazyLoading: false,
};

@State<ChatStateModel>({
  name: 'chat',
  defaults: {
    ...defaultState
  }
})

@Injectable()
export class ChatState {

  @Selector()
  static loading(state: ChatStateModel) {
    return state.loading;
  }

  @Selector()
  static chatrooms(state: ChatStateModel) {
    return state.chatroomsIds.map(id => state.chatrooms[id]);
  }

  @Selector()
  static total(state: ChatStateModel) {
    return state.total;
  }

  @Selector()
  static accountForNewChat(state: ChatStateModel) {
    return state.accountForNewChat;
  }

  @Selector()
  static unreadChatsCount(state: ChatStateModel) {
    return state.unreadChatsCount;
  }

  @Selector()
  static currentChatroom(state: ChatStateModel) {
    return state.chatrooms[state.currentChatroomId];
  }

  @Selector([ChatState.currentChatroom])
  static currentChatroomMessages(state: ChatStateModel, currentChatroom: ChatroomModel) {
    return currentChatroom && currentChatroom.messages || [];
  }

  @Selector()
  static searchLoading(state: ChatStateModel) {
    return state.searchLoading;
  }

  @Selector()
  static searchResults(state: ChatStateModel) {
    return state.searchResults;
  }

  @Selector()
  static lazyLoading(state: ChatStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private chatService: ChatService,
    private sseService: SseService,
    private userService: UserService,
    private store: Store,
    private snackbar: MatSnackBar,
    private actions$: Actions,
    private router: Router,
    private zone: NgZone,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(InitUser)
    ).subscribe(() => {
      this.store.dispatch(new ChatActions.LoadUnreadCount());
    });

    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new ChatActions.Reset());
    });
  }

  @Action(ChatActions.Reset)
  reset({ setState }: StateContext<ChatStateModel>) {
    setState({ ...defaultState });
  }

  @Action(ChatActions.ClearCurrentChat)
  clearCurrentChat({ patchState, getState }: StateContext<ChatStateModel>) {
    const state = getState();
    const currentChatroomId = state.currentChatroomId;
    if (currentChatroomId in state.chatrooms) {
      const messages = state.chatrooms[currentChatroomId].messages;
      const start = messages.length > constants.CHAT_MESSAGES_PAGE_SIZE ? messages.length - constants.CHAT_MESSAGES_PAGE_SIZE : 0;
      const end = messages.length;

      patchState({
        accountForNewChat: null,
        currentChatroomId: null,
        chatrooms: {
          ...state.chatrooms,
          [currentChatroomId]: {
            ...state.chatrooms[currentChatroomId],
            messages: messages.slice(start, end)
          }
        }
      });
    } else {
      patchState({
        accountForNewChat: null,
        currentChatroomId: null,
      });
    }
  }

  @Action(ChatActions.Load)
  load({ patchState, getState }: StateContext<ChatStateModel>, { page, limit, lazy }: ChatActions.Load) {
    let state = getState();

    if (!state.fetched) {
      patchState({ loading: true });
    }

    patchState({lazyLoading: lazy})

    return this.chatService.getAll(page, limit).pipe(
      tap(({ items, total }) => {
        state = getState();

        const chatrooms = { ...state.chatrooms };

        const newChatroomsIds = items.map(item => {
          const chatroom = this.getChatroomInfoFromChatroomUser(item);
          chatrooms[chatroom.id] = {
            ...chatrooms[chatroom.id],
            ...chatroom,
          };
          return chatroom.id;
        });

        const chatroomsIds = page === 1 ? newChatroomsIds : [...state.chatroomsIds, ...newChatroomsIds];

        patchState({
          chatroomsIds,
          chatrooms,
          loading: false,
          fetched: true,
          total,
          lazyLoading: false,
        });
      }, error => {
        console.error(ChatActions.Load.type, error);
        patchState({ loading: false, lazyLoading: false });
      })
    );
  }

  @Action(ChatActions.CreateNewLocalChat)
  createNewLocalChat({ patchState, getState }: StateContext<ChatStateModel>, { accountId }: ChatActions.CreateNewLocalChat) {
    return this.store.select(UserState.loggedInAccount).pipe(
      filter(item => !!item),
      switchMap(loggedInAccount => {
        const state = getState();
        const accountForNewChat = state.accountForNewChat;

        const setNewChatWithAccount = (account: AccountModel) => {
          patchState({
            accountForNewChat: account,
            currentChatroomId: 0,
            loading: false,
            chatrooms: {
              ...state.chatrooms,
              0: {
                id: 0,
                title: account.name,
                cover: account.photo,
                thumb: account.thumb,
                link: `/${account.pageName}/home`,
                oponentId: account.id,
                created: null,
                deleted: null,
                chatroom_users: [],
                messages: [],
                attachment: null,
                chatroomUser: {
                  id: 0,
                  is_muted: false,
                  is_blocked: false,
                  created: null,
                  deleted: null,
                  user: loggedInAccount,
                  last_read_message: null,
                  chatroomId: 0,
                }
              }
            }
          });
          this.store.dispatch(new SetGlobalLoading(false));
        };

        if (accountForNewChat && accountForNewChat.id === accountId) {
          setNewChatWithAccount(accountForNewChat);
          return EMPTY;
        } else {
          patchState({ loading: true });
          return this.chatService.findByAccountIds([loggedInAccount.id, accountId]).pipe(
            switchMap(({ chatroom_id }) => {
              if (chatroom_id) {
                return this.store.dispatch(new ChatActions.LoadCurrentChat(chatroom_id));
              } else {
                return this.userService.getById(accountId).pipe(
                  map(user => Utils.extendAccountWithBaseInfo(user)),
                  tap(account => setNewChatWithAccount(account)),
                );
              }
            }),
            catchError(error => {
              patchState({ loading: false });
              console.error(ChatActions.CreateNewLocalChat.type, error);
              return throwError(error);
            })
          );
        }
      })
    );

  }

  @Action(ChatActions.CreateNewRemoteChat)
  createNewRemoteChat({ patchState, getState }: StateContext<ChatStateModel>, { accountId, message }: ChatActions.CreateNewRemoteChat) {
    const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);

    if (loggedInAccount) {
      return this.chatService.startNewChat([loggedInAccount.id, accountId], message).pipe(
        tap(chatroomUser => {
          const chatroom = this.getChatroomInfoFromChatroomUser(chatroomUser);
          const state = getState();
          const index = state.chatroomsIds.findIndex(id => id === chatroom.id);
          const chatroomsIds = index === -1 ? [chatroom.id, ...state.chatroomsIds] : state.chatroomsIds;

          if (chatroom.messages) {
            chatroom.messages = chatroom.messages.map(item => this.extendMessage(item));
          }

          patchState({
            chatrooms: {
              ...state.chatrooms,
              [chatroom.id]: {
                ...state.chatrooms[chatroom.id],
                ...chatroom
              },
            },
            chatroomsIds,
            currentChatroomId: chatroom.id
          });

          this.zone.run(() => this.router.navigateByUrl(`/chats/${chatroom.id}`, { replaceUrl: true }));
        }, error => {
          console.error(ChatActions.CreateNewRemoteChat.type, error);
        })
      );
    }
  }

  @Action(ChatActions.LoadCurrentChat)
  loadCurrentChat({ patchState, getState }: StateContext<ChatStateModel>, { id }: ChatActions.LoadCurrentChat) {
    const state = getState();

    if (id in state.chatrooms) {
      if (state.chatrooms[id].messagesFetched) {
        patchState({ currentChatroomId: id });
        this.store.dispatch(new ChatActions.ReadMessage(id));
      } else {
        patchState({ loading: true });
        return this.chatService.getMessages(id).pipe(
          tap((messages) => {
            messages = messages.map(message => this.extendMessage(message)).reverse();
            patchState({
              chatrooms: {
                ...state.chatrooms,
                [id]: {
                  ...state.chatrooms[id],
                  messages,
                  messagesFetched: true,
                },
              },
              currentChatroomId: id,
              loading: false,
            });

            this.store.dispatch([new ChatActions.ReadMessage(id), new SetGlobalLoading(false)]);
          }, error => {
            patchState({ loading: false });
            console.error(ChatActions.LoadCurrentChat.type, error);
          })
        );
      }
    } else {
      patchState({ loading: true });
      return forkJoin([
        this.chatService.getOne(id),
        this.chatService.getMessages(id),
      ]).pipe(
        tap(([chatroomUser, messages]) => {
          const chatroom = this.getChatroomInfoFromChatroomUser(chatroomUser);
          chatroom.messages = messages.map(message => this.extendMessage(message)).reverse();

          patchState({
            chatrooms: {
              ...state.chatrooms,
              [chatroom.id]: {
                ...state.chatrooms[chatroom.id],
                ...chatroom
              },
            },
            currentChatroomId: id,
            loading: false,
          });

          this.store.dispatch([new ChatActions.ReadMessage(id), new SetGlobalLoading(false)]);
        }, error => {
          patchState({ loading: false });
          console.error(ChatActions.LoadCurrentChat.type, error);
          this.zone.run(() => this.router.navigateByUrl('/chats', { replaceUrl: true }));
        })
      );
    }
  }

  @Action(ChatActions.LoadCurrentChatMessages)
  loadCurrentChatMessages({ patchState, getState }: StateContext<ChatStateModel>, { page, limit, lazy }: ChatActions.LoadCurrentChatMessages) {
    const state = getState();
    const currentChatroomId = state.currentChatroomId;

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.chatService.getMessages(currentChatroomId, page, limit).pipe(
      tap((messages) => {
        const chatroom = state.chatrooms[currentChatroomId];

        messages = messages.map(message => this.extendMessage(message))
          .reverse()
          .concat(chatroom.messages);

        patchState({
          chatrooms: {
            ...state.chatrooms,
            [currentChatroomId]: {
              ...state.chatrooms[currentChatroomId],
              messages
            },
          },
          lazyLoading: false,
        });
      }, error => {
        console.error(ChatActions.LoadCurrentChatMessages.type, error);
      })
    );
  }

  @Action(ChatActions.TryToStartChat)
  tryToStartChat({ patchState }: StateContext<ChatStateModel>, { accountId }: ChatActions.TryToStartChat) {
    const foundLocalChatroom = this.store.selectSnapshot(ChatState.chatrooms).find(chatroom => {
      return chatroom.chatroom_users.some(chatroomUser => chatroomUser.user.id === accountId);
    });

    if (foundLocalChatroom) {
      patchState({ currentChatroomId: foundLocalChatroom.id });
      this.navigate(`/chats/${foundLocalChatroom.id}`);
    } else {
      const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);
      this.store.dispatch(new SetGlobalLoading(true));
      return this.chatService.findByAccountIds([loggedInAccountId, accountId]).pipe(
        tap(({ chatroom_id }) => {
          if (chatroom_id) {
            this.navigate(`/chats/${chatroom_id}`);
          } else {
            this.navigate(`/chats/new?userId=${accountId}`);
          }
        }, error => {
          console.error(ChatActions.TryToStartChat.type, error);
        })
      );
    }
  }

  @Action(ChatActions.AddChat)
  addChat({ getState, patchState }: StateContext<ChatStateModel>, { chatroomUser }: ChatActions.AddChat) {
    const state = getState();
    const chatroom = this.getChatroomInfoFromChatroomUser(chatroomUser);

    if (chatroom.messages) {
      chatroom.messages = chatroom.messages.map(message => this.extendMessage(message));
    }

    if (!(chatroom.id in state.chatrooms)) {
      const chatrooms = { ...state.chatrooms, [chatroom.id]: chatroom };
      const chatroomsIds = [chatroom.id, ...state.chatroomsIds];

      patchState({
        chatrooms,
        chatroomsIds,
      });
    }
  }

  @Action(ChatActions.SendMessageToChat)
  sendMessageToChat({ getState, patchState, dispatch }: StateContext<ChatStateModel>, { chatroomId, text }: ChatActions.SendMessageToChat) {
    const temporaryId = nanoid();
    const user = this.store.selectSnapshot(UserState.loggedInAccount);

    const newMessage: MessageModel = {
      attachment: null,
      chatroom: chatroomId,
      created: new Date(),
      deleted: null,
      id: null,
      is_forwarded: false,
      is_service_message: false,
      reply_to: null,
      shared_article: null,
      shared_photo: null,
      shared_product: null,
      shared_service: null,
      shared_text: null,
      shared_user: null,
      shared_video: null,
      updated: null,
      text,
      user,
      temporaryId,
    };

    const state = getState();
    const chatroom = { ...state.chatrooms[chatroomId] };
    const extendedMessage = this.extendMessage(newMessage);
    const messages = chatroom.messages ? [...chatroom.messages, extendedMessage] : [extendedMessage];

    patchState({
      chatrooms: {
        ...state.chatrooms,
        [chatroomId]: {
          ...chatroom,
          messages,
          last_message: extendedMessage,
        }
      },
    });

    this.chatService.sendMessage(chatroomId, text, temporaryId).pipe(
      switchMap(([newMsg]) => dispatch(new ChatActions.ReplaceTemporalMessage(chatroomId, temporaryId, newMsg)))
    ).subscribe(() => {
      dispatch(new ChatActions.ReadMessage(chatroomId));
    }, error => {
      console.error(ChatActions.SendMessageToChat.type, error);
    });
  }

  @Action(ChatActions.ReplaceTemporalMessage)
  replaceTemporalMessage(
    { getState, patchState }: StateContext<ChatStateModel>,
    { chatroomId, temporaryId, message }: ChatActions.ReplaceTemporalMessage,
  ) {
    const state = getState();
    const chatroom = { ...state.chatrooms[chatroomId] };
    const messages = chatroom.messages ? [...chatroom.messages] : [];
    const foundIndex = messages.findIndex(msg => msg.temporaryId === temporaryId);
    const extendedMessage = this.extendMessage(message);

    if (foundIndex !== -1) {
      messages[foundIndex] = extendedMessage;
    } else {
      messages.push(extendedMessage);
    }

    patchState({
      chatrooms: {
        ...state.chatrooms,
        [chatroomId]: {
          ...chatroom,
          messages,
          last_message: extendedMessage,
        }
      },
    });
  }

  @Action(ChatActions.SendAttachmentToChat)
  sendAttachmentToChat(
    { getState, patchState }: StateContext<ChatStateModel>,
    { chatroomId, file }: ChatActions.SendAttachmentToChat
  ) {
    let type: AbstractEntityType = 'document-attachment';

    if (file.type.includes('image')) {
      type = 'photo-attachment';
    } else
      if (file.type.includes('video')) {
        type = 'video-attachment';
      }

    const state = getState();
    const messages = [...state.chatrooms[chatroomId].messages];
    const temporaryId = nanoid();
    const user = this.store.selectSnapshot(UserState.loggedInAccount);

    messages.push({
      id: null,
      text: '',
      reply_to: null,
      is_forwarded: false,
      is_service_message: false,
      attachment: {
        id: null,
        link: '',
        thumb: '',
        file_name: file.name,
        type: 'document',
        created: new Date(),
        user,
        uploading: true,
        processing: false,
        progress: 0,
      },
      chatroom: chatroomId,
      user,
      created: new Date(),
      deleted: null,
      updated: null,
      own: true,
      temporaryId,
    });

    patchState({
      chatrooms: {
        ...state.chatrooms,
        [chatroomId]: {
          ...state.chatrooms[chatroomId],
          messages,
        }
      }
    });

    this.chatService.uploadFileByChunks(chatroomId, file, type, { chatroomId, temporaryId }).pipe(
      distinctUntilChanged<UploadStatusMessage>((prev, curr) => prev.bytesUploaded === curr.bytesUploaded),
    ).subscribe((event: UploadStatusMessage) => {
      if (event.type === 'progress') {
        const progress = Math.floor(event.bytesUploaded / file.size * 100);
        this.store.dispatch(new ChatActions.UpdateUploadingProgress(chatroomId, temporaryId, progress));
      } else
        if (event.type === 'success') {
          this.store.dispatch(new ChatActions.UpdateUploadingProgress(chatroomId, temporaryId, 100));
        }
    }, error => {
      console.error(ChatActions.SendAttachmentToChat.type, error);
    });
  }

  @Action(ChatActions.UpdateUploadingProgress)
  updateUploadingProgress(
    { getState, patchState }: StateContext<ChatStateModel>,
    { chatroomId, progress, temporaryId }: ChatActions.UpdateUploadingProgress
  ) {
    const state = getState();
    const chatrooms = { ...state.chatrooms };
    const messages = [...state.chatrooms[chatroomId].messages];
    const index = messages.findIndex(item => item.temporaryId === temporaryId);
    const attachment = { ...messages[index].attachment, progress };

    if (progress === 100) {
      attachment.processing = true;
      attachment.uploading = false;
    } else {
      attachment.processing = false;
      attachment.uploading = true;
    }

    messages[index] = {
      ...messages[index],
      attachment,
    };

    chatrooms[chatroomId] = {
      ...chatrooms[chatroomId],
      messages,
    };

    patchState({ chatrooms });
  }

  @Action(ChatActions.LoadUnreadCount)
  loadUnreadCount({ getState, patchState }: StateContext<ChatStateModel>) {
    return this.chatService.getUnreadCount().pipe(
      tap(({ count }) => {
        patchState({ unreadChatsCount: count });
      }, error => {
        console.error(ChatActions.LoadUnreadCount.type, error);
      })
    );
  }

  @Action(ChatActions.AddMessageToChat)
  addMessageToChat({ getState, patchState }: StateContext<ChatStateModel>, { chatroomId, message }: ChatActions.AddMessageToChat) {
    const state = getState();
    const extendedMessage = this.extendMessage({ ...message });
    let observable: Observable<ChatroomModel>;

    if (chatroomId in state.chatrooms) {
      observable = of(state.chatrooms[chatroomId]);
    } else {
      observable = this.chatService.getOne(chatroomId).pipe(
        map(chatroomUser => this.getChatroomInfoFromChatroomUser(chatroomUser))
      );
    }

    return observable.pipe(
      tap(chatroom => {
        let unreadChatsCount = state.unreadChatsCount;
        let messages = chatroom.messages ? [...chatroom.messages] : [];

        const lastReadMessage = chatroom.chatroomUser.last_read_message;
        const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);
        const isCurrentChat = state.currentChatroomId === chatroomId;
        const isChatRead = lastReadMessage && lastReadMessage.id === chatroom.last_message.id;
        const isMyMessage = extendedMessage.user.id === loggedInAccountId;
        const alreadyExistsIndex = messages.findIndex(
          msg => msg.id === extendedMessage.id
            || extendedMessage.temporaryId && msg.temporaryId === extendedMessage.temporaryId
        );

        if (alreadyExistsIndex === -1) {
          messages.push(extendedMessage);

          if (!isCurrentChat && isChatRead && !chatroom.chatroomUser.is_muted && !isMyMessage) {
            unreadChatsCount++;
          }
        } else {
          if (extendedMessage.attachment) {
            extendedMessage.attachment.uploading = false;
            extendedMessage.attachment.processing = false;
            messages = [...messages.filter(msg => msg.temporaryId !== extendedMessage.temporaryId), extendedMessage];
          } else {
            messages[alreadyExistsIndex] = extendedMessage;
          }
        }

        patchState({
          chatrooms: {
            ...state.chatrooms,
            [chatroomId]: {
              ...chatroom,
              messages,
              last_message: extendedMessage,
            }
          },
          unreadChatsCount,
        });

        if (isCurrentChat) {
          this.store.dispatch(new ChatActions.ReadMessage(chatroomId));
        }
      })
    );
  }

  @Action(ChatActions.ReadMessage)
  readMessage({ getState, patchState }: StateContext<ChatStateModel>, { chatroomId, messageId }: ChatActions.ReadMessage) {
    let state = getState();
    let chatroom = state.chatrooms[chatroomId];
    let lastMessage = chatroom.messages[chatroom.messages.length - 1];

    console.log('chatroom', chatroom)

    console.log('lastMessage', lastMessage)

    if (!messageId) {
      messageId = lastMessage.id;
    }

    console.log('messageId', messageId)

    if (chatroom.chatroomUser.last_read_message && chatroom.chatroomUser.last_read_message.id === messageId) {
      return;
    }

    return this.chatService.readMessage(chatroomId, messageId).pipe(
      tap(() => {
        state = getState();
        chatroom = state.chatrooms[chatroomId];
        lastMessage = chatroom.messages[chatroom.messages.length - 1];

        const unreadChatsCount = state.unreadChatsCount ? state.unreadChatsCount - 1 : 0;

        patchState({
          unreadChatsCount,
          chatrooms: {
            ...state.chatrooms,
            [chatroomId]: {
              ...chatroom,
              chatroomUser: {
                ...chatroom.chatroomUser,
                last_read_message: lastMessage,
              }
            }
          }
        });
      }, error => {
        console.error(ChatActions.ReadMessage.type, error);
      })
    );
  }

  @Action(ChatActions.ToggleMutedStatus)
  toggleMutedStatus({ getState, patchState }: StateContext<ChatStateModel>, { chatroomId }: ChatActions.ToggleMutedStatus) {
    const state = getState();
    const chatroom = state.chatrooms[chatroomId];
    const newStatus = !chatroom.chatroomUser.is_muted;

    return this.chatService.setMutedStatus(chatroomId, newStatus).pipe(
      tap(() => {
        patchState({
          chatrooms: {
            ...state.chatrooms,
            [chatroomId]: {
              ...chatroom,
              chatroomUser: {
                ...chatroom.chatroomUser,
                is_muted: newStatus
              }
            }
          }
        });
      }, error => {
        console.error(ChatActions.ToggleMutedStatus.type, error);
      })
    );
  }

  @Action(ChatActions.DeleteChatInit)
  deleteChatInit({ getState, patchState }: StateContext<ChatStateModel>, { chatroomId }: ChatActions.DeleteChatInit) {
    return this.chatService.deleteChatroom(chatroomId);
  }

  @Action(ChatActions.DeleteChat)
  deleteChat({ getState, patchState }: StateContext<ChatStateModel>, { chatroomId }: ChatActions.DeleteChat) {
    const state = getState();
    const currentChatroomId = state.currentChatroomId;
    const chatrooms = { ...state.chatrooms };
    const chatroomsIds = state.chatroomsIds.filter(id => id !== chatroomId);

    delete chatrooms[chatroomId];

    patchState({
      chatrooms,
      chatroomsIds,
      currentChatroomId: currentChatroomId === chatroomId ? null : currentChatroomId,
    });

    if (currentChatroomId === chatroomId) {
      this.zone.run(() => this.router.navigateByUrl('/chats', { replaceUrl: true }));
    }
  }

  @Action(ChatActions.SearchUsersForNewChat, { cancelUncompleted: true })
  searchUsersForNewChat(
    { patchState, getState }: StateContext<ChatStateModel>,
    { page, limit, search }: ChatActions.SearchUsersForNewChat
  ) {
    let state = getState();

    if (!state.searchFetched) {
      patchState({ searchLoading: true });
    }

    let observable: Observable<any[]>;

    if (search) {
      const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);
      observable = this.userService.search(search, page, limit, [loggedInAccountId]).pipe(
        map(items => {
          return items.map(item => Utils.extendAccountWithBaseInfo(item));
        })
      );
    } else {
      observable = this.userService.getFollowings(page, limit, search).pipe(
        map(followingsData => {
          return followingsData.map(({ followedAccount }) => Utils.extendAccountWithBaseInfo(followedAccount));
        })
      );
    }

    return observable.pipe(
      tap(searchResults => {
        if (page === 1) {
          patchState({
            searchResults,
            searchLoading: false,
            searchFetched: true,
          });
        } else {
          state = getState();
          patchState({
            searchResults: [...state.searchResults, ...searchResults],
            searchLoading: false,
            searchFetched: true,
          });
        }
      }, error => {
        patchState({ searchLoading: false });
        console.error(ChatActions.SearchUsersForNewChat.type, error);
      })
    );
  }

  @Action(ChatActions.ShareInMessage)
  shareInMessage(
    { patchState, getState }: StateContext<ChatStateModel>,
    { users, message, entityId, entityType }: ChatActions.ShareInMessage
  ) {
    return this.chatService.share(users, message, entityType, entityId).pipe(
      tap(() => { }, error => {
        console.error(ChatActions.ShareInMessage.type, error);
      })
    );
  }

  private getChatroomInfoFromChatroomUser(chatroomUser: ChatroomUserModel): ChatroomModel {
    const chatroom = chatroomUser.chatroom;
    delete chatroomUser.chatroom;
    chatroom.chatroomUser = chatroomUser;

    if (!chatroom.title && chatroom.chatroom_users[0]) {
      const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);
      const found = chatroom.chatroom_users.find(item => item.user.id !== loggedInAccountId);

      if (found) {
        const opponent = Utils.extendAccountWithBaseInfo(found.user);
        chatroom.title = opponent.name;
        chatroom.cover = opponent.photo;
        chatroom.thumb = opponent.thumb;
        chatroom.oponentId = opponent.id;
        chatroom.link = `/${opponent.pageName}/home`;
      }
    }

    if (!('last_message' in chatroom) && chatroom.messages) {
      chatroom.last_message = chatroom.messages[chatroom.messages.length - 1];
    }

    return chatroom;
  }

  private extendMessage(message: MessageModel, isRaw = false): MessageModel {
    const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);

    const keys = [
      'user',
      'text',
      'photo',
      'video',
      'article',
      'product',
      'service',
    ];

    message.own = +message.user.id === loggedInAccountId;

    if (message.attachment && message.attachment.type === 'photo' && message.attachment.link) {
      const img = new Image();
      img.src = message.attachment.link;

      if (!img.complete) {
        img.onload = () => this.store.dispatch(new ChatActions.UpdateScroll());
      }
    }

    for (const key of keys) {
      const entity = message[`shared_${key}`];

      if (entity) {
        message.sharedContentType = key as AbstractEntityType;
        if (key === 'user') {
          const user = Utils.extendAccountWithBaseInfo(entity);
          message.sharedContentLink = `/${user.pageName.toLocaleLowerCase()}/home`;
          message.sharedContentTitle = user.name;
        } else if (entity) {
          const user = Utils.extendAccountWithBaseInfo(entity.user);
          message.sharedContentLink = `/${user.pageName.toLocaleLowerCase()}/${key}s/${entity.id}`;
          message.sharedContentTitle = entity.title;
        }
      }
    }

    return message;
  }

  private navigate(route: string) {
    this.zone.run(() => this.router.navigateByUrl(route));
  }

}
