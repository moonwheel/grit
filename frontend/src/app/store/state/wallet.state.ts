import { State, Selector, Action, StateContext } from "@ngxs/store";
import { WalletActions } from "../actions/wallet.actions";
import { OrderService } from "../../shared/services/order.service";
import { Injectable } from "@angular/core";
import { forkJoin } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { PaymentService } from "../../shared/services/payment.service";
import { OrderModel } from "src/app/shared/models";

export type WalletTransactionType = "in" | "out";
export type WalletTransactionOrderRef = "purhcases" | "sales";
export interface WalletTransaction {
    orderId: number;
    type: WalletTransactionType;
    date: string;
    amount: string;
    currency: string;
    orderType: "order" | "booking";
    prductId: number;
    refTo: WalletTransactionOrderRef;
    photoUrl: string;
    fullName: string;
    transactionType: "purchase" | "sale" | "refund";
}

export class WalletStateModel {
    transactions: WalletTransaction[];
    loading: boolean;
    lazyLoading: boolean;
    fecthed: boolean;
    total: number;
    searching: boolean;
}

const INITIAL_STATE: WalletStateModel = {
    lazyLoading: false,
    loading: false,
    transactions: [],
    fecthed: false,
    total: 0,
    searching: false,
};

@State<WalletStateModel>({
    name: "wallet",
    defaults: INITIAL_STATE,
})
@Injectable()
export class WalletState {
    private DEFAULT_SERVICE_FILTER = "all";

    @Selector()
    static transactions(state: WalletStateModel) {
        return state.transactions;
    }

    @Selector()
    static loading(state: WalletStateModel) {
        return state.loading;
    }

    @Selector()
    static lazyLoading(state: WalletStateModel) {
        return state.lazyLoading;
    }

    @Selector()
    static fetched(state: WalletStateModel) {
        return state.fecthed;
    }

    @Selector()
    static total(state: WalletStateModel) {
        return state.total;
    }

    @Selector()
    static searching(state: WalletStateModel) {
        return state.searching;
    }

    constructor(
        private service: OrderService,
        private paymentService: PaymentService
    ) { }

    @Action(WalletActions.ResetWallet)
    reset(ctx: StateContext<WalletStateModel>) {
        return ctx.setState(INITIAL_STATE);
    }

    @Action(WalletActions.LoadTransactions)
    load(
        ctx: StateContext<WalletStateModel>,
        action: WalletActions.LoadTransactions
    ) {
        const { type, limit, page, search, filter, lazy } = action;
        ctx.patchState({
            loading: true,
            lazyLoading: lazy,
            searching: search ? true : false,
        });

        return this.getTransactions(page, limit, type, search, filter).pipe(
            tap((transactions: WalletTransaction[]) => {
                const txs = lazy ? [...ctx.getState().transactions, ...transactions] : transactions;
                const totalTransactions = txs.length
                ctx.setState({
                    loading: false,
                    lazyLoading: false,
                    transactions: txs,
                    fecthed: true,
                    total: totalTransactions,
                    searching: false,
                })
            }
            )
        );
    }

    private getTransactions(
        page: number,
        limit: number,
        type: WalletTransactionType | "all",
        search: string = "",
        filter: string = ""
    ) {
        const forkMapBuild: Record<string, any> = {
            all: () => {
                const itemsLimit = Math.floor(limit / 2);
                return forkJoin(
                    this.service.getSales(
                        page,
                        itemsLimit,
                        this.DEFAULT_SERVICE_FILTER,
                        search
                    ),
                    this.service.getPurchases(
                        page,
                        itemsLimit,
                        this.DEFAULT_SERVICE_FILTER,
                        search
                    )
                );
            },
            in: () =>
                forkJoin(
                    this.service.getSales(
                        page,
                        limit,
                        this.DEFAULT_SERVICE_FILTER,
                        search
                    )
                ),
            out: () =>
                forkJoin(
                    this.service.getPurchases(
                        page,
                        limit,
                        this.DEFAULT_SERVICE_FILTER,
                        search
                    )
                ),
        };

        const forkMapItem = forkMapBuild[type];

        if (!forkMapItem) throw new Error("Unssuported type: " + type);

        return forkMapItem().pipe(
            map((orders: any[]) => {
                let transactions: WalletTransaction[] = [];

                if (orders.length === 2) {
                    const [in_orders, out_orders] = orders;

                    const mappedOrders = Array.prototype.concat(
                        this.buildTransactionsList(in_orders.items, "in"),
                        this.buildTransactionsList(out_orders.items, "out")
                    );

                    transactions = mappedOrders;
                } else {
                    transactions = this.buildTransactionsList(
                        orders[0].items,
                        type as WalletTransactionType
                    );
                }

                return transactions;
            }),
            map((transactions: WalletTransaction[]) =>
                transactions.sort(this.descSortTransactions)
            ),
            map((transactions: WalletTransaction[]) => {
                if (filter) {
                    return this.filter(filter, transactions);
                }
                return transactions;
            })
        );
    }

    private descSortTransactions = (
        a: WalletTransaction,
        b: WalletTransaction
    ) => {
        const a_date = +new Date(a.date);
        const b_date = +new Date(b.date);

        return b_date - a_date;
    };

    private buildTransactionsList = (
        orders: OrderModel[],
        type?: WalletTransactionType
    ): WalletTransaction[] => {
        const mappedOrders: WalletTransaction[] = [];

        for (let i = 0; i < orders.length; i++) {
            const order = orders[i];

            let item = {} as WalletTransaction;

            item.orderId = order.id;
            item.amount =
                type == "in"
                    ? String(order.total_price - Number(order.fee))
                    : String(order.total_price);

            item.amount = Number(item.amount).toFixed(2)
            item.date = order.pay_in_date;
            item.currency = "EUR";
            item.type = type;
            item.orderType = order.type as WalletTransaction["orderType"];
            item.refTo =
                type == "in" ? "sales" : ("purchases" as WalletTransactionOrderRef);
            item.photoUrl = this.pickPhotoUrl(order, type);
            item.fullName = this.pickFullName(order, type);
            item.transactionType = type == "in" ? "sale" : "purchase";

            if (order.type === "order") {
                if (order.order_items.every((item) => item.refunded)) {
                    const refundedOrderType = type === "in" ? "out" : "in";

                    item = {
                        ...item,
                        type: refundedOrderType,
                        amount: Number(String(order.total_price - Number(order.fee))).toFixed(2),
                        transactionType: "refund",
                    };
                } else {
                    order.order_items.forEach((orderItem) => {
                        if (!orderItem.refunded) return;

                        item = {
                            ...item,
                            date: orderItem.refunded as any, //
                            amount: orderItem.price.toFixed(2),
                            transactionType: "refund",
                        };

                        mappedOrders.push(item);
                    });
                }
            }

            if (order.type === "booking") {
                if (order.refunded) {
                    item = {
                        ...item,
                        type: type === "in" ? "out" : "in",
                        transactionType: "refund",
                    };

                    mappedOrders.push(item);
                }
            }

            mappedOrders.push(item);
        }

        return mappedOrders;
    };

    private pickPhotoUrl(order: OrderModel, type: WalletTransactionType): string {
        let photoUrl = "";

        if (type === "in") {
            photoUrl = order.buyer.person.photo;
        }

        if (type === "out") {
            photoUrl = order.seller.person.photo;
        }

        return photoUrl;
    }

    private pickFullName(order: OrderModel, type: WalletTransactionType): string {
        let fullName = "";

        if (type === "in") {
            fullName = order.buyer.person.fullName;
        }

        if (type === "out") {
            fullName = order.seller.person.fullName;
        }

        return fullName;
    }

    private filter(
        filterType: string,
        txs: WalletTransaction[]
    ): WalletTransaction[] {
        const filterMap = {
            payin: () => txs.filter((tx) => tx.type == "in"),
            payout: () => txs.filter((tx) => tx.type == "out"),
            all: () => txs,
        };

        const filter = filterMap[filterType];

        if (!filter)
            throw new Error(`FilterType: ${filterType} is not supported filter.`);

        const filterResult = filter();

        return filterResult;
    }
}
