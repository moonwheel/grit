import { State, Action, StateContext, Store, createSelector, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { PhotoModel } from 'src/app/shared/models/content/photo.model';
import { PhotoService } from 'src/app/shared/services/photo.service';
import { PhotoActions } from '../actions/photo.actions';
import { AbstractState, AbstractStateModel } from './abstract.state';
import { UploadStatusMessage } from 'src/app/shared/services/abstract.service';
import { SseService } from 'src/app/shared/services/sse.service';
import { Injectable } from '@angular/core';

export interface PhotoStateModel extends AbstractStateModel<PhotoModel> {
    items: {
        [id: number]: PhotoModel
    };
}

@State<PhotoStateModel>({
    name: 'photos',
    defaults: {
        ...AbstractState.getDefaultState(),
    }
})

@Injectable()
export class PhotoState extends AbstractState {

    @Selector()
    static photos(state: PhotoStateModel) {
        return super.getAllSelector(state);
    }

    @Selector()
    static drafts(state: PhotoStateModel) {
        return super.getDraftsSelector(state);
    }

    @Selector()
    static total(state: PhotoStateModel) {
        return super.totalSelector(state);
    }

    @Selector()
    static loading(state: PhotoStateModel) {
        return super.loadingSelector(state);
    }

    @Selector()
    static uploading(state: PhotoStateModel) {
        return super.uploadingSelector(state);
    }

    @Selector()
    static lazyLoading(state: PhotoStateModel) {
        return state.fetching;
    }

    static photo(id: number) {
        return super.getOneSelector<PhotoModel>(PhotoState, id);
    }

    static likes(id: number) {
        return super.likesSelector(PhotoState, id);
    }

    static comments(id: number) {
        return super.commentsSelector(PhotoState, id);
    }

    static comment(commentId: number) {
        return super.commentSelector(PhotoState, commentId);
    }

    static replies(commentId: number) {
        return super.repliesSelector(PhotoState, commentId);
    }

    constructor(
        protected photoService: PhotoService,
        protected store: Store,
        protected sseService?: SseService,
    ) {
        super(photoService, store);

        this.sseService.fileProcessingComplete$.subscribe(({ entityType, entity }) => {
            if (entityType === 'photo') {
                this.store.dispatch(new PhotoActions.FinishProcessing(entity.id, entity));
            }
        });
    }

    @Action(PhotoActions.Init)
    init(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Init) {
        return super.init(ctx, action, PhotoActions);
    }

    @Action(PhotoActions.Load)
    load(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Load) {
        if(action.lazy) {
            ctx.patchState({fetching: true})
        }

        return super.load(ctx, action, PhotoActions.Load.type);
    }

    @Action(PhotoActions.LoadDrafts)
    loadDrafts(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LoadDrafts) {
        return super.loadDrafts(ctx, action, PhotoActions.LoadDrafts.type);
    }

    @Action(PhotoActions.GetOne)
    getOne(ctx: StateContext<PhotoStateModel>, action: PhotoActions.GetOne) {
        return super.getOne(ctx, action, PhotoActions.GetOne.type);
    }

    @Action(PhotoActions.Reset)
    reset(ctx: StateContext<PhotoStateModel>) {
        return super.reset(ctx);
    }

    @Action(PhotoActions.Add)
    add(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Add) {
        return super.add(ctx, action, PhotoActions.Add.type, PhotoActions);
    }

    @Action(PhotoActions.Upload)
    upload(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Upload) {
        return super.uploadOneFile(ctx, action, 'photo', PhotoActions.Upload.type, PhotoActions);
    }

    @Action(PhotoActions.UpdateUploadingProgress)
    updateUploadingProgress(ctx: StateContext<PhotoStateModel>, action: PhotoActions.UpdateUploadingProgress) {
        return super.updateUploadingProgress(ctx, action);
    }

    @Action(PhotoActions.FinishProcessing)
    finishProcessing(ctx: StateContext<PhotoStateModel>, action: PhotoActions.FinishProcessing) {
        return super.finishProcessing(ctx, action);
    }

    @Action(PhotoActions.Edit)
    edit(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Edit) {
        return super.edit(ctx, action, PhotoActions.Edit.type, PhotoActions);
    }

    @Action(PhotoActions.Delete)
    delete(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Delete) {
        return super.delete(ctx, action, PhotoActions.Delete.type);
    }

    @Action(PhotoActions.Like)
    like(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Like) {
        return super.like(ctx, action, PhotoActions.Like.type);
    }

    @Action(PhotoActions.LoadLikes)
    loadLikes(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LoadLikes) {
        return super.loadLikes(ctx, action, PhotoActions.LoadLikes.type);
    }

    @Action(PhotoActions.Comment)
    comment(ctx: StateContext<PhotoStateModel>, action: PhotoActions.Comment) {
        return super.comment(ctx, action, PhotoActions.Comment.type);
    }

    @Action(PhotoActions.EditComment)
    editComment(ctx: StateContext<PhotoStateModel>, action: PhotoActions.EditComment) {
        return super.editComment(ctx, action, PhotoActions.EditComment.type);
    }

    @Action(PhotoActions.LoadComments)
    loadComments(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LoadComments) {
        return super.loadComments(ctx, action, PhotoActions.LoadComments.type);
    }

    @Action(PhotoActions.LoadCommentReplies)
    loadCommentReplies(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LoadCommentReplies) {
        return super.loadCommentReplies(ctx, action, PhotoActions.LoadCommentReplies.type);
    }

    @Action(PhotoActions.LikeComment)
    likeComment(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LikeComment) {
        return super.likeComment(ctx, action, PhotoActions.LikeComment.type);
    }

    @Action(PhotoActions.LoadCommentLikes)
    loadCommentLikes(ctx: StateContext<PhotoStateModel>, action: PhotoActions.LoadCommentLikes) {
        return super.loadCommentLikes(ctx, action, PhotoActions.LoadCommentLikes.type);
    }

    @Action(PhotoActions.DeleteComment)
    deleteComment(ctx: StateContext<PhotoStateModel>, action: PhotoActions.DeleteComment) {
        return super.deleteComment(ctx, action, PhotoActions.DeleteComment.type);
    }

    @Action(PhotoActions.InsertItems)
    insertItems(ctx: StateContext<PhotoStateModel>, action: PhotoActions.InsertItems) {
        return super.insertItems(ctx, action);
    }

    @Action(PhotoActions.View)
    view(ctx: StateContext<PhotoStateModel>, action: PhotoActions.View) {
        return super.view(ctx, action, PhotoActions.View.type);
    }

    @Action(PhotoActions.AddToBookmark)
    addToBookmark(ctx: StateContext<PhotoStateModel>, action: PhotoActions.AddToBookmark) {
      return super.addToBookmark(ctx, action, PhotoActions.AddToBookmark.type);
    }

    @Action(PhotoActions.DeleteFromBookmark)
    deleteFromBookmark(ctx: StateContext<PhotoStateModel>, action: PhotoActions.DeleteFromBookmark) {
      return super.deleteFromBookmark(ctx, action, PhotoActions.DeleteFromBookmark.type);
    }
}
