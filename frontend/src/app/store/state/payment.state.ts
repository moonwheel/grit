import { State, Action, StateContext, Selector, createSelector, Select } from '@ngxs/store';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { BankAccountModel } from '../../shared/models/user/user.model';
import { PaymentService } from '../../shared/services/payment.service';
import {
  InitPayment,
  LoadPayments,
  ResetPayments,
  AddBankAccount,
  EditBankAccount,
  DeleteBankAccount,
  AddCreditCard,
  EditCreditCard,
  DeleteCreditCard,
  GetWallet,
  CardDirectPayIn,
  BankwireDirectPayIn,
} from '../actions/payment.actions';
import { WalletModel } from 'src/app/shared/models';
import { Injectable } from '@angular/core';

export class PaymentsStateModel {
  payments: any[];
  wallet: WalletModel;
  loading: boolean;
}

@State<PaymentsStateModel>({
  name: 'payments',
  defaults: {
    payments: [],
    wallet: {
      balance: 0,
      currency: 'EUR',
    },
    loading: false,
  }
})

@Injectable()
export class PaymentsState {

  @Selector()
  static wallet(state: PaymentsStateModel): WalletModel {
    return state.wallet;
  }

  @Selector()
  static balance(state: PaymentsStateModel): number {
    return state.wallet.balance;
  }

  @Selector()
  static payments(state: PaymentsStateModel) {
    return state.payments;
  }

  static payment(id: number) {
    return createSelector([PaymentsState.payments], (payments: any[]) => {
        const found = payments.find(payment => payment.id === id);
        return found && { ...found };
    });
  }

  @Selector()
  static loading(state: PaymentsStateModel) {
    return state.loading;
  }

  constructor(
      private paymentService: PaymentService,
      private router: Router,
      private snackBar: MatSnackBar
  ) { }

  @Action(InitPayment)
  init({ patchState, getState, dispatch }: StateContext<PaymentsStateModel>) {
    const state = getState();
    if (state.loading) {
        return;
    }

    patchState({ loading: true });

    return dispatch(new LoadPayments());
  }

  @Action(LoadPayments)
  load({ patchState }: StateContext<PaymentsStateModel>) {
    return this.paymentService.getAll().pipe(
      tap(data => {
        patchState({
          payments: [
            ...data.bankAccounts,
            ...data.cards,
          ],
          loading: false,
        });
      }),
      catchError(error => {
          console.error(LoadPayments.type, error);
          patchState({ loading: false });
          return throwError(error);
      })
    );
  }

  @Action(ResetPayments)
  reset(ctx: StateContext<PaymentsStateModel>) {
    ctx.patchState({
      payments: [],
    });
  }

  @Action(AddBankAccount)
  addAccount({ getState, patchState, setState }: StateContext<PaymentsStateModel>, { payload }: AddBankAccount) {

    patchState({ loading: true });

    return this.paymentService.addBankAccount(payload).pipe(tap((data) => {
      const state = getState();
      setState({
          ...state,
          payments: [
            ...state.payments,
            data,
          ],
          loading: false,
      });
    },
      error => {
        patchState({ loading: false });
        console.error('Payment Add Bank Account Action', error);
      }
    ));
  }

  @Action(AddCreditCard)
  addCreditCard({ getState, patchState, setState }: StateContext<PaymentsStateModel>, { payload }: AddCreditCard) {

    patchState({ loading: true });
    
    return this.paymentService.addCreditCard(payload).pipe(tap((data) => {
      const state = getState();
      setState({
          ...state,
          payments: [
            ...state.payments,
            data
          ],
          loading: false,
      });
    },
      error => {
        patchState({ loading: false });
        console.error('Payment Add Credit Card Action', error);
      }
    ));
  }

  @Action(EditBankAccount)
  editBankAccount({ getState, patchState }: StateContext<PaymentsStateModel>, { payload }: EditBankAccount) {
    return this.paymentService.editBankAccount(payload).pipe(
      tap(data => {
        const state = getState();
        const payments = [ ...state.payments ];
        const index = payments.findIndex(item => item.id === payload.id);
        if (index > -1) {
          payments[index] = data;
        }
        patchState({ payments });
      }, error => {
        console.error('Payment Edit Bank Account Action', error);
      }),
    );
  }

  @Action(EditCreditCard)
  editCreditCard({ getState, patchState }: StateContext<PaymentsStateModel>, { payload }: EditCreditCard) {
    return this.paymentService.editCreditCard(payload).pipe(
      tap(data => {
        const state = getState();
        const payments = [ ...state.payments ];
        const index = payments.findIndex(item => item.id === payload.id);
        if (index > -1) {
          payments[index] = data;
        }
        patchState({ payments });
      }, error => {
        console.error('Payment Edit Credit Card Action', error);
      }),
    );
  }

  @Action(DeleteBankAccount)
  deleteBankAccount({ getState, patchState }: StateContext<PaymentsStateModel>, { id }: DeleteBankAccount) {
    return this.paymentService.deleteBankAccount(id).pipe(
      tap(() => {
        const state = getState();
        patchState({
          payments: state.payments.filter(payment => payment.id !== id),
        });
      }, error => {
        console.error('Payment Delete Bank Account Action', error);
      }),
    );
  }

  @Action(DeleteCreditCard)
  deleteCreditCard({ getState, patchState }: StateContext<PaymentsStateModel>, { id }: DeleteCreditCard) {
    return this.paymentService.deleteBankAccount(id).pipe(
      tap(() => {
        const state = getState();
        patchState({
          payments: state.payments.filter(payment => payment.id !== id),
        });
      }, error => {
        console.error('Payment Delete Credit Card Action', error);
      }),
    );
  }

  @Action(GetWallet)
  getWallet({ patchState }: StateContext<PaymentsStateModel>) {

    patchState({loading: true})

    return this.paymentService.getWallet().pipe(
      tap(wallet => {
        patchState({ wallet, loading: false });
      }, error => {
        console.error('Get Wallet Action: ', error);
        patchState({loading: false})
      }),
    );
  }
  
  @Action(CardDirectPayIn)
  cardDirectPayIn({ patchState }: StateContext<PaymentsStateModel>, { card, amount }: CardDirectPayIn) {
    return this.paymentService.cardDirectPayIn(card, amount).pipe(
      tap(data => {
        console.log('CardDirectPayIn DATA: ', data);
      }, error => {
        console.error('Card Direct PayIn Action: ', error);
      }),
    );
  }

  @Action(BankwireDirectPayIn)
  bankwireDirectPayIn({ patchState }: StateContext<PaymentsStateModel>, { bank, amount }: BankwireDirectPayIn) {
    return this.paymentService.bankwireDirectPayIn(bank, amount).pipe(
      tap(data => {
        console.log('BankwireDirectPayIn DATA: ', data);
      }, error => {
        console.error('Bankwire Direct PayIn Action: ', error);
      }),
    );
  }
}
