import { State, Action, StateContext, Selector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { TimelineActions } from '../actions/timeline.actions';
import { NgZone, Injectable } from '@angular/core';

import { SseService } from 'src/app/shared/services/sse.service';
import { FeedItemModel, AbstractEntityType, AbstractEntityModel, AccountModel } from 'src/app/shared/models';
import { PhotoActions } from '../actions/photo.actions';
import { VideoActions } from '../actions/video.actions';
import { ArticleActions } from '../actions/article.actions';
import { ServiceActions } from '../actions/service.actions';
import { TextActions } from '../actions/text.actions';
import { TextState, TextStateModel } from './text.state';
import { PhotoState, PhotoStateModel } from './photo.state';
import { VideoState, VideoStateModel } from './video.state';
import { ArticleState, ArticleStateModel } from './article.state';
import { AbstractActions } from '../actions/abstract.actions';
import { UserState } from './user.state';
import { ServiceState, ServiceStateModel } from './service.state';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { Utils } from 'src/app/shared/utils/utils';
import { ResetViewableUserStore } from '../actions/user.actions';

export class TimelineStateModel {
  items: FeedItemModel[];
  loading: boolean;
  fetched: boolean;
  lazyLoading: boolean;
}

@State<TimelineStateModel>({
  name: 'timeline',
  defaults: {
    items: [],
    loading: false,
    fetched: false,
    lazyLoading: false,
  }
})

@Injectable()
export class TimelineState {

  @Selector()
  static lazyLoading(state: TimelineStateModel) {
    return state.lazyLoading;
  }

  @Selector([
    TextState,
    PhotoState,
    VideoState,
    ArticleState,
    ServiceState,
  ])
  static items(
    feedState: TimelineStateModel,
    textState: TextStateModel,
    photoState: PhotoStateModel,
    videoState: VideoStateModel,
    articleState: ArticleStateModel,
    serviceState: ServiceStateModel,
  ) {
    const items: AbstractEntityModel[] = [];

    for (const item of feedState.items) {
      if (item.tableName === 'text' && item.id in textState.items) {
        items.push(textState.items[item.id]);
      } else
      if (item.tableName === 'photo' && item.id in photoState.items) {
        items.push(photoState.items[item.id]);
      } else
      if (item.tableName === 'video' && item.id in videoState.items) {
        items.push(videoState.items[item.id]);
      } else
      if (item.tableName === 'article' && item.id in articleState.items) {
        items.push(articleState.items[item.id]);
      } else
      if (item.tableName === 'service' && item.id in serviceState.items) {
        items.push(serviceState.items[item.id]);
      }
    }

    return items;
  }

  @Selector()
  static loading(state: TimelineStateModel) {
    return state.loading;
  }

  constructor(
    private userService: UserService,
    private actions$: Actions,
    private router: Router,
    private store: Store,
    private zone: NgZone,
    private sseService: SseService,
  ) {
    this.subscribeToItemDeletion(PhotoActions.Delete, 'photo');
    this.subscribeToItemDeletion(VideoActions.Delete, 'video');
    this.subscribeToItemDeletion(ArticleActions.Delete, 'article');
    this.subscribeToItemDeletion(TextActions.Delete, 'text');
    this.subscribeToItemDeletion(ServiceActions.Delete, 'service');

    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new TimelineActions.Reset());
    });
  }

  @Action(TimelineActions.Load)
  load({ patchState, getState }: StateContext<TimelineStateModel>, { page, limit, lazy }: TimelineActions.Load) {
    let state = getState();
    const viewableAccount = this.store.selectSnapshot(UserState.viewableAccount);
    const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);
    const userId = viewableAccount.id === loggedInAccount.id ? null : viewableAccount.id;

    if (!state.fetched) {
      patchState({ loading: true });
    }

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.userService.getTimeline(page, limit, userId).pipe(
      tap((result: AbstractEntityModel[]) => {
        state = getState();

        const texts = [];
        const photos = [];
        const videos = [];
        const articles = [];
        const services = [];

        let items: FeedItemModel[] = result.map(item => {
          const extended = this.handleItem(item, viewableAccount);

          if (item.tableName === 'text') {
            texts.push(extended);
          } else
          if (item.tableName === 'photo') {
            photos.push(extended);
          } else
          if (item.tableName === 'video') {
            videos.push(extended);
          } else
          if (item.tableName === 'article') {
            articles.push(extended);
          } else
          if (item.tableName === 'service') {
            services.push(extended);
          }

          return {
            id: extended.id,
            tableName: extended.tableName,
          };
        });

        if (texts.length) {
          this.store.dispatch(new TextActions.InsertItems(texts));
        }

        if (photos.length) {
          this.store.dispatch(new PhotoActions.InsertItems(photos));
        }

        if (videos.length) {
          this.store.dispatch(new VideoActions.InsertItems(videos));
        }

        if (articles.length) {
          this.store.dispatch(new ArticleActions.InsertItems(articles));
        }

        if (services.length) {
          this.store.dispatch(new ServiceActions.InsertItems(services));
        }

        if (page > 1) {
          items = [ ...state.items, ...items ];
        } else {
          items = items;
        }

        patchState({ items, loading: false, fetched: true, lazyLoading: false, });
      }, error => {
        console.error(TimelineActions.Load, error);
        patchState({ loading: false, lazyLoading: false });
      })
    );
  }

  @Action(TimelineActions.DeleteItem)
  delete({ patchState, getState }: StateContext<TimelineStateModel>, { id, tableName }: TimelineActions.DeleteItem) {
    const state = getState();
    const items = [ ...state.items ];
    const index = items.findIndex(item => item.id === Number(id) && item.tableName === tableName);

    if (index !== -1) {
      items.splice(index, 1);
    }

    patchState({ items });
  }

  @Action(TimelineActions.Reset)
  reset({ patchState }: StateContext<TimelineStateModel>) {
    patchState({ items: [], loading: false, fetched: false });
  }

  private subscribeToItemDeletion(DeleteAction: typeof AbstractActions.Delete, tableName: AbstractEntityType) {
    this.actions$.pipe(
      ofActionSuccessful(DeleteAction)
    ).subscribe(({ id }) => {
      this.store.dispatch(new TimelineActions.DeleteItem(id, tableName));
    });
  }

  private handleItem(item: AbstractEntityModel, account: AccountModel): AbstractEntityModel {
    item.id = Number(item.id);
    item.likesCount = Number(item.likesCount);
    item.viewsCount = Number(item.viewsCount);
    item.commentsCount = Number(item.commentsCount);
    item.user = Utils.extendAccountWithBaseInfo(account);

    if (item.tableName === 'service') {
      (item as ServiceModel).schedule = {
        byDate: [
          { from: item.date_from, to: item.date_to } as any
        ],
        byDays: [],
        duration: item.duration,
        id: null,
        created: null,
        updated: null,
        deleted: null,
        break: null,
      };
    }

    return item;
  }

}
