import { tap, withLatestFrom, switchMap, catchError, finalize, distinctUntilChanged } from 'rxjs/operators';

import { StateContext, createSelector, Store, Selector } from '@ngxs/store';

import { AccountModel, AbstractEntityModel, AbstractEntityType } from 'src/app/shared/models';
import { Utils } from 'src/app/shared/utils/utils';
import { UserState } from './user.state';
import { CommentModel } from 'src/app/shared/components/actions/comments/comment.model';
import { AbstractActions } from '../actions/abstract.actions';
import { AbstractService, UploadStatusMessage } from 'src/app/shared/services/abstract.service';
import { concat, throwError } from 'rxjs';

export type AbstractMapperFunction = (arg: AbstractEntityModel, itemToCompare?: AbstractEntityModel) => AbstractEntityModel;

/**
 * Models
 */

export interface AbstractStateModel<T extends AbstractEntityModel = AbstractEntityModel> {
  ids: number[];
  draftIds: number[];
  total: number;
  totalFound: number;

  currentItem: T;

  items: {
    [id: number]: T
  };
  deletedItems: {
    [id: number]: T
  };
  likes: {
    [id: number]: AccountModel
  };
  comments: {
    [id: number]: CommentModel
  };
  deletedComments: {
    [id: number]: CommentModel
  };
  replies: {
    [id: number]: CommentModel
  };
  commentLikes: {
    [id: number]: AccountModel
  };

  loading: boolean;
  fetched: boolean;
  draftsFetched: boolean;

  likesLoading: boolean;
  commentsLoading: boolean;
  repliesLoading: boolean;
  commenting: boolean;
  fetching: boolean;
}

/**
 * State
 */

export abstract class AbstractState {

  static getDefaultState<T extends AbstractStateModel = AbstractStateModel>(): T {
    return {
      ids: [],
      draftIds: [],
      items: {},
      deletedItems: {},
      likes: {},
      comments: {},
      deletedComments: {},
      replies: {},
      commentLikes: {},
      total: 0,
      totalFound: 0,
      currentItem: null,
      loading: false,
      fetched: false,
      draftsFetched: false,
      likesLoading: false,
      commentsLoading: false,
      repliesLoading: false,
      commenting: false,
      fetching: false,
    } as T;
  }

  /**
   * Decorated selectors. These selectors should be decorated with @Selector() decorator in child class
   */

  static getAllSelector(state: AbstractStateModel) {
    return state.ids.map(id => state.items[id]);
  }

  static getCurrentSelector(state: AbstractStateModel) {
    return state.currentItem;
  }

  static getDraftsSelector(state: AbstractStateModel) {
    return state.draftIds.map(id => state.items[id]);
  }

  static totalSelector(state: AbstractStateModel) {
    return state.total;
  }

  static totalFoundSelector(state: AbstractStateModel) {
    return state.totalFound;
  }

  static loadingSelector(state: AbstractStateModel) {
    return state.loading;
  }

  static uploadingSelector(state: AbstractStateModel) {
    return state.ids.some(id => state.items[id] && state.items[id].uploading);
  }

  /**
   * Memoized selectors with parameters
   */

  static getOneSelector<P extends AbstractEntityModel, T extends typeof AbstractState = typeof AbstractState>(stateClass: T, id: number) {
    return createSelector([stateClass], (state: AbstractStateModel<P>) => state.items[id]);
  }

  static getManySelector<P extends AbstractEntityModel, T extends typeof AbstractState = typeof AbstractState>(stateClass: T) {
    return createSelector([stateClass], (state: AbstractStateModel<P>) => state.ids.map(id => state.items[id]));
  }

  static likesSelector<T extends typeof AbstractState>(stateClass: T, id: number) {
    const itemSelector = AbstractState.getOneSelector(stateClass, id);
    return createSelector([stateClass, itemSelector], (state: AbstractStateModel, entity: AbstractEntityModel) => {
      if (!entity || !entity.likes) {
        return [];
      }
      return entity.likes.map(item => state.likes[item]);
    });
  }

  static commentsSelector<T extends typeof AbstractState>(stateClass: T, id: number) {
    const itemSelector = AbstractState.getOneSelector(stateClass, id);

    return createSelector([stateClass, itemSelector], (state: AbstractStateModel, entity: AbstractEntityModel) => {
      if (!entity || !entity.comments) {
        return [];
      }
      return entity.comments.map(item => state.comments[item]);
    });
  }

  static commentSelector<T>(stateClass: T, id: number, reply = false) {
    return createSelector([stateClass], (state: AbstractStateModel) => {
      if (!reply) {
        return state.comments[id];
      } else {
        return state.replies[id];
      }
    });
  }

  static repliesSelector<T>(stateClass: T, commentId: number) {
    return createSelector([stateClass], (state: AbstractStateModel) => {
      const comment = state.comments[commentId];

      if (!comment || !comment.replies) {
        return [];
      }
      return comment.replies.map(item => state.replies[item]);
    });
  }

  static commentLikesSelector<T>(stateClass: T, commentId: number, reply = false) {
    return createSelector([stateClass], (state: AbstractStateModel) => {
      const comment = reply ? state.replies[commentId] : state.comments[commentId];

      if (!comment || !comment.likes) {
        return [];
      }

      return comment.likes.map(item => state.commentLikes[item]);
    });
  }

  /**
   * These memoized selectors are only for AbstractState static calls
   */

  static likesLoadingSelector<T>(stateClass: T) {
    return createSelector([stateClass], (state: AbstractStateModel) => state.likesLoading);
  }

  static commentsLoadingSelector<T>(stateClass: T) {
    return createSelector([stateClass], (state: AbstractStateModel) => state.commentsLoading);
  }

  static repliesLoadingSelector<T>(stateClass: T) {
    return createSelector([stateClass], (state: AbstractStateModel) => state.repliesLoading);
  }

  static commentingSelector<T>(stateClass: T) {
    return createSelector([stateClass], (state: AbstractStateModel) => state.commenting);
  }

  constructor(protected service: AbstractService,
              protected store: Store,
  ) {
  }

  protected init(
    { patchState, getState, dispatch }: StateContext<AbstractStateModel>,
    { filter, order, limit }: AbstractActions.Init,
    Actions: typeof AbstractActions,
  ) {
    const state = getState();

    if (state.loading) {
      return;
    }

    if (!state.fetched) {
      patchState({ loading: true });
    }

    return dispatch(new Actions.Load(1, limit, filter, order));
  }

  protected load(
    ctx: StateContext<AbstractStateModel>,
    action: AbstractActions.Load,
    errorMessage: string,
    mapper?: AbstractMapperFunction,
  ) {

    return this.store.selectOnce(UserState.viewableAccountId).pipe(
      switchMap(accountId => this.service.getAll(
        action.page,
        action.limit,
        action.filter,
        action.order,
        accountId,
        false,
        action.search,
      )),
      tap(data => this.setLoadedItems(ctx, action, data, mapper), error => {
        console.error(errorMessage, error);
        ctx.patchState({ loading: false });
      }),
    );
  }

  protected setLoadedItems(
    ctx: StateContext<AbstractStateModel>,
    action: AbstractActions.Load,
    data: any,
    mapper?: AbstractMapperFunction,
  ) {
    const newItems = data.items;
    const total = data.total;
    const state = ctx.getState();
    const items = { ...state.items };
    const newIds = newItems
      .filter(item => !(item.id in state.deletedItems))
      .map(item => {
        const newItem = mapper ? mapper(item, items[item.id]) : item;
        if (item.user) {
          newItem.user = Utils.extendAccountWithBaseInfo(item.user);
        }
        items[item.id] = { ...items[item.id], ...newItem };
        return item.id;
      });
    const ids = action.page <= 1 ? [ ...newIds ] : [ ...state.ids, ...newIds ];
    ctx.patchState({
      ids,
      items,
      total: ids.length,
      totalFound: action.search ? total : state.totalFound,
      loading: false,
      fetched: true,
      fetching: false,
    });
  }

  protected loadDrafts(
    { patchState, getState }: StateContext<AbstractStateModel>,
    { filter, order, page, limit }: AbstractActions.LoadDrafts,
    errorMessage: string,
    mapper?: AbstractMapperFunction,
  ) {
    let state = getState();

    if (!state.draftsFetched) {
      patchState({ loading: true });
    }

    return this.store.selectOnce(UserState.viewableAccountId).pipe(
      switchMap(accountId => this.service.getAll(page, limit, filter, order, accountId, true)),
      tap(data => {
        state = getState();
        const drafts = data.items;
        const items = { ...state.items };
        const newDraftIds = drafts
          .filter(item => !(item.id in state.deletedItems))
          .map(item => {
            const newItem = mapper ? mapper(item, items[item.id]) : item;

            if (item.user) {
              newItem.user = Utils.extendAccountWithBaseInfo(item.user);
            }

            items[item.id] = { ...items[item.id], ...newItem };

            return item.id;
          });

        const draftIds = [ ...newDraftIds ];

        patchState({
          draftIds,
          items,
          draftsFetched: true,
          loading: false,
        });
      }, error => {
        patchState({ loading: false });
        console.error(errorMessage, error);
      }),
    );
  }

  protected reset(ctx: StateContext<AbstractStateModel>) {
    ctx.patchState(AbstractState.getDefaultState());
  }

  protected add(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { payload, dataToUpload }: AbstractActions.Add,
    errorMessage: string,
    Actions: typeof AbstractActions,
    mapper?: AbstractMapperFunction,
  ) {
    patchState({ loading: true });

    return this.service.add(payload).pipe(
      tap((data) => {
        const state = getState();
        const newItem: AbstractEntityModel = {
          ...(mapper ? mapper(data) : data),
          uploading: Boolean(dataToUpload),
          progress: 0,
        };

        if (newItem.user) {
          newItem.user = Utils.extendAccountWithBaseInfo(newItem.user);
        }

        const items = {
          ...state.items,
          [data.id]: newItem
        };

        let ids = state.ids;
        let draftIds = state.draftIds;
        let total = state.total;

        if (!payload.draft) {
          ids = [ data.id, ...ids ];
          total = total + 1;
        } else {
          draftIds = [ data.id, ...draftIds ];
        }

        patchState({
          ids,
          draftIds,
          items,
          total,
          loading: false,
        });

        if (dataToUpload) {
          this.store.dispatch(new Actions.Upload(data.id, dataToUpload));
        }
      }, error => {
        console.error(errorMessage, error);
        patchState({ loading: false });
      })
    );
  }

  protected uploadOneFile(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, dataToUpload }: AbstractActions.Upload,
    entityType: AbstractEntityType,
    errorMessage: string,
    Actions: typeof AbstractActions,
  ) {
    const file = dataToUpload as File;

    return this.service.uploadFileByChunks(id, file, entityType).pipe(
      distinctUntilChanged<UploadStatusMessage>((prev, curr) => prev.bytesUploaded === curr.bytesUploaded),
      tap((event: UploadStatusMessage) => {
        if (event.type === 'progress') {
          const progress = Math.floor(event.bytesUploaded / file.size * 100);
          this.store.dispatch(new Actions.UpdateUploadingProgress(id, progress));
        } else
        if (event.type === 'success') {
          const state = getState();
          const items = { ...state.items };

          items[id] = {
            ...items[id],
            uploading: false,
            processing: true,
          };

          patchState({ items });
        }
      }, error => {
        console.error(errorMessage, error);
      }),
    );
  }

  protected uploadManyFiles(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, dataToUpload }: AbstractActions.Upload,
    entityType: AbstractEntityType,
    fileEntites: { id: number }[],
    errorMessage: string,
    Actions: typeof AbstractActions,
  ) {
    const files = dataToUpload || [];

    let allFilesSize = 0;
    let uploadedAll = 0;
    let uploadedCurrentFile = 0;

    const observables = files.map(({ file, index, uniqueId }) => {
      const fileEntityId = fileEntites && fileEntites[index] && fileEntites[index].id;
      allFilesSize += file.size;

      return this.service.uploadFileByChunks(
        id,
        file,
        entityType,
        { 'grit-file-entity-id': fileEntityId || uniqueId },
      );
    });

    return concat(...observables).pipe(
      distinctUntilChanged<UploadStatusMessage>((prev, curr) => prev.bytesUploaded === curr.bytesUploaded),
      tap((event: UploadStatusMessage) => {
        if (event.type === 'progress') {
          uploadedCurrentFile = event.bytesUploaded + uploadedAll;
          const progress = Math.floor(uploadedCurrentFile / allFilesSize * 100);
          this.store.dispatch(new Actions.UpdateUploadingProgress(id, progress));
        } else
        if (event.type === 'success') {
          uploadedAll += event.bytesUploaded;
        }
      }),
      finalize(() => {
        const state = getState();
        const items = { ...state.items };

        items[id] = {
          ...items[id],
          uploading: false,
          processing: Boolean(files && files.length),
          progress: 0,
        };

        patchState({ items });
      }),
      catchError(error => {
        console.error(errorMessage, error);
        return throwError(error);
      })
    );
  }

  protected updateUploadingProgress(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, progress }: AbstractActions.UpdateUploadingProgress,
  ) {
    const state = getState();
    const items = { ...state.items };

    items[id] = {
      ...items[id],
      uploading: true,
      progress
    };

    patchState({ items });
  }

  protected finishProcessing(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, payload }: AbstractActions.FinishProcessing,
    mapper?: AbstractMapperFunction,
  ) {
    const state = getState();
    const items = { ...state.items };
    const item = { ...items[id] };
    const uploaded = !item.uploading || item.progress === 100;

    if (item && uploaded) {
      const newItem = mapper ? mapper(payload) : payload;

      if (newItem.user) {
        newItem.user = Utils.extendAccountWithBaseInfo(newItem.user);
      }

      items[payload.id] = {
        ...item,
        ...newItem,
        processing: false,
      };

      patchState({
        items,
      });
    }
  }

  protected getOne(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, enableLoading }: AbstractActions.GetOne,
    errorMessage: string,
    mapper?: AbstractMapperFunction,
  ) {
    const state = getState();

    if (state.items[id] && state.items[id].isDetailed) {
      enableLoading = false;
    }

    patchState({ loading: enableLoading });

    return this.service.getOne(id).pipe(
      tap((data) => {
        if (data.user) {
          data.user = Utils.extendAccountWithBaseInfo(data.user);
        }

        // its important to set isDetailed here, because it can be used in mapper
        data.isDetailed = true;

        const currentItem = {
          ...state.items[id],
          ...(mapper ? mapper(data) : data),
        };
        const items = {
          ...state.items,
          [data.id]: currentItem
        };

        let ids = state.ids;
        let draftIds = state.draftIds;

        if (data.draft) {
          if (draftIds.indexOf(data.id) === -1) {
            draftIds = [ data.id, ...draftIds ];
          }
        } else {
          if (ids.indexOf(data.id) === -1) {
            ids = [ data.id, ...ids ];
          }
        }

        patchState({
          ids,
          draftIds,
          items,
          currentItem,
          loading: false,
        });
      }, error => {
        console.error(errorMessage, error);
        patchState({ loading: false });
      })
    );
  }

  protected edit(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { payload, dataToUpload }: AbstractActions.Edit,
    errorMessage: string,
    Actions: typeof AbstractActions,
    mapper?: AbstractMapperFunction,
  ) {
    const entityId = payload instanceof FormData ? Number(payload.get('id')) : payload.id;

    patchState({ loading: true });

    return this.service.edit(payload).pipe(
      tap((data) => {
        if (data.user) {
          data.user = Utils.extendAccountWithBaseInfo(data.user);
        }

        const state = getState();
        const items = { ...state.items };
        const newItem: AbstractEntityModel = {
          ...(mapper ? mapper(data) : data),
          uploading: Boolean(dataToUpload),
          progress: 0,
        };

        let ids = state.ids;
        let draftIds = state.draftIds;

        items[entityId] = { ...items[entityId], ...newItem };

        if (newItem.draft) {
          ids = ids.filter(id => +id !== +newItem.id);

          if (draftIds.indexOf(newItem.id) === -1) {
            draftIds = [ newItem.id, ...draftIds ];
          }
        } else {
          draftIds = draftIds.filter(id => +id !== +newItem.id);

          if (ids.indexOf(newItem.id) === -1) {
            ids = [ newItem.id, ...ids ];
          }
        }

        patchState({
          items,
          ids,
          draftIds,
          loading: false,
        });

        if (dataToUpload) {
          this.store.dispatch(new Actions.Upload(data.id, dataToUpload));
        }
      }, error => {
        console.error(errorMessage, error);
        patchState({ loading: false });
      }),
    );
  }

  protected delete(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id }: AbstractActions.Delete,
    errorMessage: string,
  ) {
    const state = getState();
    const ids = state.ids.filter(itemId => +itemId !== +id);
    const draftIds = state.draftIds.filter(itemId => +itemId !== +id);
    const items = { ...state.items };

    let total = state.total;

    if (items[id] && !items[id].draft) {
      total--;
    }

    let deletedItems = { ...state.deletedItems };

    deletedItems[id] = items[id];

    delete items[id];

    patchState({
      ids,
      draftIds,
      items,
      total,
      deletedItems,
    });

    return this.service.delete(id).pipe(
      tap(() => {
        deletedItems = { ...state.deletedItems };

        delete deletedItems[id];

        patchState({ deletedItems });
      }, error => {
        console.error(errorMessage, error);
      })
    );
  }

  protected like(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id }: AbstractActions.Like,
    errorMessage: string,
  ) {
    const state = getState();
    const foundItem = { ...state.items[id] };
    const likes = { ...state.likes };
    const account = this.store.selectSnapshot(UserState.loggedInAccount);

    foundItem.liked = !foundItem.liked;

    if (!foundItem.likesCount) {
      foundItem.likesCount = 0;
    }

    if (!foundItem.likes) {
      foundItem.likes = [];
    }

    if (foundItem.liked) {
      const likedAccount = Utils.extendAccountWithBaseInfo(account);

      foundItem.likesCount += 1;
      foundItem.likes = [ likedAccount.id, ...foundItem.likes ];

      likes[likedAccount.id] = likedAccount;
    } else {
      foundItem.likesCount -= 1;
      foundItem.likes = foundItem.likes.filter(item => +item !== +account.id);

      delete likes[account.id];
    }

    const items = { ...state.items, [id]: foundItem };

    patchState({ items, likes });

    return this.service.like(id).pipe(
      withLatestFrom(this.store.select(UserState.loggedInAccount)),
      catchError(error => {
        console.error(errorMessage, error);
        return throwError(error);
      }),
    );
  }

  protected loadLikes(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, page, limit }: AbstractActions.LoadLikes,
    errorMessage: string,
  ) {
    let state = getState();
    let foundItem = { ...state.items[id] };

    if (!foundItem || !foundItem.likes || !foundItem.likes.length) {
      patchState({ likesLoading: true });
    }

    return this.service.getLikes(id, page, limit).pipe(
      tap((data: AccountModel[]) => {
        state = getState();
        foundItem = { ...state.items[id] };

        const likes = { ...state.likes };

        if (!foundItem.likes || page === 1) {
          foundItem.likes = [];
        } else {
          foundItem.likes = [ ...foundItem.likes ];
        }

        data.forEach(item => {
          likes[item.id] = Utils.extendAccountWithBaseInfo(item);
          foundItem.likes.push(item.id);
        });

        const items = { ...state.items, [id]: foundItem };

        patchState({ items, likes, likesLoading: false });
      },
      error => {
        patchState({ likesLoading: false });
        console.error(errorMessage, error);
      })
    );
  }

  protected comment(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, newCommentData, replyTo }: AbstractActions.Comment,
    errorMessage: string,
  ) {
    patchState({ commenting: true });

    return this.service.comment(id, newCommentData, replyTo).pipe(
      withLatestFrom(this.store.select(UserState.loggedInAccount)),
      tap(([data, account]) => {
        const state = getState();
        const comments = { ...state.comments };
        const items = { ...state.items };
        const foundItem = { ...items[id] };
        const newComment: CommentModel = {
          ...data.comment,
          replyTo,
          commentTo: replyTo ? null : id,
          user: {
            ...account,
            ...Utils.extendAccountWithBaseInfo(account),
          }
        };

        if (!replyTo) {
          if (!foundItem.commentsCount) {
            foundItem.commentsCount = 0;
          }

          foundItem.commentsCount++;
          foundItem.comments = [ ...(foundItem.comments || []), newComment.id ];

          comments[newComment.id] = newComment;
          items[id] = foundItem;

          patchState({ items, comments, commenting: false });
        } else {
          const replies = { ...state.replies };
          const comment = { ...comments[replyTo] };

          if (!comment.repliesCount) {
            comment.repliesCount = 0;
          }

          comment.repliesCount++;
          comment.replies = [ ...(comment.replies || []), newComment.id ];

          replies[newComment.id] = newComment;
          comments[replyTo] = comment;

          patchState({ comments, replies, commenting: false });
        }

      }, error => {
        patchState({ commenting: false });
        console.error(errorMessage, error);
      })
    );
  }

  protected editComment(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, comment, replyTo }: AbstractActions.EditComment,
    errorMessage: string,
  ) {
    patchState({ commenting: true });

    return this.service.editComment(id, comment, replyTo).pipe(
      tap((data) => {
        const state = getState();
        const comments = { ...state.comments };
        const items = { ...state.items };

        const newComment: CommentModel = {
          ...data.comment,
        };

        delete newComment.user;

        if (!replyTo) {
          comments[newComment.id] = {
            ...comments[newComment.id],
            ...newComment
          };

          patchState({ items, comments, commenting: false });
        } else {
          const replies = { ...state.replies };

          replies[newComment.id] = {
            ...replies[newComment.id],
            ...newComment
          };

          patchState({ comments, replies, commenting: false });
        }
      }, error => {
        patchState({ commenting: false });
        console.error(errorMessage, error);
      })
    );
  }

  protected loadComments(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, page, limit, filter }: AbstractActions.LoadComments,
    errorMessage: string,
  ) {
    let state = getState();
    let comments = { ...state.comments };
    let foundItem = { ...state.items[id] };

    if (!foundItem || !foundItem.comments || !foundItem.comments.length) {
      patchState({ commentsLoading: true });
    }

    return this.service.getComments(id, filter, page, limit).pipe(
      tap((newComments: {raw: boolean, comments: CommentModel[]}) => {
        state = getState();
        comments = { ...state.comments };
        foundItem = { ...state.items[id] };

        if (!foundItem.comments || page === 1) {
          foundItem.comments = [];
        } else {
          foundItem.comments = [ ...foundItem.comments ];
        }

        newComments.comments.forEach(item => {
          item = Utils.convertRawQueryTree(item).comment

          item.user = {
            ...item.user,
            ...Utils.extendAccountWithBaseInfo(item.user),
          };

          item = {
            ...item,
            commentTo: id,
            user: {
              ...item.user,
              ...Utils.extendAccountWithBaseInfo(item.user),
            }
          };

          comments[item.id] = item;
          foundItem.comments.push(item.id);
        });

        const items = { ...state.items, [id]: foundItem };
        patchState({ items, comments, commentsLoading: false });
      },
      error => {
        patchState({ commentsLoading: false });
        console.error(errorMessage, error);
      })
    );
  }

  protected loadCommentReplies(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, page, limit }: AbstractActions.LoadCommentReplies,
    errorMessage: string,
  ) {
    let state = getState();
    let replies = { ...state.replies };
    let comment = { ...state.comments[id] };

    if (!comment || !comment.replies || !comment.replies.length) {
      patchState({ repliesLoading: true });
    }

    return this.service.getReplies(id, page, limit).pipe(
      tap((data: {replies: any[], raw: boolean}) => {
        state = getState();
        replies = { ...state.replies };
        comment = { ...state.comments[id] };

        if (!comment.replies || page === 1) {
          comment.replies = [];
        } else {
          comment.replies = [ ...comment.replies ];
        }

        data.replies.forEach(item => {
          item = Utils.convertRawQueryTree(item).reply;

          const newReply = {
            ...item,
            replyTo: id,
            user: {
              ...item.user,
              ...Utils.extendAccountWithBaseInfo(item.user)
            }
          };

          replies[newReply.id] = newReply;
          comment.replies.push(newReply.id);
        });

        patchState({
          replies,
          comments: {
            ...state.comments,
            [id]: comment
          },
          repliesLoading: false,
        });
      },
      error => {
        patchState({ repliesLoading: false });
        console.error(errorMessage, error);
      })
    );
  }

  protected loadCommentLikes(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, reply, page, limit }: AbstractActions.LoadCommentLikes,
    errorMessage: string,
  ) {
    let state = getState();
    let commentLikes = { ...state.commentLikes };
    let comments = { ...state.comments };
    let replies = { ...state.replies };
    let editableItem = reply ? { ...replies[id] } : { ...comments[id] };

    if (!editableItem || !editableItem.likes || !editableItem.likes.length) {
      patchState({ likesLoading: true });
    }

    return this.service.getCommentLikes(id, reply, page, limit).pipe(
      tap((data: AccountModel[]) => {
        state = getState();
        commentLikes = { ...state.commentLikes };
        comments = { ...state.comments };
        replies = { ...state.replies };
        editableItem = reply ? { ...replies[id] } : { ...comments[id] };

        if (!editableItem.likes || page === 1) {
          editableItem.likes = [];
        }

        data.forEach(item => {
          const newLike = Utils.extendAccountWithBaseInfo(item);
          commentLikes[newLike.id] = newLike;
          editableItem.likes.push(newLike.id);
        });

        if (reply) {
          patchState({
            commentLikes,
            replies: {
              ...replies,
              [id]: editableItem
            },
            likesLoading: false,
          });
        } else {
          patchState({
            commentLikes,
            comments: {
              ...comments,
              [id]: editableItem
            },
            likesLoading: false,
          });
        }
      },
      error => {
        patchState({ likesLoading: false });
        console.error(errorMessage, error);
      })
    );
  }

  likeComment(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, reply }: AbstractActions.LikeComment,
    errorMessage: string,
  ) {
    const state = getState();
    const account = this.store.selectSnapshot(UserState.loggedInAccount);
    const comments = { ...state.comments };
    const replies = { ...state.replies };
    const editableItem = reply ? { ...replies[id] } : { ...comments[id] };
    const commentLikes = { ...state.commentLikes };

    editableItem.liked = !editableItem.liked;

    if (!editableItem.likesCount) {
      editableItem.likesCount = 0;
    }

    if (!editableItem.likes) {
      editableItem.likes = [];
    }

    if (editableItem.liked) {
      const likedAccount = Utils.extendAccountWithBaseInfo(account);
      editableItem.likesCount += 1;
      editableItem.likes = [ ...editableItem.likes, likedAccount.id ];

      commentLikes[likedAccount.id] = likedAccount;
    } else {
      editableItem.likesCount -= 1;
      editableItem.likes = editableItem.likes.filter(item => +item !== +account.id);

      delete commentLikes[account.id];
    }

    if (reply) {
      replies[id] = editableItem;
    } else {
      comments[id] = editableItem;
    }

    patchState({
      commentLikes,
      comments,
      replies,
    });

    return this.service.likeComment(id, reply).pipe(
      catchError(error => {
        console.error(errorMessage, error);
        return throwError(error);
      })
    );
  }

  protected deleteComment(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id, reply }: AbstractActions.DeleteComment,
    errorMessage: string,
  ) {
    const state = getState();
    const comments = { ...state.comments };
    const deletedComments = { ...state.deletedComments };

    if (reply) {
      const replies = { ...state.replies };
      const replyItem = replies[id];
      const comment = comments[replyItem.replyTo];

      comments[replyItem.replyTo] = {
        ...comment,
        replies: comment.replies.filter(replyId => +replyId !== +replyItem.id),
        repliesCount: comment.repliesCount - 1
      };

      deletedComments[id] = replies[id];

      delete replies[id];

      patchState({
        comments,
        replies,
        deletedComments,
      });
    } else {
      const items = { ...state.items };
      const itemId = comments[id].commentTo;
      const item = items[itemId];

      items[itemId] = {
        ...item,
        comments: item.comments.filter(commentId => +commentId !== +id),
        commentsCount: item.commentsCount - 1
      };

      deletedComments[id] = comments[id];

      delete comments[id];

      patchState({
        items,
        comments,
        deletedComments,
      });
    }

    return this.service.deleteComment(id, reply).pipe(
      tap(() => {
        delete deletedComments[id];
        patchState({ deletedComments });
      }, error => {
        console.error(errorMessage, error);
      })
    );
  }

  protected insertItems(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { newItems }: AbstractActions.InsertItems,
    mapper?: AbstractMapperFunction,
  ) {
    const items = { ...getState().items };

    for (const item of newItems) {
      for (const key in item) {
        if (item[key] === null || item[key] === undefined) {
          delete item[key];
        }
      }
      items[item.id] = {
        ...items[item.id],
        ...(mapper ? mapper(item, items[item.id]) : item),
      };
    }

    patchState({ items });
  }

  protected view(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id }: AbstractActions.View,
    errorMessage: string,
  ) {
    const state = getState();
    const foundItem = { ...state.items[id] };
    if (!foundItem.viewed) {
      foundItem.viewed = true;
      foundItem.viewsCount = ++foundItem.viewsCount || 0;
      const items = { ...state.items, [id]: foundItem };
      patchState({ items });
    }

    return this.service.view(id).pipe(
      withLatestFrom(this.store.select(UserState.loggedInAccount)),
      tap(() => {}, error => {
        console.error(errorMessage, error);
      })
    );
  }

  protected addToBookmark(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id }: AbstractActions.View,
    errorMessage: string,
  ) {
    const state = getState();
    const foundItem = { ...state.items[id] };
    foundItem.bookmarked = true;
    foundItem.bookmarksCount = foundItem.bookmarksCount + 1;
    const items = { ...state.items, [id]: foundItem };
    patchState({ items });
  }

  protected deleteFromBookmark(
    { getState, patchState }: StateContext<AbstractStateModel>,
    { id }: AbstractActions.View,
    errorMessage: string,
  ) {
    const state = getState();
    const foundItem = { ...state.items[id] };
    foundItem.bookmarked = false;
    foundItem.bookmarksCount = foundItem.bookmarksCount - 1;
    const items = { ...state.items, [id]: foundItem };
    patchState({ items });
  }
}
