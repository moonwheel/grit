import { State, Action, StateContext, Selector, Store, ofActionSuccessful, Actions } from '@ngxs/store';
import { tap, switchMap, filter, map, take, catchError } from 'rxjs/operators';

import { AccountModel } from 'src/app/shared/models/user/account.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamActions } from '../actions/team.actions';
import { BusinessService } from 'src/app/shared/services/business.service';
import { Utils } from 'src/app/shared/utils/utils';
import { UserState } from './user.state';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { of, throwError } from 'rxjs';
import { ResetViewableUserStore } from '../actions/user.actions';
import { TranslateService } from '@ngx-translate/core';
import { ChangeAccount, Logout, UpdateTokens } from '../actions/auth.actions';
import { BusinessState } from './business.state';
import { Router } from '@angular/router';
import { DeleteLocalBusiness } from '../actions/business.actions';

export interface TeamStateModel {
  items: AccountModel[];
  autoSuggestions: AccountModel[];
  loading: boolean;
  fetched: boolean;
  total: number;
  lazyLoading: boolean;
  searching: boolean;
}

const defaultState: TeamStateModel = {
  items: [],
  autoSuggestions: [],
  loading: false,
  fetched: false,
  total: 0,
  searching: false,
  lazyLoading: false,
};

@State<TeamStateModel>({
  name: 'team',
  defaults: {
    ...defaultState
  }
})

@Injectable()
export class TeamState {

  @Selector()
  static items(state: TeamStateModel) {
    return state.items;
  }

  @Selector()
  static loading(state: TeamStateModel) {
    return state.loading;
  }

  @Selector()
  static autoSuggestions(state: TeamStateModel) {
    return state.autoSuggestions;
  }

  @Selector()
  static total(state: TeamStateModel) {
    return state.total;
  }

  @Selector()
  static lazyLoading(state: TeamStateModel) {
    return state.lazyLoading
  }

  @Selector()
  static searching(state: TeamStateModel) {
    return state.searching;
  }

  constructor(
    private businessService: BusinessService,
    private store: Store,
    private snackbar: MatSnackBar,
    private actions$: Actions,
    private translateService: TranslateService,
    private router: Router,
    private zone: NgZone,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new TeamActions.Reset());
    });
  }

  @Action(TeamActions.Reset)
  reset({ setState }: StateContext<TeamStateModel>) {
    setState({ ...defaultState });
  }

  @Action(TeamActions.Load, { cancelUncompleted: true })
  load({ patchState, getState }: StateContext<TeamStateModel>, { page, limit, search, lazy }: TeamActions.Load) {
    let state = getState();

    if (!state.fetched) {
      patchState({ loading: true });
    }

    patchState({
      lazyLoading: lazy,
      searching: !!search
    })

    return this.getCurrentBusiness().pipe(
      switchMap(business => this.businessService.getBuisinessUsers(business.id, search, page, limit)),
      tap(({ items, total }) => {
        state = getState();
        items = items.map(item => Utils.extendAccountWithBaseInfo(item));
        if (page > 1) {
          items = [ ...state.items, ...items ];
        } else {
          items = items;
        }

        patchState({ items, loading: false, fetched: true, total, lazyLoading: false });
      }, error => {
        console.error(TeamActions.Load.type, error);
        patchState({ loading: false, lazyLoading: false });
      })
    );
  }

  @Action(TeamActions.Delete)
  delete({ patchState, getState }: StateContext<TeamStateModel>, { id }: TeamActions.Delete) {
    const user = this.store.selectSnapshot(UserState.loggedInAccount);
    return this.businessService.deleteUserFromBusiness(user.business.id, id).pipe(
      tap(data => {
        const state = getState();
        patchState({
          items: state.items.filter(item => item.id !== id),
          total: state.total - 1,
        });

        if (user.id === id) {
          this.store.dispatch(new UpdateTokens(data)).subscribe(() => {
            const accounts = this.store.selectSnapshot(BusinessState.businessList);
            const personalAccount = accounts.find(account => !account.business);
            this.store.dispatch([
              new DeleteLocalBusiness(id),
              new ChangeAccount(personalAccount),
            ]);
            this.zone.run(() => this.router.navigateByUrl('/menu'));
          });
        }
      }),
      catchError(error => {
        console.error(TeamActions.Delete.type, error);
        this.showAdminErrorMessageIfTextContain(error.error && error.error.error, 'You are the only admin');
        return of(null);
      })
    );
  }

  @Action(TeamActions.AddMany)
  addMany({ patchState, getState }: StateContext<TeamStateModel>, { accounts }: TeamActions.AddMany) {
    const data = accounts.map(item => ({ email: item.person.email, role: item.role }));

    patchState({ loading: true });

    return this.getCurrentBusiness().pipe(
      switchMap(business => this.businessService.addUserToBusiness(business.id, data)),
      tap(() => {
        const state = getState();
        patchState({
          items: [
            ...state.items,
            ...accounts.map(item => ({
              ...item,
              role: { type: item.role }
            }))
          ],
          total: state.total + accounts.length,
          loading: false
        });
      }, error => {
        patchState({ loading: false });
        console.error(TeamActions.AddMany.type, error);

        if (error instanceof HttpErrorResponse) {
          if (error.error.error) {
            this.snackbar.open(error.error.error);
          }
        }
      })
    );
  }

  @Action(TeamActions.GetAutosuggestions, { cancelUncompleted: true })
  searchPossibleTeamMembers({ patchState }: StateContext<TeamStateModel>, { text }: TeamActions.GetAutosuggestions) {
    if (!text) {
      patchState({ autoSuggestions: [] });
      return;
    }

    return this.getCurrentBusiness().pipe(
      switchMap(business => this.businessService.searchPossibleMembers(business.id, text.toLowerCase())),
      tap(({ items }) => {
        const loggedInAccountId = this.store.selectSnapshot(UserState.loggedInAccountId);
        const autoSuggestions = items
          .map(item => Utils.extendAccountWithBaseInfo(item))
          .filter(item => item.id !== loggedInAccountId);

        patchState({ autoSuggestions });
      }, error => {
        console.error(TeamActions.GetAutosuggestions.type, error);
      })
    );
  }

  @Action(TeamActions.UpdateRole)
  updateRole({ patchState, getState }: StateContext<TeamStateModel>, { userId, role }: TeamActions.UpdateRole) {
    return this.getCurrentBusiness().pipe(
      switchMap(business => this.businessService.updateBusinessUserRole(business.id, userId, role)),
      tap(() => {
        const state = getState();
        const items = [ ...state.items ];
        const index = items.findIndex(item => item.id === userId);

        if (index !== -1) {
          items[index] = {
            ...items[index],
            role: {
              ...items[index].role,
              type: role
            }
          };

          patchState({ items });
        }
      }),
      catchError(error => {
        console.error(TeamActions.UpdateRole.type, error);
        this.showAdminErrorMessageIfTextContain(error.error && error.error.error, 'You can not devote the last admin');
        return of(null);
      })
    );
  }

  private getCurrentBusiness() {
    return this.store.select(UserState.loggedInAccount).pipe(
      filter(user => !!user),
      take(1),
      map(user => {
        if (user.business) {
          return user.business;
        } else {
          throw new Error('Account has no business');
        }
      }),
    );
  }

  private showAdminErrorMessageIfTextContain(text: string, substr: string) {
    if (text && text.includes(substr)) {
      const message = this.translateService.instant('ERRORS.CANT_DELETE_ADMIN');
      this.snackbar.open(message, '', { duration: 3000 });
    }
  }

}
