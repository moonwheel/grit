import { State, Action, StateContext, Selector, Store, createSelector } from '@ngxs/store';
import { throwError, concat } from 'rxjs';
import { tap, catchError, distinctUntilChanged, finalize } from 'rxjs/operators';

import {
  InitReviews,
  LoadReviews,
  GetReview,
  AddReview,
  EditReview,
  DeleteReview,
  LikeReview,
  LikeByReview,
  AddReviewVideo,
  DeleteReviewVideo,
  DeleteReviewPhoto,
  AddReplyReview,
  EditReplyReview,
  DeleteReplyReview,
  Upload,
  UpdateUploadingProgress,
  FinishProcessing,
  LoadReviewReplies,
  LoadReviewLikes,
  LikeReplyReview,
  LoadReviewReplyLikes,
} from '../actions/review.actions';

import { ReviewService } from '../../shared/services/review.service';
import { ResetReviews } from '../actions/review.actions';
import { AbstractActions } from '../actions/abstract.actions';
import { AbstractMapperFunction } from './abstract.state';
import { UploadStatusMessage, AbstractService } from 'src/app/shared/services/abstract.service';
import { Utils } from 'src/app/shared/utils/utils';
import { AccountModel } from 'src/app/shared/models';
import { Injectable } from '@angular/core';

export class ReviewStateModel {
  items: any[];
  replies: any[];
  likes: any[];
  total: number;
  totalOne: number;
  totalTwo: number;
  totalThree: number;
  loading: boolean;
  fetched: boolean;
  repliesLoading: boolean;
  likesLoading: boolean;
  reviewed: boolean;
}

@State<ReviewStateModel>({
  name: 'reviews',
  defaults: {
    items: [],
    replies: [],
    likes: [],
    total: 0,
    totalOne: 0,
    totalTwo: 0,
    totalThree: 0,
    loading: false,
    fetched: false,
    repliesLoading: false,
    likesLoading: false,
    reviewed: false,
  }
})

@Injectable()
export class ReviewState {

  @Selector()
  static total(state: ReviewStateModel) {
    return state.total;
  }

  @Selector()
  static totalOne(state: ReviewStateModel) {
    return state.totalOne;
  }

  @Selector()
  static totalTwo(state: ReviewStateModel) {
    return state.totalTwo;
  }

  @Selector()
  static totalThree(state: ReviewStateModel) {
    return state.totalThree;
  }

  @Selector()
  static items(state: ReviewStateModel) {
    return state.items;
  }

  @Selector()
  static reviewed(state: ReviewStateModel) {
    return state.reviewed;
  }

  static getOne(id: number) {
    return createSelector([ReviewState], (state: ReviewStateModel) => {
      return state.items.find(item => item.id === id);
    });
  }

  @Selector()
  static replies(state: ReviewStateModel) {
    return state.replies;
  }

  @Selector()
  static likes(state: ReviewStateModel) {
    return state.likes;
  }

  @Selector()
  static loading(state: ReviewStateModel) {
    return state.loading;
  }

  @Selector()
  static likesLoading(state: ReviewStateModel) {
    return state.likesLoading;
  }

  @Selector()
  static repliesLoading(state: ReviewStateModel) {
    return state.repliesLoading;
  }

  constructor(
    private reviewService: ReviewService,
    private store: Store,
  ) { }

  @Action(InitReviews)
  init({ getState, patchState, dispatch }: StateContext<ReviewStateModel>, { entity, id, limit, rating }: InitReviews) {
    const state = getState();

    if (state.loading) {
      return;
    }

    if (!state.fetched) {
      patchState({ loading: true });
    }

    return dispatch(new LoadReviews(entity, id, 1, limit, rating));
  }

  @Action(LoadReviews)
  load(
    { patchState, getState }: StateContext<ReviewStateModel>,
    { entity, id, page, limit, rating, orderBy, order }: LoadReviews,
  ) {
    return this.reviewService.getList(entity, id, page, limit, rating, orderBy, order).pipe(
      tap(data => {
        const state = getState();

        const totalOne = data.ratingsTotal.find(item => item.rating === '1');
        const totalTwo = data.ratingsTotal.find(item => item.rating === '2');
        const totalThree = data.ratingsTotal.find(item => item.rating === '3');

        if (page === 1) {
          patchState({
            items: data.items,
            total: data.total,
            totalOne: totalOne ? totalOne.count : 0,
            totalTwo: totalTwo ? totalTwo.count : 0,
            totalThree: totalThree ? totalThree.count : 0,
            reviewed: !!data.reviewed,
            loading: false,
            fetched: true,
          })
        } else {
          patchState({
            items: [
              ...state.items,
              ...data.items,
            ],
            total: data.total,
            totalOne: totalOne ? totalOne.count : 0,
            totalTwo: totalTwo ? totalTwo.count : 0,
            totalThree: totalThree ? totalThree.count : 0,
            loading: false,
            fetched: true,
          })
        }
      }),
      catchError(error => {
        console.error(LoadReviews.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    );
  }

  @Action(ResetReviews)
  reset(ctx: StateContext<ReviewStateModel>) {
    ctx.patchState({
      items: [],
      total: 0,
      loading: false,
      fetched: false,
    });
  }

  // @Action(GetReview)
  // getReview({ patchState }: StateContext<ReviewStateModel>, { entity, id, }: GetReview) {
  //   return this.reviewService.getReview(entity, id).pipe(
  //     tap(data => {
  //       console.log('Review: ', data);
  //     }),
  //   );
  // }

  @Action(AddReview)
  addReview(
    { patchState,getState }: StateContext<ReviewStateModel>,
    { entity, id, review, dataToUpload }: AddReview,
  ) {
    return this.reviewService.add(entity, id, review).pipe(
      tap(data => {
        const state = getState();

        patchState({
          items: [ ...state.items, data ],
          total: state.total + 1,
          reviewed: true,
        });

        if (dataToUpload) {
          this.store.dispatch(new Upload(data.id, entity, dataToUpload));
        }
      })
    );
  }

  @Action(Upload)
  uploadManyFiles(
    { getState, patchState }: StateContext<ReviewStateModel>,
    { id, entity, dataToUpload, fileEntites }: Upload,
  ) {
    const files = dataToUpload || [];

    let allFilesSize = 0;
    let uploadedAll = 0;
    let uploadedCurrentFile = 0;

    const observables = files.map(({ file, index, uniqueId }) => {
      const fileEntityId = fileEntites && fileEntites[index] && fileEntites[index].id;
      allFilesSize += file.size;

      return this.reviewService.uploadFileByChunks(
        id,
        file,
        entity,
        { 'grit-file-entity-id': fileEntityId || uniqueId },
      );
    });

    return concat(...observables).pipe(
      distinctUntilChanged<UploadStatusMessage>((prev, curr) => prev.bytesUploaded === curr.bytesUploaded),
      tap((event: UploadStatusMessage) => {
        if (event.type === 'progress') {
          uploadedCurrentFile = event.bytesUploaded + uploadedAll;
          const progress = Math.floor(uploadedCurrentFile / allFilesSize * 100);
          // this.store.dispatch(new UpdateUploadingProgress(id, progress));
        } else
        if (event.type === 'success') {
          uploadedAll += event.bytesUploaded;
        }
      }),
      catchError(error => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  @Action(UpdateUploadingProgress)
  updateUploadingProgress(
    { getState, patchState }: StateContext<ReviewStateModel>,
    { id, progress }: AbstractActions.UpdateUploadingProgress,
  ) {
    const state = getState();
    const items = { ...state.items };

    items[id] = {
      ...items[id],
      uploading: true,
      progress
    };

    patchState({ items });
  }

  @Action(FinishProcessing)
  finishProcessing(
    { getState, patchState }: StateContext<ReviewStateModel>,
    { id, payload }: AbstractActions.FinishProcessing,
    mapper?: AbstractMapperFunction,
  ) {
    const state = getState();
    const items = { ...state.items };
    const item = { ...items[id] };
    const uploaded = !item.uploading || item.progress === 100;

    if (item && uploaded) {
      const newItem = mapper ? mapper(payload) : payload;

      if (newItem.user) {
        newItem.user = Utils.extendAccountWithBaseInfo(newItem.user);
      }

      items[payload.id] = {
        ...item,
        ...newItem,
        processing: false,
      };

      patchState({
        items,
      });
    }
  }

  @Action(EditReview)
  editReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, review }: EditReview) {
    return this.reviewService.edit(entity, review).pipe(
      tap(data => {
        const state = getState();
        const items = [ ...state.items ];
        const index = items.findIndex(item => item.id === review.id);
        if (index > -1) {
          items[index] = { ...items[index], ...data };
          patchState({ items });
        }
      }),
    );
  }

  @Action(DeleteReview)
  deleteReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, id }: DeleteReview) {
    return this.reviewService.delete(entity, id).pipe(
      tap(() => {
        const state = getState();

        const found = state.items.find(item => item.id === id);

        if (found) {
          const items = state.items.filter(item => item.id !== id);

          let field = '';

          if (found.rating === '1') {
            field = 'totalOne';
          } else if (found.rating === '2') {
            field = 'totalTwo';
          } else if (found.rating === '3') {
            field = 'totalThree';
          }

          patchState({
            items,
            total: state.total - 1,
            [field]: state[field] - 1,
            reviewed: false,
          });
        }
      })
    );
  }

  @Action(LikeReview)
  likeReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, id }: LikeReview) {
    const state = getState();
    const items = [ ...state.items ];
    const index = items.findIndex(item => item.id === id);
    const likes = state.likes.filter(item => item.id !== id);
    if (index > -1) {
      items[index] = {
        ...items[index],
        ...{
          liked: !items[index].liked,
          likesCount: items[index].liked ? items[index].likesCount - 1 : items[index].likesCount + 1,
        },
      };
      patchState({ items, likes });
    }

    return this.reviewService.like(entity, id);
  }

  @Action(AddReviewVideo)
  addReviewVideo() {
    console.log('Video review');
  }

  @Action(DeleteReviewVideo)
  deleteReviewVideo() {
    console.log('Delete video review');
  }

  @Action(AddReplyReview)
  addReplyReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, id, review }: AddReplyReview) {
    return this.reviewService.addReply(entity, id, review).pipe(
      tap(data => {
        const state = getState();
        const items = [ ...state.items ];
        const index = items.findIndex(item => item.id === id);
        if (index > -1) {

          const reviewReplies = [ ...items[index].replies ];
          reviewReplies.push(data);

          const replies = [ ...state.replies ];
          replies.push(data);

          items[index] = {
            ...items[index],
            replies: reviewReplies,
          };
          patchState({ items, replies });
        }
      })
    );
  }

  @Action(EditReplyReview)
  editReplyReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, id, reply }: EditReplyReview) {
    return this.reviewService.editReply(entity, reply).pipe(
      tap(data => {
        const state = getState();
        const items = [ ...state.items ];
        const index = items.findIndex(item => item.id === id);
        if (index > -1) {

          const replies = [ ...items[index].replies ];
          const replyIndex = replies.findIndex(item => item.id === reply.id);

          replies[replyIndex] = data;

          items[index] = {
            ...items[index],
            replies,
          };

          patchState({ items });
        }
      }),
    );
  }

  @Action(DeleteReplyReview)
  deleteReplyReview({ patchState, getState }: StateContext<ReviewStateModel>, { entity, reviewId, id }: DeleteReplyReview) {
    return this.reviewService.deleteReply(entity, id).pipe(
      tap(data => {
        const state = getState();
        const items = [ ...state.items ];
        let replies = [ ...state.replies ];
        const index = items.findIndex(item => item.id === reviewId);
        if (index > -1) {

          let reviewReplies = [ ...items[index].replies ];
          reviewReplies = reviewReplies.filter(item => item.id !== id);

          replies = replies.filter(item => item.id !== id);

          items[index] = {
            ...items[index],
            replies: reviewReplies,
          };

          patchState({ items, replies });
        }
      }),
    );
  }

  @Action(LoadReviewReplies)
  loadReviewReplies({ getState, patchState }: StateContext<ReviewStateModel>, { id, entity }: LoadReviewReplies) {
    
    patchState({ repliesLoading: true });

    return this.reviewService.getReplies(entity, id).pipe(
      tap((data: any) => {
        
        patchState({
          replies: data.items,
          repliesLoading: false,
        });
      },
      error => {
        patchState({ repliesLoading: false });
        console.error(error);
      })
    );
  }

  @Action(LoadReviewLikes)
  loadReviewLikes({ patchState }: StateContext<ReviewStateModel>, { id, entity }: LoadReviewLikes) {
    return this.reviewService.getLikes(entity, id).pipe(
      tap((data: AccountModel[]) => {
        patchState({
          likes: data,
          likesLoading: false,
        });
      },
      error => {
        patchState({ likesLoading: false });
        console.log(error);
      })
    );
  }

  @Action(LikeReplyReview)
  likeReplyReview({ getState, patchState }: StateContext<ReviewStateModel>, { id, entity }: LikeReplyReview) {

    const state = getState();
    const replies = [ ...state.replies ];
    const index = replies.findIndex(item => item.id === id);
    const likes = state.likes.filter(item => item.id !== id);
    if (index > -1) {
      replies[index] = {
        ...replies[index],
        ...{
          liked: !replies[index].liked,
          likesCount: replies[index].liked ? replies[index].likesCount - 1 : replies[index].likesCount + 1,
        },
      };
      patchState({ replies, likes });
    }

    return this.reviewService.likeReply(entity, id);
  }

  @Action(LoadReviewReplyLikes)
  loadReplyLikes({ patchState }: StateContext<ReviewStateModel>, { id, entity }: LoadReviewLikes) {
    return this.reviewService.getLikesReply(entity, id).pipe(
      tap((data: AccountModel[]) => {
        patchState({
          likes: data,
          likesLoading: false,
        });
      },
      error => {
        patchState({ likesLoading: false });
        console.log(error);
      })
    );
  }
}
