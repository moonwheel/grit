import { tap, finalize, catchError } from 'rxjs/operators';
import { concat, throwError } from 'rxjs';

import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { VideoActions } from '../actions/video.actions';

import { VideoModel } from 'src/app/shared/models/video.model';
import { VideoService } from 'src/app/shared/services/video.service';
import { AbstractState, AbstractStateModel } from './abstract.state';
import { UploadStatusMessage } from 'src/app/shared/services/abstract.service';
import { SseService } from 'src/app/shared/services/sse.service';
import { Injectable } from '@angular/core';

export interface VideoStateModel extends AbstractStateModel {
    items: {
        [id: number]: VideoModel
    };
}

@State<VideoStateModel>({
    name: 'videos',
    defaults: {
        ...AbstractState.getDefaultState(),
    }
})

@Injectable()
export class VideoState extends AbstractState {

    @Selector()
    static videos(state: VideoStateModel) { return super.getAllSelector(state); }

    @Selector()
    static drafts(state: VideoStateModel) { return super.getDraftsSelector(state); }

    @Selector()
    static total(state: VideoStateModel) { return super.totalSelector(state); }

    @Selector()
    static loading(state: VideoStateModel) { return super.loadingSelector(state); }

    @Selector()
    static uploading(state: VideoStateModel) { return super.uploadingSelector(state); }

    @Selector()
    static lazyLoading(state:VideoStateModel) {
        return state.fetching;
    }

    static video(id: number) {
        return super.getOneSelector<VideoModel>(VideoState, id);
    }

    static likes(id: number) {
        return super.likesSelector(VideoState, id);
    }

    static comments(id: number) {
        return super.commentsSelector(VideoState, id);
    }

    static comment(commentId: number) {
        return super.commentSelector(VideoState, commentId);
    }

    static replies(commentId: number) {
        return super.repliesSelector(VideoState, commentId);
    }

    constructor(
        protected service: VideoService,
        protected store: Store,
        protected sseService?: SseService,
    ) {
        super(service, store);

        this.sseService.fileProcessingComplete$.subscribe(({ entityType, entity }) => {
            if (entityType === 'video') {
                this.store.dispatch(new VideoActions.FinishProcessing(entity.id, entity));
            }
        });
    }

    @Action(VideoActions.Init)
    init(ctx: StateContext<VideoStateModel>, action: VideoActions.Init) {
        return super.init(ctx, action, VideoActions);
    }

    @Action(VideoActions.Load)
    load(ctx: StateContext<VideoStateModel>, action: VideoActions.Load) {
        if(action.lazy) {
            ctx.patchState({fetching: true})
        }

        return super.load(ctx, action, VideoActions.Load.type);
    }

    @Action(VideoActions.LoadDrafts)
    loadDrafts(ctx: StateContext<VideoStateModel>, action: VideoActions.LoadDrafts) {
        return super.loadDrafts(ctx, action, VideoActions.LoadDrafts.type);
    }

    @Action(VideoActions.GetOne)
    getOne(ctx: StateContext<VideoStateModel>, action: VideoActions.GetOne) {
      this.store.dispatch(new VideoActions.View(action.id));
      return super.getOne(ctx, action, VideoActions.GetOne.type);
    }

    @Action(VideoActions.Reset)
    reset(ctx: StateContext<VideoStateModel>) {
        return super.reset(ctx);
    }

    @Action(VideoActions.Add)
    add(ctx: StateContext<VideoStateModel>, action: VideoActions.Add) {
        return super.add(ctx, action, VideoActions.Add.type, VideoActions);
    }

    @Action(VideoActions.Upload)
    upload(ctx: StateContext<VideoStateModel>, action: VideoActions.Upload) {
        return super.uploadManyFiles(ctx, action, 'video', null, VideoActions.Upload.type, VideoActions);
    }

    @Action(VideoActions.UpdateUploadingProgress)
    updateUploadingProgress(ctx: StateContext<VideoStateModel>, action: VideoActions.UpdateUploadingProgress) {
        return super.updateUploadingProgress(ctx, action);
    }

    @Action(VideoActions.FinishProcessing)
    finishProcessing(ctx: StateContext<VideoStateModel>, action: VideoActions.FinishProcessing) {
        return super.finishProcessing(ctx, action);
    }

    @Action(VideoActions.Edit)
    edit(ctx: StateContext<VideoStateModel>, action: VideoActions.Edit) {
        return super.edit(ctx, action, VideoActions.Edit.type, VideoActions);
    }

    @Action(VideoActions.Delete)
    delete(ctx: StateContext<VideoStateModel>, action: VideoActions.Delete) {
        return super.delete(ctx, action, VideoActions.Delete.type);
    }

    @Action(VideoActions.Like)
    like(ctx: StateContext<VideoStateModel>, action: VideoActions.Like) {
        return super.like(ctx, action, VideoActions.Like.type);
    }

    @Action(VideoActions.LoadLikes)
    loadLikes(ctx: StateContext<VideoStateModel>, action: VideoActions.LoadLikes) {
        return super.loadLikes(ctx, action, VideoActions.LoadLikes.type);
    }

    @Action(VideoActions.Comment)
    comment(ctx: StateContext<VideoStateModel>, action: VideoActions.Comment) {
        return super.comment(ctx, action, VideoActions.Comment.type);
    }

    @Action(VideoActions.EditComment)
    editComment(ctx: StateContext<VideoStateModel>, action: VideoActions.EditComment) {
        return super.editComment(ctx, action, VideoActions.EditComment.type);
    }

    @Action(VideoActions.LoadComments)
    loadComments(ctx: StateContext<VideoStateModel>, action: VideoActions.LoadComments) {
        return super.loadComments(ctx, action, VideoActions.LoadComments.type);
    }

    @Action(VideoActions.LoadCommentReplies)
    loadCommentReplies(ctx: StateContext<VideoStateModel>, action: VideoActions.LoadCommentReplies) {
        return super.loadCommentReplies(ctx, action, VideoActions.LoadCommentReplies.type);
    }

    @Action(VideoActions.LikeComment)
    likeComment(ctx: StateContext<VideoStateModel>, action: VideoActions.LikeComment) {
        return super.likeComment(ctx, action, VideoActions.LikeComment.type);
    }

    @Action(VideoActions.LoadCommentLikes)
    loadCommentLikes(ctx: StateContext<VideoStateModel>, action: VideoActions.LoadCommentLikes) {
        return super.loadCommentLikes(ctx, action, VideoActions.LoadCommentLikes.type);
    }

    @Action(VideoActions.DeleteComment)
    deleteComment(ctx: StateContext<VideoStateModel>, action: VideoActions.DeleteComment) {
        return super.deleteComment(ctx, action, VideoActions.DeleteComment.type);
    }

    @Action(VideoActions.InsertItems)
    insertItems(ctx: StateContext<VideoStateModel>, action: VideoActions.InsertItems) {
        return super.insertItems(ctx, action);
    }

    @Action(VideoActions.View)
    view(ctx: StateContext<VideoStateModel>, action: VideoActions.View) {
        return super.view(ctx, action, VideoActions.View.type);
    }

    @Action(VideoActions.AddToBookmark)
    addToBookmark(ctx: StateContext<VideoStateModel>, action: VideoActions.AddToBookmark) {
      return super.addToBookmark(ctx, action, VideoActions.AddToBookmark.type);
    }

    @Action(VideoActions.DeleteFromBookmark)
    deleteFromBookmark(ctx: StateContext<VideoStateModel>, action: VideoActions.DeleteFromBookmark) {
      return super.deleteFromBookmark(ctx, action, VideoActions.DeleteFromBookmark.type);
    }
}
