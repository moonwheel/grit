import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import {
  InitPurchases,
  LoadPurchases,
  InitSales,
  LoadSales,
  ResetOrders,
  CancelOrder,
  ShipOrder,
  RefundOrder,
  ReturnOrder,
  GetOneSale,
  GetOnePurchase,
} from '../actions/order.actions';

import { OrderService } from '../../shared/services/order.service';
import { ChangeBooking } from '../actions/booking.actions';
import { Injectable } from '@angular/core';
import { OrderModel } from '../../shared/models';

export interface SingleOrderEntity {
  loading: boolean;
  fetched: boolean;
  order: OrderModel | null;
}

interface DetailOrderList {
  [type: string] : {
    [orderId: string]: SingleOrderEntity
  }
}

export class OrderStateModel {
  purchases: any[];
  sales: any[];
  orders: any[];
  totalPurchases: number;
  totalSales: number;
  loading: boolean;
  fetchedPurchases: boolean;
  fetchedSales: boolean;
  totalItems: number;
  lazyLoading: boolean;
  ordersDetail: DetailOrderList;
}

@State<OrderStateModel>({
  name: 'orders',
  defaults: {
    purchases: [],
    sales: [],
    orders: [],
    totalPurchases: 0,
    totalSales: 0,
    loading: false,
    fetchedPurchases: false,
    fetchedSales: false,
    totalItems: 0,
    lazyLoading: false,
    ordersDetail: {
      ['product']: {},
      ['service']: {}
    },
  }
})

@Injectable()
export class OrderState {

  @Selector()
  static lazyLoading(state: OrderStateModel) {
    return state.lazyLoading;
  }

  @Selector()
  static purchases(state: OrderStateModel) {
    return state.purchases;
  }

  @Selector()
  static sales(state: OrderStateModel) {
    return state.sales;
  }

  @Selector()
  static totalPurchases(state: OrderStateModel) {
    return state.totalPurchases;
  }

  @Selector()
  static totalSales(state: OrderStateModel) {
    return state.totalSales;
  }

  @Selector() 
  static totalItems(state: OrderStateModel) {
    return state.totalItems;
  }

  @Selector()
  static loading(state: OrderStateModel) {
    return state.loading;
  }

  static selectOrder(type: string, id: string) {
    return createSelector([OrderState],(store) => {
      return store.ordersDetail[type][id] || null;
    })
  }


  constructor(
    private orderService: OrderService,
  ) { }

  @Action(InitPurchases)
  initPurchases({ getState, patchState, dispatch }: StateContext<OrderStateModel>, { limit, filter, search }: InitPurchases) {
    const state = getState();

    if (state.loading) {
      return;
    }

    patchState({ loading: true });

    return dispatch(new LoadPurchases(1, limit, filter, search));
  }

  @Action(LoadPurchases)
  loadPurchases({ patchState, getState }: StateContext<OrderStateModel>, { page, limit, filter, search, lazy }: LoadPurchases) {

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.orderService.getPurchases(page, limit, filter, search).pipe(
      tap(data => {
        const state = getState();

        const purchases = page > 1 ? [...state.purchases, ...data.items]: data.items

        patchState({
          purchases,
          totalPurchases: data.total,
          loading: false,
          fetchedPurchases: true,
          lazyLoading: false,
          totalItems: this.calculateTotalOrderItems(purchases),
        })
      }),
      catchError(error => {
        console.error(LoadPurchases.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    );
  }

  @Action(GetOnePurchase)
  loadPurchase({patchState, getState}: StateContext<OrderStateModel>, {type, id}: GetOneSale) {
    const state = getState();

    patchState({
      ordersDetail: {
        ...state.ordersDetail,
        [type]: {
          ...state.ordersDetail[type],
          [id]: {
            loading: true,
            fetched: false,
            order: null
          }
        }
      }
    })

    return this.orderService.getPurchases(1, 1, '', id).pipe(
      tap(data => {
        const order = data.items.find(item => type === 'product' ? item.type === 'order' : item.type = 'booking');

        patchState({
          ordersDetail: {
            ...state.ordersDetail,
            [type]: {
              ...state.ordersDetail[type],
              [id]: {
                loading: false,
                fetched: true,
                order: order || null,
              }
            }
          }
        })
      }),
      catchError(error => {
        console.error(GetOneSale.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    )
  }

  @Action(InitSales)
  initSales({ getState, patchState, dispatch }: StateContext<OrderStateModel>, { limit, filter, search }: InitSales) {
    const state = getState();

    if (state.loading) {
      return;
    }

    patchState({ loading: true });

    return dispatch(new LoadSales(1, limit, filter, search));
  }

  @Action(GetOneSale)
  loadSale({patchState, getState}: StateContext<OrderStateModel>, {type, id}: GetOneSale) {
    const state = getState();

    patchState({
      ordersDetail: {
        ...state.ordersDetail,
        [type]: {
          ...state.ordersDetail[type],
          [id]: {
            loading: true,
            fetched: false,
            order: null
          }
        }
      }
    })

    return this.orderService.getSales(1, 1, '', id).pipe(
      tap(data => {
        const order = data.items.find(item => type === 'product' ? item.type === 'order' : item.type = 'booking');

        patchState({
          ordersDetail: {
            ...state.ordersDetail,
            [type]: {
              ...state.ordersDetail[type],
              [id]: {
                loading: false,
                fetched: true,
                order: order || null,
              }
            }
          }
        })
      }),
      catchError(error => {
        console.error(GetOneSale.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    )
  }

  @Action(LoadSales)
  loadSales({ patchState, getState }: StateContext<OrderStateModel>, { page, limit, filter, search, lazy }: LoadSales) {

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    
    return this.orderService.getSales(page, limit, filter, search).pipe(
      tap(data => {
        const state = getState();

        const sales = page > 1 ? [...state.sales, ...data.items]: data.items

        patchState({
          sales,
          totalSales: data.total,
          loading: false,
          fetchedSales: true,
          totalItems: this.calculateTotalOrderItems(sales),
          lazyLoading: false,
        });
      }),
      catchError(error => {
        console.error(LoadSales.type, error);
        patchState({ loading: false });
        return throwError(error);
      })
    );
  }



  @Action(ResetOrders)
  reset(ctx: StateContext<OrderStateModel>) {
    ctx.patchState({
      purchases: [],
      sales: [],
      totalPurchases: 0,
      totalSales: 0,
      loading: false,
      fetchedPurchases: false,
      fetchedSales: false,
    });
  }

  @Action(ShipOrder)
  shipOrder({ patchState, getState }: StateContext<OrderStateModel>, { id, items }: ShipOrder) {
    return this.orderService.ship(id, { items })
      .pipe(
        tap(data => {
          const state = getState();
          const sales = [ ...state.sales ];
          const index = sales.findIndex(item => item.id === id);
          if (index > -1) {
            sales[index] = {
              ...data,
              type: 'order',
            };
          }
          patchState({ sales });
        }),
        catchError(error => {
          console.error(LoadSales.type, error);
          patchState({ loading: false });
          return throwError(error);
        })
      );
  }

  @Action(CancelOrder)
  cancelOrder({ patchState, getState }: StateContext<OrderStateModel>, { id, items }: CancelOrder) {
    return this.orderService.cancel(id, { items })
      .pipe(
        tap(data => {
          const state = getState();
          const type = data.who_cancelled === 'buyer' ? 'purchases' : 'sales';
          const orders =  [ ...state[type] ];
          const index = orders.findIndex(item => item.id === id && item.type === 'order');
          if (index > -1) {
            orders[index] = {
              ...data,
              type: 'order',
            };
          }
          patchState({
            [type]: orders,
          });
        }),
      );
  }

  @Action(ReturnOrder)
  returnOrder({ patchState, getState }: StateContext<OrderStateModel>, { id, items, reason, description }: ReturnOrder) {
    return this.orderService.return(id, { items, reason, description })
      .pipe(
        tap(data => {
          const state = getState();
          const purchases = [ ...state.purchases ];
          const index = purchases.findIndex(item => item.id === id);
          if (index > -1) {
            purchases[index] = {
              ...data,
              type: 'order',
            };
          }
          patchState({ purchases });
        }),
      );
  }

  @Action(RefundOrder)
  refundOrder({ patchState, getState }: StateContext<OrderStateModel>, { id, items, payment }: RefundOrder) {
    return this.orderService.refund(id, { items, payment })
      .pipe(
        tap(data => {
          const state = getState();
          const sales = [ ...state.sales ];
          const index = sales.findIndex(item => item.id === id);
          if (index > -1) {
            sales[index] = {
              ...data,
              type: 'order',
            };
          }
          patchState({ sales });
        }),
      );
  }

  @Action(ChangeBooking)
  changeBooking({ patchState, getState }: StateContext<OrderStateModel>, { id, booking, type }: ChangeBooking) {
    const state = getState();
    const bookings = [ ...state[type] ];
    const index = bookings.findIndex(item => item.id === id && item.type === 'booking');
    if (index > -1) {
      bookings[index] = {
        ...booking,
        type: 'booking',
      };
    }
    patchState({ [type]: bookings });
  }

  private calculateTotalOrderItems = (items: OrderModel[]): number => { // calculates total amount of items(including order items)
    const ordersWithItems = items.filter(item => item.order_items && item.order_items.length);

    return (items.length - ordersWithItems.length) + ordersWithItems.reduce((accum: number, item: OrderModel) => {
      return accum + item.order_items.length;
    }, 0)
  }
}
