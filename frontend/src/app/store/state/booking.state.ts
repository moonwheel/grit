import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { AddBooking, CancelBooking, RefundBooking, ChangeBooking } from '../actions/booking.actions';
import { BookingService } from '../../shared/services/booking.service';
import { Injectable } from '@angular/core';

export class BookingStateModel {
  loading: boolean;
}

@State<BookingStateModel>({
  name: 'booking',
  defaults: {
    loading: false,
  }
})

@Injectable()
export class BookingState {

  @Selector()
  static loading(state: BookingStateModel) {
    return state.loading;
  }

  constructor(
    private store: Store,
    private bookingService: BookingService,
  ) { }

  @Action(AddBooking)
  addBooking({ patchState }: StateContext<BookingStateModel>, { payload }: AddBooking) {
    return this.bookingService.add(payload).pipe(
      tap(() => patchState({ loading: false })),
      catchError(error => {
        console.error(AddBooking.type, error);
        return throwError(error);
      })
    );
  }

  @Action(CancelBooking)
  cancelBooking(ctx: StateContext<BookingStateModel>, { id }: CancelBooking) {
    return this.bookingService.cancel(id).pipe(
      tap(data => {
        const type = data.who_cancelled === 'buyer' ? 'purchases' : 'sales';
        this.store.dispatch(new ChangeBooking(id, data, type));
      }),
      catchError(error => {
        console.error(CancelBooking.type, error);
        return throwError(error);
      })
    );
  }

  @Action(RefundBooking)
  refundBooking(ctx: StateContext<BookingStateModel>, { id }: RefundBooking) {
    return this.bookingService.refund(id).pipe(
      tap(data => {
        this.store.dispatch(new ChangeBooking(id, data, 'sales'));
      }),
      catchError(error => {
        console.error(RefundBooking.type, error);
        return throwError(error);
      })
    );
  }
}
