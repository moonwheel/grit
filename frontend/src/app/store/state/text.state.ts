import { Action, createSelector, Selector, State, StateContext, Store } from '@ngxs/store';
import { TextModel } from 'src/app/shared/models/text.model';
import { TextService } from 'src/app/shared/services/text.service';
import { TextActions } from '../actions/text.actions';
import { AbstractState, AbstractStateModel } from './abstract.state';
import { Injectable } from '@angular/core';

export interface TextStateModel extends AbstractStateModel<TextModel> {
    items: {
        [id: number]: TextModel
    };
}

@State<TextStateModel>({
    name: 'texts',
    defaults: {
        ...AbstractState.getDefaultState(),
    }
})

@Injectable()
export class TextState extends AbstractState {

    @Selector()
    static texts(state: TextStateModel) { return super.getAllSelector(state); }

    @Selector()
    static drafts(state: TextStateModel) { return super.getDraftsSelector(state); }

    @Selector()
    static total(state: TextStateModel) { return super.totalSelector(state); }

    @Selector()
    static loading(state: TextStateModel) { return super.loadingSelector(state); }

    @Selector()
    static uploading(state: TextStateModel) { return super.uploadingSelector(state); }

    static text(id: number) {
        return super.getOneSelector<TextModel>(TextState, id);
    }

    static likes(id: number) {
        return super.likesSelector(TextState, id);
    }

    constructor(
        protected service: TextService,
        protected store: Store,
    ) {
        super(service, store);
    }

    @Action(TextActions.Init)
    init(ctx: StateContext<TextStateModel>, action: TextActions.Init) {
        return super.init(ctx, action, TextActions);
    }

    @Action(TextActions.Load)
    load(ctx: StateContext<TextStateModel>, action: TextActions.Load) {
        return super.load(ctx, action, TextActions.Load.type);
    }

    @Action(TextActions.LoadDrafts)
    loadDrafts(ctx: StateContext<TextStateModel>, action: TextActions.LoadDrafts) {
        return super.loadDrafts(ctx, action, TextActions.LoadDrafts.type);
    }

    @Action(TextActions.GetOne)
    getOne(ctx: StateContext<TextStateModel>, action: TextActions.GetOne) {
        this.store.dispatch(new TextActions.View(action.id));
        return super.getOne(ctx, action, TextActions.GetOne.type);
    }

    @Action(TextActions.Reset)
    reset(ctx: StateContext<TextStateModel>) {
        return super.reset(ctx);
    }

    @Action(TextActions.Add)
    add(ctx: StateContext<TextStateModel>, action: TextActions.Add) {
        return super.add(ctx, action, TextActions.Add.type, TextActions);
    }

    @Action(TextActions.Edit)
    edit(ctx: StateContext<TextStateModel>, action: TextActions.Edit) {
        return super.edit(ctx, action, TextActions.Edit.type, TextActions);
    }

    @Action(TextActions.Delete)
    delete(ctx: StateContext<TextStateModel>, action: TextActions.Delete) {
        return super.delete(ctx, action, TextActions.Delete.type);
    }

    @Action(TextActions.Like)
    like(ctx: StateContext<TextStateModel>, action: TextActions.Like) {
        return super.like(ctx, action, TextActions.Like.type);
    }

    @Action(TextActions.LoadLikes)
    loadLikes(ctx: StateContext<TextStateModel>, action: TextActions.LoadLikes) {
        return super.loadLikes(ctx, action, TextActions.LoadLikes.type);
    }

    @Action(TextActions.Comment)
    comment(ctx: StateContext<TextStateModel>, action: TextActions.Comment) {
        return super.comment(ctx, action, TextActions.Comment.type);
    }

    @Action(TextActions.EditComment)
    editComment(ctx: StateContext<TextStateModel>, action: TextActions.EditComment) {
        return super.editComment(ctx, action, TextActions.EditComment.type);
    }

    @Action(TextActions.LoadComments)
    loadComments(ctx: StateContext<TextStateModel>, action: TextActions.LoadComments) {
        return super.loadComments(ctx, action, TextActions.LoadComments.type);
    }

    @Action(TextActions.LoadCommentReplies)
    loadCommentReplies(ctx: StateContext<TextStateModel>, action: TextActions.LoadCommentReplies) {
        return super.loadCommentReplies(ctx, action, TextActions.LoadCommentReplies.type);
    }

    @Action(TextActions.LikeComment)
    likeComment(ctx: StateContext<TextStateModel>, action: TextActions.LikeComment) {
        return super.likeComment(ctx, action, TextActions.LikeComment.type);
    }

    @Action(TextActions.LoadCommentLikes)
    loadCommentLikes(ctx: StateContext<TextStateModel>, action: TextActions.LoadCommentLikes) {
        return super.loadCommentLikes(ctx, action, TextActions.LoadCommentLikes.type);
    }

    @Action(TextActions.DeleteComment)
    deleteComment(ctx: StateContext<TextStateModel>, action: TextActions.DeleteComment) {
        return super.deleteComment(ctx, action, TextActions.DeleteComment.type);
    }

    @Action(TextActions.InsertItems)
    insertItems(ctx: StateContext<TextStateModel>, action: TextActions.InsertItems) {
        return super.insertItems(ctx, action);
    }

    @Action(TextActions.View)
    view(ctx: StateContext<TextStateModel>, action: TextActions.View) {
        return super.view(ctx, action, TextActions.View.type);
    }

    @Action(TextActions.AddToBookmark)
    addToBookmark(ctx: StateContext<TextStateModel>, action: TextActions.AddToBookmark) {
      return super.addToBookmark(ctx, action, TextActions.AddToBookmark.type);
    }

    @Action(TextActions.DeleteFromBookmark)
    deleteFromBookmark(ctx: StateContext<TextStateModel>, action: TextActions.DeleteFromBookmark) {
      return super.deleteFromBookmark(ctx, action, TextActions.DeleteFromBookmark.type);
    }
}
