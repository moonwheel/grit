import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { tap, finalize, catchError } from 'rxjs/operators';
import { DateTime } from 'luxon';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { ServiceService } from 'src/app/shared/services/service.service';
import { ServiceActions } from '../actions/service.actions';
import { constants } from 'src/app/core/constants/constants';
import { AbstractStateModel, AbstractState } from './abstract.state';
import { SseService } from 'src/app/shared/services/sse.service';
import { Injectable } from '@angular/core';

export interface ServiceStateModel extends AbstractStateModel<ServiceModel> {
  items: {
    [id: number]: ServiceModel
  };
}

@State<ServiceStateModel>({
  name: 'services',
  defaults: {
    ...AbstractState.getDefaultState(),
  }
})

@Injectable()
export class ServiceState extends AbstractState {

  @Selector()
  static services(state: ServiceStateModel) { return super.getAllSelector(state); }

  @Selector()
  static drafts(state: ServiceStateModel) { return super.getDraftsSelector(state); }

  @Selector()
  static total(state: ServiceStateModel) { return super.totalSelector(state); }

  @Selector()
  static loading(state: ServiceStateModel) { return super.loadingSelector(state); }

  @Selector()
  static uploading(state: ServiceStateModel) { return super.uploadingSelector(state); }

  @Selector()
  static lazyLoading(state: ServiceStateModel) {
    return state.fetching;
  }

  static service(id: number) {
    return super.getOneSelector<ServiceModel>(ServiceState, id);
  }

  @Selector([ServiceState.services])
  static extendedServices(state: ServiceStateModel, services: ServiceModel[]) {
    const extendedServices = services.map(service => {
      const extendedService = { ...service };

      try {
        if (extendedService.schedule && extendedService.schedule.byDate && extendedService.schedule.byDate.length) {
          let start = DateTime.fromISO(extendedService.schedule.byDate[0].from);
          extendedService.schedule.byDate.forEach(date => {
            if (DateTime.fromISO(date.from) < start) {
              start = DateTime.fromISO(date.from);
            }
          });
          const startInfo = start.toFormat('dd MMM y, HH:mm');
          extendedService.schedule = {
            ...extendedService.schedule,
            startInfo
          };
        } else {
          const found = constants.DURATIONS.find(item =>
            extendedService.schedule
            && (
              item.value === extendedService.schedule.duration
              || item.value === extendedService.duration
            )
          );
          const durationInfo = found && found.name;
          extendedService.schedule = {
            ...extendedService.schedule,
            durationInfo
          };
        }
      } catch (err) {
        console.error(err);
      }

      return extendedService;
    });

    return extendedServices;
  }

  constructor(
    protected service: ServiceService,
    protected store: Store,
    protected sseService?: SseService,
  ) {
    super(service, store);

    this.sseService.fileProcessingComplete$.subscribe(({ entityType, entity }) => {
      if (entityType === 'service') {
        this.store.dispatch(new ServiceActions.FinishProcessing(entity.id, entity));
      }
    });
  }

  @Action(ServiceActions.Init)
  init(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Init) {
    return super.init(ctx, action, ServiceActions);
  }

  @Action(ServiceActions.Load)
  load(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Load) {
    if(action.lazy) {
      ctx.patchState({fetching: true})
    }

    return super.load(ctx, action, ServiceActions.Load.type, this.manageService);
  }

  @Action(ServiceActions.LoadDrafts)
  loadDrafts(ctx: StateContext<ServiceStateModel>, action: ServiceActions.LoadDrafts) {
    return super.loadDrafts(ctx, action, ServiceActions.LoadDrafts.type, this.manageService);
  }

  @Action(ServiceActions.GetOne)
  getOne(ctx: StateContext<ServiceStateModel>, action: ServiceActions.GetOne) {
    return super.getOne(ctx, action, ServiceActions.Add.type, this.manageService);
  }

  @Action(ServiceActions.Reset)
  reset(ctx: StateContext<ServiceStateModel>) {
    return super.reset(ctx);
  }

  @Action(ServiceActions.Add)
  add(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Add) {
    return super.add(ctx, action, ServiceActions.Add.type, ServiceActions, this.manageService);
  }

  @Action(ServiceActions.Upload)
  upload(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Upload) {
    const service: ServiceModel = this.store.selectSnapshot(ServiceState.service(action.id));
    return super.uploadManyFiles(ctx, action, 'service', service.photos, ServiceActions.Upload.type, ServiceActions);
  }

  @Action(ServiceActions.UpdateUploadingProgress)
  updateUploadingProgress(ctx: StateContext<ServiceStateModel>, action: ServiceActions.UpdateUploadingProgress) {
    return super.updateUploadingProgress(ctx, action);
  }

  @Action(ServiceActions.FinishProcessing)
  finishProcessing(ctx: StateContext<ServiceStateModel>, action: ServiceActions.FinishProcessing) {
    return super.finishProcessing(ctx, action, this.manageService);
  }

  @Action(ServiceActions.Edit)
  edit(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Edit) {
    return super.edit(ctx, action, ServiceActions.Edit.type, ServiceActions, this.manageService);
  }

  @Action(ServiceActions.Delete)
  delete(ctx: StateContext<ServiceStateModel>, action: ServiceActions.Delete) {
    return super.delete(ctx, action, ServiceActions.Delete.type);
  }

  @Action(ServiceActions.ChangeActiveState)
  changeServiceState({getState, patchState}: StateContext<ServiceStateModel>, {payload}) {
    return this.service.edit(payload).pipe(
      tap((data) => {
        const state = getState();
        const items = { ...state.items };
        items[payload.id] = {
          ...items[payload.id],
          active: payload.service.active,
        };
        patchState({ items });
      }, error => {
        console.error(ServiceActions.ChangeActiveState.type, error);
      })
    );
  }

  @Action(ServiceActions.InsertItems)
  insertItems(ctx: StateContext<ServiceStateModel>, action: ServiceActions.InsertItems) {
    return super.insertItems(ctx, action, this.manageService);
  }

  private manageService(service: ServiceModel, existingService?: ServiceModel) {
    if (service && service.photos) {
      service.photos = service.photos.sort((prev, curr) => prev.index - curr.index);
    }
    if (!service.photoCover && service.photos && service.photos.length) {
      service.photoCover = service.photos[0];
    }

    if (existingService && !service.isDetailed && existingService.isDetailed) {
      service.schedule = { ...existingService.schedule };
    }

    if (service.schedule) {
      if (service.schedule.byDate && service.schedule.byDate.length) {
        service.date_from = service.schedule.byDate[0].from;
        service.date_to = service.schedule.byDate[0].to;
      }
    }

    return service;
  }

  @Action(ServiceActions.AddToBookmark)
  addToBookmark(ctx: StateContext<ServiceStateModel>, action: ServiceActions.AddToBookmark) {
    return super.addToBookmark(ctx, action, ServiceActions.AddToBookmark.type);
  }

  @Action(ServiceActions.DeleteFromBookmark)
  deleteFromBookmark(ctx: StateContext<ServiceStateModel>, action: ServiceActions.DeleteFromBookmark) {
    return super.deleteFromBookmark(ctx, action, ServiceActions.DeleteFromBookmark.type);
  }
}

