import { State, Action, StateContext, Selector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { FeedActions } from '../actions/feed.actions';
import { NgZone, Injectable } from '@angular/core';

import { PhotoActions } from '../actions/photo.actions';
import { VideoActions } from '../actions/video.actions';
import { ArticleActions } from '../actions/article.actions';
import { ServiceActions } from '../actions/service.actions';
import { TextActions } from '../actions/text.actions';
import { FeedItemModel, AbstractEntityType, AbstractEntityModel } from 'src/app/shared/models';
import { AbstractActions } from '../actions/abstract.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { TextState, TextStateModel } from './text.state';
import { PhotoState, PhotoStateModel } from './photo.state';
import { VideoState, VideoStateModel } from './video.state';
import { ArticleState, ArticleStateModel } from './article.state';
import { UnfollowUser, ResetViewableUserStore } from '../actions/user.actions';
import { ServiceState, ServiceStateModel } from './service.state';

export class FeedStateModel {
  items: FeedItemModel[];
  loading: boolean;
  fetched: boolean;
  lazyLoading: boolean;
}

@State<FeedStateModel>({
  name: 'feed',
  defaults: {
    items: [],
    loading: false,
    fetched: false,
    lazyLoading: false,
  }
})

@Injectable()
export class FeedState {

  @Selector([
    TextState,
    PhotoState,
    VideoState,
    ArticleState,
    ServiceState,
  ])
  static items(
    feedState: FeedStateModel,
    textState: TextStateModel,
    photoState: PhotoStateModel,
    videoState: VideoStateModel,
    articleState: ArticleStateModel,
    serviceState: ServiceStateModel,
  ) {
    const items: AbstractEntityModel[] = [];

    for (const item of feedState.items) {
      if (item.tableName === 'text' && item.id in textState.items) {
        items.push(textState.items[item.id]);
      } else
      if (item.tableName === 'photo' && item.id in photoState.items) {
        items.push(photoState.items[item.id]);
      } else
      if (item.tableName === 'video' && item.id in videoState.items) {
        items.push(videoState.items[item.id]);
      } else
      if (item.tableName === 'article' && item.id in articleState.items) {
        items.push(articleState.items[item.id]);
      } else
      if (item.tableName === 'service' && item.id in serviceState.items) {
        items.push(serviceState.items[item.id]);
      }
    }

    return items;
  }

  @Selector()
  static loading(state: FeedStateModel) {
    return state.loading;
  }

  @Selector()
  static lazyLoading(state: FeedStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private userService: UserService,
    private actions$: Actions,
    private router: Router,
    private store: Store,
    private zone: NgZone,
  ) {
    this.subscribeToItemDeletion(PhotoActions.Delete, 'photo');
    this.subscribeToItemDeletion(VideoActions.Delete, 'video');
    this.subscribeToItemDeletion(ArticleActions.Delete, 'article');
    this.subscribeToItemDeletion(TextActions.Delete, 'text');
    this.subscribeToItemDeletion(ServiceActions.Delete, 'service');

    this.subscribeToItemAdding(PhotoState.photos, PhotoActions.Add, 'photo');
    this.subscribeToItemAdding(VideoState.videos, VideoActions.Add, 'video');
    this.subscribeToItemAdding(ArticleState.articles, ArticleActions.Add, 'article');
    this.subscribeToItemAdding(TextState.texts, TextActions.Add, 'text');
    this.subscribeToItemAdding(ServiceState.services, ServiceActions.Add, 'service');

    this.actions$.pipe(
      ofActionSuccessful(UnfollowUser)
    ).subscribe((action: UnfollowUser) => {
      this.store.dispatch(new FeedActions.Unfollow(action.id));
    });

    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new FeedActions.Reset());
    });
  }

  @Action(FeedActions.Load)
  load({ patchState, getState }: StateContext<FeedStateModel>, { page, limit, lazy }: FeedActions.Load) {
    let state = getState();

    if (!state.fetched) {
      patchState({ loading: true });
    }

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.userService.getFeed(page, limit).pipe(
      tap((result: AbstractEntityModel[]) => {
        state = getState();

        const texts = [];
        const photos = [];
        const videos = [];
        const articles = [];
        const services = [];

        let items: FeedItemModel[] = result.map(item => {
          const extended = this.handleItem(item);

          if (item.tableName === 'text') {
            texts.push(extended);
          } else
          if (item.tableName === 'photo') {
            photos.push(extended);
          } else
          if (item.tableName === 'video') {
            videos.push(extended);
          } else
          if (item.tableName === 'article') {
            articles.push(extended);
          } else
          if (item.tableName === 'service') {
            services.push(extended);
          }

          return {
            id: extended.id,
            tableName: extended.tableName,
            userId: extended.user.id,
          };
        });

        if (texts.length) {
          this.store.dispatch(new TextActions.InsertItems(texts));
        }

        if (photos.length) {
          this.store.dispatch(new PhotoActions.InsertItems(photos));
        }

        if (videos.length) {
          this.store.dispatch(new VideoActions.InsertItems(videos));
        }

        if (articles.length) {
          this.store.dispatch(new ArticleActions.InsertItems(articles));
        }

        if (services.length) {
          this.store.dispatch(new ServiceActions.InsertItems(services));
        }

        if (page > 1) {
          items = [ ...state.items, ...items ];
        } else {
          items = items;
        }

        patchState({ items, loading: false, fetched: true, lazyLoading: false, });
      }, error => {
        console.error(FeedActions.Load, error);
        patchState({ loading: false, lazyLoading: false });
      })
    );
  }

  @Action(FeedActions.DeleteItem)
  delete({ patchState, getState }: StateContext<FeedStateModel>, { id, tableName }: FeedActions.DeleteItem) {
    const state = getState();
    const items = [ ...state.items ];
    const index = items.findIndex(item => item.id === Number(id) && item.tableName === tableName);

    if (index !== -1) {
      items.splice(index, 1);
    }

    patchState({ items });
  }

  @Action(FeedActions.AddItem)
  add({ patchState, getState }: StateContext<FeedStateModel>, { item, tableName }: FeedActions.AddItem) {
    const state = getState();
    const newItem: FeedItemModel = { id: item.id, tableName };
    const items = [ newItem, ...state.items ];

    patchState({ items });
  }

  @Action(FeedActions.Unfollow)
  unfollow({ patchState, getState }: StateContext<FeedStateModel>, { userId }: FeedActions.Unfollow) {
    const state = getState();
    const items = state.items.filter(item => item.userId !== userId);

    patchState({ items });
  }

  @Action(FeedActions.Reset)
  reset({ patchState }: StateContext<FeedStateModel>) {
    patchState({ items: [], loading: false, fetched: false });
  }

  private subscribeToItemDeletion(DeleteAction: typeof AbstractActions.Delete, tableName: AbstractEntityType) {
    this.actions$.pipe(
      ofActionSuccessful(DeleteAction)
    ).subscribe(({ id }) => {
      this.store.dispatch(new FeedActions.DeleteItem(id, tableName));
    });
  }

  private subscribeToItemAdding(
    itemsSelector: (arg: any) => any,
    AddAction: typeof AbstractActions.Add,
    tableName: AbstractEntityType
  ) {
    this.actions$.pipe(
      ofActionSuccessful(AddAction)
    ).subscribe(() => {
      const items = this.store.selectSnapshot(itemsSelector);
      if (items[0]) {
        this.store.dispatch(new FeedActions.AddItem(items[0], tableName));
      }
    });
  }

  private handleItem(item: AbstractEntityModel): AbstractEntityModel {
    item.id = Number(item.id);
    item.likesCount = Number(item.likesCount);
    item.viewsCount = Number(item.viewsCount);
    item.commentsCount = Number(item.commentsCount);
    item.user = Utils.extendAccountWithBaseInfo(item.user);
    item.user.following = true;

    return item;
  }

  @Action(FeedActions.ViewItem)
  view(ctx: StateContext<FeedStateModel>, { id, tableName }: FeedActions.ViewItem) {
    switch (tableName) {
      case 'text':
        this.store.dispatch(new TextActions.View(id));
        break;
      case 'photo':
        this.store.dispatch(new PhotoActions.View(id));
        break;
      case 'video':
        this.store.dispatch(new VideoActions.View(id));
        break;
      case 'article':
        this.store.dispatch(new ArticleActions.View(id));
        break;
    }
  }
}
