import { State, Action, StateContext, Selector, createSelector, Store, StateOperator } from '@ngxs/store';
import { patch, compose } from '@ngxs/store/operators';
import { tap, catchError, filter as rxFilter, take, switchMap } from 'rxjs/operators';
import { SearchActions, SearchEntityType } from '../actions/search.actions';
import { SearchService } from 'src/app/shared/services/search.service';
import { constants } from 'src/app/core/constants/constants';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


export interface SearchItemModel {
  id: number;
  type: SearchEntityType;
  /* tslint:disable */
  text_person_fullname: string;
  text_person_pagename: string;
  text_business_name: string;
  text_business_pagename: string;
  person_fullname: string;
  person_pagename: string;
  business_name: string;
  business_pagename: string;
  business_thumb: string;
  text_title: string;
  created: string;
  amount_views: number;
  amount_likes: number;
  photo?: string;
  thumb?: string;
  filterName?: string;
  /* tslint:enable */

  pageLink?: string;
  userName?: string;
  pageName?: string;
}

export interface SearchSubStateModel {
  items: SearchItemModel[];
  total: number;
  loading: boolean;
  fetched: boolean;
  fetchedForAllList: boolean;
  currentSearchTerm: string;
}

export interface SearchStateModel {
  suggestions: any[];

  entities: {
    [key in SearchEntityType]: SearchSubStateModel
  };
  searching: boolean;
  lazyLoading: boolean;
  loading: boolean;
}

export const getDefaultSearchSubState = (): SearchSubStateModel => ({
  items: [],
  total: 0,
  loading: false,
  fetched: false,
  fetchedForAllList: false,
  currentSearchTerm: '',
});

export const patchSearchSubModel = (type: SearchEntityType, data: Partial<SearchSubStateModel>): StateOperator<SearchStateModel> => {
  return patch<SearchStateModel>({
    entities: patch({
      [type]: patch(data)
    }),
  });
};

const patchLazyLoading = (isLazyLoading: boolean): StateOperator<SearchStateModel> => {
  return patch<SearchStateModel>({
    lazyLoading: isLazyLoading,
  })
}

const patchLocalLoading = (loading: boolean): StateOperator<SearchStateModel> => {
  return patch<SearchStateModel>({
    loading,
  })
}


@State<SearchStateModel>({
  name: 'search',
  defaults: {
    suggestions: [],
    searching: false,
    lazyLoading: false,
    loading: false,
    entities: {
      all: getDefaultSearchSubState(),
      account: getDefaultSearchSubState(),
      photo: getDefaultSearchSubState(),
      video: getDefaultSearchSubState(),
      article: getDefaultSearchSubState(),
      product: getDefaultSearchSubState(),
      service: getDefaultSearchSubState(),
    }
  }
})

@Injectable()
export class SearchState {

  static items(key: SearchEntityType) {
    return createSelector([SearchState], (state: SearchStateModel) => {
      return state.entities[key].items;
    });
  }

  static total(key: SearchEntityType) {
    return createSelector([SearchState], (state: SearchStateModel) => {
      return state.entities[key].total;
    });
  }

  @Selector()
  static suggestions(state: SearchStateModel) {
    return state.suggestions;
  }

  @Selector()
  static loading(state: SearchStateModel) {
    return Object.keys(state.entities).some((key: SearchEntityType) => {
      const entity = state.entities[key];
      return entity.loading;
    });
  }

  @Selector()
  static localLoading(state: SearchStateModel) {
    return state.loading
  }

  @Selector()
  static lazyLoading(state: SearchStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private service: SearchService,
    private store: Store,
  ) {
  }

  @Action(SearchActions.Search)
  search(
    { getState, setState, patchState }: StateContext<SearchStateModel>,
    { text, searchType, page, limit, forAllList, location, lazy }: SearchActions.Search
  ) {
    if (!text) {
      return;
    }

    const state = getState();

    if (lazy) {
      patchState({ lazyLoading: lazy })
    }

    patchState({loading: true,})

    if (
      forAllList && !state.entities[searchType].fetchedForAllList
      || !forAllList && !state.entities[searchType].fetched
      || text !== state.entities[searchType].currentSearchTerm
    ) {
      patchSearchSubModel(searchType, {
        loading: true,
        fetched: text !== state.entities[searchType].currentSearchTerm ? false : state.entities[searchType].fetched
      })

      setState(
        compose(patchSearchSubModel(searchType, {
          loading: true,
          fetched: text !== state.entities[searchType].currentSearchTerm ? false : state.entities[searchType].fetched
        }))
      );
    }

    let observable: Observable<any>;

    if (location) {
      observable = this.service.searchByLocation(text, searchType, page, limit);
    } else {
      observable = this.service.search(text, searchType, page, limit);
    }

    return observable.pipe(
      tap((result) => {
        const { items, total } = this.getSearchItemsFromResponse(result);
        let newItems = [];

        if (page === 1) {
          newItems = items;
        } else {
          newItems = [...state.entities[searchType].items, ...items];
        }

        const newState: Partial<SearchSubStateModel> = {
          currentSearchTerm: text,
          items: newItems,
          total,
          loading: false,
        };

        if (!forAllList) {
          newState.fetched = true;
        } else {
          newState.fetchedForAllList = true;
        }

        setState(
          compose(
            patchLazyLoading(false),
            patchLocalLoading(false),
            patchSearchSubModel(searchType, newState)
          )
        );
      }, error => {
        console.error(SearchActions.Search.type, error);
        setState(
          patchSearchSubModel(searchType, { loading: false })
        );
      })
    );
  }

  @Action(SearchActions.GetAutoSuggestions, { cancelUncompleted: true })
  getAutoSuggestions({ patchState }: StateContext<SearchStateModel>, { text }: SearchActions.GetAutoSuggestions) {
    if (!text) {
      patchState({ suggestions: [] });
      return;
    }

    return this.service.autosuggest(text).pipe(
      tap((result) => {
        let suggestions = [];

        if (result[0] && result[0].options) {
          for (const option of result[0].options) {
            if (option && option.text) {
              suggestions.push(option.text);
            }
          }
        }

        suggestions = suggestions.sort((prev, curr) => prev.length - curr.length);

        patchState({ suggestions });
      })
    );
  }

  trackByHash(index: number, item: SearchItemModel) {
    return `${item.id}-${item.type}`;
  }

  private extendItem(item: SearchItemModel): SearchItemModel {
    item.pageLink = item.text_business_pagename || item.text_person_pagename || item.business_pagename || item.person_pagename;
    item.userName = item.text_business_name || item.text_person_fullname || item.business_name || item.person_fullname;
    item.pageName = item.text_business_pagename || item.text_person_pagename || item.business_pagename || item.person_pagename;

    if (!item.photo) {
      item.photo = item.type === 'account' ? constants.PLACEHOLDER_AVATAR_PATH : constants.PLACEHOLDER_NO_PHOTO_PATH;
    }

    if (!item.thumb) {
      item.thumb = item.photo;
    }

    return item;
  }

  private getSearchItemsFromResponse(response: any): { items: SearchItemModel[], total: number } {
    const items = [];

    if (response.aggregations && response.aggregations.by_id && response.aggregations.by_id.buckets) {
      for (const bucket of response.aggregations.by_id.buckets) {
        if (bucket.min_price_hits && bucket.min_price_hits.hits && bucket.min_price_hits.hits.hits) {
          const item = bucket.min_price_hits.hits.hits[0];
          if (item && item._source) {
            const extendedItem = this.extendItem(item._source);
            items.push(extendedItem);
          }
        }
      }
    }

    const total = response.hits && response.hits.total && response.hits.total.value || 0;

    return { items, total };
  }

}

