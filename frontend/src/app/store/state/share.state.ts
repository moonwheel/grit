import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ShareModel } from '../../shared/models/actions/share.model';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ShareState {


  private url = `${environment.baseUrl}/shares`;

  private shareState = new BehaviorSubject<ShareModel[]>([]);

  shares = this.shareState.asObservable();

  constructor(private http: HttpClient) { }

  getShares() {
    this.http.get<ShareModel[]>(this.url).subscribe(shares => { this.shareState.next(shares); });
  }

  getShare(id: number) {
    return this.http.get<ShareModel>(`${this.url}/${id}`);
  }

  addShare(share: ShareModel) {
    this.http.post<ShareModel>(this.url, share, httpOptions).subscribe(newShare => {
      this.shareState.next(this.shareState.getValue().concat([newShare]));
    });
  }

  deleteShare(id: number) {
    return this.http.delete<ShareModel>(`${this.url}/${id}`);
  }

}
