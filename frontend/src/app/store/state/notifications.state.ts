import { State, Action, StateContext, Selector, Store, ofActionSuccessful, Actions } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Utils } from 'src/app/shared/utils/utils';
import { NotificationModel } from 'src/app/shared/models';
import { NotificationActions } from '../actions/notification.actions';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { InitUser, ResetViewableUserStore } from '../actions/user.actions';
import { SseService } from 'src/app/shared/services/sse.service';
import { UserState } from './user.state';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

export const NOTIFICATIONS_PAGE_SIZE = 15;

export interface NotificationStateModel {
  items: NotificationModel[];
  loading: boolean;
  fetched: boolean;
  total: number;
  unread: number;
  lazyLoading: boolean;
}

const defaultState: NotificationStateModel = {
  items: [],
  loading: false,
  fetched: false,
  total: 0,
  unread: 0,
  lazyLoading: false,
};

@State<NotificationStateModel>({
  name: 'notification',
  defaults: {
    ...defaultState
  }
})

@Injectable()
export class NotificationState {

  @Selector()
  static items(state: NotificationStateModel) {
    return state.items;
  }

  @Selector()
  static loading(state: NotificationStateModel) {
    return state.loading;
  }

  @Selector()
  static total(state: NotificationStateModel) {
    return state.total;
  }

  @Selector()
  static unread(state: NotificationStateModel) {
    return state.unread;
  }

  @Selector()
  static lazyLoading(state: NotificationStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private notificationService: NotificationService,
    private store: Store,
    private snackbar: MatSnackBar,
    private actions$: Actions,
    private sseService: SseService,
    private router: Router,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new NotificationActions.Reset());
    });

    this.actions$.pipe(
      ofActionSuccessful(InitUser)
    ).subscribe(() => {
      this.store.dispatch(new NotificationActions.GetUnreadCount());
    });

    this.sseService.notifications$.subscribe(({ notification }) => {
      this.store.dispatch(new NotificationActions.AddItem(notification));
    });
  }

  @Action(NotificationActions.Reset)
  reset({ setState }: StateContext<NotificationStateModel>) {
    setState({ ...defaultState });
  }

  @Action(NotificationActions.Load)
  load({ patchState, getState }: StateContext<NotificationStateModel>, { page, limit, lazy }: NotificationActions.Load) {
    let state = getState();

    if (!state.fetched) {
      patchState({ loading: true });
    }

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.notificationService.getAll(page, limit).pipe(
      tap(({ items, total }) => {
        state = getState();

        items = items.map(item => this.extendItem(item));

        if (page > 1) {
          items = [ ...state.items, ...items ];
        } else {
          items = items;
        }

        patchState({ items, loading: false, fetched: true, total, lazyLoading: false, });
      }, error => {
        console.error(NotificationActions.Load.type, error);
        patchState({ loading: false, lazyLoading: false });
      })
    );
  }

  @Action(NotificationActions.GetUnreadCount)
  getUnreadCount({ patchState }: StateContext<NotificationStateModel>) {
    return this.notificationService.getUnreadCount().pipe(
      tap(({ count }) => {
        patchState({ unread: count });
      }, error => {
        console.error(NotificationActions.GetUnreadCount.type, error);
      })
    );
  }

  @Action(NotificationActions.MarkAllAsRead)
  markAllAsRead({ patchState, getState }: StateContext<NotificationStateModel>) {
    const state = getState();
    const items = state.items.map(item => {
      return {
        ...item,
        read: true,
      };
    });

    patchState({
      items,
      unread: 0,
    });

    return this.notificationService.readAll().pipe(
      tap(() => {}, error => {
        console.error(NotificationActions.MarkAllAsRead.type, error);
      })
    );
  }

  @Action(NotificationActions.AddItem)
  addItem({ patchState, getState }: StateContext<NotificationStateModel>, { item }: NotificationActions.AddItem) {
    item = this.extendItem(item);

    const state = getState();
    const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);
    const isNotificationsPage = this.router.url.startsWith('/notifications');

    let unread = state.unread;

    if (
      !isNotificationsPage
      && (
        loggedInAccount.followers_notification && item.action === 'follows'
        || loggedInAccount.likes_notification && item.action === 'liked'
        || loggedInAccount.comments_notification && item.action === 'commented'
        || loggedInAccount.tags_notification && (item.action === 'tagged' || item.action === 'mentioned')
      )
    ) {
      unread++;
    }

    if (isNotificationsPage) {
      item.read = true;
    }

    const rest = state.total > NOTIFICATIONS_PAGE_SIZE
      ? state.items.slice(0, state.items.length - 1)
      : state.items;

    const items = [ item, ...rest ];

    patchState({
      items,
      unread,
    });

    if (isNotificationsPage) {
      return this.notificationService.read([item.id]).pipe(
        tap(() => {}, error => {
          console.error(NotificationActions.AddItem.type, error);
        })
      );
    }
  }

  private extendItem(item: any): NotificationModel {
    item = {
      ...item,
      issuer: Utils.extendAccountWithBaseInfo(item.issuer),
      owner: Utils.extendAccountWithBaseInfo(item.owner),
    };

    const entity = item[item.notifications.content.type];

    if (entity) {
      const productType = item.notifications.content.type === 'product' ? 'shop' : item.notifications.content.type + 's';

      item.link = `/${item.owner.pageName}/${productType}/${entity.id}`;
    }

    if (item.notifications.content.type === 'article') {
      item.link = `/${item.owner.pageName}/blog/${entity.id}`;
    }

    if (item.notifications.action === 'follows' || item.action === 'liked') {
      item.link = `/${item.issuer.pageName}/home`;
    }

    if (item.photo) {
      item.thumb = item.photo.thumb;
    }

    if (item.video) {
      item.thumb = item.video.cover;
    }

    if (item.article) {
      item.thumb = item.article.thumb;
    }

    if (item.product) {
      // item.thumb = item.product.photos[0] && item.product.photos[0].photoUrl;
      item.thumb = item.product.cover?.thumbUrl;
    }

    if (item.service) {
      item.thumb = item.service.photos[0] && item.service.photos[0].link;
    }

    if (item.product_review) {
      item.thumb = item.thumbUrl;
      item.product_review.product.user = Utils.extendAccountWithBaseInfo(item.product_review.product.user);
      item.link = `/${item.product_review.product.user.pageName}/products/${item.product_review.product.id}`;
    }

    if (item.service_review) {
      item.thumb = item.service_review.service.photoCover.thumb;
      item.service_review.service.user = Utils.extendAccountWithBaseInfo(item.service_review.service.user);
      item.link = `/${item.service_review.service.user.pageName}/services/${item.service_review.service.id}`;
    }

    return item;
  }

}
