import { State, Action, StateContext, Selector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { tap, filter, take, switchMapTo, switchMap } from 'rxjs/operators';
import { ProductModel, ProductDetailsModel } from 'src/app/shared/models/content/products/product.model';
import { ProductService } from 'src/app/shared/services/product.service';
import { ProductActions } from '../actions/product.actions';
import { AbstractState, AbstractStateModel } from './abstract.state';
import { SseService } from 'src/app/shared/services/sse.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserState } from './user.state';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

export interface ProductStateModel extends AbstractStateModel<ProductModel> {
    items: {
        [id: number]: ProductModel,
    };
    detailedItems: ProductDetailsModel[];
    detailedItemsTotal: number;
    detailedItemsTotalFound: number;
    fetchedDetailedItems: boolean;
}

const getDefaultProductState = (): ProductStateModel => ({
    ...AbstractState.getDefaultState(),
    detailedItems: [],
    detailedItemsTotal: 0,
    detailedItemsTotalFound: 0,
    fetchedDetailedItems: false,
});

@State<ProductStateModel>({
    name: 'products',
    defaults: getDefaultProductState(),
})

@Injectable()
export class ProductState extends AbstractState {

    @Selector()
    static products(state: ProductStateModel) {
        return super.getAllSelector(state);
    }

    @Selector()
    static detailedItems(state: ProductStateModel) {
        return state.detailedItems;
    }

    @Selector()
    static detailedItemsTotal(state: ProductStateModel) {
        return state.detailedItemsTotal;
    }

    @Selector()
    static detailedItemsTotalFound(state: ProductStateModel) {
        return state.detailedItemsTotalFound;
    }

    @Selector()
    static drafts(state: ProductStateModel) {
        return super.getDraftsSelector(state);
    }

    @Selector()
    static total(state: ProductStateModel) {
        return super.totalSelector(state);
    }

    @Selector()
    static totalFound(state: ProductStateModel) {
        return super.totalFoundSelector(state);
    }

    @Selector()
    static loading(state: ProductStateModel) {
        return super.loadingSelector(state);
    }

    @Selector()
    static uploading(state: ProductStateModel) {
        return super.uploadingSelector(state);
    }

    @Selector()
    static lazyLoading(state: ProductStateModel) {
        return state.fetching;
    }

    static product(id: number) {
        return super.getOneSelector<ProductModel>(ProductState, id);
    }

    constructor(
        protected service: ProductService,
        protected store: Store,
        protected sseService?: SseService,
        protected router?: Router,
        protected snackbar?: MatSnackBar,
        protected actions$?: Actions,
        protected translateService?: TranslateService,
    ) {
        super(service, store);

        this.sseService.fileProcessingComplete$.subscribe(({ entityType, entity }) => {
            if (entityType === 'product') {
                this.store.dispatch(new ProductActions.FinishProcessing(entity.id, entity));
            }
        });

        this.actions$.pipe(
            ofActionSuccessful(ProductActions.Add),
            switchMapTo(
                this.router.events.pipe(
                    filter(event => event instanceof NavigationEnd),
                    take(1)
                ),
            )
        ).subscribe((event: NavigationEnd) => {
            if (event.url.includes('/feed') || event.url.includes('/home')) {
                this.translateService.get('INFO_MESSAGES.NO_PRODUCTS_ON_TIMELINE_AND_FEED').subscribe(message => {
                    this.snackbar.open(message);
                });
            }
        });
    }

    // @Action(ProductActions.Init)
    // init(ctx: StateContext<ProductStateModel>, action: ProductActions.Init) {
    // return super.init(ctx, action, ProductActions);
    // }

    @Action(ProductActions.Init)
    init(ctx: StateContext<ProductStateModel>, action: ProductActions.Init) {
        const state = ctx.getState();

        if (state.loading) {
            return;
        }

        if (!state.fetched) {
            ctx.patchState({ loading: true });
        }

        return ctx.dispatch(new ProductActions.Load(1, action.limit, action.filter, action.order, '', action.categoryId));
    }

    // @Action(ProductActions.Load)
    // load(ctx: StateContext<ProductStateModel>, action: ProductActions.Load) {
    // return super.load(ctx, action, ProductActions.Load.type, this.manageProduct);
    // }

    @Action(ProductActions.Load)
    load(ctx: StateContext<ProductStateModel>, action: ProductActions.Load) {
        if (action.lazy) {
            ctx.patchState({ fetching: true })
        }

        return this.store.selectOnce(UserState.viewableAccountId).pipe(
            switchMap(accountId => this.service.getAll(
                action.page,
                action.limit,
                action.filter,
                action.order,
                accountId,
                false,
                action.search,
                action.categoryId,
            )),
            tap(data => this.setLoadedItems(ctx, action, data, this.manageProduct), error => {
                console.error(ProductActions.Load.type, error);
                ctx.patchState({ loading: false });
            }),
        );
    }

    @Action(ProductActions.LoadDrafts)
    loadDrafts(ctx: StateContext<ProductStateModel>, action: ProductActions.LoadDrafts) {
        return super.loadDrafts(ctx, action, ProductActions.LoadDrafts.type, this.manageProduct);
    }

    @Action(ProductActions.GetOne)
    getOne(ctx: StateContext<ProductStateModel>, action: ProductActions.GetOne) {
        return super.getOne(ctx, action, ProductActions.GetOne.type, this.manageProduct);
    }

    @Action(ProductActions.Reset)
    reset(ctx: StateContext<ProductStateModel>) {
        ctx.patchState(getDefaultProductState());
    }

    @Action(ProductActions.Add)
    add(ctx: StateContext<ProductStateModel>, action: ProductActions.Add) {
        return super.add(ctx, action, ProductActions.Add.type, ProductActions, this.manageProduct);
    }

    @Action(ProductActions.Upload)
    upload(ctx: StateContext<ProductStateModel>, action: ProductActions.Upload) {
        const product: ProductModel = this.store.selectSnapshot(ProductState.product(action.id));
        return super.uploadManyFiles(ctx, action, 'product', product.photos, ProductActions.Upload.type, ProductActions);
    }

    @Action(ProductActions.UpdateUploadingProgress)
    updateUploadingProgress(ctx: StateContext<ProductStateModel>, action: ProductActions.UpdateUploadingProgress) {
        return super.updateUploadingProgress(ctx, action);
    }

    @Action(ProductActions.FinishProcessing)
    finishProcessing(ctx: StateContext<ProductStateModel>, action: ProductActions.FinishProcessing) {
        return super.finishProcessing(ctx, action, this.manageProduct);
    }

    @Action(ProductActions.Edit)
    edit(ctx: StateContext<ProductStateModel>, action: ProductActions.Edit) {
        return super.edit(ctx, action, ProductActions.Edit.type, ProductActions, this.manageProduct);
    }

    @Action(ProductActions.ChangeActiveState)
    changeProductState({ getState, patchState }: StateContext<ProductStateModel>, { payload }) {
        return this.service.changeState(payload.id).pipe(
            tap(() => {
                const state = getState();
                const items = { ...state.items };
                const detailedItems = [...state.detailedItems].map(item => {
                    if (+item.id === +payload.id) {
                        return {
                            ...item,
                            active: !payload.active,
                        };
                    } else {
                        return item;
                    }
                });

                items[payload.id] = {
                    ...items[payload.id],
                    active: !payload.active,
                };
                patchState({ items, detailedItems });
            }, error => {
                console.error('Change Product State Action', error);
            }),
        );
    }

    @Action(ProductActions.Delete)
    delete(ctx: StateContext<ProductStateModel>, action: ProductActions.Delete) {
        return super.delete(ctx, action, ProductActions.Delete.type);
    }

    @Action(ProductActions.InsertItems)
    insertItems(ctx: StateContext<ProductStateModel>, action: ProductActions.InsertItems) {
        return super.insertItems(ctx, action, this.manageProduct);
    }

    private manageProduct(product: ProductModel, existing: ProductModel): ProductModel {
        product = { ...product };

        if (existing && existing.isDetailed && existing.photos && !product.isDetailed) {
            product.photos = existing.photos;
        }

        if (existing && existing.isDetailed && existing.variants && !product.isDetailed) {
            product.variants = existing.variants;
        }

        product.photos = (product.photos || [])
            .filter(photo => !!photo.id)
            .sort((prev, curr) => prev.order - curr.order);

        if (product.type === 'singleProduct') {
            product.cover = product.cover || { ...product.photos[0] };
        }

        if (product.type === 'productVariants') {
            if (product.variants) {
                product.variants = product.variants.map(variant => {
                    return {
                        ...variant,
                        photos: variant.photos
                            .slice()
                            .sort((prev, curr) => prev.order - curr.order)
                            .map(item => item.photo || item)
                    };
                });
            }

            // if (!product.cheapestVariant) {
            //     product.cheapestVariant = product.variants.reduce((acc, curr) => {
            //         if (acc && curr.enable && curr.price < acc.price) {
            //             return curr;
            //         } else {
            //             return acc;
            //         }
            //     }, product.variants[0]);
            // }

            // if (product.cheapestVariant) {
                product.cover = product.cover_ ? product.cover_ : product.cover;
                // product.price = product.cheapestVariant.price;
            // }
        }

        return product;
    }

    @Action(ProductActions.AddToBookmark)
    addToBookmark(ctx: StateContext<ProductStateModel>, action: ProductActions.AddToBookmark) {
        super.addToBookmark(ctx, action, ProductActions.AddToBookmark.type);

        const state = ctx.getState();
        const detailedItems = [...state.detailedItems].map(item => {
            if (+item.id === +action.id) {
                return {
                    ...item,
                    bookmarked: true
                };
            }
            return item;
        });

        ctx.patchState({ detailedItems });
    }

    @Action(ProductActions.DeleteFromBookmark)
    deleteFromBookmark(ctx: StateContext<ProductStateModel>, action: ProductActions.DeleteFromBookmark) {
        super.deleteFromBookmark(ctx, action, ProductActions.DeleteFromBookmark.type);

        const state = ctx.getState();
        const detailedItems = [...state.detailedItems].map(item => {
            if (+item.id === +action.id) {
                return {
                    ...item,
                    bookmarked: false
                };
            }
            return item;
        });

        ctx.patchState({ detailedItems });
    }

    @Action(ProductActions.GetDetailedList)
    getDetailedList(ctx: StateContext<ProductStateModel>, action: ProductActions.GetDetailedList) {
        let state = ctx.getState();

        if (!state.fetchedDetailedItems && action.page === 1) {
            ctx.patchState({ loading: true });
        }

        if(action.lazy) {
            ctx.patchState({fetching: true})
        }

        return this.store.selectOnce(UserState.viewableAccountId).pipe(
            switchMap(accountId => this.service.getDetailedList(
                action.page,
                action.limit,
                action.filter,
                action.order,
                accountId,
                false,
                action.search,
                action.categoryId,
            )),
            tap(data => {
                state = ctx.getState();
                const total = data.total;
                const newItems = data.items.map(item => {
                    const variantText = [];
                    const keys = [
                        'product_variants_color',
                        'product_variants_flavor',
                        'product_variants_material',
                        'product_variants_size',
                    ];
                    for (const key of keys) {
                        if (item[key]) {
                            variantText.push(item[key]);
                        }
                    }

                    item.variantText = variantText.join(' / ');
                    item.sales = item.sales || 0;
                    item.revenue = item.revenue || 0;
                    item.price = item.price || 0;

                    return item;
                });

                const detailedItems = action.page <= 1
                    ? [...newItems]
                    : [...state.detailedItems, ...newItems];

                ctx.patchState({
                    detailedItems,
                    detailedItemsTotal: action.search ? state.detailedItemsTotal : total,
                    detailedItemsTotalFound: action.search ? total : state.detailedItemsTotalFound,
                    loading: false,
                    fetchedDetailedItems: true,
                    fetching: false
                });
            }, error => {
                console.error(ProductActions.GetDetailedList.type, error);
                ctx.patchState({ loading: false, fetching: false, });
            }),
        );
    }
}
