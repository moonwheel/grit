import { State, Action, StateContext, Selector, createSelector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { tap, catchError } from 'rxjs/operators';
import { AddressModel } from 'src/app/shared/models/user/user.model';
import { AddressService } from 'src/app/shared/services/address.service';
import { AddAddress, InitAddress, DeleteAddress, EditAddress, LoadAddresses, ResetAddresses } from '../actions/address.actions';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { ResetViewableUserStore } from '../actions/user.actions';

export class AddressStateModel {
    addresses: AddressModel[];
    loading: boolean;
}

@State<AddressStateModel>({
    name: 'address',
    defaults: {
        addresses: [],
        loading: false
    }
})

@Injectable()
export class AddressState {

    @Selector()
    static addresses(state: AddressStateModel) { return state.addresses; }

    static address(id: number) {
        return createSelector([AddressState.addresses], (addresses: AddressModel[]) => {
            const found = addresses.find(address => +address.id === +id);
            return found && { ...found };
        });
    }

    @Selector()
    static loading(state: AddressStateModel) { return state.loading; }

    constructor(
        private addressService: AddressService,
        private store: Store,
        private actions$: Actions,
    ) {
        this.actions$.pipe(
            ofActionSuccessful(ResetViewableUserStore)
        ).subscribe(() => {
            this.store.dispatch(new ResetAddresses());
        });
    }

    @Action(InitAddress)
    init({ patchState, getState, dispatch }: StateContext<AddressStateModel>) {
        const state = getState();
        if (state.loading) {
            return;
        }

        patchState({ loading: true });

        return dispatch(new LoadAddresses());
    }

    @Action(LoadAddresses)
    load({ patchState }: StateContext<AddressStateModel>) {
        return this.addressService.getAll().pipe(
            tap(addresses => patchState({ addresses, loading: false })),
            catchError(error => {
                console.error(LoadAddresses.type, error);
                patchState({ loading: false });
                return throwError(error);
            })
        );
    }

    @Action(ResetAddresses)
    reset(ctx: StateContext<AddressStateModel>) {
        ctx.patchState({
            addresses: [],
        });
    }

    @Action(AddAddress)
    addAddress({ getState, setState }: StateContext<AddressStateModel>, { payload }) {
        return this.addressService.add(payload).pipe(tap((data) => {
            const state = getState();
            setState({
                ...state,
                addresses: [...state.addresses, data]
            });
        },
            error => {
                console.error('Address Add Action', error);
            }
        ));
    }

    @Action(EditAddress)
    editAddress({ getState, setState }: StateContext<AddressStateModel>, { payload, index }) {
        return this.addressService.edit(payload).pipe(tap((data) => {
            const state = getState();
            const addresses = [ ...state.addresses ];
            const foundIndex = index === undefined ? addresses.findIndex(item => +item.id === +data.id) : index;
            addresses[foundIndex] = data;
            setState({
                ...state,
                addresses
            });
        },
            error => {
                console.error('Address Edit Action', error);
            }
        ));
    }

    @Action(DeleteAddress)
    deleteAddress({ getState, patchState }: StateContext<AddressStateModel>, { id }) {
        const state = getState();

        patchState({
            addresses: state.addresses.filter(address => +address.id !== +id)
        });

        return this.addressService.delete(id).pipe(
            catchError(error => {
                console.error('Address Delete Action', error);
                return throwError(error);
            })
        );
    }
}
