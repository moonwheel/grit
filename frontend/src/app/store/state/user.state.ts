import {
  State,
  Action,
  StateContext,
  Selector,
  Store,
  NgxsOnInit,
  createSelector,
  Actions,
  ofActionSuccessful,
} from '@ngxs/store';
import { tap, switchMap, switchMapTo, filter } from 'rxjs/operators';
import { merge, of } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import {
  ChangeUserLanguage,
  InitUser,
  UpdateUser,
  LoadViewableUser,
  ResetViewableUserStore,
  FollowUser,
  UnfollowUser,
  GetFollowings,
  GetFollowers,
  ResetLoggedInUserStore,
  BlockUser,
  UnblockUser,
  BlockedList,
  GetFollowRequests,
  ApproveFollowingRequest,
  RejectFollowingRequest,
  AddFollowRequest,
  ChangeUserFollowingRequest,
  ValidateKycDocument,
  ToggleNotificationSetting,
  SetLocalLanguage,
  SetViewableUser,
  SetViewableAccountFollowingStatus,
} from '../actions/user.actions';
import { constants } from 'src/app/core/constants/constants';
import { NgZone, Injectable } from '@angular/core';
import { AccountModel } from 'src/app/shared/models/user/account.model';
import { Utils } from 'src/app/shared/utils/utils';
import { ArticleActions } from '../actions/article.actions';
import { TextActions } from '../actions/text.actions';
import { VideoActions } from '../actions/video.actions';
import { ProductActions } from '../actions/product.actions';
import { ServiceActions } from '../actions/service.actions';
import { PhotoActions } from '../actions/photo.actions';
import { AuthState } from './auth.state';
import { SseService } from 'src/app/shared/services/sse.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SetCartTotal } from '../actions/cart.actions';
import { WebsocketService } from 'src/app/shared/services/websocket.service';
import { Logout } from '../actions/auth.actions';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { TranslateService } from '@ngx-translate/core';

export const LANG_STORAGE_KEY = 'user.language';

export interface UserStateModel {
  loggedInAccount: AccountModel;
  viewableAccount: AccountModel;
  loading: boolean;
  followings: AccountModel[];
  followers: AccountModel[];
  loadingFollow: boolean;
  followedAccountIds: {
    [id: number]: boolean
  };
  blocked: AccountModel[];
  loadingBlocked: boolean;
  followRequests: AccountModel[];
  followRequestsFetched: boolean;
  language: string;
  lazyLoading: boolean; // TODO move followings logic to diferent state
}

@State<UserStateModel>({
  name: 'user',
  defaults: {
    loggedInAccount: null,
    viewableAccount: null,
    loading: false,
    lazyLoading: false,
    followings: [],
    followers: [],
    loadingFollow: false,
    followedAccountIds: {},
    blocked: [],
    loadingBlocked: false,
    followRequests: [],
    followRequestsFetched: false,
    language: 'deutsch',
  }
})

@Injectable()
export class UserState implements NgxsOnInit {

  @Selector()
  static loading(state: UserStateModel) {
    return state.loading;
  }

  @Selector()
  static loadingFollow(state: UserStateModel) {
    return state.loadingFollow;
  }

  @Selector()
  static type(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.person.type;
  }

  @Selector()
  static info(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.person;
  }

  @Selector()
  static isMyAccount(state: UserStateModel) {
    return state.loggedInAccount && !state.viewableAccount || (
      state.loggedInAccount && state.viewableAccount &&
      state.loggedInAccount.id === state.viewableAccount.id
    );
  }

  @Selector()
  static isBusinessAccount(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.person && state.loggedInAccount.person.type === 'business';
  }

  @Selector()
  static loggedInAccount(state: UserStateModel) {
    return state.loggedInAccount;
  }

  @Selector()
  static loggedInAccountId(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.id;
  }

  @Selector([UserState.loggedInAccount])
  static loggedInAccountPageName(state: UserStateModel, account: AccountModel) {
    return account && Utils.getAccountPageName(account);
  }

  @Selector([UserState.loggedInAccount])
  static loggedInAccountPhoto(state: UserStateModel, account: AccountModel) {
    const photo = account && (account.business ? account.business.photo : account.person.photo);
    return photo ? photo : constants.PLACEHOLDER_AVATAR_PATH;
  }

  @Selector()
  static loggedInAccountRoleType(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.role && state.loggedInAccount.role.type;
  }

  @Selector([UserState.loggedInAccount])
  static loggedInAccountVerification(state: UserStateModel, account: AccountModel) {
    return account && (account.business ? account.business.verificationStatus : account.person.verificationStatus);
  }

  @Selector()
  static viewableAccount(state: UserStateModel) {
    return state.viewableAccount ? state.viewableAccount : state.loggedInAccount;
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountUser(state: UserStateModel, account: AccountModel) {
    return account && account.person;
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountPhoto(state: UserStateModel, account: AccountModel) {
    const photo = account && (account.business ? account.business.photo : account.person.photo);
    return photo || constants.PLACEHOLDER_AVATAR_PATH;
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountPageName(state: UserStateModel, account: AccountModel) {
    return account && Utils.getAccountPageName(account);
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountFullName(state: UserStateModel, account: AccountModel) {
    return account && (account.business ? account.business.name : account.person.fullName);
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountDescription(state: UserStateModel, account: AccountModel) {
    return account && (account.business ? account.business.description : account.person.description);
  }

  @Selector([UserState.viewableAccount])
  static viewableAccountIsBlocked(state: UserStateModel, account: AccountModel) {
    return account && account.isBlocked;
  }

  @Selector([UserState.viewableAccount, UserState.loggedInAccount])
  static viewableAccountId(state: UserStateModel, viewableAccount: AccountModel, loggedInAccount: AccountModel) {
    if (viewableAccount.id === loggedInAccount.id) {
      return null;
    }

    if (viewableAccount.baseBusinesAccount) {
      return viewableAccount.baseBusinesAccount.id;
    } else {
      return viewableAccount.id;
    }
  }

  @Selector([UserState.isMyAccount])
  static isFolowing({ viewableAccount }: UserStateModel, isMyAccount: boolean) {
    return !isMyAccount && !!viewableAccount.following;
  }

  @Selector([UserState.isMyAccount])
  static followingRequest({ viewableAccount }: UserStateModel, isMyAccount: boolean) {
    return !isMyAccount && !!viewableAccount.followingRequest;
  }

  @Selector([UserState.isMyAccount])
  static followingStatus({ viewableAccount }: UserStateModel, isMyAccount: boolean) {
    return viewableAccount.followingStatus;
  }

  @Selector()
  static followings({ followings }: UserStateModel) {
    return followings;
  }

  @Selector()
  static followers({ followers }: UserStateModel) {
    return followers;
  }

  @Selector()
  static isBlocked({ viewableAccount }: UserStateModel) {
    return viewableAccount.isBlocked;
  }

  @Selector()
  static isBlocking({ viewableAccount }: UserStateModel) {
    return viewableAccount.isBlocking;
  }

  @Selector()
  static blocked(state: UserStateModel) {
    return state.blocked;
  }

  @Selector()
  static loadingBlocked(state: UserStateModel) {
    return state.loadingBlocked;
  }

  @Selector()
  static followRequests({ followRequests }: UserStateModel) {
    return followRequests;
  }

  @Selector()
  static followRequestsCount(state: UserStateModel) {
    return state.loggedInAccount && state.loggedInAccount.followingRequestCount;
  }

  @Selector()
  static language(state: UserStateModel) {
    return state.language;
  }

  @Selector([UserState.viewableAccount])
  static isPrivate(state: UserStateModel, account: AccountModel) {
    if (account && account.business) {
      return account.business.privacy === 'private';
    } else if (account && account.person) {
      return account.person.privacy === 'private';
    } else {
      return false;
    }
  }

  @Selector()
  static lazyLoadingFollowing(state: UserStateModel) {
    return state.lazyLoading;
  }

  static isCachedFollowing(id: number, defaultValue: boolean) {
    return createSelector([UserState], (state: UserStateModel) => {
      if (id in state.followedAccountIds) {
        return state.followedAccountIds[id];
      } else {
        return !!defaultValue;
      }
    });
  }

  constructor(
    private userService: UserService,
    private router: Router,
    private store: Store,
    private zone: NgZone,
    private sseService: SseService,
    private wsService: WebsocketService,
    private snackbar: MatSnackBar,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private actions$: Actions,
  ) {
    this.sseService.message$.subscribe(({ entityType, entity }) => {
      if (entityType === 'followingRequest') {
        this.store.dispatch(new AddFollowRequest(entity));
      }
      if (entityType === 'followingRequestChanged') {
        this.store.dispatch(new ChangeUserFollowingRequest(entity.userId, entity.status, entity.followId));
      }
    });

    merge(
      this.actions$.pipe(ofActionSuccessful(InitUser)),
      this.actions$.pipe(ofActionSuccessful(UpdateUser)),
      this.actions$.pipe(ofActionSuccessful(ChangeUserLanguage)),
    ).pipe(
      switchMapTo(this.store.select(AuthState.loading)),
      filter(loading => !loading),
    ).subscribe(() => {
      const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);

      if (loggedInAccount) {
        const language = this.store.selectSnapshot(UserState.language);

        if (language !== loggedInAccount.person.language) {
          this.store.dispatch(new SetLocalLanguage(loggedInAccount.person.language));
        }
      }
    });
  }

  ngxsOnInit({ getState, dispatch }: StateContext<UserStateModel>) {
    const state = getState();
    this.translateService.use(state.language);
    dispatch(new InitUser());
  }

  @Action(InitUser)
  init({patchState}: StateContext<UserStateModel>) {
    return this.store.selectOnce(AuthState.token).pipe(
      switchMap(token => {
        if (token) {
          return this.userService.getMe().pipe(
            tap((data) => {
              data.person.type = data.business ? 'business' : 'person';
              const person = data.person;
              const followedAccountIds = {};

              if (data.business) {
                person.business = data.business;
              }

              for (const item of data.followings) {
                followedAccountIds[item.followedAccount.id] = true;
              }

              delete data.followings;

              this.store.dispatch(new SetCartTotal(data.cartProductsCount));

              patchState({
                loggedInAccount: data,
                followedAccountIds,
                loading: false,
              });
              this.sseService.init();
              this.wsService.init();
            }, error => {
              patchState({
                loading: false,
              });
              console.error('User Init Action', error);
              this.store.dispatch(new Logout());
              return of();
            })
          );
        } else {
          patchState({
            loggedInAccount: null,
            loading: false,
          });
          return of();
        }
      }),
    );
  }

  @Action(LoadViewableUser)
  loadViewableUser({patchState}: StateContext<UserStateModel>, { username }) {
    patchState({ loading: true });
    return this.userService.getByUsername(username).pipe(
      tap((data) => {
        data.person.type = data.business ? 'business' : 'person';
        const person = data.person;
        if (data.business) {
          person.business = data.business;
        }
        patchState({
          viewableAccount: data,
          loading: false,
        });
      },
      error => {
        console.error(LoadViewableUser.type, error);
        patchState({ loading: false });
      }),
    );
  }

  @Action(SetViewableUser)
  setViewableUser({patchState}: StateContext<UserStateModel>, { user }) {
    user.person.type = user.business ? 'business' : 'person';
    const person = user.person;
    if (user.business) {
      person.business = user.business;
    }
    patchState({
      viewableAccount: user,
      loading: false,
    });
  }

  @Action(UpdateUser)
  update({patchState, getState}: StateContext<UserStateModel>, {payload}) {
    if (payload && payload.email) {
      payload.email = payload.email.toLowerCase();
    }
    patchState({ loading: true });
    return this.userService.updateUser(payload).pipe(
      tap((data) => {
        data.person.type = data.business ? 'business' : 'person';
        const person = data.person;
        const state = getState();
        if (data.business) {
          person.business = data.business;
        }
        patchState({
          loggedInAccount: {
            ...state.loggedInAccount,
            person
          },
          loading: false,
        });
      },
      error => {
        patchState({ loading: false });
        console.error(UpdateUser.type, error);
      }),
    );
  }

  @Action(ChangeUserLanguage)
  changeUserLanguage({patchState, getState}: StateContext<UserStateModel>, {payload}) {
    return this.userService.changeLanguage(payload).pipe(tap(
      () => {
        const state = getState();

        patchState({
          loggedInAccount: {
            ...state.loggedInAccount,
            person: {
              ...state.loggedInAccount.person,
              language: payload.language,
            }
          }
        });
        return;
      },
      error => {
        console.error(ChangeUserLanguage.type, error);
      }
    ));
  }

  @Action(ResetViewableUserStore)
  resetStore({patchState}: StateContext<UserStateModel>) {
    patchState({
      viewableAccount: null,
      followers: [],
    });

    return this.store.dispatch([
      new ArticleActions.Reset(),
      new TextActions.Reset(),
      new VideoActions.Reset(),
      new PhotoActions.Reset(),
      new ProductActions.Reset(),
      new ServiceActions.Reset(),
    ]);
  }

  @Action(ResetLoggedInUserStore)
  resetLoggedInUserStore({patchState}: StateContext<UserStateModel>) {
    patchState({
      loggedInAccount: null,
      loading: true,
    });

    return this.store.dispatch(new ResetViewableUserStore());
  }

  @Action(FollowUser)
  follow({ patchState, getState }: StateContext<UserStateModel>, { id }: FollowUser) {
    let state = getState();
    let viewableAccount = state.viewableAccount && { ...state.viewableAccount };

    const loggedInAccount = { ...state.loggedInAccount };

    if (loggedInAccount.followingsCount >= constants.MAX_FOLLOWERS_NUMBER) {
      const num = constants.MAX_FOLLOWERS_NUMBER;
      this.translateService.get('ERRORS.MAX_FOLLOWERS', { num }).subscribe(message => {
        this.snackbar.open(message);
      });
      return;
    }

    const followedAccountIds = { ...state.followedAccountIds, [id]: true };
    let followers = [ ...state.followers ];

    loggedInAccount.followingsCount++;

    if (viewableAccount && viewableAccount.id === id) {
      if (viewableAccount.person.privacy === 'private') {
        viewableAccount.followingRequest = true;
      } else {
        viewableAccount.following = true;
        viewableAccount.followersCount++;
      }

      const newFollower = Utils.extendAccountWithBaseInfo({ ...state.loggedInAccount });

      followers = [ newFollower, ...followers ];
    }

    const index = followers.findIndex(item => item.id === id);

    if (index !== -1) {
      followers[index] = {
        ...followers[index],
        following: true,
        followersCount: followers[index].followingsCount + 1
      };
    }

    patchState({ viewableAccount, loggedInAccount, followers, followedAccountIds });

    return this.userService.follow(id).pipe(
      tap(data => {
        state = getState();
        viewableAccount = state.viewableAccount && { ...state.viewableAccount };

        if (viewableAccount && viewableAccount.id === id) {
          this.store.dispatch(new SetViewableAccountFollowingStatus(data.status));
        }
      })
    );
  }

  @Action(UnfollowUser)
  unfollow({ patchState, getState }: StateContext<UserStateModel>, { id }: UnfollowUser) {
    const state = getState();
    const viewableAccount = state.viewableAccount && { ...state.viewableAccount };
    const loggedInAccount = { ...state.loggedInAccount };
    const followedAccountIds = { ...state.followedAccountIds, [id]: false };
    const followings = state.followings.filter(item => item.id !== id);

    let followers = [ ...state.followers ];

    loggedInAccount.followingsCount--;

    if (viewableAccount && viewableAccount.id === id) {
      if (viewableAccount.person.privacy === 'private' && viewableAccount.followingRequest) {
        viewableAccount.followingRequest = false;
      } else {
        viewableAccount.following = false;
        viewableAccount.followersCount--;
        viewableAccount.followingStatus = 'rejected';
        followers = followers.filter(item => item.id !== loggedInAccount.id);
      }
    }

    const index = followers.findIndex(item => item.id === id);
    if (index !== -1) {
      followers[index] = {
        ...followers[index],
        following: false,
      };
    }

    patchState({ viewableAccount, loggedInAccount, followedAccountIds, followings, followers });

    return this.userService.unfollow(id);
  }

  @Action(SetViewableAccountFollowingStatus)
  setViewableAccountFollowingStatus(
    { patchState, getState }: StateContext<UserStateModel>,
    { status }: SetViewableAccountFollowingStatus
  ) {
    const state = getState();
    const viewableAccount = state.viewableAccount && { ...state.viewableAccount };

    if (viewableAccount) {
      viewableAccount.followingStatus = status;
    }

    patchState({ viewableAccount });
  }

  @Action(GetFollowings)
  getFollowings({ patchState, getState }: StateContext<UserStateModel>, { page, limit, search, lazy }: GetFollowings) {
    let state = getState();

    if (page === 1 && !state.followings.length && !search) {
      patchState({ loadingFollow: true });
    }

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    return this.userService.getFollowings(page, limit, search).pipe(
      tap(followingsData => {
        const followings = followingsData.map(({ followedAccount }) => {
          return Utils.extendAccountWithBaseInfo(followedAccount);
        });

        if (page === 1) {
          patchState({
            followings,
            loadingFollow: false
          });
        } else {
          state = getState();

          patchState({
            followings: [ ...state.followings, ...followings ],
            loadingFollow: false,
            lazyLoading: false,
          });
        }
      }, error => {
        patchState({ loadingFollow: false, lazyLoading: false });
        console.error(GetFollowings.type, error);
      })
    );
  }

  @Action(GetFollowers)
  getFollowers({ patchState, getState }: StateContext<UserStateModel>, { page, limit, userId, search }: GetFollowers) {
    let state = getState();

    if (page === 1 && !state.followers.length && search === null) {
      patchState({ loadingFollow: true });
    }

    return this.userService.getFollowers(page, limit, userId, search).pipe(
      tap(followersData => {
        const followers = followersData.map(({ account }) => ({
          ...Utils.extendAccountWithBaseInfo(account),
        }));

        if (page === 1) {
          patchState({
            followers,
            loadingFollow: false
          });
        } else {
          state = getState();

          patchState({
            followers: [ ...state.followers, ...followers ],
            loadingFollow: false
          });
        }
      }, error => {
        patchState({ loadingFollow: false });
        console.error(GetFollowers.type, error);
      })
    );
  }

  @Action(BlockUser)
  block({ patchState, getState }: StateContext<UserStateModel>, { id }: BlockUser) {
    const state = getState();
    const loggedInAccount = { ...state.loggedInAccount };

    return this.userService.block(id).pipe(
      tap(blockedData => {
        const viewableAccount = state.viewableAccount && { ...state.viewableAccount };
        const followedAccountIds = { ...state.followedAccountIds, [id]: false };
        const followings = state.followings.filter(item => item.id !== id);
        let followers = [ ...state.followers ];

        if (viewableAccount && viewableAccount.id === id) {
          viewableAccount.isBlocked = true;
          if (viewableAccount.following) {
            viewableAccount.followersCount--;
          }
          viewableAccount.following = false;

          followers = followers.filter(item => item.id !== loggedInAccount.id);
        }

        const blocked = state.blocked.map(item => {
          if (item.id !== id) {
            return item;
          }
          return { ...item, isBlocked: true };
        });

        const index = followers.findIndex(item => item.id === id);

        if (index !== -1) {
          followers[index] = {
            ...followers[index],
            following: false,
          };
        }

        patchState({
          viewableAccount,
          loggedInAccount,
          blocked,
          followedAccountIds,
          followings,
          followers,
        });
      }, error => {
        this.snackbar.open(error.error.error, '', { duration: 3000 });
      })
    );
  }

  @Action(UnblockUser)
  unblock({ patchState, getState }: StateContext<UserStateModel>, { id }: UnblockUser) {
    return this.userService.unblock(id).pipe(
      tap(blockedData => {
        const state = getState();
        const viewableAccount = state.viewableAccount && { ...state.viewableAccount };
        if (viewableAccount && viewableAccount.id === id) {
          viewableAccount.isBlocked = false;
        }
        const blocked = state.blocked.map(item => {
          if (item.id !== id) {
            return item;
          }
          return { ...item, isBlocked: false };
        });
        patchState({
          viewableAccount,
          blocked,
        });
      }, error => {
        this.snackbar.open(error.error.error, '', { duration: 3000 });
      })
    );
  }

  @Action(BlockedList)
  blockedList({ patchState }: StateContext<UserStateModel>) {
    patchState({ loadingBlocked: true });

    return this.userService.blockedList().pipe(
      tap(blockedData => {
        if (blockedData.blocked) {
          const blocked = blockedData.blocked.map((blockedAccount: AccountModel) => {
            const user = Utils.extendAccountWithBaseInfo(blockedAccount);
            user.isBlocked = true;
            return user;
          });
          patchState({
            blocked,
            loadingBlocked: false,
          });
        }
      }, () => {
        patchState({
          loadingBlocked: false,
        });
      })
    );
  }

  @Action(GetFollowRequests)
  getFollowRequests({ getState, patchState }: StateContext<UserStateModel>, { offset, limit }: GetFollowRequests) {
    const state = getState();

    // to prevent loading of already loaded items
    if (offset < state.followRequests.length) {
      return;
    }

    if (!state.followRequestsFetched) {
      patchState({ loadingFollow: true });
    }

    return this.userService.getFollowRequests(offset, limit).pipe(
      tap(followersData => {
        const items = followersData.incomingRequests.map(request => {
          const res = { ...request };
          res.account =  Utils.extendAccountWithBaseInfo(res.account);
          return res;
        });
        const followRequests = !offset ? items : [ ...state.followRequests, ...items ];
        patchState({
          followRequests,
          loadingFollow: false,
          followRequestsFetched: true,
        });
      }, () => {
        patchState({ loadingFollow: false });
      })
    );
  }

  @Action(AddFollowRequest)
  addFollowRequest({ patchState, getState }: StateContext<UserStateModel>, { payload }: AddFollowRequest) {
    const state = getState();
    const account = Utils.extendAccountWithBaseInfo(payload.account);
    const followRequests = [ ...state.followRequests, {
      ...payload,
      account,
    }];
    const loggedInAccount = { ...state.loggedInAccount };
    loggedInAccount.followingRequestCount++;
    patchState({ loggedInAccount, followRequests });
  }

  @Action(ApproveFollowingRequest)
  approveFollowingRequest({ patchState, getState }: StateContext<UserStateModel>, { followId }: ApproveFollowingRequest) {
    return this.userService.approveFollowingRequest(followId).pipe(
      tap(() => {
        const state = getState();
        const loggedInAccount = { ...state.loggedInAccount };
        loggedInAccount.followingRequestCount--;
        const followRequests = state.followRequests.filter(request => request.id !== followId);
        patchState({ loggedInAccount, followRequests });
      })
    );
  }

  @Action(RejectFollowingRequest)
  rejectFollowingRequest({ patchState, getState }: StateContext<UserStateModel>, { followId }: RejectFollowingRequest) {
    return this.userService.rejectFollowingRequest(followId).pipe(
      tap(() => {
        const state = getState();
        const loggedInAccount = { ...state.loggedInAccount };
        loggedInAccount.followingRequestCount--;
        const followRequests = state.followRequests.filter(request => request.id !== followId);
        patchState({ loggedInAccount, followRequests });
      })
    );
  }

  @Action(ChangeUserFollowingRequest)
  changeUserFollowRequest(
    { patchState, getState }: StateContext<UserStateModel>,
    { userId, status, followId }: ChangeUserFollowingRequest
  ) {
    const state = getState();
    if (status === 'approved') {
      const viewableAccount = state.viewableAccount && { ...state.viewableAccount };
      if (viewableAccount && viewableAccount.id === userId) {
        viewableAccount.followingRequest = false;
        viewableAccount.following = true;
        viewableAccount.followersCount++;
      }
      patchState({ viewableAccount });
    } else if (status === 'rejected') {
      const viewableAccount = state.viewableAccount && { ...state.viewableAccount };
      if (viewableAccount && viewableAccount.id === userId) {
        viewableAccount.followingRequest = false;
        viewableAccount.following = false;
      }
      patchState({ viewableAccount });
    } else if (status === 'canceled') {
      const loggedInAccount = { ...state.loggedInAccount };
      const followRequests = state.followRequests.filter(request => request.id !== followId);
      loggedInAccount.followingRequestCount--;
      patchState({ loggedInAccount, followRequests });
    }
  }

  @Action(ToggleNotificationSetting)
  toggleNotificationSetting(
    { patchState, getState }: StateContext<UserStateModel>,
    { key }: ToggleNotificationSetting
  ) {
    const state = getState();
    const loggedInAccount = { ...state.loggedInAccount };

    loggedInAccount[key] = !loggedInAccount[key];

    patchState({ loggedInAccount });

    return this.notificationService.setConfig({ [key]: loggedInAccount[key] }).pipe(
      tap(() => {}, error => {
        console.error(ToggleNotificationSetting.type, error);
      })
    );
  }

  @Action(ValidateKycDocument)
  validateKycDocument({ patchState, getState }: StateContext<UserStateModel>, { file }: ValidateKycDocument) {
    return this.userService.validateKycDocument(file).pipe(
      tap(data => {
        const state = getState();
        const loggedInAccount = { ...state.loggedInAccount };

        if (loggedInAccount.business) {
          const business = { ...loggedInAccount.business };
          business.kycDocumentId = data.Id;
          business.verificationStatus = data.Status;
          loggedInAccount.business = business;
        } else {
          const person = { ...loggedInAccount.person };
          person.kycDocumentId = data.Id;
          person.verificationStatus = data.Status;
          loggedInAccount.person = person;
        }

        patchState({ loggedInAccount });
      }),
    );
  }

  @Action(SetLocalLanguage)
  setLocalLanguage({ patchState }: StateContext<UserStateModel>, { language }: SetLocalLanguage) {
    this.translateService.use(language);
    patchState({ language });
  }
}
