import { State, Action, StateContext, Selector, createSelector, Store, StateOperator, Actions, ofActionSuccessful } from '@ngxs/store';
import { patch } from '@ngxs/store/operators';
import { tap } from 'rxjs/operators';
import { DiscoverActions, DiscoverEntityType } from '../actions/discover.actions';
import { DiscoverService } from 'src/app/shared/services';
import { constants } from 'src/app/core/constants/constants';
import { PhotoModel, VideoModel, ArticleModel, ProductModel, ServiceModel } from 'src/app/shared/models';
import { Utils } from 'src/app/shared/utils/utils';
import { SearchService } from 'src/app/shared/services/search.service';
import { Injectable } from '@angular/core';
import { ResetViewableUserStore } from '../actions/user.actions';
import { PRODUCT_CATEGORIES } from 'src/app/modules/page/products/shared';
import { SERVICE_CATEGORIES } from 'src/app/modules/page/services/shared';

export type DiscoverSubStateItemModel = PhotoModel | VideoModel | ArticleModel | ProductModel | ServiceModel;

export interface DiscoverLocationAutosuggestion {
  address: string;
  lat: number;
  lng: number;
}

export interface DiscoverSubStateModel {
  items: PhotoModel[] | VideoModel[] | ArticleModel[] | ProductModel[] | ServiceModel[];
  total: number;
  fetched: boolean;
}

export interface DiscoverStateModel {
  currentEntityType: DiscoverEntityType;
  currentEntityCategory: string;
  locationText: string;
  locationAutosuggestions: DiscoverLocationAutosuggestion[];
  loading: boolean;
  lazyLoading: boolean;

  entities: {
    [key in DiscoverEntityType]: {
      [categoryName: string]: DiscoverSubStateModel
    }
  };
}

export const getDefaultDuscoverSubState = (categories: string[]) => {
  const result = {};

  for (const category of categories) {
    result[category] = {
      items: [],
      total: 0,
      fetched: false,
    };
  }

  return result;
};

export const patchDiscoverSubModel = (
  type: DiscoverEntityType,
  category: string,
  data: Partial<DiscoverSubStateModel>
): StateOperator<DiscoverStateModel> => {
  return patch<DiscoverStateModel>({
    entities: patch({
      [type]: patch({
        [category]: patch(data)
      })
    }),
  });
};

const getDefaultState = (): DiscoverStateModel => ({
  currentEntityType: 'photo',
  currentEntityCategory: constants.DEFAULT_MEDIA_CATEGORY,
  locationText: '',
  locationAutosuggestions: [],
  loading: false,
  lazyLoading: false,
  entities: {
    photo: getDefaultDuscoverSubState([ constants.DEFAULT_MEDIA_CATEGORY, ...constants.MEDIA_CATEGORIES ]),
    video: getDefaultDuscoverSubState([ constants.DEFAULT_MEDIA_CATEGORY, ...constants.MEDIA_CATEGORIES ]),
    article: getDefaultDuscoverSubState([ constants.DEFAULT_MEDIA_CATEGORY, ...constants.MEDIA_CATEGORIES ]),
    product: getDefaultDuscoverSubState([ constants.DEFAULT_MEDIA_CATEGORY, ...PRODUCT_CATEGORIES ]),
    service: getDefaultDuscoverSubState([ constants.DEFAULT_MEDIA_CATEGORY, ...SERVICE_CATEGORIES]),
  }
});

@State<DiscoverStateModel>({
  name: 'discover',
  defaults: getDefaultState()
})

@Injectable()
export class DiscoverState {

  static items(key: DiscoverEntityType) {
    return createSelector([DiscoverState], (state: DiscoverStateModel) => {
      const entity = state.entities[key];
      const category = entity && entity[state.currentEntityCategory];
      return category && category.items || [];
    });
  }

  static total(key: DiscoverEntityType) {
    return createSelector([DiscoverState], (state: DiscoverStateModel) => {
      const entity = state.entities[key];
      const category = entity[state.currentEntityCategory];
      return category && category.total || 0;
    });
  }

  @Selector()
  static loading(state: DiscoverStateModel) {
    return state.loading;
  }

  @Selector()
  static currentEntityType(state: DiscoverStateModel) {
    return state.currentEntityType;
  }

  @Selector()
  static currentEntityCategory(state: DiscoverStateModel) {
    return state.currentEntityCategory;
  }

  @Selector()
  static locationAutosuggestions(state: DiscoverStateModel) {
    return state.locationAutosuggestions;
  }

  @Selector()
  static lazyLoading(state: DiscoverStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private discoverService: DiscoverService,
    private searchService: SearchService,
    private store: Store,
    private actions$: Actions,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(ResetViewableUserStore)
    ).subscribe(() => {
      this.store.dispatch(new DiscoverActions.Reset());
    });
  }

  @Action(DiscoverActions.Load)
  load(
    { getState, setState, patchState }: StateContext<DiscoverStateModel>,
    { entityType, categoryName, page, limit, lazy, lat, lng, radius, address }: DiscoverActions.Load
  ) {
    const state = getState();

    const category = state.entities[entityType][categoryName] || {
      items: [],
      total: 0,
      fetched: false,
    };

    if (!category.fetched) {
      patchState({ loading: true });
    }

    if (address && lat && lng) {
      patchState({
        locationAutosuggestions: [
          { address, lat, lng }
        ]
      });
    }

    if(lazy) {
      patchState({
        lazyLoading: lazy,
      })
    }

    patchState({
      currentEntityType: entityType,
      currentEntityCategory: categoryName
    });

    return this.discoverService.load(entityType, categoryName, page, limit, lat, lng, radius).pipe(
      tap(({ items, total }) => {
        items = items.map(item => this.extendItem(item, entityType));

        if (page !== 1) {
          items = [ ...category.items, ...items ];
        }

        const newState: Partial<DiscoverSubStateModel> = {
          items,
          total,
          fetched: true,
        };

        setState(
          patchDiscoverSubModel(entityType, categoryName, newState)
        );
        patchState({ loading: false, lazyLoading: false, });
      }, error => {
        console.error(DiscoverActions.Load.type, error);
        patchState({ loading: false, lazyLoading: false, });
      })
    );
  }

  @Action(DiscoverActions.GetLocationAutosuggestions, { cancelUncompleted: true })
  getLocationAutosuggestions(
    { getState, patchState }: StateContext<DiscoverStateModel>,
    { text }: DiscoverActions.GetLocationAutosuggestions
  ) {
    if (!text) {
      return patchState({
        locationText: '',
        locationAutosuggestions: []
      });
    }

    return this.searchService.getPlacesAutocomplete(text).pipe(
      tap((result) => {
        const state = getState();

        if (!result.tomTom || !result.tomTom.results) {
          console.warn('Error while loading location autosuggestions', result);
          return patchState({
            locationAutosuggestions: [],
            locationText: text,
          });
        }

        const locationAutosuggestions = result.tomTom.results.map(item => ({
          address: item.address.freeformAddress,
          lat: item.position.lat,
          lng: item.position.lon,
        }));

        patchState({
          locationAutosuggestions,
          locationText: text,
        });
      }, error => {
        console.error(DiscoverActions.GetLocationAutosuggestions.type, error);
      })
    );
  }

  @Action(DiscoverActions.Reset)
  reset({ patchState }: StateContext<DiscoverStateModel>) {
    patchState(getDefaultState());
  }

  private extendItem(item: DiscoverSubStateItemModel, type: DiscoverEntityType): DiscoverSubStateItemModel {
    let pathPart = `${type}s`;

    if (type === 'article') {
      pathPart = `blog`;
    } else if (type === 'product') {
      pathPart = `shop`;
    }

    item.user = Utils.extendAccountWithBaseInfo(item.user);
    item.directLink = Utils.getEntityDirectLink(item, pathPart);
    item.tableName = type;

    if (type === 'product') {
      const product = item as ProductModel;

      if (!product.cover) {
        product.cover = product.photos[0];
      }

      return product;
    }

    if (type === 'service') {
      const service = item as ServiceModel;

      if (!service.cover) {
        service.photoCover = service.photos[0];
      }

      if (service.schedule && service.schedule.duration) {
        const found = constants.DURATIONS.find(duration => duration.value === service.schedule.duration);
        service.durationInfo = found && found.name;
      }

      return service;
    }

    return item;
  }

}
