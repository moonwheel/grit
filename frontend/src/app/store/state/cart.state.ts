import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { CartProductModel } from 'src/app/shared/models';
import { CartService } from 'src/app/shared/services/cart.service';

import { InitCart, LoadCart, AddToCart, EditInCart, DeleteFromCart, CheckoutCart, SetCartTotal, ResetCart } from '../actions/cart.actions';
import { NgZone, Injectable } from '@angular/core';

export interface CartStateModel {
  cart: CartProductModel[];
  total: number;
  productsPrice: number;
  shippingPrice: number;
  totalPrice: number;
  loading: boolean;
  fetched: boolean;
  lazyLoading: boolean;
}

@State<CartStateModel>({
  name: 'cart',
  defaults: {
    cart: [],
    total: 0,
    productsPrice: 0,
    shippingPrice: 0,
    totalPrice: 0,
    loading: false,
    fetched: false,
    lazyLoading: false,
  },
})

@Injectable()
export class CartState {

  @Selector()
  static cart(state: CartStateModel) {
    return state.cart;
  }

  @Selector()
  static loading(state: CartStateModel) {
    return state.loading;
  }

  @Selector()
  static total(state: CartStateModel) {
    return state.total;
  }

  @Selector()
  static totalPrice(state: CartStateModel) {
    return state.totalPrice;
  }

  @Selector()
  static productsPrice(state: CartStateModel) {
    return state.productsPrice;
  }

  @Selector()
  static shippingPrice(state: CartStateModel) {
    return state.shippingPrice;
  }

  @Selector()
  static lazyLoading(state: CartStateModel) {
    return state.lazyLoading;
  }

  constructor(
    private cartService: CartService,
    private router: Router,
    private snackBar: MatSnackBar,
    private zone: NgZone,
  ) { }

  @Action(InitCart)
  init({ patchState, getState, dispatch }: StateContext<CartStateModel>, { limit }: InitCart) {
    const state = getState();

    if (state.loading) {
      return;
    }

    if (!state.fetched) {
      patchState({ loading: true });
    }

    return dispatch(new LoadCart(1, limit));
  }

  @Action(LoadCart)
  load({ patchState }: StateContext<CartStateModel>, { page, limit, lazy }: LoadCart) {
    return this.cartService.list(page, limit).pipe(
      tap(cart => {
        patchState({
          cart: cart.products,
          productsPrice: cart.productsPrice,
          shippingPrice: cart.shippingPrice,
          totalPrice: cart.totalPrice,
          loading: false,
          fetched: true,
          lazyLoading: lazy
        });
      }),
      catchError(error => {
        patchState({ loading: false });
        return throwError(error);
      }),
    );
  }

  @Action(ResetCart)
  reset({ patchState }: StateContext<CartStateModel>) {
    patchState({
      cart: [],
      total: 0,
      productsPrice: 0,
      shippingPrice: 0,
      totalPrice: 0,
    });
  }

  @Action(AddToCart)
  add({ getState, setState }: StateContext<CartStateModel>, { payload }: AddToCart) {
    return this.cartService.add(payload).pipe(
      tap(() => {
        const state = getState();
        setState({
          ...state,
          cart: [...state.cart, payload],
          total: state.total + 1,
        });
      }, error => {
        console.error('Cart Add Action: ', error);
      })
    );
  }

  @Action(EditInCart)
  edit({ patchState }: StateContext<CartStateModel>, { payload }: EditInCart) {
    return this.cartService.edit(payload).pipe(
      tap(data => {
        patchState({
          productsPrice: data.productsPrice,
          shippingPrice: data.shippingPrice,
          cart: data.products,
          total: data.count,
          totalPrice: data.totalPrice,
        });
      }, error => {
        console.error('Cart Edit Action: ', error);
      })
    );
  }

  @Action(DeleteFromCart)
  delete({ getState, patchState }: StateContext<CartStateModel>, { id }: DeleteFromCart) {
    return this.cartService.delete(id).pipe(
      tap(data => {
        const state = getState();
        patchState({
          productsPrice: data.productsPrice,
          shippingPrice: data.shippingPrice,
          cart: state.cart.filter(item => item.id !== id),
          total: state.total > 1 ? state.total - 1 : 0,
          totalPrice: data.totalPrice,
        });
      }, error => {
        console.error('Cart Delete Action: ', error);
      }),
    );
  }

  @Action(CheckoutCart)
  checkout({ getState, patchState }: StateContext<CartStateModel>, { payload }: CheckoutCart) {
    return this.cartService.checkout(payload).pipe(
      tap(() => {
        patchState({ cart: [] });
      }, error => {
        console.error('Cart Delete Action: ', error);
      }),
    );
  }

  @Action(SetCartTotal)
  setTotal({ patchState }: StateContext<CartStateModel>, { total }: SetCartTotal) {
    patchState({ total });
  }
}
