import { Action, Actions, ofActionSuccessful, Selector, State, StateContext, Store } from '@ngxs/store';
import {
  ChangeAccount,
  ChangeForgottenPassword,
  ChangePassword,
  Login,
  Logout,
  Register,
  SendEmailVerification,
  UpdateTokens,
  VerifyPasswordWithAuth,
  VerifyPasswordWithoutAuth,
  SetAuthLoading,
} from '../actions/auth.actions';
import { tap, switchMap, catchError, switchMapTo, filter } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgZone, Injectable } from '@angular/core';
import { throwError, of, interval } from 'rxjs';
import { InitUser, ResetLoggedInUserStore } from '../actions/user.actions';
import { GetBusiness, InitBusiness } from '../actions/business.actions';
import { TranslateService } from '@ngx-translate/core';
import { ResetOrders } from '../actions/order.actions';
import { constants } from 'src/app/core/constants/constants';
import { SetGlobalLoading } from '../actions/visible.actions';

export const TOKEN_STORAGE_KEY = 'auth.token';
export const REFRESH_TOKEN_STORAGE_KEY = 'auth.refreshToken';
export const PROFILE_ID_STORAGE_KEY = 'profile_id';

export class AuthStateModel {
  token?: string;
  user?: string;
  refreshToken?: string;
  loading?: boolean;
}

@State<AuthStateModel>( {
  name: 'auth'
} )

@Injectable()
export class AuthState {

  @Selector()
  static token( state: AuthStateModel ) {
    return state.token;
  }

  @Selector()
  static refreshToken( state: AuthStateModel ) {
    return state.refreshToken;
  }

  @Selector()
  static loading( state: AuthStateModel ) {
    return state.loading;
  }

  constructor(
    private userService: UserService,
    private router: Router,
    private snackBar: MatSnackBar,
    private zone: NgZone,
    private store: Store,
    private translateService: TranslateService,
    private actions$: Actions,
  ) {
    this.actions$.pipe(
      ofActionSuccessful(InitUser),
      switchMap(() => interval(constants.REFRESH_JWT_TOKEN_INTERVAL)),
      switchMapTo(this.store.selectOnce(AuthState.refreshToken)),
      filter(refreshToken => !!refreshToken),
      switchMap(refreshToken => this.userService.refreshJwtToken(refreshToken))
    ).subscribe(tokens => {
      this.store.dispatch(new UpdateTokens(tokens));
    });
  }

  @Action( Login )
  login( {patchState, dispatch}: StateContext<AuthStateModel>, {payload} ) {
    if (payload.user) {
        payload.user = {
          ...payload.user,
          email: payload.user.email && payload.user.email.toLowerCase()
        };
    } else {
      payload = {
        ...payload,
        email: payload.email && payload.email.toLowerCase()
      };
    }

    return this.userService.login( payload ).pipe(
      tap(({token, user, refreshToken}) => {
        patchState({token, user, refreshToken});
        localStorage.setItem(REFRESH_TOKEN_STORAGE_KEY, JSON.stringify(refreshToken));
      }, error => {
        console.log( error );
      }),
      switchMap(({ reactivationtoken }) => {
        if (reactivationtoken) {
          return this.userService.reactivate(reactivationtoken);
        } else {
          return of(null);
        }
      }),
      switchMap(() => dispatch([new InitUser(), new InitBusiness()]))
    );
  }

  @Action( Register )
  register( {patchState}: StateContext<AuthStateModel>, {payload} ) {
    if (payload.user) {
        payload.user = {
          ...payload.user,
          email: payload.user.email && payload.user.email.toLowerCase()
        };
    } else {
      payload = {
        ...payload,
        email: payload.email && payload.email.toLowerCase()
      };
    }
    return this.userService.register( payload ).pipe( tap( ( data ) => {
        // this.zone.run(() => {
        //     this.router.navigate(['/']);
        // })
      },
      error => {
        if ( error.error.error === 'User with this email already exists in database' ) {
          return;
        }
        this.snackBar.open( error.error.error, '', {
          duration: 3000,
        } );
      }
    ) );
  }

  @Action( ChangePassword )
  changePassword( {patchState}: StateContext<AuthStateModel>, {payload} ) {
    return this.userService.changePassword( payload ).pipe( tap( ( data ) => {
        this.zone.run( () => {
          if ( data.type && data.type === 'forgot' ) {
            this.router.navigate( ['/'] );
          }
          this.translateService.get('INFO_MESSAGES.PASSWORD_CHANGED').subscribe(message => {
            this.snackBar.open(message);
          });
        } );
      },
      error => {
        this.snackBar.open( error.error.error, '', {
          duration: 3000,
        } );
      }
    ) );
  }

  @Action( ChangeForgottenPassword )
  changeForgottenPassword( {patchState}: StateContext<AuthStateModel>, {payload} ) {
    return this.userService.changeForgottenPassword( payload ).pipe( tap( ( data ) => {
        this.zone.run( () => {
          this.router.navigate( ['/feed'] );
          this.translateService.get('INFO_MESSAGES.PASSWORD_CHANGED').subscribe(message => {
            this.snackBar.open(message);
          });
        } );
      },
      error => {
        this.snackBar.open( error.error.error, '', {
          duration: 3000,
        } );
      }
    ) );
  }

  @Action( VerifyPasswordWithAuth )
  verifyPasswordWithAuth( {patchState}: StateContext<AuthStateModel>, {payload} ) {
    return this.userService.verifyPasswordWithAuth().pipe( tap( ( data ) => {
        this.zone.run( () => {
          this.translateService.get('INFO_MESSAGES.RESET_PASSWORD_LETTER_SENT').subscribe(message => {
            this.snackBar.open(message);
          });
        } );
      },
      error => {
        this.snackBar.open( error.error.error, '', {
          duration: 3000,
        } );
      }
    ) );
  }

  @Action( VerifyPasswordWithoutAuth )
  verifyPasswordWithoutAuth( {patchState}: StateContext<AuthStateModel>, {payload} ) {
    return this.userService.verifyPasswordWithoutAuth( payload ).pipe( tap( ( data ) => {
        this.zone.run( () => {
          this.translateService.get('INFO_MESSAGES.RESET_PASSWORD_LETTER_SENT').subscribe(message => {
            this.snackBar.open(message);
          });
        } );
      },
      error => {
        this.snackBar.open( error.error.error, '', {
          duration: 3000,
        } );
      }
    ) );
  }

  @Action( Logout )
  logout( {setState, dispatch}: StateContext<AuthStateModel> ) {
    setState( {} );

    localStorage.removeItem(TOKEN_STORAGE_KEY);
    localStorage.removeItem(REFRESH_TOKEN_STORAGE_KEY);

    return dispatch(new ResetLoggedInUserStore()).pipe(
      tap(() => {
        this.router.navigate(['/']);
      })
    );
  }

  @Action( UpdateTokens )
  updateTokens( {getState, setState}: StateContext<AuthStateModel>, { payload: { token, refreshToken } } ) {
    const state = getState();

    localStorage.setItem( TOKEN_STORAGE_KEY, JSON.stringify( token ) );

    if (refreshToken) {
      localStorage.setItem( REFRESH_TOKEN_STORAGE_KEY, JSON.stringify( refreshToken ) );
    }

    setState( {
      ...state,
      token,
      refreshToken: refreshToken ? refreshToken : state.refreshToken,
    } );
  }

  @Action( ChangeAccount )
  switch( {patchState}: StateContext<AuthStateModel>, { account }: ChangeAccount ) {
    this.store.dispatch(new SetGlobalLoading(true));
    this.store.dispatch(new ResetLoggedInUserStore());
    this.store.dispatch(new ResetOrders());

    const id = typeof account === 'number' ? account : account.id;

    return this.userService.changeAccount( id ).pipe(
      switchMap(data => {
        const { token, refreshToken } = data;

        localStorage.setItem(PROFILE_ID_STORAGE_KEY, String(id) );
        localStorage.setItem(TOKEN_STORAGE_KEY, JSON.stringify(token));
        localStorage.setItem(REFRESH_TOKEN_STORAGE_KEY, JSON.stringify(refreshToken));

        patchState({ token, refreshToken });

        const actions = [ new InitUser() ];

        if (typeof account === 'object' && account.business) {
          actions.push(new GetBusiness(account.business.id));
        }

        return this.store.dispatch(actions);
      }),
      tap(() => {
        this.store.dispatch(new SetGlobalLoading(false));
      }),
      catchError(error => {
        console.error(ChangeAccount.type, error );
        this.store.dispatch(new SetGlobalLoading(false));
        return throwError(error);
      })
    );
  }

  @Action( SendEmailVerification )
  sendEmailVerification( {patchState, getState}: StateContext<AuthStateModel> ) {
    return this.userService.sendVerification().pipe( tap( ( res ) => {
    } ) );
  }

  @Action( SetAuthLoading )
  setAuthLoading( {patchState}: StateContext<AuthStateModel>, { loading }) {
    patchState({ loading });
  }
}
