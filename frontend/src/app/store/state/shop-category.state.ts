import { State, Action, StateContext, Selector, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ShopCategoryModel } from 'src/app/shared/models/shop-category.model';
import { ShopCategoryService } from 'src/app/shared/services/shop-category.service';
import {
    InitShopCategory,
    AddShopCategory,
    EditShopCategory,
    DeleteShopCategory,
    LoadShopCategories,
    ResetShopCategories
} from '../actions/shop-category.actions';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { ResetViewableUserStore } from '../actions/user.actions';

export class ShopCategoryStateModel {
    categories: ShopCategoryModel[];
    loading: boolean;
}

@State<ShopCategoryStateModel>({
    name: 'shopCategory',
    defaults: {
        categories: [],
        loading: false,
    }
})

@Injectable()
export class ShopCategoryState {

    @Selector()
    static categories(state: ShopCategoryStateModel) { return state.categories; }

    constructor(
        private addressService: ShopCategoryService,
        private store: Store,
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
        private actions$: Actions,
    ) {
        this.actions$.pipe(
            ofActionSuccessful(ResetViewableUserStore)
        ).subscribe(() => {
            this.store.dispatch(new ResetShopCategories());
        });
    }

    @Action(InitShopCategory)
    init({ patchState, getState, dispatch }: StateContext<ShopCategoryStateModel>) {
        const state = getState();

        if (state.loading) {
        return;
        }

        patchState({ loading: true });
        return dispatch(new LoadShopCategories());
    }

    @Action(LoadShopCategories)
    load({ patchState }: StateContext<ShopCategoryStateModel>) {
        return this.addressService.getAll().pipe(
            tap(categories => patchState({ categories, loading: false })),
            catchError(error => {
                console.error(LoadShopCategories.type, error);
                patchState({ loading: false });
                return throwError(error);
            })
        );
    }

    @Action(ResetShopCategories)
    reset(ctx: StateContext<ShopCategoryStateModel>) {
        ctx.patchState({
            categories: [],
        });
    }

    @Action(AddShopCategory)
    addShopCategory({ getState, setState }: StateContext<ShopCategoryStateModel>, { payload }) {
        return this.addressService.add(payload).pipe(tap((data) => {
            const state = getState();

            setState({
                ...state,
                categories: [...state.categories, data]
            });
        },
            error => {
                console.error('ShopCategory Add Action', error);
            }
        ));
    }

    @Action(EditShopCategory)
    editShopCategory({ getState, setState }: StateContext<ShopCategoryStateModel>, { payload, index }) {
        return this.addressService.edit(payload).pipe(tap((data) => {
            const state = getState();
            const categories = [ ...state.categories ];
            categories[index] = { ...categories[index], ...data };
            setState({
                ...state,
                categories
            });
        },
            error => {
                console.error('ShopCategory Edit Action', error);
            }
        ));
    }

    @Action(DeleteShopCategory)
    deleteShopCategory({ getState, patchState }: StateContext<ShopCategoryStateModel>, { id }) {
        const state = getState();
        patchState({
            categories: state.categories.filter(address => address.id !== id)
        });
        return this.addressService.delete(id).pipe(
            catchError(error => {
                console.error('ShopCategory Delete Action', error);

                if (error instanceof HttpErrorResponse) {
                    const errorText: string = error.error && error.error.error;

                    if (typeof errorText === 'string' && errorText.includes('foreign key violation')) {
                        this.translateService.get('ERRORS.CANT_DELETE_CATEGORY').subscribe(message => {
                            this.snackBar.open(message, null, { duration: 5000 });
                        });
                    }
                }
                return throwError(error);
            })
        );
    }
}
