import { State, Action, StateContext, Selector } from '@ngxs/store';
import { ToggleVisible, SetGlobalLoading } from '../actions/visible.actions';
import { Injectable } from '@angular/core';

export class VisibleStateModel {
  isVisibleMain ?= true;
  isVisibleSearch ?= true;
  isVisibleUserTabs ?= true;
  isVisibleAddButton ?= true;
  isPullToRefreshEnabled ?= true;
  isKeyboardShown ?= false;
  globalLoading ?= false;
}

@State<VisibleStateModel>({
  name: 'visible',
  defaults: {
    isVisibleMain: true,
    isVisibleSearch: true,
    isVisibleUserTabs: true,
    isVisibleAddButton: true,
    isPullToRefreshEnabled: true,
    isKeyboardShown: false,
    globalLoading: false,
  }
})

@Injectable()
export class VisibleState {

  @Selector()
  static visible(state: VisibleStateModel) {
    return state;
  }

  @Selector()
  static isPullToRefreshEnabled(state: VisibleStateModel) {
    return state.isPullToRefreshEnabled;
  }

  @Selector()
  static isVisibleAddButton(state: VisibleStateModel) {
    return state.isVisibleAddButton && !state.isKeyboardShown;
  }

  @Selector()
  static globalLoading(state: VisibleStateModel) {
    return state.globalLoading;
  }

  constructor() { }

  @Action(ToggleVisible)
  toggleVisible({ getState, setState }: StateContext<VisibleStateModel>, { payload }) {
    const visibleSettings = Object.entries(getState()).reduce((accum, item) => {
      const [stateKey, stateValue] = item;
      accum[stateKey] = (payload && payload[stateKey] !== undefined) ? payload[stateKey] : stateValue;
      return accum;
    }, {});
    return setState(visibleSettings);
  }

  @Action(SetGlobalLoading)
  etGlobalLoading({ patchState }: StateContext<VisibleStateModel>, { globalLoading }: SetGlobalLoading) {
    patchState({ globalLoading });
  }
}
