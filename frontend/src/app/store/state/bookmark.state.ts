import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { tap, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { BookmarkService } from '../../shared/services/bookmark.service';
import { AddBookmark, LoadBookmark, DeleteBookmark, InitBookmark } from '../actions/bookmark.actions';
import { ProductActions } from '../actions/product.actions';
import { ServiceActions } from '../actions/service.actions';
import { PhotoActions } from '../actions/photo.actions';
import { VideoActions } from '../actions/video.actions';
import { TextActions } from '../actions/text.actions';
import { ArticleActions } from '../actions/article.actions';
import { UserState } from './user.state';
import { Utils } from 'src/app/shared/utils/utils';
import { Injectable } from '@angular/core';

export class BookmarkStateModel {
  items: any[];
  loading: boolean;
  fetched: boolean;
  lazyLoading: boolean;
  searching: boolean;
}

@State<BookmarkStateModel>({
  name: 'bookmarks',
  defaults: {
    items: [],
    loading: false,
    fetched: false,
    lazyLoading: false,
    searching: false,
  },
})

@Injectable()
export class BookmarkState {

  @Selector()
  static bookmarks(state: BookmarkStateModel) {
    return state.items;
  }

  @Selector()
  static total(state: BookmarkStateModel) {
    return state.items.length;
  }

  @Selector()
  static loading(state: BookmarkStateModel) {
    return state.loading;
  }

  @Selector()
  static lazyLoading(state: BookmarkStateModel) {
    return state.lazyLoading;
  }

  @Selector()
  static searching(state: BookmarkStateModel) {
    return state.searching;
  }

  @Selector()
  static fetched(state: BookmarkStateModel) {
    return state.fetched;
  }

  constructor(
    private bookmarkService: BookmarkService,
    protected store: Store,
  ) { }

  @Action(InitBookmark)
  initBookmark({ patchState, getState, dispatch }: StateContext<BookmarkStateModel>, { limit }) {
    const state = getState();
    if (state.loading) {
      return;
    }

    patchState({ loading: true });
    
    return dispatch(new LoadBookmark(1, limit));
  }

  @Action(LoadBookmark)
  loadBookmark({ getState, patchState }: StateContext<BookmarkStateModel>, { page, limit, search, lazy }) {

    if(lazy) {
      patchState({lazyLoading: lazy})
    }

    patchState({
      searching: !!search
    })

    return this.store.selectOnce(UserState.viewableAccountId).pipe(
      switchMap(() => this.bookmarkService.getAll(page, limit, search)),
      tap(data => {
        const state = getState();

        if (page === 1) {
          patchState({
            items: data,
            loading: false,
            fetched: true,
            lazyLoading: false,
            searching: false,
          });
        } else {
          patchState({
            items: [ ...state.items, ...data ],
            loading: false,
            fetched: true,
            lazyLoading: false,
            searching: false,
          });
        }
      }, () => {
        patchState({ loading: false, lazyLoading: false });
      }),
    );
  }

  @Action(AddBookmark)
  addBookmark({ dispatch }: StateContext<BookmarkStateModel>, { payload }) {
    switch(payload.type) {
      case 'product':
        dispatch(new ProductActions.AddToBookmark(payload.id, payload.type));
        break;
      case 'service':
        dispatch(new ServiceActions.AddToBookmark(payload.id, payload.type));
        break;
      case 'photo':
        dispatch(new PhotoActions.AddToBookmark(payload.id, payload.type));
        break;
      case 'video':
        dispatch(new VideoActions.AddToBookmark(payload.id, payload.type));
        break;
      case 'text':
        dispatch(new TextActions.AddToBookmark(payload.id, payload.type));
        break;
      case 'article':
        dispatch(new ArticleActions.AddToBookmark(payload.id, payload.type));
        break;
    }
    return this.bookmarkService.addBookmark(payload).pipe(tap(() => {
      console.log('Bookmark Added');
    }, error => {
        console.error('Bookmark Add Action', error);
      }
    ));
  }

  @Action(DeleteBookmark)
  deleteBookmark({ getState, patchState, dispatch }: StateContext<BookmarkStateModel>, { payload }) {
    switch(payload.type) {
      case 'product':
        dispatch(new ProductActions.DeleteFromBookmark(payload.id, payload.type));
        break;
      case 'service':
        dispatch(new ServiceActions.DeleteFromBookmark(payload.id, payload.type));
        break;
      case 'photo':
        dispatch(new PhotoActions.DeleteFromBookmark(payload.id, payload.type));
        break;
      case 'video':
        dispatch(new VideoActions.DeleteFromBookmark(payload.id, payload.type));
        break;
      case 'text':
        dispatch(new TextActions.DeleteFromBookmark(payload.id, payload.type));
        break;
      case 'article':
        dispatch(new ArticleActions.DeleteFromBookmark(payload.id, payload.type));
        break;
    }
    return this.bookmarkService.deleteBookmark(payload).pipe(tap(() => {
        const state = getState();
        patchState({
          items: state.items.filter(item => item.id !== payload.bookmarkId),
        });
      },
      error => {
       console.error('Bookmark Delete Action', error);
      }
   ));
  }
}
