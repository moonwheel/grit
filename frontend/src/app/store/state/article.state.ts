import { State, Action, StateContext, Selector, createSelector, Store } from '@ngxs/store';
import { ArticleActions } from '../actions/article.actions';
import { ArticleService } from 'src/app/shared/services/article.service';
import { ArticleModel } from 'src/app/shared/models/article.model';
import { AbstractStateModel, AbstractState } from './abstract.state';
import { SseService } from 'src/app/shared/services/sse.service';
import { Injectable } from '@angular/core';

export interface ArticleStateModel extends AbstractStateModel<ArticleModel> {
    items: {
        [id: number]: ArticleModel
    };
}

@State<ArticleStateModel>({
    name: 'articles',
    defaults: {
        ...AbstractState.getDefaultState(),
    }
})

@Injectable()
export class ArticleState extends AbstractState {

    @Selector()
    static articles(state: ArticleStateModel) { return super.getAllSelector(state); }

    @Selector()
    static drafts(state: ArticleStateModel) { return super.getDraftsSelector(state); }

    @Selector()
    static total(state: ArticleStateModel) { return super.totalSelector(state); }

    @Selector()
    static loading(state: ArticleStateModel) { return super.loadingSelector(state); }

    @Selector()
    static uploading(state: ArticleStateModel) { return super.uploadingSelector(state); }

    @Selector()
    static lazyLoading (state: ArticleStateModel) {
      return state.fetching;
    }

    static article(id: number) {
        return super.getOneSelector<ArticleModel>(ArticleState, id);
    }

    static likes(id: number) {
        return super.likesSelector(ArticleState, id);
    }

    static comments(id: number) {
        return super.commentsSelector(ArticleState, id);
    }

    static comment(commentId: number) {
        return super.commentSelector(ArticleState, commentId);
    }

    static replies(commentId: number) {
        return super.repliesSelector(ArticleState, commentId);
    }

    constructor(
        protected service: ArticleService,
        protected store: Store,
        protected sseService?: SseService,
    ) {
        super(service, store);

        this.sseService.fileProcessingComplete$.subscribe(({ entityType, entity }) => {
            if (entityType === 'article') {
                this.store.dispatch(new ArticleActions.FinishProcessing(entity.id, entity));
            }
        });
    }

    @Action(ArticleActions.Init)
    init(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Init) {
        return super.init(ctx, action, ArticleActions);
    }

    @Action(ArticleActions.Load)
    load(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Load) {
        if(action.lazy) {
            ctx.patchState({fetching: true})
        }

        return super.load(ctx, action, ArticleActions.Load.type);
    }

    @Action(ArticleActions.LoadDrafts)
    loadDrafts(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LoadDrafts) {
        return super.loadDrafts(ctx, action, ArticleActions.LoadDrafts.type);
    }

    @Action(ArticleActions.GetOne)
    getOne(ctx: StateContext<ArticleStateModel>, action: ArticleActions.GetOne) {
        this.store.dispatch(new ArticleActions.View(action.id));
        return super.getOne(ctx, action, ArticleActions.GetOne.type);
    }

    @Action(ArticleActions.Reset)
    reset(ctx: StateContext<ArticleStateModel>) {
        return super.reset(ctx);
    }

    @Action(ArticleActions.Add)
    add(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Add) {
        return super.add(ctx, action, ArticleActions.Add.type, ArticleActions);
    }

    @Action(ArticleActions.Upload)
    upload(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Upload) {
        return super.uploadManyFiles(ctx, action, 'article', [], ArticleActions.Upload.type, ArticleActions);
    }

    @Action(ArticleActions.UpdateUploadingProgress)
    updateUploadingProgress(ctx: StateContext<ArticleStateModel>, action: ArticleActions.UpdateUploadingProgress) {
        return super.updateUploadingProgress(ctx, action);
    }

    @Action(ArticleActions.FinishProcessing)
    finishProcessing(ctx: StateContext<ArticleStateModel>, action: ArticleActions.FinishProcessing) {
        return super.finishProcessing(ctx, action);
    }

    @Action(ArticleActions.Edit)
    edit(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Edit) {
        return super.edit(ctx, action, ArticleActions.Edit.type, ArticleActions);
    }

    @Action(ArticleActions.Delete)
    delete(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Delete) {
        return super.delete(ctx, action, ArticleActions.Delete.type);
    }

    @Action(ArticleActions.Like)
    like(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Like) {
        return super.like(ctx, action, ArticleActions.Like.type);
    }

    @Action(ArticleActions.LoadLikes)
    loadLikes(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LoadLikes) {
        return super.loadLikes(ctx, action, ArticleActions.LoadLikes.type);
    }

    @Action(ArticleActions.Comment)
    comment(ctx: StateContext<ArticleStateModel>, action: ArticleActions.Comment) {
        return super.comment(ctx, action, ArticleActions.Comment.type);
    }

    @Action(ArticleActions.EditComment)
    editComment(ctx: StateContext<ArticleStateModel>, action: ArticleActions.EditComment) {
        return super.editComment(ctx, action, ArticleActions.EditComment.type);
    }

    @Action(ArticleActions.LoadComments)
    loadComments(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LoadComments) {
        return super.loadComments(ctx, action, ArticleActions.LoadComments.type);
    }

    @Action(ArticleActions.LoadCommentReplies)
    loadCommentReplies(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LoadCommentReplies) {
        return super.loadCommentReplies(ctx, action, ArticleActions.LoadCommentReplies.type);
    }

    @Action(ArticleActions.LikeComment)
    likeComment(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LikeComment) {
        return super.likeComment(ctx, action, ArticleActions.LikeComment.type);
    }

    @Action(ArticleActions.LoadCommentLikes)
    loadCommentLikes(ctx: StateContext<ArticleStateModel>, action: ArticleActions.LoadCommentLikes) {
        return super.loadCommentLikes(ctx, action, ArticleActions.LoadCommentLikes.type);
    }

    @Action(ArticleActions.DeleteComment)
    deleteComment(ctx: StateContext<ArticleStateModel>, action: ArticleActions.DeleteComment) {
        return super.deleteComment(ctx, action, ArticleActions.DeleteComment.type);
    }

    @Action(ArticleActions.InsertItems)
    insertItems(ctx: StateContext<ArticleStateModel>, action: ArticleActions.InsertItems) {
        return super.insertItems(ctx, action);
    }

    @Action(ArticleActions.View)
    view(ctx: StateContext<ArticleStateModel>, action: ArticleActions.View) {
        return super.view(ctx, action, ArticleActions.View.type);
    }

    @Action(ArticleActions.AddToBookmark)
    addToBookmark(ctx: StateContext<ArticleStateModel>, action: ArticleActions.AddToBookmark) {
      return super.addToBookmark(ctx, action, ArticleActions.AddToBookmark.type);
    }

    @Action(ArticleActions.DeleteFromBookmark)
    deleteFromBookmark(ctx: StateContext<ArticleStateModel>, action: ArticleActions.DeleteFromBookmark) {
      return super.deleteFromBookmark(ctx, action, ArticleActions.DeleteFromBookmark.type);
    }
}
