import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { tap, switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusinessService } from 'src/app/shared/services/business.service';
import {
  CreateNewBusiness,
  GetBusiness,
  GetBusinessCategories,
  InitBusiness,
  UpdateBusiness,
  RestoreBusiness, DeleteLocalBusiness
} from '../actions/business.actions';
import { BusinessModel } from '../../shared/models/business.model';
import { NgZone, Injectable } from '@angular/core';
import { constants } from '../../core/constants/constants';
import { BusinessCategoryModel } from '../../shared/models/business-category.model';
import { AccountModel } from 'src/app/shared/models';
import { InitUser, ChangeUserLanguage } from '../actions/user.actions';
import { AuthState } from './auth.state';
import { UserState } from './user.state';

export class BusinessCategoryStateModel {
  categories: BusinessCategoryModel[];
  businessList: AccountModel[];
  business: BusinessModel;
  newBusinessId: number;
  loading: boolean;
  fetched: boolean;
}

@State<BusinessCategoryStateModel>( {
  name: 'business',
  defaults: {
    categories: [],
    businessList: [],
    business: null,
    newBusinessId: null,
    loading: false,
    fetched: false,
  }
} )

@Injectable()
export class BusinessState {

  @Selector()
  static business( state: BusinessCategoryStateModel ) {
    return state.business;
  }

  @Selector()
  static loading( state: BusinessCategoryStateModel ) {
    return state.loading;
  }

  @Selector()
  static categories( state: BusinessCategoryStateModel ) {
    return state.categories;
  }

  @Selector()
  static description( state: any ) {
    return state.business.description;
  }

  @Selector()
  static bName( state: any ) {
    return state.business.name;
  }

  @Selector()
  static businessAddress( state: any ) {
    const address = state.business.address;
    return `${ address.street }, ${ address.zip } ${ address.city }`;
  }

  @Selector()
  static businessPhone( state: any ) {
    return state.business.phone;
  }

  @Selector()
  static legalNotice( state: any ) {
    return state.business.legal_notice;
  }

  @Selector()
  static businessList( state: BusinessCategoryStateModel ) {
    return state.businessList;
  }

  @Selector()
  static businessPage( state: any ) {
    return state.business.pageName;
  }

  @Selector()
  static businessPhoto( state: any ) {
    return state.business.photo ? state.business.photo : constants.PLACEHOLDER_AVATAR_PATH;
  }

  constructor(
    private service: BusinessService,
    private router: Router,
    private snackBar: MatSnackBar,
    private zone: NgZone,
    private store: Store,
  ) {
  }

  ngxsOnInit(ctx: StateContext<BusinessCategoryStateModel>) {
    const token = this.store.selectSnapshot(AuthState.token);

    if (token) {
      ctx.dispatch(new InitBusiness());
    }
  }

  @Action( InitBusiness )
  init( {getState, setState}: StateContext<BusinessCategoryStateModel> ) {
    return this.service.getAll().pipe( tap( ( data ) => {
        const state = getState();
        setState( {
          ...state,
          businessList: data
        } );
        return {businessList: data};
      },
      error => {
        console.error( 'BusinessCategory Init Action', error );
      }
    ) );
  }

  @Action( GetBusinessCategories )
  getBusinessCategories( {patchState}: StateContext<any> ) {
    return this.service.getBusinessCategories().pipe( tap( ( data ) => {
        patchState( {categories: data} );
      },
      error => {
        console.error( 'Business Category Get Action', error );
      }
    ) );
  }

  @Action( CreateNewBusiness )
  createNewBusiness( {patchState}: StateContext<BusinessCategoryStateModel>, {payload} ) {
    return this.service.createNewBusiness( payload ).pipe( tap( ( data ) => {
        if ( data.status === 'success' ) {
          const newBusinessId = data.account;
          patchState({ newBusinessId });
        }

        return data;
      }
    ) );
  }

  @Action( UpdateBusiness )
  updateBusiness( {getState, patchState}: StateContext<BusinessCategoryStateModel>, {payload, id} ) {
    patchState({ loading: true });
    return this.service.updateBusiness( payload, id ).pipe(
      switchMap( ( data ) => {
        const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);
        const currentLanguage = loggedInAccount.person.language;
        const actions = [
          new InitUser(),
          new GetBusiness(id),
        ];

        if (currentLanguage !== payload.language) {
          actions.push(new ChangeUserLanguage({ language: payload.language }));
        }

        return this.store.dispatch(actions);
      }),
      tap(() => {
        patchState({ loading: false });
      }, error => {
        patchState({ loading: false });
      }),
    );
  }


  @Action( GetBusiness )
  getBusiness( {patchState, getState}: StateContext<BusinessCategoryStateModel>, {id} ) {
    const state = getState();

    patchState({ loading: true });

    return this.service.getOne(id).pipe(
      tap(data => {
        patchState({
          business: data,
          loading: false,
          fetched: true,
        });
      }, error => {
        patchState({ loading: false });
        console.error( 'User Change Account Action ', error );
      })
    );
  }

  @Action(RestoreBusiness)
  restoreBusiness({patchState, getState}: StateContext<BusinessCategoryStateModel>, { reactivationToken }: RestoreBusiness) {
    return this.service.restoreBusiness(reactivationToken).pipe(
      tap(data => {
        const state = getState();
        const businessList = [ ...state.businessList ];
        const foundIndex = businessList.findIndex(item => item.business && item.business.reactivationtoken === reactivationToken);

        if (foundIndex !== -1) {
          businessList[foundIndex] = {
            ...businessList[foundIndex],
            deleted: null,
            business: {
              ...businessList[foundIndex].business,
              deleted: null
            },
          };

          patchState({ businessList });
        }
      }, error => {
        console.error(RestoreBusiness.type, error);
      })
    );
  }

  @Action(DeleteLocalBusiness)
  deleteLocalBusiness({patchState, getState}: StateContext<BusinessCategoryStateModel>, { id }: DeleteLocalBusiness) {
    const state = getState();
    const businessList = state.businessList.filter(item => item.id !== id);
    patchState({ businessList });
  }


}
