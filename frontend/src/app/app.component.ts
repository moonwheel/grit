import { Component, OnInit, Renderer2 } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { TextState } from './store/state/text.state';
import { PhotoState } from './store/state/photo.state';
import { VideoState } from './store/state/video.state';
import { ArticleState } from './store/state/article.state';
import { ProductState } from './store/state/product.state';
import { ServiceState } from './store/state/service.state';
import { VisibleState } from './store/state/visible.state';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @Select(VisibleState.globalLoading) globalLoading$: Observable<boolean>;

  constructor(private store: Store,
              private renderer: Renderer2,
              private router: Router,
  ) {}

  ngOnInit(): void {
    history.scrollRestoration = 'manual'; // prevents browser from automatic scorll restoration

    window.addEventListener('beforeunload', (event) => {
      const uploading = [
        this.store.selectSnapshot(TextState.uploading),
        this.store.selectSnapshot(PhotoState.uploading),
        this.store.selectSnapshot(VideoState.uploading),
        this.store.selectSnapshot(ArticleState.uploading),
        this.store.selectSnapshot(ProductState.uploading),
        this.store.selectSnapshot(ServiceState.uploading),
      ].some(Boolean);

      if (uploading) {
        event.preventDefault();
        event.returnValue = '';
      }
    });

    const className = 'disable-pull-to-refresh';

    this.store.select(VisibleState.isPullToRefreshEnabled).subscribe(enabled => {
      if (enabled) {
        this.renderer.removeClass(document.body, className);
      } else {
        this.renderer.addClass(document.body, className);
      }
    });

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0); // reseting scroll position on view change
      }
    });
  }
}
