import { Directive, AfterViewInit, ElementRef, Input, AfterViewChecked, OnInit } from '@angular/core';

@Directive({
  selector: '[autoFocus]'
})
export class AutofocusDirective implements AfterViewInit {

  @Input()
  set autoFocus(value: any) {
    if (typeof value === 'boolean') {
      this.shouldFocus = value;
    } else {
      this.shouldFocus = true;
    }
  }
  get autoFocus() {
    return this.shouldFocus;
  }

  private shouldFocus = true;

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    if (this.autoFocus) {
      // setTimeout used to avoid ExpressionChangedAfterItHasBeenCheckedError
      setTimeout(() => {
        this.el.nativeElement.focus();
      });
    }
  }

}
