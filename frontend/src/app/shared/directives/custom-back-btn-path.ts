import { Directive, OnInit, Input, OnDestroy } from "@angular/core";

@Directive({
    selector: '[backButtonPath]'
})
export class CustomBackButtonPath implements OnInit, OnDestroy{
    private backButtonPathKey = 'backButtonPath';

    @Input() backButtonPath: string;

    ngOnInit() {
        this.setBackButtonPath();
    }

    ngOnDestroy() {
        this.clearKey();
    }

    setBackButtonPath = () => {
        window.localStorage.setItem(this.backButtonPathKey, this.backButtonPath);
    }

    clearKey = () => {
        window.localStorage.removeItem(this.backButtonPathKey);
    }
}