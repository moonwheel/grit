import { Directive, AfterViewInit, ElementRef, OnDestroy, HostListener } from '@angular/core';
import { Subject, fromEvent, timer, merge, Subscription } from 'rxjs';
import { Platform } from '@angular/cdk/platform';
import { Store } from '@ngxs/store';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { take, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appTabsVisibilityTrigger], input, input[type="text"], input[type="number"], textarea'
})
export class TabsVisibilityTriggerDirective implements AfterViewInit, OnDestroy {

  private destroy$ = new Subject();
  private subscription$ = new Subscription();

  constructor(
    private el: ElementRef<HTMLInputElement>,
    private platform: Platform,
    private store: Store,
  ) { }

  ngAfterViewInit() { }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  @HostListener('focus')
  onFocus() {
    if (this.isMobile()) {
      this.subscription$ = this.createResizeSubscription();
      // this.waitUntilKeyboardShownOrHidden().subscribe(() => {
        // this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleAddButton: false  }));
      // });
    }
  }

  @HostListener('blur')
  onBlur() {
    if (this.isMobile()) {
      this.subscription$.unsubscribe();
      this.waitUntilKeyboardShownOrHidden().subscribe({
        complete: () => {
          this.store.dispatch(new ToggleVisible({ isKeyboardShown: false }));
        }
      });
    }
  }

  private isMobile() {
    return this.platform.ANDROID || this.platform.IOS;
  }

  private waitUntilKeyboardShownOrHidden() {
    return fromEvent(window, 'resize').pipe(
      take(1),
      // 500ms is timeout whe keyboard guaranteed to be shown\hidden
      takeUntil(timer(500))
    );
  }

  private createResizeSubscription() {
    let cachedWidth = window.innerWidth;
    let cachedHeight = window.innerHeight;

    return fromEvent(window, 'resize').pipe(
      takeUntil(this.destroy$),
    ).subscribe(() => {
      const currentWidth = window.innerWidth;
      const currentHeight = window.innerHeight;

      if (currentWidth !== cachedWidth) {
        // if width was changed - this means we rotated phone and can skip logic
        cachedWidth = currentWidth;
        cachedHeight = currentHeight;
        return;
      }

      // if width was not changed, but height was - this means we opened keyboard
      if (currentHeight < cachedHeight) {
        this.store.dispatch(new ToggleVisible({ isKeyboardShown: true }));
      } else {
        this.store.dispatch(new ToggleVisible({ isKeyboardShown: false }));
      }
    });
  }

}
