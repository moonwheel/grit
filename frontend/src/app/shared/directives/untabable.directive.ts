import { Directive, ElementRef, HostListener } from '@angular/core';
import { Platform } from '@angular/cdk/platform';

@Directive({
  selector: '[appUntabable]'
})
export class UntabableDirective {

  constructor(private el: ElementRef<HTMLInputElement>,
              private platform: Platform,
  ) {
  }

  @HostListener('blur', ['$event'])
  onBlur($event: any) {
    if (this.isMobile()) {
      const otherElementTouched = $event.sourceCapabilities && $event.sourceCapabilities.firesTouchEvents;

      if (!$event.relatedTarget || !otherElementTouched) {
        $event.preventDefault();
        this.el.nativeElement.focus();
      }
    }
  }

  @HostListener('keydown.Tab', ['$event'])
  keypress($event: any) {
    $event.preventDefault();
    this.el.nativeElement.dispatchEvent(new Event('blur'));
  }

  private isMobile() {
    const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /iPad/i,
      /iPod/i,
      /BlackBerry/i,
      /Windows Phone/i
    ];

    return toMatch.some((toMatchItem) => {
      return navigator.userAgent.match(toMatchItem);
    });
  }
}
