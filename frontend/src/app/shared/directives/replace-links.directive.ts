import { Directive, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Directive({
  selector: '[appReplaceLinks]'
})
export class ReplaceLinksDirective {
  constructor(private el: ElementRef,
              private router: Router,
              private dialog: MatDialog,
  ) { }

  @HostListener('click', ['$event'])
  public onClick(event: any) {
    if (event.target.tagName === 'A') {
      const link = event.target.getAttribute('href');
      this.dialog.closeAll();
      this.router.navigateByUrl(link);
      event.preventDefault();
    } else {
      return;
    }
  }
}
