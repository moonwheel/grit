import { Directive, AfterViewInit, ElementRef, Self, Optional, HostBinding, HostListener, OnDestroy } from '@angular/core';
import { NgModel, NgControl } from '@angular/forms';
import { NEVER, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appPriceInput]'
})
export class PriceInputDirective implements AfterViewInit, OnDestroy {

  private value = '';

  private destroy$ = new Subject();

  private get inputElement() {
    return this.el.nativeElement as HTMLInputElement;
  }

  constructor(private el: ElementRef,
              @Self() @Optional() private model: NgModel,
              @Self() @Optional() private control: NgControl,
  ) {
  }

  ngAfterViewInit() {
    this.checkCents();
    const observable = this.model && this.model.valueChanges || this.control && this.control.valueChanges || NEVER;
    observable.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.checkCents();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  @HostListener('blur')
  inputBlur() {
    this.checkCents();
  }

  private checkCents() {
    this.inputElement.value = this.getValueWithCents(this.inputElement.value);
  }

  private getValueWithCents(value: string): string {
    if (!value) {
      return '';
    }

    const splitted = value.split(',');
    const main = splitted[0];
    let afterDot = splitted[1];

    if (!afterDot) {
      afterDot = '00';
    } else
    if (afterDot.length === 1) {
      afterDot += '0';
    }

    return `${main},${afterDot}`;
  }

}
