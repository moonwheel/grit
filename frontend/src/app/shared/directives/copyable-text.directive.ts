import { ElementRef, HostListener, Input, Directive } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";

@Directive({
    selector: '[copyableText]'
})
export class CopyAbleTextDirective {
    
    @Input('copyableText') copyValue: string;
    @Input('snackDuration') duration: number = 3000;

    private node: HTMLElement = null;

    constructor(private ref: ElementRef, private snack: MatSnackBar, private translationService: TranslateService) {}


    @HostListener('click', ['$event'])
    handleClick(event: any) {
        const container = this.createTextContainer();

        container.value = this.copyValue;
        document.body.appendChild(container);

        container.select();
        document.execCommand('copy');

        document.body.removeChild(container);

        this.showSnack();
    }

    private showSnack() {
        this.translationService.get('GENERAL.ACTIONS.COPIED_TEXT').subscribe(text => {
            this.snack.open(text, '', {duration: this.duration})
        })
    }

    private createTextContainer() {
        const elem = document.createElement('textarea');
        elem.style.height = '0px';

        return elem;
    }
}