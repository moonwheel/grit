import { Directive, HostListener, Input } from '@angular/core';
import { Store } from '@ngxs/store';

import { ChatActions } from 'src/app/store/actions/chat.actions';

@Directive({
  selector: '[appMessageTo]'
})
export class MessageToDirective {

  @Input() set appMessageTo(value: number) {
    this.accountId = Number(value);
  }

  private accountId: number;

  constructor(private store: Store,
  ) {
  }

  @HostListener('click')
  goToChat() {
    this.store.dispatch(new ChatActions.TryToStartChat(this.accountId));
  }
}
