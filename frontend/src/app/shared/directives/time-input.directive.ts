import {Directive, HostListener, ElementRef} from '@angular/core';

@Directive({
    selector: '[timeInput]'
})
export class TimeInputDirective {

    timeRegexp = new RegExp('^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$');
    value = '';

    constructor(private el: ElementRef) {
    }

    @HostListener('focusout') onLoseFocus() {
        this.value = this.el.nativeElement.value;
        if (this.timeRegexp.test(this.value)) {
            return
        }
        this.el.nativeElement.value = '';
    }
}
