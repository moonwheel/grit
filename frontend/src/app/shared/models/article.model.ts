import { ViewModel } from '../components/actions/views/view.model';
import { ShareModel } from './actions/share.model';
import { BookmarkModel } from './bookmark.model';
import { AbstractEntityModel } from './abstract-entity.model';

export interface ArticleModel extends AbstractEntityModel {
  id: number;
  user: any;
  cover: string;
  thumb: string;
  title: string;
  text: string;
  photos: File[];
  location: string;
  shares: ShareModel[];
  bookmarks: BookmarkModel[];
  created: string;
  updated: Date;
  deleted: Date;
}
