
import { ProductModel } from 'src/app/shared/models/content/products/product.model';
import { UserModel } from 'src/app/shared/models/user/user.model';

export interface CartModel {
  id: number;
  seller: UserModel;
  buyer: UserModel;
  products: ProductModel;
  quantity: number;
  createdAt: Date;
}
