import { UserModel } from 'src/app/shared/models/user/user.model';

export interface MessageModel {
  id: string;
  user: UserModel;
  message: string;
  photos: File[];
  videos: File[];
  documents: File[];
  location: string;
  createdAt: Date;
}
