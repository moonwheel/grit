export interface CreditCardModel {
  id: string;
  userId: string;
  alias: string;
  cardProvider: string;
  expiration: string;
  currency: string;
  type: string;
}

export interface CreditCardRequestModel {
  id?: string;
  holder: string;
  card_number: string;
  month: string;
  year: string;
  cvc: string;
}
