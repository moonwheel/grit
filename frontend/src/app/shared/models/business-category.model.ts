export interface BusinessCategoryModel {
  id: number;
  parentId: number;
  name: string;
  created: Date;
  updated: Date;
  deleted: Date;
}
