export interface RegisterModel {
    user: {
        type: string;
        fullName?: string;
        // lastName?: string;
        birthday?: Date;
        gender?: string;
        email: string;
        password: string;
        created?: Date;
    };
}
