export interface BusinessModel {
    photo: any;
    thumb: any;
    name: string;
    pageName: string;
    id: number;
    category: number;
    description?: string;
    street: string;
    zip: string;
    city: string;
    phone: string;
    reactivationtoken: string;
    deleted: Date;
    language?: string;
    privacy?: string;
    legal_notice?: string;
    hours: any[];
    kycDocumentId?: string;
    verificationStatus?: string;
}
