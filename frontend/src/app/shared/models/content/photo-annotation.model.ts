import { AccountModel } from '../user';

export class PhotoAnnotationModel {
  id: number;
  target: AccountModel;
  x: number;
  y: number;
}
