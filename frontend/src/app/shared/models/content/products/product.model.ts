import { MessageModel } from 'src/app/shared/models/main/message.model';
import { ShareModel } from '../../actions/share.model';
import { BookmarkModel } from '../../bookmark.model';
import { ReviewModel } from '../../../components/actions/reviews/review.model';
import { ProductVariantModel } from './product-variant.model';
import { AbstractEntityModel } from '../../abstract-entity.model';
import { AddressModel } from '../../user';

export interface ProductModel extends AbstractEntityModel {
  id: number;
  addressId: string;
  address: AddressModel;
  cover: ProductPhoto;
  cover_: ProductPhoto;
  photos: ProductPhoto[];
  type: string; // Single Product or Product Variation
  productVariation: ProductVariantModel[];
  brand: string;
  title: string;
  condition: string;
  price: string | number;
  quantity: number; // only Business consider quantity of Product Variants
  category: string;
  shopcategory_: any; // only Business
  description: string;
  deliveryOptions: string;
  deliveryTime: string;
  shippingCosts: number;
  freeShipping: boolean;
  collection: boolean;
  onlyCollection: boolean;
  variants: ProductVariantModel[];
  variantOptions: string;
  cheapestVariant: ProductVariantModel;
  payerReturn: string;
  draft: boolean;
  active: boolean;
  questions: MessageModel[];
  shares: ShareModel[];
  bookmarks: BookmarkModel[];
  reviews: ReviewModel[]; // only Business
  rating: number; // only Business
  reviewsCount: number; // only Business
  created: Date;
  updated: Date;
  deleted: Date;
  selectedVariant?: ProductVariantModel;
  sellerRating?: number;
  bought?: any[];
  sellerRatingCount?: number;
}

export interface ProductPhoto {
  created: string;
  deleted: string;
  id: number | null;
  photoUrl: string;
  thumbUrl: string;
  order: number;
}

export interface ProductDetailsModel {
  active: boolean;
  bookmarked: boolean;
  category: string;
  condition: string;
  created: string;
  deliveryOptions: string;
  deliveryTime: string;
  id: string;
  photoUrl: string;
  price: string;
  product_reviews_count: string;
  product_shopcategory_name: string;
  product_variants_id: string;
  product_variants_color: string;
  product_variants_flavor: string;
  product_variants_material: string;
  product_variants_size: string;
  quantity: string;
  rating: string;
  revenue: string;
  sales: string;
  shippingCosts: string;
  thumbUrl: string;
  title: string;

  variantText?: string;
}

export interface NormalizedRawModel {
  id: string;
  type: string;
  title: string;
  condition: string;
  price: string;
  quantity: string;
  category: string;
  description: string;
  variantOptions: null | string;
  deliveryOptions: string;
  deliveryTime: string;
  shippingCosts: string;
  payerReturn: null | string;
  bank: {
    id: string | null;
  }
  active: boolean;
  draft: boolean;
  user: any;
  business: any;
  shopcategory: any;
  address: any;
  reviews: any;
  cover: {
    id: string | null;
    photoUrl: string | null;
    thumbUrl: string | null;
  }
  created: string;
  updated: string;
}