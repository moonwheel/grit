import { ProductPhoto } from './product.model';

export interface ProductVariantPhotoModel {
  variantId: string;
  photoId: string;
  order: number;
  photo: ProductPhoto;
}

export interface ProductVariantModel {
  id: number;
  photos: any[];
  size: string;
  color: string;
  flavor: string;
  material: string;
  price: string | number;
  quantity: string | number;
  enable: boolean;
  photosOrder: string;
  cover?: ProductVariantPhotoModel;
}
