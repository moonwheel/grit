import { UserModel } from 'src/app/shared/models/user/user.model';
import { ServiceModel } from '../service.model';

export interface BookingModel {
  id: number;
  seller: UserModel;
  buyer: UserModel;
  service: ServiceModel; // Only one service can be booked at the same time (no Shopping Cart)
  booked: Date;
  paid: Date;
  cancelled: Date;
  whoCancelled: string; // Buyer or Seller
  refunded: Date;
  sold: boolean;
}
