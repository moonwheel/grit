import { ShareModel } from '../actions/share.model';
import { BookmarkModel } from '../bookmark.model';
import { AbstractEntityModel } from '../abstract-entity.model';
import { PhotoAnnotationModel } from './photo-annotation.model';

export interface PhotoModel extends AbstractEntityModel {
  id: number;
  photo: string;
  thumb: string;
  filterName: string;
  title: string;
  category: string;
  description: string;
  location: string;
  shares: ShareModel[];
  bookmarks: BookmarkModel[];
  created: Date;
  updated: Date;
  deleted: Date;
  link: string;
  index: number;
  annotations: PhotoAnnotationModel[];
  viewsCount: number;
  likesCount: number;
}
