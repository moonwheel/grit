export interface IrregularScheduleModel {
    id: number;
    from: Date;
    to: Date;
    created: Date;
    updated: Date;
    deleted: Date;
    intervals: any;
}
