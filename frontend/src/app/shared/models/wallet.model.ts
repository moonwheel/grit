export interface WalletModel {
  id?: string;
  balance: number;
  currency: string;
  created?: Date;
}
