import { UserModel } from 'src/app/shared/models/user/user.model';

export interface BookmarkModel {
  id: number;
  user: UserModel;
  bookmark: boolean;
  createdAt: Date;
  updatedAt?: Date;
}

export interface UserBookmarkRaw {
  bookmarks_id: string;
  bookmarks_created: Date;
  bookmarks_owner_id: string;
  bookmarks_account_id?: any;
  bookmarks_text_id?: any;
  bookmarks_photo_id: string;
  bookmarks_video_id?: any;
  bookmarks_article_id?: any;
  bookmarks_product_id?: any;
  bookmarks_service_id?: any;
  owner_id: string;
  account_id?: any;
  account_person_id?: any;
  account_person_thumb?: any;
  account_person_pageName?: any;
  account_person_fullName?: any;
  account_business_id?: any;
  account_business_thumb?: any;
  account_business_name?: any;
  account_business_pageName?: any;
  photo_id: string;
  photo_thumb: string;
  photo_title: string;
  photo_person_id: string;
  photo_account_person_id: string;
  photo_account_person_thumb?: any;
  photo_account_person_pageName: string;
  photo_account_person_fullName: string;
  photo_account_business_id?: any;
  photo_account_business_thumb?: any;
  photo_account_business_name?: any;
  photo_account_business_pageName?: any;
  photo_likes_id?: any;
  photo_likes_created?: any;
  photo_likes_updated?: any;
  photo_likes_user_id?: any;
  photo_likes_photo_id?: any;
  video_id?: any;
  video_thumb?: any;
  video_title?: any;
  video_person_id?: any;
  video_account_person_id?: any;
  video_account_person_thumb?: any;
  video_account_person_pageName?: any;
  video_account_person_fullName?: any;
  video_account_business_id?: any;
  video_account_business_thumb?: any;
  video_account_business_name?: any;
  video_account_business_pageName?: any;
  video_likes_id?: any;
  video_likes_created?: any;
  video_likes_updated?: any;
  video_likes_user_id?: any;
  video_likes_video_id?: any;
  article_id?: any;
  article_thumb?: any;
  article_title?: any;
  article_person_id?: any;
  article_account_person_id?: any;
  article_account_person_thumb?: any;
  article_account_person_pageName?: any;
  article_account_person_fullName?: any;
  article_account_business_id?: any;
  article_account_business_thumb?: any;
  article_account_business_name?: any;
  article_account_business_pageName?: any;
  article_likes_id?: any;
  article_likes_created?: any;
  article_likes_user_id?: any;
  article_likes_article_id?: any;
  text_id?: any;
  text_text?: any;
  text_person_id?: any;
  text_account_person_id?: any;
  text_account_person_thumb?: any;
  text_account_person_pageName?: any;
  text_account_person_fullName?: any;
  text_account_business_id?: any;
  text_account_business_thumb?: any;
  text_account_business_name?: any;
  text_account_business_pageName?: any;
  text_likes_id?: any;
  text_likes_created?: any;
  text_likes_updated?: any;
  text_likes_user_id?: any;
  text_likes_text_id?: any;
  product_id?: any;
  product_type?: any;
  product_title?: any;
  product_price?: any;
  product_active?: any;
  product_created?: any;
  product_photos_id?: any;
  product_photos_photoUrl?: any;
  product_photos_thumbUrl?: any;
  product_photos_order?: any;
  product_photos_created?: any;
  product_photos_deleted?: any;
  product_photos_product_id?: any;
  product_variants_id?: any;
  product_variants_size?: any;
  product_variants_color?: any;
  product_variants_flavor?: any;
  product_variants_material?: any;
  product_variants_price?: any;
  product_variants_quantity?: any;
  product_variants_enable?: any;
  product_variants_created?: any;
  product_variants_updated?: any;
  product_variants_deleted?: any;
  product_variants_product_id?: any;
  variants_to_photos_variantId?: any;
  variants_to_photos_photoId?: any;
  variants_to_photos_order?: any;
  variants_to_photos_productId?: any;
  product_variants_photos_id?: any;
  product_variants_photos_photoUrl?: any;
  product_variants_photos_thumbUrl?: any;
  product_variants_photos_order?: any;
  product_variants_photos_created?: any;
  product_variants_photos_deleted?: any;
  product_variants_photos_product_id?: any;
  product_person_id?: any;
  product_account_person_id?: any;
  product_account_person_thumb?: any;
  product_account_person_pageName?: any;
  product_account_person_fullName?: any;
  product_account_business_id?: any;
  product_account_business_thumb?: any;
  product_account_business_name?: any;
  product_account_business_pageName?: any;
  service_id?: any;
  service_active?: any;
  service_title?: any;
  service_price?: any;
  service_quantity?: any;
  service_hourlyPrice?: any;
  service_schedule_id?: any;
  service_schedule_duration?: any;
  service_schedule_break?: any;
  service_schedule_created?: any;
  service_schedule_updated?: any;
  service_schedule_deleted?: any;
  service_schedule_service_id?: any;
  service_schedule_bydate_id?: any;
  service_schedule_bydate_from?: any;
  service_schedule_bydate_to?: any;
  service_schedule_bydate_bookings?: any;
  service_schedule_bydate_created?: any;
  service_schedule_bydate_updated?: any;
  service_schedule_bydate_deleted?: any;
  service_schedule_bydate_scheduleId?: any;
  service_photos_id?: any;
  service_photos_link?: any;
  service_photos_thumb?: any;
  service_photos_index?: any;
  service_photos_created?: any;
  service_photos_updated?: any;
  service_photos_deleted?: any;
  service_photos_service_id?: any;
  service_person_id?: any;
  service_account_person_id?: any;
  service_account_person_thumb?: any;
  service_account_person_pageName?: any;
  service_account_person_fullName?: any;
  service_account_business_id?: any;
  service_account_business_thumb?: any;
  service_account_business_name?: any;
  service_account_business_pageName?: any;
  product_price_conditional?: any;
}

const resolveMap = {
  'account': 'user',
}

export function mapRawbookmarkModelToAppModel(rawBookmark: UserBookmarkRaw) {
  const bookmark: Record<string, any> = {};
  bookmark.photo = pickPropertiesAndConvertToObject('photo', rawBookmark, resolveMap);
  bookmark.text = pickPropertiesAndConvertToObject('text', rawBookmark, resolveMap);
  bookmark.article = pickPropertiesAndConvertToObject('article',rawBookmark, resolveMap);
  bookmark.video = pickPropertiesAndConvertToObject('video', rawBookmark, resolveMap);
  bookmark.service = pickPropertiesAndConvertToObject('service', rawBookmark, resolveMap);
  bookmark.product = pickPropertiesAndConvertToObject('product', rawBookmark, resolveMap);
  bookmark.user = pickPropertiesAndConvertToObject('user', rawBookmark, resolveMap);

  return bookmark;
}

function pickPropertiesAndConvertToObject(prefix: string, sourceObj: Record<string, any>, resolveKeysMap?: Record<string, any>): Record<string, any> {
  const keys = Object.keys(sourceObj);
  const obj: Record<string, any> = {};

  const matchedKeys = keys.filter(key => key.split('_')[0] === prefix);
  
  for(let i = 0; i < matchedKeys.length; i++) {
    const key = matchedKeys[i];
    const value = sourceObj[key];
    if(!value) continue;

    let path = key.split('_');
    path.shift();
    path = path.join('_') as any;

    setValueToPath(path as any, obj, value, resolveKeysMap);
  }


  return Object.keys(obj).length ? obj : null;
}

function setValueToPath(path: string, obj, value: any, resolve?) {
  const pathArray = path.split('_');
  let key = pathArray.shift();

  if(resolve && key in resolve) {
    key = resolve[key];
  }

  if(!key) return obj;

  if(!obj[key]) {
    obj[key] = {};
  }

  if(!pathArray.length) {
    obj[key] = value;
  }

  if(pathArray.length) return setValueToPath(pathArray.join('_'), obj[key], value);
}