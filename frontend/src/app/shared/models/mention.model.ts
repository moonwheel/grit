import { AccountModel } from './user';

export interface MentionModel {
  id: number;
  target: AccountModel;
  mention_text: string;
}
