import { AddressModel } from './user/user.model';
import { MessageModel } from 'src/app/shared/models/main/message.model';
import { ShareModel } from './actions/share.model';
import { BookmarkModel } from './bookmark.model';
import { ReviewModel } from '../components/actions/reviews/review.model';
import { ScheduleModel } from './schedule.model';
import { PhotoModel } from './content/photo.model';
import { RegularScheduleModel } from './regular-schedule.model';
import { IrregularScheduleModel } from './irregular-schedule.model';
import { AbstractEntityModel } from './abstract-entity.model';
import { MentionModel } from './mention.model';

export interface ServiceModel extends AbstractEntityModel {
  id: number;
  active: boolean;
  draft: boolean;
  title: string;
  price: number;
  hourlyPrice: boolean;
  description: string;
  performance: string;
  category: string;
  address: AddressModel;
  bank_id: string;
  created: Date;
  updated: Date;
  deleted: Date;
  cover: File;
  rating: number;
  quantity: number;
  reviewsCount: number;

  schedule: ScheduleModel;
  byDate: RegularScheduleModel;
  byDays: IrregularScheduleModel;
  message: MessageModel[];
  shares: ShareModel[];
  bookmarks: BookmarkModel[];
  reviews: ReviewModel[];
  photos: PhotoModel[];
  hashtags: any[];
  mentions: MentionModel[];
  user_: {
    id: number;
    fullName: string;
    // lastName: string;
    photo: string;
    residence: string;
    sellerRating: string;
  };

  displayDateInfoHeader?: any[];
  displayDateInfo?: any[];
  durationInfo?: string;

  photoCover: PhotoModel;
}
