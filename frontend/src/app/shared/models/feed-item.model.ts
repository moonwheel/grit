import { AbstractEntityType } from './abstract-entity.model';

export interface FeedItemModel {
  id: number;
  tableName: AbstractEntityType;
  userId?: number;
}
