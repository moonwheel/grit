export interface Annotation {
  annotations: Array<object>;
  targetPhotoId?: number;
  value: string;
}
