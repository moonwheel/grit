import { MessageModel } from './message.model';
import { ChatroomModel } from './chatroom.model';
import { AccountModel } from '../user';

export type AttachmentType = 'photo' | 'video' | 'document';

// tslint:disable:variable-name
export class MessageAttachmentModel {
  id: number;
  link: string;
  thumb: string | null;
  file_name: string | null;
  type: AttachmentType;
  created: Date;
  user: AccountModel;

  uploading?: boolean;
  processing?: boolean;
  progress?: number;
}
