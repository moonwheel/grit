import { ChatroomModel } from './chatroom.model';
import { AccountModel } from '../user';
import { MessageAttachmentModel } from './message-attachment.model';
import { AbstractEntityType } from '../abstract-entity.model';
import { ArticleModel } from '../article.model';

// tslint:disable:variable-name
export class MessageModel {
  id: number | string;
  text: string | null;
  reply_to: MessageModel;
  is_forwarded: boolean;
  is_service_message: boolean;
  attachment: MessageAttachmentModel;
  chatroom: number;
  user: AccountModel;
  created: Date;
  deleted: Date;
  updated: Date | null;

  shared_article?: any;
  shared_photo?: any;
  shared_product?: any;
  shared_service?: any;
  shared_text?: any;
  shared_user?: any;
  shared_video?: any;

  own?: boolean;
  sharedContentType?: AbstractEntityType | 'user';
  sharedContentLink?: string;
  sharedContentTitle?: string;

  temporaryId?: string;
}
