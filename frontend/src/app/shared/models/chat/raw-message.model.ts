import { MessageModel } from './message.model';

export interface RawMessageModel {
    messages_id: string;
    messages_text: string;
    messages_is_forwarded: boolean;
    messages_is_service_message: boolean;
    messages_created: string;
    messages_updated: null | string;
    reply_to_id: null | string;
    reply_to_text: null | string;
    reply_to_is_forwarded: null | string;
    reply_to_is_service_message: null | string;
    reply_to_created: null | string;
    reply_to_updated: null | string;
    attachment_id: null | string;
    attachment_link: null | string;
    attachment_thumb: null | string;
    attachment_duration: null | string;
    attachment_file_name: null | string;
    attachment_type: null | string;
    attachment_created: null | string;
    shared_user_id: null | string;
    shared_user_business_id: null | string;
    shared_user_business_photo: null | string;
    shared_user_business_name: null | string;
    shared_user_business_pageName: null | string;
    shared_user_person_id: null | string;
    shared_user_person_photo: null | string;
    shared_user_person_pageName: null | string;
    shared_user_person_fullName: null | string;
    shared_text_id: null | string;
    shared_text_text: null | string;
    shared_text_created: null | string;
    shared_text_updated: null | string;
    shared_text_user_id: null | string;
    shared_text_user_business_id: null | string;
    shared_text_user_business_photo: null | string;
    shared_text_user_business_name: null | string;
    shared_text_user_business_pageName: null | string;
    shared_text_user_person_id: null | string;
    shared_text_user_person_photo: null | string;
    shared_text_user_person_pageName: null | string;
    shared_text_user_person_fullName: null | string;
    shared_photo_id: null | string;
    shared_photo_photo: null | string;
    shared_photo_thumb: null | string;
    shared_photo_title: null | string;
    shared_photo_description: null | string;
    shared_photo_category: null | string;
    shared_photo_location: null | string;
    shared_photo_filterName: null | string;
    shared_photo_created: null | string;
    shared_photo_updated: null | string;
    shared_photo_user_id: null | string;
    shared_photo_user_business_id: null | string;
    shared_photo_user_business_photo: null | string;
    shared_photo_user_business_name: null | string;
    shared_photo_user_business_pageName: null | string;
    shared_photo_user_person_id: null | string;
    shared_photo_user_person_photo: null | string;
    shared_photo_user_person_pageName: null | string;
    shared_photo_user_person_fullName: null | string;
    shared_video_id: null | string;
    shared_video_video: null | string;
    shared_video_cover: null | string;
    shared_video_thumb: null | string;
    shared_video_title: null | string;
    shared_video_description: null | string;
    shared_video_duration: null | string;
    shared_video_category: null | string;
    shared_video_location: null | string;
    shared_video_created: null | string;
    shared_video_updated: null | string;
    shared_video_user_id: null | string;
    shared_video_user_business_id: null | string;
    shared_video_user_business_photo: null | string;
    shared_video_user_business_name: null | string;
    shared_video_user_business_pageName: null | string;
    shared_video_user_person_id: null | string;
    shared_video_user_person_photo: null | string;
    shared_video_user_person_pageName: null | string;
    shared_video_user_person_fullName: null | string;
    shared_article_id: null | string;
    shared_article_cover: null | string;
    shared_article_thumb: null | string;
    shared_article_title: null | string;
    shared_article_text: null | string;
    shared_article_category: null | string;
    shared_article_location: null | string;
    shared_article_created: null | string;
    shared_article_updated: null | string;
    shared_article_user_id: null | string;
    shared_article_user_business_id: null | string;
    shared_article_user_business_photo: null | string;
    shared_article_user_business_name: null | string;
    shared_article_user_business_pageName: null | string;
    shared_article_user_person_id: null | string;
    shared_article_user_person_photo: null | string;
    shared_article_user_person_pageName: null | string;
    shared_article_user_person_fullName: null | string;
    shared_product_id: null | string;
    shared_product_type: null | string;
    shared_product_title: null | string;
    shared_product_condition: null | string;
    shared_product_price: null | string;
    shared_product_quantity: null | string;
    shared_product_category: null | string;
    shared_product_description: null | string;
    shared_product_variantOptions: null | string;
    shared_product_deliveryOptions: null | string;
    shared_product_deliveryTime: null | string;
    shared_product_shippingCosts: null | string;
    shared_product_payerReturn: null | string;
    shared_product_active: null | string;
    shared_product_created: null | string;
    shared_product_updated: null | string;
    shared_product_user_id: null | string;
    shared_product_user_business_id: null | string;
    shared_product_user_business_photo: null | string;
    shared_product_user_business_name: null | string;
    shared_product_user_business_pageName: null | string;
    shared_product_user_person_id: null | string;
    shared_product_user_person_photo: null | string;
    shared_product_user_person_pageName: null | string;
    shared_product_user_person_fullName: null | string;
    shared_service_id: null | string;
    shared_service_active: null | string;
    shared_service_title: null | string;
    shared_service_price: null | string;
    shared_service_quantity: null | string;
    shared_service_hourlyPrice: null | string;
    shared_service_description: null | string;
    shared_service_performance: null | string;
    shared_service_category: null | string;
    shared_service_created: null | string;
    shared_service_updated: null | string;
    shared_service_user_id: null | string;
    shared_service_user_business_id: null | string;
    shared_service_user_business_photo: null | string;
    shared_service_user_business_name: null | string;
    shared_service_user_business_pageName: null | string;
    shared_service_user_person_id: null | string;
    shared_service_user_person_photo: null | string;
    shared_service_user_person_pageName: null | string;
    shared_service_user_person_fullName: null | string;
    author_id: string;
    author_business_id: null | string;
    author_business_photo: null | string;
    author_business_name: null | string;
    author_business_pageName: null | string;
    author_person_id: string;
    author_person_photo: null | string;
    author_person_pageName: string;
    author_person_fullName: string;
};

export function mapRawMessageModelToMessageModel(rawMessage: RawMessageModel): MessageModel {
    const message = {} as MessageModel;

    const user = {
        id: +rawMessage.author_person_id,
        person: {
            id: +rawMessage.author_person_id,
            photo: rawMessage.author_person_photo,
            pageName: rawMessage.author_person_pageName,
            fullName: rawMessage.author_person_fullName,
        } as any,
        business: !rawMessage.author_business_id ? null : {
            id: rawMessage.author_business_id,
            name: rawMessage.author_business_name,
            photo: rawMessage.author_business_photo,
            pageName: rawMessage.author_business_pageName,
        } as any
    } as any;

    message.id = +rawMessage.messages_id;
    message.user = user;
    message.text = rawMessage.messages_text;
    message.reply_to = !rawMessage.reply_to_id ? {
        id: rawMessage.reply_to_id,
        text: rawMessage.reply_to_text,
        is_forwarded: rawMessage.reply_to_is_forwarded,
        is_service_message: rawMessage.reply_to_is_service_message,
        created: rawMessage.reply_to_created,
        updated: rawMessage.reply_to_updated
    } as any : null;
    message.is_forwarded = rawMessage.messages_is_forwarded;
    message.is_service_message = rawMessage.messages_is_service_message;
    message.attachment = {
        id: +rawMessage.attachment_id,
        link: rawMessage.attachment_link,
        thumb: rawMessage.attachment_thumb,
        file_name: rawMessage.attachment_file_name,
        type: rawMessage.attachment_type as any,
        created: rawMessage.attachment_created as any,
        user: user
        // uploading
        // procressing
        // progress
    },

    message.created = rawMessage.messages_created as any;
    //messages.deleted
    message.updated = rawMessage.messages_updated as any

    return message;
}