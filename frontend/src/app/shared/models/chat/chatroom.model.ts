import { ChatroomUserModel } from './chatroom-user.model';
import { MessageAttachmentModel } from './message-attachment.model';
import { MessageModel } from './message.model';

// tslint:disable:variable-name
export class ChatroomModel {
  id: number;
  title: string | null;
  cover: string | null;
  thumb: string | null;
  link: string | null;
  oponentId: number | null;
  created: Date;
  deleted: Date;
  chatroom_users: ChatroomUserModel[];
  messages: MessageModel[];
  attachment: MessageAttachmentModel;

  last_message?: MessageModel;
  chatroomUser?: ChatroomUserModel;
  messagesFetched?: boolean;
}
