import { AccountModel } from '../user';
import { MessageModel } from './message.model';
import { ChatroomModel } from './chatroom.model';

// tslint:disable:variable-name
export class ChatroomUserModel {
  id: number;
  is_muted: boolean;
  is_blocked: boolean;
  created: Date;
  deleted: Date;
  user: AccountModel;
  last_read_message: MessageModel | null;

  chatroom?: ChatroomModel;
  chatroomId?: number;
}
