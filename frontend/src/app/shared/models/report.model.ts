export interface ReportModel {
    id: number;
    report: string;
    type: string;
    status: string;
    created: string;
    updated: string;
    user: any;
    reportType: string;
    target: any;
  }