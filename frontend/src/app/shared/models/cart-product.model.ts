import { UserModel } from './user';
import { ProductModel } from './content';

export interface CartProductModel {
  id?: number;
  user?: UserModel;
  product: ProductModel;
  quantity: number;
  collected: boolean;
  created?: string;
  updated?: string;
}
