import { AccountModel } from './user';
import { TextModel } from './text.model';
import { PhotoModel, ProductModel } from './content';
import { VideoModel } from './video.model';
import { ArticleModel } from './article.model';
import { ServiceModel } from './service.model';
import { ReviewModel } from '../components/actions/reviews/review.model';
import { Injectable } from '@angular/core';

export type NotificationAction =
  'follows'
  | 'liked'
  | 'commented'
  | 'tagged'
  | 'ordered'
  | 'shipped'
  | 'booked'
  | 'cancelled'
  | 'replied'
  | 'mentioned'
  | 'tagged';

export type NotificationContentDisplayName =
  'text'
  | 'photo'
  | 'video'
  | 'article'
  | 'review'
  | 'product'
  | 'service'
  | 'comment'
  | 'order'
  | 'booking';

export type NotificationContentType =
  'text'
  | 'photo'
  | 'video'
  | 'article'
  | 'product'
  | 'service';

// tslint:disable:variable-name
export class NotificationModel {
  id: number;
  action: NotificationAction;
  content_display_name: NotificationContentDisplayName;
  content_type: NotificationContentType;

  text: TextModel;
  photo: PhotoModel;
  video: VideoModel;
  article: ArticleModel;
  product: ProductModel;
  service: ServiceModel;
  product_review: any;
  service_review: any;
  seller_review: any;

  issuer: AccountModel;
  owner: AccountModel;
  read: boolean;
  created: string;
  thumbUrl: string;

  thumb?: string;
  link?: string;
}
