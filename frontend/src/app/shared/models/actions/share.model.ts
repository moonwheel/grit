import { UserModel } from 'src/app/shared/models/user/user.model';

export interface ShareModel {
  id: number;
  user: UserModel;
  repost?: boolean;
  message?: boolean;
  external?: boolean;
  text?: string; // only for Repost
  createdAt: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
