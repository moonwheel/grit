import { ViewModel } from '../components/actions/views/view.model';
import { ShareModel } from './actions/share.model';
import { BookmarkModel } from './bookmark.model';
import { AbstractEntityModel } from './abstract-entity.model';

export interface VideoModel extends AbstractEntityModel {
  id: number;
  video: string;
  duration: string;
  cover: string;
  thumb: string;
  title: string;
  description: string;
  location?: string;
  video_shares: ShareModel[];
  video_bookmarks: BookmarkModel[];
  created: Date;
  updated: Date;
  deleted: Date;
  user_id: number;
}
