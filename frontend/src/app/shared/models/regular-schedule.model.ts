import {Time} from '@angular/common';

export interface RegularScheduleModel {
    id: number;
    from: Time;
    to: Time;
    created: Date;
    updated: Date;
    deleted: Date;
    day: {
        id: number;
        day: string;
    };
    intervals: any;
}
