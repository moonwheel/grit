

export interface ScheduleModel {
    id: number;
    duration: string;
    break: string;
    created: Date;
    updated: Date;
    deleted: Date;
    byDate?: ScheduleByDateModel[];
    byDays?: ScheduleByDayModel[];
    startInfo?: string;
    durationInfo?: string;
}

export interface ScheduleByDayModel {
    id: number;
    from: string;
    to: string;
    day: { id: number, day: string };
    created: Date;
    updated: Date;
    intervals: IntervalModel[];
}

export interface ScheduleByDateModel {
    id: number;
    from: string;
    to: string;
    bookings: number;
    created: Date;
    updated: Date;
    deleted: Date;
}

export interface IntervalModel {
    id: number;
    from: string;
    to: string;
}
