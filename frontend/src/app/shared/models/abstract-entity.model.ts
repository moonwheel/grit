import { AccountModel, AddressModel } from './user';
import { MentionModel } from './mention.model';

export type AbstractEntityType =
  'photo'
  | 'video'
  | 'article'
  | 'product'
  | 'service'
  | 'text'
  | 'followingRequest'
  | 'followingRequestChanged'
  | 'photo-attachment'
  | 'video-attachment'
  | 'document-attachment'
  | 'review'
;

export interface AbstractEntityModel {
  id: number;
  draft: boolean;
  likes: number[];
  liked: boolean;
  likesCount: number;
  comments: number[];
  commentsCount: number;
  views: number[];
  viewsCount: number;
  uploading: boolean;
  processing: boolean;
  progress: number;
  user: AccountModel;
  entity_business_address: AddressModel;
  tableName?: AbstractEntityType;
  isDetailed?: boolean;
  date_from?: string;
  date_to?: string;
  duration?: string;
  bookmarked?: boolean;
  bookmarksCount?: number;
  viewed?: boolean;
  directLink?: string;
  mentions?: MentionModel[];
  created?: Date | string;
  title?: string;
  photo?: string;
  thumb?: string;
}
