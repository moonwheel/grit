import { UserModel } from 'src/app/shared/models/user/user.model';


export interface FollowersModel {
  id: number;
  user: UserModel;
  follower: UserModel[];
  createdAt: Date;
  deletedAt?: Date;
}
