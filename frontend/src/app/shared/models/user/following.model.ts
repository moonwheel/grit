import { UserModel } from 'src/app/shared/models/user/user.model';

export interface FollowingModel {
  id: number;
  user: UserModel;
  following: UserModel[];
  createdAt: Date;
  deletedAt?: Date;
}
