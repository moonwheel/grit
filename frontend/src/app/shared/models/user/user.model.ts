import { OrderModel } from '../order.model';
import { BookingModel } from '../content/booking.model';
import { BusinessModel } from '../business.model';

export interface UserModel {
  id: number;
  active: boolean;
  type: string; // Consumer or Business
  photo?: string;
  thumb?: string;
  photoData?: File;
  pageName?: string;
  fullName: string; // only Consumer
  // firstName: string; // only Consumer
  // lastName: string; // only Consumer
  companyName: string;
  phone: string; // only Business
  email: string;
  password: string;
  gender: string; // only Consumer
  birthday: Date; // only Consumer
  residence?: string; // only Consumer
  description?: string;
  legalNotice: string; // only Business
  addresses?: AddressModel[];
  delivery_address?: AddressModel;
  hours?: HoursModel[];
  bankAccount?: BankAccountModel[];
  orders?: OrderModel[];
  bookings?: BookingModel[];
  followers?: UserModel[];
  following?: UserModel[];
  bookmarks?: string[]; // ?
  kycDocumentId?: string;
  verificationStatus?: string;
  sellerReviews: number; // Seller Reviews for Products/Shop
  sellerRating: number; // Seller Rating for Products/Shop
  block: boolean; // User can block a User
  privacy: string; // Public or Private (only accepted Follower)
  language: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  business: BusinessModel;
}

export interface AddressModel {
  id: number;
  user: UserModel;
  type: string; // main or delivery
  fullName: string;
  companyName: string; // company or care of
  street: string; // Street and Number
  appendix: string; // e.g. Appartment, Unit, Building
  zip: number;
  city: string;
  state: string; // necessary?
  country: string; // necessary?
  created: string; // necessary?
}

export interface HoursModel {
  id: number;
  user: UserModel;
  day: string;
  start: Date;
  end: Date;
}

export interface BankAccountModel {
  id?: number;
  holder: string;
  iban: string;
}
