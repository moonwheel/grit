import { BusinessModel } from '../business.model';
import { UserModel } from './user.model';

export interface AccountModel {
  id: number;
  business: BusinessModel;
  person: UserModel;
  baseBusinesAccount: AccountModel;
  following: boolean;
  followingRequest: boolean;
  followingsCount: number;
  followersCount: number;
  followingRequestCount: number;
  role: any;
  deleted: string;

  followers_notification: boolean;
  likes_notification: boolean;
  comments_notification: boolean;
  tags_notification: boolean;

  // this can be calculated and added on frontend
  name?: string;
  pageName?: string;
  photo?: string;
  thumb?: string;
  userName?: string;
  isBlocked?: boolean;
  isBlocking?: boolean;
  followingStatus?: string;
}
