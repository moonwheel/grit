import { AbstractEntityModel } from './abstract-entity.model';

export interface TextModel extends AbstractEntityModel {
  id: number;
  text: string;
  user_: number;
  likes: number[];
  likesCount: number;
  liked: boolean;
  comments: number[];
  commentsCount: number;
  created: string;
  deleted: Date;
  draft: boolean;
}
