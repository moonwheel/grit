import { AddressModel, AccountModel } from 'src/app/shared/models';
import { ProductModel } from './content/products/product.model';

export class OrderModelAppointment {
  created: string;
  end: string;
  id: number;
  start: string;
}

export class OrderService {
  id: number;
  active: boolean;
  bank_id: null | string;
  bookmarksCount: number;
  category: string;
  created: string;
  description: string;
  draft: boolean;
  hourlyPrice: boolean;
  perfomance: string;
  photoCover: any;
  photos: any[];
  price: string;
  quantity: string;
  reviewsCount: number;
  title: string;
  updated: string;
}

export class OrderModel {
  id: number;
  payment_type: string;
  payment_status: string;
  bank: any;
  total_price: number;
  shipping_price: number;
  created: Date;
  updated: Date;
  ordered: Date;
  paid: Date;
  shipped: Date;
  collected: Date;
  sold: boolean;
  who_cancelled: 'buyer' | 'seller';
  cancelled: Date;
  received: Date;
  returned: Date;
  refunded: Date;
  quantity: number;
  collect: false;
  price: number;
  payment: number;
  fee: number;
  reason: string;
  seller: AccountModel;
  buyer: AccountModel;
  amount_wallet: number;
  order_items: OrderItemModel[];
  delivery_address: AddressModel;
  billing_address: AddressModel;
  amount_card?: number;
  return_shipping_cost?: number;
  type: string;
  appointments: OrderModelAppointment[];
  service: OrderService;
  pay_in_date: any;
}

export class OrderItemModel {
  id: number;
  price: number;
  quantity: number;
  collect: boolean;
  payment: any;
  reason: string;
  shipped: Date;
  collected: Date;
  reviewed: Date;
  received: Date;
  refunded: Date;
  created: Date;
  cancel_reason: string;
  cancel_description: string;
  who_cancelled: 'buyer' | 'seller';
  when_cancelled: Date;
  return_reason: string;
  return_description: string;
  updated: Date;
  product: ProductModel;
  returned?: Date;
  cancelled?: Date;
  appointments: any;
  pay_out_refund_date: string;
}
