import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'customCurrency'
})
export class CustomCurrencyPipe extends CurrencyPipe implements PipeTransform {
  transform(value: string) {
    const result = super.transform(value, 'EUR', 'symbol', '1.2-2', 'de-AT');
    return result && result.replace(/\s/, '');
  }
}
