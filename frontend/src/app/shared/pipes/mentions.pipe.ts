import { Pipe, PipeTransform } from '@angular/core';
import { MentionModel } from '../models';
import { Utils } from '../utils/utils';

@Pipe({
  name: 'mentions'
})
export class MentionsPipe implements PipeTransform {
  transform(value: string = '', mentions: MentionModel[]) {

    if (mentions && mentions.length) {
      mentions = [ ...mentions ].sort((prev, cur) => cur.mention_text.length - prev.mention_text.length);
      const regexp = new RegExp(mentions.map(item => `(${item.mention_text})`).join('|'), 'g');
      const matches = [...Utils.matchAll(value, regexp)];

      let offset = 0;

      for (const match of matches) {
        const text = match[0];
        const from = match.index + offset;
        const to = from + text.length;
        const mention = mentions.find(item => item.mention_text === text);
        const pagename = Utils.getAccountPageName(mention.target);
        const str = `<a class="blue" href="/${pagename}/home">${mention.mention_text}</a>`;

        value = value.substring(0, from) + str + value.substring(to);
        offset += str.length - text.length;
      }
    } else if (!mentions) {
      console.warn('No mentions provided for "mentions" pipe');
    }

    return value;
  }
}
