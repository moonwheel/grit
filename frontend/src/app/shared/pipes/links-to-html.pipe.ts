import { Pipe, PipeTransform } from "@angular/core";
import Autolinker from 'autolinker';

@Pipe({name: 'linkstohtml'})
export class LinksToHtmlPipe implements PipeTransform {
    transform(content: string): string {
        if(typeof content !== 'string') return content;

        let processedContent = Autolinker.link(content);

        return processedContent
    }
}