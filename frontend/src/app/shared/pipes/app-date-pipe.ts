import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from "@angular/common";

@Pipe({
    name: 'appDate',
})
export class AppDatePipe extends DatePipe implements PipeTransform {
    public DATE_FORMAT = 'dd MMM yyyy, HH:mm';

    transform(value: any, format = this.DATE_FORMAT, timezone?: string, locale?: string) {
        const formatted = super.transform(value, format, timezone, locale);

        return formatted;
    }
}