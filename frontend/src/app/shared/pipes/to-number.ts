import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toNumber'
})

export class ToNumberPipe implements PipeTransform {
  transform(value: string): any {
    const splitNumber = value.split(',');
    const retNumber = Number(splitNumber.join('.'));
    return isNaN(retNumber) ? 0 : retNumber;
  }
}
