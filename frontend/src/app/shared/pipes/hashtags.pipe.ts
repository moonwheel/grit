import { Pipe, PipeTransform } from '@angular/core';
import { constants } from 'src/app/core/constants/constants';

@Pipe({
  name: 'hashtags'
})
export class HashtagsPipe implements PipeTransform {
  transform(value: string) {
    return value.replace(new RegExp(constants.HASHTAGS_REGEXP_TEXT, 'g'), (match) => {
      const link = encodeURIComponent(match);
      return `<a class="blue" href="/search?type=all&q=${link}">${match}</a>`;
    });
  }
}
