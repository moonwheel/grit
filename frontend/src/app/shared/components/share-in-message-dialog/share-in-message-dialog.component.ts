import { Component, Inject, OnInit, AfterViewInit, ViewChildren, QueryList, NgZone, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar'
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, NgModel } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { filter, map, switchMap, skip, debounceTime, takeUntil } from 'rxjs/operators';
import { ChatActions } from 'src/app/store/actions/chat.actions';
import { ChatState } from 'src/app/store/state/chat.state';
import { Observable, of, EMPTY } from 'rxjs';
import { AccountModel, AbstractEntityType } from '../../models';
import { AbstractInfiniteListComponent } from '../abstract-infinite-list/abstract-infinite-list.component';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';

export interface ShareInMessageDialogComponentData {
  entityId: number;
  entityType: AbstractEntityType;
}

@Component({
  selector: 'app-share-in-message-dialog',
  templateUrl: './share-in-message-dialog.component.html',
  styleUrls: ['./share-in-message-dialog.component.scss'],
})
export class ShareInMessageDialogComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ChatState.searchResults) searchResults$: Observable<AccountModel[]>;
  @Select(ChatState.searchLoading) searchLoading$: Observable<boolean>;
  @Select(UserState.loggedInAccountPhoto) loggedInAccountPhoto$: Observable<string>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;

  total$ = this.loggedInAccount$.pipe(
    filter(item => Boolean(item)),
    map(account => account.followingsCount)
  );

  itemSize = 60;
  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';
  message = '';
  checked: Record<number, boolean> = {};

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  virtualScrollHeight$ = this.createVirtualScrollHeightObservable(
    this.searchResults$,
    this.itemSize,
    3,
    this.zone
  );

  constructor(private dialogRef: MatDialogRef<ShareInMessageDialogComponent>,
    private formBuilder: FormBuilder,
    private store: Store,
    private zone: NgZone,
    @Inject(MAT_DIALOG_DATA)
    private data: ShareInMessageDialogComponentData,
    private snackbar: MatSnackBar,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new ChatActions.SearchUsersForNewChat());
  }

  ngOnDestroy() {
    this.store.dispatch(new ChatActions.SearchUsersForNewChat());
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new ChatActions.SearchUsersForNewChat(this.searchValue, this.page, this.pageSize));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
      switchMap(() => {
        this.page = 1;
        return this.store.dispatch(new ChatActions.SearchUsersForNewChat(this.searchValue, this.page, this.pageSize));
      })
    ).subscribe();
  }

  send() {
    const checked = [];

    for (const key in this.checked) {
      if (this.checked[key]) {
        checked.push(+key);
      }
    }

    if (checked.length) {
      this.store.dispatch(new ChatActions.ShareInMessage(
        checked,
        this.message,
        this.data.entityType,
        this.data.entityId,
      ));
      this.dialogRef.close();
    } else {
      const message = this.translateService.instant('DIALOGS.SHARE_MESSAGE.INVALID');
      this.snackbar.open(message, '', { duration: 3000 });
    }
  }

}
