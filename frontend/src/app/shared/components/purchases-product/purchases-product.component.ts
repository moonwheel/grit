import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ViewChildren, QueryList, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgModel } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SelectionModel } from '@angular/cdk/collections';
import { ProductModel, AccountModel, OrderModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';
import {CancelOrder, ReturnOrder, GetOnePurchase} from 'src/app/store/actions/order.actions';
import { Store } from '@ngxs/store';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { DateTime } from 'luxon';
import { ReviewAddComponent } from '../actions/reviews/review-add/review-add.component';
import { TranslateService } from '@ngx-translate/core';

// So propably you can use just the pay-out popover for this case,
// where the user can select the bank account and can also add another one if he want.
// Also we need to show a small info (e.g. on the Refund popover),
// that not enough money is on his Grit Wallet to refund the money.

@Component({
  selector: 'purchases-product',
  templateUrl: './purchases-product.component.html',
  styleUrls: ['./purchases-product.component.scss']
})

export class PurchasesProductComponent implements OnInit {
  @Input()
  item: OrderModel;

  cancelForm: FormGroup;
  returnForm: FormGroup;

  openedDialog: MatDialogRef<unknown, any>;

  selection = new SelectionModel<OrderModel>(true);

  showShippingCosts = false;

  wrongSelection: number[] = [];

  reasons: string[] = [
    'DONT_LIKE',
    'POOR_SERVICE',
    'PRICE_PERFORMANCE',
    'WRONG_PRODUCT',
    'PRODUCT_DAMAGED',
    'PRODUCT_DEFECTIVE',
    'DELIVERY_INCOMPLETE',
    'DELIVERED_TOO_LATE',
    'OTHER',
  ];

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.cancelForm = this.formBuilder.group({
      reason: ['', Validators.required],
      description: '',
    });

    this.returnForm = this.formBuilder.group({
      reason: ['', Validators.required],
      description: '',
      shippingCosts: '',
    });
  }

  showAddress(data: any) {
    if (!data) {
      return '';
    }
    return [data.street, data.zip, data.city].filter(Boolean).join(', ');
  }

  showVariantData(data: any) {
    if (!data) {
      return '';
    }
    return [data.size, data.color, data.flavor, data.material].filter(Boolean).join(', ');
  }

  isItemsForCanceling() {
    let count = 0;
    this.item.order_items.forEach(item => {
      if (!item.shipped && !item.cancelled && !item.returned) {
        count += 1;
      }
    });
    return this.item.order_items.length >= count && count > 0;
  }

  isItemsForReturning(){
    if (!this.item.seller.business) { return false; }
    let count = 0;
    this.item.order_items.forEach(item => {
      if (item.shipped && !item.returned){
        count += 1;
      }
    });
    return this.item.order_items.length >= count && count > 0;
  }

  isReturnButton() {
    return this.isItemsForReturning();
  }

  isCancelButton() {
    return this.isItemsForCanceling();
  }

  //  action - CANCEL, RETURN
  filterOrderItemsForDialogs(items: any, action: string) {
    switch (action) {
      case 'RETURN':
        return items.filter(item => item.shipped);
      case 'CANCEL':
        return items.filter(item => !item.shipped && !item.cancelled && !item.refunded);
      default: return;
    }
  }

  //  action - CANCEL, RETURN
  openDialog(templateRef, dialogAction: string) {
    switch (dialogAction) {
      case 'RETURN': {
        if (this.isItemsForReturning()) {
          this.openedDialog = this.dialog.open(templateRef, {
            width: '500px',
          });
        } else {
          this.translateService.get('INFO_MESSAGES.PRODUCTS_CANNOT_BE_RETURNED').subscribe(message => {
            this.snackbar.open(message, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
          });
        }
        break;
      }
      case 'CANCEL': {
        if (this.isItemsForCanceling()){
          this.openedDialog = this.dialog.open(templateRef, {
            width: '500px',
          });
        } else {
          this.translateService.get('INFO_MESSAGES.PRODUCTS_CANNOT_BE_CANCELED').subscribe(message => {
            this.snackbar.open(message, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
          });
        }
        break;
      }
      default: return;
    }
  }

  refreshIfComplete() {
    this.store.dispatch(new GetOnePurchase(String(this.item.id), 'product' ));
  }

  cancelProducts() {
    if (this.selection.selected.length){
      this.store.dispatch(
        new CancelOrder(
          this.item.id,
          this.selection.selected.map(item => item.id),
        )
      ).pipe(
        catchError(result => {
          this.snackbar.open(result.error.error, '', {
            duration: 3000,
            panelClass: ['orders-snackbar'],
          });
          return of('');
        })
      ).subscribe(() => {
        this.openedDialog.close();
        this.refreshIfComplete();
      });
    }
  }

  openReturnPaymentDialog(templateRef) {
    if (this.selection.selected.length){
      this.openedDialog.close();
      this.openedDialog = this.dialog.open(templateRef, {
        width: '500px',
      });
    }
  }

  return() {
    if (this.returnForm.valid) {

      this.wrongSelection = [];

      this.selection.selected.forEach(item => {
        if (!item.shipped || item.cancelled || item.returned || item.refunded) {
          this.wrongSelection.push(item.id);
        }
      });

      if (this.wrongSelection.length) {
        this.openedDialog.close();
        return;
      }

      this.openedDialog.close();

      this.store.dispatch(
        new ReturnOrder(
          this.item.id,
          this.selection.selected.map(item => item.id),
          this.returnForm.get('reason').value,
          this.returnForm.get('description').value,
        )
      ).pipe(
        catchError(result => {
          this.snackbar.open(result.error.error, '', {
            duration: 3000,
            panelClass: ['orders-snackbar'],
          });
          return of('');
        })
      ).subscribe(() => {
        this.refreshIfComplete();
      });
    }
  }

  getProductPhoto(product: ProductModel) {
    // if (product.selectedVariant && product.selectedVariant.cover) {
    //   return product.selectedVariant.cover.photo.thumbUrl || product.selectedVariant.cover.photo.photoUrl;
    // } else if (product.photos.length) {
    //   return product.photos[0].thumbUrl;
    // } else {
    //   return constants.PLACEHOLDER_NO_PHOTO_PATH;
    // }

    return product.cover?.thumbUrl || product.cover?.photoUrl || constants.PLACEHOLDER_NO_PHOTO_PATH
  }

  getProductUrl(user: AccountModel, product: ProductModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}/shop/${product.id}`;
  }

  getUserPhoto(user: AccountModel) {
    if (user.business) {
      return user.business.photo || constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
    }
  }

  getUserLink(user: AccountModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}`;
  }

  getTimeDifference(date: any) {
    if (!date) {
      return true;
    }
    const now = DateTime.local();
    return now.diff(DateTime.fromISO(date), 'day').toObject().days < 30;
  }

  getProductStatus(item: any) {
    if (item.cancelled && !item.shipped) {
      return { type: 'CANCELLED', date: this.formatProductDate(item.cancelled) };
    } else if (!item.cancelled && item.shipped && !item.returned) {
      return item.collect
        ? { type: 'COLLECTED', date: this.formatProductDate(item.shipped) }
        : { type: 'SHIPPED', date: this.formatProductDate(item.shipped) };
    } else if (item.cancelled && !item.shipped && !item.refunded) {
      return { type: 'NOT_REFUNDED', date: '' };
    } else if (item.cancelled && !item.shipped && item.refunded) {
      return { type: 'REFUNDED_AUTO', date: this.formatProductDate(item.refunded) };
    } else if (item.cancelled && item.shipped && !item.returned) {
      return { type: 'CANCELLED', date: this.formatProductDate(item.cancelled) };
    } else if (item.returned && !item.refunded && item.shipped) {
      return { type: 'RETURNED', date: this.formatProductDate(item.returned) };
    } else if (item.returned && item.refunded && item.shipped) {
      return { type: 'REFUNDED', date: this.formatProductDate(item.refunded) };
    } else {
      return null;
    }
  }

  isShowInfoIcon(item: any): boolean {
    return item.cancelled && !item.shipped && !item.refunded
      || item.cancelled && item.shipped && !item.refunded
      || item.cancelled && item.shipped && item.returned && !item.refunded;
  }

  formatProductDate(date: string) {
    return DateTime.fromISO(date).toFormat('dd LLL yyyy, t');
  }

  reviewDialog(item: any, entity: string) {
    this.dialog.open(ReviewAddComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: { item, entity },
    });
  }

  clearSelectionError(id: number) {
    this.wrongSelection = this.wrongSelection.filter(item => item !== id);
  }
}
