import {Component, Inject, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SelectionModel } from '@angular/cdk/collections';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { Observable, of } from 'rxjs';

import { OrderModel, ProductModel, AccountModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';
import { ShipOrder, CancelOrder, RefundOrder, GetOneSale } from 'src/app/store/actions/order.actions';
import { catchError } from 'rxjs/operators';
import { DateTime } from 'luxon';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { CardDirectPayIn } from 'src/app/store/actions/payment.actions';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sales-product',
  templateUrl: './sales-product.component.html',
  styleUrls: ['./sales-product.component.scss']
})

export class SalesProductComponent implements OnInit {

  @Select(UserState.loggedInAccountPageName)
  loggedInAccountPageName$: Observable<string>;

  @Select(PaymentsState.balance)
  balance$: Observable<any>;

  @Input()
  item: OrderModel;

  cancelForm: FormGroup;
  payInForm: FormGroup;

  openedDialog: MatDialogRef<unknown, any>;

  selection = new SelectionModel<OrderModel>(true);

  refundShippingCosts = true;

  wrongSelection: number[] = [];

  reasons: string[] = [
    'DONT_LIKE',
    'POOR_SERVICE',
    'PRICE_PERFORMANCE',
    'WRONG_PRODUCT',
    'PRODUCT_DAMAGED',
    'PRODUCT_DEFECTIVE',
    'DELIVERY_INCOMPLETE',
    'DELIVERED_TOO_LATE',
    'OTHER',
  ];
  quantity: any;

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.cancelForm = this.formBuilder.group({
      reason: ['', Validators.required],
      description: '',
    });

    this.payInForm = this.formBuilder.group({
      bank: ['', Validators.required],
      amount: ['', Validators.required],
    });
  }

  refreshIfComplete() {
    this.store.dispatch(new GetOneSale(String(this.item.id), 'product' ));
  }

  showVariantData(data: any) {
    if (!data) {
      return '';
    }
    return [data.size, data.color, data.flavor, data.material].filter(Boolean).join(', ');
  }

  isItemsForCanceling() {
    let count = 0;
    this.item.order_items.forEach(item => {
      if (!item.shipped && !item.cancelled && !item.refunded) {
        count += 1;
      }
    });

    return this.item.order_items.length >= count && count > 0;
  }
  isItemsForShipping(){
    let count = 0;
    this.item.order_items.forEach(item => {
      if (!item.shipped && !item.cancelled && !item.refunded){
        count += 1;
      }
    });
    return this.item.order_items.length >= count && count > 0;
  }

  isItemsForRefunding(){
    let count = 0;
    this.item.order_items.forEach(item => {
      if ((item.cancelled && !item.refunded) || (item.returned && !item.refunded)){
        count += 1;
      }
    });

    return this.item.order_items.length >= count && count > 0;
  }


  counter(count: number) {
    count = +count;
    return Array(count <= 10 ? count : 10);
  }

  showAddress(data: any) {
    if (!data) {
      return '';
    }
    return [data.street, data.zip, data.city].filter(Boolean).join(', ');
  }

  isRefundButton() {
    return this.isItemsForRefunding();
  }

  isCancelButton() {
    return this.isItemsForCanceling();
  }
  isShipButton() {
    return this.isItemsForShipping();
  }

  //  action - SHIP, CANCEL, REFUND
  filterOrderItemsForDialogs(items: any, action: string) {
    switch (action) {
      case 'SHIP':
        return items.filter(item => !item.shipped && !item.cancelled && !item.refunded);
      case 'CANCEL':
        return items.filter(item => !item.shipped && !item.cancelled && !item.refunded);
      case 'REFUND':
        return items.filter(item => (item.cancelled && !item.refunded) || (item.returned && !item.refunded));
      default: return;
    }
  }

  shipProducts() {
    if (this.selection.selected.length){
        this.store.dispatch(
          new ShipOrder(this.item.id, this.selection.selected.map(item => item.id))
        ).pipe(
          catchError(result => {
            this.snackbar.open(result.error.error, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
            return of('');
          })
        ).subscribe(() => {
          this.refreshIfComplete();
          this.openedDialog.close();
        });
    } else {
      this.translateService.get('INFO_MESSAGES.SELECT_PRODUCTS_FOR_SHIPPED').subscribe(message => {
        this.snackbar.open(message, '', {
          duration: 3000,
          panelClass: ['orders-snackbar'],
        });
      });
    }
  }

  //  action - SHIP, CANCEL, REFUND
  openDialog(templateRef, dialogAction: string) {
    switch (dialogAction) {
      case 'SHIP': {
        if (this.isItemsForShipping()) {
          this.openedDialog = this.dialog.open(templateRef, {
            width: '500px',
          });
        } else {
          this.translateService.get('INFO_MESSAGES.PRODUCTS_CANNOT_BE_SHIPPED').subscribe(message => {
            this.snackbar.open(message, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
          });
        }
        break;
      }
      case 'CANCEL': {
        if (this.isItemsForCanceling()){
          this.openedDialog = this.dialog.open(templateRef, {
            width: '500px',
          });
        } else {
          this.translateService.get('INFO_MESSAGES.PRODUCTS_CANNOT_BE_CANCELED').subscribe(message => {
            this.snackbar.open(message, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
          });
        }
        break;
      }
      case 'REFUND': {
        if (this.isItemsForRefunding()){
          this.openedDialog = this.dialog.open(templateRef, {
            width: '500px',
          });
        } else {
          this.translateService.get('INFO_MESSAGES.PRODUCTS_CANNOT_BE_REFUNDED').subscribe(message => {
            this.snackbar.open(message, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
          });
        }
        break;
      }
      default: return;
    }
  }

  openRefundPaymentDialog(templateRef) {
    if (this.selection.selected.length){
      this.openedDialog.close();
      this.openedDialog = this.dialog.open(templateRef, {
        width: '500px',
      });
      this.wrongSelection = [];

      const totalPrice = this.selection.selected
        .reduce((sum, item) => sum += Number(item.price), 0);

      const balance = this.store.selectSnapshot(PaymentsState.balance);
      if (totalPrice > balance) {
        this.openedDialog = this.dialog.open(templateRef, {
          width: '500px',
        });
      } else {
        this.store.dispatch(
          new RefundOrder(this.item.id, this.selection.selected.map(item => item.id))
        ).pipe(
          catchError(result => {
            this.snackbar.open(result.error.error, '', {
              duration: 3000,
              panelClass: ['orders-snackbar'],
            });
            return of('');
          })
        ).subscribe();
      }
    }
  }

  cancelProducts() {
    if (this.selection.selected.length){
      this.store.dispatch(
        new CancelOrder(
          this.item.id,
          this.selection.selected.map(item => item.id),
        )
      ).pipe(
        catchError(result => {
          this.snackbar.open(result.error.error, '', {
            duration: 3000,
            panelClass: ['orders-snackbar'],
          });
          return of('');
        })
      ).subscribe(() => {
        this.openedDialog.close();
        this.refreshIfComplete();
      });
    }
  }

  payInToWallet() {
    if (this.payInForm.valid) {
      this.store.dispatch(
        new CardDirectPayIn(
          this.payInForm.get('bank').value,
          this.payInForm.get('amount').value,
        )
      );
    }
  }
  isAmountWaller(amountOnWallet: number) {
    return parseInt(String(amountOnWallet), 10) !== 0;
  }

  getInfo() {
    this.translateService.get('INFO_MESSAGES.INFO_ABOUT_FEES').subscribe(message => {
      this.snackbar.open(message, '', {
        duration: 3000,
        panelClass: ['orders-snackbar'],
      });
    });
  }

  getPriceForProducts(item: any) {
    return parseInt(item.total_price, 10) - parseInt(item.shipping_price, 10);
  }

  getProductStatus(item: any) {
    if (item.cancelled && !item.shipped && !item.refunded) {
      return { type: 'CANCELLED', date: this.formatProductDate(item.cancelled) };
    } else if (!item.cancelled && item.shipped && !item.returned) {
      return item.collect
        ? { type: 'COLLECTED', date: this.formatProductDate(item.shipped) }
        : { type: 'SHIPPED', date: this.formatProductDate(item.shipped) };
    }
    // else if (item.cancelled && !item.shipped && !item.refunded) {
    //   return { type: 'NOT_REFUNDED', date: '' };
    // }
    else if (item.cancelled && !item.shipped && item.refunded) {
      return { type: 'REFUNDED_AUTO', date: this.formatProductDate(item.refunded) };
    }
    // else if (item.cancelled && item.shipped && !item.returned) {
    //   return { type: 'CANCELLED', date: this.formatProductDate(item.cancelled) };
    // }
    else if (item.returned && !item.refunded && item.shipped) {
      return { type: 'RETURNED', date: this.formatProductDate(item.returned) };
    } else if (item.returned && item.refunded && item.shipped) {
      return { type: 'REFUNDED', date: this.formatProductDate(item.refunded) };
    } else {
      return null;
    }
  }

  getProductsReturnCosts() {
    return this.selection.selected.reduce((sum, item) => {
      return sum + (+item.return_shipping_cost || +item.shipping_price);
    }, 0);
  }

  getProductPhoto(product: ProductModel) {
    // if (product.selectedVariant && product.selectedVariant.cover) {
    //   return product.selectedVariant.cover.photo.thumbUrl || product.selectedVariant.cover.photo.photoUrl;
    // } else if (product.photos.length) {
    //   return product.photos[0].thumbUrl;
    // } else {
    //   return constants.PLACEHOLDER_NO_PHOTO_PATH;
    // }
    return product.cover?.thumbUrl || product.cover?.photoUrl || constants.PLACEHOLDER_NO_PHOTO_PATH
  }

  getProductUrl(user: AccountModel, product: ProductModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}/shop/${product.id}`;
  }

  getUserPhoto(user: AccountModel) {
    if (user.business) {
      return user.business.photo || constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
    }
  }

  isShowInfoIcon(item: any): boolean {
    return item.cancelled && !item.shipped && !item.refunded
      || item.cancelled && item.shipped && !item.refunded
      || item.cancelled && item.shipped && item.returned && !item.refunded;
  }

  formatProductDate(date: string) {
    return DateTime.fromISO(date).toFormat('dd LLL yyyy, t');
  }

  getUserLink(user: AccountModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}`;
  }

  clearSelectionError(id: number) {
    this.wrongSelection = this.wrongSelection.filter(item => item !== id);
  }
}
