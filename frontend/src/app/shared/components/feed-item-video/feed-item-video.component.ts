import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { VideoModel } from 'src/app/shared/models';
import { VideoActions } from 'src/app/store/actions/video.actions';
import { VideoState } from 'src/app/store/state/video.state';
import { AbstractFeedItemComponent } from 'src/app/shared/components/abstract-feed-item/abstract-feed-item.component';
import { environment } from 'src/environments/environment';
import { UserState } from 'src/app/store/state/user.state';

@Component({
  selector: 'app-feed-item-video',
  templateUrl: './feed-item-video.component.html',
  styleUrls: [ './feed-item-video.component.scss' ],
})
export class FeedItemVideoComponent extends AbstractFeedItemComponent implements OnInit {

  @Select(UserState.loggedInAccountId)
  loggedInAccountId$: Observable<number>;

  @Input() item: VideoModel;

  @Input() autoplay = false;
  @Input() muted = true;
  @Input() playOnHover = true;
  @Input() truncatable = false;
  @Input() borderBottom = true;
  @Input() timeAgoFormat = false;

  Actions = VideoActions;
  State = VideoState;

  currentUrl = '';

  constructor(protected store: Store,
              protected dialog: MatDialog,
  ) {
    super(dialog, store);
  }

  ngOnInit() {
    this.currentUrl = environment.appUrl;
  }
}
