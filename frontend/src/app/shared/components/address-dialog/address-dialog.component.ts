import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddressModel } from '../../models';
import { EditAddress, AddAddress } from 'src/app/store/actions/address.actions';
import { Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { filter, take } from 'rxjs/operators';

export interface AddressDialogComponentData {
  address: AddressModel;
}


@Component({
  selector: 'app-address-dialog',
  templateUrl: './address-dialog.component.html'
})

export class AddressDialogComponent implements OnInit {

  addressForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddressDialogComponent>,
              private formBuilder: FormBuilder,
              private store: Store,
              @Inject(MAT_DIALOG_DATA)
              private data: AddressDialogComponentData,
  ) { }

  ngOnInit() {
    this.addressForm = this.formBuilder.group({
      id: null,
      type: '',
      fullName: ['', Validators.required],
      street: ['', Validators.required],
      appendix: '',
      zip: ['', Validators.required],
      city: ['', Validators.required]
    });

    if (this.data && this.data.address) {
      this.addressForm.patchValue(this.data.address);
    } else {
      const account = this.store.selectSnapshot(UserState.loggedInAccount);
      const fullName = account.business ? account.business.name : account.person.fullName;

      this.addressForm.patchValue({ fullName });
    }
  }

  saveAddress() {
    const formValue = this.addressForm.value;

    if(!this.addressForm.valid) {
      this.addressForm.markAllAsTouched();
      return;
    }

    if (this.data && this.data.address) {
      this.store.dispatch(new EditAddress(formValue)).subscribe(() => {
        this.dialogRef.close();
      });
    } else {
      this.store.dispatch(new AddAddress(formValue)).subscribe(() => {
        this.dialogRef.close(true);
      });
    }
  }
}
