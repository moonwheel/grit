import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from "@angular/core";
import memo from 'memoizee';

const PARAGRAPH_SELECTOR = 'p';
const CHARACTER_LIMIT_IN_ARTICLE_PREVIEW = 150;

@Component({
    selector: 'app-editor-output-view',
    templateUrl: './editor-output-view.component.html',
    styleUrls: ['./editor-output-view.component.scss']
})
export class EditorOutputView {
    private _content: string[] | string;
    private shouldRenderMoreBtn: boolean = false

    @Input('expanded') expanded: boolean = false;
    @Input('rowLimit') rowLimit: number = 5;
    @Output('onExpandClick') onExpandClick = new EventEmitter();

    @Input('content') set content(value: string) {
        this._content = value;
    }

    
    get content() {
        if((this.isContentExceedRowLimit(this._content) || this.isContentExceedAllowedCharLength(this._content)) && !this.expanded) {
            this.shouldRenderMoreBtn = true;

            const limitedContent = this.limitContentToRowLimit(this._content, this.rowLimit);
            
            const content = limitedContent.map(item => item.outerHTML);
            return content;
        }

        this.shouldRenderMoreBtn = false;
        return [this._content] as any;
    }

    isContentExceedAllowedCharLength = memo((content: string) => {
        const wrapper = this.createDOMWrapper();
        wrapper.innerHTML = content;

        return wrapper.innerText.length > CHARACTER_LIMIT_IN_ARTICLE_PREVIEW;
    })

    isContentExceedRowLimit = memo((content: string): boolean => {
        const wrapper = this.createDOMWrapper();
        wrapper.innerHTML = content;

        const wraps = wrapper.querySelectorAll(PARAGRAPH_SELECTOR); // might be replaced with custom class name in future;

        return wraps.length > this.rowLimit;
    });

    limitContentToRowLimit = memo((content: string, rowLimit: number) => {
        let limit = rowLimit;
        const wrapper = this.createDOMWrapper();
        wrapper.innerHTML = content;

        let characterLenEntries = 0;

        const rows = wrapper.querySelectorAll(PARAGRAPH_SELECTOR);

        for(let i = 0; i < rows.length; i++) {
            const row = rows[i];

            characterLenEntries+= row.innerText.length;

            if(characterLenEntries > CHARACTER_LIMIT_IN_ARTICLE_PREVIEW) {
                limit = i + 1;
                const limitDiff = characterLenEntries - row.innerText.length;
                const sliceAmount = CHARACTER_LIMIT_IN_ARTICLE_PREVIEW - limitDiff;
                row.innerText = row.innerText.slice(0, sliceAmount);
                break;
            }
        }

        const elements = Array.from(rows).slice(0, limit);

        return elements;
    })

    private createDOMWrapper = () => document.createElement('div');
}