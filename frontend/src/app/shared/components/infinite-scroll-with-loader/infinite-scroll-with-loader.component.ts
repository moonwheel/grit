import { Component, Input, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

@Component({
    selector: 'app-infinite-scroll-with-loader',
    templateUrl: './infinite-scroll-with-loader.component.html',
    styleUrls: ['./infinite-scroll-with-loader.component.scss']
})
export class InfiniteScrollWithLoader {
    @Input('infiniteScrollDistance') infiniteScrollDistance: number = 2;
    @Input('infiniteScrollThrottle') infiniteScrollThrottle: number = 150;
    @Input('infiniteScrollUpDistance') infiniteScrollUpDistance: number = 2;
    @Input('showLoading') showLoading: boolean = false;
    @Input('spinnerPosition') spinnerPosition: 'top' | 'bottom' | 'replace-content' = 'bottom';
    @Input('spinnerContainerClass') spinnerContainerClass: string = ''

    @Output('scrolled') scrolled = new EventEmitter();
    @Output('scrolledUp') scrolledUp = new EventEmitter();
}