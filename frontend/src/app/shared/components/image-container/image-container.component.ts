import { Component, OnInit, Input, ChangeDetectionStrategy, ViewChild, ElementRef, ChangeDetectorRef, HostBinding } from '@angular/core';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: [ './image-container.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageContainerComponent implements OnInit {
  @ViewChild('img', { static: true }) img: ElementRef<HTMLImageElement>;
  @HostBinding('class.preloading') preloading = true;

  @Input() defaultUrl: string = constants.PLACEHOLDER_NO_PHOTO_PATH;
  @Input() additionalClass: string;

  @Input() set src(value: string) {
    if (!value || value === 'null') {
      value = this.defaultUrl;
    }

    this.img.nativeElement.src = value;

    // If image is already in cache - no need to wait until loading
    if (this.img.nativeElement.complete) {
      this.preloading = false;
      this.cdr.detectChanges();
      return;
    }

    this.img.nativeElement.onload = () => {
      this.preloading = false;
      this.cdr.detectChanges();
    };
  }

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }

}
