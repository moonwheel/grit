import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IClipboardResponse } from 'ngx-clipboard';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'copy-link-item',
  templateUrl: './copy-link-item.component.html',
  styleUrls: ['./copy-link-item.component.scss']
})
export class CopyLinkItemComponent {

  @Input()
  link: string;

  @Output()
  handleClick = new EventEmitter();

  constructor(
    private snackbar: MatSnackBar,
    private translateService: TranslateService,
  ) { }

  copyLink(event: IClipboardResponse) {
    if (event.isSuccess) {
      this.translateService.get('INFO_MESSAGES.LINK_COPIED').subscribe(message => {
        this.snackbar.open(message, '', {
          duration: 3000,
          panelClass: ['copied-snackbar'],
        });
      });
    }
    this.handleClick.emit();
  }
}
