
import {
  throttleTime,
  map,
  filter,
  takeUntil,
  switchMapTo,
  distinctUntilKeyChanged,
  take,
  switchMap,
  startWith,
  tap,
} from 'rxjs/operators';
import { Observable, Subject, EMPTY, observable, of } from 'rxjs';
import { ViewChild, OnDestroy, ViewChildren, QueryList, NgZone, Directive, OnInit } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Utils } from '../../utils/utils';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';

@Directive()
export abstract class AbstractInfiniteListComponent implements OnDestroy {

  @ViewChildren(CdkVirtualScrollViewport) viewportQuery: QueryList<CdkVirtualScrollViewport>;

  infiniteScroll$ = new Subject();

  trackById = Utils.trackById;
  get viewport(): CdkVirtualScrollViewport {
    return this.viewportQuery && this.viewportQuery.first;
  }

  abstract total$: Observable<number>;

  abstract page = 1;
  abstract pageSize = 15;
  filter = 'date';
  searchValue = '';

  protected destroy$ = new Subject();

  constructor() {
  }

  /**
   * If you implement OnDestroy interface in child class
   * you should call super.ngOnDestroy() in child class ngOnDestroy method
   */
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * emits scroll event to infinite scroll observer
   * e.g <div infiniteScroll (scrolled)="triggerScroll">
   */

  triggerScroll() {
    this.infiniteScroll$.next();
  }

  /**
   * Components which use ngx-infinite-scroll should subscribe to this method.
   */

  infiniteScrollScrolled(): Observable<any> {
    return this.infiniteScroll$.pipe(
      throttleTime(500),
      switchMap(() => {
      this.page++;

      return of(null);
    }), takeUntil(this.destroy$))
  }

  protected scrolled(): Observable<any> {
    return this.viewportQuery.changes.pipe(
      switchMap(() => {
        if (this.viewport) {
          return this.viewport.scrolledIndexChange.pipe(
            throttleTime(200),
            switchMapTo(this.total$.pipe(take(1))),
            map(total => {
              const end = this.viewport.getRenderedRange().end;
              const displayed = this.viewport.getDataLength();
              return { total, displayed, end };
            }),
            filter(({ total, displayed, end }) => displayed < total && end === displayed && end > 0),
            distinctUntilKeyChanged('end'),
            map(() => this.page++),
            takeUntil(this.destroy$),
          );
        } else {
          return this.infiniteScrollScrolled();
        }
      })
    );
  }

  protected createVirtualScrollHeightObservable(
    items$: Observable<any[]>,
    itemSize: number,
    maxItems: number,
    zone: NgZone,
    defaultValue = '0px',
    additional = 0
  ) {
    return items$.pipe(
      map(items => {
        const max = maxItems * itemSize;
        const current = items.length * itemSize + additional;
        const result = `${current > max ? max : current}px`;
        return result;
      }),
      startWith(defaultValue),
      tap(() => {
        zone.run(() => {
          setTimeout(() => {
            if (this.viewport) {
              this.viewport.checkViewportSize();
            }
          });
        });
      }),
    );
  }
}
