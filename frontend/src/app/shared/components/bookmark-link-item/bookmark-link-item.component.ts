import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngxs/store';

import { AddBookmark, DeleteBookmark } from 'src/app/store/actions/bookmark.actions';

@Component({
  selector: 'bookmark-link-item',
  templateUrl: './bookmark-link-item.component.html',
  styleUrls: ['./bookmark-link-item.component.scss']
})

export class BookmarkLinkItemComponent {

  @Input()
  id: number;

  @Input()
  type: string;

  @Input()
  isBookmark: boolean = false;

  @Output()
  handleClick = new EventEmitter();

  constructor(private store: Store) { }

  bookmark() {
    if (this.isBookmark) {
      this.removeFromBookmark();
    } else {
      this.addToBookmark();
    }
    this.handleClick.emit();
  }

  addToBookmark() {
    this.store.dispatch(new AddBookmark({
      id: this.id,
      type: this.type,
    }));
  }

  removeFromBookmark() {
    this.store.dispatch(new DeleteBookmark({
      id: this.id,
      type: this.type,
    }));
  }
}
