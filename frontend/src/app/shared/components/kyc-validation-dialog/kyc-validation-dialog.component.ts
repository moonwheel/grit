import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ValidateKycDocument } from 'src/app/store/actions/user.actions';
import { UserState } from 'src/app/store/state/user.state';
import { PhotoModel } from 'src/app/shared/models';
import { Utils } from 'src/app/shared/utils/utils';
import { constants } from 'src/app/core/constants/constants';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-kyc-validation-dialog',
  templateUrl: './kyc-validation-dialog.component.html',
})

export class KycValidationDialogComponent implements OnInit {

  @Select(UserState.loggedInAccountId)
  loggedInAccountId$: Observable<number>;

  photo$: Observable<PhotoModel>;
  origin = 'normal';
  localPhotoLoading = false;
  photoFile: File;

  kycPersonalForm: FormGroup;
  kycBusinessForm: FormGroup;

  accountTypes = [{
    value: 'personal',
    label: 'PERSONAL',
  }, {
    value: 'business',
    label: 'BUSINESS',
  }];
  selectedType = 'personal';

  months = [ ...constants.MONTHS ];
  maxDate = 31;
  maxYear = DateTime.local().year;
  minYear = DateTime.local().minus( {years: 100} ).year;

  get photoData() {
    const control = this.kycPersonalForm.get('photo');
    return control && control.value;
  }

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<KycValidationDialogComponent>,
  ) { }

  ngOnInit() {
    this.kycPersonalForm = this.fb.group({
      photo: ['', Validators.required],
      email: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      countryOfResidence: ['', Validators.required],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      nationality: ['', Validators.required],
    });

    this.kycBusinessForm = this.fb.group({
      photo: ['', Validators.required],
      headquartersAddress: ['', Validators.required],
      legalRepresentativeEmail: ['', Validators.required],
      legalRepresentativeAddress: ['', Validators.required],
      companyNumber: ['', Validators.required],
    });
  }

  submit() {
    if (this.kycPersonalForm.valid) {
      const result = this.kycPersonalForm.get('photo').value.split(',')[1];
      this.store.dispatch(new ValidateKycDocument(result));
      this.dialogRef.close();
    }
  }

  selectPhoto(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target && target.files && target.files[0]) {
      this.readFile(target.files[0]);
      target.value = null;
    }
  }

  removePhoto(event: Event) {
    event.stopPropagation();
    this.kycPersonalForm.get('photo').setValue(null);
    this.origin = 'normal';
  }

  readFile(file: File) {
    this.localPhotoLoading = true;
    this.photoFile = file;

    Utils.getImageData(file).then(({ url }) => {
      this.kycPersonalForm.get('photo').setValue(url);
      this.localPhotoLoading = false;
    });
  }

  onKey(event) {
    const value = event;
    if (value >= this.maxYear && value >= 1000) {
      this.kycPersonalForm.get('year').setValue(this.maxYear, {
        emitEvent: false,
        emitViewToModelChange: false
      });
    } else if (value <= this.minYear && value >= 1000) {
      this.kycPersonalForm.get('year').setValue(this.minYear, {
        emitEvent: false,
        emitViewToModelChange: false
      });
    }
  }
}
