import { Component, Input, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'app-location-button',
  templateUrl: './location-button.component.html',
  styleUrls: ['./location-button.component.scss']
})
export class LocationButtonComponent {

  @Input() location = '';
  @Input() businessAddress = null;

  constructor(private router: Router,
  ) { }

  @HostListener('click')
  onClick() {
    if (this.location) {
      let query = '';

      if (this.businessAddress) {
        query = Utils.getAddressString(this.businessAddress);
      } else {
        query = this.location;
      }

      this.router.navigate(['/search'], {
        queryParams: {
          q: query,
          type: 'all',
          location: true,
        }
      });
    }
  }

}
