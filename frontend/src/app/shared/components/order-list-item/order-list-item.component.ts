import { Component, Input, OnInit, EventEmitter, Output } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

export const SHIPPED_STATUS = 'shipped';
export const REFUNDED_STATUS = 'refunded';
export const CANCELLED_STATUS = 'cancelled';
export const RETURNED_STATUS = 'returned';
export const APPOINTMENT_STATUS = 'appointment';
export const NOT_SHIPPED_STATUS = 'not-shipped';

const buildTranslationString = (value: string) => {
    return `GENERAL.PRODUCT_STATUSES.${value}`;
}

const STATUS_TO_TRANSLATION_MAP = {
    [CANCELLED_STATUS]: buildTranslationString('CANCELLED'),
    [REFUNDED_STATUS]: buildTranslationString('REFUNDED'),
    [SHIPPED_STATUS]: buildTranslationString('SHIPPED'),
    [APPOINTMENT_STATUS]: buildTranslationString('APPOINTMENT'),
    [NOT_SHIPPED_STATUS]: buildTranslationString('NOT_SHIPPED')
}

@Component({
    selector: 'app-order-list-item',
    styleUrls: ['./order-list-item.component.scss'],
    templateUrl: './order-list-item.component.html',
})
export class OrderListItemComponent implements OnInit {
    @Input('title') title: string = '';
    @Input('photoUrl') photoUrl: string = '';
    @Input('status') status: string = '';
    @Input('statusDate') statusDate: string = '';
    @Input('orderUrl') orderUrl;
    @Input('price') price: string;

    @Output('clicked') clicked = new EventEmitter();

    statusTranslation = '';

    constructor(private translateService: TranslateService) {

    }

    ngOnInit() {
        this.translateService.get(this.statusToTranslationMap(this.status), { date: this.statusDate }).subscribe(translation => {
            this.statusTranslation = translation;
        })
    }

    statusToTranslationMap = (status: string): string => {
        return STATUS_TO_TRANSLATION_MAP[status];
    }
}