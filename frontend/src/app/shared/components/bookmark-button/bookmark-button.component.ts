import { Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';

import { AddBookmark, DeleteBookmark } from 'src/app/store/actions/bookmark.actions';

@Component({
  selector: 'bookmark-button',
  templateUrl: './bookmark-button.component.html',
  styleUrls: ['./bookmark-button.component.scss']
})

export class BookmarkButtonComponent {

  @Input()
  id: number;

  @Input()
  type: string;

  @Input()
  isBookmark = false;

  @Input()
  bookmarksCount = 0;

  constructor(private store: Store) { }

  bookmark() {
    if (this.isBookmark) {
      this.removeFromBookmark();
    } else {
      this.addToBookmark();
    }
  }

  addToBookmark() {
    this.store.dispatch(new AddBookmark({
      id: this.id,
      type: this.type,
    }));
  }

  removeFromBookmark() {
    this.store.dispatch(new DeleteBookmark({
      id: this.id,
      type: this.type,
    }));
  }
}
