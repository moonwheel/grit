import { Component, OnInit, forwardRef, Input, TemplateRef, Optional, Injector } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AddressModel } from '../../models';
import { DeleteAddress, LoadAddresses } from 'src/app/store/actions/address.actions';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddressDialogComponent } from '../address-dialog/address-dialog.component';
import { SatPopover } from '@ncstate/sat-popover';
import { AddressState } from 'src/app/store/state/address.state';
import { CustomErrorStateMatcher } from '../../utils/custom-error-state-matcher';

@Component({
  selector: 'app-address-input',
  templateUrl: './address-input.component.html',
  styleUrls: [ './address-input.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AddressInputComponent),
      multi: true,
    }
  ],
})
export class AddressInputComponent implements OnInit, ControlValueAccessor {

  @Input()
  placeholder: string;

  @Input()
  required = false;

  @Input()
  type = 'delivery';

  @Select(AddressState.addresses)
  addresses$: Observable<AddressModel[]>;

  value: number;
  disabled = false;
  control: NgControl;
  matcher: ErrorStateMatcher;

  propagateChange: (arg: any) => void;
  propagateTouched: (arg: any) => void;

  get selectedAddress(): string {
    const address = this.store.selectSnapshot(AddressState.address(this.value));
    const appendix = address && address.appendix ? ` ${address.appendix},` : '';
    return address && `${address.fullName}, ${address.street},${appendix} ${address.zip} ${address.city}`;
  }

  get errors() {
    return this.control && this.control.errors;
  }

  constructor(private dialog: MatDialog,
              private store: Store,
              @Optional()
              private injector: Injector,
  ) { }

  ngOnInit() {
    this.control = this.injector.get(NgControl);
    this.matcher = new CustomErrorStateMatcher(this.control);
    this.store.dispatch(new LoadAddresses());
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  openAddAddressDialog(event: Event) {
    event.stopPropagation();

    this.dialog.open(AddressDialogComponent).afterClosed().subscribe((success) => {
      if (success) {
        const addresses = this.store.selectSnapshot(AddressState.addresses);

        const newAddress = addresses.reduce((prev, curr) => {
          const prevCreated = new Date(prev.created).getTime();
          const currCreated = new Date(curr.created).getTime();
          return prevCreated < currCreated ? curr : prev;
        }, addresses[0]);

        this.writeValue(newAddress.id);
        this.propagateChange(newAddress.id);
      }
    });
  }

  openEditAddressDialog(address: AddressModel, popover: SatPopover) {
    this.dialog.open(AddressDialogComponent, {
      data: {
        address,
      },
      disableClose: true,
    });
    popover.close();
  }

  openAddressMorePopover(popover: SatPopover, event: Event) {
    popover.toggle();
    event.stopPropagation();
  }

  deleteAddress(id: number, popover: SatPopover) {
    this.store.dispatch(new DeleteAddress(id)).subscribe(() => {
      this.writeValue(null);
      this.propagateChange(null);
      popover.close();
    });
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }
}
