import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { UnfollowDialogComponent } from '../actions/follow/unfollow-dialog/unfollow-dialog.component';
import { Observable } from 'rxjs';

export abstract class AbstractDetailPageComponent {
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.isFolowing) isFolowing$: Observable<boolean>;

  constructor(protected dialog: MatDialog,
              protected store: Store,
  ) {
  }

  follow() {
    const id = this.store.selectSnapshot(UserState.viewableAccountId);
    this.store.dispatch(new FollowUser(id));
  }

  unfollow() {
    const account = this.store.selectSnapshot(UserState.viewableAccount);
    this.dialog.open(UnfollowDialogComponent, { data: { account } });
  }
}
