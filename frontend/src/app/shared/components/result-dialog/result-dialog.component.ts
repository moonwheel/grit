import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ResultDialogComponentData {
  message: string;
}

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html'
})

export class ResultDialogComponent implements OnInit {

  message = '';

  constructor(
    private dialogRef: MatDialogRef<ResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private config: ResultDialogComponentData,
  ) { }

  ngOnInit() {
    if (this.config) {
      if (this.config.message) {
        this.message = this.config.message;
      }
    }
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }
}
