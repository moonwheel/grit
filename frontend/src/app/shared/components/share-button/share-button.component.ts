import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Share, Device } from '@capacitor/core';
import 'share-api-polyfill';

import { ShareState } from '../../../store/state/share.state';
import { TranslateService } from '@ngx-translate/core';
import { ShareInMessageDialogComponent } from '../share-in-message-dialog/share-in-message-dialog.component';

@Component({
  selector: 'share-button',
  templateUrl: './share-button.component.html',
  styleUrls: ['./share-button.component.scss']
})
export class ShareButtonComponent {

  @Input()
  externalUrl = '';

  @Input()
  entityType: string = null;

  @Input()
  entityId: number = null;

  constructor(
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  async share() {
    try {
      const deviceInfo = await Device.getInfo();
      const translation = await this.translateService.get('DIALOGS.SHARE').toPromise();
      if (deviceInfo.platform === 'web') {
        await (navigator as any).share({
          title: translation.TITLE,
          text: translation.TEXT,
          url: this.externalUrl,
        }, {
          print: false,
          sms: false,
        });
      } else {
        await Share.share({
          text: `${translation.TEXT} ${this.externalUrl}`,
          url: this.externalUrl,
          dialogTitle: translation.TITLE
        });
      }
    } catch (e) {
      console.warn('Share error: ', e);
    }
  }

  openMessageDialog() {
    this.dialog.open(ShareInMessageDialogComponent, {
      disableClose: true,
      panelClass: ['full-screen-dialog'],
      data: {
        entityId: this.entityId,
        entityType: this.entityType,
      },
    });
  }
}
