import { Component, OnInit, Inject, AfterViewInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, NgModel } from '@angular/forms';
import { BankAccountModel, CreditCardModel } from '../../models';
import { EditBankAccount, AddBankAccount, AddCreditCard } from 'src/app/store/actions/payment.actions';
import { Store, Selector, Select } from '@ngxs/store';
import { Subject, Observable } from 'rxjs';
import { PaymentsState } from 'src/app/store/state/payment.state';

export interface BankDialogComponentData {
  model: BankAccountModel | CreditCardModel;
  type: string;
}

export type CreditCardType = 'visa' | 'mastercard' | 'none' | '';

@Component({
  selector: 'app-bank-dialog',
  templateUrl: './bank-dialog.component.html',
  styleUrls: ['./bank-dialog.component.scss']
})

export class BankDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(PaymentsState.loading)
  loading$: Observable<boolean>;

  protected destroy$ = new Subject();

  paymentTypes = [{
    value: 'bank_account',
    label: 'Bank Account',
  }, {
    value: 'credit_card',
    label: 'Credit/Debit Card',
  }];

  selectedType = 'bank_account';

  currentCardType: CreditCardType = '';

  ibanRegexPattern = '[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}';
  cardNumberRegexPattern = '(5[1-5][0-9]{14}$|^2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|'
    + '7(?:[01][0-9]|20))[0-9]{12}|4[0-9]{12}(?:[0-9]{3})?)';
  monthRegexValidator = '^(0?[1-9]|1[012])$';
  yearRegexValidator = '^[2-9]{1}[0-9]{1}$';
  cvcRegexValidator = '^[0-9]{3}$';

  bankForm: FormGroup;

  creditCardForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<BankDialogComponent>,
    private formBuilder: FormBuilder,
    private store: Store,
    @Inject(MAT_DIALOG_DATA)
    private data: BankDialogComponentData,
  ) { }

  ngOnInit() {
    this.bankForm = this.formBuilder.group({
      name: ['', Validators.required],
      iban: ['', Validators.required],
      created: new Date(),
    });

    this.creditCardForm = this.formBuilder.group({
      holder: ['', Validators.required],
      number: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      cvc: ['', Validators.required],
    });

    if (this.data && this.data.model) {
      if (this.data.type === 'bank_account') {
        this.selectedType = 'bank_account';
        this.bankForm.patchValue(this.data.model);
      } else if (this.data.type === 'credit_card') {
        this.selectedType = 'credit_card';
        this.creditCardForm.patchValue(this.data.model);
      }
    }
  }

  ngAfterViewInit() {
    this.creditCardForm.get('number').valueChanges.subscribe(val => {
      this.currentCardType = this.checkCreditCardType(val);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  checkCreditCardType(value: string): CreditCardType {
    const visaRegex = /^4/;
    const mastercardRegex = /^5[1-5]/;

    if (visaRegex.test(value)) {
      return 'visa';
    } else if (mastercardRegex.test(value)) {
      return 'mastercard';
    } else if (value !== '') {
      return 'none';
    } else {
      return '';
    }
  }

  saveBankAccount() {
    const formValue = this.bankForm.value;

    if (!this.bankForm.valid) {
      Object.values(this.bankForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }

    if (this.data && this.data.model) {
      this.store.dispatch(new EditBankAccount(formValue));
    } else {
      this.store.dispatch(new AddBankAccount(formValue));
    }

    this.loading$.subscribe(isLoading => {
      if (!isLoading) {
        this.dialogRef.close(true);
      }
    });
  }

  saveCreditCard() {
    if (this.creditCardForm.valid) {
      const formValue = this.creditCardForm.value;
      this.store.dispatch(new AddCreditCard(formValue));
    } else {
      Object.values(this.creditCardForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    this.loading$.subscribe(isLoading => {
      if (!isLoading) {
        this.dialogRef.close(true);
      }
    });
  }

  sendForm() {
    if (this.selectedType === 'credit_card') {
      this.saveCreditCard();
    } else if (this.selectedType === 'bank_account') {
      this.saveBankAccount();
    }
  }
}
