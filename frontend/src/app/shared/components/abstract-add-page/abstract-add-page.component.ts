import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { switchMap, takeUntil } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/core/guards';
import { SaveDialogComponent } from 'src/app/shared/components/save-dialog/save-dialog.component';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { TemplateRef } from '@angular/core';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { Utils } from '../../utils/utils';

export abstract class AbstractAddPageComponent implements CanComponentDeactivate {

  isEditFlow: boolean = false;
  isDraft: boolean = false;
  canImmediatelyDeactivate: boolean = false;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;
  isEditReady: boolean = false;
  editMode: boolean = false;

  saveDraftQuestion: string;
  saveChangesQuestion: string;

  protected destroy$ = new Subject();

  abstract form: FormGroup;

  constructor(protected router: Router,
              protected route: ActivatedRoute,
              protected dialog: MatDialog,
              protected navigationService: NavigationService,
              protected translateService: TranslateService,
  ) {
    this.route.queryParams.subscribe(() => {
      this.canImmediatelyDeactivate = false;
    });

    this.translateService.stream('DIALOGS.SAVE').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.saveDraftQuestion = res.TITLE_DRAFT;
      this.saveChangesQuestion = res.TITLE_CHANGES;
    });
  }

  abstract wasChanged(): boolean;
  abstract save(arg?: any): Observable<boolean>;

  canDeactivate() {
    if (this.wasChanged()) {
      let text = '';

      if (this.isEditFlow) {
        text = this.isDraft ? this.saveDraftQuestion : this.saveChangesQuestion;
      } else {
        text = this.saveDraftQuestion;
      }

      return this.dialog.open(SaveDialogComponent, {
        data: { text }
      }).afterClosed().pipe(
        switchMap(result => {
          if (result) {
            return this.save(this.isEditFlow ? this.isDraft : true);
          } else if (result === false) {
            return of(true);
          } else {
            this.navigationService.makeSafeBackNavigationAfterCanDeactivateGuard();
            return of(false);
          }
        })
      );
    } else {
      return of(true);
    }
  }

  back(): void {
    if (this.isEditFlow && !this.isDraft && this.form.invalid) {
      Utils.touchForm(this.form);
      return;
    }
    this.navigationService.back();
  }

  saveAndBack(arg?: any) {
    this.save(arg).subscribe(saved => {
      if (saved) {
        this.navigationService.backAvoidHomeAddPage();
      }
    });
  }

  selectDraft(id: number) {
    const currentId = this.route.snapshot.queryParams.id;
    if (+currentId !== +id) {
      this.canDeactivate().subscribe(() => {
        this.router.navigate(['../add'], {
          relativeTo: this.route,
          queryParams: { id }
        });
      });
    }
  }

  saveDraft() {
    const text = this.saveDraftQuestion;
    this.dialog.open(SaveDialogComponent, {
      data: { text }
    }).afterClosed().subscribe(yes => {
      if (yes) {
        this.saveAndBack(true);
      }
    });
  }

  openDialog(tempRef: TemplateRef<any>) {
    this.dialog.open(tempRef);
  }

  setEditReadyState(state: boolean) {
    this.isEditReady = state;
  }

  setEditViewMode(isEditMode: boolean) {
    this.editMode = isEditMode;
  }
}
