import { Component, OnInit, Input } from '@angular/core';
import { TextModel } from '../../models';
import { Store } from '@ngxs/store';
import { TextActions } from 'src/app/store/actions/text.actions';
import { TextState } from 'src/app/store/state/text.state';
import { MatDialog } from '@angular/material/dialog';
import { AbstractFeedItemComponent } from '../abstract-feed-item/abstract-feed-item.component';
import { environment } from 'src/environments/environment';
import { constants } from 'src/app/core/constants/constants';
import { LinksToHtmlPipe } from '../../pipes/links-to-html.pipe';

@Component({
  selector: 'app-feed-item-text',
  templateUrl: './feed-item-text.component.html',
  styleUrls: [ './feed-item-text.component.scss' ],
})
export class FeedItemTextComponent extends AbstractFeedItemComponent implements OnInit {

  @Input() item: TextModel;
  @Input() truncatable = false;

  Actions = TextActions;
  State = TextState;

  currentUrl = '';

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  constructor(protected store: Store,
              protected dialog: MatDialog,
  ) {
    super(dialog, store);
  }

  ngOnInit() {
    this.currentUrl = environment.appUrl;
  }

}
