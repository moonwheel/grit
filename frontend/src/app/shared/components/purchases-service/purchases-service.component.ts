import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgModel } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccountModel, ServiceModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';
import { Store } from '@ngxs/store';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CancelBooking } from 'src/app/store/actions/booking.actions';
import { DateTime } from 'luxon';
import { ReviewAddComponent } from '../actions/reviews/review-add/review-add.component';

@Component({
  selector: 'purchases-service',
  templateUrl: './purchases-service.component.html',
  styleUrls: ['./purchases-service.component.scss']
})

export class PurchasesServiceComponent implements OnInit {

  @Input()
  item: any;

  cancelForm: FormGroup;

  openedDialog: MatDialogRef<unknown, any>;

  reasons: string[] = [
    'DONT_LIKE',
    'POOR_SERVICE',
    'PRICE_PERFORMANCE',
    'OTHER',
  ];

  // TODO: Denys, this was undefined variable used in template
  // i decided to make it false by default but you need to check it
  notRefunded = false;

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.cancelForm = this.formBuilder.group({
      reason: ['', Validators.required],
      description: '',
    });
  }

  cancel() {
    this.store.dispatch(
      new CancelBooking(
        this.item.id,
      )
    ).pipe(
      catchError(result => {
        this.snackbar.open(result.error.error, '', {
          duration: 3000,
          panelClass: ['orders-snackbar'],
        });
        return of('');
      })
    ).subscribe(() => {
      this.openedDialog.close();
    });
  }

  cancelBooking(templateRef) {
    this.openedDialog = this.dialog.open(templateRef, {});
  }

  showAddress(data: any) {
    if (!data) {
      return '';
    }
    return [data.street, data.zip, data.city].filter(Boolean).join(', ');
  }

  getUserPhoto(user: AccountModel) {
    if (user.business) {
      return user.business.photo || constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
    }
  }

  getUserLink(user: AccountModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}`;
  }

  getServiceUrl(user: AccountModel, service: ServiceModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}/services/${service.id}`;
  }

  getTimeDifference(date: string) {
    if (!date) {
      return true;
    }
    const now = DateTime.local();
    return now.diff(DateTime.fromISO(date), 'hour').toObject().hours < 12;
  }

  reviewDialog(item: any, entity: string) {
    this.dialog.open(ReviewAddComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: { item, entity },
    });
  }

  showRefunded() {
    // TODO: Implement when ready
    console.warn('showRefunded method is not implemented yet');
  }
}
