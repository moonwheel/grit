import { Component, OnInit, Input } from '@angular/core';
import { PhotoModel, PhotoAnnotationModel } from '../../models';
import { Store, Select } from '@ngxs/store';
import { PhotoActions } from 'src/app/store/actions/photo.actions';
import { PhotoState } from 'src/app/store/state/photo.state';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { InitAnnotations } from 'src/app/store/actions/annotation.actions';
import { HttpClient } from '@angular/common/http';
import { AbstractFeedItemComponent } from '../abstract-feed-item/abstract-feed-item.component';
import { AnnotationState } from 'src/app/store/state/annotation.state';
import { UserState } from 'src/app/store/state/user.state';

@Component({
  selector: 'app-feed-item-photo',
  templateUrl: './feed-item-photo.component.html',
  styleUrls: [ './feed-item-photo.component.scss' ],
})
export class FeedItemPhotoComponent extends AbstractFeedItemComponent implements OnInit {

  @Select(UserState.loggedInAccountId)
  loggedInAccountId$: Observable<number>;

  @Input()
  set item(value: PhotoModel) {
    this.photo = value;

    if (value) {
      this.annotations$ = this.store.select(AnnotationState.photoAnnotations(this.item.id));
    }
  }
  get item() {
    return this.photo;
  }

  @Input() truncatable = false;
  @Input() borderBottom = true;
  @Input() timeAgoFormat = false;

  Actions = PhotoActions;
  State = PhotoState;

  showAnnotations = false;

  currentUrl = '';

  annotations$: Observable<PhotoAnnotationModel[]>;

  private photo: PhotoModel;

  constructor(protected store: Store,
              protected dialog: MatDialog,
              protected http: HttpClient,
  ) {
    super(dialog, store);
  }

  ngOnInit() {
    this.currentUrl = environment.appUrl;
  }

  toggleAnnotations() {
    this.showAnnotations = !this.showAnnotations;
    this.store.dispatch(new InitAnnotations(this.item.id));
  }

  getPositionCSS(annotation: PhotoAnnotationModel) {
    return `translate(${annotation.x}px, ${annotation.y}px)`;
  }

}
