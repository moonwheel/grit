import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ArticleModel } from '../../models';
import { Store } from '@ngxs/store';
import { ArticleActions } from 'src/app/store/actions/article.actions';
import { ArticleState } from 'src/app/store/state/article.state';
import { MatDialog } from '@angular/material/dialog';
import { AbstractFeedItemComponent } from '../abstract-feed-item/abstract-feed-item.component';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'app-feed-item-article',
  templateUrl: './feed-item-article.component.html',
  styleUrls: [ './feed-item-article.component.scss' ],
})
export class FeedItemArticleComponent extends AbstractFeedItemComponent implements OnInit {

  @Input() item: ArticleModel;
  @Input() timeAgoFormat = false;
  @Input() expanded: boolean = false;

  Actions = ArticleActions as any;
  State = ArticleState;

  currentUrl = '';

  constructor(protected store: Store,
              protected dialog: MatDialog,
              protected cdr: ChangeDetectorRef,
              protected router?: Router,
  ) {
    super(dialog, store);
    this.handleMoreClick = this.handleMoreClick.bind(this);
  }

  ngOnInit() {
    this.currentUrl = environment.appUrl;
  }

  handleMoreClick() {
    this.navigateToArticlePage();
  }

  navigateToArticlePage() {
    const link = Utils.getAccountPageName(this.item.user);
    const page = `/${link}/blog/${this.item.id}`

    this.router.navigateByUrl(page);

  }

  onArticleTextClick(event: any) {
    const target = event.target;
    const parent = target && target.parentElement;
    const parent2 = parent && parent.parentElement;

    const targetDataId = target && target.getAttribute('data-id');
    const parentDataId = parent && parent.getAttribute('data-id');
    const parent2DataId = parent2 && parent2.getAttribute('data-id');

    const targetId = targetDataId || parentDataId || parent2DataId;

    if (targetId) {
      const mention = this.item.mentions && this.item.mentions.find(item => {
        return +item.target.id === +targetId;
      });

      if (mention) {
        const pagename = Utils.getAccountPageName(mention.target);
        const link = `/${pagename}/home`;
        this.dialog.closeAll();
        this.router.navigateByUrl(link);
        event.preventDefault();
      }
    } else {
      return;
    }
  }

}
