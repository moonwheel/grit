import { Component, Input, HostBinding } from "@angular/core";

@Component({
    selector: 'app-no-items-placeholder',
    styleUrls: ['./no-items-placeholder.component.scss'],
    templateUrl: './no-items.placeholder.component.html',
})
export class NoItemsPlaceholderComponent {
    @Input('text') text: string = '';
    @HostBinding('class.displayed') @Input('show') show: boolean = false;
}