import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Share, Device } from '@capacitor/core';
import 'share-api-polyfill';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'share-link-item',
  templateUrl: './share-link-item.component.html',
  styleUrls: ['./share-link-item.component.scss']
})

export class ShareLinkItemComponent {

  @Input()
  link: string;

  @Input()
  iconOnly = false;

  @Output()
  handleClick = new EventEmitter();

  constructor(private translateService: TranslateService) {}

  async share() {
    this.handleClick.emit();
    try {
      const deviceInfo = await Device.getInfo();
      const translation = await this.translateService.get('DIALOGS.SHARE').toPromise();
      if (deviceInfo.platform === 'web') {
        await (navigator as any).share({
          title: translation.TITLE,
          text: translation.TEXT,
          url: this.link,
        }, {
          print: false,
          sms: false,
        });
      } else {
        await Share.share({
          text: `${translation.TEXT} ${this.link}`,
          url: this.link,
          dialogTitle: translation.TITLE
        });
      }
    } catch (e) {
      console.warn('Sharing failed: ', e);
    }
  }
}
