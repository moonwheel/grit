import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareLinkItemComponent } from './share-link-item.component';

describe('ShareLinkItemComponent', () => {
  let component: ShareLinkItemComponent;
  let fixture: ComponentFixture<ShareLinkItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareLinkItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareLinkItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
