import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgModel } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccountModel, ServiceModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { of } from 'rxjs';
import { CancelBooking, RefundBooking } from 'src/app/store/actions/booking.actions';
import { DateTime } from 'luxon';
import { CardDirectPayIn } from 'src/app/store/actions/payment.actions';

@Component({
  selector: 'sales-service',
  templateUrl: './sales-service.component.html',
  styleUrls: ['./sales-service.component.scss']
})

export class SalesServiceComponent implements OnInit {

  @Input()
  item: any;

  cancelForm: FormGroup;
  payInForm: FormGroup;

  openedDialog: MatDialogRef<unknown, any>;

  reasons: string[] = [
    'DONT_LIKE',
    'POOR_SERVICE',
    'PRICE_PERFORMANCE',
    'OTHER',
  ];

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.cancelForm = this.formBuilder.group({
      reason: ['', Validators.required],
      description: '',
    });

    this.payInForm = this.formBuilder.group({
      bank: ['', Validators.required],
      amount: ['', Validators.required],
    });
  }

  cancel() {
    this.store.dispatch(
      new CancelBooking(
        this.item.id,
      )
    ).pipe(
      catchError(result => {
        this.snackbar.open(result.error.error, '', {
          duration: 3000,
          panelClass: ['orders-snackbar'],
        });
        return of('');
      })
    ).subscribe(() => {
      this.openedDialog.close();
    });
  }

  cancelBooking(templateRef) {
    this.openedDialog = this.dialog.open(templateRef, {});
  }

  refund() {
    this.store.dispatch(
      new RefundBooking(this.item.id)
    ).pipe(
      catchError(result => {
        this.snackbar.open(result.error.error, '', {
          duration: 3000,
          panelClass: ['orders-snackbar'],
        });
        return of('');
      })
    ).subscribe();
  }

  showAddress(data: any) {
    if (!data) {
      return '';
    }
    return [data.street, data.zip, data.city].filter(Boolean).join(', ');
  }

  getUserPhoto(user: AccountModel) {
    if (user.business) {
      return user.business.photo || constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
    }
  }

  getUserLink(user: AccountModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}`;
  }

  getServiceUrl(user: AccountModel, service: ServiceModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}/services/${service.id}`;
  }

  getTimeDifference(date: any) {
    if (!date) {
      return true;
    }
    const now = DateTime.local();
    return now.diff(DateTime.fromISO(date), 'hour').toObject().hours < 12;
  }

  payInToWallet() {
    if (this.payInForm.valid) {
      this.store.dispatch(
        new CardDirectPayIn(
          this.payInForm.get('bank').value,
          this.payInForm.get('amount').value,
        )
      );
    }
  }
}
