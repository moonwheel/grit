import { Component, OnInit, Input, Output, EventEmitter, HostBinding, HostListener, ɵConsole } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { catchError, tap } from "rxjs/operators";
import { throwError } from "rxjs";

@Component({
    selector: 'app-file-placeholder',
    styleUrls: ['./file-placeholder.component.scss'],
    templateUrl: './file-placeholder.component.html',
})
export class FilePlaceholderComponent implements OnInit {
    @Input('fileType') fileType: string;
    @Input('fileName') fileName: string
    @Input('fileUrl') fileUrl: string;

    fetched: boolean = false;
    fileExists: boolean = false;

    @Output('click') click = new EventEmitter();

    fileTypeToIconMap: Record<string, string> = {
        'document': 'description',
        'photo': 'photo_camera',
        'video': 'videocam',
    };

    constructor(private snackbar: MatSnackBar, private translateService: TranslateService, private http: HttpClient) { }

    ngOnInit() {
        const icon = this.mapFileTypeToIcon(this.fileType);

        const fileUrl = this.fileType === 'video' ? `${this.fileUrl}.mpd` : this.fileUrl
        this.tryToGetFile(fileUrl);
    }



    tryToGetFile = async (src: string) => {
        try {
            const file = this.isFileExists(src)

            this.fileExists = file;
            this.fetched = true;
        } catch (err) {
            this.fetched = true;
        }
    }


    mapFileTypeToIcon(fileType: string): string {
        return this.fileTypeToIconMap[fileType];
    }

    handleClick() {
        this.translateService.get('COMPONENTS.FILE_PLACEHOLDER_COMPONENT.DELETED_FILES_MESSAGE').subscribe(message => {
            this.snackbar.open(message);
        })
    }

    isFileExists(url: string): boolean {
        const xhr = new XMLHttpRequest();
        xhr.open('HEAD', url, false);
        xhr.send();
         
        if (xhr.status == 404) {
            return false;
        }

        return true;
    }
}
