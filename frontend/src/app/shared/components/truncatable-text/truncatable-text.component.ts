import { Component, OnInit, Input, ChangeDetectionStrategy, OnChanges, ViewChild, ViewContainerRef, AfterViewInit, ElementRef, EventEmitter, Output } from '@angular/core';
import { MentionModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';

const defaultToSlice = 150;
const ROWS_LIMIT = 5;
const LINE_WRAP_REGEX = /\n|<\/p>|<\/li>/g;

@Component({
  selector: 'app-truncatable-text',
  templateUrl: './truncatable-text.component.html',
  styleUrls: [ './truncatable-text.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TruncatableTextComponent implements OnInit {

  @Input() text: string;
  @Input() truncatable = false;
  @Input() borderBottom = true;
  @Input() padding = true;
  @Input() mentions: MentionModel[] = [];
  @Input() onMore;

  truncated = true;
  toSlice = defaultToSlice;

  constructor(private hostRef: ElementRef) { }

  handleMoreClick() {
    if(this.onMore) return this.onMore();

    this.truncated = false;
  }

  handleLessClick() {
    this.truncated = true;
  }

  ngOnInit() {
    this.toSlice = this.getNumberToSlice();
  }

  ngAfterViewInit() {
    this.convertLatestNodeToInlineContent();
  }

  convertLatestNodeToInlineContent() { // converts latest node element to inline (text) so '...more' btn can stick with content
    const textContainer = this.hostRef.nativeElement.getElementsByClassName('truncatable-text-container')[0];

    if(!textContainer) return;

    const childs = textContainer.childNodes;

    const latestChild = childs && childs.length ? childs[childs.length - 1] : null;

    if(latestChild && latestChild.nodeName == 'P') {
      latestChild.style.display = 'inline';
    }
  }

  getNumberToSlice(): number {
    if (this.text && this.truncatable && this.mentions && this.text.length >= defaultToSlice) {
      // lets form regexp from hashtegs regesp and mentions
      let regexpText = `(${constants.HASHTAGS_REGEXP_TEXT})`;

      for (const mention of this.mentions) {
        regexpText += `|(${mention.mention_text})`;
      }

      const regexp = RegExp(regexpText, 'g');

      // start to search regexp in our text
      let match = regexp.exec(this.text);
      while (match !== null) {
        const start = match.index;
        const end = regexp.lastIndex;

        // if we are reached end or default slice - return it and stop iteration
        if (start > defaultToSlice) {
          return defaultToSlice;
        }

        // if our match is on place of defalt slice - decrease slice to start before current hit
        if (end > defaultToSlice && start <= defaultToSlice) {
          return start;
        }

        match = regexp.exec(this.text);
      }
    }

    const rowsInText = this.getAmountOfRows(this.text);

    if(rowsInText >= ROWS_LIMIT) {
      const amountToSlice = this.text.slice(0, this.getSliceIndex(this.text)).length;

      return amountToSlice;
    }

    
    return defaultToSlice;
  }

  getAmountOfRows(text: string) {
    if(typeof text !== 'string') return 0;

    const rowsInText = text.match(LINE_WRAP_REGEX);

    return rowsInText ? rowsInText.length : 0;
  }

  getSliceIndex(text: string) {
    for(let i = 1, match; (match = LINE_WRAP_REGEX.exec(text)) !== null; i++) {
      if(i === ROWS_LIMIT) return match.index;
    }
  }
}
