import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  OnDestroy,
  HostBinding,
} from '@angular/core';
import { Subject, BehaviorSubject, combineLatest } from 'rxjs';
import { AbstractEntityModel } from '../../models';
import { constants } from 'src/app/core/constants/constants';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: [ './thumbnail.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThumbnailComponent implements OnInit, OnDestroy {
  @ViewChild('img', { static: true }) img: ElementRef<HTMLImageElement>;

  @Input() defaultUrl: string = constants.PLACEHOLDER_NO_PHOTO_PATH;
  @Input() additionalClass: string;
  @Input() preloadedClassName: string;

  @Input() set item(value: AbstractEntityModel) {
    this.item$.next(value);
  }

  @Input() set url(value: string) {
    this.url$.next(value);
  }

  preloading = true;

  @HostBinding('class')
  get hostClass() {
    return !this.preloading ? this.preloadedClassName : ''
  }

  item$ = new BehaviorSubject<any>(null);
  url$ = new BehaviorSubject<string>(null);

  private destroy$ = new Subject<string>();

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    combineLatest([
      this.item$,
      this.url$,
    ]).pipe(
      takeUntil(this.destroy$),
    ).subscribe(([ item, url ]) => {
      if (
        item && (item.processing || item.uploading)
        || this.img.nativeElement.src === url
      ) {
        return;
      }

      if (!url || url === 'null') {
        url = this.defaultUrl;
      }

      this.img.nativeElement.src = url;

      if (this.img.nativeElement.complete) {
        this.preloading = false;
        this.cdr.detectChanges();
      } else {
        this.preloading = true;
        this.cdr.detectChanges();
        this.img.nativeElement.onload = () => {
          this.preloading = false;
          this.cdr.detectChanges();
        };
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
