import { Component } from "@angular/core";
import { LoadingComponent } from "../loading/loading.component";

@Component({
    selector: 'app-main-loader',
    styleUrls: ['./main-loader.component.scss'],
    templateUrl: './main-loader.component.html',
})
export class MainLoader extends LoadingComponent {

}