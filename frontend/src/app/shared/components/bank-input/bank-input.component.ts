import { Component, OnInit, forwardRef, Input, TemplateRef, Optional, Injector } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, FormControl, FormGroupDirective, NgForm } from '@angular/forms';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SatPopover } from '@ncstate/sat-popover';

import { BankAccountModel, CreditCardModel } from '../../models';
import { BankDialogComponent } from '../bank-dialog/bank-dialog.component';
import { LoadPayments, DeleteBankAccount, DeleteCreditCard } from 'src/app/store/actions/payment.actions';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { CustomErrorStateMatcher } from '../../utils/custom-error-state-matcher';

@Component({
  selector: 'app-bank-input',
  templateUrl: './bank-input.component.html',
  styleUrls: ['./bank-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BankInputComponent),
      multi: true,
    }
  ],
})

export class BankInputComponent implements OnInit, ControlValueAccessor {

  @Input()
  placeholder = 'Payment';

  @Input()
  required = false;

  @Input()
  onlyCard = false;

  @Input()
  onlyBank = false;

  @Select(PaymentsState.payments)
  payments$: Observable<any[]>;

  value: number;
  disabled = false;
  control: NgControl;
  matcher: ErrorStateMatcher;

  propagateChange: (arg: any) => void;
  propagateTouched: (arg: any) => void;

  get selectedPayment() {
    const account = this.store.selectSnapshot(PaymentsState.payment(this.value));
    return account;
  }

  get errors() {
    return this.control && this.control.errors;
  }

  constructor(
    private dialog: MatDialog,
    private store: Store,
    @Optional()
    private injector: Injector,
  ) { }

  ngOnInit() {
    this.control = this.injector.get(NgControl);
    this.matcher = new CustomErrorStateMatcher(this.control);
    this.store.dispatch(new LoadPayments());
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  openAddAccountDialog(event: Event) {
    event.stopPropagation();

    this.dialog.open(BankDialogComponent, {
      width: '500px',
    }).afterClosed().subscribe((success) => {
      if (success) {
        const payments = this.store.selectSnapshot(PaymentsState.payments);
        const newAccount = payments.slice(-1)[0];
        this.writeValue(newAccount.id);
        this.propagateChange(newAccount.id);
      }
    });
  }

  openEditAccountDialog(account: any, popover: SatPopover) {
    this.dialog.open(BankDialogComponent, {
      data: {
        model: account,
        type: account.iban ? 'bank_account' : 'credit_card',
      },
      width: '500px',
    });
    popover.close();
  }

  openAccountMorePopover(popover: SatPopover, event: Event) {
    popover.toggle();
    event.stopPropagation();
  }

  deleteAccount(id: number, popover: SatPopover) {
    this.store.dispatch(new DeleteBankAccount(id)).subscribe(() => {
      this.writeValue(null);
      this.propagateChange(null);
      popover.close();
    });
  }

  deleteCreditCard(id: number, popover: SatPopover) {
    this.store.dispatch(new DeleteCreditCard(id)).subscribe(() => {
      this.writeValue(null);
      this.propagateChange(null);
      popover.close();
    });
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      width: '500px',
      panelClass: 'myClass',
      disableClose: true,
    });
  }

  checkCreditCardType(value: string) {
    const visaRegex = /^4/;
    const mastercardRegex = /^5[1-5]/;

    if (visaRegex.test(value)) {
      return 'visa';
    } else if (mastercardRegex.test(value)) {
      return 'mastercard';
    } else {
      return '';
    }
  }
}
