import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { ServiceModel } from '../../models';
import { Store } from '@ngxs/store';
import { ServiceActions } from 'src/app/store/actions/service.actions';
import { ServiceState } from 'src/app/store/state/service.state';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { AbstractFeedItemComponent } from '../abstract-feed-item/abstract-feed-item.component';
import { SatPopover } from '@ncstate/sat-popover';
import { ReviewListComponent } from '../actions/reviews/review-list/review-list.component';
import { constants } from 'src/app/core/constants/constants';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { DateTime } from 'luxon';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-feed-item-service',
  templateUrl: './feed-item-service.component.html',
  styleUrls: [ './feed-item-service.component.scss' ],
})
export class FeedItemServiceComponent extends AbstractFeedItemComponent implements OnInit {

  @Input() truncatable = false;
  @Input() borderBottom = true;
  @Input() timeAgoFormat = false;

  @Input() set item(data) {
    if (data) {
      this.setService(data);
    }
  }

  get item() {
    return this.itemCache;
  }

  durations = [ ...constants.DURATIONS ];

  config: SwiperConfigInterface = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
      bulletActiveClass: 'swiper-pagination__active'
    }
  };

  Actions = ServiceActions;
  State = ServiceState;

  photosList: any[] = [];

  currentUrl = '';

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  private itemCache: ServiceModel;

  constructor(
    protected store: Store,
    protected dialog: MatDialog,
    protected http: HttpClient,
  ) {
    super(dialog, store);
  }

  ngOnInit() {
    this.currentUrl = environment.appUrl;
  }

  setService(data: ServiceModel) {
    const service = { ...data };

    if (!service.schedule.duration) {
      const displayDateInfo = [];
      service.schedule.byDate.forEach(schedule => {
        const startInfo = DateTime.fromISO(schedule.from).toFormat('dd MMM y');
        const endInfo = DateTime.fromISO(schedule.to).toFormat('dd MMM y');
        const formatedFrom = DateTime.fromISO(schedule.from).toFormat('dd MMM y, HH:mm - ');

        let formatedTo: string;
        if (startInfo === endInfo) {
          formatedTo = DateTime.fromISO(schedule.to).toFormat('HH:mm');
        } else {
          formatedTo = DateTime.fromISO(schedule.to).toFormat('dd MMM y, HH:mm');
        }

        displayDateInfo.push(formatedFrom + formatedTo);
      });

      service.displayDateInfoHeader = displayDateInfo.splice(0, 1);
      service.displayDateInfo = displayDateInfo;
    } else {
      const found = this.durations.find(item => item.value === service.schedule.duration);
      service.durationInfo = found && found.name;
    }

    this.itemCache = service;
    this.photosList = this.filterPhotos();
  }

  filterPhotos() {
    if (this.item && this.item.photos) {
      return this.item.photos.map(photo => `${photo.link}`);
    } else {
      return [];
    }
  }

  serviceReviews() {
    this.dialog.open(ReviewListComponent, {
      disableClose: true,
      width: '500px',
      data: {
        entity: 'service',
        item: this.item,
      },
    });
  }

  inactiveService(service: ServiceModel) {
    const payload = { id: service.id, service: { active: !service.active } };
    this.store.dispatch(new ServiceActions.ChangeActiveState(payload));
  }
}
