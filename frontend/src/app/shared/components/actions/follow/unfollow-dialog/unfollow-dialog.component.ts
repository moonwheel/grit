import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountModel } from 'src/app/shared/models';
import { Store } from '@ngxs/store';
import { UnfollowUser } from 'src/app/store/actions/user.actions';
import { constants } from 'src/app/core/constants/constants';
import { Utils } from 'src/app/shared/utils/utils';

export interface UnfollowDialogComponentData {
  account: AccountModel;
}

@Component({
  selector: 'app-unfollow-dialog',
  templateUrl: './unfollow-dialog.component.html'
})
export class UnfollowDialogComponent implements OnInit {

  account: AccountModel;

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  constructor(private dialogRef: MatDialogRef<UnfollowDialogComponent>,
              @Inject(MAT_DIALOG_DATA)
              private config: UnfollowDialogComponentData,
              private store: Store,
  ) { }

  ngOnInit() {
    if (this.config) {
      if (this.config.account) {
        this.account = Utils.extendAccountWithBaseInfo(this.config.account);
      }
    }
  }

  close(result: boolean = false) {
    this.dialogRef.close(result);
  }

  unfollow() {
    this.store.dispatch(new UnfollowUser(this.account.id));
    this.dialogRef.close(true);
  }

}
