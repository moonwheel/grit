import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountModel } from 'src/app/shared/models';
import { Store } from '@ngxs/store';
import { BlockUser, UnblockUser } from 'src/app/store/actions/user.actions';
import { constants } from 'src/app/core/constants/constants';
import { Utils } from 'src/app/shared/utils/utils';

type BlockedDialogType = 'block' | 'unblock';

export interface BlockedDialogComponentData {
  account: AccountModel;
  type: BlockedDialogType;
}

@Component({
  selector: 'app-blocked-dialog',
  templateUrl: './blocked-dialog.component.html',
})

export class BlockedDialogComponent implements OnInit {

  type: BlockedDialogType = 'block';

  account: AccountModel;

  constructor(
    private dialogRef: MatDialogRef<BlockedDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private config: BlockedDialogComponentData,
    private store: Store,
  ) { }

  ngOnInit() {
    if (this.config) {
      if (this.config.account) {
        this.account = Utils.extendAccountWithBaseInfo(this.config.account);
      }
      if (this.config.type) {
        this.type = this.config.type;
      }
    }
  }

  close(result: boolean = false) {
    this.dialogRef.close(result);
  }

  block() {
    this.store.dispatch(new BlockUser(this.account.id)).subscribe(() => {
      this.dialogRef.close(true);
    });
  }

  unblock() {
    this.store.dispatch(new UnblockUser(this.account.id)).subscribe(() => {
      this.dialogRef.close(true);
    });
  }
}
