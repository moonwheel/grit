import { Component, Inject, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { CommentModel } from '../comment.model';
import { Store, Select } from '@ngxs/store';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { FormBuilder, Validators } from '@angular/forms';
import { take, switchMap, takeUntil } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';
import { SaveDialogComponent } from '../../../save-dialog/save-dialog.component';
import { constants } from 'src/app/core/constants/constants';

export interface CommentEditComponentData {
  commentId: number;
  reply: boolean;
  State: typeof AbstractState;
  Actions: typeof AbstractActions;
}

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: [ './comment-edit.component.scss' ]
})
export class CommentEditComponent implements OnInit, OnDestroy {

  @Select(UserState.loggedInAccountPhoto) userPhoto$: Observable<string>;

  comment$: Observable<CommentModel>;

  editCommentForm = this.fb.group({
    text: ['', Validators.required],
    mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
  });

  openedDialog: MatDialogRef<any>;
  initialText = '';
  reply = false;

  private destroy$ = new Subject();

  constructor(private store: Store,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<CommentEditComponent>,
              @Inject(MAT_DIALOG_DATA)
              private config: CommentEditComponentData,
              private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;

    const { commentId, State, reply } = this.getValidConfig();

    this.reply = !!reply;

    this.comment$ = this.store.select(AbstractState.commentSelector(State, commentId, reply));

    this.comment$.pipe(
      take(1),
      takeUntil(this.destroy$),
    ).subscribe(comment => {
      this.initialText = comment.text;
      this.editCommentForm.get('text').patchValue(comment.text);
    });

    this.dialogRef.backdropClick().pipe(
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.close();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.openedDialog = this.dialog.open(templateRef);
  }

  closeOpenedDialog() {
    this.openedDialog.close();
  }

  editComment() {
    const { Actions } = this.getValidConfig();
    const form = this.editCommentForm.value;

    this.comment$.pipe(
      take(1),
      switchMap(comment => {
        return this.store.dispatch(new Actions.EditComment(comment.id, form, comment.replyTo));
      })
    ).subscribe(() => {
      this.dialogRef.close();
    });
  }

  close() {
    const text = this.editCommentForm.get('text').value;

    if (text === this.initialText || !text) {
      this.dialogRef.close();
    } else {
      this.dialog.open(SaveDialogComponent).afterClosed().subscribe(yes => {
        if (yes) {
          this.editComment();
        } else {
          this.dialogRef.close();
        }
      });
    }
  }

  private getValidConfig(): CommentEditComponentData {
    if (
      !this.config ||
      !this.config.State ||
      !this.config.Actions
    ) {
      throw new Error('CommentEditComponentData should be provided');
    } else {
      return this.config;
    }
  }

}
