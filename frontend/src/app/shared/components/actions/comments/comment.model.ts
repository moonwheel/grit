import { LikeModel } from '../likes/like.model';
import { AccountModel, MentionModel } from 'src/app/shared/models';

export interface CommentModel {
  id: number; // maybe a commentId and ReplyId
  user: AccountModel;
  text: string;
  comment: string;
  likes: number[];
  liked: boolean;
  likesCount: number;
  replies: number[];
  repliesCount: number;
  created: Date;
  updated?: Date;
  deleted?: Date;
  commentTo?: number;
  replyTo?: number;
  mentions: MentionModel[];
}
