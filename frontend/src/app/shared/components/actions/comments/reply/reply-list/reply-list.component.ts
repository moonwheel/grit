import { Component, OnInit, Inject, TemplateRef, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { Observable, Subject, combineLatest } from 'rxjs';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { Store, Select } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommentModel } from '../../comment.model';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { LikeListComponentData, LikeListComponent } from '../../../likes/like-list/like-list.component';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommentEditComponentData, CommentEditComponent } from '../../comment-edit/comment-edit.component';
import { SaveDialogComponentData, SaveDialogComponent } from 'src/app/shared/components/save-dialog/save-dialog.component';
import { takeUntil, switchMap, filter, map } from 'rxjs/operators';
import { AccountModel } from 'src/app/shared/models';
import { UnfollowDialogComponent } from '../../../follow/unfollow-dialog/unfollow-dialog.component';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { TranslateService } from '@ngx-translate/core';
import { AbstractInfiniteListComponent } from 'src/app/shared/components/abstract-infinite-list/abstract-infinite-list.component';
import { constants } from 'src/app/core/constants/constants';

export interface ReplyListComponentData {
  entityId: number;
  commentId: number;
  State: typeof AbstractState;
  Actions: typeof AbstractActions;
}

@Component({
  selector: 'app-reply-list',
  templateUrl: './reply-list.component.html',
  styleUrls: [ './reply-list.component.scss' ],
})
export class ReplyListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(UserState.loggedInAccountPhoto) userPhoto$: Observable<string>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;

  truncating = true;

  addCommentForm = this.fb.group({
    text: ['', Validators.required],
    mentions: [[], Validators.maxLength(10)],
  });

  openedDialog: MatDialogRef<any>;

  page = 1;
  pageSize = 15;
  filter = 'relevance';
  itemSize = 120;
  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  comment$: Observable<CommentModel>;
  replies$: Observable<CommentModel[]>;
  repliesLoading$: Observable<boolean>;
  commenting$: Observable<boolean>;
  total$: Observable<number>;
  virtualScrollHeight$: Observable<string>;

  constructor(private store: Store,
              private router: Router,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<ReplyListComponent>,
              @Inject(MAT_DIALOG_DATA)
              private config: ReplyListComponentData,
              private fb: FormBuilder,
              private translateService: TranslateService,
              private zone: NgZone,
  ) {
    super();
  }

  ngOnInit() {
    const { commentId, State } = this.getValidConfig();

    this.comment$ = this.store.select(AbstractState.commentSelector(State, commentId));
    this.replies$ = this.store.select(AbstractState.repliesSelector(State, commentId));
    this.repliesLoading$ = this.store.select(AbstractState.repliesLoadingSelector(State));
    this.commenting$ = this.store.select(AbstractState.commentingSelector(State));

    this.total$ = this.comment$.pipe(
      filter(item => !!item),
      map(item => item.repliesCount)
    );

    this.virtualScrollHeight$ = this.createVirtualScrollHeightObservable(
      this.replies$,
      this.itemSize,
      3,
      this.zone,
      '121px',
      122,
    );

    this.loadItems();

    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.close();
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.loadItems();
    });
  }

  loadItems() {
    const { commentId, Actions } = this.getValidConfig();
    this.store.dispatch(new Actions.LoadCommentReplies(commentId, this.page, this.pageSize));
  }

  likeComment() {
    const { Actions, commentId } = this.getValidConfig();
    this.store.dispatch(new Actions.LikeComment(commentId));
  }

  showCommentLikes() {
    const { commentId } = this.getValidConfig();
    this.showLikesModal(commentId, false);
  }

  likeReply(replyId: number) {
    const { Actions } = this.getValidConfig();
    this.store.dispatch(new Actions.LikeComment(replyId, true));
  }

  showReplyLikes(replyId: number) {
    this.showLikesModal(replyId, true);
  }

  replyComment() {
    const { Actions, entityId, commentId } = this.getValidConfig();
    const form = this.addCommentForm.value;

    this.store.dispatch(new Actions.Comment(entityId, form, commentId)).subscribe(() => {
      this.addCommentForm.reset({
        mentions: [],
      });
    });
  }

  editVideo() {

  }

  goToUserPage(comment: CommentModel) {
    this.router.navigateByUrl(`${comment.user.pageName}/home`).then(() => {
      this.dialog.closeAll();
    });
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.openedDialog = this.dialog.open(templateRef);
  }

  closeOpenedDialog() {
    this.openedDialog.close();
  }

  deleteComment(commentId: number, reply = false) {
    const { Actions } = this.getValidConfig();

    this.store.dispatch(new Actions.DeleteComment(commentId, reply));

    if (this.openedDialog) {
      this.openedDialog.close();
    }

    if (!reply) {
      this.dialogRef.close();
    }
  }

  openEditCommentDialog(comment: CommentModel, reply = false) {
    const { Actions, State } = this.getValidConfig();
    const data: CommentEditComponentData = {
      commentId: comment.id,
      reply,
      Actions,
      State,
    };
    this.dialog.open(CommentEditComponent, { data });
  }

  editComment(id: number, isReply = false) {
    const { Actions, commentId } = this.getValidConfig();
    const form = this.addCommentForm.value;

    this.store.dispatch(new Actions.EditComment(id, form, isReply ? commentId : null)).subscribe(() => {
      if (this.openedDialog) {
        this.openedDialog.close();
      }
    });
  }

  close() {
    if (!this.addCommentForm.get('text').value) {
      this.dialogRef.close();
    } else {
      combineLatest([
        this.translateService.get('DIALOGS.REPLY_DISCARD.TITLE'),
        this.translateService.get('GENERAL.ACTIONS.CONTINUE'),
        this.translateService.get('GENERAL.ACTIONS.DISCARD'),
      ]).pipe(
        takeUntil(this.destroy$),
        switchMap(([text, leftButtonText, rightButtonText]) => {
          const data: SaveDialogComponentData = {
            text: text as string,
            leftButtonColor: 'grey',
            leftButtonText: leftButtonText as string,
            rightButtonColor: 'red',
            rightButtonText: rightButtonText as string,
          };
          return this.dialog.open(SaveDialogComponent, { data }).afterClosed();
        })
      ).subscribe((discard) => {
        if (discard) {
          this.dialogRef.close();
        }
      });
    }
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  follow(account: AccountModel, event: Event) {
    event.stopPropagation();

    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  reportComment() {
    // TODO Implement when reports are done
    console.warn('Method is not implemented');
  }

  private getValidConfig(): ReplyListComponentData {
    if (
      !this.config ||
      !this.config.State ||
      !this.config.Actions
    ) {
      throw new Error('LikeListComponent dialog data should be provided');
    } else {
      return this.config;
    }
  }

  private showLikesModal(commentOrReplyId: number, reply: boolean) {
    const { Actions, State } = this.getValidConfig();

    const data: LikeListComponentData = {
      id: null,
      State,
      Actions,
      commentId: commentOrReplyId,
      reply
    };

    this.dialog.open(LikeListComponent, {
      data,
    }).afterClosed().subscribe(shouldClose => {
      if (shouldClose) {
        this.dialogRef.close();
      }
    });
  }

}
