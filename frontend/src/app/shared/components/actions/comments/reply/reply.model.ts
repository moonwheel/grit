import { UserModel } from 'src/app/shared/models/user/user.model';
import { LikeModel } from '../../likes/like.model';

export interface ReplyModel {
  id: number;
  user: UserModel;
  reply: string;
  likes: LikeModel[];
  createdAt: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
