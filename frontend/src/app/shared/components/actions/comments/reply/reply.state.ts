import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ReplyModel } from './reply.model';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})

export class ReplyState {

  private url: string = `${environment.baseUrl}/replies`;

  private replyState = new BehaviorSubject<ReplyModel[]>([]);

  replies = this.replyState.asObservable();

  constructor(private http: HttpClient) { }

  getReplies() {
    this.http.get<ReplyModel[]>(this.url).subscribe(replies => { this.replyState.next(replies) });
  }

  getReply(id: number) {
    return this.http.get<ReplyModel>(`${this.url}/${id}`);
  }

  addReply(reply: ReplyModel) {
    this.http.post<ReplyModel>(this.url, reply, httpOptions).subscribe(
      reply => { this.replyState.next(this.replyState.getValue().concat([reply])) });
  }

  updateReply(reply: ReplyModel) {
    return this.http.put<ReplyModel>(`${this.url}/${reply.id}`, reply, httpOptions);
  }

  deleteReply(id: number) {
    return this.http.delete<ReplyModel>(`${this.url}/${id}`);
  }

}
