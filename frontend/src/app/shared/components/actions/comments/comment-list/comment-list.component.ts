import { Component, Inject, OnInit, TemplateRef, OnDestroy, NgZone, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ReplyListComponent, ReplyListComponentData } from '../reply/reply-list/reply-list.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject, from, combineLatest } from 'rxjs';
import { CommentModel } from '../comment.model';
import { Store, Select } from '@ngxs/store';
import { Router } from '@angular/router';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { UserState } from 'src/app/store/state/user.state';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { LikeListComponentData, LikeListComponent } from '../../likes/like-list/like-list.component';
import { FormBuilder, Validators } from '@angular/forms';
import { CommentEditComponent, CommentEditComponentData } from '../comment-edit/comment-edit.component';
import { takeUntil, switchMap, filter, map } from 'rxjs/operators';
import { SaveDialogComponent, SaveDialogComponentData } from '../../../save-dialog/save-dialog.component';
import { AccountModel } from 'src/app/shared/models';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { UnfollowDialogComponent } from '../../follow/unfollow-dialog/unfollow-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';
import { AbstractInfiniteListComponent } from '../../../abstract-infinite-list/abstract-infinite-list.component';

export interface CommentListComponentData {
  id: number;
  State: typeof AbstractState;
  Actions: typeof AbstractActions;
}

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: [ './comment-list.component.scss' ]
})

export class CommentListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(UserState.loggedInAccountPhoto) userPhoto$: Observable<string>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;

  commentsMapping = {};

  loading = true;
  truncating = true;

  addCommentForm = this.fb.group({
    text: ['', Validators.required],
    mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
  });

  openedDialog: MatDialogRef<any>;

  page = 1;
  pageSize = 15;
  filter = 'relevance';
  itemSize = 120;

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  comments$: Observable<CommentModel[]>;
  commentsLoading$: Observable<boolean>;
  commenting$: Observable<boolean>;
  total$: Observable<number>;
  virtualScrollHeight$: Observable<string>;

  constructor(private store: Store,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<CommentListComponent>,
              private router: Router,
              @Inject(MAT_DIALOG_DATA)
              private config: CommentListComponentData,
              private fb: FormBuilder,
              private translateService: TranslateService,
              private zone: NgZone,
  ) {
    super();
  }

  ngOnInit() {
    const { id, State } = this.getValidConfig();

    this.comments$ = this.store.select(AbstractState.commentsSelector(State, id)).pipe(
      switchMap(result => from(Promise.resolve(result)))
    );

    this.commentsLoading$ = this.store.select(AbstractState.commentsLoadingSelector(State));
    this.commenting$ = this.store.select(AbstractState.commentingSelector(State));

    this.total$ = this.store.select(AbstractState.getOneSelector(State, id)).pipe(
      filter(item => !!item),
      map(item => item.commentsCount)
    );

    this.virtualScrollHeight$ = this.createVirtualScrollHeightObservable(
      this.comments$,
      this.itemSize,
      3,
      this.zone,
      '120px'
    );

    this.loadItems();

    this.dialogRef.disableClose = true;

    this.dialogRef.backdropClick().pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.close();
    });

    this.translateService.stream('DIALOGS.COMMENT_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.commentsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.loadItems();
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  like(commentId: number) {
    const { Actions } = this.getValidConfig();
    this.store.dispatch(new Actions.LikeComment(commentId));
  }

  addComment() {
    const { id, Actions } = this.getValidConfig();
    const form = this.addCommentForm.value;

    this.store.dispatch(new Actions.Comment(id, form)).subscribe(() => {
      this.addCommentForm.reset({
        mentions: [],
      });
    });
  }

  showCommentReplies(commentId: number) {
    const { id, Actions, State } = this.getValidConfig();

    const data: ReplyListComponentData = {
      entityId: id,
      commentId,
      State,
      Actions,
    };

    this.dialog.open(ReplyListComponent, { data, panelClass: 'dialog-with-message-input-panel' });
  }

  showLikes(commentId: number) {
    const { id, Actions, State } = this.getValidConfig();

    const data: LikeListComponentData = {
      id,
      State,
      Actions,
      commentId
    };

    this.dialog.open(LikeListComponent, {
      data,
    }).afterClosed().subscribe(shouldClose => {
      if (shouldClose) {
        this.dialogRef.close();
      }
    });
  }

  loadItems() {
    const { id, Actions } = this.getValidConfig();
    this.store.dispatch(new Actions.LoadComments(id, this.page, this.pageSize, this.filter));
  }

  applyFilter(filterBy: 'relevance' | 'date') {
    this.filter = filterBy;
    this.page = 1;
    this.loadItems();
  }

  goToUserPage(comment: CommentModel) {
    this.router.navigateByUrl(`${comment.user.pageName}/home`).then(() => {
      this.dialog.closeAll();
    });
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.openedDialog = this.dialog.open(templateRef);
  }

  closeOpenedDialog() {
    this.openedDialog.close();
  }

  deleteComment(commentId: number) {
    const { Actions } = this.getValidConfig();

    this.store.dispatch(new Actions.DeleteComment(commentId));

    if (this.openedDialog) {
      this.openedDialog.close();
    }
  }

  openEditCommentDialog(comment: CommentModel, reply = false) {
    const { Actions, State } = this.getValidConfig();
    const data: CommentEditComponentData = {
      commentId: comment.id,
      reply,
      Actions,
      State,
    };
    this.dialog.open(CommentEditComponent, { data });
  }

  editComment(commentId: number) {
    const { Actions } = this.getValidConfig();
    const form = this.addCommentForm.value;

    this.store.dispatch(new Actions.EditComment(commentId, form)).subscribe(() => {
      if (this.openedDialog) {
        this.openedDialog.close();
      }
    });
  }

  close() {
    if (!this.addCommentForm.get('text').value) {
      this.dialogRef.close();
    } else {
      combineLatest([
        this.translateService.get('DIALOGS.COMMENT_DISCARD.TITLE'),
        this.translateService.get('GENERAL.ACTIONS.CONTINUE'),
        this.translateService.get('GENERAL.ACTIONS.DISCARD'),
      ]).pipe(
        takeUntil(this.destroy$),
        switchMap(([text, leftButtonText, rightButtonText]) => {
          const data: SaveDialogComponentData = {
            text: text as string,
            leftButtonColor: 'grey',
            leftButtonText: leftButtonText as string,
            rightButtonColor: 'red',
            rightButtonText: rightButtonText as string,
          };
          return this.dialog.open(SaveDialogComponent, { data }).afterClosed();
        })
      ).subscribe((discard) => {
        if (discard) {
          this.dialogRef.close();
        }
      });
    }
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  follow(account: AccountModel, event: Event) {
    event.stopPropagation();

    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  reportComment() {
    // TODO Implement when reports are done
    console.warn('Method is not implemented');
  }

  private getValidConfig(): CommentListComponentData {
    if (
      !this.config ||
      !this.config.State ||
      !this.config.Actions
    ) {
      throw new Error('CommentListComponentData should be provided');
    } else {
      return this.config;
    }
  }

}
