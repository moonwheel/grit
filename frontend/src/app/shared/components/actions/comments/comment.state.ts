import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { CommentModel } from './comment.model';
import { environment } from 'src/environments/environment.prod';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})

export class CommentState {

  private url: string = environment.baseUrl;

  private commentState = new BehaviorSubject<CommentModel[]>([]);

  comments = this.commentState.asObservable();

  constructor(private http: HttpClient) { }

  getComments() {
    this.http.get<CommentModel[]>(this.url).subscribe(comments => { this.commentState.next(comments) });
  }

  getComment(id: number, type: string) {
    return this.http.get<CommentModel[]>(`${this.url}/${type}/${id}/comment`).subscribe(comments => { this.commentState.next(comments); });
  }

  addComment(text, id, type) {
    const comment = {
      text: text,
    };
    this.http.post<CommentModel>(`${this.url}/${type}/${id}/comment`, comment, httpOptions).subscribe(
      comment => { this.commentState.next(this.commentState.getValue().concat([comment])) });
  }

  updateComment(comment: CommentModel) {
    return this.http.put<CommentModel>(`${this.url}/${comment.id}`, comment, httpOptions);
  }

  deleteComment(id: number) {
    return this.http.delete<CommentModel>(`${this.url}/${id}`);
  }

}
