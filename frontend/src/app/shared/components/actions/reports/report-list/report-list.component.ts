import { Component, OnInit } from '@angular/core';
import { InitReport } from '../../../../../store/actions/report.actions';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {ReportState} from '../../../../../store/state/report.state';
import {environment} from '../../../../../../environments/environment';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'report-list',
  templateUrl: './report-list.component.html'
})

export class ReportListComponent implements OnInit {

  @Select(ReportState.reports) reports: Observable<any[]>;

  environment = environment;

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new InitReport());
  }
}
