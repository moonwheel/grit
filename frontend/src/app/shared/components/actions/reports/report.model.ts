import { UserModel } from 'src/app/shared/models/user/user.model';

export interface ReportModel {
  id: number;
  user: UserModel;
  contentType: string; // User, Photo, Video, Article, Product, Service, Comment, Review
  reportType: string; // Spam, Insult, Violence, Pornography, Terrorism, Infringement
  report?: string;
  created: Date;
}
