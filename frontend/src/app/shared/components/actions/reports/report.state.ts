import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ReportModel } from './report.model';
import { environment } from 'src/environments/environment.prod';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ReportState {
  private url = `${environment.baseUrl}/reports`;
  private reportState = new BehaviorSubject<ReportModel[]>([]);

  reports = this.reportState.asObservable();
  constructor(private http: HttpClient) { }

  getReports() {
    this.http.get<ReportModel[]>(this.url).subscribe(reports => { this.reportState.next(reports); });
  }

  getReport(id: number) {
     return this.http.get<ReportModel>(`${this.url}/${id}`);
  }

  addReport(report: ReportModel) {
    this.http.post<ReportModel>(this.url, report, httpOptions).subscribe(
      report => { this.reportState.next(this.reportState.getValue().concat([report])); });
   }
}
