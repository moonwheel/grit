import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AddReport, LoadReportCategories } from 'src/app/store/actions/report.actions';
import { ReportState } from 'src/app/store/state/report.state';

@Component({
  selector: 'app-report-add',
  templateUrl: './report-add.component.html'
})

export class ReportAddComponent implements OnInit {

  @Select(ReportState.categories)
  categories$: Observable<any[]>;

  @Input()
  contentType: string;

  @Input()
  contentId: number;

  reportForm: FormGroup;

  categories: string[] = [
    'GENERAL.CATEGORIES.REPORTS.FRAUD',
    'GENERAL.CATEGORIES.REPORTS.SPAM',
    'GENERAL.CATEGORIES.REPORTS.INSULT',
    'GENERAL.CATEGORIES.REPORTS.VIOLENCE',
    'GENERAL.CATEGORIES.REPORTS.PORNOGRAPHY',
    'GENERAL.CATEGORIES.REPORTS.TERRORISM',
    'GENERAL.CATEGORIES.REPORTS.INFRINGEMENT'
  ];

  constructor(private formBuilder: FormBuilder, private store: Store) { }

  ngOnInit() {
    this.reportForm = this.formBuilder.group({
      category: ['', Validators.required],
      description: '',
      created: new Date()
    });
    this.store.dispatch(new LoadReportCategories());
  }

  addReport() {
    const payload = {
      category: this.reportForm.get('category').value,
      type: this.contentType,
      report: this.reportForm.get('description').value
    };
    this.store.dispatch(new AddReport(this.contentType, this.contentId, payload))
      .subscribe(() => {
        this.reportForm.reset();
      });
  }
}
