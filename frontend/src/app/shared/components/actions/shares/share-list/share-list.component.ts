import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ShareState } from '../../../../../store/state/share.state';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-share-list',
  templateUrl: './share-list.component.html'
})

export class ShareListComponent implements OnInit {

  shares = this.shareState.shares;

  user = { photo: 'assets/images/Thomas.png' };

  followings = [
    {
      id: 'sandrameyer',
      firstName: 'Sandra',
      lastName: 'Meyer',
      photo: 'assets/images/Avatar.jpg',
      companyName: '',
    },
    {
      id: 'sandrameyer',
      firstName: 'Sandra',
      lastName: 'Meyer',
      photo: 'assets/images/Avatar.jpg',
      companyName: '',
    }
  ];

  send = '';
  value = '';

  messageColor = '#4169e1';

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  constructor(private shareState: ShareState, public dialog: MatDialog) { }

  ngOnInit() {
    // this.shareState.getShares();
  }

  /*
  share() {
    if (this.photo) {
      this.photo.shares++;
    } else {
      this.photo.shares--;
    }
  }
  */

  message(templateRef) {
    const dialogRef = this.dialog.open(templateRef);
  }

  sendMessage() {
    if (this.messageColor === '#4169e1') {

      this.messageColor = '#4d4d4d';

    } else {

      this.messageColor = '#4169e1';
    }
  }

}
