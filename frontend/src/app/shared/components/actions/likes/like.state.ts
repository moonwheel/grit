import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { LikeModel } from './like.model';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})

export class LikeState {

  private url: string = `${environment.baseUrl}/likes`;

  private likeState = new BehaviorSubject<LikeModel[]>([]);

  likes = this.likeState.asObservable();

  constructor(private http: HttpClient) { }

  getLikes() {
    this.http.get<LikeModel[]>(this.url).subscribe(likes => { this.likeState.next(likes) });
  }

  getLike(id: number) {
    return this.http.get<LikeModel>(`${this.url}/${id}`);
  }

  addLike(like: LikeModel) {
    this.http.post<LikeModel>(this.url, like, httpOptions).subscribe(
      like => { this.likeState.next(this.likeState.getValue().concat([like])) });
  }

  deleteLike(id: number) {
    return this.http.delete<LikeModel>(`${this.url}/${id}`);
  }

}
