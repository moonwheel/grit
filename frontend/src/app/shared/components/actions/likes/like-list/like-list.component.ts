import { Component, Input, OnInit, Inject, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject, of } from 'rxjs';
import { AccountModel } from 'src/app/shared/models';
import { Router } from '@angular/router';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { UserState } from 'src/app/store/state/user.state';
import { FollowUser, UnfollowUser } from 'src/app/store/actions/user.actions';
import { tap, takeUntil, filter, map } from 'rxjs/operators';
import { UnfollowDialogComponent } from '../../follow/unfollow-dialog/unfollow-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { AbstractInfiniteListComponent } from '../../../abstract-infinite-list/abstract-infinite-list.component';
import { constants } from 'src/app/core/constants/constants';

export interface LikeListComponentData {
  id: number;
  State: typeof AbstractState;
  Actions: typeof AbstractActions;
  commentId?: number;
  reviewId?: number;
  reply?: boolean;
}

@Component({
  selector: 'app-like-list',
  templateUrl: './like-list.component.html',
  styleUrls: [ './like-list.component.scss' ],
})

export class LikeListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;

  total$: Observable<any>;
  entity$: Observable<any>;
  likes$: Observable<AccountModel[]>;
  likesLoading$: Observable<boolean>;
  virtualScrollHeight$: Observable<string>;

  page = 1;
  pageSize = 15;
  filter = 'date';
  itemSize = 60;
  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  likesMapping = {};

  constructor(private store: Store,
              @Inject(MAT_DIALOG_DATA)
              private config: LikeListComponentData,
              private router: Router,
              private dialogRef: MatDialogRef<LikeListComponent>,
              private dialog: MatDialog,
              private translateService: TranslateService,
              private zone: NgZone,
  ) {
    super();
  }

  ngOnInit() {
    const { id, State, Actions, commentId, reply } = this.getValidConfig();

    this.likesLoading$ = this.store.select(AbstractState.likesLoadingSelector(State));

    if (!commentId) {
      this.entity$ = this.store.select(AbstractState.getOneSelector(State, id));
      this.likes$ = this.store.select(AbstractState.likesSelector(State, id));

      this.store.dispatch(new Actions.LoadLikes(id));
    } else {
      this.entity$ = this.store.select(AbstractState.commentSelector(State, commentId, reply));
      this.likes$ = this.store.select(AbstractState.commentLikesSelector(State, commentId, reply));

      this.store.dispatch(new Actions.LoadCommentLikes(commentId, reply));
    }

    this.total$ = this.entity$.pipe(
      filter(item => !!item),
      map(item => item.likesCount)
    );

    this.virtualScrollHeight$ = this.createVirtualScrollHeightObservable(
      this.likes$,
      this.itemSize,
      5,
      this.zone,
      '60px'
    );

    this.translateService.stream('DIALOGS.LIKE_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.likesMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      const { id, Actions, commentId, reply } = this.getValidConfig();

      if (!commentId) {
        this.store.dispatch(new Actions.LoadLikes(id, this.page, this.pageSize));
      } else {
        this.store.dispatch(new Actions.LoadCommentLikes(commentId, reply, this.page, this.pageSize));
      }
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  like() {
    const { Actions, id, commentId, reply } = this.getValidConfig();

    if (!commentId) {
      this.store.dispatch(new Actions.Like(id));
    } else {
      this.store.dispatch(new Actions.LikeComment(commentId, reply));
    }
  }

  follow(account: AccountModel, event: Event) {
    event.stopPropagation();

    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  goToUserPage(user: AccountModel) {
    this.router.navigateByUrl(`${user.pageName}/home`).then(() => {
      this.dialogRef.close(true);
    });
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  private getValidConfig(): LikeListComponentData {
    if (
      !this.config ||
      !this.config.State ||
      !this.config.Actions
    ) {
      throw new Error('LikeListComponent dialog data should be provided');
    } else {
      return this.config;
    }
  }

}
