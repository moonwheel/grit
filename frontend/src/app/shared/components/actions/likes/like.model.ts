import { UserModel } from 'src/app/shared/models/user/user.model';

export interface LikeModel {
  id: number;
  user: UserModel;
  like: boolean;
  createdAt: Date;
  updatedAt?: Date;
}
