import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-not-logged-dialog',
  templateUrl: './not-logged-dialog.component.html',
})

export class NotLoggedDialogComponent {

  constructor(
    private dialogRef: MatDialogRef<NotLoggedDialogComponent>,
    private router: Router,
  ) { }

  close() {
    this.dialogRef.close();
  }

  goToPage(page: string) {
    if (page === 'register') {
      this.router.navigateByUrl('/register').then(() => {
        this.close();
      });
    } else {
      this.router.navigateByUrl('/').then(() => {
        this.close();
      });
    }
  }
}
