import { UserModel } from 'src/app/shared/models/user/user.model';

export interface ViewModel {
  id: number;
  user: UserModel;
  view: boolean;
  createdAt: Date;
}
