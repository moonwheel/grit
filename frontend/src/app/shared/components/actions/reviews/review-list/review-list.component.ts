import { Component, OnInit, Input, Inject, OnDestroy, AfterViewInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import {
  InitReviews,
  LoadReviews,
  DeleteReview,
  LikeReview,
  ResetReviews,
  AddReplyReview,
  DeleteReplyReview,
} from 'src/app/store/actions/review.actions';
import { AbstractInfiniteListComponent } from 'src/app/shared/components/abstract-infinite-list/abstract-infinite-list.component';
import { ReviewState } from 'src/app/store/state/review.state';
import { AccountModel } from 'src/app/shared/models';
import { constants } from 'src/app/core/constants/constants';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FollowUser, UnfollowUser } from 'src/app/store/actions/user.actions';
import { UserState } from 'src/app/store/state/user.state';
import { ReviewAddComponent } from '../review-add/review-add.component';
import { ReviewReplyComponent, ReviewReplyComponentData } from '../review-reply/review-reply.component';
import { ReviewSliderComponent } from '../review-slider/review-slider.component';
import { LikeListComponentData, LikeListComponent } from '../../likes/like-list/like-list.component';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { ReviewLikesComponent, ReviewLikesComponentData } from '../review-likes/review-likes.component';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

interface ReviewListDialogData {
  entityId: number;
  entity: string;
  rating: number;
  item: any;
  sellerRating?: number;
  State?: typeof AbstractState;
  Actions?: typeof AbstractActions;
}

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
})

export class ReviewListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(ReviewState.total)
  total$: Observable<number>;

  @Select(ReviewState.totalOne)
  totalOne$: Observable<number>;

  @Select(ReviewState.totalTwo)
  totalTwo$: Observable<number>;

  @Select(ReviewState.totalThree)
  totalThree$: Observable<number>;

  @Select(ReviewState.loading)
  loading$: Observable<boolean>;

  @Select(ReviewState.items)
  items$: Observable<any[]>;

  @Select(ReviewState.reviewed)
  reviewed$: Observable<any>;

  @Select(UserState.loggedInAccountId)
  loggedInAccountId$: Observable<number>;

  entityId: number = null;
  entity = 'product';
  rating = 0;
  item: any = null;

  product = {
    reviewsCount: 10,
    rating: 2.8
  };

  reviews = [];

  reviewsMapping = {};

  page = 1;
  pageSize = 15;
  searchValue = '';
  filter = 'all';
  orderBy = 'relevance';
  order = 'asc';

  truncating = true;

  likeColor = '#4d4d4d';

  followColor = '#4d4d4d';

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private data: ReviewListDialogData,
    private store: Store,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    super();

    this.entityId = data.item ? data.item.id : null;
    this.entity = data.entity;
    this.rating = data.item.rating || 0;
    this.item = data.item;
  }

  ngOnInit() {
    this.store.dispatch(new InitReviews(this.entity, this.entityId));

    this.translateService.stream('DIALOGS.REVIEW_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.reviewsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.store.dispatch(new ResetReviews());
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new LoadReviews(this.entity, this.entityId, this.page, this.pageSize, this.filter, this.orderBy));
    });
  }

  like(id: number) {
    this.store.dispatch(new LikeReview(this.entity, id));
  }

  showReviewReplies(reviewId: number) {
    const data: ReviewReplyComponentData = {
      entity: this.entity,
      reviewId,
    };

    this.dialog.open(ReviewReplyComponent, {
      width: '500px',
      data,
      panelClass: 'dialog-with-message-input-panel',
    });
  }

  showLikes(reviewId: number) {
    const data: ReviewLikesComponentData = {
      entity: this.entity,
      reviewId,
    };
    this.dialog.open(ReviewLikesComponent, { width: '500px', data });
  }

  follow(user: AccountModel) {
    if (user.following) {
      this.store.dispatch(new UnfollowUser(user.id));
    } else {
      this.store.dispatch(new FollowUser(user.id));
    }
  }

  filterStars(value: string) {
    this.filter = value;
    this.page = 1;
    this.store.dispatch(new LoadReviews(this.entity, this.entityId, this.page, this.pageSize, this.filter));
  }

  orderFilter(value: string) {
    this.page = 1;
    this.orderBy = value;
    this.order = this.order === 'asc' ? 'desc' : 'asc';
    this.store.dispatch(new LoadReviews(
      this.entity,
      this.entityId,
      this.page,
      this.pageSize,
      this.filter,
      this.orderBy,
      this.order,
    ));
  }

  addReview() {
    this.dialog.open(ReviewAddComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        item: this.item,
        entity: this.entity,
      },
    });
  }

  editReview(review: any) {
    this.dialog.open(ReviewAddComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        entity: this.entity,
        item: this.item,
        review,
      },
    });
  }

  deleteReview(id: number) {
    this.store.dispatch(new DeleteReview(this.entity, id));
  }

  replyReview(id: number) {
    this.dialog.open(ReviewReplyComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        entity: this.entity,
        reviewId: id,
      },
    });
  }

  editReply(id: number, reply: any) {
    this.dialog.open(ReviewReplyComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        entity: this.entity,
        reviewId: id,
        reply,
      },
    });
  }

  deleteReply(id: number, reviewId: number) {
    this.store.dispatch(new DeleteReplyReview(this.entity, reviewId, id));
  }

  getUserPhoto(user: AccountModel) {
    if (user.business) {
      return user.business.photo || constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
    }
  }

  getUserLink(user: AccountModel) {
    return `/${user.business ? user.business.pageName : user.person.pageName}`;
  }

  openImagesDialog(items: any[]) {
    this.dialog.open(ReviewSliderComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        items,
      },
    });
  }

  private getValidConfig() {
    if (
      !this.data ||
      !this.data.State ||
      !this.data.Actions
    ) {
      throw new Error('ReviewListComponentData should be provided');
    } else {
      return this.data;
    }
  }
}
