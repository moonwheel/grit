import { Component, Input, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';

import { UnfollowDialogComponent } from '../../follow/unfollow-dialog/unfollow-dialog.component';
import { UserState } from 'src/app/store/state/user.state';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { AccountModel } from 'src/app/shared/models';
import { LikeReview, LoadReviewLikes, LoadReviewReplyLikes } from 'src/app/store/actions/review.actions';
import { ReviewState, ReviewStateModel } from 'src/app/store/state/review.state';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

export interface ReviewLikesComponentData {
  entity: string;
  reviewId?: number;
  reply?: boolean;
}

@Component({
  selector: 'app-review-likes',
  templateUrl: './review-likes.component.html',
})

export class ReviewLikesComponent implements OnInit, OnDestroy {

  @Select(UserState.loggedInAccountId)
  loggedInAccountId$: Observable<number>;

  @Select(ReviewState.likes)
  likes$: Observable<AccountModel[]>;

  @Select(ReviewState.likesLoading)
  loading$: Observable<boolean>;

  review = {
    liked: false,
  };

  likesMapping = {
    '=0': 'No Likes',
    '=1': '1 Like',
    other: '# Likes'
  };

  private destroy$ = new Subject();

  constructor(
    private store: Store,
    @Inject(MAT_DIALOG_DATA)
    private config: ReviewLikesComponentData,
    private router: Router,
    private dialogRef: MatDialogRef<ReviewLikesComponent>,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    const { entity, reviewId, reply } = this.getValidConfig();
    if (reply) {
      this.store.dispatch(new LoadReviewReplyLikes(entity, reviewId));
    } else {
      this.store.dispatch(new LoadReviewLikes(entity, reviewId, reply));
    }

    this.store.select(ReviewState.getOne(reviewId)).subscribe(data => {
      this.review = data;
    });

    this.translateService.stream('DIALOGS.REVIEW_LIKES.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.likesMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  like() {
    const { entity, reviewId } = this.getValidConfig();
    this.store.dispatch(new LikeReview(entity, reviewId));
  }

  follow(account: AccountModel, event: Event) {
    event.stopPropagation();

    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  goToUserPage(user: AccountModel) {
    const link = user.business ? user.business.pageName : user.person.pageName;
    this.router.navigateByUrl(`${link}/home`).then(() => {
      this.dialogRef.close(true);
    });
  }

  getUserPhoto(user: AccountModel) {
    return user.business && user.business.photo || user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  private getValidConfig(): ReviewLikesComponentData {
    if (!this.config) {
      throw new Error('ReviewLikesComponent dialog data should be provided');
    }
    return this.config;
  }
}
