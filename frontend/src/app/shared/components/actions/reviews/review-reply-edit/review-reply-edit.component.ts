import { Component, Inject, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';

import { UserState } from 'src/app/store/state/user.state';
import { EditReplyReview } from 'src/app/store/actions/review.actions';
import { constants } from 'src/app/core/constants/constants';

export interface ReviewReplyEditComponentData {
  entity: string;
  reply: any;
}

@Component({
  selector: 'app-review-reply-edit',
  templateUrl: './review-reply-edit.component.html',
})

export class ReviewReplyEditComponent implements OnInit, OnDestroy {

  @Select(UserState.loggedInAccountPhoto)
  userPhoto$: Observable<string>;

  openedDialog: MatDialogRef<any>;

  form: FormGroup = this.fb.group({
    text: ['', Validators.required],
    mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
  });

  private destroy$ = new Subject();

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ReviewReplyEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    private config: ReviewReplyEditComponentData,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    const { reply } = this.getValidConfig();
    this.form.patchValue({
      text: reply.text,
      mentions: reply.mentions,
      hashtags: reply.hashtags,
    });
    this.dialogRef.disableClose = true;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.openedDialog = this.dialog.open(templateRef);
  }

  closeOpenedDialog() {
    this.openedDialog.close();
  }

  editReply() {
    const { entity, reply } = this.getValidConfig();

    if (this.form.valid) {
      const { text, mentions, hashtags } = this.form.value;
      this.store.dispatch(new EditReplyReview(entity, reply.id, {
        id: reply.id,
        text,
        mentions,
        hashtags,
      })).subscribe(() => {
        this.dialogRef.close();
        this.form.reset({
          mentions: [],
          hashtags: [],
        });
      });
    }
  }

  close() {
    this.dialogRef.close();
  }

  private getValidConfig(): ReviewReplyEditComponentData {
    if (!this.config ) {
      throw new Error('ReviewReplyEditComponentData should be provided');
    } else {
      return this.config;
    }
  }
}
