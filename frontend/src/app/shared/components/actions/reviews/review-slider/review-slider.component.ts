import { Component, Optional, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { constants } from 'src/app/core/constants/constants';

export interface ReviewSliderDialogData {
  items: any[];
}

@Component({
  selector: 'app-review-slider',
  templateUrl: './review-slider.component.html'
})

export class ReviewSliderComponent implements OnInit {

  images: any[] = [];

  config: SwiperConfigInterface = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
      bulletActiveClass: 'swiper-pagination__active'
    }
  };

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(
    @Optional()
    @Inject(MAT_DIALOG_DATA)
    private data: ReviewSliderDialogData,
  ) { }

  ngOnInit() {
    if (this.data && this.data.items) {
      this.images = this.data.items;
    }
  }

  getPhotosSource() {
    if (this.images.length) {
      return this.images;
    }
    return [{
      link: 'images/photo-placeholder.png',
    }];
  }
}
