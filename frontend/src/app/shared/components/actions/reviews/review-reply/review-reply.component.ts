import { Component, OnInit, Inject, TemplateRef, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UserState } from 'src/app/store/state/user.state';
import { SaveDialogComponentData, SaveDialogComponent } from 'src/app/shared/components/save-dialog/save-dialog.component';
import { AccountModel } from 'src/app/shared/models';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { ReviewLikesComponentData, ReviewLikesComponent } from '../review-likes/review-likes.component';
import { ReviewState } from 'src/app/store/state/review.state';
import { LoadReviewReplies, LikeReview, AddReplyReview, DeleteReplyReview, LikeReplyReview } from 'src/app/store/actions/review.actions';
import { UnfollowDialogComponent } from '../../follow/unfollow-dialog/unfollow-dialog.component';
import { constants } from 'src/app/core/constants/constants';
import { ReviewAddComponent } from '../review-add/review-add.component';
import { ReviewReplyEditComponent } from '../review-reply-edit/review-reply-edit.component';

export interface ReviewReplyComponentData {
  entity: string;
  reviewId: number;
  item?: any;
}

@Component({
  selector: 'app-review-reply',
  templateUrl: './review-reply.component.html',
})

export class ReviewReplyComponent implements OnInit, OnDestroy {

  @Select(UserState.loggedInAccountPhoto)
  userPhoto$: Observable<string>;

  @Select(UserState.loggedInAccount)
  loggedInAccount$: Observable<any>;

  @Select(ReviewState.replies)
  replies$: Observable<any[]>;

  @Select(ReviewState.repliesLoading)
  loading$: Observable<boolean>;

  truncating = true;

  openedDialog: MatDialogRef<any>;

  review = null;

  repliesLoading$: Observable<boolean>;

  form: FormGroup = this.fb.group({
    text: ['', Validators.required],
    mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
  });

  private destroy$ = new Subject();

  constructor(
    private store: Store,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ReviewReplyComponent>,
    @Inject(MAT_DIALOG_DATA)
    private config: ReviewReplyComponentData,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    const { entity, reviewId } = this.getValidConfig();

    this.store.dispatch(new LoadReviewReplies(entity, reviewId));

    this.store.select(ReviewState.getOne(reviewId)).subscribe(data => {
      this.review = data;
    });

    this.dialogRef.disableClose = true;

    this.dialogRef.backdropClick().pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.close();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  likeReview() {
    const { entity, reviewId } = this.getValidConfig();
    this.store.dispatch(new LikeReview(entity, reviewId));
  }

  showReviewLikes() {
    this.showLikesModal(false);
  }

  likeReply(replyId: number) {
    const { entity } = this.getValidConfig();
    this.store.dispatch(new LikeReplyReview(entity, replyId));
  }

  showReplyLikes(replyId: number) {
    this.showLikesModal(true);
  }

  openEditReviewDialog(review: any) {
    const { entity, item } = this.getValidConfig();
    this.dialog.open(ReviewAddComponent, {
      panelClass: ['full-screen-dialog'],
      disableClose: true,
      data: {
        entity,
        item,
        review,
      },
    });
  }

  openEditReplyDialog(reply: any) {
    const { entity, item } = this.getValidConfig();
    this.dialog.open(ReviewReplyEditComponent, {
      disableClose: true,
      width: '500px',
      data: {
        entity,
        reply,
      },
    });
  }

  addReply() {
    const { entity, reviewId } = this.getValidConfig();
    const { text, mentions, hashtags } = this.form.value;
    this.store.dispatch(new AddReplyReview(entity, reviewId, {
      text,
      mentions,
      hashtags,
    })).subscribe(() => {
      this.form.reset({
        hashtags: [],
        mentions: [],
      });
    });
  }

  goToUserPage(user: AccountModel) {
    const link = user.business ? user.business.pageName : user.person.pageName;
    this.router.navigateByUrl(`${link}/home`).then(() => {
      this.dialogRef.close(true);
    });
  }

  getUserPhoto(user: AccountModel) {
    return user.business && user.business.photo || user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.openedDialog = this.dialog.open(templateRef);
  }

  closeOpenedDialog() {
    this.openedDialog.close();
  }

  deleteReply(replyId: number) {
    const { entity, reviewId } = this.getValidConfig();

    this.store.dispatch(new DeleteReplyReview(entity, reviewId, replyId)).subscribe(() => {
      if (this.openedDialog) {
        this.openedDialog.close();
      }
    });
  }

  close() {
    this.dialogRef.close();
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  follow(account: AccountModel, event: Event) {
    event.stopPropagation();

    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  reportComment() {
    // TODO Implement when reports are done
    console.warn('Method is not implemented');
  }

  private getValidConfig(): ReviewReplyComponentData {
    if (!this.config) {
      throw new Error('ReviewReplyComponent dialog data should be provided');
    } else {
      return this.config;
    }
  }

  private showLikesModal(isReply = false) {
    const { entity, reviewId } = this.getValidConfig();

    const data: ReviewLikesComponentData = {
      reviewId,
      reply: isReply,
      entity,
    };

    this.dialog.open(ReviewLikesComponent, {
      width: '500px',
      data,
    }).afterClosed().subscribe(shouldClose => {
      if (shouldClose) {
        this.dialogRef.close();
      }
    });
  }
}
