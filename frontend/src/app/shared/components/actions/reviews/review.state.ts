import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ReviewModel } from './review.model';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})

export class ReviewState {

  private url: string = `${environment.baseUrl}/reviews`;

  private reviewState = new BehaviorSubject<ReviewModel[]>([]);

  reviews = this.reviewState.asObservable();

  constructor(private http: HttpClient) { }

  getReviews() {
    this.http.get<ReviewModel[]>(this.url).subscribe(reviews => { this.reviewState.next(reviews) });
  }

  getReview(id: number) {
    return this.http.get<ReviewModel>(`${this.url}/${id}`);
  }

  addReview(review: ReviewModel) {
    this.http.post<ReviewModel>(this.url, review, httpOptions).subscribe(
      review => { this.reviewState.next(this.reviewState.getValue().concat([review])) });
  }

  updateReview(review: ReviewModel) {
    return this.http.put<ReviewModel>(`${this.url}/${review.id}`, review, httpOptions);
  }

  deleteReview(id: number) {
    return this.http.delete<ReviewModel>(`${this.url}/${id}`);
  }

}
