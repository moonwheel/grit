import { Component, OnInit, Input, Inject, Optional } from '@angular/core';

@Component({
  selector: 'rating-stars',
  templateUrl: './rating-stars.component.html'
})

export class RatingStarsComponent {

  @Input()
  rating: number;

  @Input()
  color: 'blue' | 'yellow' = 'blue';
}
