import { UserModel } from 'src/app/shared/models/user/user.model';

export interface ReviewModel {
  id: number;
  user: UserModel;
  review: string;
  files: File[]; // only for Products & Services not for Seller Review
  createdAt: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
