import { Component, OnInit, Input, Inject, Optional, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { Store } from '@ngxs/store';

import { AddReview, EditReview } from 'src/app/store/actions/review.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { constants } from 'src/app/core/constants/constants';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { map, catchError, takeUntil } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

export interface ReviewAddDialogData {
  entity: string;
  review?: any;
  item: any;
}

@Component({
  selector: 'app-review-add',
  templateUrl: './review-add.component.html',
  styleUrls: ['./review-add.component.scss']
})

export class ReviewAddComponent implements OnInit, OnDestroy {

  item: any = null;

  review: any = null;

  cover = constants.PLACEHOLDER_NO_PHOTO_PATH;

  entity: string;

  photosMapping = {};

  reviewForm: FormGroup;

  reviewFile: File = null;

  photosLoading = false;

  canImmediatelyDeactivate = false;

  reviewColor = '#4d4d4d';

  reviewSellerColor = '#4d4d4d';

  maxPhotoNumber = 6;

  invalidImages = false;

  isInputValueChanged = false;

  deletedPhotos = [];

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  barRatingTitles$ = this.translateService.stream('DIALOGS.REVIEW_ADD.BAR_RATING_TITLES').pipe(
    map((titles: any) => [titles.NO, titles.PARTLY, titles.YES])
  );

  get photos(): Array<any> {
    const photos = this.reviewForm.get('photos');
    return photos ? photos.value : [];
  }

  set photos(value: Array<any>) {
    this.reviewForm.get('photos').setValue(value);
  }

  private destroy$ = new Subject();

  constructor(
    private dialogRef: MatDialogRef<ReviewAddComponent>,
    private store: Store,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    @Optional()
    @Inject(MAT_DIALOG_DATA)
    private data: ReviewAddDialogData,
  ) { }

  ngOnInit() {
    this.reviewForm = this.formBuilder.group({
      text: ['', Validators.required],
      rating: [null, Validators.required],
      photos: [[]],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
    });

    this.translateService.stream('DIALOGS.REVIEW_ADD.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.photosMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });

    if (this.data) {

      if (this.data.review) {
        this.reviewForm.patchValue({
          text: this.data.review.text,
          rating: this.data.review.rating,
          photos: this.data.review.photo.map(item => ({ id: item.id, photoUrl: item.link })),
        });
      }

      this.entity = this.data.entity;

      this.item = this.data.item;

      if (this.entity === 'seller') {
        this.cover = this.item.business ? this.data.item.business.photo : this.data.item.person.photo;
      } else if (this.entity === 'product') {
        if (this.item.product.selectedVariant) {
          this.cover = this.item.product.selectedVariant.cover.photo.photoUrl;
        } else if (this.item.product.photos.length) {
          this.cover = this.item.product.photos[0].photoUrl;
        } else {
          this.cover = constants.PLACEHOLDER_NO_PHOTO_PATH;
        }
      } else if (this.entity === 'service') {
        this.cover = this.item.photoCover.thumb;
      }
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.onload = (loadEvent: any) => {
        this.reviewFile = loadEvent.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  removeFile(event) {
    event.stopPropagation();
    this.reviewFile = null;
  }

  addReview() {
    const photoFiles = this.photos
        .map(({ photoFile }, index) => ({ file: photoFile, index, uniqueId: photoFile ? photoFile.name : null }))
        .filter(item => !!item.file);

    const dataToUpload = photoFiles.length ? photoFiles : null;

    let observable = null;

    if (this.reviewForm.valid) {

      if (this.data && this.data.review) {
        observable = this.store.dispatch(new EditReview(this.entity, {
          id: this.data.review.id,
          text: this.reviewForm.get('text').value,
          rating: this.reviewForm.get('rating').value,
          mentions: this.reviewForm.get('mentions').value,
        }, dataToUpload));

        this.close();
      } else {
        observable = this.store.dispatch(new AddReview(this.entity, this.item.id, {
          text: this.reviewForm.get('text').value,
          rating: this.reviewForm.get('rating').value,
          mentions: this.reviewForm.get('mentions').value,
          itemId: this.item.id,
        }, dataToUpload));

        this.close();
      }
    }

    return observable && observable.pipe(
      map(() => this.canImmediatelyDeactivate = true),
      catchError(() => of(false)),
    );
  }

  close() {
    this.dialogRef.close();
  }

  async selectPhotos(event: Event): Promise<void> {
    const target = event.target as HTMLInputElement;
    const files = target.files;

    if (files && files.length) {
      this.photosLoading = true;

      const promises = Array.from(files).map(async (file) => {
        const { url } = await Utils.getImageData(file);

        if (this.reviewForm.get('photos').value.length < this.maxPhotoNumber) {
          const currentPhotos = this.reviewForm.get('photos').value;
          currentPhotos.push({
            photoFile: file,
            photoUrl: url,
            index: this.reviewForm.get('photos').value.length
          });
          this.reviewForm.get('photos').setValue(currentPhotos);
        }
      });

      await Promise.all(promises);

      this.photosLoading = false;

      target.value = null;
    }
  }

  reorderImages(event: CdkDragDrop<string[]>): void {
    const photos = [ ...this.photos ];
    moveItemInArray(photos, event.previousIndex, event.currentIndex);
    this.photos = photos;
  }

  removePhotoByIndex(index: number): void {
    if (this.photos[index]) {
      this.deletedPhotos.push(this.photos[index]);
      this.photos = this.photos.filter((photo, idx) => index !== idx);
    }
  }

  getDefaultImage() {
    if (this.entity === 'seller') {
      return constants.PLACEHOLDER_AVATAR_PATH;
    } else {
      return this.defaultImage;
    }
  }

  getUserName(user) {
    return user.business ? user.business.name : user.person.fullName;
  }
}
