import {
  Component,
  OnInit,
  forwardRef,
  Input,
  Optional,
  Injector,
  OnDestroy,
  ViewChild,
  AfterViewInit,
  ElementRef
} from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FloatLabelType } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, FormControl, FormGroupDirective } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Subject, BehaviorSubject, of, fromEvent, NEVER } from 'rxjs';
import { debounceTime, takeUntil, distinctUntilChanged, switchMap, map, tap, catchError, take, filter } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { CustomErrorStateMatcher } from '../../utils/custom-error-state-matcher';
import { TranslateService } from '@ngx-translate/core';
import { SearchService } from '../../services/search.service';

export interface LocationSuggestion {
  title?: string;
  address_location: string;
  address_id?: number;
}

@Component({
  selector: 'app-location-input',
  templateUrl: './location-input.component.html',
  styleUrls: [ './location-input.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LocationInputComponent),
      multi: true,
    }
  ],
})
export class LocationInputComponent implements OnInit, OnDestroy, AfterViewInit, ControlValueAccessor {
  @Input() placeholder = '';
  @Input() autofocus = false;
  @Input() floatLabel: FloatLabelType = 'auto';
  @Input() businessAddressFormControlName = 'business_address';

  @ViewChild('locationInput', { static: true }) locationInput: ElementRef<HTMLInputElement>;

  disabled = false;
  control: NgControl;
  matcher: CustomErrorStateMatcher;
  businessAddressFormControl: FormControl;
  internalInputControl = new FormControl('');
  value = '';

  autosuggestions$: BehaviorSubject<LocationSuggestion[]> = new BehaviorSubject([]);

  propagateChange: (arg: any) => void;
  propagateTouched: (arg: any) => void;

  get errors() {
    return this.control && this.control.errors;
  }

  private destroy$ = new Subject();

  constructor(private store: Store,
              @Optional()
              private injector: Injector,
              private userService: UserService,
              private searchService: SearchService,
              private matSnackBar: MatSnackBar,
              private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.control = this.injector.get(NgControl, null);
    this.matcher = new CustomErrorStateMatcher(this.control);
  }

  ngAfterViewInit() {
    this.setupSearchInput();
    this.setupTouchedEvent();
    const form = this.getParentFormGroup();
    if (form) {
      this.businessAddressFormControl = form.control.get(this.businessAddressFormControlName) as FormControl;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(value: any): void {
    this.internalInputControl.patchValue({ address_location: value }, { emitEvent: false });
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  setupSearchInput() {
    this.internalInputControl.valueChanges.pipe(
      debounceTime(100),
      filter(value => typeof value === 'string'),
      map(value => value.trim()),
      distinctUntilChanged(),
      tap(() => {
        this.value = null;
        this.propagateChange(null);
        this.setBusinessAddressId(null);
      }),
      switchMap(searchTerm => {
        if (!searchTerm) {
          return of(null);
        }

        return this.searchService.getPlacesAutocomplete(searchTerm).pipe(
          catchError(() => of(null))
        );
      }),
      takeUntil(this.destroy$),
    ).subscribe(data => {
      if (data) {
        const suggestions: LocationSuggestion[] = [];

        for (const business of data.business.suggest.text_business_name) {
          for (const { _source } of business.options) {
            suggestions.push({
              title: _source.text_business_name,
              address_location: _source.address,
              address_id: _source.address_id
            });
          }
        }

        if ('results' in data.tomTom) {
          for (const { address: { freeformAddress } } of data.tomTom.results) {
            suggestions.push({
              address_location: freeformAddress,
            });
          }
        }

        this.autosuggestions$.next(suggestions);
      } else {
        this.autosuggestions$.next([]);
      }
    });
  }

  setupTouchedEvent() {
    fromEvent(this.locationInput.nativeElement, 'input').pipe(
      take(1),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.propagateTouched(true);
    });
  }

  getSuggestionText(suggestion: LocationSuggestion) {
    return suggestion && (suggestion.title || suggestion.address_location);
  }

  clear() {
    this.internalInputControl.setValue(null);
    this.propagateChange(null);
    this.setBusinessAddressId(null);
  }

  onSuggestionSelect(suggestion: MatAutocompleteSelectedEvent) {
    const { address_id, address_location, title } = suggestion.option.value as LocationSuggestion;

    if (address_id) {
      this.value = title;
      this.setBusinessAddressId(address_id);
    } else {
      this.value = address_location;
      this.setBusinessAddressId(null);
    }

    this.propagateChange(this.value);
  }

  isClearButtonVisible() {
    return typeof this.internalInputControl.value === 'object' && this.internalInputControl.value
      ? this.internalInputControl.value.address_location
      : this.internalInputControl.value;
  }

  private setBusinessAddressId(id: number) {
    if (this.businessAddressFormControl) {
      this.businessAddressFormControl.setValue(id);
    }
  }

  private getParentFormGroup() {
    const formGroup = this.injector.get(FormGroupDirective, null);
    if (!formGroup) {
      console.warn('app-location-input component cant be used outside FormGroupDirective');
    }
    return formGroup;
  }
}
