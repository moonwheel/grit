import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { UnfollowDialogComponent } from '../actions/follow/unfollow-dialog/unfollow-dialog.component';
import { Observable } from 'rxjs';
import { AbstractEntityModel, AccountModel } from '../../models';
import { filter, map } from 'rxjs/operators';
import { Output, EventEmitter, TemplateRef, Directive } from '@angular/core';
import { AbstractActions } from 'src/app/store/actions/abstract.actions';
import { AbstractState } from 'src/app/store/state/abstract.state';
import { LikeListComponentData, LikeListComponent } from '../actions/likes/like-list/like-list.component';
import { CommentListComponent, CommentListComponentData } from '../actions/comments/comment-list/comment-list.component';
import { constants } from 'src/app/core/constants/constants';

@Directive()
export abstract class AbstractFeedItemComponent {
  abstract item: AbstractEntityModel;
  abstract Actions: typeof AbstractActions;
  abstract State: typeof AbstractState;

  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;

  @Output() delete: EventEmitter<void> = new EventEmitter();

  isMyAccount$ = this.loggedInAccount$.pipe(
    filter(item => Boolean(item)),
    map(account => account.id === this.item.user.id),
  );

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(protected dialog: MatDialog,
              protected store: Store,
  ) {
  }

  follow() {
    this.store.dispatch(new FollowUser(this.item.user.id)).subscribe(() => {
      this.item.user.following = true;
    });
  }

  unfollow() {
    const account = this.item.user;
    this.dialog.open(UnfollowDialogComponent, { data: { account } })
      .afterClosed()
      .subscribe(success => {
        if (success) {
          this.item = {
            ...this.item,
            user: {
              ...this.item.user,
              following: false
            }
          };
        }
      });
  }

  like(): void {
    this.store.dispatch(new this.Actions.Like(this.item.id));
  }

  showLikes() {
    const data: LikeListComponentData = {
      id: this.item.id,
      State: this.State,
      Actions: this.Actions,
    };
    this.dialog.open(LikeListComponent, { data });
  }

  showComments() {
    const data: CommentListComponentData = {
      id: this.item.id,
      State: this.State,
      Actions: this.Actions,
    };
    this.dialog.open(CommentListComponent, {
      data,
      panelClass: 'dialog-with-message-input-panel',
    });
  }

  share() {
  }

  link() {
  }

  bookmark() {
  }

  openDialog(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  deleteItem(): void {
    this.store.dispatch(new this.Actions.Delete(this.item.id));
    this.delete.emit();
  }
}
