import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Store, Select } from '@ngxs/store';
import { Subject, Observable } from 'rxjs';

import { BankAccountModel } from 'src/app/shared/models';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { LoadPayments } from 'src/app/store/actions/payment.actions';

@Component({
  selector: 'app-payout-dialog',
  templateUrl: './payout-dialog.component.html',
  styleUrls: ['./payout-dialog.component.scss']
})

export class PayoutDialogComponent implements OnInit, OnDestroy {

  @Select(PaymentsState.payments)
  payments$: Observable<any[]>;

  protected destroy$ = new Subject();

  items: BankAccountModel[] = [];

  payOutForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<PayoutDialogComponent>,
    private store: Store,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.payOutForm = this.fb.group({
      amount: ['', Validators.required],
      bank: ['', Validators.required],
    });

    this.store.dispatch(new LoadPayments());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  payout() {
    if (this.payOutForm.valid) {
      this.dialogRef.close();
    }
  }
}
