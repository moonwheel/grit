import {
  Component,
  OnInit,
  forwardRef,
  Input,
  Optional,
  Injector,
  OnDestroy,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { FloatLabelType } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Store } from '@ngxs/store';
import {Subject, BehaviorSubject, of, fromEvent, Observable} from 'rxjs';
import { MentionConfig, MentionDirective } from 'angular-mentions';
import {
  debounceTime,
  takeUntil,
  distinctUntilChanged,
  switchMap,
  map,
  tap,
  skip
} from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { AccountModel, MentionModel } from '../../models';
import { Utils } from '../../utils/utils';
import { CustomErrorStateMatcher } from '../../utils/custom-error-state-matcher';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-input-with-mentions',
  templateUrl: './input-with-mentions.component.html',
  styleUrls: [ './input-with-mentions.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputWithMentionsComponent),
      multi: true,
    }
  ],
})
export class InputWithMentionsComponent implements OnInit, OnDestroy, AfterViewInit, ControlValueAccessor {

  @Input() placeholder = '';
  @Input() autofocus = false;
  @Input() materialDesign = true;
  @Input() inputClass = '';
  @Input() floatLabel: FloatLabelType = 'auto';
  @Input() mentionsFormControlName = 'mentions';
  @Input() hashtagsFormControlName = 'hashtags';

  @ViewChild(MentionDirective) mentionDirective: MentionDirective;

  value: string;
  disabled = false;
  control: NgControl;
  matcher: CustomErrorStateMatcher;
  mentionsFormControl: FormControl;
  hashtagsFormControl: FormControl;
  mentionAutosuggestions$: BehaviorSubject<AccountModel[]> = new BehaviorSubject([]);
  suggestions: string[] = [];
  isFocused = false;
  mentionsConfig: MentionConfig = {
    labelKey: 'name',
    allowSpace: true,
    mentionSelect: this.onMentionSelect.bind(this)
  };

  // Was tried to implement  config for multiple mentions but Suggestions for # appeared with delay and from the second attempt
 /* mentionsConfig: MentionConfig = {
   mentions: [
     {
       items: [],
       labelKey: 'text',
       allowSpace: true,
       triggerChar: '#',
       returnTrigger: true,
       mentionSelect: this.onHashtagSelect.bind(this)
     },
     {
       items: [],
       labelKey: 'name',
       allowSpace: true,
       triggerChar: '@',
       mentionSelect: this.onMentionSelect.bind(this),
     }
     ]
  };*/
  mentions: MentionModel[] = [];

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  propagateChange: (arg: any) => void;
  propagateTouched: (arg: any) => void;
  textFromControl = '';

  get errors() {
    return this.control && this.control.errors;
  }

  private destroy$ = new Subject();

  constructor(private store: Store,
              @Optional()
              private injector: Injector,
              private userService: UserService,
              private searchService: SearchService,
              private matSnackBar: MatSnackBar,
              private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.control = this.injector.get(NgControl, null);
    this.matcher = new CustomErrorStateMatcher(this.control);
    this.textFromControl = '';
  }

  ngAfterViewInit() {
    const form = this.getParentFormGroup();

    if (form) {
      this.mentionsFormControl = form.control.get(this.mentionsFormControlName) as FormControl;

      if (this.mentionsFormControl) {
        this.setupMentionSuggestionsSubscription();
        this.setupMentionsChangeSubscription();
      }

      this.hashtagsFormControl = form.control.get(this.hashtagsFormControlName) as FormControl;

      if (this.hashtagsFormControl) {
        this.setupHashtagsChangeSubscription();
      }
    }

    this.setupFormSubmitSubscription();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  setupMentionSuggestionsSubscription() {
    if (this.mentionDirective) {
      this.mentionDirective.searchTerm.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(searchTerm => {
          if (!searchTerm) {
            return of([]);
          }
          const mentions = this.mentionsFormControl.value || [];
          const exceptIds = mentions.map(item => item.target.id);
          return this.userService.search(searchTerm, 1, 20, exceptIds);
        }),
        map(users => users.map(Utils.extendAccountWithBaseInfo)),
        takeUntil(this.destroy$),
      ).subscribe(users => {
        this.mentionAutosuggestions$.next(users);
      });
    }
  }

  setupMentionsChangeSubscription() {
    if (this.control && this.mentionsFormControl) {
      this.control.valueChanges.pipe(
        debounceTime(200),
        takeUntil(this.destroy$),
      ).subscribe((text: string) => {
        const mentions = this.mentionsFormControl.value || [];
        console.log('mentions', mentions);
        const directed = mentions.filter(mention => text.match(mention.mention_text));
        console.log('directed', directed);
        this.mentionsFormControl.setValue(directed);
      });
    }
  }

  setupHashtagsChangeSubscription() {
    if (this.control && this.hashtagsFormControl) {
      this.control.valueChanges.pipe(
        skip(1),
        debounceTime(200),
        takeUntil(this.destroy$),
        tap((text) => {
            this.textFromControl = '';
            const hashtags = text && text.match(new RegExp(constants.HASHTAGS_REGEXP_TEXT, 'g')) || [];
            this.hashtagsFormControl.setValue(hashtags);
            if (hashtags.length) {
              this.textFromControl = hashtags.pop();
        }
    }),
        switchMap(() => {
         return this.searchService.autosuggest(this.textFromControl);
        })
      ).subscribe((result) => {
       if (result[0] && result[0].options && result[0].options.length) {
          this.suggestions = result[0].options.map(option => ({...option, count: option._source.count}));
        }
      });
    }
  }

  /**
   * Used only for invalid message indication on submit.
   * Can cause unexpected side effects if you want to post form data
   * not on form submit
   */
  setupFormSubmitSubscription() {
    const form = this.getParentFormGroup();

    if (form && this.mentionsFormControl) {
      form.ngSubmit.pipe(
        takeUntil(this.destroy$)
      ).subscribe(() => {
        if (this.mentionsFormControl) {
          const mentions = this.mentionsFormControl.value || [];
          if (mentions.length > constants.MAX_MENTIONS_NUMBER) {
            const num = constants.MAX_MENTIONS_NUMBER;
            this.translateService.get('ERRORS.MAX_MENTIONS', { num }).subscribe(message => {
              this.matSnackBar.open(message);
            });
          }
        }

        if (this.hashtagsFormControl) {
          const hashtags = this.hashtagsFormControl.value || [];
          if (hashtags.length > constants.MAX_HASHTAGS_NUMBER) {
            const num = constants.MAX_HASHTAGS_NUMBER;
            this.translateService.get('ERRORS.MAX_HASHTAGS', { num }).subscribe(message => {
              this.matSnackBar.open(message);
            });
          }
        }
      });
    }
  }

  onMentionSelect(account: AccountModel) {
    const mentionText = `@${account.name}`;

    if (this.mentionsFormControl) {
      const mentions = this.mentionsFormControl.value || [];
      const found = mentions.find(item => item.mention_text === mentionText);

      if (!found) {
        const newMention: MentionModel = {
          id: null,
          target: account,
          mention_text: mentionText,
        };
        mentions.push(newMention);
      }
      this.mentionsFormControl.setValue(mentions);
    }

    this.mentionAutosuggestions$.next([]);

    return mentionText;
  }

  onHashtagSelect(event, hashtag) {
    if (this.hashtagsFormControl) {
      const hashtags = this.hashtagsFormControl.value || [];
      const found = hashtags.find(item => item === hashtag.text);

      if (!found) {
        // add new hashtag
        hashtags.push(hashtag.text);
      }
      this.hashtagsFormControl.setValue(hashtags);
      this.value = this.value.substring(0, this.value.lastIndexOf(this.textFromControl)) + hashtag.text;
      this.suggestions = [];
      this.textFromControl = '';
    }
  }

  onModelChange($event: any) {
    this.propagateChange($event);
    this.propagateTouched(true);
  }

  private getParentFormGroup() {
    const formGroup = this.injector.get(FormGroupDirective, null);
    if (!formGroup) {
      console.warn('app-input-with-mentions component cant be used outside FormGroupDirective');
    }
    return formGroup;
  }

  onFocus(event) {
    if (event) {
      if (event.type === 'focus'){
        return this.isFocused = true;
      }
    }
  }
}
