import {
  Component,
  ChangeDetectionStrategy,
  HostBinding,
  ViewChild,
  OnInit
} from '@angular/core'
import { MatFormFieldControl } from '@angular/material/form-field'
import { _MatQuillBase } from './mat-quill-base'
import 'quill-mention'
import { map, debounceTime } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Utils } from '../../utils/utils';
import { AccountModel } from '../../models';

const MENTION_CHAR = '@';

interface QuillMention {
  id: number; //user id
  value: string; // display name
  denotationChar: string;
  link: string;
}

interface MentionBag {
  query: string;
  renderList: (items: QuillMention[]) => void;
  mentionChar: string;
}

// Increasing integer for generating unique ids for mat-quill components.
let nextUniqueId = 0

const SELECTOR = 'mat-quill'

@Component({
  selector: SELECTOR,
  exportAs: 'matQuill',
  template: `
    <ng-content select="[quill-editor-toolbar]"></ng-content>
    <ng-content></ng-content>
  `,
  inputs: ['disabled'],
  providers: [{ provide: MatFormFieldControl, useExisting: MatQuill }],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatQuill extends _MatQuillBase implements OnInit {
  controlType = SELECTOR
  @HostBinding() id = `${SELECTOR}-${nextUniqueId++}`

  static ngAcceptInputType_disabled: boolean | string | null | undefined
  static ngAcceptInputType_required: boolean | string | null | undefined

  searchBehavior$ = new BehaviorSubject<MentionBag>({} as any);


  content = ''
  matContent = ''

  mentions: any[] = [];

  modules = {
    mention: {
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
      mentionDenotationChars: [MENTION_CHAR],
      showDenotationChar: false,
      dataAttributes: ['id', 'link', 'denotationChar', 'value'],
      isolateCharacter: true,
      onSelect: (item, insertItem) => {
        const editor = this.quillEditor
        insertItem(item, true)
        // necessary because quill-mention triggers changes as 'api' instead of 'user'
        editor.insertText(editor.getLength() - 1, '', 'user')
      },
      source: (searchTerm, renderList, mentionChar) => {
        this.searchBehavior$.next({ query: searchTerm, renderList, mentionChar })
        return;
      }
    },
    toolbar: []
  }

  ngOnInit() {
    this.getMentionHandler().subscribe(this.mentionHandler);
  }

  getMentionHandler() {
    return this.searchBehavior$.pipe(debounceTime(150), map(bag => bag));
  }

  mentionHandler = (bag: MentionBag) => {
    const mentions = this.getMentionsListFromTree(this.value);
    const excludeIds = mentions.map(mention => mention.id);

    this.userService.search(bag.query, 1, 20, excludeIds)
      .pipe(map(users => users.map(Utils.extendAccountWithBaseInfo)))
      .subscribe(users => {
        if(!bag.renderList) return;

        let mentionsToRender: QuillMention[] = [];

        mentionsToRender = this.mapUsersToMention(users)
        return bag.renderList(mentionsToRender);
      })
  }

  getMentionsListFromTree = (value: string): QuillMention[] => {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = value;

    const mentions = wrapper.getElementsByClassName('mention');

    return Array.from(mentions).map(mentionElement => {
      const id = mentionElement.getAttribute('data-id');
      const denotationChar = mentionElement.getAttribute('data-denotation-char');
      const value = mentionElement.getAttribute('data-value');
      const link = mentionElement.getAttribute('data-lini');

      return {
        id: +id,
        denotationChar,
        value,
        link
      }
    })
  }

  private mapUsersToMention(users: AccountModel[]): QuillMention[] {
    return users.map(user => ({ id: user.id, value: `${MENTION_CHAR}${user.name}`, link: Utils.getAccountPageUrl(user), denotationChar: MENTION_CHAR }));
  }
}
