import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: 'app-accordion-panel',
    styleUrls: ['./accordion-panel.component.scss'],
    templateUrl: './accordion-panel.component.html'
}) 
export class AccordionComponent implements OnInit{
    private _opened: boolean = false;

    @Input('initialOpened') initialOpened: boolean = false;
    @Input('title') title: string = '';
    @Input('icon') icon: string = '';
    @Input('disabled') disabled: boolean = false; 

    get opened() {
        return this._opened;
    }

    set opened(opened: boolean) {
        this._opened = opened;
    }

    ngOnInit() {
        this._opened = this.initialOpened;
    }

    toggle() {
        if(this.disabled) return;

        this.opened = !this.opened;
    }
}