import { Component, Input, OnInit } from "@angular/core";
import { OrderModel } from "../../models";
import { SHIPPED_STATUS, CANCELLED_STATUS, REFUNDED_STATUS, APPOINTMENT_STATUS } from "../order-list-item/order-list-item.component";

@Component({
    selector: 'app-booking-list-item',
    templateUrl: './booking-list-item.component.html'
})
export class BookingListItem implements OnInit {
    @Input() order: OrderModel;
    @Input() filter: string = 'all';
    @Input() type: 'purchase' | 'sale';

    ngOnInit() {
        if (!this.order) throw new Error('Order is required prop.');
    }

    pickTitle(order) {
        return (order as any).service.title;
    }

    pickImageUrl(order) {
        return (order as any).service.photoCover.thumb;
    }

    getOrderStatus(order: OrderModel) {
        const { shipped, cancelled, refunded } = order;

        if (shipped) return SHIPPED_STATUS;
        if (refunded) return REFUNDED_STATUS;
        if (cancelled) return CANCELLED_STATUS;


        return APPOINTMENT_STATUS;
    }

    pickDateByStatus(status: string, entity: OrderModel) {
        const selectorsMap = {
            [CANCELLED_STATUS]: () => entity.cancelled,
            [REFUNDED_STATUS]: () => entity.refunded,
            [SHIPPED_STATUS]: () => entity.shipped,
            [APPOINTMENT_STATUS]: () => entity.appointments[0].start
        };

        const selector = selectorsMap[status];

        return selector ? selector() : '';
    }

    getStatus<TItem extends { shipped?: any, cancelled?: any, refunded: any }>(item: TItem) {
        if (item.shipped) return SHIPPED_STATUS;
        if (item.refunded) return REFUNDED_STATUS;
        if (item.cancelled) return CANCELLED_STATUS;

        return APPOINTMENT_STATUS;
    }

    getDateFormat() {
        return "dd MMM yyyy, hh:mm ";
    }

    getProductPrice() {
        return this.order.service.price;
    } 

    buildUrl() {
        return this.type === 'purchase' ? `/purchases/service/${this.order.id}` : `/sales/service/${this.order.id}`;
    }
}