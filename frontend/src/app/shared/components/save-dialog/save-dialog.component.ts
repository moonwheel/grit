import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

export interface SaveDialogComponentData {
  text: string;

  leftButtonText?: string;
  leftButtonColor?: string;

  rightButtonText?: string;
  rightButtonColor?: string;
}


@Component({
  selector: 'app-save-dialog',
  templateUrl: './save-dialog.component.html'
})

export class SaveDialogComponent implements OnInit {

  text: string;
  leftButtonText = 'No';
  leftButtonColor = 'grey';
  rightButtonText = 'Yes';
  rightButtonColor = 'blue';

  constructor(private dialogRef: MatDialogRef<SaveDialogComponent>,
              @Inject(MAT_DIALOG_DATA)
              private config: SaveDialogComponentData,
              private translateService: TranslateService,
  ) { }

  ngOnInit() {
    if (this.config) {
      if (this.config.text) {
        this.text = this.config.text;
      } else {
        this.translateService.get('DIALOGS.SAVE.TITLE_CHANGES').subscribe(text => {
          this.text = text;
        });
      }

      if (this.config.leftButtonText) {
        this.leftButtonText = this.config.leftButtonText;
      } else {
        this.translateService.get('GENERAL.ACTIONS.NO').subscribe(text => {
          this.leftButtonText = text;
        });
      }

      if (this.config.leftButtonColor) {
        this.leftButtonColor = this.config.leftButtonColor;
      }

      if (this.config.rightButtonText) {
        this.rightButtonText = this.config.rightButtonText;
      } else {
        this.translateService.get('GENERAL.ACTIONS.YES').subscribe(text => {
          this.rightButtonText = text;
        });
      }

      if (this.config.rightButtonColor) {
        this.rightButtonColor = this.config.rightButtonColor;
      }
    }
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }

}
