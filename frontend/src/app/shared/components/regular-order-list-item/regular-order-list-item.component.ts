import { Component, Input, OnInit } from "@angular/core";
import { OrderModel, OrderItemModel } from "../../models";
import { CANCELLED_STATUS, SHIPPED_STATUS, REFUNDED_STATUS, NOT_SHIPPED_STATUS } from "../order-list-item/order-list-item.component";

@Component({
    selector: 'app-regular-order-list-item',
    templateUrl: './regular-order-list-item.component.html'
})
export class RegularOrderListItem implements OnInit {
    @Input() order: OrderModel | null = null;
    @Input() filter: string = 'all';
    @Input() type: 'purchase' | 'sale';

    ngOnInit() {
        if (!this.order) throw new Error('Order is required.');
    }

    getOrderStatus = (item: OrderItemModel) => {
        let status = NOT_SHIPPED_STATUS;

        const itemStatus = this.getStatus(item);

        if(itemStatus) {
            status = itemStatus;
        }

        if(!itemStatus) {
            const orderStatus = this.getStatus(this.order);
            if(orderStatus) {
                status = orderStatus;
            }
        }

        return status;
    }

    getStatus<TItem extends { shipped?: any, cancelled?: any, refunded: any }>(item: TItem) {
        if(this.filter === 'all') {
            if (item.shipped) return SHIPPED_STATUS;
            if (item.refunded) return REFUNDED_STATUS;
            if (item.cancelled) return CANCELLED_STATUS;
        } else {
            if(this.filter === 'open') {
                return NOT_SHIPPED_STATUS;
            }
            if(this.filter === 'cancelled') {
                return CANCELLED_STATUS;
            }
            if(this.filter === 'refunded') {
                return REFUNDED_STATUS;
            }
        }

    }

    pickDateByStatus(status: string, entity: OrderItemModel | OrderModel) {
        const selectorsMap = {
            [CANCELLED_STATUS]: () => entity.cancelled,
            [REFUNDED_STATUS]: () => entity.refunded,
            [SHIPPED_STATUS]: () => entity.shipped,
        };

        const selector = selectorsMap[status];

        return selector ? selector() : '';
    }

    pickTitle(item: OrderItemModel): string {
        return item.product.title;
    }

    pickImageUrl(item: OrderItemModel): string {
        // return item.product.photos[0].thumbUrl;
        return item.product.cover?.thumbUrl;
    }

    getDateFormat() {
        return "dd MMM yyyy";
    }

    buildUrl = (): string => {
        return this.type == 'purchase' ? `/purchases/product/${this.order.id}` : `/sales/product/${this.order.id}`;
    }
}