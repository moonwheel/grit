import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { polyfill, Player, ui } from 'shaka-player/dist/shaka-player.ui';
import { fromEvent, Subject, BehaviorSubject, Observable } from 'rxjs';
import { takeUntil, distinctUntilChanged } from 'rxjs/operators';

// current version of typescript doesn't contain type ResizeObserver
declare class ResizeObserver {
  constructor(arg: any)
  observe(arg?: any): any;
  disconnect(arg?: any): void;
}

@Component({
  selector: 'app-shaka-player',
  templateUrl: './shaka-player.component.html',
  styleUrls: [ './shaka-player.component.scss' ]
})
export class ShakaPlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('videoPlayer', { static: true }) videoElementRef: ElementRef<HTMLVideoElement>;
  @ViewChild('videoPlayerContainer', { static: true }) videoPlayerContainerRef: ElementRef<HTMLDivElement>;

  @Input() autoplay = false;
  @Input() muted = true;
  @Input() playOnHover = true;

  @Input() set src(value: string) {
    if (value) {
      this.srcM3u8 = `${value}.m3u8`;
      this.srcMpd = `${value}.mpd`;
      this.manifestUri = this.srcM3u8;

      // Check to see if the browser supports the basic APIs Shaka needs.
      if (Player.isBrowserSupported()) {
        // Everything looks good!
        this.initPlayer(this.videoElementRef.nativeElement);
      } else {
        // This browser does not have the minimum set of APIs we need.
        console.error('Browser not supported!');
      }
    }
  }

  @Input() observeHeightChange = false;
  @Output() heightChange: EventEmitter<void> = new EventEmitter();

  manifestUri: string;

  srcM3u8: string;
  srcMpd: string;

  private player: Player;
  private intersectionObserver: IntersectionObserver;
  private intersectionObservable$: Observable<boolean>;
  private resizeObserver: ResizeObserver;
  private destroy$ = new Subject();
  private wasAutomaticallyPlayed = false;

  private get videoElem() {
    return this.videoElementRef && this.videoElementRef.nativeElement as HTMLVideoElement;
  }

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // Install built-in polyfills to patch browser incompatibilities.
    polyfill.installAll();

    let playTimeout: NodeJS.Timer;

    if (this.playOnHover) {
      this.intersected().pipe(
        takeUntil(this.destroy$),
      ).subscribe((isVisible) => {
        const isPlaying = this.videoElem && this.videoElem.played;

        if (!isVisible && isPlaying) {
          this.videoElem.pause();
        }

        if (isVisible && this.player && !this.player.getManifest()) {
          this.startLoading();
        }

        if (isVisible) {
          playTimeout = setTimeout(() => {
            if (this.videoElem && !this.wasAutomaticallyPlayed) {
              this.wasAutomaticallyPlayed = true;
              this.videoElem.play();
            }
          }, 1000);
        } else {
          clearTimeout(playTimeout);
        }
      });
    }

    if (this.observeHeightChange) {
      this.resizeObserver = new ResizeObserver(() => {
        this.heightChange.emit();
      });
      this.resizeObserver.observe(this.videoPlayerContainerRef.nativeElement);
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.intersectionObserver) {
      this.intersectionObserver.disconnect();
    }
    if (this.resizeObserver) {
      this.resizeObserver.disconnect();
    }
  }

  private async initPlayer(videoElement: HTMLVideoElement) {
    this.player = new Player(videoElement);

    const uiContainer = this.videoPlayerContainerRef.nativeElement;
    const uiInstance = new ui.Overlay(this.player, uiContainer, videoElement);

    uiInstance.configure({
      overflowMenuButtons : ['quality', 'picture_in_picture'],
    });

    this.player.addEventListener('error', this.onErrorEvent);

    this.startLoading();
  }

  private async startLoading() {
    try {
      await this.player.load(this.manifestUri);

      const uiContainer = this.videoPlayerContainerRef.nativeElement;

      const shakaControlsContainer = uiContainer.querySelector('.shaka-controls-container') as HTMLDivElement;
      const shakaSkimContainer = uiContainer.querySelector('.shaka-skim-container') as HTMLDivElement;
      const shakaPlayButtonContainer = uiContainer.querySelector('.shaka-play-button-container') as HTMLDivElement;
      const shakaPlayButton = uiContainer.querySelector('.shaka-play-button') as HTMLDivElement;
      const shakaMuteButton = uiContainer.querySelector('.shaka-mute-button') as HTMLDivElement;

      if (this.muted) {
        shakaMuteButton.click();
      }

      let hidden = true;

      shakaControlsContainer.ontransitionend = () => {
        const style = getComputedStyle(shakaControlsContainer);
        hidden = style.opacity === '0';

        if (hidden) {
          // this is used because preventing of click propagation on shakaPlayButton doesnt work
          shakaPlayButton.style.pointerEvents = 'none';
        } else {
          shakaPlayButton.style.pointerEvents = 'all';
        }
      };

      const preventer = (event: Event) => {
        if (hidden) {
          event.stopPropagation();
          event.preventDefault();
        }
      };

      shakaSkimContainer.onclick = preventer;
      shakaPlayButtonContainer.onclick = preventer;
    } catch (error) {
      this.onError(error);
    }
  }

  private onErrorEvent(event) {
    // Extract the util.Error object from the event.
    this.onError(event.detail);
  }

  private onError(error) {
    // Log the error
    console.error('Error code', error.code, 'object', error);
  }

  private intersected(): Observable<boolean> {
    if (!this.intersectionObservable$) {
      this.intersectionObservable$ = new Observable<boolean>(observer => {
        this.intersectionObserver = new IntersectionObserver((entries) => {
          const isIntersecting = entries[0] && entries[0].isIntersecting;
          observer.next(isIntersecting);
        });

        this.intersectionObserver.observe(this.elementRef.nativeElement);

        return () => this.intersectionObserver.disconnect();
      }).pipe(
        distinctUntilChanged()
      );
    }

    return this.intersectionObservable$;
  }

}
