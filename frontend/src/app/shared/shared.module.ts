import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRouting } from './shared.routing';
import { environment } from 'src/environments/environment';

// Modules
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from '@danielmoncada/angular-datetime-picker';
/**
 * This module is disabled because it takes to much space in bundle,
 * but we do not use it anywhere in app. If you want to enable this module
 * in future - you need to extend current 5.5mb budget size
 */
// import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxMaskModule } from 'ngx-mask';
import { LuxonModule } from 'luxon-angular';
import { EllipsisModule } from 'ngx-ellipsis';
import { ClipboardModule } from 'ngx-clipboard';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MentionModule } from 'angular-mentions';
import { TranslateModule } from '@ngx-translate/core';

// Components
import { MainComponent } from '../modules/main/main.component';
import { CommentListComponent } from './components/actions/comments/comment-list/comment-list.component';
import { LikeListComponent } from './components/actions/likes/like-list/like-list.component';

import { ReportAddComponent } from './components/actions/reports/report-add/report-add.component';
import { ReportListComponent } from './components/actions/reports/report-list/report-list.component';

import { ReviewAddComponent } from './components/actions/reviews/review-add/review-add.component';
import { ReviewReplyComponent } from './components/actions/reviews/review-reply/review-reply.component';
import { ReviewListComponent } from './components/actions/reviews/review-list/review-list.component';

import { ReplyListComponent } from './components/actions/comments/reply/reply-list/reply-list.component';
import { FollowersComponent } from '../modules/user/components/followers/followers.component';
import { AddressDialogComponent } from './components/address-dialog/address-dialog.component';
import { AddressInputComponent } from './components/address-input/address-input.component';
import { ShareListComponent } from './components/actions/shares/share-list/share-list.component';
import { UnfollowDialogComponent } from './components/actions/follow/unfollow-dialog/unfollow-dialog.component';
import { SaveDialogComponent } from './components/save-dialog/save-dialog.component';
import { CommentEditComponent } from './components/actions/comments/comment-edit/comment-edit.component';
import { BlockedDialogComponent } from './components/actions/blocked/blocked-dialog/blocked-dialog.component';
import { CopyLinkItemComponent } from './components/copy-link-item/copy-link-item.component';
import { ShareButtonComponent } from './components/share-button/share-button.component';
import { ShareLinkItemComponent } from './components/share-link-item/share-link-item.component';
import { BookmarkButtonComponent } from './components/bookmark-button/bookmark-button.component';
import { BookmarkLinkItemComponent } from './components/bookmark-link-item/bookmark-link-item.component';
import { NotLoggedDialogComponent } from './components/actions/unauthorized/not-logged-dialog/not-logged-dialog.component';
import { BankDialogComponent } from './components/bank-dialog/bank-dialog.component';
import { BankInputComponent } from './components/bank-input/bank-input.component';
import { ResultDialogComponent } from './components/result-dialog/result-dialog.component';
import { PayoutDialogComponent } from './components/payout-dialog/payout-dialog.component';
import { RatingStarsComponent } from './components/actions/reviews/rating-stars/rating-stars.component';
import { ReviewSliderComponent } from './components/actions/reviews/review-slider/review-slider.component';
import { ReviewLikesComponent } from './components/actions/reviews/review-likes/review-likes.component';
import { ReviewReplyEditComponent } from './components/actions/reviews/review-reply-edit/review-reply-edit.component';
import { KycValidationDialogComponent } from './components/kyc-validation-dialog/kyc-validation-dialog.component';
import {
  ShakaPlayerComponent,
  FeedItemArticleComponent,
  FeedItemPhotoComponent,
  FeedItemVideoComponent,
  ThumbnailComponent,
  LoadingComponent,
  FeedItemTextComponent,
  FeedItemServiceComponent,
  TruncatableTextComponent,
  InputWithMentionsComponent,
  SalesProductComponent,
  SalesServiceComponent,
  PurchasesProductComponent,
  PurchasesServiceComponent,
  ShareInMessageDialogComponent,
  LocationInputComponent,
  LocationButtonComponent,
  BarRatingComponent,
  ImageContainerComponent,
} from './components';
import { FilePlaceholderComponent } from './components/file-placeholder/file-placeholder.component';
import { NoItemsPlaceholderComponent } from './components/no-items-placeholder/no-items-placeholder.component';
import { MainLoader } from './components/main-loader/main-loader.component';
import { AppContentLayoutComponent } from './components/app-content-layout/app-content-layout.component';
import { SubHeaderLayoutComponent } from '../modules/main/components/layouts/sub-header-layout/sub-header-layout.component';
import { EditorOutputView } from './components/editor-view-output/editor-output-view.component';
import { AccordionComponent } from './components/accordion-panel/accordion-panel.component';
import { OrderListItemComponent } from './components/order-list-item/order-list-item.component';
import { BookingListItem } from './components/booking-list-item/booking-list-item.component';
import { RegularOrderListItem } from './components/regular-order-list-item/regular-order-list-item.component';

// Directives
import { AutosizeDirective } from './directives/autosize.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { ConnectFormDirective } from './directives/connect-form.directive';
import { TimeInputDirective } from './directives/time-input.directive';
import { UntabableDirective } from './directives/untabable.directive';
import { PriceInputDirective } from './directives/price-input.directive';
import { TabsVisibilityTriggerDirective } from './directives/tabs-visibility-trigger.directive';
import { MessageToDirective } from './directives/message-to.directive';
import { ReplaceLinksDirective } from './directives/replace-links.directive';
import { CopyAbleTextDirective } from './directives/copyable-text.directive';

// Pipes
import { BarRatingPipe } from './pipes/bar-rating.pipe';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { ToNumberPipe } from './pipes/to-number';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { CustomCurrencyPipe } from './pipes/custom-currency.pipe';
import { TrimPipe } from './pipes/trim.pipe';
import { HashtagsPipe } from './pipes/hashtags.pipe';
import { MentionsPipe } from './pipes/mentions.pipe';
import { LinksToHtmlPipe } from './pipes/links-to-html.pipe';

import { MatQuillModule } from './components/mat-quill/mat-quill-module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { InfiniteScrollWithLoader } from './components/infinite-scroll-with-loader/infinite-scroll-with-loader.component';
import { AppDatePipe } from './pipes/app-date-pipe';
import { CustomBackButtonPath } from './directives/custom-back-btn-path';



const entryComponentsToExport = [
  CommentListComponent,
  LikeListComponent,
  ReportAddComponent,
  ReportListComponent,
  ReviewAddComponent,
  ReviewReplyComponent,
  ReviewListComponent,
  AddressDialogComponent,
  ReplyListComponent,
  FollowersComponent,
  SaveDialogComponent,
  CommentEditComponent,
  ShareListComponent,
  UnfollowDialogComponent,
  ThumbnailComponent,
  BlockedDialogComponent,
  NotLoggedDialogComponent,
  BankDialogComponent,
  ResultDialogComponent,
  PayoutDialogComponent,
  ReviewSliderComponent,
  ReviewLikesComponent,
  ReviewReplyEditComponent,
  KycValidationDialogComponent,
  ShareInMessageDialogComponent,
];

const componentsToExport = [
  MainComponent,
  AddressInputComponent,
  BarRatingComponent,
  LoadingComponent,
  ShakaPlayerComponent,
  FeedItemArticleComponent,
  FeedItemPhotoComponent,
  FeedItemVideoComponent,
  FeedItemTextComponent,
  FeedItemServiceComponent,
  TruncatableTextComponent,
  CopyLinkItemComponent,
  ShareButtonComponent,
  ShareLinkItemComponent,
  BookmarkButtonComponent,
  BookmarkLinkItemComponent,
  InputWithMentionsComponent,
  BankInputComponent,
  SalesProductComponent,
  SalesServiceComponent,
  PurchasesProductComponent,
  PurchasesServiceComponent,
  RatingStarsComponent,
  LocationInputComponent,
  LocationButtonComponent,
  AppContentLayoutComponent,
  SubHeaderLayoutComponent,
  FilePlaceholderComponent,
  ImageContainerComponent,
  NoItemsPlaceholderComponent,
  MainLoader,
  EditorOutputView,
  AccordionComponent,
  InfiniteScrollWithLoader,
  OrderListItemComponent,
  RegularOrderListItem,
  BookingListItem,
  ...entryComponentsToExport,
];

const directivesToExport = [
  AutosizeDirective,
  AutofocusDirective,
  ConnectFormDirective,
  TimeInputDirective,
  PriceInputDirective,
  UntabableDirective,
  TabsVisibilityTriggerDirective,
  MessageToDirective,
  ReplaceLinksDirective,
  CustomBackButtonPath,
  CopyAbleTextDirective,
];

const pipesToExport = [
  BarRatingPipe,
  TimeAgoPipe,
  TruncatePipe,
  ToNumberPipe,
  SanitizeHtmlPipe,
  CustomCurrencyPipe,
  TrimPipe,
  HashtagsPipe,
  MentionsPipe,
  LinksToHtmlPipe,
  AppDatePipe,
];

const modulesToExport = [
  SharedRouting,
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  MaterialModule,
  DragDropModule,
  PickerModule,
  SwiperModule,
  SatPopoverModule,
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
  LuxonModule,
  ScrollingModule,
  ClipboardModule,
  MentionModule,
  TranslateModule,
  InfiniteScrollModule,
];

export const MY_NATIVE_FORMATS = {
  fullPickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false },
  datePickerInput: { day: 'numeric', month: 'numeric', year: 'numeric' },
  timePickerInput: { hour: 'numeric', minute: 'numeric' },
  monthYearLabel: { year: 'numeric', month: 'numeric' },
  dateA11yLabel: { year: 'numeric', month: 'numeric', day: 'numeric' },
  monthYearA11yLabel: { year: 'numeric', month: 'numeric' },
};

@NgModule({
  declarations: [
    // Components
    ...componentsToExport,
    // Directive
    ...directivesToExport,
    // Pipes
    ...pipesToExport,
    NotLoggedDialogComponent,
  ],
  imports: [
    ...modulesToExport,
    // NgxMapboxGLModule.withConfig(
    //   { accessToken: 'pk.eyJ1IjoidGhvbWFzbG9obWFubiIsImEiOiJjanBqdjY2ZHIwMGJ2M3F0NmNqaDloaXJhIn0.iarJxHhGR38eFMZRtCcnbw' }
    // ),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.production
    }),
    NgxMaskModule.forRoot(),
    MatQuillModule,
    EllipsisModule,
  ],
  exports: [
    // Modules
    ...modulesToExport,
    // NgxMapboxGLModule,
    NgxsReduxDevtoolsPluginModule,
    NgxMaskModule,
    MatQuillModule,
    EllipsisModule,
    // Components
    ...componentsToExport,
    // Directive
    ...directivesToExport,
    // Pipes
    ...pipesToExport,
  ],
  entryComponents: [
    ...entryComponentsToExport
  ],
  providers: [
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS },
  ]
})

export class SharedModule { }
