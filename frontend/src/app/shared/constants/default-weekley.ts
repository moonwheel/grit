export const getDefaultWeekley = () => {
  return [
    {
      id: 1,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.MONDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: true,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 2,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.TUESDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: true,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 3,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.WEDNESDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: true,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 4,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.THURSDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: true,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 5,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.FRIDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: true,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 6,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.SATURDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: false,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    },
    {
      id: 7,
      day: 'GENERAL.DATE_TIME.WEEK_DAYS_SHORT.SUNDAY',
      firstPartId: null,
      secondPartId: null,
      enabled: false,
      from_first: null,
      to_first: null,
      addition: false,
      from_second: null,
      to_second: null
    }
  ];
};

