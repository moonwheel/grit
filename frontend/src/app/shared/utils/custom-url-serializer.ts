import { DefaultUrlSerializer, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomUrlSerializer extends DefaultUrlSerializer {
  serialize(tree: UrlTree): string {
    const serialized = super.serialize(tree);

    // use our logic only for search results
    if (serialized.includes('search')) {
      return serialized.replace(/(%20|%2B)+/g, '+');
    } else {
      return serialized;
    }
  }
}
