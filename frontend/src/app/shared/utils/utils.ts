import { FormGroup } from '@angular/forms';
import { AccountModel } from '../models/user/account.model';
import { ActivatedRouteSnapshot } from '@angular/router';
import { constants } from 'src/app/core/constants/constants';
import * as loadImage from 'blueimp-load-image';
import { ProductModel, ProductVariantModel, AbstractEntityModel, AddressModel } from '../models';
import _set from 'lodash/set';

export class Utils {
    public static touchForm(form: FormGroup) {
        Object.keys(form.controls).forEach(field => {
            const control = form.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }

    public static getAccountPageName(account: AccountModel): string {
        if (account && account.business) {
            return account.business.pageName.toLowerCase();
        } else
        if (account && account.person) {
            return account.person.pageName.toLowerCase();
        } else {
            return '';
        }
    }

    public static getAccountPageUrl(account: AccountModel): string {
        return `/${encodeURI(account.pageName)}/home`;
    }

    public static getAccountFullName(account: AccountModel): string {
      if (account && account.business) {
          return account.business.name;
      } else
      if (account && account.person) {
          return account.person.fullName;
      } else {
          return '';
      }
  }

  public static getParentRouteParam(route: ActivatedRouteSnapshot, paramName: string): any {
      if (paramName in route.params) {
          return route.params[paramName];
      } else
      if (route.parent) {
          return Utils.getParentRouteParam(route.parent, paramName);
      } else {
          return null;
      }
  }

  public static imageUrl(photoUrl: string, placeholder = constants.PLACEHOLDER_AVATAR_PATH): string {
      return photoUrl || placeholder;
  }

  public static async getImageData(srcFile: File): Promise<{ url: string, file: File }> {
      const img: HTMLCanvasElement = await new Promise((resolve, reject) => {
          loadImage(srcFile, (data) => {
              if (data.error) {
                  return reject(data.error);
              } else {
                  return resolve(data);
              }
          }, {
              // i do not understand why, but 'orientation: true' stopped working.
              // But 'canvas: true is enough to maintain correct orientation
              canvas: true,
              maxWidth: 750
          });
      });

      const url = img.toDataURL();
      const res = await fetch(url);
      const blob = await res.blob();
      const typeParsed = url.match(/\:\S+\;/)[0]; // this regexp takes all between ":" and ";" characters
      const type = typeParsed && typeParsed.slice(1, typeParsed.length - 1);
      const file = new File([blob], 'some-file.jpg', { type });

      return { url, file };
  }

  public static getCheepestProductVariant(product: ProductModel): ProductVariantModel {
      const enabled = product.variants.find(item => item.enable);
      return product.variants.reduce((cheepest, current) => {
          if (!current.enable) {
              return cheepest;
          }
          return +cheepest.price > +current.price ? current : cheepest;
      }, enabled);
  }

  public static extendAccountWithBaseInfo(account: AccountModel): AccountModel {
      const business = account.business;
      const person = account.person;

      let additionalInfo: Partial<AccountModel> = {};

      if (account.business || account.person) {
          additionalInfo = {
              name: business ? business.name : person.fullName,
              pageName: account.business ? account.business.pageName : account.person.pageName,
              photo: Utils.imageUrl(account.business ? account.business.photo : account.person.photo),
              thumb: Utils.imageUrl(
                  account.business ?
                  (account.business.thumb || account.business.photo) :
                  (account.person.thumb || account.person.photo)
              ),
          };
      }

      return {
          ...account,
          ...additionalInfo,
          id: account.id,
      };
  }

  public static trackById(index: number, item: { id: number }) {
      return item.id;
  }

  public static replaceSpacesWithPluses(text: string) {
      if (!text) {
          return '';
      }

      return text.trim().replace(/\s+/g, '+');
  }

  public static replacePlusesWithSpaces(text: string) {
      if (!text) {
          return '';
      }

      return text.trim().replace(/\++/g, ' ');
  }

  /**
   * Retrieve the value at a given path
   * path({ a: { b: { c: 1 } } }, ['a', 'b']) -> 1
   * path({ c: { b: 1 } }, ['a', 'b']) -> undefined
   */
  public static path(obj: any, fieldPath: string[]) {
    return fieldPath.reduce((res: any, key: string) => res && res[key], obj);
  }

  /**
   * Retrieve the value at a given path or return provided default value
   * pathOr({ a: { b: 1 } }, ['a', 'b'], 15) -> 1
   * pathOr({ c: { b: 1 } }, ['a', 'b'], 15) -> 15
   */
  public static pathOr(obj: any, fieldPath: string[], defaultValue: any) {
    return Utils.path(obj, fieldPath) || defaultValue;
  }

  public static replaceInputSelectionText(elem: HTMLTextAreaElement | HTMLInputElement, text: string): string {
    const value = elem.value;
    const before = value.substring(0, elem.selectionStart);
    const after = value.substring(elem.selectionEnd, value.length);
    const newSelectionStart = elem.selectionStart + text.length;
    const newValue = `${before}${text}${after}`;

    elem.value = newValue;
    elem.setSelectionRange(newSelectionStart, newSelectionStart);

    return newValue;
  }

  public static getEntityDirectLink(elem: AbstractEntityModel, middlePath: string): string {
    const pageName = Utils.getAccountPageName(elem.user);

    if (pageName) {
        return `/${pageName}/${middlePath}/${elem.id}`;
    } else {
        return '';
    }
  }

  // This is added because current version of Typescript (3.5.3) does not contain
  // String.prototype.matchAll() method
  public static matchAll(str: string, regexp: RegExp): RegExpExecArray[] {
    let match = regexp.exec(str);
    const result = [];

    while (match !== null) {
        result.push(match);
        match = regexp.exec(str);
    }

    return result;
  }

  public static getAddressString(address: AddressModel): string {
    if (address) {
        const result = [];
        if (address.fullName) {
            result.push(`${address.fullName},`);
        }

        result.push(`${address.street},`);

        if (address.appendix) {
            result.push(`${address.appendix},`);
        }

        result.push(`${address.zip}`);
        result.push(`${address.city}`);

        return result.join(' ');
    } else {
        return '';
    }
  }

  public static convertRawQueryTree(tree: Record<string, any>) {
    const obj: Record<string, any> = {};

    for(let key in tree) {

        let path = key.replace(/_/g, '.');

        path = path.replace('account', 'user');

        _set(obj, path, tree[key]);
    }


    return obj
  }
}
