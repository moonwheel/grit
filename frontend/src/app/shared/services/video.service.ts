import { EventEmitter, Injectable, NgZone} from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { VideoModel } from '../models/video.model';
import { AbstractService } from './abstract.service';
import { HttpHeaders } from '@angular/common/http';
import { Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})
export class VideoService extends AbstractService {

  prefix = '/video';

  constructor(protected apiService: ApiService,
              protected store: Store,
  ) {
    super(apiService, store);
  }
}
