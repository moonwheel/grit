import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { Store } from '@ngxs/store';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { environment } from 'src/environments/environment';
import { AbstractEntityType, NotificationModel } from '../models';
import { AuthState } from 'src/app/store/state/auth.state';

interface ServerEvent {
  type: string;
  entityType: AbstractEntityType;
  entity: any;
  notification?: NotificationModel;
  temporaryId?: string;
}

@Injectable({
  providedIn: 'root'
})
export class SseService {

  eventSource: EventSource;

  message$ = new Subject();
  processingFile$ = new Subject();
  fileProcessingComplete$ = new Subject<ServerEvent>();
  attachmentProcessingComplete$ = new Subject<ServerEvent>();
  notifications$ = new Subject<ServerEvent>();

  constructor(
    private store: Store,
    private zone: NgZone,
  ) { }

  init() {
    if (this.eventSource) {
      this.eventSource.close();
    }

    const token = this.store.selectSnapshot(AuthState.token);

    this.eventSource = new EventSourcePolyfill(`${environment.baseUrl}/events`, {
      headers: {
        authorization: `Bearer ${token}`
      }
    });

    this.eventSource.addEventListener('message', (event) => {
      // console.log('sse message event', event)
      const data: ServerEvent = JSON.parse(event.data);

      if (data.type === 'file_processing_complete') {
        this.zone.run(() => {
          this.fileProcessingComplete$.next(data);
        });
      } else if (data.type === 'new_notification') {
        this.zone.run(() => {
          this.notifications$.next(data);
        });
      } else if (data.type === 'following_request') {
        this.zone.run(() => {
          this.message$.next(data);
        });
      } else if (data.type === 'attachment_file_processing_complete') {
        this.zone.run(() => {
          this.attachmentProcessingComplete$.next(data);
        });
      }
    });

    this.eventSource.onerror = error => {
      console.log('error in event source', error);
    };
  }
}
