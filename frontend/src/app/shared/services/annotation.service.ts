import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AnnotationService {
  constructor(
    private apiService: ApiService
  ) { }

  getAllForPhoto(photoId: number): Observable<any> {
    return this.apiService.get(`/photo/${photoId}/annotation`);
  }
}

