import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(
    private apiService: ApiService
  ) { }


  search(text: string, type: string, page = 1, limit = 15): Observable<any> {
    const params: any = { text, page, limit };
    if (type) {
      params.type = type;
    }
    return this.apiService.get(`/search`, null, null, params);
  }

  searchByLocation(text: string, type: string, page = 1, limit = 15): Observable<any> {
    const params: any = { text, page, limit };
    if (type) {
      params.type = type;
    }
    return this.apiService.get(`/search/location`, null, null, params);
  }

  autosuggest(text: string): Observable<any> {
    const params: any = { text };
    return this.apiService.get(`/search/autosuggest`, null, null, params);
  }

  getPlacesAutocomplete(text: string): Observable<any> {
    const params: any = { text };
    return this.apiService.get(`/search/location/autosuggest`, null, null, params);
  }

}
