import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import { takeUntil, switchMap, tap, filter, startWith, map } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { PhotoState } from '../../store/state/photo.state';
import { TextState } from '../../store/state/text.state';
import { VideoState } from '../../store/state/video.state';
import { ArticleState } from '../../store/state/article.state';
import { ServiceState } from '../../store/state/service.state';
import { TimelineState } from 'src/app/store/state/timeline.state';
import { AbstractEntityModel, FeedItemModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class TimelineService implements OnDestroy {

  timeline$: Observable<(FeedItemModel | AbstractEntityModel)[]>;

  private destroy$: Subject<void> = new Subject();

  constructor(private store: Store) {
    this.timeline$ = this.store.select(TimelineState.items).pipe(
      // switchMap(items => {
      //   const sourses = items.map(itemInTimeline => {
      //     const itemInStore = this.getItemInStore(itemInTimeline);

      //     if (itemInStore) {
      //       return this.store.select(this.getSelector(itemInTimeline)).pipe(
      //         filter(item => Boolean(item)),
      //         map(item => ({
      //           ...item,
      //           tableName: itemInTimeline.tableName
      //         })),
      //         startWith(itemInStore),
      //       );
      //     } else {
      //       return of(itemInTimeline);
      //     }
      //   });

      //   return combineLatest(sourses);
      // }),
      takeUntil(this.destroy$),
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.destroy$.unsubscribe();
  }

  private getItemInStore(item: FeedItemModel): FeedItemModel | AbstractEntityModel {
    const itemInStore: AbstractEntityModel = {
      ...this.store.selectSnapshot(this.getSelector(item))
    };

    if (itemInStore) {
      itemInStore.tableName = item.tableName;
    }

    return itemInStore || item;
  }

  private getSelector(item: FeedItemModel): (param: any) => AbstractEntityModel {
    switch (item.tableName) {
      case 'article':
        return ArticleState.article(item.id);
      case 'photo':
        return PhotoState.photo(item.id);
      case 'video':
        return VideoState.video(item.id);
      case 'service':
        return ServiceState.service(item.id);
      case 'text':
        return TextState.text(item.id);
    }
  }

}
