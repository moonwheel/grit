import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { mapRawbookmarkModelToAppModel } from '../models/bookmark.model';
import { map } from 'rxjs/operators';
import { Utils } from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {
  constructor(
    private apiService: ApiService
  ) { }


  getAll(page = 1, limit = 15, search = ''): Observable<any> {
    const params: any = { page, limit };

    if (search) {
        params.search = search;
    }


    return this.apiService.get(`/bookmark`, null, null, params).pipe(map(data => {
      if(data.raw) {

        return data.userBookmarks.map((item) => {
          const data = Utils.convertRawQueryTree(item);
          
          return data;
        });
      }

      return data;
    }));
  }

  addBookmark(data): Observable<any> {
    return this.apiService.post('/bookmark', data);
  }

  deleteBookmark(data): Observable<any> {
    return this.apiService.delete(`/bookmark/${data.type}/${data.id}`);
  }
}
