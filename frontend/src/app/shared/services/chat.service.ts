import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { ApiService } from './api.service';
import { AbstractService } from './abstract.service';
import { ChatroomUserModel, MessageModel } from '../models/chat';
import { constants } from 'src/app/core/constants/constants';
import { map } from 'rxjs/operators';
import { mapRawMessageModelToMessageModel } from '../models/chat/raw-message.model';

@Injectable({
  providedIn: 'root',
})
export class ChatService extends AbstractService {
  prefix = `/messaging/chatrooms`;

  constructor(
    protected apiService: ApiService,
    protected store: Store,
  ) {
    super(apiService, store);
  }

  getAll(page = 1, limit = 15): Observable<{ total: number, items: any[] }> {
    const params: any = {
      page,
      limit,
    };

    return this.apiService.get(`${this.prefix}`, null, false, params);
  }

  getOne(chatroomId: number): Observable<ChatroomUserModel> {
    return this.apiService.get(`${this.prefix}/${chatroomId}`);
  }

  findByAccountIds(users: number[]): Observable<{ chatroom_id: number }> {
    return this.apiService.post(`${this.prefix}/find`, { users });
  }

  getMessages(chatroomId: number, page = 1, limit = constants.CHAT_MESSAGES_PAGE_SIZE): Observable<MessageModel[]> {
    const params = { page, limit };
    return this.apiService.get(`${this.prefix}/${chatroomId}/messages`, null, null, params).pipe(map(({messages, raw}) => {
      if(raw) {
        return messages.map(mapRawMessageModelToMessageModel);
      }

      return messages;
    }));
  }

  startNewChat(users: number[], message: string): Observable<ChatroomUserModel> {
    return this.apiService.post(`${this.prefix}`, { users, message });
  }

  sendMessage(chatroomId: number, text: string, temporaryId: string): Observable<any> {
    return this.apiService.post(`${this.prefix}/${chatroomId}/messages`, { message: { text, temporaryId } });
  }

  getUnreadCount(): Observable<{ count: number }> {
    return this.apiService.get(`${this.prefix}/unread_amount`);
  }

  readMessage(chatroomId: number, messageId: number): Observable<{ count: number }> {
    const data = {
      last_read_message_id: messageId
    };
    return this.apiService.put(`${this.prefix}/${chatroomId}/read`, data);
  }

  setMutedStatus(chatroomId: number, muted: boolean): Observable<any> {
    const data = {
      is_muted: muted
    };
    return this.apiService.put(`${this.prefix}/${chatroomId}/mute`, data);
  }

  deleteChatroom(chatroomId: number): Observable<any> {
    return this.apiService.delete(`${this.prefix}/${chatroomId}`);
  }

  share(userIds: number[], message: string, entityType: string, entityId: number): Observable<any> {
    const data = {
      userIds,
      message,
      entityType,
      entityId,
    };
    return this.apiService.post(`${this.prefix}/share`, data);
  }

}
