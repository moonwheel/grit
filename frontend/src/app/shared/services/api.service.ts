import { HttpClient, HttpHeaders, HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  bearerToken: string;
  currentUser: any;
  loading = false;
  apiUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  setHeaders(headerType, authenticate): any {
    const headersConf = {};
    if (headerType === 'json') {
      headersConf['Content-Type'] = 'application/json';
    } else if (headerType === 'form') {
      headersConf['Content-Type'] = 'application/x-www-form-urlencoded';
    } else if (headerType === 'multipart') {
      headersConf['Content-Type'] = 'multipart/form-data';
    }

    return new HttpHeaders(headersConf);
  }

  get(path: string, headerType: string = 'json', authenticate: boolean = false, params = {}): Observable<any> {
    const headers = this.setHeaders(headerType, authenticate);

    return this.http.get(this.apiUrl + path, { headers, params });
  }

  patch(path: string, body: any = {}, headerType: string = 'json', authenticate: boolean = false): Observable<any> {
    const headers = this.setHeaders(headerType, authenticate);

    return this.http.patch(this.apiUrl + path, body, { headers });
  }

  post(path: string, body, headerType: string = 'json', authenticate: boolean = false): Observable<any> {
    const headers = this.setHeaders(headerType, authenticate);

    return this.http.post(this.apiUrl + path, body, { headers });
  }

  put(path: string, body, headerType: string = 'json', authenticate: boolean = false): Observable<any> {
    const headers = this.setHeaders(headerType, authenticate);

    return this.http.put(this.apiUrl + path, body, { headers });
  }

  delete(path: string, params: any = {}, headerType: string = 'json', authenticate: boolean = false): Observable<any> {
    const headers = this.setHeaders(headerType, authenticate);

    return this.http.delete(this.apiUrl + path, { headers, params });
  }

  upload(path, formData, method = 'put', headers?: HttpHeaders) {
    if (!headers) {
      headers = new HttpHeaders();
      headers = headers.append('Content-Type', 'false');
    }

    return this.http[method](this.apiUrl + path, formData, {
      reportProgress: true,
      observe: 'events',
      headers
    }).pipe(
      map(event => event),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    return throwError(error.error.error || 'Something bad happened. Please try again later.');
  }

}
