import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(
    private apiService: ApiService
  ) { }

  getReporstList(page = 1, limit = 15, filter = {}, search = ''): Observable<any> {
    
    let params: any = { page, limit };

    if (filter) {
      params = {
        ...params,
        ...filter,
      };
    }

    if (search) {
      params.search = search;
    }

    return this.apiService.get(`/report/all`, null, false, params);
  }
  
  getReports(): Observable<any> {
    return this.apiService.get(`/report/all`, null);
  }

  addReport(id: number, data: any): Observable<any> {
    return this.apiService.post(`/report/${id}`, data);
  }

  editReport(id: number, data: any): Observable<any> {
    return this.apiService.patch(`/report/${id}`, data);
  }

  deleteReport(id: number): Observable<any> {
    return this.apiService.delete(`/report/${id}`);
  }

  getCategories(): Observable<any> {
    return this.apiService.get(`/report/categories`);
  }

  getTypes(): Observable<any> {
    return this.apiService.get(`/report/types`);
  }
}
