import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { BankAccountModel, CreditCardModel, CreditCardRequestModel, WalletModel } from '../models';

@Injectable({
    providedIn: 'root'
})
export class PaymentService {
    constructor(
        private apiService: ApiService
    ) { }

    getAll(): Observable<any> {
        return this.apiService.get('/payments/all', null);
    }

    addBankAccount(data: BankAccountModel): Observable<any> {
        return this.apiService.post('/payments/bank', data);
    }

    addCreditCard(data: CreditCardRequestModel): Observable<any> {
        return this.apiService.post('/payments/cards', data);
    }

    editBankAccount(data: BankAccountModel): Observable<any> {
        return this.apiService.post('/payments/bank', data);
    }

    editCreditCard(data: CreditCardRequestModel): Observable<any> {
        return this.apiService.post('/payments/cards', data);
    }

    deleteBankAccount(id: number): Observable<any> {
        return this.apiService.put(`/payments/bank/${id}`, null);
    }

    deleteCreditCard(id: number): Observable<any> {
        return this.apiService.put(`/payments/cards/${id}`, null);
    }

    getWallet(): Observable<WalletModel> {
        return this.apiService.get('/payments/wallet', null);
    }

    cardDirectPayIn(card: number, amount: number): Observable<any> {
        return this.apiService.post('/payments/payins/card', { card, amount });
    }

    bankwireDirectPayIn(bank: number, amount: number): Observable<any> {
        return this.apiService.post('/payments/payins/bankwire', { bank, amount });
    }

    getTransactions() {
        return this.apiService.get('/payments/transactions', null);
    }
}
