import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { AbstractService } from './abstract.service';
import { Store } from '@ngxs/store';
import { map } from 'rxjs/operators';
import { Utils } from '../utils/utils';
import { InitUser } from '../../store/actions/user.actions';

@Injectable({
    providedIn: 'root'
})
export class ProductService extends AbstractService {
    prefix = '/product';

    constructor(
        protected apiService: ApiService,
        protected store: Store,
    ) {
        super(apiService, store);
    }

    changeState(id): Observable<any> {
        return this.apiService.patch(`${this.prefix}/active/${id}`, {});
    }

    getAll(
        page = 1,
        limit = 15,
        filter = '',
        order = 'desc',
        userId = null,
        draft = false,
        search = '',
        categoryId = null
      ): Observable<{ total: number, items: any[] }> {
        const params: any = {
          filter,
          order,
          draft,
          page,
          limit,
          search,
        };
    
        if (userId) {
          params.userId = userId;
        }
    
        if (categoryId) {
          params.shopCategoryId = categoryId;
        }
    
        return this.apiService.get(`${this.prefix}/all`, null, false, params).pipe(map(data => {
            data.items = data.items.map((item) => Utils.convertRawQueryTree(item).product)

            return data;
        }));
      }

    getDetailedList(
        page = 1,
        limit = 15,
        filter = '',
        order = 'desc',
        userId = null,
        draft = false,
        search = '',
        categoryId = null
    ): Observable<{ total: number, items: any[] }> {
        const params: any = {
            filter,
            order,
            draft,
            page,
            limit,
            search,
        };

        if (userId) {
            params.userId = userId;
        }

        if (categoryId) {
            params.shopCategoryId = categoryId;
        }

        return this.apiService.get(`${this.prefix}/businesslist`, null, false, params);
    }
}
