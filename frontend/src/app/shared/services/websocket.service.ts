import io from 'socket.io-client';

import { EventEmitter, Injectable, NgZone } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Store } from '@ngxs/store';
import { Socket } from 'socket.io';
import { environment } from 'src/environments/environment';
import { AbstractEntityType } from '../models';
import { ChatActions } from 'src/app/store/actions/chat.actions';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  socket: Socket;

  message$ = new Subject();
  processingFile$ = new Subject();
  fileProcessingComplete$ = new Subject<{ entityType: AbstractEntityType, entity: any }>();

  constructor(protected store: Store,
  ) {
  }

  init() {
    const store = this.store.snapshot();
    const token = store && store.auth.token;

    if (this.socket) {
      this.socket.disconnect();
    }

    this.socket = io.connect(environment.baseUrl, {
      query: { token },
      transports:['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('websocket connected');

      this.socket.on('joined', data => {
        // console.log('joined to room event:', data);
      });

      this.socket.on('new_messages', data => {
        console.log('!!!new socket message:', data);

        for (const message of data) {
          this.store.dispatch(new ChatActions.AddMessageToChat(+message.chatroom, message));
        }
      });

      this.socket.on('new_chat', data => {
        console.log('!!!new_chat:', data);
        this.store.dispatch(new ChatActions.AddChat(data));
      });

      this.socket.on('deleted_chatroom', data => {
        console.log('!!!deleted_chatroom:', data);
        this.store.dispatch(new ChatActions.DeleteChat(+data.chatroom));
      });

      this.socket.on('deleted_message', data => {
        console.log('!!!deleted_message:', data);
      });

      this.socket.on('updated_message', data => {
        console.log('!!!updated_message:', data);
      });

      this.socket.on('read_message', data => {
        console.log('!!!read_message:', data);
      });
    });
  }
}
