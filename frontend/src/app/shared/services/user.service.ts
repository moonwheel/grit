import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(
        private apiService: ApiService,
        private http: HttpClient,
    ) { }

    login(data): Observable<any> {
        return this.apiService.post('/user/login', data);
    }

    register(data): Observable<any> {
        return this.apiService.post('/user/register', data);
    }

    logout(): Observable<any> {
        return this.apiService.post('/logout', null);
    }

    getMe(): Observable<any> {
        return this.apiService.get('/user');
    }

    updateUser(data): Observable<any> {
        return this.apiService.put('/user', data);
    }

    changePassword(data): Observable<any> {
        return this.apiService.post('/user/changepass', data);
    }

    changeForgottenPassword(data): Observable<any> {
        return this.apiService.post('/user/changepassnoauth', data);
    }

    verifyPasswordWithAuth(): Observable<any> {
        return this.apiService.get('/user/forgotpass');
    }

    verifyPasswordWithoutAuth(data): Observable<any> {
        return this.apiService.post('/user/forgotpassnoauth', data);
    }

    delete(password: string): Observable<any> {
        return this.apiService.post(`/user/deactivate`, { password });
    }

    reactivate(token: string): Observable<any> {
        return this.apiService.get(`/user/reactivate/${token}`);
    }

    changeAccount(id): Observable<any> {
        return this.apiService.patch(`/user/switch/${id}`);
    }

    sendVerification(): Observable<any> {
        return this.apiService.get('/user/resend/mverify');
    }

    changeLanguage(language): Observable<any> {
        return this.apiService.upload('/user/language', language, 'patch');
    }

    getByUsername(search: string): Observable<any> {
        return this.apiService.get('/user/username/' + search, 'json', true);
    }

    follow(id: number): Observable<any> {
        return this.apiService.post(`/user/following/${id}`, {});
    }

    unfollow(id: number): Observable<any> {
        return this.apiService.delete(`/user/following/${id}`);
    }

    getFollowings(page = 1, limit = 15, search = ''): Observable<any[]> {
        const params: any = { page, limit };

        if (search) {
            params.search = search;
        }

        return this.apiService.get(`/user/following/`, null, null, params);
    }

    getFollowers(page = 1, limit = 15, userId = null, search = ''): Observable<any[]> {
        const params: any = { page, limit };

        if (search) {
            params.search = search;
        }

        if (userId) {
            return this.apiService.get(`/user/${userId}/followers/`, null, null, params);
        } else {
            return this.apiService.get(`/user/followers/`, null, null, params);
        }
    }

    getTimeline(page = 1, limit = 15, userId = null): Observable<any> {
        const params: any = {
            page,
            limit,
        };
        if (userId) {
            params.userId = userId;
        }
        return this.apiService.get('/user/timeline', null, false, params);
    }

    getFeed(page = 1, limit = 15): Observable<any> {
        const params = {
            page,
            limit,
        };
        return this.apiService.get('/user/feed', null, false, params);
    }

    search(search: string, page = 1, limit = 20, except = []): Observable<any[]> {
        const params: any = {
            search,
            page,
            limit,
        };

        if (except.length) {
            params.except = JSON.stringify(except);
        }

        return this.apiService.get('/user/search', null, true, params);
    }

    getById(id: number): Observable<any> {
        return this.apiService.get(`/user/${id}`);
    }

    blockedList(): Observable<any> {
      return this.apiService.get('/user/blocked');
    }

    block(id: number): Observable<any> {
      return this.apiService.post(`/user/blocked/${id}`, {});
    }

    unblock(id: number): Observable<any> {
        return this.apiService.delete(`/user/blocked/${id}`);
    }

    getFollowRequests(offset = 0, limit = 3): Observable<any> {
        const params: any = {
            offset,
            limit,
        };
        return this.apiService.get(`/user/followers/requests`, null, false, params);
    }

    approveFollowingRequest(requestId: number) {
        return this.apiService.put(`/user/followers/requests/approve/${requestId}`, {});
    }

    rejectFollowingRequest(requestId: number) {
        return this.apiService.put(`/user/followers/requests/reject/${requestId}`, {});
    }

    validateKycDocument(file: string): Observable<any> {
        return this.apiService.post(`/user/kyc/validate`, { file });
    }

    refreshJwtToken(refreshToken: string): Observable<any> {
        const headers = { refreshToken };
        return this.http.post(environment.baseUrl + '/user/token', {}, { headers });
    }
}
