import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})

export class OrderService {

  constructor(
    public apiService: ApiService,
  ) { }

  getPurchases(page = 1, limit = 15, filter = '', search = ''): Observable<any> {
    const params: any = { page, limit };

    if (search) {
      params.search = search;
    }

    if (filter) {
      params.filter = filter;
    }

    return this.apiService.get(`/orders/purchases`, null, null, params);
  }

  getSales(page = 1, limit = 15, filter = '', search = ''): Observable<any> {
    const params: any = { page, limit };

    if (search) {
      params.search = search;
    }
    
    if (filter) {
      params.filter = filter;
    }

    return this.apiService.get(`/orders/sales`, null, null, params);
  }

  getOrder(orderId: number) {
    return this.apiService.get(`/orders/${orderId}`)
  }

  ship(id: number, data: any): Observable<any> {
    return this.apiService.post(`/orders/${id}/ship`, data);
  }

  cancel(id: number, data: any): Observable<any> {
    return this.apiService.post(`/orders/${id}/cancel`, data);
  }

  receive(id: number, data: any): Observable<any> {
    return this.apiService.post(`/orders/${id}/receive`, data);
  }

  return(id: number, data: any): Observable<any> {
    return this.apiService.post(`/orders/${id}/return`, data);
  }

  refund(id: number, data: any): Observable<any> {
    return this.apiService.post(`/orders/${id}/refund`, data);
  }
}
