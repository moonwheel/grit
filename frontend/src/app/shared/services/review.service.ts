import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as Tus from 'tus-js-client';
import { Store } from '@ngxs/store';

import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';
import { AuthState } from 'src/app/store/state/auth.state';
import { UserState } from 'src/app/store/state/user.state';
import { UploadStatusMessage } from './abstract.service';

@Injectable({
  providedIn: 'root',
})

export class ReviewService {

  constructor(
    public apiService: ApiService,
    protected store: Store,
  ) { }

  getList(entity: string, itemId: number, page = 1, limit = 15, rating: string, orderBy: string, order: string): Observable<any> {
    const params: any = { page, limit };

    if (rating) {
      params.rating = rating;
    }

    if (orderBy) {
      params.order = order;
      params.orderBy = orderBy;
    }

    const entityName = entity === 'seller' ? 'product/seller' : entity;

    return this.apiService.get(`/${entityName}/${itemId}/review`, null, null, params);
  }

  add(entity: string, itemId: number, data: any): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.post(`/${entityName}/${itemId}/review`, data);
  }

  getReview(entity: string, itemId: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.get(`/${entityName}/review/${itemId}`);
  }

  edit(entity: string, data: any): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.patch(`/${entityName}/review/${data.id}`, data);
  }

  delete(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.delete(`/${entityName}/review/${id}`);
  }

  like(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.post(`/${entityName}/review/${id}/like`, {});
  }

  likeReply(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.post(`/${entityName}/review/reply/${id}/like`, {});
  }

  getLikes(entity: string, id: number): Observable<any[]> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.get(`/${entityName}/review/${id}/like`);
  }

  getLikesReply(entity: string, id: number): Observable<any[]> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.get(`/${entityName}/review/reply/${id}/like`);
  }

  addVideo(entity: string, id: number, data: any): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.patch(`/${entityName}/review/${id}/video`, data);
  }

  removePhoto(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.delete(`/${entityName}/review/${id}/photo`);
  }

  removeVideo(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.delete(`/${entityName}/review/${id}/video`);
  }

  getReplies(entity: string, id: number): Observable<any[]> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.get(`/${entityName}/review/${id}/replies`);
  }

  addReply(entity: string, replyTo: number, review: any): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.post(`/${entityName}/review/${replyTo}/reply`, {
      ...review,
      replyTo,
    });
  }

  editReply(entity: string, data: any): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.patch(`/${entityName}/review/reply/${data.id}`, data);
  }

  deleteReply(entity: string, id: number): Observable<any> {
    const entityName = entity === 'seller' ? 'product/seller' : entity;
    return this.apiService.delete(`/${entityName}/review/reply/${id}`);
  }

  uploadFileByChunks(id: number, file: File, entity: string, metadata: any = {}): Observable<UploadStatusMessage> {
    return new Observable(observer => {
      const token = this.store.selectSnapshot(AuthState.token);
      const user = this.store.selectSnapshot(UserState.loggedInAccount);

      const entityName = entity === 'seller' ? 'product/seller' : entity;

      const userId = user && user.id;
      const upload = new Tus.Upload(file, {
        endpoint: `${environment.baseUrl}/${entityName}/review/${id}/upload`,
        retryDelays: [0, 3000, 5000, 10000, 20000],
        chunkSize: 1 * 1024 * 1024,
        headers: {
          Authorization: `Bearer ${token.replace(/\"/g, '')}`,
        },
        metadata: {
          id,
          userId,
          entityType: entity,
          size: file.size,
          name: file.name,
          type: file.type,
          ...metadata,
        },
        onError: (error) => {
          observer.error(error);
        },
        onProgress: (bytesUploaded, bytesTotal) => {
          const percentage = bytesUploaded / bytesTotal * 100;

          if (percentage === 100) {
            observer.next({
              type: 'success',
              value: 100,
              bytesUploaded: file.size
            });
            observer.complete();
          } else {
            observer.next({
              type: 'progress',
              value: Math.floor(percentage),
              bytesUploaded,
            });
          }
        },
        onSuccess: () => {
          observer.next({
            type: 'success',
            value: 100,
            bytesUploaded: file.size
          });
          observer.complete();
        }
      });

      upload.findPreviousUploads().then((previousUploads) => {
        if (previousUploads.length) {
          upload.resumeFromPreviousUpload(previousUploads[previousUploads.length - 1]);
        }

        upload.start();
      });
    });
  }
}
