import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CartProductModel } from 'src/app/shared/models';

import { ApiService } from './api.service';

export interface CartRequestModel {
  id?: number;
  product_id: number;
  quantity: number;
  product_variant_id: number;
}

export interface CartResponseModel {
  count: number;
  productsPrice: number;
  shippingPrice: number;
  totalPrice: number;
  products: CartProductModel[];
}

@Injectable({
  providedIn: 'root',
})

export class CartService {
  constructor(
    private apiService: ApiService,
  ) { }

  list(page = 1, limit = 15): Observable<CartResponseModel> {
    return this.apiService.get('/cart', null);
  }

  add(data: CartRequestModel): Observable<CartResponseModel> {
    return this.apiService.post('/cart', data);
  }

  edit(data: CartRequestModel): Observable<CartResponseModel> {
    return this.apiService.put(`/cart/${data.id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.apiService.delete(`/cart/${id}`);
  }

  checkout(data: any): Observable<any> {
    return this.apiService.post(`/payments/process`, data);
  }
}
