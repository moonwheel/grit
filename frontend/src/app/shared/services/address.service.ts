import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { AddressModel } from '../models';

@Injectable({
    providedIn: 'root'
})
export class AddressService {
    constructor(
        private apiService: ApiService
    ) { }

    getAll(): Observable<any> {
        return this.apiService.get('/address', null);
    }

    add(data: AddressModel): Observable<any> {
        return this.apiService.post('/address', data);
    }

    edit(data: AddressModel): Observable<any> {
        return this.apiService.patch(`/address/${data.id}`, data);
    }

    delete(id: number): Observable<any> {
        return this.apiService.delete(`/address/${id}`);
    }
}
