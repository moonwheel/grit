import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { NotificationModel, AccountModel } from '../models';
import { mapRawbookmarkModelToAppModel } from '../models/bookmark.model';
import { map } from 'rxjs/operators';
import { Utils } from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  prefix = '/notification';

  constructor(
    protected apiService: ApiService,
  ) {}

  getAll(page = 1, limit = 15): Observable<{ total: number, items: NotificationModel[] }> {
    const params: any = {
      page,
      limit,
    };

    return this.apiService.get(`${this.prefix}`, null, false, params).pipe(map(data => {
      if(data.raw) {
        data.items = data.items.map(item => Utils.convertRawQueryTree(item))
      }

      return data;
    }));
  }

  getUnreadCount(): Observable<{ count: number }> {
    return this.apiService.get(`${this.prefix}/unread`);
  }

  read(idArray: number[]): Observable<any> {
    const data: any = {
      idArray
    };
    return this.apiService.put(`${this.prefix}`, data);
  }

  readAll(): Observable<any> {
    return this.apiService.put(`${this.prefix}`, {});
  }

  setConfig(data: Partial<AccountModel>): Observable<any> {
    return this.apiService.put(`${this.prefix}/settings`, data);
  }
}
