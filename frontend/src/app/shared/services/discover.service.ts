import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DiscoverService {
  constructor(
    private apiService: ApiService
  ) { }

  load(type: string, category: string, page = 1, limit = 15, lat = null, lng = null, radius = null): Observable<any> {
    const params: any = { page, limit, category: category.toLowerCase() };
    if (lat !== null && lng !== null && radius !== null) {
      params.lat = lat;
      params.lng = lng;
      params.radius = radius;
    }
    return this.apiService.get(`/discover/${type}s`, null, null, params);
  }

}
