import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ScrollObservableService {
  private scrollSubject$: Subject<any> = new Subject();
  
  public scrolled$ = this.scrollSubject$.asObservable();

  handleScroll(event: any) {
    this.scrollSubject$.next(event);
  }
}
