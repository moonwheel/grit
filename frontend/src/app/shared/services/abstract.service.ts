import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as Tus from 'tus-js-client';
import { AuthState } from 'src/app/store/state/auth.state';
import { Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { AbstractEntityType } from '../models';
import { CommentModel } from '../components/actions/comments/comment.model';

export interface UploadStatusMessage {
  type: 'progress' | 'success';
  value?: number;
  bytesUploaded?: number;
}

export abstract class AbstractService {

  protected abstract prefix: string;

  constructor(
    protected apiService: ApiService,
    protected store: Store,
  ) { }

  getAll(
    page = 1,
    limit = 15,
    filter = '',
    order = 'desc',
    userId = null,
    draft = false,
    search = '',
    categoryId = null
  ): Observable<{ total: number, items: any[] }> {
    const params: any = {
      filter,
      order,
      draft,
      page,
      limit,
      search,
    };

    if (userId) {
      params.userId = userId;
    }

    if (categoryId) {
      params.shopCategoryId = categoryId;
    }

    return this.apiService.get(`${this.prefix}/all`, null, false, params);
  }

  getOne(id: number): Observable<any> {
    return this.apiService.get(`${this.prefix}/${id}`);
  }

  add(data: any): Observable<any> {
    return this.apiService.post(`${this.prefix}`, data);
  }

  addWithUpload(data: any): Observable<any> {
    return this.apiService.upload(`${this.prefix}`, data, 'post');
  }

  upload(id: number, data: FormData): Observable<any> {
    const headers = new HttpHeaders({ 'content-type': 'multipart/form-data' });
    return this.apiService.upload(`${this.prefix}/${id}/upload`, data, 'put', headers);
  }

  edit(data: any): Observable<any> {
    return this.apiService.put(`${this.prefix}`, data);
  }

  editWithUpload(data: any): Observable<any> {
    return this.apiService.upload(`${this.prefix}`, data, 'put');
  }

  delete(id: number): Observable<any> {
    return this.apiService.delete(`${this.prefix}/${id}`);
  }

  uploadFileByChunks(id: number, file: File, entityType: AbstractEntityType, metadata: any = {}): Observable<UploadStatusMessage> {
    return new Observable(observer => {
      const token = this.store.selectSnapshot(AuthState.token);
      const user = this.store.selectSnapshot(UserState.loggedInAccount);
      const userId = user && user.id;
      const upload = new Tus.Upload(file, {
        endpoint: `${environment.baseUrl}${this.prefix}/${id}/upload`,
        retryDelays: [0, 3000, 5000, 10000, 20000],
        chunkSize: 1 * 1024 * 1024,
        headers: {
          Authorization: `Bearer ${token.replace(/\"/g, '')}`,
        },
        metadata: {
          id,
          userId,
          entityType,
          size: file.size,
          name: file.name,
          type: file.type,
          ...metadata,
        },
        onError: (error) => {
          observer.error(error);
        },
        onProgress: (bytesUploaded, bytesTotal) => {
          const percentage = bytesUploaded / bytesTotal * 100;

          if (percentage === 100) {
            observer.next({
              type: 'success',
              value: 100,
              bytesUploaded: file.size
            });
            observer.complete();
          } else {
            observer.next({
              type: 'progress',
              value: Math.floor(percentage),
              bytesUploaded,
            });
          }
        },
        onSuccess: () => {
          observer.next({
            type: 'success',
            value: 100,
            bytesUploaded: file.size
          });
          observer.complete();
        }
      });

      upload.findPreviousUploads().then((previousUploads) => {
        if (previousUploads.length) {
          upload.resumeFromPreviousUpload(previousUploads[previousUploads.length - 1]);
        }

        upload.start();
      });
    });
  }

  like(id: number): Observable<any> {
    return this.apiService.post(`${this.prefix}/${id}/like`, {});
  }

  getLikes(id: number, page: number = 1, limit: number = 15): Observable<any> {
    const params = {
      page,
      limit,
    };
    return this.apiService.get(`${this.prefix}/${id}/like`, null, false, params);
  }

  comment(id: number, comment: Partial<CommentModel>, replyTo: number = null): Observable<any> {
    const params: any = {
      ...comment
    };

    if (replyTo) {
      params.replyTo = replyTo;
    }

    return this.apiService.post(`${this.prefix}/${id}/comment`, params);
  }

  editComment(id: number, comment: Partial<CommentModel>, replyTo: number = null): Observable<any> {
    const params: any = {
      ...comment
    };

    if (replyTo) {
      params.replyTo = replyTo;
    }

    return this.apiService.put(`${this.prefix}/${id}/comment`, params);
  }

  getComments(id: number, filterBy = 'relevance', page: number = 1, limit: number = 15): Observable<any> {
    const params = {
      page,
      limit,
      filterBy,
      order: 'desc',
    };
    return this.apiService.get(`${this.prefix}/${id}/comment`, null, false, params);
  }

  getReplies(commentId: number, page: number = 1, limit: number = 15): Observable<any> {
    const params = {
      page,
      limit,
    };
    return this.apiService.get(`${this.prefix}/comment/${commentId}/replies`, null, false, params);
  }

  getCommentLikes(id: number, reply = false, page: number = 1, limit: number = 15) {
    const params: any = {
      page,
      limit,
    };
    if (reply) {
      params.reply = reply;
    }
    return this.apiService.get(`${this.prefix}/comment/${id}/like`, null, false, params);
  }

  likeComment(id: number, reply: boolean = false) {
    const params: any = {};

    if (reply) {
      params.reply = reply;
    }

    return this.apiService.post(`${this.prefix}/comment/${id}/like`, params);
  }

  deleteComment(id: number, reply: boolean = false) {
    const params: any = {};

    if (reply) {
      params.reply = reply;
    }

    return this.apiService.delete(`${this.prefix}/comment/${id}`, params);
  }

  view(id: number): Observable<any> {
    return this.apiService.post(`${this.prefix}/${id}/view`, {});
  }
}
