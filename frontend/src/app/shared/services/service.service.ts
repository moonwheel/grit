import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { AbstractService } from './abstract.service';
import { Store } from '@ngxs/store';

@Injectable({
    providedIn: 'root'
})
export class ServiceService extends AbstractService {

    prefix = '/service';

    constructor(
        protected apiService: ApiService,
        protected store: Store,
    ) {
        super(apiService, store);
    }

}
