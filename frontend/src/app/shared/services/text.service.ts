import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { AbstractService } from './abstract.service';
import { Store } from '@ngxs/store';

@Injectable({
    providedIn: 'root'
})
export class TextService extends AbstractService {

    prefix = '/text';

    constructor(
        protected apiService: ApiService,
        protected store: Store,
    ) {
        super(apiService, store);
    }

    add(data): Observable<any> {
        return this.apiService.post('/text', data);
    }

    edit(data): Observable<any> {
        return this.apiService.put('/text', data);
    }

    delete(id): Observable<any> {
        return this.apiService.delete(`/text/${id}`);
    }

}
