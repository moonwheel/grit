import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { BookingModel } from 'src/app/shared/models';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})

export class BookingService {
  
  constructor(
    private apiService: ApiService,
  ) { }

  add(data: any): Observable<any> {
    return this.apiService.post('/bookings', data);
  }

  cancel(id: number): Observable<any> {
    return this.apiService.post(`/bookings/${id}/cancel`, {});
  }

  refund(id: number): Observable<any> {
    return this.apiService.post(`/bookings/${id}/refund`, null);
  }
}
