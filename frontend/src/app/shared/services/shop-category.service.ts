import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class ShopCategoryService {
    constructor(
        private apiService: ApiService
    ) { }

    getAll(): Observable<any> {
        return this.apiService.get('/shopcategory/all', null);
    }

    add(data): Observable<any> {
        return this.apiService.post('/shopcategory', data);
    }

    edit(data): Observable<any> {
        return this.apiService.put('/shopcategory', data);
    }

    delete(id): Observable<any> {
        return this.apiService.delete(`/shopcategory/${id}`);
    }
}
