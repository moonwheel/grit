
import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  previousUrl: string;

  counter = 1;

  private storageKey = 'previousUrl';
  private storageCounterKey = 'previousUrlCounter';
  private backButtonPathKey = 'backButtonPath';
  // this used to avoid side effects of back navigation after canDeactivate guard returned false
  private defendAgainstBrowserBackButton = false;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private store: Store,
  ) {
    window.onpopstate = () => this.defendAgainstBrowserBackButton = true;

    this.previousUrl = localStorage.getItem(this.storageKey);
    this.counter = +localStorage.getItem(this.storageCounterKey) - 1;

    let prevPathname = this.previousUrl;

    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    ).subscribe((event: NavigationStart) => {
      const split = event.url.split('?');

      prevPathname = location.pathname.toString();
      
      const backButtonForcedPath = localStorage.getItem(this.backButtonPathKey);

      if (split.length <= 1 || split[0] !== location.pathname && !backButtonForcedPath) {
        this.previousUrl = location.pathname.toString();
      }

      localStorage.setItem(this.storageKey, this.previousUrl);
    });

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.defendAgainstBrowserBackButton = false;

      if (prevPathname === location.pathname) {
        this.counter++;
      } else {
        this.counter = 1;
      }

      localStorage.setItem(this.storageCounterKey, this.counter.toString());
    });
  }

  back(forbiddenRoutes: string[] = []): void {
    const backButtonForcedPath = localStorage.getItem(this.backButtonPathKey);

    if(backButtonForcedPath) { // this is hack used to set custom redirecting for "back button action"
      this.router.navigateByUrl(backButtonForcedPath);
      return;
    } 

    if (this.previousUrl) {
      if (forbiddenRoutes.indexOf(this.previousUrl) === -1) {
        if (history.length > 1) {
          if (this.counter === 0) {
            this.counter = 1;
          }
          history.go(-this.counter);
          this.counter = 0;
        } else {
          this.defaultGoBack();
        }
      } else {
        if (history.length > 3) {
          history.go(-this.counter - 1);
          this.counter = 0;
        } else {
          this.defaultGoBack();
        }
      }
    } else {
      this.defaultGoBack();
    }
  }

  backAvoidHomeAddPage(): void {
    const pageName = this.store.selectSnapshot(UserState.loggedInAccountPageName);
    const urlToAvoid = `/${pageName}/home/add`;

    this.back([urlToAvoid]);
  }

  nativeGoBack(): void {
    history.back();
  }

  makeSafeBackNavigationAfterCanDeactivateGuard() {
    if (this.defendAgainstBrowserBackButton) {
      history.pushState(null, '', '');
    }
  }

  private defaultGoBack() {
    this.router.navigate(['../'], {
      relativeTo: this.getLastChild(this.activatedRoute)
    });
  }

  private getLastChild(route: ActivatedRoute): ActivatedRoute {
    if (route.firstChild) {
      return this.getLastChild(route.firstChild);
    } else {
      return route;
    }
  }
}
