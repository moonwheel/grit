import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  constructor(
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<any> {
    return this.apiService.get('/business');
  }

  getOne(id): Observable<any> {
    return this.apiService.get(`/business/${id}`);
  }

  getBusinessCategories(): Observable<any> {
    return this.apiService.get('/business/categories');
  }

  createNewBusiness(data): Observable<any> {
    return this.apiService.post('/business', data);
  }

  updateBusiness(data, id): Observable<any> {
    return this.apiService.patch(`/business/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.apiService.delete(`/business/${id}`);
  }

  addUserToBusiness(id: number, data: any): Observable<any> {
    return this.apiService.post(`/business/${id}/users`, data);
  }

  updateBusinessUserRole(businessId: number, userId: number, role: string): Observable<any> {
    return this.apiService.put(`/business/${businessId}/users/${userId}`, { role });
  }

  deleteUserFromBusiness(businessId: number, userId: number): Observable<any> {
    return this.apiService.delete(`/business/${businessId}/users/${userId}`);
  }

  getBuisinessUsers(id: number, search = '', page: number = 1, limit: number = 15): Observable<any> {
    const params: any = {
      page,
      limit,
    };

    if (search) {
      params.search = search;
    }

    return this.apiService.get(`/business/${id}/users`, null, false, params);
  }

  searchPossibleMembers(businessId: number, search = '', page: number = 1, limit: number = 15): Observable<any> {
    const params: any = {
      page,
      limit,
    };

    if (search) {
      params.search = search;
    }

    return this.apiService.get(`/business/${businessId}/users/search-possible-members`, null, false, params);
  }

  restoreBusiness(token: string): Observable<any> {
    const params: any = {
    };

    return this.apiService.get(`/business/reactivate/${token}`, params);
  }
}
