import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

// Guards
import { UnauthorizedGuard, AuthGuard, ProfileGuard } from './core/guards';

const routes: Routes = [
  {
    path: 'register',
    loadChildren: () => import('./modules/register/register.module').then(m => m.RegisterModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
    canActivate: [UnauthorizedGuard]
  },
  {
    path: 'changepass',
    loadChildren: () => import('./modules/forgotpass/forgotpass.module').then(m => m.ForgotPassModule),
  },
  {
    path: 'error',
    loadChildren: () => import('./modules/error/error.module').then(m => m.ErrorModule),
    canActivate: [AuthGuard]
  },
  {
    path: '404',
    loadChildren: () => import('./modules/not-found/not-found.module').then(m => m.NotFoundModule)
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./modules/homepage/homepage.module').then(m => m.HomepageModule),
    canActivate: [UnauthorizedGuard]
  },
  {
    path: '',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule),
    canActivate: [AuthGuard]
  },
  {
    path: ':username',
    loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule),
    canActivate: [AuthGuard, ProfileGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'corrected',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule],
  providers: [],
})

export class AppRouting { }
