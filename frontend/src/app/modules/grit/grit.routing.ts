import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard, AdminGuard } from 'src/app/core/guards';

const routes: Routes = [{
  path: '',
  loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule),
  canActivate: [AuthGuard, AdminGuard],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class GritRouting { }
