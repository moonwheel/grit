import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GritRouting } from './grit.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GritRouting,
    SharedModule,
  ]
})

export class GritModule { }
