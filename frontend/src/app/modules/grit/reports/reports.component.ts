import { Component, OnInit, ViewEncapsulation, ViewChildren, QueryList, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NgModel, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, EMPTY, of, Subject } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { Select, Store } from '@ngxs/store';

import { ReportState, ReportStateModel } from '../../../store/state/report.state';
import { LoadReports, InitReport, DeleteReport, EditReport, LoadReportCategories, LoadReportTypes } from '../../../store/actions/report.actions';
import { TranslateService } from '@ngx-translate/core';

import { switchMap, skip, debounceTime, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})

export class ReportsComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ReportState.reports)
  reports$: Observable<any[]>;

  @Select(ReportState.total)
  total$: Observable<number>;

  @Select(ReportState.loading)
  loading$: Observable<boolean>;

  @Select(ReportState.categories)
  categories$: Observable<any[]>;

  @Select(ReportState.types)
  types$: Observable<any[]>;

  @ViewChildren('searchInput', { read: NgModel })
  searchInput: QueryList<NgModel>;

  @ViewChild('searchInputElement')
  searchInputElement: ElementRef;

  displayedColumns: string[] = [
    'id',
    'type',
    'user',
    'target',
    'category',
    'description',
    'created',
    'status',
    'options',
  ];

  statuses = [{
    id: 'all',
    name: 'All',
  }, {
    id: 'open',
    name: 'Open',
  }, {
    id: 'checking',
    name: 'Checking',
  }, {
    id: 'checked',
    name: 'Checked',
  }, {
    id: 'removed',
    name: 'Removed',
  }];

  reportsMapping = {};

  dataSource = new MatTableDataSource<any>([]);

  destroy$ = new Subject();

  header = true;
  search = false;

  page = 1;
  pageSize = 15;

  filterForm: FormGroup;

  searchValue = '';

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.store.dispatch(new InitReport(this.pageSize));
    this.store.dispatch(new LoadReportCategories());
    this.store.dispatch(new LoadReportTypes());

    this.filterForm = this.formBuilder.group({
      type: [-1, Validators.required],
      category: [-1, Validators.required],
      status: ['all', Validators.required]
    });

    this.reports$.subscribe(reports => {
      this.dataSource.data = reports;
    });

    this.filterValueChange();

    this.translateService.stream('PAGES.REPORTS.REPORT_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.reportsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      const filters = this.filterForm.value;
      this.page = 1;
      this.store.dispatch(new LoadReports(this.page, this.pageSize, filters, this.searchValue));
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchValue) {
      const filters = this.filterForm.value;
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new LoadReports(this.page, this.pageSize, filters, this.searchValue));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  filterValueChange() {
    this.filterForm.valueChanges.subscribe(filterData => {
      this.page = 1;
      this.store.dispatch(new LoadReports(this.page, this.pageSize, filterData, this.searchValue));
    });
  }

  getSearchValue(query: string) {
    this.searchValue = query;
  }

  clearSearchValue() {
    this.searchValue = '';
  }

  checkReport(id: number) {
    this.store.dispatch(new EditReport(id, {
      status: 'checked',
    }));
  }

  checkingReport(id: number) {
    this.store.dispatch(new EditReport(id, {
      status: 'checking',
    }));
  }

  deleteReport(id: number) {
    this.store.dispatch(new DeleteReport(id));
  }

  getUserId(user) {
    if (user && user.business) {
      return user.business.id;
    } else if (user && user.person) {
      return user.person.id;
    } else {
      return null;
    }
  }
}
