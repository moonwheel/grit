import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

import { ReportsRouting } from './reports.routing';
import { ReportsComponent } from './reports.component';

import { ReportsOptionsComponent } from '../reports-options/reports-options.component';

@NgModule({
  declarations: [
    ReportsComponent,
    ReportsOptionsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReportsRouting,
  ]
})

export class ReportsModule { }
