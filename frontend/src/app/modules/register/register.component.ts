import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { ChangeAccount, Login, Register, SetAuthLoading } from 'src/app/store/actions/auth.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { BusinessState } from 'src/app/store/state/business.state';
import { Observable, Subject } from 'rxjs';
import { BusinessCategoryModel } from 'src/app/shared/models/business-category.model';
import { DateTime } from 'luxon';
import { Router } from '@angular/router';
import { CreateNewBusiness, GetBusinessCategories, InitBusiness } from 'src/app/store/actions/business.actions';
import { ValidatePassword } from 'src/app/core/validators/password.validator';
import { switchMapTo, takeUntil, filter } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';
import { AuthState } from 'src/app/store/state/auth.state';

@Component( {
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
} )

export class RegisterComponent implements OnInit, OnDestroy {

  @Select( BusinessState.categories ) categories$: Observable<BusinessCategoryModel[]>;
  @Select( AuthState.loading ) loading$: Observable<boolean>;

  public months = [ ...constants.MONTHS ];

  public maxDate = 31;

  public registerForm: FormGroup;

  public person = true;
  public business = false;
  public password = false;
  public hasAccount = false;
  public maxYear = DateTime.local().year;
  public minYear = DateTime.local().minus( {years: 100} ).year;
  public businessCategories = [];
  public createBusiness = false;
  public createNewBusiness = false;
  public isShortPassword = false;
  public sameEmail = '';
  public richedBusinessLimit = false;

  private destroy$ = new Subject();

  constructor(
    private navigationService: NavigationService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private store: Store,
    private router: Router,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    const type = window.location.search;
    if ( type.includes( 'business' ) ) {
      this.createBusiness = true;
      this.person = false;
      this.business = true;
    }

    this.registerForm = this.formBuilder.group( {
      user: this.formBuilder.group( {
        userType: [this.createBusiness ? 'business' : 'person', Validators.required],
        fullName: ['', !this.createBusiness && Validators.required],
        email: ['', !this.createBusiness && Validators.required],
        password: ['', !this.createBusiness && [Validators.required, ValidatePassword]],
        gender: ['', !this.createBusiness && Validators.required],
        day: ['', !this.createBusiness && Validators.required],
        month: ['', !this.createBusiness && Validators.required],
        year: ['', !this.createBusiness && Validators.required],
        birthday: ''
      } ),
      business: this.formBuilder.group( {
        name: ['', this.createBusiness && Validators.required],
        category: ['', this.createBusiness && Validators.required],
        street: ['', this.createBusiness && Validators.required],
        zip: ['', this.createBusiness && Validators.required],
        city: ['', this.createBusiness && Validators.required],
        phone: ['', this.createBusiness && Validators.required]
      } )
    } );

    this.getBusinessCategories();
    this.updateBusinessValidation();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getBusinessCategories() {
    this.store.dispatch( new GetBusinessCategories() ).subscribe( ( data ) => {
      const categories = Object.getOwnPropertyNames( data.business.categories );
      categories.forEach( item => {
        this.businessCategories.push( {
          name: item,
          categories: data.business.categories[item]
        } );
      } );
    } );
  }

  updateBusinessValidation() {
    const name = this.dateBusinessForm.get( 'name' );
    const category = this.dateBusinessForm.get( 'category' );
    const street = this.dateBusinessForm.get( 'street' );
    const zip = this.dateBusinessForm.get( 'zip' );
    const city = this.dateBusinessForm.get( 'city' );
    const phone = this.dateBusinessForm.get( 'phone' );

    this.userForm.get( 'userType' ).valueChanges
      .subscribe( userType => {
        if ( userType === 'business' ) {
          name.setValidators( [Validators.required] );
          category.setValidators( [Validators.required] );
          street.setValidators( [Validators.required] );
          zip.setValidators( [Validators.required] );
          city.setValidators( [Validators.required] );
          phone.setValidators( [Validators.required] );
        } else {
          if ( this.hasAccount ) {
            this.switchUserForm();
          }
          name.clearValidators();
          name.updateValueAndValidity();
          category.clearValidators();
          category.updateValueAndValidity();
          street.clearValidators();
          street.updateValueAndValidity();
          zip.clearValidators();
          zip.updateValueAndValidity();
          city.clearValidators();
          city.updateValueAndValidity();
          phone.clearValidators();
          phone.updateValueAndValidity();
        }
      } );
  }

  get userForm() {
    return this.registerForm.get( 'user' ) as FormGroup;
  }

  get dateBusinessForm() {
    return this.registerForm.get( 'business' ) as FormGroup;
  }

  countMaxDate() {
    const chosenMonth = this.userForm.controls.month.value;
    if ( [0, 2, 4, 6, 7, 9, 11].includes( chosenMonth ) ) {
      this.maxDate = 31;
    }
    if ( [3, 5, 8, 10].includes( chosenMonth ) ) {
      this.maxDate = 30;
    }
    if ( chosenMonth === 1 ) {
      if ( this.userForm.controls.year.value && !(this.userForm.controls.year.value % 4) ) {
        this.maxDate = 29;
      } else {
        this.maxDate = 28;
      }
    }
  }

  showPassword( event: Event ) {
    this.password = !this.password;
    event.stopPropagation();
  }

  showPerson() {
    this.person = true;
    this.business = false;
  }

  showBusiness() {
    this.person = false;
    this.business = true;
  }

  setUserType( e ) {
    if ( e.value === 'business' ) {
      this.showBusiness();
    } else {
      this.showPerson();
    }
  }

  switchUserForm() {
    this.hasAccount = !this.hasAccount;

    const fullName = this.userForm.get( 'fullName' );
    const day = this.userForm.get( 'day' );
    const month = this.userForm.get( 'month' );
    const year = this.userForm.get( 'year' );
    const gender = this.userForm.get( 'gender' );
    const email = this.userForm.get( 'email' );
    const password = this.userForm.get( 'password' );

    if ( this.hasAccount ) {
      fullName.clearValidators();
      fullName.updateValueAndValidity();
      day.clearValidators();
      day.updateValueAndValidity();
      month.clearValidators();
      month.updateValueAndValidity();
      year.clearValidators();
      year.updateValueAndValidity();
      gender.clearValidators();
      gender.updateValueAndValidity();
      email.clearValidators();
      email.updateValueAndValidity();
      password.clearValidators();
      password.updateValueAndValidity();
    } else {
      fullName.setValidators( [Validators.required] );
      day.setValidators( [Validators.required] );
      month.setValidators( [Validators.required] );
      year.setValidators( [Validators.required] );
      gender.setValidators( [Validators.required] );
      email.setValidators( [Validators.required] );
      password.setValidators( [Validators.required] );
    }
  }

  buildBirthdayValue() {
    let errorFound = false;
    const day = this.userForm.get( 'day' );
    const month = this.userForm.get( 'month' );
    const year = this.userForm.get( 'year' );
    this.countMaxDate();
    if ( +day.value > this.maxDate || !day.value ) {
      day.setErrors( {badDate: true} );
      errorFound = true;
    } else {
      day.setErrors( null );
    }
    if ( year.value > this.maxYear || year.value < this.minYear ) {
      year.setErrors( {badYear: true} );
      errorFound = true;
    } else {
      year.setErrors( null );
    }
    if ( errorFound || month.value === '' ) {
      return;
    }
    const textMonth = this.months.filter( item => item.id === month.value )[0].name;
    const textDate = `${ day.value } ${ textMonth } ${ year.value }`;
    this.userForm.get( 'birthday' ).setValue( DateTime.fromFormat( textDate, 'd MMM yyyy' ).toISO() );
  }

  showShortPasswordError() {
    const userForm = this.registerForm.get( 'user' );
    const password = userForm.get( 'password' );

    this.isShortPassword = !!(password.errors && password.dirty && password.errors.password);
  }

  signUp() {
    if ( this.userForm.get( 'userType' ).value === 'person' || !this.hasAccount && !this.createBusiness ) {
      this.buildBirthdayValue();
    }
    if ( !this.registerForm.valid ) {
      Utils.touchForm( this.registerForm );
      return;
    }

    this.store.dispatch(new SetAuthLoading(true));

    if ( this.userForm.get( 'userType' ).value === 'person' ) {
      delete this.userForm.value.day;
      delete this.userForm.value.month;
      delete this.userForm.value.year;
      const payload = {user: this.userForm.value};
      this.store.dispatch( new Register( payload ) )
        .subscribe( () => {
            this.store.dispatch( new Login( payload.user ) ).subscribe( () => {
              this.navigateToMyPage();
            });
          },
          (err) => {
            this.checkSameEmailError(err);
            this.store.dispatch(new SetAuthLoading(false));
          }
        );
    } else {
      const userPayload = {user: this.userForm.value};
      userPayload.user.userType = 'person';
      const businessPayload = {
        business: {
          name: this.dateBusinessForm.value.name,
          category: this.dateBusinessForm.value.category,
          phone: this.dateBusinessForm.value.phone + '',
          address: {
            street: this.dateBusinessForm.value.street,
            zip: this.dateBusinessForm.value.zip,
            city: this.dateBusinessForm.value.city
          }
        }
      };
      if ( this.hasAccount && !this.createBusiness ) {
        this.store.dispatch( new Login( userPayload.user ) ).subscribe( () => {
          this.store.dispatch( new CreateNewBusiness( businessPayload ) ).subscribe( ( data ) => {
            this.navigateToMyPage();
          }, err => {
            this.store.dispatch(new SetAuthLoading(false));
          } );
        } );
      } else if ( !this.hasAccount && !this.createBusiness ) {
        this.store.dispatch( new Register( userPayload ) ).subscribe( () => {
            this.store.dispatch( new Login( userPayload.user ) ).subscribe( () => {
              this.store.dispatch( new CreateNewBusiness( businessPayload ) ).subscribe( ( data ) => {
                this.store.dispatch( new InitBusiness() ).subscribe( () => {
                  this.navigateToMyPage();
                });
              } );
            } );
          },
          ( err ) => {
            this.checkSameEmailError(err);
            this.store.dispatch(new SetAuthLoading(false));
          }
        );
      } else {
        this.store.dispatch( new CreateNewBusiness( businessPayload ) ).subscribe( ( data ) => {
          if ( data.business && data.business.newBusinessId ) {
            this.store.dispatch(new ChangeAccount( data.business.newBusinessId)).pipe(
              switchMapTo(this.store.selectOnce(UserState.loggedInAccount))
            ).subscribe(account => {
              this.navigateToMyPage();
            }, err => {
              this.store.dispatch(new SetAuthLoading(false));
            } );
          } else {
            this.navigateToMyPage();
          }
        } );
      }
    }
  }

  back() {
    this.navigationService.back();
  }

  onKey( $event ) {
    const value = $event;
    if ( value >= 1000 ) {
      if ( value >= this.maxYear ) {
        this.userForm.get( 'year' ).setValue( this.maxYear, {
          emitEvent: false,
          emitViewToModelChange: false
        } );
      } else if ( value <= this.minYear ) {
        this.userForm.get( 'year' ).setValue( this.minYear, {
          emitEvent: false,
          emitViewToModelChange: false
        } );
      }
    }
  }

  private checkSameEmailError(errorResponse: any) {
    if (errorResponse.error.error === 'User with this email already exists in database') {
      this.userForm.get( 'email' ).setErrors( {duplicate: true} );
      this.translateService.get('PAGES.REGISTER.USER_FORM.EMAIL.ERRORS.SAME_EMAIL').subscribe(res => {
        this.sameEmail = res;
      });
    }
  }

  private navigateToMyPage() {
    this.store.select(UserState.loggedInAccountPageName).pipe(
      filter(item => Boolean(item)),
      takeUntil(this.destroy$)
    ).subscribe(async pageName => {
      await this.router.navigate( [`/${pageName}/home`] );
      this.store.dispatch(new SetAuthLoading(false));
    });
  }
}
