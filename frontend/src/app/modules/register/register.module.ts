import { NgModule } from '@angular/core';

// Components
import { RegisterComponent } from './register.component';

// Modules
import { RegisterRouting } from './register.routing';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    RegisterComponent,
  ],
  imports: [
    SharedModule,
    RegisterRouting,
  ]
})
export class RegisterModule { }
