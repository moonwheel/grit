import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ProductDetailComponent } from './product-detail.component';
import { UploadingGuard, UploadingGuardStateToken } from 'src/app/core/guards';
import { ProductState } from 'src/app/store/state/product.state';

const routes: Routes = [
  {
    path: '',
    component: ProductDetailComponent,
    canActivate: [ UploadingGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UploadingGuard,
    { provide: UploadingGuardStateToken, useValue: ProductState },
  ]
})
export class ProductDetailRouting {}
