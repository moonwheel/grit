import { Component, OnDestroy, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReviewListComponent } from '../../../../shared/components/actions/reviews/review-list/review-list.component';
import { SwiperConfigInterface, SwiperComponent } from 'ngx-swiper-wrapper';
import { UserState } from 'src/app/store/state/user.state';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, of } from 'rxjs';
import { ProductState } from 'src/app/store/state/product.state';
import { environment } from 'src/environments/environment';
import { ProductModel } from 'src/app/shared/models/content/products/product.model';
import { ProductVariantModel } from 'src/app/shared/models/content/products/product-variant.model';
import { ProductActions } from 'src/app/store/actions/product.actions';
import { AddToCart } from 'src/app/store/actions/cart.actions';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { switchMap, takeUntil, switchMapTo} from 'rxjs/operators';
import { Utils } from 'src/app/shared/utils/utils';
import { AbstractDetailPageComponent } from 'src/app/shared/components';
import { AccountModel } from 'src/app/shared/models';
import { constants } from 'src/app/core/constants/constants';

type ProductVariantType = 'color' | 'size' | 'material' | 'flavor';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
})

export class ProductDetailComponent extends AbstractDetailPageComponent implements OnInit, OnDestroy {

  // @ts-ignore
  @ViewChild(SwiperComponent) swiper: SwiperComponent;

  size: string = null;
  color: string = null;
  material: string = null;
  flavor: string = null;
  seller = null;

  @Select(UserState.viewableAccount) viewableAccount$: Observable<AccountModel>;
  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.viewableAccountPageName) viewableAccountPageName$: Observable<string>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(ProductState.loading) loading$: Observable<boolean>;

  product: ProductModel;

  config: SwiperConfigInterface = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
      bulletActiveClass: 'swiper-pagination__active'
    }
  };

  sizes: Array<string> = [];

  colors: Array<string> = [];

  flavors: Array<string> = [];

  materials: Array<string> = [];

  bookmarkColor = '#4d4d4d';

  selectedVariant: ProductVariantModel = null;

  environment = environment;

  quantity: number;

  variantPrices: { [key: string]: number | string } = {};

  currentUrl = '';

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  private destroy$ = new Subject();

  constructor(
    protected route: ActivatedRoute,
    protected dialog: MatDialog,
    protected store: Store,
    protected router: Router,
    protected snackbar: MatSnackBar,
  ) {
    super(dialog, store);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));

    this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(ProductState.product(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new ProductActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(ProductState.product(id)))
          );
        } else {
          return this.store.select(ProductState.product(id));
        }
      }),
      takeUntil(this.destroy$),
    ).subscribe((product: ProductModel) => {
      this.product = product;
      this.quantity = 1;
      if (this.product) {
        this.processProduct(this.product);
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params.dialog && params.dialog === 'review') {
        this.productReviews();
      }
    });

    this.currentUrl = window.location.href;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  // Data

  processProduct(product: ProductModel) {
    if (product.type === 'productVariants' && product && product.variants && product.variants.length) {
      const sizes = [];
      const flavors = [];
      const colors = [];
      const materials = [];

      product.variants.forEach(variant => {
        if (variant.enable) {
          sizes.push(variant.size);
          flavors.push(variant.flavor);
          colors.push(variant.color);
          materials.push(variant.material);

          const key = this.generateVariantPriceKey(variant);

          this.variantPrices[key] = variant.price;
        }
      });

      this.sizes = Array.from(new Set(sizes));
      this.flavors = Array.from(new Set(flavors));
      this.colors = Array.from(new Set(colors));
      this.materials = Array.from(new Set(materials));

      this.selectedVariant = Utils.getCheepestProductVariant(product);

      this.size = this.selectedVariant && this.selectedVariant.size;
      this.color = this.selectedVariant && this.selectedVariant.color;
      this.material = this.selectedVariant && this.selectedVariant.material;
      this.flavor = this.selectedVariant && this.selectedVariant.flavor;
    }
  }

  variantSelectionChange() {
    this.selectedVariant = this.product.variants.find((v) => {
      return v.size === this.size && v.color === this.color && v.material === this.material && v.flavor === this.flavor;
    });
  }

  getPhotosSource() {
    if (this.selectedVariant && this.selectedVariant.photos && this.selectedVariant.photos.length) {
      return this.selectedVariant.photos;
    } else if (this.product.photos && this.product.photos.length) {
      return this.product.photos;
    }
    return [{ photoUrl: 'images/photo-placeholder.png' }];
  }

  // Reviews

  sellerReviews() {
    this.dialog.open(ReviewListComponent, {
      disableClose: true,
      width: '500px',
      data: {
        entity: 'seller',
        item: {
          ...this.product.user,
          rating: this.product.sellerRating,
        },
      },
    });
  }

  productReviews() {
    if (this.product) {
      this.dialog.open(ReviewListComponent, {
        disableClose: true,
        width: '500px',
        data: {
          entity: 'product',
          item: this.product,
        },
      });
    }
  }

  // Actions

  deleteDialog(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  deleteProduct(id: number) {
    this.store.dispatch(new ProductActions.Delete(id));
    this.dialog.closeAll();
    const pageName = this.store.selectSnapshot(UserState.loggedInAccountPageName);
    this.router.navigate([`/${pageName}/shop`]);
  }

  reportDialog(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  counter(count: number) {
    count = +count;
    return Array(count <= 10 ? count : 10);
  }

  isPriceNotFree(price: any) {
    return !!price && price !== '0' && price !== '0.00' && price !== '0,00';
  }

  getVariantPrice(key: ProductVariantType, value: string) {
    if (this.selectedVariant) {
      const priceKey = this.generateVariantPriceKey(this.selectedVariant, key, value);
      return this.variantPrices[priceKey];
    } else {
      return '';
    }
  }

  variantExists(key: ProductVariantType, value: string) {
    if (this.selectedVariant) {
      const priceKey = this.generateVariantPriceKey(this.selectedVariant, key, value);
      return priceKey in this.variantPrices;
    } else {
      return false;
    }
  }

  private generateVariantPriceKey(variant: ProductVariantModel, key?: ProductVariantType, value?: string): string {
    let result = '';

    for (const param of ['color', 'size', 'material', 'flavor']) {
      if (param === key) {
        result += value;
      } else {
        result += variant[param] || '';
      }
    }

    return result;
  }

  inactiveProduct(product: ProductModel) {
    this.store.dispatch(new ProductActions.ChangeActiveState(product));
  }

  addToCart() {
    this.store.dispatch(new AddToCart({
      product_id: this.product.id,
      quantity: this.quantity,
      product_variant_id: this.selectedVariant ? this.selectedVariant.id : null,
    }));
    this.snackbar.open('Product added to the cart', undefined, { panelClass: 'centered-snackbar-text' });
  }

  showQuantity(quantity: number) {
    return quantity > 0;
  }
}
