import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ProductDetailRouting } from './product-detail.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ProductDetailComponent } from './product-detail.component';


@NgModule({
  declarations: [
    ProductDetailComponent,
  ],
  imports: [
    CommonModule,
    ProductDetailRouting,
    SharedModule,
  ]
})
export class ProductDetailModule { }
