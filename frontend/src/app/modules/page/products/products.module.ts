import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { ProductsRouting } from './products.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ProductsRouting,
    SharedModule,
  ]
})
export class ProductsModule { }
