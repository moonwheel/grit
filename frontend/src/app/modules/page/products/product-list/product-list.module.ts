import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ProductListRouting } from './product-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';

// Components
import { ProductListComponent } from './product-list.component';



@NgModule({
  declarations: [
    ProductListComponent,
  ],
  imports: [
    CommonModule,
    ProductListRouting,
    SharedModule,
    ListViewLayout,
    
  ]
})
export class ProductListModule { }
