import { Component, OnInit, TemplateRef, AfterViewInit, OnDestroy, ElementRef, ViewChildren, ViewChild, QueryList } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { Observable, BehaviorSubject, EMPTY, of } from 'rxjs';
import { takeUntil, debounceTime, switchMap, skip, map } from 'rxjs/operators';

import { ProductState } from 'src/app/store/state/product.state';
import { ProductActions } from 'src/app/store/actions/product.actions';
import { UserState } from 'src/app/store/state/user.state';
import { ShopCategoryState } from 'src/app/store/state/shop-category.state';
import { ShopCategoryModel, ProductModel, ProductDetailsModel } from 'src/app/shared/models';
import { InitShopCategory } from 'src/app/store/actions/shop-category.actions';
import { AbstractInfiniteListComponent } from 'src/app/shared/components/abstract-infinite-list/abstract-infinite-list.component';
import { VisibleState } from 'src/app/store/state/visible.state';
import { NgModel } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstarctOrder } from 'src/app/store/actions/abstract.actions';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: [ './product-list.component.scss' ],
})

export class ProductListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ProductState.products) products$: Observable<ProductModel[]>;
  @Select(ProductState.detailedItems) detailedItems$: Observable<ProductDetailsModel[]>;
  @Select(ProductState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.isBusinessAccount) isBusinessAccount$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;
  @Select(ShopCategoryState.categories) categories$: Observable<ShopCategoryModel[]>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(ProductState.lazyLoading) lazyLoading$: Observable<boolean>;

  showSearch$ = new BehaviorSubject(false);
  tableLayout$ = new BehaviorSubject(false);

  total$ = this.tableLayout$.pipe(
    switchMap(
      isTable => isTable
        ? this.store.select(ProductState.detailedItemsTotal)
        : this.store.select(ProductState.total)
    )
  );

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;

  productMapping = {};

  page = 1;
  pageSize = 15;
  filter = 'date';
  order: AbstarctOrder = 'desc';
  categoryId = -1;

  currentUrl = '';

  searchValue = '';

  navigating = false;

  displayedColumns: string[] = [
    'options',
    'status',
    'photo',
    'title',
    'variant',
    'condition',
    'price',
    'quantity',
    'category',
    'shop_category',
    'delivery_options',
    'delivery_time',
    'shipping_costs',
    'sales',
    'revenue',
    'reviews',
    'rating',
    'created',
  ];

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new InitShopCategory());
    this.store.dispatch(new ProductActions.Init(this.pageSize, this.filter, this.order));
    this.currentUrl = window.location.href;

    this.translateService.stream('PAGES.PRODUCT_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.productMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.loadList();
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.loadList();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.searchValue) {
      this.store.dispatch(new ProductActions.Load(1, this.pageSize, 'date', 'desc'));
    }
  }

  loadList(order: AbstarctOrder = 'desc', categoryId = null) {
    this.order = order;
    if (this.tableLayout$.value) {
      this.store.dispatch(new ProductActions.GetDetailedList(this.page, this.pageSize, this.filter, order, this.searchValue, categoryId, true));
    } else {
      this.store.dispatch(new ProductActions.Load(this.page, this.pageSize, this.filter, order, this.searchValue, categoryId, true));
    }
  }

  // Header

  showHeader() {
    this.showSearch$.next(false);

    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.loadList();
    }
  }

  showSearch() {
    this.showSearch$.next(true);

    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  scroll() {
    console.log('scroll');
  }

  switchLayout() {
    this.tableLayout$.next(!this.tableLayout$.value);
    this.page = 1;
    this.filter = 'date';
    this.order = 'desc';
    this.loadList();
  }

  // Filter

  applyFilter(filterName: string, order: any = 'desc') {
    this.filter = filterName;
    this.page = 1;
    this.loadList(order);
  }

  filterByCategory(categoryId: number) {
    this.categoryId = categoryId;
    this.page = 1;
    if (categoryId > -1) {
      this.loadList('desc', this.categoryId);
    } else {
      this.loadList();
    }
  }

  applyTableFilter(filterName: keyof ProductDetailsModel | 'date') {
    this.page = 1;
    let order: AbstarctOrder = 'desc';

    if (filterName === this.filter) {
      order = this.order === 'desc' ? 'asc' : 'desc';
    }

    this.filter = filterName;
    this.loadList(order);
  }

  // Actions

  inactiveProduct(product: ProductModel) {
    this.store.dispatch(new ProductActions.ChangeActiveState(product));
  }

  deleteDialog(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  deleteProduct(id: number) {
    this.store.dispatch(new ProductActions.Delete(id));
  }

  reportDialog(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  reportProduct(tempRef: TemplateRef<any>): void {
    this.dialog.open(tempRef);
  }

  isPriceNotFree(price: any) {
    return !!price && price !== '0' && price !== '0.00' && price !== '0,00';
  }

  onScroll() {
    this.page++;
    this.loadList();
  }
}
