import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ProductAddRouting } from './product-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ProductAddComponent } from './product-add.component';
import { AddViewLayout } from '../../components/layouts/add-view-layout/add-view-layout.module';


@NgModule({
  declarations: [
    ProductAddComponent,
  ],
  imports: [
    CommonModule,
    ProductAddRouting,
    SharedModule,
    AddViewLayout
  ]
})
export class ProductAddModule { }
