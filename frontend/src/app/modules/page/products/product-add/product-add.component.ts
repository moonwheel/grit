import { Component, OnInit, ViewChild, ElementRef, OnDestroy, TemplateRef } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { ProductState } from 'src/app/store/state/product.state';
import { Observable, of, EMPTY } from 'rxjs';
import { ProductModel } from 'src/app/shared/models/content/products/product.model';
import { UserState } from 'src/app/store/state/user.state';
import { ProductActions } from 'src/app/store/actions/product.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { ProductVariantModel } from 'src/app/shared/models/content/products/product-variant.model';
import { InitShopCategory, EditShopCategory, AddShopCategory, DeleteShopCategory } from 'src/app/store/actions/shop-category.actions';
import { ShopCategoryState } from 'src/app/store/state/shop-category.state';
import { ShopCategoryModel } from 'src/app/shared/models/shop-category.model';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { map, catchError, switchMapTo, switchMap, takeUntil } from 'rxjs/operators';
import { SatPopover } from '@ncstate/sat-popover';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';

import { PRODUCT_CATEGORIES } from '../shared';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';

export enum PRODUCT_VARIANT_ERROR_MESSAGES {
  NOT_ENABLED = 'PAGES.PRODUCT_ADD.FORM.VARIANT_PHOTOS_DIALOG.ERRORS.NOT_ENABLED',
  EMPTY = 'PAGES.PRODUCT_ADD.FORM.VARIANT_PHOTOS_DIALOG.ERRORS.EMPTY',
  NO_PHOTO = 'PAGES.PRODUCT_ADD.FORM.VARIANT_PHOTOS_DIALOG.ERRORS.NO_PHOTO',
}

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss'],
})

export class ProductAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @ViewChild('deliveryOptions', { static: true }) deliveryOptions: MatSelect;
  @ViewChild('deliveryTime', { static: true }) deliveryTime: ElementRef;
  @ViewChild('shippingCosts', { static: true }) shippingCosts: ElementRef;

  @Select(ProductState.drafts) drafts$: Observable<ProductModel[]>;
  @Select(ProductState.loading) loading$: Observable<boolean>;

  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;
  @Select(ShopCategoryState.categories) shopCategories$: Observable<ShopCategoryModel[]>;

  // Chip Input Settings
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  invalidImages = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  //

  form: FormGroup;

  bankForm: FormGroup;

  shopCategoryForm: FormGroup;

  singleProduct = true;
  productVariants = false;
  isEditFlow = false;
  photosLoading = false;

  productId: string;
  product: ProductModel;

  categories = [ ...PRODUCT_CATEGORIES ];

  shopCategories = [];

  variantOptions = [
    'Size',
    'Color',
    'Flavor',
    'Material',
    'Size-Color',
    'Size-Flavor',
    'Size-Material',
    'Size-Color-Material'
  ];

  size = false;

  color = false;

  flavor = false;

  material = false;

  sizes = [];

  colors = [];

  materials = [];

  flavors = [];

  deletedPhotos = [];

  editShopCategoryIndex = -1;

  variants: Array<ProductVariantModel> = [];

  variantsError = '';
  variantPhotosError = '';

  onlyShipping = true;

  onlyCollection = false;

  isFreeShipping = false;

  isDraft = false;

  isInputValueChanged = false;

  selectedVariant: ProductVariantModel;

  productDraftsMapping = {};

  isSubmitted = false;

  trackById = Utils.trackById;

  singleProductMaxPhotoNumber = 6;
  productVariantsMaxPhotoNumber = 20;

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  get photos(): Array<Photo> {
    const photos = this.form.get('photos');
    return photos ? photos.value : [];
  }

  set photos(value: Array<Photo>) {
    const control = this.form.get('photos');
    if (control) {
      control.setValue(value);
    }
  }

  get shopCategoryName(): string {
    const control = this.form.get('shopcategory_');
    const categories = this.store.selectSnapshot(ShopCategoryState.categories);

    if (control && categories.length) {
      const value = control && control.value;
      const found = categories.find(item => +item.id === +value);

      return found ? found.name : '';
    }

    return '';
  }

  constructor(
    protected formBuilder: FormBuilder,
    protected router: Router,
    protected route: ActivatedRoute,
    protected store: Store,
    protected dialog: MatDialog,
    protected navigationService: NavigationService,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));
    this.store.dispatch(new InitShopCategory());
    this.store.dispatch(new ProductActions.LoadDrafts());

    this.form = this.formBuilder.group({
      id: '',
      photos: [[], Validators.required],
      type: ['singleProduct', Validators.required],
      title: ['', Validators.required],
      condition: ['', Validators.required],
      price: ['', Validators.required],
      quantity: ['', Validators.required],
      category: ['', Validators.required],
      shopcategory_: ['', Validators.required],
      description: '',
      variantOptions: '',
      address: '',
      deliveryOptions: ['onlyShipping', Validators.required],
      deliveryTime: ['', Validators.required],
      shippingCosts: ['', Validators.required], // Free Shipping = 0
      onlyCollection: 'false', // true or false
      // payerReturn: ['buyer', Validators.required],
      hashtags: [[], Validators.maxLength(constants.MAX_HASHTAGS_NUMBER)],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
    });

    this.bankForm = this.formBuilder.group({
      holder: ['', Validators.required],
      iban: ['', Validators.required],
    });

    this.shopCategoryForm = this.formBuilder.group({
      id: '',
      name: ['', Validators.required],
    });

    this.setFormData();

    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.productDraftsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  setFormData(): void {
    this.route.queryParams.pipe(
      switchMap(params => {
        this.isEditFlow = 'id' in params;
        this.isDraft = false;

        this.form.reset({
          hashtags: [],
          mentions: [],
        });
        this.form.patchValue({
          type: 'singleProduct',
          deliveryOptions: 'onlyShipping',
          onlyCollection: 'false',
          // payerReturn: 'buyer',
        });
        this.variants = [];
        this.photos = [];
        this.variantOptions = [];

        if (this.isEditFlow) {
          this.setEditViewMode(true);

          const id = +params.id;
          const itemInStore = this.store.selectSnapshot(ProductState.product(id));

          if (!itemInStore || !itemInStore.isDetailed) {
            return this.store.dispatch(new ProductActions.GetOne(id)).pipe(
              switchMapTo(this.store.selectOnce(ProductState.product(id)))
            );
          } else {
            return of(itemInStore);
          }
        } else {
          return EMPTY;
        }
      })
    ).subscribe(product => {
      product = { ...product };

      if (product.type === 'productVariants') {
        this.showProductVariants();

        if (product.variantOptions === 'size') {
          this.showSize();
        } else if (product.variantOptions === 'color') {
          this.showColor();
        } else if (product.variantOptions === 'flavor') {
          this.showFlavor();
        } else if (product.variantOptions === 'material') {
          this.showMaterial();
        } else if (product.variantOptions === 'sizeColor') {
          this.showSizeColor();
        } else if (product.variantOptions === 'sizeFlavor') {
          this.showSizeFlavor();
        } else if (product.variantOptions === 'sizeMaterial') {
          this.showSizeMaterial();
        } else if (product.variantOptions === 'sizeColorMaterial') {
          this.showSizeColorMaterial();
        }

        product.variants = product.variants.map(variant => {
          if (variant.size && this.sizes.findIndex(s => s.name === variant.size) === -1) {
            this.sizes.push({ name: variant.size });
          }
          if (variant.color && this.colors.findIndex(c => c.name === variant.color) === -1) {
            this.colors.push({ name: variant.color });
          }
          if (variant.flavor && this.flavors.findIndex(f => f.name === variant.flavor) === -1) {
            this.flavors.push({ name: variant.flavor });
          }
          if (variant.material && this.materials.findIndex(m => m.name === variant.material) === -1) {
            this.materials.push({ name: variant.material });
          }

          return {
            ...variant,
            price: variant.price === null ? null : Number(variant.price),
            photos: variant.photos.map((item) => {
              const found = product.photos.findIndex(photo => +photo.id === +item.id);
              return found;
            }).filter(item => item !== -1),
          };
        });
      } else {
        product.price = product.price === null ? null : Number(product.price);
        this.showSingleProduct();
      }

      product.shopcategory_ = product.shopcategory_ && Number(product.shopcategory_.id);

      this.product = product;
      this.variants = product.variants;
      this.isDraft = product.draft;

      if (product.shippingCosts === '0' as any) {
        this.isFreeShipping = true;
        this.freeShippingChanged();
      }

      this.form.patchValue(product);

      if (!product.deliveryOptions) {
        return;
      }

      if (product.deliveryOptions === 'onlyShipping') {
        this.showOnlyShipping();
        this.form.patchValue({ address: null });
      } else {
        this.form.patchValue({ address: +product.addressId });
        if (product.deliveryOptions === 'onlyCollection') {
          this.showOnlyCollection();
        } else {
          this.showShippingCollection();
        }
      }

      this.setEditReadyState(true);
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  async selectPhotos(event: Event): Promise<void> {
    const target = event.target as HTMLInputElement;
    const files = target.files;

    if (files && files.length) {
      this.photosLoading = true;

      const promises = Array.from(files).map(async (srcFile) => {
        const { url, file } = await Utils.getImageData(srcFile);
        const photosNumber = this.photos.length;

        if (
          (photosNumber < this.singleProductMaxPhotoNumber) && this.singleProduct ||
          !this.singleProduct && (photosNumber < this.productVariantsMaxPhotoNumber)
        ) {
          this.photos = [
            ...this.photos,
            {
              id: null,
              photoFile: file,
              photoUrl: url,
            }
          ];
          this.isInputValueChanged = true;
        }

        return { url, file };
      });

      await Promise.all(promises);

      this.photosLoading = false;

      target.value = null;
    }
  }

  showSingleProduct() {
    this.singleProduct = true;
    this.productVariants = false;
    if (this.photos.length > this.singleProductMaxPhotoNumber) {
      this.photos.length = this.singleProductMaxPhotoNumber;
    }
    this.setFormControlValidator('variantOptions', null);

    this.setFormControlValidator('price', [Validators.required]);
    this.setFormControlValidator('quantity', [Validators.required]);
  }

  showProductVariants() {
    this.singleProduct = false;
    this.productVariants = true;
    this.setFormControlValidator('variantOptions', [Validators.required]);
    this.setFormControlValidator('price', null);
    this.setFormControlValidator('quantity', null);
  }

  // Variant Options
  showSize() {
    this.size = true;
    this.color = false;
    this.flavor = false;
    this.material = false;
    this.addVariants();
  }

  showColor() {
    this.size = false;
    this.color = true;
    this.flavor = false;
    this.material = false;
    this.addVariants();
  }

  showFlavor() {
    this.size = false;
    this.color = false;
    this.flavor = true;
    this.material = false;
    this.addVariants();
  }

  showMaterial() {
    this.size = false;
    this.color = false;
    this.flavor = false;
    this.material = true;
    this.addVariants();
  }

  showSizeColor() {
    this.size = true;
    this.color = true;
    this.flavor = false;
    this.material = false;
    this.addVariants();
  }

  showSizeFlavor() {
    this.size = true;
    this.color = false;
    this.flavor = true;
    this.material = false;
    this.addVariants();
  }

  showSizeMaterial() {
    this.size = true;
    this.color = false;
    this.flavor = false;
    this.material = true;
    this.addVariants();
  }

  showSizeColorMaterial() {
    this.size = true;
    this.color = true;
    this.flavor = false;
    this.material = true;
    this.addVariants();
  }

  // Size Option
  addSize(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const index = this.sizes.findIndex(size => size.name === value.trim());
      if (index === -1) {
        this.sizes.push({ name: value.trim() });
      }
    }

    if (input) {
      input.value = '';
    }

    this.addVariants();
  }

  removeSize(size: any): void {
    const index = this.sizes.indexOf(size);

    if (index >= 0) {
      this.sizes.splice(index, 1);
    }

    this.addVariants();
  }

  //

  // Color Option
  addColor(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const index = this.colors.findIndex(color => color.name === value.trim());
      if (index === -1) {
        this.colors.push({ name: value.trim() });
      }
    }

    if (input) {
      input.value = '';
    }

    this.addVariants();
  }

  removeColor(color: any) {
    const index = this.colors.indexOf(color);

    if (index >= 0) {
      this.colors.splice(index, 1);
    }

    this.addVariants();
  }

  //

  // Material Option
  addMaterial(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const index = this.materials.findIndex(material => material.name === value.trim());
      if (index === -1) {
        this.materials.push({ name: value.trim() });
      }
    }

    if (input) {
      input.value = '';
    }

    this.addVariants();
  }

  removeMaterial(material: any) {
    const index = this.materials.indexOf(material);

    if (index >= 0) {
      this.materials.splice(index, 1);
    }

    this.addVariants();
  }

  //

  // Flavor Option
  addFlavor(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const index = this.flavors.findIndex(flavor => flavor.name === value.trim());
      if (index === -1) {
        this.flavors.push({ name: value.trim() });
      }
    }

    if (input) {
      input.value = '';
    }

    this.addVariants();
  }

  removeFlavor(flavor: any): void {
    const index = this.flavors.indexOf(flavor);

    if (index >= 0) {
      this.flavors.splice(index, 1);
    }

    this.addVariants();
  }

  //

  addVariants() {
    const options = this.getActiveVariantOptions();
    const newVariants = [];
    let maxId = -1;

    this.variants.forEach(v => {
      if (maxId < v.id) {
        maxId = v.id;
      }
    });

    let index = maxId + 1;

    if (options.length === 1) {
      this[options[0]].forEach((item) => {
        const variant: ProductVariantModel = this.getEmptyVariant(index++);
        variant[options[0].slice(0, -1)] = item.name;
        newVariants.push(variant);
      });
    } else if (options.length === 2) {
      this[options[0]].forEach((firstItem) => {
        this[options[1]].forEach((secondItem) => {
          const variant: ProductVariantModel = this.getEmptyVariant(index++);
          variant[options[0].slice(0, -1)] = firstItem.name;
          variant[options[1].slice(0, -1)] = secondItem.name;
          newVariants.push(variant);
        });
      });
    } else if (options.length === 3) {
      this[options[0]].forEach((firstItem) => {
        this[options[1]].forEach((secondItem) => {
          this[options[2]].forEach((thirdItem) => {
            const variant: ProductVariantModel = this.getEmptyVariant(index++);
            variant[options[0].slice(0, -1)] = firstItem.name;
            variant[options[1].slice(0, -1)] = secondItem.name;
            variant[options[2].slice(0, -1)] = thirdItem.name;
            newVariants.push(variant);
          });
        });
      });
    }

    this.variants = this.variants.filter(variant => {
      const newVariantIndex = this.findSameVariantIndex(newVariants, variant);
      return newVariantIndex !== -1;
    });

    newVariants.forEach(newVariant => {
      const variantIndex = this.findSameVariantIndex(this.variants, newVariant);
      if (variantIndex === -1) {
        this.variants.push(newVariant);
      }
    });
  }

  findSameVariantIndex(variants, variant) {
    return variants.findIndex((v: ProductVariantModel) => {
      const sameSize = v.size === variant.size;
      const sameFlavor = v.flavor === variant.flavor;
      const sameColor = v.color === variant.color;
      const sameMaterial = v.material === variant.material;
      return sameSize && sameFlavor && sameColor && sameMaterial;
    });
  }

  showOnlyShipping() {
    this.onlyShipping = true;
    this.onlyCollection = false;
    this.setOnlyShippingValidators(true);
    this.setOnlyCollectionValidators(false);
  }

  setOnlyShippingValidators(isRequired) {
    const validator = isRequired ? [Validators.required] : null;
    this.setFormControlValidator('deliveryTime', validator);
    this.setFormControlValidator('shippingCosts', this.isFreeShipping ? null : validator);
    // this.setFormControlValidator('payerReturn', validator);
  }

  showOnlyCollection() {
    this.onlyShipping = false;
    this.onlyCollection = true;
    this.setOnlyShippingValidators(false);
    this.setOnlyCollectionValidators(true);
  }

  setOnlyCollectionValidators(isRequired) {
    const validator = isRequired ? [Validators.required] : null;
    this.setFormControlValidator('address', validator);
  }

  showShippingCollection() {
    this.onlyShipping = true;
    this.onlyCollection = true;
    this.setOnlyShippingValidators(true);
    this.setOnlyCollectionValidators(true);
  }

  freeShippingChanged() {
    this.setFormControlValidator('shippingCosts', this.isFreeShipping ? null : [Validators.required]);
  }

  setFormControlValidator(name, validator) {
    this.form.controls[name].setValidators(validator);
    this.form.controls[name].updateValueAndValidity();
  }

  save(isDraft = false) {
    this.isSubmitted = true;
    this.form.markAsTouched();
    const variantAreValid = this.checkVariantsValidity();

    if (this.form.valid && variantAreValid || isDraft) {
      const formValue = this.form.value;

      formValue.photos = formValue.photos.map(item => ({
        id: item.id,
      }));

      formValue.description = formValue.description && formValue.description.trim();
      formValue.draft = isDraft;

      if (this.singleProduct) {
        formValue.price = formValue.price === '' ? null : formValue.price;

        delete formValue.variants;
      } else {
        delete formValue.price;
        delete formValue.quantity;

        formValue.variants = this.variants.map(item => ({
          ...item,
          price: item.price === '' ? null : item.price,
          quantity: item.quantity === '' ? null : item.quantity,
        }));
      }

      if (!formValue.address || formValue.deliveryOptions === 'onlyShipping') {
        delete formValue.address;
      }

      if (this.isFreeShipping) {
        formValue.shippingCosts = 0;
      }

      const photoFiles = this.photos
        .map((item, index) => ({ file: item.photoFile, index }))
        .filter(item => Boolean(item.file));

      const dataToUpload = photoFiles.length ? photoFiles : null;

      let observable: Observable<any>;

      if (this.isEditFlow) {
        formValue.deletedPhotos = this.deletedPhotos;
        observable = this.store.dispatch(new ProductActions.Edit(formValue, dataToUpload));
      } else {
        observable = this.store.dispatch(new ProductActions.Add(formValue, dataToUpload));
      }

      return observable.pipe(
        map(() => this.canImmediatelyDeactivate = true),
        catchError(() => of(false)),
      );
    } else {
      this.invalidImages = true;
      Utils.touchForm(this.form);

      return of(false);
    }
  }

  checkVariantsValidity(): boolean {
    if (!this.singleProduct) {
      const hasEnabled = !!this.variants.find(item => item.enable);
      if (hasEnabled) {
        this.variantsError = '';
      } else {
        this.variantsError = PRODUCT_VARIANT_ERROR_MESSAGES.NOT_ENABLED;
        Utils.touchForm(this.form);
        return false;
      }

      const hasEmpty = !!this.variants.find(item =>
        item.enable &&
        (
          item.price === null ||
          item.quantity === null ||
          item.quantity < 1 ||
          !item.photos.length
        )
      );
      if (!hasEmpty || this.form.untouched) {
        this.variantsError = '';
      } else {
        this.variantsError = PRODUCT_VARIANT_ERROR_MESSAGES.EMPTY;
        return false;
      }

      return hasEnabled && !hasEmpty;
    } else {
      return true;
    }
  }

  deleteDraft(product: ProductModel): void {
    this.store.dispatch(new ProductActions.Delete(+product.id));
    this.dialog.closeAll();
    if (+product.id === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }

  getActiveVariantOptions(): Array<string> {
    const options = [];

    if (this.size && this.sizes.length > 0) {
      options.push('sizes');
    }

    if (this.color && this.colors.length > 0) {
      options.push('colors');
    }

    if (this.material && this.materials.length > 0) {
      options.push('materials');
    }

    if (this.flavor && this.flavors.length > 0) {
      options.push('flavors');
    }

    return options;
  }

  openShopCategory(categoryModal, event: Event) {
    event.stopPropagation();
    this.editShopCategoryIndex = -1;
    this.shopCategoryForm.reset();
    this.dialog.open(categoryModal);
  }

  variantPhotosDialog(tempRef, variant): void {
    this.dialog.open(tempRef);
    this.selectedVariant = JSON.parse(JSON.stringify(variant));
  }

  selectVariantPhoto(index: number) {
    if (this.selectedVariant) {
      const selectedPhotoIndex = this.selectedVariant.photos.findIndex(p => p === index);
      if (selectedPhotoIndex !== -1) {
        this.selectedVariant.photos.splice(selectedPhotoIndex, 1);
      } else {
        this.selectedVariant.photos.push(index);
      }

      if (this.selectedVariant.photos.length) {
        this.variantPhotosError = '';
      } else {
        this.variantPhotosError = PRODUCT_VARIANT_ERROR_MESSAGES.NO_PHOTO;
      }
    }
  }

  saveVariantPhotos() {
    if (this.selectedVariant && this.selectedVariant.photos.length) {
      this.variantPhotosError = '';
      const variantIndex = this.variants.findIndex(v => v.id === this.selectedVariant.id);
      if (variantIndex !== -1) {
        this.variants[variantIndex].photos = this.selectedVariant.photos;
        this.selectedVariant = null;
      }
      this.checkVariantsValidity();
      this.dialog.closeAll();
    } else {
      this.variantPhotosError = PRODUCT_VARIANT_ERROR_MESSAGES.NO_PHOTO;
    }
  }

  closeVariantPhotosDialog() {
    this.dialog.closeAll();
    this.variantPhotosError = '';
  }

  isVariantPhotoSelected(index: number) {
    return !!(this.selectedVariant && this.selectedVariant.photos.findIndex(p => p === index) !== -1);
  }

  getSelectedVariantPhotoIndex(index: number) {
    return this.selectedVariant ? this.selectedVariant.photos.findIndex(p => p === index) : -1;
  }

  reorderImages(event: CdkDragDrop<string[]>): void {
    const photos = [ ...this.photos ];
    moveItemInArray(photos, event.previousIndex, event.currentIndex);
    this.photos = photos;
  }

  removePhotoByIndex(index: number): void {
    if (this.photos[index]) {
      this.deletedPhotos.push(this.photos[index]);
      this.photos = this.photos.filter((photo, idx) => index !== idx);

      const variants = [ ...this.variants ];

      if (this.selectedVariant) {
        variants.push(this.selectedVariant);
      }

      for (const variant of variants) {
        variant.photos = variant.photos
          .filter(idx => index !== idx)
          .map(idx => idx > index ? idx - 1 : idx);
      }
    }
  }

  addShopCategory(form) {
    const formValue = form.value;

    if (this.editShopCategoryIndex >= 0) {
      this.store.dispatch(new EditShopCategory(formValue, this.editShopCategoryIndex));
    } else {
      this.store.dispatch(new AddShopCategory(formValue)).subscribe(() => {
        const categories = this.store.selectSnapshot(ShopCategoryState.categories);

        const newCategrory = categories.reduce((prev, curr) => {
          const prevCreated = new Date(prev.created).getTime();
          const currCreated = new Date(curr.created).getTime();
          return prevCreated < currCreated ? curr : prev;
        }, categories[0]);

        this.form.get('shopcategory_').setValue(newCategrory.id);
      });
    }
  }

  moreShopCategory(categoryMore, event: Event) {
    categoryMore.toggle();
    event.stopPropagation();
  }

  deleteShopCategory(id: number, modal: SatPopover) {
    this.store.dispatch(new DeleteShopCategory(id));
    const control = this.form.get('shopcategory_');
    if (+control.value === +id) {
      control.setValue(null);
    }
    modal.close();
  }

  editShopCategory(category, index: number, form: FormGroup, dropDown, modal) {
    this.editShopCategoryIndex = index;
    form.patchValue(category);

    dropDown.close();
    this.dialog.open(modal);
  }

  getProductPhotoUrl(photo: Photo) {
    return (photo.photoFile ? photo.photoUrl : `${photo.photoUrl}`) || constants.PLACEHOLDER_NO_PHOTO_PATH;
  }

  getProductPhotoCssUrl(photo: Photo) {
    return `url(${this.getProductPhotoUrl(photo)})`;
  }

  getProductDraftPhoto(product: ProductModel) {
    return product.photos && product.photos.length && product.photos[0].photoUrl ?
      `${product.photos[0].photoUrl}` :
      'assets/images/photo-placeholder.png';
  }

  wasChanged() {
    return (
      this.isInputValueChanged
      || !this.form.pristine
      || !this.bankForm.pristine
      || !this.shopCategoryForm.pristine
    );
  }

  private getEmptyVariant(index: number): ProductVariantModel {
    return {
      id: index,
      photos: [],
      size: null,
      color: null,
      flavor: null,
      material: null,
      price: null,
      quantity: null,
      enable: true,
      photosOrder: null,
    };
  }
}

export interface Photo {
  id: string;
  photoUrl: any;
  photoFile: File;
}
