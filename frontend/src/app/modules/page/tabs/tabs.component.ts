import {
  Component,
  OnInit,
  ViewChildren,
  QueryList,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { RouterLinkActive } from '@angular/router';
import { Select } from '@ngxs/store';
import { from, Observable, Subject } from 'rxjs';
import { map, filter, startWith, takeUntil, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { VisibleState, VisibleStateModel } from '../../../store/state/visible.state';
import { UserState } from '../../../store/state/user.state';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})

export class TabsComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(VisibleState.visible)
  visibleBlocks$: Observable<VisibleStateModel>;

  @Select(UserState.isBlocking)
  isBlocking$: Observable<boolean>;

  @Select(UserState.isBlocked)
  isBlocked$: Observable<boolean>;

  @Select(UserState.isMyAccount)
  isMyAccount$: Observable<boolean>;

  @Select(UserState.isFolowing)
  isFolowing$: Observable<boolean>;

  @Select(UserState.isPrivate)
  isPrivate$: Observable<boolean>;

  @ViewChildren(RouterLinkActive) links: QueryList<RouterLinkActive>;

  @ViewChild('tabs') tabsElem: ElementRef<HTMLDivElement>;

  isVisibleUserTabs$ = this.visibleBlocks$.pipe(
    filter(dataVisible => Boolean(dataVisible)),
    map(dataVisible => dataVisible.isVisibleUserTabs),
    startWith(true),
    distinctUntilChanged(),
    // This added to avoid 'Expression was changed' error
    switchMap(value => from(Promise.resolve(value))),
  );

  private destroy$ = new Subject();

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.scrollToSelectedTab();
    this.links.changes.pipe(
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.scrollToSelectedTab();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private scrollToSelectedTab() {
    setTimeout(() => {
      this.links.forEach((routerLink, index) => {
        if (routerLink.isActive && this.tabsElem) {
          const tabs = this.tabsElem.nativeElement;
          const tab = tabs.children[index] as HTMLElement;
          const paddingLeft = parseFloat(getComputedStyle(tabs).paddingLeft);
          const offset = tab.offsetLeft - paddingLeft;
          const widthWithTab = offset + tab.offsetWidth + paddingLeft * 2;
          const allTabsWidth = tabs.offsetWidth;

          if (widthWithTab > allTabsWidth) {
            this.tabsElem.nativeElement.scrollTo(offset, 0);
          }
        }
      });
    });
  }
}
