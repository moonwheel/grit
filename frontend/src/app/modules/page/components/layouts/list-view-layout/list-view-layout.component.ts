import { Component, Input } from "@angular/core";

@Component({
    templateUrl: './list-view-layout.component.html',
    styleUrls: ['./list-view-layout.component.scss'],
    selector: 'list-view-layout',
})
export class ListViewLayoutComponent {
    @Input('loading') loading: boolean = false;
    @Input('expand') expand: boolean = false;
    @Input('disableBorders') disableBorders = false;
    @Input('isListEmpty') isListEmpty: boolean = false;
    @Input('emptyTextPlaceholder') emptyTextPlaceholder: string = '';
}