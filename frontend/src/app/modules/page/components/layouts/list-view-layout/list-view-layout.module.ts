import { NgModule } from "@angular/core";
import { ListViewLayoutComponent } from "./list-view-layout.component";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../../../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
    ],
    declarations: [ListViewLayoutComponent],
    exports: [ListViewLayoutComponent]
})
export class ListViewLayout {}