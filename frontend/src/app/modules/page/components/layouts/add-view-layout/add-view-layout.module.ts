import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddViewLayoutComponent } from "./add-view-layout.component";
import { SharedModule } from "../../../../../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
    ],
    declarations: [AddViewLayoutComponent],
    exports: [AddViewLayoutComponent]
})
export class AddViewLayout { }