import { Component, Input, EventEmitter, Output } from "@angular/core";
import { NavigationService } from "../../../../../shared/services/navigation.service";

@Component({ 
    styleUrls: ['./add-view-layout.component.scss'], 
    selector: 'add-view-layout', 
    templateUrl: './add-view-layout.template.html' 
})
export class AddViewLayoutComponent {
    @Input('formId') formId: string;
    @Input('disabled') disabled: boolean;
    @Input('accountPhotoSrc') src: string;
    @Input('loading') loading: boolean;
    @Output('onSubmitClick') onSubmitClick = new EventEmitter();

    constructor(private navigationSerivce: NavigationService) {
    }

    back() {
        this.navigationSerivce.back();
    }

}