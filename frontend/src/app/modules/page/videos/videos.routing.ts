import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./video-list/video-list.module').then(m => m.VideoListModule) },
  { path: 'add', loadChildren: () => import('./video-add/video-add.module').then(m => m.VideoAddModule) },
  { path: ':id', loadChildren: () => import('./video-detail/video-detail.module').then(m => m.VideoDetailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideosRouting {
}
