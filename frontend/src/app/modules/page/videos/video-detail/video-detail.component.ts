import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VideoState } from 'src/app/store/state/video.state';
import { Store, Select } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { VideoActions } from 'src/app/store/actions/video.actions';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { VideoModel } from '../../../../shared/models/video.model';
import { MatDialog } from '@angular/material/dialog';
import { switchMap, mapTo, switchMapTo, tap } from 'rxjs/operators';
import { AbstractDetailPageComponent } from 'src/app/shared/components';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: [ './video-detail.component.scss' ]
})

export class VideoDetailComponent extends AbstractDetailPageComponent implements OnInit, OnDestroy {

  @Select(VideoState.loading) loading$: Observable<boolean>;

  video$: Observable<VideoModel>;

  constructor(protected store: Store,
              protected route: ActivatedRoute,
              protected router: Router,
              protected dialog: MatDialog,
  ) {
    super(dialog, store);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));

    this.video$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(VideoState.video(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new VideoActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(VideoState.video(id))),
            tap(video => {
              if (video && !video.viewed) {
                this.store.dispatch(new VideoActions.View(id));
              }
            }),
          );
        } else {
          return this.store.select(VideoState.video(id));
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  onDelete() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
