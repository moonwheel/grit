import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { VideoDetailComponent } from './video-detail.component';
import { UploadingGuard, UploadingGuardStateToken } from 'src/app/core/guards';
import { VideoState } from 'src/app/store/state/video.state';

const routes: Routes = [
  {
    path: '',
    component: VideoDetailComponent,
    canActivate: [ UploadingGuard ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UploadingGuard,
    { provide: UploadingGuardStateToken, useValue: VideoState },
  ]
})
export class VideoDetailRouting {}
