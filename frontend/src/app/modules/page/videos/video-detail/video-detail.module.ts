import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { VideoDetailRouting } from './video-detail.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { VideoDetailComponent } from './video-detail.component';


@NgModule({
  declarations: [
    VideoDetailComponent,
  ],
  imports: [
    CommonModule,
    VideoDetailRouting,
    SharedModule,
  ]
})
export class VideoDetailModule { }
