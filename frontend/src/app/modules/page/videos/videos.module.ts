import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { VideosRouting } from './videos.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    VideosRouting,
    SharedModule,
  ]
})
export class VideosModule { }
