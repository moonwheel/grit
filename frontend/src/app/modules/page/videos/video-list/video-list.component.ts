import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { VideoState } from 'src/app/store/state/video.state';
import { Store, Select } from '@ngxs/store';
import { VideoActions } from 'src/app/store/actions/video.actions';
import { Observable } from 'rxjs';
import { VideoModel } from 'src/app/shared/models/video.model';
import { UserState } from '../../../../store/state/user.state';
import { MatDialog } from '@angular/material/dialog';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { VisibleState } from 'src/app/store/state/visible.state';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})

export class VideoListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(VideoState.videos) videos$: Observable<VideoModel[]>;
  @Select(VideoState.total) total$: Observable<number>;
  @Select(VideoState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(VideoState.lazyLoading) lazyLoading$: Observable<boolean>;

  page = 1;
  pageSize = 15;
  itemSize = 120;
  filter = 'date';
  searchValue = '';

  videoMapping = {};

  currentUrl = '';
  navigating = false;

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new VideoActions.Init(this.pageSize));
    this.currentUrl = window.location.href;

    this.translateService.stream('PAGES.VIDEO_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.videoMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new VideoActions.Load(this.page, this.pageSize, this.filter, 'asc', '', true));
    });
  }

  dateFilter(filter) {
    this.store.dispatch(new VideoActions.Init(15, 'date', 'asc'));
    filter.close();
  }

  viewFilter(filter) {
    this.store.dispatch(new VideoActions.Init(15, 'views', 'asc'));
    filter.close();
  }

  likeFilter(filter) {
    this.store.dispatch(new VideoActions.Init(15, 'likes', 'asc'));
    filter.close();
  }

  generateCover(video): string {
    return video && video.cover;
  }

  // Actions

  deleteDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteVideo(id) {
    this.store.dispatch(new VideoActions.Delete(id));
  }

  reportVideo(tempRef): void {
    this.dialog.open(tempRef);
  }

}
