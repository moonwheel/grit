import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { VideoListRouting } from './video-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { VideoListComponent } from './video-list.component';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';


@NgModule({
  declarations: [
    VideoListComponent,
  ],
  imports: [
    CommonModule,
    VideoListRouting,
    SharedModule,
    ListViewLayout,
  ]
})
export class VideoListModule { }
