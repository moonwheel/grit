import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { VideoAddComponent } from './video-add.component';
// Guards
import { ProfileGuard, CanDeactivateGuard, ViewerGuard } from 'src/app/core/guards';
// Resolvers
import { VideoFileResolver } from 'src/app/core/resolvers';

const routes: Routes = [
  {
    path: '',
    component: VideoAddComponent,
    canActivate: [ProfileGuard, ViewerGuard],
    canDeactivate: [CanDeactivateGuard],
    resolve: { file: VideoFileResolver },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoAddRouting {}
