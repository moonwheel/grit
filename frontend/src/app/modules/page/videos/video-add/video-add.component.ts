import { Component, OnDestroy, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { VideoActions } from 'src/app/store/actions/video.actions';
import { Store, Select } from '@ngxs/store';
import { Utils } from 'src/app/shared/utils/utils';
import { VideoState } from 'src/app/store/state/video.state';
import { UserState } from 'src/app/store/state/user.state';
import { Observable, of, EMPTY } from 'rxjs';
import { VideoModel } from 'src/app/shared/models/video.model';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { map, catchError, switchMap, mapTo, takeUntil, switchMapTo } from 'rxjs/operators';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-video-add',
  templateUrl: './video-add.component.html',
  styleUrls: ['./video-add.component.scss']
})

export class VideoAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select(VideoState.videos) videos$: Observable<VideoModel[]>;
  @Select(VideoState.drafts) drafts$: Observable<VideoModel[]>;
  @Select(VideoState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;

  form: FormGroup;

  isSubmitted = false;
  isDraft = false;
  isInputValueChanged = false;
  invalidVideo = false;
  coverLoading = false;
  localVideoLoading = false;

  coverData: string;
  coverFile: File;
  oldCoverData: string;

  videoData: SafeUrl;
  videoFile: File;

  categories = [ ...constants.MEDIA_CATEGORIES ];

  videoDraftsMapping = {};

  isEditFlow = false;

  trackById = Utils.trackById;

  constructor(
    protected navigationService: NavigationService,
    protected formBuilder: FormBuilder,
    protected router: Router,
    protected route: ActivatedRoute,
    protected dialog: MatDialog,
    protected store: Store,
    protected zone: NgZone,
    protected sanitizer: DomSanitizer,
    protected snakcbar: MatSnackBar,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));
    this.store.dispatch(new VideoActions.LoadDrafts());

    this.route.data.subscribe(({ file }) => {
      if (file) {
        this.readVideoFile(file);
      }
    });

    this.form = this.formBuilder.group({
      id: '',
      video: [null, Validators.required],
      title: ['', Validators.required],
      category: ['', Validators.required],
      description: '',
      location: '',
      business_address: null,
      created: new Date(),
      hashtags: [[], Validators.maxLength(constants.MAX_HASHTAGS_NUMBER)],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
    });

    this.setFormData();

    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.videoDraftsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  setFormData(): void {
    this.route.queryParams.pipe(
      switchMap(params => {
        this.isEditFlow = 'id' in params;

        this.form.reset({
          hashtags: [],
          mentions: [],
        });
        this.coverData = null;
        this.videoData = null;
        this.videoFile = null;

        if (this.isEditFlow) {
          this.setEditViewMode(true);

          const id = +params.id;
          const itemInStore = this.store.selectSnapshot(VideoState.video(id));

          if (!itemInStore || !itemInStore.isDetailed) {
            return this.store.dispatch(new VideoActions.GetOne(id)).pipe(
              switchMapTo(this.store.selectOnce(VideoState.video(id)))
            );
          } else {
            return of(itemInStore);
          }
        } else {
          return EMPTY;
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(video => {
      this.coverData = video.cover;
      this.oldCoverData = video.cover;
      this.isDraft = video.draft;
      this.videoData = video.video;
      this.form.patchValue(video);
      this.form.get('business_address').patchValue(video.entity_business_address);

      this.setEditReadyState(true);
    });
  }

  save(isDraft = false) {
    if (this.form.valid || isDraft) {
      this.isSubmitted = true;

      const videoInfo = this.isEditFlow ? this.form.value : this.form.getRawValue();
      const videoAndCoverData = [];

      if (this.coverFile) {
        videoAndCoverData.push({ file: this.coverFile, index: videoAndCoverData.length });
      }

      if (this.videoFile) {
        videoAndCoverData.push({ file: this.videoFile, index: videoAndCoverData.length });
      }

      if (this.coverData === this.oldCoverData) {
        videoInfo.cover = this.coverData;
      }

      videoInfo.draft = isDraft;
      videoInfo.description = videoInfo.description && videoInfo.description.trim();

      let observable: Observable<any>;

      if (this.isEditFlow) {
        observable = this.store.dispatch(new VideoActions.Edit(videoInfo, videoAndCoverData));
      } else {
        observable = this.store.dispatch(new VideoActions.Add(videoInfo, videoAndCoverData));
      }

      return observable.pipe(
        map(() => this.canImmediatelyDeactivate = true),
        catchError(() => of(false)),
      );
    } else {
      this.invalidVideo = true;
      Utils.touchForm(this.form);

      return of(false);
    }
  }

  selectVideo(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files[0]) {
      this.readVideoFile(target.files[0]);
      target.value = null;
    }
  }

  removeVideo(event: Event) {
    event.stopPropagation();
    this.videoData = null;
    this.videoFile = null;
    this.isInputValueChanged = true;
    this.form.get('video').setValue(null);
  }

  selectCover(event: Event) {
    const target = event.target as HTMLInputElement;
    const srcFile = target.files && target.files[0];
    if (srcFile) {
      this.coverLoading = true;
      Utils.getImageData(srcFile).then(({ url, file }) => {
        this.coverData = url;
        this.coverFile = file;
        this.isInputValueChanged = true;
        this.coverLoading = false;
      });
      target.value = null;
    }
  }

  removeCover() {
    this.coverData = null;
    this.isInputValueChanged = true;
  }

  wasChanged() {
    return (
      !this.form.pristine
      || this.isInputValueChanged
    );
  }

  addLocation() {

  }

  deleteDraft(video: VideoModel) {
    this.store.dispatch(new VideoActions.Delete(video.id));
    this.dialog.closeAll();
    if (+video.id === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }

  private readVideoFile(file: File) {
    const maxSize = constants.MAX_VIDEO_SIZE_MB * Math.pow(2, 20);
    if (file.size > maxSize) {
      const num = constants.MAX_VIDEO_SIZE_MB;
      this.translateService.get('ERRORS.MAX_VIDEO_SIZE', { num }).subscribe(message => {
        this.snakcbar.open(message);
      });
      return;
    }
    this.isInputValueChanged = true;
    this.videoFile = file;
    this.form.get('video').setValue(this.videoFile);

    const url = URL.createObjectURL(file);

    this.videoData = this.sanitizer.bypassSecurityTrustUrl(url);

  }
}
