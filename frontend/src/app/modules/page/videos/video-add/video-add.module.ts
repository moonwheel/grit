import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { VideoAddRouting } from './video-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { VideoAddComponent } from './video-add.component';
import { AddViewLayout } from '../../components/layouts/add-view-layout/add-view-layout.module';


@NgModule({
  declarations: [
    VideoAddComponent,
  ],
  imports: [
    CommonModule,
    VideoAddRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class VideoAddModule { }
