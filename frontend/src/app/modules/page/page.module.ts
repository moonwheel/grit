import { NgModule } from '@angular/core';

// Modules
import { PageRouting } from './page.routing';
import { SharedModule } from '../../shared/shared.module';

// Components
import { TabsComponent } from './tabs/tabs.component';

@NgModule({
  declarations: [
    TabsComponent,
  ],
  exports: [],
  imports: [
    PageRouting,
    SharedModule,
  ]
})
export class PageModule { }
