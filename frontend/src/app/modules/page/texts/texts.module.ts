import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

// Modules
import { TextsRouting } from './texts.routing';

// Components
import { TextAddComponent } from './text-add/text-add.component';
import { AddViewLayout } from '../components/layouts/add-view-layout/add-view-layout.module';

@NgModule({
  declarations: [
    TextAddComponent,
  ],
  imports: [
    CommonModule,
    TextsRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class TextsModule {
}
