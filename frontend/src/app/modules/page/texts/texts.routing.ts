import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { TextAddComponent } from './text-add/text-add.component';
import { ProfileGuard, CanDeactivateGuard, ViewerGuard } from 'src/app/core/guards';

const routes: Routes = [
  // { path: '', loadChildren: () => import('./text-list/text-list.module').then(m => m.TextListModule) }, 
  { path: 'add', component: TextAddComponent, canActivate: [ProfileGuard, ViewerGuard], canDeactivate: [CanDeactivateGuard] },
  { path: ':id', loadChildren: () => import('./text-detail/text-detail.module').then(m => m.TextDetailModule) },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TextsRouting {
}
