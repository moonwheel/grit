import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlainTextAddComponent } from './plain-text-add.component';

describe('PlainTextComponent', () => {
  let component: PlainTextAddComponent;
  let fixture: ComponentFixture<PlainTextAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlainTextAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlainTextAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
