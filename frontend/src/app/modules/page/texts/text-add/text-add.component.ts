import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { take, filter, catchError, map, switchMap, mapTo, takeUntil, switchMapTo } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of, EMPTY } from 'rxjs';
import { TextActions } from '../../../../store/actions/text.actions';
import { Utils } from '../../../../shared/utils/utils';
import { UserState } from '../../../../store/state/user.state';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { TextModel } from '../../../../shared/models/text.model';
import { TextState } from '../../../../store/state/text.state';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-text',
  templateUrl: './text-add.component.html',
  styleUrls: ['./text-add.component.scss']
})
export class TextAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;
  @Select(TextState.drafts) drafts$: Observable<TextModel[]>;
  @Select(TextState.loading) loading$: Observable<boolean>;

  plainText$: Observable<TextModel>;

  form: FormGroup;

  isEditFlow = false;
  isDraft = false;

  textDraftsMapping = {};

  constructor(
    protected navigationService: NavigationService,
    protected formBuilder: FormBuilder,
    protected store: Store,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialog: MatDialog,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));
    this.store.dispatch(new TextActions.LoadDrafts());
    this.form = this.formBuilder.group({
      id: '',
      text: ['', Validators.required],
      created: new Date(),
      hashtags: [[], Validators.maxLength(constants.MAX_HASHTAGS_NUMBER)],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
    });
    this.setFormData();
    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.textDraftsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  private setFormData(): void {
    this.route.queryParams.pipe(
      switchMap(params => {
        this.isEditFlow = 'id' in params;

        this.form.reset({
          hashtags: [],
          mentions: [],
        });
        this.isDraft = false;

        if (this.isEditFlow) {
          const id = +params.id;
          const itemInStore = this.store.selectSnapshot(TextState.text(id));

          if (!itemInStore) {
            return this.store.dispatch(new TextActions.Init()).pipe(
              switchMapTo(this.store.selectOnce(TextState.text(id)))
            );
          } else {
            return of(itemInStore);
          }
        } else {
          return EMPTY;
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(text => {
      this.isDraft = text.draft;
      this.form.patchValue(text);
    });
  }

  transformData(formData: TextModel): object {
    return Object.entries(formData).reduce((accum, textField) => {
      const [textKey, textValue] = textField;
      const newKey = textKey.indexOf('text_') !== -1 ? textKey.split('text_')[1] : textKey;
      accum[newKey] = textValue;
      return accum;
    }, {});
  }

  // Actions

  save(isDraft = false) {
    if (this.form.valid || isDraft) {
      const form = this.form.value;
      form.draft = isDraft;

      const data = this.transformData(form);

      let observable: Observable<any>;
      if (this.isEditFlow) {
        observable = this.store.dispatch(new TextActions.Edit(data));
      } else {
        observable = this.store.dispatch(new TextActions.Add(data));
      }

      return observable.pipe(
        map(() => this.canImmediatelyDeactivate = true),
        catchError(() => of(false)),
      );
    } else {
      Utils.touchForm(this.form);

      return of(false);
    }
  }

  wasChanged() {
    return !this.form.pristine;
  }

  deleteDraft(text: TextModel): void {
    this.store.dispatch(new TextActions.Delete(+text.id));
    this.dialog.closeAll();
    if (+text.id === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }

}
