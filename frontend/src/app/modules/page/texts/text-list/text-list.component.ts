import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';

import { TextModel } from 'src/app/shared/models';
import { TextState } from 'src/app/store/state/text.state';
import { TextActions } from 'src/app/store/actions/text.actions';
import { UserState } from 'src/app/store/state/user.state';
import { Utils } from 'src/app/shared/utils/utils';
import { VisibleState } from 'src/app/store/state/visible.state';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-text-list',
  templateUrl: './text-list.component.html',
  styleUrls: [ './text-list.component.scss']
})

export class TextListComponent implements OnInit, OnDestroy {

  @Select(TextState.texts) texts$: Observable<TextModel[]>;
  @Select(TextState.total) total$: Observable<number>;
  @Select(TextState.loading) loading$: Observable<boolean>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;

  textMapping = {};

  truncated = {};

  page = 1;
  pageSize = 30;
  filter = 'date';

  currentUrl = '';

  trackById = Utils.trackById;
  navigating = false;

  private destroy$ = new Subject();

  constructor(private store: Store,
              private translateService: TranslateService,
              private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.currentUrl = environment.appUrl;
    this.store.dispatch(new TextActions.Init(this.pageSize));

    this.translateService.stream('PAGES.TEXT_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.textMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  applyFilter(filter: string): void {
    this.filter = filter;
    this.store.dispatch(new TextActions.Init(this.pageSize, this.filter, 'desc'));
  }

  onScroll() {
    this.page++;
    this.store.dispatch(new TextActions.Load(this.page, this.pageSize, this.filter, 'asc'));
  }

  getUserName(item) {
    return item.user.business && item.user.business.name || item.user.person.fullName;
  }

  getPageName(item) {
    return item.user.business && item.user.business.pageName || item.user.person.pageName;
  }

  getUserPhoto(item) {
    return item.user.business && item.user.business.photo || item.user.person.photo || constants.PLACEHOLDER_AVATAR_PATH;
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }

  deleteText(textId: number) {
    this.store.dispatch(new TextActions.Delete(textId));
  }
}
