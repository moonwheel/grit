import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { SharedModule } from 'src/app/shared/shared.module';

import { TextListRouting } from './text-list.routing';
import { TextListComponent } from './text-list.component';

@NgModule({
  declarations: [
    TextListComponent,
  ],
  imports: [
    CommonModule,
    TextListRouting,
    SharedModule,
    InfiniteScrollModule,
  ]
})

export class TextListModule { }
