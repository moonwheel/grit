import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TextListComponent } from './text-list.component';

const routes: Routes = [
  {
    path: '',
    component: TextListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TextListRouting { }
