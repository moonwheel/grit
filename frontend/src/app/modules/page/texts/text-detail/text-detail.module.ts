import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { TextDetailRouting } from './text-detail.routing';
import { SharedModule } from '../../../../shared/shared.module';

import { TextDetailComponent } from './text-detail.component';


@NgModule({
  declarations: [
    TextDetailComponent,
  ],
  imports: [
    CommonModule,
    TextDetailRouting,
    SharedModule,
  ]
})
export class TextDetailModule { }
