import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { switchMap, tap, switchMapTo } from 'rxjs/operators';

import { TextModel } from '../../../../shared/models/text.model';
import { TextState } from '../../../../store/state/text.state';
import { TextActions } from '../../../../store/actions/text.actions';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { UserState } from '../../../../store/state/user.state';

@Component({
  selector: 'app-text-detail',
  templateUrl: './text-detail.component.html',
  styleUrls: ['./text-detail.component.scss'],
})

export class TextDetailComponent implements OnInit, OnDestroy {

  @Select(TextState.loading)
  loading$: Observable<boolean>;

  text$: Observable<TextModel>;

  constructor(
    protected store: Store,
    protected route: ActivatedRoute,
    protected dialog: MatDialog,
    protected router: Router,
  ) {
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));

    this.text$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(TextState.text(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new TextActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(TextState.text(id))),
            tap(text => {
              if (text && !text.viewed) {
                this.store.dispatch(new TextActions.View(id));
              }
            }),
          );
        } else {
          return this.store.select(TextState.text(id));
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  onDelete() {
    this.router.navigate(['../../home'], { relativeTo: this.route });
  }
}
