import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextDetailComponent } from './text-detail.component';
import { TextState } from '../../../../store/state/text.state';

const routes: Routes = [
  {
    path: '',
    component: TextDetailComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextDetailRouting {}
