import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./service-list/service-list.module').then(m => m.ServiceListModule) },
  { path: 'add', loadChildren: () => import('./service-add/service-add.module').then(m => m.ServiceAddModule) },
  { path: 'book', loadChildren: () => import('./service-book/service-book.module').then(m => m.ServiceBookModule) },
  { path: ':id', loadChildren: () => import('./service-detail/service-detail.module').then(m => m.ServiceDetailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServicesRouting {
}
