import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { ServicesRouting } from './services.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ServicesRouting,
    SharedModule
  ]
})
export class ServicesModule { }
