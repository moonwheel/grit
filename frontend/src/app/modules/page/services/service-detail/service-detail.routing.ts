import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ServiceDetailComponent } from './service-detail.component';
import { UploadingGuard, UploadingGuardStateToken } from 'src/app/core/guards';
import { ServiceState } from 'src/app/store/state/service.state';

const routes: Routes = [
  {
    path: '',
    component: ServiceDetailComponent,
    canActivate: [ UploadingGuard ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UploadingGuard,
    { provide: UploadingGuardStateToken, useValue: ServiceState },
  ]
})
export class ServiceDetailRouting {}
