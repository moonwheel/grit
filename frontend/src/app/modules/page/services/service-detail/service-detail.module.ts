import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ServiceDetailRouting } from './service-detail.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ServiceDetailComponent } from './service-detail.component';


@NgModule({
  declarations: [
    ServiceDetailComponent,
  ],
  imports: [
    CommonModule,
    ServiceDetailRouting,
    SharedModule,
  ]
})
export class ServiceDetailModule { }
