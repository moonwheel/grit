import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ReviewListComponent } from '../../../../shared/components/actions/reviews/review-list/review-list.component';
import { AddBookmark } from '../../../../store/actions/bookmark.actions';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Select, Store } from '@ngxs/store';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { ServiceActions } from '../../../../store/actions/service.actions';
import { constants } from 'src/app/core/constants/constants';
import { DateTime } from 'luxon';
import { switchMap, takeUntil, switchMapTo, map } from 'rxjs/operators';
import { SatPopover } from '@ncstate/sat-popover';
import { UserState } from 'src/app/store/state/user.state';
import { Observable, Subject, of } from 'rxjs';
import { ServiceState } from 'src/app/store/state/service.state';
import { AbstractDetailPageComponent } from 'src/app/shared/components';

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss'],
})

export class ServiceDetailComponent extends AbstractDetailPageComponent implements OnInit, OnDestroy {

  @Select(ServiceState.loading) loading$: Observable<boolean>;

  service$: Observable<ServiceModel>;

  private destroy$ = new Subject();

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialog: MatDialog,
    protected store: Store,
  ) {
    super(dialog, store);
  }

  ngOnInit() {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));
    this.service$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(ServiceState.service(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new ServiceActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(ServiceState.service(id)))
          );
        } else {
          return this.store.select(ServiceState.service(id));
        }
      }),
      takeUntil(this.destroy$),
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  onDelete() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
