import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { switchMap, takeUntil, delay, switchMapTo } from 'rxjs/operators';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { ServiceState } from '../../../../store/state/service.state';
import { ServiceActions } from '../../../../store/actions/service.actions';
import { environment } from 'src/environments/environment';
import { DateTime } from 'luxon';
import { constants } from 'src/app/core/constants/constants';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { UserState } from 'src/app/store/state/user.state';
import { AddBooking } from 'src/app/store/actions/booking.actions';
import { ResultDialogComponent } from 'src/app/shared/components/result-dialog/result-dialog.component';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { AddressModel } from 'src/app/shared/models';

@Component({
  selector: 'app-service-book',
  templateUrl: './service-book.component.html',
  styleUrls: ['./service-book.component.scss']
})

export class ServiceBookComponent implements OnInit, OnDestroy {

  @Select(ServiceState.services)
  services: Observable<ServiceModel[]>;

  @Select(ServiceState.loading)
  loading$: Observable<boolean>;

  @Select(UserState.viewableAccountFullName)
  viewableAccountFullName$: Observable<string>;

  @Select(PaymentsState.balance)
  balance$: Observable<number>;

  serviceBookForm: FormGroup;
  service: ServiceModel;
  environment = environment;
  workDays;
  workTime = [];
  today = new Date();
  durations = [ ...constants.DURATIONS ];

  showCash = false;

  quantities = [2, 1, 0, 11, 5, 10, 15, 8, 0, 0, 1, 1, 2, 2, 2];

  total = 0;

  upToMaxQuantityArray = [];
  banks = [];

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  private destroy$ = new Subject();

  constructor(
    private navigationService: NavigationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private actions$: Actions,
    private store: Store
  ) { }

  ngOnInit() {
    this.store.dispatch(new ToggleVisible({ isVisibleSearch: false, isVisibleMain: false, isVisibleUserTabs: false }));

    this.setupForms();
    this.setupSubscriptions();

    this.actions$.pipe(
      ofActionSuccessful(AddBooking),
      takeUntil(this.destroy$),
    ).subscribe(data => {
      return this.dialog.open(ResultDialogComponent, {
        data: { message: 'Success' },
      }).afterClosed().subscribe(() => {
        this.router.navigateByUrl('/purchases');
      });;
    });

    this.actions$.pipe(
      ofActionErrored(AddBooking),
      takeUntil(this.destroy$),
    ).subscribe(data => {
      return this.dialog.open(ResultDialogComponent, {
        data: {
          message: 'Error',
        },
      });
    });
  }

  ngOnDestroy() {
    this.store.dispatch(new ToggleVisible({ isVisibleSearch: true, isVisibleMain: true, isVisibleUserTabs: true }));
    this.destroy$.next();
    this.destroy$.complete();
  }

  setupForms() {
    this.serviceBookForm = this.formBuilder.group({
      date: '',
      time: [{ value: '', disabled: true }],
      appointment: ['', Validators.required],
      address: '',
      payment: ['', Validators.required],
      bookedTime: [[]],
      grit_wallet: false,
    });
  }

  setupSubscriptions() {
    this.route.queryParams.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(ServiceState.service(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new ServiceActions.GetOne(id)).pipe(
            switchMapTo(this.store.selectOnce(ServiceState.service(id)))
          );
        } else {
          return of(item);
        }
      })
    ).subscribe(service => {
      this.service = { ...service };
      if (this.service.schedule.byDays) {
        this.getWorkingDaysArray();
      }
      if (this.service.schedule.byDate) {
        this.getWorkingTimeArray();
      }
      if (this.service.performance === 'customer') {
        this.serviceBookForm.get('address').setValidators([Validators.required]);
      }
      const durationInfo = this.durations.filter(item => {
        return item.value === this.service.schedule.duration;
      }).map(item => item.name);

      this.service.durationInfo = durationInfo && durationInfo[0];
    });

    this.serviceBookForm.get('date').valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(value => {
      if (value) {
        this.serviceBookForm.get('time').enable();
      } else {
        this.serviceBookForm.get('time').disable();
      }
    });

    this.serviceBookForm.get('bookedTime').valueChanges.pipe(
      delay(100),
      takeUntil(this.destroy$)
    ).subscribe(() => {
      if (this.service.schedule.byDays && this.service.schedule.byDays.length) {
        this.getWorkingTimeArray();
      } else
      if (this.service.schedule.byDays && this.service.schedule.byDays.length) {
        this.getWorkingDaysArray();
      }

      if (this.workTime.length) {
        this.serviceBookForm.get('time').enable();
        this.serviceBookForm.get('appointment').enable();
      } else {
        this.serviceBookForm.get('time').disable();
        this.serviceBookForm.get('appointment').disable();
      }
    });
  }

  back() {
    this.navigationService.back();
  }

  togglePositionToDatepicker(status: boolean): void {
    setTimeout(() => {
      const overlayPane = document.getElementsByClassName('cdk-overlay-pane')[0] as HTMLElement;
      if (status) {
        overlayPane.style.top = '50%';
        overlayPane.style.left = '50%';
        overlayPane.style.bottom = 'auto';
        overlayPane.style.right = 'auto';
        overlayPane.style.transform = 'translate(-50%, -50%)';
        overlayPane.parentElement.style.background = 'rgba(0, 0, 0, 0.7)';
      }
    }, 0);
  }

  // Schedule

  filterDays = (day) => {
    if (this.workDays) {
      const dayValue = new Date(day).getDay();
      return this.workDays.findIndex(item => item === dayValue) !== -1;
    }
  }

  getWorkingDaysArray() {
    this.workDays = this.service.schedule.byDays.map(day => {
      if (day.day.id < 7) {
        return day.day.id;
      } else {
        return 0;
      }
    });
  }

  getWorkingTimeArray() {
    this.workTime = [];
    const day = this.serviceBookForm.get('date').value;
    if (!day) {
      return;
    }
    const dayNumber = new Date(day).getDay();
    const schedule = this.service.schedule.byDays.filter(item => {
      if (dayNumber !== 0) {
        return item.day.id === dayNumber;
      } else {
        return item.day.id === 7;
      }

    });
    let fromValue;
    let toValue;
    let ranges = [];
    const list = [];

    schedule.forEach(item => {
      ranges = ranges.concat(item.intervals);
    });
    ranges.forEach(range => {
      fromValue = range.from.substring(0, 5);
      toValue = range.to.substring(0, 5);
      list.push({
        from: fromValue,
        to: toValue,
        date: this.serviceBookForm.get('date').value
      });
    });

    const quantity = this.service.performance === 'customer' ? 1 : this.service.quantity;

    if (list.length) {
      list.forEach(item => {
        if (this.serviceBookForm.controls.bookedTime.value.length) {
          const dateTimeValue = DateTime
            .fromJSDate(this.serviceBookForm.get('date').value)
            .plus({
              minutes: +item.from.split(':')[1],
              hours: +item.from.split(':')[0]
            })
            .toISO();

          const found = this.serviceBookForm.controls.bookedTime.value.find(element => {
            return element.dateTimeValue.toISO() === dateTimeValue;
          });

          if (!found) {
            this.workTime.push({
              name: item.from + ' - ' + item.to,
              value: item.from,
              quantity
            });
          }
        } else {
          this.workTime.push({
            name: item.from + ' - ' + item.to,
            value: item.from,
            quantity
          });
        }
      });
    } else {
      this.workTime = [{ name: 'No time available', value: '', quantity: '', upToMaxQuantityArray: [] }];
    }
  }

  getAppointmentsArray() {
    this.workTime = [];
    const list = [];
    let found = [];
    const quantity = this.service.performance === 'customer' ? 1 : this.service.quantity;

    this.service.schedule.byDate.forEach((item, index) => {
      const start = DateTime.fromISO(item.from).toFormat(`dd MMM y, HH:mm`);
      const end = DateTime.fromISO(item.to).toFormat('dd MMM y, HH:mm');
      const name = start + ' - ' + end;
      if (this.serviceBookForm.get('bookedTime').value.length) {
        found = this.serviceBookForm.get('bookedTime').value.filter(element => {
          const firstV = DateTime.fromISO(element.dateTimeValue).toISO();
          const secondV = DateTime.fromISO(item.from).toISO();
          return firstV === secondV;
        });
      }
      if (!found.length) {
        list.push({ name, value: item.from, quantity });
      }
    });
    if (list.length) {
      list.forEach(item => {
        if (item.quantity !== 0) {
          this.workTime.push(item);
        }
      });
    } else {
      this.workTime = [{ name: 'All appointments has been selected', value: '', quantity: '', upToMaxQuantityArray: [] }];
    }
  }

  addBooking(type) {
    const upToMaxQuantityArray = [];
    let valueToDisplay;
    let availableQuantity;
    switch (type) {
      case 'daily':
        if (!this.serviceBookForm.get('time').value) {
          break;
        }
        const time = DateTime.fromFormat(this.serviceBookForm.get('time').value, 'HH:mm').toFormat('HH:mm');
        const displayedTime = this.workTime.filter(item => item.value === time)[0].name;
        availableQuantity = this.workTime.filter(item => item.value === time)[0].quantity;
        if (availableQuantity > 10) {
          availableQuantity = 10;
        }
        for (let i = 1; i <= availableQuantity; i++) {
          upToMaxQuantityArray.push(i);
        }
        const dateTimeValue = DateTime.fromJSDate(this.serviceBookForm.get('date').value)
          .plus({ minutes: +time.split(':')[1], hours: +time.split(':')[0] });
        valueToDisplay = dateTimeValue.toFormat('dd MMM y') + ', ' + displayedTime;

        const valueToTime = displayedTime.split(' - ');
        
        this.serviceBookForm.get('bookedTime').setValue([
          ...this.serviceBookForm.get('bookedTime').value,
          {
            valueToDisplay,
            time,
            dateTimeValue,
            quantity: 1,
            upToMaxQuantityArray,
            date: {
              start: dateTimeValue.toFormat('dd MMM y') + ', ' + valueToTime[0],
              end: dateTimeValue.toFormat('dd MMM y') + ', ' + valueToTime[1],
            },
          }
        ]);
        this.serviceBookForm.get('time').setValue('');
        break;
      case 'appointments':
        if (!this.serviceBookForm.get('appointment').value) {
          break;
        }
        const date = DateTime.fromISO(this.serviceBookForm.get('appointment').value).toISO();
        valueToDisplay = this.workTime.filter(item => {
          const itemToISO = DateTime.fromISO(item.value).toISO();
          return itemToISO === date;
        })[0].name;
        availableQuantity = this.workTime.filter(item => {
          const itemToISO = DateTime.fromISO(item.value).toISO();
          return itemToISO === date;
        })[0].quantity;
        if (availableQuantity > 10) {
          availableQuantity = 10;
        }
        for (let i = 1; i <= availableQuantity; i++) {
          upToMaxQuantityArray.push(i);
        }

        const valueToShedule = valueToDisplay.split(' - ');

        this.serviceBookForm.get('bookedTime').setValue([
          ...this.serviceBookForm.get('bookedTime').value,
          {
            valueToDisplay,
            dateTimeValue: this.serviceBookForm.get('appointment').value,
            quantity: 1,
            upToMaxQuantityArray,
            date: {
              start: valueToShedule[0] || '',
              end: valueToShedule[1] || '',
            },
          }
        ]);
        this.serviceBookForm.get('appointment').setValue('');
        break;
    }
    this.countTotal();
    this.serviceBookForm.patchValue({'appointment': 'set'}); // sets value for appointment field to make field valid
  }

  removeBooking(index) {
    const filteredBookingList = this.serviceBookForm.get('bookedTime').value.filter((item, idx) => idx !== index);

    this.serviceBookForm.get('bookedTime').setValue(filteredBookingList);

    if(!filteredBookingList.length) { // triggers required on appointment field if no appointments are selected
      this.serviceBookForm.patchValue({'appointment': null});
    }

    this.countTotal();
  }

  countTotal() {
    let quantityOfItems = 0;
    this.serviceBookForm.get('bookedTime').value.forEach(item => {
      quantityOfItems = quantityOfItems + +item.quantity;
    });
    this.total = this.service.price * quantityOfItems;
  }

  // Book

  confirmBooking() {
    const formValue = this.serviceBookForm.value;

    if(!this.serviceBookForm.valid) {
      return;
    }

    this.store.dispatch(new AddBooking({
      serviceId: this.service.id,
      payment: formValue.payment,
      appointments: formValue.bookedTime.map(val => val.date),
      gritWallet: formValue.grit_wallet,
    }));
  }

  showAddress(address: AddressModel) {
    if (!address) {
      return '';
    }
    return [
      address.street,
      address.zip,
      address.city,
    ].filter(Boolean).join(', ');
  }
}
