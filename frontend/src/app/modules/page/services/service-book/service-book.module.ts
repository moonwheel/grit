import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ServiceBookRouting } from './service-book.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ServiceBookComponent } from './service-book.component';


@NgModule({
  declarations: [
    ServiceBookComponent,
  ],
  imports: [
    CommonModule,
    ServiceBookRouting,
    SharedModule,
  ]
})
export class ServiceBookModule { }
