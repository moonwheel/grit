import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ServiceBookComponent } from './service-book.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceBookComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceBookRouting {}
