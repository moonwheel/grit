import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ServiceAddRouting } from './service-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ServiceAddComponent } from './service-add.component';
import { AddViewLayout } from '../../components/layouts/add-view-layout/add-view-layout.module';


@NgModule({
  declarations: [
    ServiceAddComponent,
  ],
  imports: [
    CommonModule,
    ServiceAddRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class ServiceAddModule { }
