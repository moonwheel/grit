import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ServiceAddComponent } from './service-add.component';
// Guards
import { ProfileGuard, CanDeactivateGuard, ViewerGuard } from 'src/app/core/guards';


const routes: Routes = [
  {
    path: '',
    component: ServiceAddComponent,
    canActivate: [ProfileGuard, ViewerGuard],
    canDeactivate: [CanDeactivateGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceAddRouting {}
