import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ServiceState } from '../../../../store/state/service.state';
import { UserState } from 'src/app/store/state/user.state';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, of } from 'rxjs';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { ServiceActions } from '../../../../store/actions/service.actions';
import { environment } from 'src/environments/environment';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { constants } from 'src/app/core/constants/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { getDefaultWeekley } from 'src/app/shared/constants';
import { SERVICE_CATEGORIES, SERVICE_BREAKS, SERVICE_TIME_VALUES } from '../shared';
import { takeUntil, map, catchError, switchMapTo } from 'rxjs/operators';
import { Utils } from 'src/app/shared/utils/utils';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-service-add',
  templateUrl: './service-add.component.html',
  styleUrls: ['./service-add.component.scss']
})
export class ServiceAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;
  @Select(ServiceState.services) services$: Observable<ServiceModel[]>;
  @Select(ServiceState.drafts) drafts$: Observable<ServiceModel[]>;
  @Select(ServiceState.loading) loading$: Observable<boolean>;

  form: FormGroup;

  durations = [ ...constants.DURATIONS ];
  imagesArray = [];

  breaks = [ ...SERVICE_BREAKS ];
  defaultWeekly = [ ...getDefaultWeekley() ];
  timeValues = [ ...SERVICE_TIME_VALUES ];
  categories = [ ...SERVICE_CATEGORIES ];

  serviceDraftsMapping = {};

  performanceLocation = false;
  isMultipleDaysDuration = false;
  isEditFlow = false;
  isDraft = false;
  photosLoading = false;
  environment = environment;
  weekly;
  invalidImages = false;
  noAnyAppointment = false;
  removedDateRanges = [];
  serviceId: number;

  leaveDialogRef: MatDialogRef<{}, any>;

  trackById = Utils.trackById;

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  get dateRangeForm() {
    return this.form.get('dateRange') as FormArray;
  }

  get photos(): Array<any> {
    const photos = this.form.get('photos');
    return photos ? photos.value : [];
  }

  set photos(value: Array<any>) {
    this.form.get('photos').setValue(value);
  }

  constructor(
    protected formBuilder: FormBuilder,
    protected dialog: MatDialog,
    protected store: Store,
    protected router: Router,
    protected route: ActivatedRoute,
    protected navigationService: NavigationService,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));
    this.store.dispatch(new ServiceActions.LoadDrafts());

    this.form = this.formBuilder.group({
      id: null,
      photos: [[], Validators.required],
      title: ['', Validators.required],
      price: ['', Validators.required],
      category: ['', Validators.required],
      description: '',
      performance: ['', Validators.required],
      address: null,
      duration: ['01:00', Validators.required],
      break: ['00:00', Validators.required],
      dateRange: this.formBuilder.array([]),
      views: 0,
      message: 0,
      shares: 0,
      bookmarks: 0,
      draft: false,
      frequency: true,
      quantity: 1,
      hashtags: [[], Validators.maxLength(constants.MAX_HASHTAGS_NUMBER)],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
    });

    this.setAddressValidators();

    this.route.queryParams.subscribe(queryParams => {
      if ('id' in queryParams) {
        this.isEditFlow = true;
      }

      if (this.isEditFlow) {
        this.setEditViewMode(true);
        this.setEditMode(+queryParams.id);
      } else {
        this.addDayRange(false);
      }
    });

    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.serviceDraftsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  setAddressValidators() {
    const addressControl = this.form.get('address');

    this.form.get('performance').valueChanges.pipe(
      takeUntil(this.destroy$),
    ).subscribe((performance) => {
      if (performance !== 'customer') {
        addressControl.setValidators([Validators.required]);
      } else {
        addressControl.clearValidators();
        addressControl.updateValueAndValidity();
      }
    });
  }

  wasChanged() {
    const photos = this.form.value.photos;
    const newPhoto = photos && photos.find(item => item.file);
    return (
      !this.form.pristine
      || (photos && this.imagesArray.length !== photos.length)
      || newPhoto
    );
  }

  async selectPhotos(event: Event) {
    const target = event.target as HTMLInputElement;
    const files = target.files;

    if (files && files.length) {
      this.photosLoading = true;

      const promises = Array.from(files).map(async (file) => {
        const { url } = await Utils.getImageData(file);

        if (this.form.get('photos').value.length < 6) {
          const currentPhotos = this.form.get('photos').value;
          currentPhotos.push({
            file,
            link: url,
            index: this.form.controls.photos.value.length
          });
          this.form.get('photos').setValue(currentPhotos);
        }
      });

      await Promise.all(promises);

      this.photosLoading = false;

      target.value = null;
    }
  }

  removeSelectedPhoto(index: number) {
    this.form.get('photos').value.splice(index, 1);
  }

  reorderImages(event: CdkDragDrop<string[]>) {
    const photos = this.form.get('photos').value;
    moveItemInArray(photos, event.previousIndex, event.currentIndex);
    this.form.get('photos').setValue(photos);
  }

  checkRanges() {
    const dateRange = this.form.value.dateRange;
    if (
      !dateRange.length ||
      dateRange[0].day && !dateRange.find(item => item.enabled)
    ) {
      this.noAnyAppointment = true;
      this.dateRangeForm.setErrors({ isEmpty: true });
    }

    this.dateRangeForm.controls.forEach(range => {
      if (this.form.controls.frequency.value) {
        const durationTime = this.form.get('duration').value;

        if (range.value.from_first && range.value.to_first) {
          const firstValue = DateTime.fromISO(range.value.from_first);
          const lastValue = DateTime.fromISO(range.value.to_first);
          const shouldEnd = firstValue.plus({ minutes: +durationTime.split(':')[1], hours: +durationTime.split(':')[0] });
          if (lastValue >= shouldEnd) {
            range.get('from_first').setErrors(null);
            range.get('to_first').setErrors(null);
          } else {
            range.get('from_first').setErrors({ badRange: true });
            range.get('to_first').setErrors({ badRange: true });
          }
        }
        if (range.value.from_second && range.value.to_second) {
          const firstValue = DateTime.fromISO(range.value.from_second);
          const lastValue = DateTime.fromISO(range.value.to_second);
          const shouldEnd = firstValue.plus({ minutes: +durationTime.split(':')[1], hours: +durationTime.split(':')[0] });
          if (lastValue >= shouldEnd) {
            range.get('from_second').setErrors(null);
            range.get('to_second').setErrors(null);
          } else {
            range.get('from_second').setErrors({ badRange: true });
            range.get('to_second').setErrors({ badRange: true });
          }
        }
      } else {
        const firstValue = DateTime.fromJSDate(range.value.from);
        const lastValue = DateTime.fromJSDate(range.value.to);
        if (firstValue >= lastValue) {
          range.get('from').setErrors({ badRange: true });
          range.get('to').setErrors({ badRange: true });
        } else {
          range.get('from').setErrors(null);
          range.get('to').setErrors(null);
        }
      }
    });
  }

  addDateRange() {
    this.noAnyAppointment = false;
    this.dateRangeForm.setErrors(null);
    if (this.dateRangeForm.value[0] && !this.dateRangeForm.value[0].from) {
      while (this.dateRangeForm.value.length) {
        this.dateRangeForm.removeAt(0);
      }
    }
    const fromTo = this.formBuilder.group({
      from: [[], Validators.required],
      to: [[], Validators.required],
      id: null
    });
    this.dateRangeForm.push(fromTo);
  }

  addDayRange(editing: boolean) {
    this.noAnyAppointment = false;
    this.dateRangeForm.setErrors(null);
    if (this.dateRangeForm.value[0] && !this.dateRangeForm.value[0].day) {
      this.dateRangeForm.clear();
    }
    if (!this.dateRangeForm.value[0]) {
      this.weekly = this.defaultWeekly.slice();
      this.weekly.forEach(day => {
        if (editing) {
          day.enabled = false;
        }
        if (day.enabled) {
          day.from_first = ['', Validators.required];
          day.to_first = ['', Validators.required];
        }
        const item = this.formBuilder.group(day);
        this.dateRangeForm.push(item);
      });
    }
  }

  deleteDateRange(index: number, removedRange: AbstractControl) {
    if (removedRange.value.id) {
      this.removedDateRanges.push(removedRange.value.id);
    }
    this.dateRangeForm.removeAt(index);
  }

  changeFrequency() {
    this.isMultipleDaysDuration = !this.form.controls.frequency.value;

    if (this.isMultipleDaysDuration) {
      this.addDateRange();
    } else {
      if (!this.form.controls.duration.value) {
        this.form.controls.duration.setValue('01:00');
      }
      if (!this.form.controls.break.value) {
        this.form.controls.break.setValue('00:00');
      }
      this.addDayRange(false);
    }

    this.setDurationAndBreakValidators();
  }

  setDurationAndBreakValidators() {
    if (this.isMultipleDaysDuration) {
      this.form.controls.duration.clearValidators();
      this.form.controls.break.clearValidators();
    } else {
      this.form.controls.duration.setValidators(Validators.required);
      this.form.controls.break.setValidators(Validators.required);
    }
    this.form.controls.duration.updateValueAndValidity();
    this.form.controls.break.updateValueAndValidity();
  }

  toggleDay(day: AbstractControl) {
    const index = this.dateRangeForm.value.findIndex(element => element.day === day.value.day);
    const currentDayControls = this.dateRangeForm.controls[index];

    if (!currentDayControls) {
      return;
    }

    if (day.value.enabled) {
      this.noAnyAppointment = false;
      this.dateRangeForm.setErrors(null);
      currentDayControls.get('from_first').setValidators([Validators.required]);
      currentDayControls.get('to_first').setValidators([Validators.required]);
    } else {
      currentDayControls.get('from_second').clearValidators();
      currentDayControls.get('from_second').updateValueAndValidity();
      currentDayControls.get('to_second').clearValidators();
      currentDayControls.get('to_second').updateValueAndValidity();
      currentDayControls.get('from_first').clearValidators();
      currentDayControls.get('from_first').updateValueAndValidity();
      currentDayControls.get('to_first').clearValidators();
      currentDayControls.get('to_first').updateValueAndValidity();
    }
  }

  toggleSecondTimeRange(day: AbstractControl, show: boolean) {
    const index = this.dateRangeForm.value.findIndex(element => element.day === day.value.day);
    const currentDayControls = this.dateRangeForm.controls[index];

    if (!currentDayControls) {
      return;
    }

    currentDayControls.get('addition').setValue(show);

    if (show) {
      currentDayControls.get('from_second').setValidators([Validators.required]);
      currentDayControls.get('to_second').setValidators([Validators.required]);
    } else {
      currentDayControls.get('from_second').clearValidators();
      currentDayControls.get('from_second').updateValueAndValidity();
      currentDayControls.get('to_second').clearValidators();
      currentDayControls.get('to_second').updateValueAndValidity();
    }
  }

  removeErrors(day: AbstractControl, order: string) {
    day.get(`from_${order}`).updateValueAndValidity();
    day.get(`to_${order}`).updateValueAndValidity();
  }

  save(draft: boolean) {
    // console.log('form validation', Object.keys(this.form.controls).reduce((res, key) => {
      // res[key] = this.form.controls[key].valid;
      // return res;
    // }, {}));

    const description = this.form.controls.description.value;
    const price = this.form.controls.price.value;
    const payload: any = {
      id: this.serviceId,
      service: {
        active: true,
        hourlyPrice: false,
        bank_id: null,
        title: this.form.controls.title.value,
        price: price === '' ? null : price,
        quantity: this.form.controls.quantity.value,
        description: description && description.trim(),
        performance: 'customer',
        category: this.form.controls.category.value
      },
      hashtags: this.form.controls.hashtags.value,
      mentions: this.form.controls.mentions.value,
    };

    if (!draft) {
      this.checkRanges();
    }

    payload.service.draft = !!draft;
    payload.draft = !!draft;

    if (!this.form.valid && !draft) {
      this.invalidImages = true;
      this.form.markAllAsTouched();
      return of(false);
    }

    payload.schedule = {
      duration: this.form.controls.duration.value,
      break: this.form.controls.break.value
    };
    payload.byDays = [];
    payload.byDate = [];

    if (this.isMultipleDaysDuration) {
      payload.byDate = this.form.value.dateRange;
      if (this.removedDateRanges.length) {
        this.removedDateRanges.forEach(range => payload.byDate.push({ id: range }));
      }
    } else {
      this.form.value.dateRange.forEach(day => {
        const dayPayload = [];
        if (day.enabled && (day.from_first || day.to_first)) {
          dayPayload.push({
            from: day.from_first,
            to: day.to_first,
            day: day.id,
            id: day.firstPartId
          });
        }
        if (day.enabled && day.addition && day.from_second) {
          dayPayload.push({
            from: day.from_second,
            to: day.to_second,
            day: day.id,
            id: day.secondPartId,
          });
        }
        if (!day.enabled && day.firstPartId) {
          dayPayload.push({
            id: day.firstPartId
          });
        }
        if ((!day.enabled || !day.addition || !day.from_second) && day.secondPartId) {
          dayPayload.push({
            id: day.secondPartId
          });
        }
        if (dayPayload.length) {
          payload.byDays = payload.byDays.concat(dayPayload);
        }
      });
    }

    if (this.performanceLocation) {
      payload.service.address = this.form.controls.address.value;
      payload.service.performance = 'provider';
    }

    const photoFiles = this.photos
      .map(({ file }, index) => ({ file, index }))
      .filter(item => !!item.file);

    const dataToUpload = photoFiles.length ? photoFiles : null;

    let observable: Observable<any>;

    if (this.isEditFlow) {
      payload.photos = this.photos.map((photoItem, index) => {
        if (photoItem.id) {
          return {
            id: photoItem.id,
            index
          };
        } else {
          return {
            index,
          };
        }
      });

      observable = this.store.dispatch(new ServiceActions.Edit(payload, dataToUpload));
      console.log('payload', payload);
    } else {
      if (this.photos) {
        payload.photosLength = this.photos.length;
      }

      observable = this.store.dispatch(new ServiceActions.Add(payload, dataToUpload));
    }

    return observable.pipe(
      map(() => this.canImmediatelyDeactivate = true),
      catchError(() => of(false)),
    );
  }

  setEditMode(serviceId: number) {
    window.scroll({ top: 0, left: 0, behavior: 'smooth' });

    this.serviceId = serviceId;

    const itemInStore = this.store.selectSnapshot(ServiceState.service(serviceId));

    if (!itemInStore || !itemInStore.isDetailed) {
      this.store.dispatch(new ServiceActions.GetOne(serviceId, true)).pipe(
        switchMapTo(this.store.selectOnce(ServiceState.service(serviceId)))
      ).subscribe(service => {
        this.setValue(service);
        this.setEditReadyState(true);
      });
    } else {
      this.setValue(itemInStore);
      this.setEditReadyState(true);
    }
  }

  setValue(service: ServiceModel) {
    this.isDraft = false;
    this.defaultWeekly = [ ...getDefaultWeekley() ];
    this.imagesArray = [];
    this.form.reset({
      hashtags: [],
      mentions: [],
    });
    this.form.patchValue({
      duration: '01:00',
      break: '00:00',
      dateRange: [],
    });

    if (service) {
      this.isDraft = service.draft;
      if (service.photos && service.photos.length) {
        this.imagesArray = service.photos;
        const photos = service.photos.map((item, index) => {
          return {
            id: item.id,
            link: `${item.link}`,
            index
          };
        });
        this.form.controls.photos.setValue(photos);
      } else {
        this.form.controls.photos.setValue([]);
      }

      this.form.controls.id.setValue(service.id);
      this.form.controls.title.setValue(service.title);
      this.form.controls.price.setValue(service.price);
      this.form.controls.category.setValue(service.category);
      this.form.controls.description.setValue(service.description);
      this.form.controls.performance.setValue(service.performance);
      this.performanceLocation = (service.performance === 'provider');
      this.form.controls.address.setValue(service.address && service.address.id);
      this.form.controls.quantity.setValue(service.quantity || 1);
      this.form.controls.hashtags.setValue(service.hashtags || []);
      this.form.controls.mentions.setValue(service.mentions || []);
      if (service.schedule && service.schedule.id) {
        this.form.controls.duration.setValue(service.schedule.duration);
        this.form.controls.break.setValue(service.schedule.break);
        this.form.controls.frequency.setValue(true);
        if (service.schedule.byDays && service.schedule.byDays.length || service.schedule.duration) {
          this.isMultipleDaysDuration = false;
          this.addDayRange(true);
          this.dateRangeForm.controls.forEach((control) => {
            const days = (service.schedule.byDays || []).filter(element => element.day.id === control.value.id);

            if (days.length) {
              days.forEach(day => {
                this.addDailySchedule(day);
              });
            } else {
              const dayControlValue = control.value;
              dayControlValue.enabled = false;
              control.setValue(dayControlValue);
              this.toggleDay(control);
            }
          });
        }
        if (service.schedule.byDate.length) {
          this.isMultipleDaysDuration = true;
          this.form.controls.frequency.setValue(false);
          this.addAppointmentSchedule(service.schedule.byDate);
        }
      }

      this.setDurationAndBreakValidators();
    }
  }

  addDailySchedule(day) {
    const weekly = this.defaultWeekly.slice();
    const currentDay = weekly[day.day.id - 1];
    currentDay.enabled = true;
    if (day.from && (!currentDay.from_first || !currentDay.from_first[0])) {
      currentDay.from_first = day.from;
    }
    if (day.to && (!currentDay.to_first || !currentDay.to_first[0])) {
      currentDay.to_first = day.to;
    }
    if (day.id && (currentDay.from_first === day.from)) {
      currentDay.firstPartId = day.id;
    }
    if (day.from && currentDay.from_first !== day.from) {
      currentDay.addition = true;
      if (day.from && currentDay.from_first !== day.from) {
        currentDay.from_second = day.from;
      }
      if (day.to && currentDay.to_first !== day.to) {
        currentDay.to_second = day.to;
      }
      if (day.id && (day.from && currentDay.from_first !== day.from)) {
        currentDay.secondPartId = day.id;
      }
    }
    this.dateRangeForm.controls.forEach((item, index) => {
      item.setValue(weekly[index]);
    });
  }

  addAppointmentSchedule(schedule) {
    this.isMultipleDaysDuration = true;
    schedule.forEach((item, index) => {
      this.addDateRange();
      let content;
      if (item.id) {
        content = { from: item.from, to: item.to, id: item.id };
      } else {
        content = { from: item.from, to: item.to };
      }
      this.dateRangeForm.controls[index].setValue(content);
    });

  }

  togglePositionToDatepicker(status: boolean): void {
    setTimeout(() => {
      const overlayPane = document.getElementsByClassName('cdk-overlay-pane')[0] as HTMLElement;
      if (status) {
        overlayPane.classList.add('grit-datetime-picker-panel');
        overlayPane.parentElement.classList.add('grit-datetime-picker-backdrop');
      }
    }, 0);
  }

  deleteDraft(service: ServiceModel): void {
    this.store.dispatch(new ServiceActions.Delete(+service.id));
    this.dialog.closeAll();
    if (+service.id === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }
}
