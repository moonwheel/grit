import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ServiceListComponent } from './service-list.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceListRouting {}
