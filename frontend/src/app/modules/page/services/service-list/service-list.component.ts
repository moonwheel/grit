import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, QueryList, ElementRef, ViewChildren } from '@angular/core';
import { ServiceState } from '../../../../store/state/service.state';
import { Select, Store } from '@ngxs/store';
import { Observable, BehaviorSubject, EMPTY, of } from 'rxjs';
import { ServiceModel } from 'src/app/shared/models/service.model';
import { ServiceActions } from '../../../../store/actions/service.actions';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UserState } from 'src/app/store/state/user.state';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { VisibleState } from 'src/app/store/state/visible.state';
import { map, debounceTime, takeUntil, switchMap, skip } from 'rxjs/operators';
import { NgModel } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
})
export class ServiceListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ServiceState.extendedServices) services$: Observable<ServiceModel[]>;
  @Select(ServiceState.total) total$: Observable<number>;
  @Select(ServiceState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<string>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(ServiceState.lazyLoading) lazyLoading$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;

  servicesSearch$ = new BehaviorSubject<string>('');

  header = true;
  search = false;
  serviceMapping = {};
  page = 1;
  pageSize = 15;
  filter = 'date';

  currentUrl = '';

  searchValue = '';
  navigating = false;

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new ServiceActions.Init(this.pageSize, this.filter, 'desc'));
    this.currentUrl = window.location.href;

    this.translateService.stream('PAGES.SERVICE_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.serviceMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  click() {
    console.log('clicked');
    this.store.dispatch(new ServiceActions.Load(this.page, this.pageSize, this.filter, 'desc', this.searchValue, true));
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new ServiceActions.Load(this.page, this.pageSize, this.filter, 'desc', this.searchValue, true));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new ServiceActions.Load(this.page, this.pageSize, this.filter, 'desc', this.searchValue));
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.searchValue) {
      this.store.dispatch(new ServiceActions.Load(1, this.pageSize, 'date', 'desc'));
    }
  }

  generateImageLink(service: ServiceModel): string {
    const cover = service.photoCover && (service.photoCover.thumb || service.photoCover.link);
    const firstPhoto = service.photos && service.photos[0] && service.photos[0].link;
    return cover || firstPhoto;
  }

  // Header

  showHeader() {
    this.header = true;
    this.search = false;
    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new ServiceActions.Load(this.page, this.pageSize, this.filter, 'desc', this.searchValue));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  // Filter

  applyFilter(filterName: string, order: any = 'desc') {
    this.filter = filterName;
    this.store.dispatch(new ServiceActions.Init(this.pageSize, this.filter, order));
  }

  // Actions

  inactiveService(service) {
    this.store.dispatch(new ServiceActions.ChangeActiveState({
      id: service.id,
      service: { active: !service.active }
    }));
  }

  deleteDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteService(id) {
    this.store.dispatch(new ServiceActions.Delete(id));
  }

  reportDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  getSearchValue(query: string) {
    this.searchValue = query;
    this.servicesSearch$.next(query);
  }
}
