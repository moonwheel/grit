import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ServiceListRouting } from './service-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';

// Components
import { ServiceListComponent } from './service-list.component';



@NgModule({
  declarations: [
    ServiceListComponent,
  ],
  imports: [
    CommonModule,
    ServiceListRouting,
    SharedModule,
    ListViewLayout,
  ]
})
export class ServiceListModule { }
