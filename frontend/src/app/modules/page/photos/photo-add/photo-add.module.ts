import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { PhotoAddRouting } from './photo-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { PhotoAddComponent } from './photo-add.component';
import { AddViewLayout } from '../../components/layouts/add-view-layout/add-view-layout.module';


@NgModule({
  declarations: [
    PhotoAddComponent,
  ],
  imports: [
    CommonModule,
    PhotoAddRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class PhotoAddModule { }
