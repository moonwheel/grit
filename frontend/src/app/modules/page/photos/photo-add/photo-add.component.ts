import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PhotoState } from 'src/app/store/state/photo.state';
import { PhotoActions } from 'src/app/store/actions/photo.actions';
import { Store, Select } from '@ngxs/store';
import { PhotoModel } from 'src/app/shared/models/content/photo.model';
import { Observable, of, EMPTY } from 'rxjs';
import { UserState } from 'src/app/store/state/user.state';
import { Utils } from 'src/app/shared/utils/utils';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { map, catchError, switchMap, takeUntil, switchMapTo, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PHOTO_FILTERS } from '../shared';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';
import { constants } from 'src/app/core/constants/constants';
import { UserService } from 'src/app/shared/services';
import { AccountModel, PhotoAnnotationModel } from 'src/app/shared/models';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { TranslateService } from '@ngx-translate/core';
import { InitAnnotations } from 'src/app/store/actions/annotation.actions';
import { AnnotationState } from 'src/app/store/state/annotation.state';

@Component({
  selector: 'app-photo-add',
  templateUrl: './photo-add.component.html',
  styleUrls: ['./photo-add.component.scss'],
})
export class PhotoAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select(PhotoState.photos) photos$: Observable<PhotoModel[]>;
  @Select(PhotoState.drafts) drafts$: Observable<PhotoModel[]>;
  @Select(PhotoState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;

  @ViewChild('photoContainer') photoContainer: ElementRef<HTMLDivElement>;

  photo$: Observable<PhotoModel>;

  form: FormGroup;

  isEditFlow = false;
  isDraft = false;
  isSubmitted = false;

  isInputValueChanged = false;
  invalidImage = false;

  categories = [ ...constants.MEDIA_CATEGORIES ];
  filters = [ ...PHOTO_FILTERS ];

  origin = 'normal';

  draftMapping = {};

  userList: AccountModel[] = [];
  showSearchFieled = false;
  annotations: PhotoAnnotationModel[] = [];
  showAnnotations = false;
  removable = true;
  localPhotoLoading = false;

  trackById = Utils.trackById;

  searchUsersInputControl = new FormControl();

  private photoFile: File;
  private initialPhotoData = '';

  get photoData() {
    const control = this.form.get('photo');
    return control && control.value;
  }

  constructor(
    protected formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    protected store: Store,
    protected dialog: MatDialog,
    protected http: HttpClient,
    protected navigationService: NavigationService,
    protected userService: UserService,
    protected matSnackbar: MatSnackBar,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));

    this.store.dispatch(new PhotoActions.LoadDrafts());

    this.route.data.subscribe(({ file }) => {
      if (file) {
        this.readFile(file);
      }
    });

    this.form = this.formBuilder.group({
      id: '',
      photo: [null, Validators.required],
      title: ['', Validators.required],
      category: ['', Validators.required],
      description: '',
      location: '',
      business_address: null,
      hashtags: [[], Validators.maxLength(constants.MAX_HASHTAGS_NUMBER)],
      mentions: [[], Validators.maxLength(constants.MAX_MENTIONS_NUMBER)],
      created: new Date(),
    });

    this.setFormData();
    this.setupUserSearchInput();

    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.draftMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  resetForm(): void {
    this.initialPhotoData = null;
    this.isDraft = false;
    this.origin = 'normal';
    this.form.reset({
      hashtags: [],
      mentions: [],
    });
  }

  setFormData(): void {
    this.route.queryParams.pipe(
      switchMap(params => {
        this.isEditFlow = 'id' in params;
        this.resetForm();
        const id = +params.id;

        if (this.isEditFlow) {
          this.setEditViewMode(true);
          const itemInStore = this.store.selectSnapshot(PhotoState.photo(id));

          this.store.dispatch(new InitAnnotations(id)).subscribe(() => {
            this.annotations = this.store.selectSnapshot(AnnotationState.photoAnnotations(id))
              .map(item => ({ ...item }));
          });

          if (!itemInStore || !itemInStore.isDetailed) {
            return this.store.dispatch(new PhotoActions.GetOne(id)).pipe(
              switchMapTo(this.store.selectOnce(PhotoState.photo(id)))
            );
          } else {
            return of(itemInStore);
          }
        } else {
          return EMPTY;
        }
      }),
      takeUntil(this.destroy$),
    ).subscribe(photo => {
      this.initialPhotoData = photo.photo;
      this.isDraft = photo.draft;
      this.origin = photo.filterName;
      this.form.patchValue(photo);
      this.form.get('business_address').setValue(photo.entity_business_address);
      
      this.setEditReadyState(true);
    });
  }

  selectPhoto(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target && target.files && target.files[0]) {
      this.readFile(target.files[0]);
      target.value = null;
    }
  }

  removePhoto(event: Event) {
    event.stopPropagation();
    this.isInputValueChanged = false;
    this.showSearchFieled = false;
    this.form.get('photo').setValue(null);
    this.origin = 'normal';
  }

  save(isDraft = false) {
    if (this.form.valid || isDraft) {
      this.isSubmitted = true;
      const photo = this.form.value;
      photo.draft = isDraft;
      photo.description = photo.description && photo.description.trim();
      photo.filterName = this.origin;
      photo.annotations = this.annotations;

      delete photo.photo;
      const data = this.transformData(photo);

      let observable: Observable<any>;
      if (this.isEditFlow) {
        observable = this.store.dispatch(new PhotoActions.Edit(data, this.photoFile));
      } else {
        observable = this.store.dispatch(new PhotoActions.Add(data, this.photoFile));
      }
      return observable.pipe(
        map(() => this.canImmediatelyDeactivate = true),
        catchError(() => of(false)),
      );
    } else {
      this.invalidImage = true;
      Utils.touchForm(this.form);

      return of(false);
    }
  }

  setFilter(origin: string) {
    this.origin = origin;
    this.isInputValueChanged = true;
  }

  generatePhotoUrl(photo: PhotoModel): string {
    const photoUrl = photo.photo;
    return `${photoUrl}`;
  }

  transformData(dataPhoto: PhotoModel): any {
    return Object.entries(dataPhoto).reduce((accum, paramPhoto) => {
      const [photoKey, photoValue] = paramPhoto;
      const cutKey = (photoKey.indexOf('photo_') !== -1) ? photoKey.split('_')[1] : photoKey;
      accum[cutKey] = photoValue;
      return accum;
    }, {});
  }

  // Tag

  setupUserSearchInput() {
    this.searchUsersInputControl.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(value => String(value).toLowerCase()),
      switchMap(searchTerm => {
        if (!searchTerm) {
          return of([]);
        }
        const exceptIds = this.annotations.map(item => item.target.id);
        return this.userService.search(searchTerm, 1, 20, exceptIds);
      }),
    ).subscribe(users => {
      users = users.map(user => Utils.extendAccountWithBaseInfo(user));
      this.userList = users;
    });
  }

  onUserSuggestionSelect(event: MatAutocompleteSelectedEvent) {
    const target = event.option.value as AccountModel;
    const tag: PhotoAnnotationModel = {
      id: null,
      x: 0,
      y: 0,
      target,
    };
    this.annotations = this.annotations.filter(item => item.target.id !== target.id);
    this.annotations.push(tag);
    this.showAnnotations = true;
    this.showSearchFieled = false;
    this.searchUsersInputControl.reset('');
  }

  getSuggestionText(user: AccountModel) {
    return user && user.name;
  }

  toggleUserSearch() {
    if (this.annotations.length >= constants.MAX_ANNOTATIONS_NUMBER) {
      const num = constants.MAX_ANNOTATIONS_NUMBER;
      this.translateService.get('ERRORS.MAX_ANNOTATIONS', { num }).subscribe(message => {
        this.matSnackbar.open(message);
      });
      this.showAnnotations = true;
      this.showSearchFieled = false;
      return;
    }

    this.searchUsersInputControl.reset('');
    this.showSearchFieled = !this.showSearchFieled;

    if (this.showAnnotations) {
      this.showAnnotations = false;
    }
  }

  toggleAnnotations() {
    this.showAnnotations = !this.showAnnotations;

    if (this.showAnnotations) {
      this.showSearchFieled = false;
    }
  }

  removeTag(annotation: PhotoAnnotationModel) {
    this.annotations = this.annotations.filter(item => item.target.id !== annotation.target.id);
  }

  onDrop(event: CdkDragEnd<PhotoAnnotationModel>, annotation: PhotoAnnotationModel) {
    const containerBounds = this.photoContainer.nativeElement.getBoundingClientRect();
    const elemBounds = event.source.element.nativeElement.getBoundingClientRect();
    const maxX = containerBounds.width / 2 - elemBounds.width / 2;
    const maxY = containerBounds.height / 2 - elemBounds.height / 2;

    let valueX = annotation.x + event.distance.x;
    let valueY = annotation.y + event.distance.y;

    if (valueX < -maxX) {
      valueX = -maxX;
    } else if (valueX > maxX) {
      valueX = maxX;
    }

    if (valueY < -maxY) {
      valueY = -maxY;
    } else if (valueY > maxY) {
      valueY = maxY;
    }

    annotation.x = Math.round(valueX);
    annotation.y = Math.round(valueY);
  }

  getPosition({ x, y }: PhotoAnnotationModel) {
    return { x, y };
  }

  wasChanged(): boolean {
    return (
      !this.form.pristine
      || this.isInputValueChanged
    );
  }

  deleteDraft(photoId: number) {
    this.store.dispatch(new PhotoActions.Delete(photoId));
    this.dialog.closeAll();
    if (+photoId === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }

  private readFile(file: File) {
    this.localPhotoLoading = true;
    this.photoFile = file;

    Utils.getImageData(file).then(({ url }) => {
      this.isInputValueChanged = true;
      this.form.get('photo').setValue(url);
      this.localPhotoLoading = false;
    });
  }
}
