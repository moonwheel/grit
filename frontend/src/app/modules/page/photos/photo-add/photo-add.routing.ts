import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { PhotoAddComponent } from './photo-add.component';
// Resolvers
import { PhotoFileResolver } from 'src/app/core/resolvers';
// Guards
import { ProfileGuard, CanDeactivateGuard, ViewerGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    component: PhotoAddComponent,
    resolve: { file: PhotoFileResolver },
    canActivate: [ProfileGuard, ViewerGuard],
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoAddRouting {}
