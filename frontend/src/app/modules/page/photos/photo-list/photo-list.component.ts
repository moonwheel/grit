import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { PhotoModel } from 'src/app/shared/models/content/photo.model';
import { PhotoState } from 'src/app/store/state/photo.state';
import { PhotoActions } from 'src/app/store/actions/photo.actions';
import { UserState } from 'src/app/store/state/user.state';
import { Utils } from 'src/app/shared/utils/utils';
import { VisibleState } from 'src/app/store/state/visible.state';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})

export class PhotoListComponent implements OnInit, OnDestroy {

  @Select(PhotoState.photos) photos$: Observable<PhotoModel[]>;
  @Select(PhotoState.total) total$: Observable<number>;
  @Select(PhotoState.loading) loading$: Observable<boolean>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(PhotoState.lazyLoading) lazyLoading$: Observable<boolean>;

  photoMapping = {};

  page = 1;
  pageSize = 30;
  filter = 'date';

  trackById = Utils.trackById;

  navigating = false;

  private destroy$ = new Subject();

  constructor(private store: Store,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new PhotoActions.Init(this.pageSize));

    this.translateService.stream('PAGES.PHOTO_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.photoMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  applyFilter(filter: string): void {
    this.filter = filter;
    this.store.dispatch(new PhotoActions.Init(this.pageSize, this.filter, 'desc'));
  }

  generatePhotoUrl(photo): string {
    const photoUrl = photo.thumb || photo.photo_photo || photo.photo;
    return `${photoUrl}`;
  }

  onScroll() {
    this.page++;
    this.store.dispatch(new PhotoActions.Load(this.page, this.pageSize, this.filter, 'asc', '', true));
  }

}
