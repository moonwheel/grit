import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { PhotoListComponent } from './photo-list.component';

const routes: Routes = [
  {
    path: '',
    component: PhotoListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoListRouting {}
