import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { PhotoListRouting } from './photo-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { PhotoListComponent } from './photo-list.component';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';


@NgModule({
  declarations: [
    PhotoListComponent,
  ],
  imports: [
    CommonModule,
    PhotoListRouting,
    SharedModule,
    ListViewLayout
  ]
})
export class PhotoListModule { }
