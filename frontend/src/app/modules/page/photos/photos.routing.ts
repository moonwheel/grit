import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./photo-list/photo-list.module').then(m => m.PhotoListModule) },
  { path: 'add', loadChildren: () => import('./photo-add/photo-add.module').then(m => m.PhotoAddModule) },
  { path: ':id', loadChildren: () => import('./photo-detail/photo-detail.module').then(m => m.PhotoDetailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotosRouting {
}
