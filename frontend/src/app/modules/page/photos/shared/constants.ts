export const PHOTO_FILTERS = [
  { name: 'Normal', filter: 'normal' },
  { name: 'Bangkok', filter: 'filter-aden' },
  { name: 'Berlin', filter: 'filter-clarendon' },
  { name: 'Dubai', filter: 'filter-juno' },
  { name: 'Dublin', filter: 'filter-ludwig' },
  { name: 'Havana', filter: 'filter-lark' },
  { name: 'London', filter: 'filter-gingham' },
  { name: 'Madrid', filter: 'filter-lofi' },
  { name: 'Miami', filter: 'filter-valencia' },
  { name: 'New York', filter: 'filter-mayfair' },
  { name: 'Oslo', filter: 'filter-inkwell' },
  { name: 'Paris', filter: 'filter-moon' },
  { name: 'Rio', filter: 'filter-slumber' },
  { name: 'Sydney', filter: 'filter-crema' },
  { name: 'Tokyo', filter: 'filter-nashville' },
  { name: 'Toronto', filter: 'filter-hudson' }

  // { name: 'Amaro', filter: 'filter-amaro' },
  // { name: 'Hefe', filter: 'filter-hefe' },
  // { name: 'Perpetua', filter: 'filter-perpetua' },
  // { name: 'Reyes', filter: 'filter-reyes' },
  // { name: 'Rise', filter: 'filter-rise' },
  // { name: 'Sierra', filter: 'filter-sierra' },
  // { name: 'Willow', filter: 'filter-willow' },
  // { name: 'X-Pro II', filter: 'filter-xpro-ii' }
];
