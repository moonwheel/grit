import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { PhotosRouting } from './photos.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    PhotosRouting,
    SharedModule,
  ]
})
export class PhotosModule { }
