import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { PhotoDetailComponent } from './photo-detail.component';
import { UploadingGuard, UploadingGuardStateToken } from 'src/app/core/guards';
import { PhotoState } from 'src/app/store/state/photo.state';

const routes: Routes = [
  {
    path: '',
    component: PhotoDetailComponent,
    canActivate: [ UploadingGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UploadingGuard,
    { provide: UploadingGuardStateToken, useValue: PhotoState },
  ]
})
export class PhotoDetailRouting {}
