import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { PhotoState } from 'src/app/store/state/photo.state';
import { Observable, of } from 'rxjs';
import { PhotoActions } from 'src/app/store/actions/photo.actions';
import { PhotoModel } from '../../../../shared/models/content/photo.model';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { LoadAnnotations } from '../../../../store/actions/annotation.actions';
import { switchMap, tap, switchMapTo } from 'rxjs/operators';

@Component({
  selector: 'app-photo-detail',
  templateUrl: './photo-detail.component.html',
  styleUrls: ['./photo-detail.component.scss'],
})

export class PhotoDetailComponent implements OnInit, OnDestroy {

  @Select(PhotoState.loading) loading$: Observable<boolean>;

  photo$: Observable<PhotoModel>;

  constructor(
    protected store: Store,
    protected route: ActivatedRoute,
    protected dialog: MatDialog,
    protected router: Router,
  ) {
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));

    this.photo$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(PhotoState.photo(id));

        this.store.dispatch(new LoadAnnotations(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new PhotoActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(PhotoState.photo(id))),
            tap(photo => {
              if (photo && !photo.viewed) {
                this.store.dispatch(new PhotoActions.View(id));
              }
            }),
          );
        } else {
          return this.store.select(PhotoState.photo(id));
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  onDelete() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
