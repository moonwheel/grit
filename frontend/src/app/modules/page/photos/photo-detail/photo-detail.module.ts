import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { PhotoDetailRouting } from './photo-detail.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { PhotoDetailComponent } from './photo-detail.component';


@NgModule({
  declarations: [
    PhotoDetailComponent,
  ],
  imports: [
    CommonModule,
    PhotoDetailRouting,
    SharedModule,
  ]
})
export class PhotoDetailModule { }
