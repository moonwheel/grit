import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { TabsComponent } from './tabs/tabs.component';

// Resolvers
import { UserPageResolver } from 'src/app/core/resolvers';

// Guards
import { AuthGuard, ProfileGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    component: TabsComponent,
    resolve: {
      page: UserPageResolver
    },
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        data: { preload: true }
      },
      {
        path: 'texts',
        loadChildren: () => import('./texts/texts.module').then(m => m.TextsModule)
      },
      {
        path: 'photos',
        loadChildren: () => import('./photos/photos.module').then(m => m.PhotosModule)
      },
      {
        path: 'videos',
        loadChildren: () => import('./videos/videos.module').then(m => m.VideosModule)
      },
      {
        path: 'blog',
        loadChildren: () => import('./articles/articles.module').then(m => m.ArticlesModule)
      },
      {
        path: 'shop',
        loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'services',
        loadChildren: () => import('./services/services.module').then(m => m.ServicesModule)
      },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ]
  },
  {
    path: 'following',
    loadChildren: () => import('../user/components/following/following.module').then(m => m.FollowingModule),
    canActivate: [AuthGuard, ProfileGuard]
  },
  {
    path: 'blocked',
    loadChildren: () => import('../user/components/blocked/blocked.module').then(m => m.BlockedModule),
    canActivate: [AuthGuard, ProfileGuard]
  },
  {
    path: 'saved',
    loadChildren: () => import('../user/components/bookmarks/bookmarks.module').then(m => m.BookmarksModule),
    canActivate: [AuthGuard, ProfileGuard]
  },
  {
    path: 'orders-bookings',
    loadChildren: () => import('../user/components/orders-bookings/orders-bookings.module').then(m => m.OrdersBookingsModule),
    canActivate: [AuthGuard, ProfileGuard]
  },
  {
    path: 'team',
    loadChildren: () => import('../user/components/team/team.module').then(m => m.TeamModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRouting { }
