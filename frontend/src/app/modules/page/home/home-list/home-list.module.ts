import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { HomeListRouting } from './home-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { HomeListComponent } from './home-list.component';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';


@NgModule({
  declarations: [
    HomeListComponent,
  ],
  imports: [
    CommonModule,
    HomeListRouting,
    SharedModule,
    ListViewLayout,
  ]
})
export class HomeListModule { }
