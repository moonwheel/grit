import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { HomeListComponent } from './home-list.component';

const routes: Routes = [
  {
    path: '',
    component: HomeListComponent,
    data: {
      preload: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeListRouting {}
