import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FollowersComponent } from 'src/app/modules/user/components/followers/followers.component';
import { Store, Select, Actions, ofActionSuccessful } from '@ngxs/store';
import { Observable, EMPTY, of, combineLatest } from 'rxjs';
import { switchMap, map, filter, takeUntil, take, switchMapTo } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';
import { environment } from 'src/environments/environment';
import { PhotoActions } from 'src/app/store/actions/photo.actions';
import { TextActions } from 'src/app/store/actions/text.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { VideoActions } from 'src/app/store/actions/video.actions';
import { ArticleActions } from 'src/app/store/actions/article.actions';
import { SendEmailVerification, ChangeAccount } from 'src/app/store/actions/auth.actions';
import {
  CommentListComponent,
  CommentListComponentData
} from '../../../../shared/components/actions/comments/comment-list/comment-list.component';
import { DateTime } from 'luxon';
import { BusinessState } from '../../../../store/state/business.state';
import { ServiceActions } from '../../../../store/actions/service.actions';
import { constants } from '../../../../core/constants/constants';
import { getDefaultWeekley } from '../../../../shared/constants';
import { AccountModel, AbstractEntityModel, FeedItemModel, ServiceModel, UserModel } from 'src/app/shared/models';
import { LikeListComponentData, LikeListComponent } from 'src/app/shared/components/actions/likes/like-list/like-list.component';
import { TextState } from 'src/app/store/state/text.state';
import { GetBusiness } from 'src/app/store/actions/business.actions';
import { FollowUser, UnfollowUser, LoadViewableUser, BlockUser } from 'src/app/store/actions/user.actions';
import { UnfollowDialogComponent } from 'src/app/shared/components/actions/follow/unfollow-dialog/unfollow-dialog.component';
import { BlockedDialogComponent } from 'src/app/shared/components/actions/blocked/blocked-dialog/blocked-dialog.component';
import { TimelineActions } from 'src/app/store/actions/timeline.actions';
import { TimelineState } from 'src/app/store/state/timeline.state';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { VisibleState } from 'src/app/store/state/visible.state';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home-list',
  templateUrl: './home-list.component.html',
  styleUrls: ['./home-list.component.scss'],
})
export class HomeListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {
  @Select(UserState.type) userType$: Observable<string>;

  @Select(UserState.viewableAccountUser) viewableAccountUser$: Observable<UserModel>;
  @Select(UserState.viewableAccount) viewableAccount$: Observable<AccountModel>;
  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;
  @Select(UserState.viewableAccountPageName) viewableAccountPageName$: Observable<string>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.viewableAccountDescription) viewableAccountDescription$: Observable<string>;
  @Select(UserState.viewableAccountIsBlocked) viewableAccountIsBlocked$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;

  @Select(UserState.loggedInAccountPageName) loggedInAccountPageName$: Observable<string>;

  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.isFolowing) isFolowing$: Observable<boolean>;
  @Select(UserState.followingRequest) followingRequest$: Observable<boolean>;
  @Select(UserState.followRequestsCount) followRequestsCount$: Observable<number>;
  @Select(UserState.followingStatus) followingStatus$: Observable<string>;
  @Select(UserState.isBlocked) isBlocked$: Observable<boolean>;
  @Select(UserState.isBlocking) isBlocking$: Observable<boolean>;
  @Select(UserState.isPrivate) isPrivate$: Observable<boolean>;

  @Select(BusinessState.businessAddress) businessAddress$: Observable<string>;
  @Select(BusinessState.businessPhone) businessPhone$: Observable<object>;
  @Select(BusinessState.legalNotice) legalNotice$: Observable<object>;
  @Select(BusinessState.businessPhoto) businessPhoto$: Observable<object>;
  @Select(BusinessState.description) businessDescription$: Observable<string>;
  @Select(BusinessState.bName) businessName$: Observable<string>;

  @Select(TimelineState.items) timeline$: Observable<any[]>;

  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  
  @Select(TimelineState.lazyLoading) lazyLoading$: Observable<boolean>;

  canEditSettings$ = combineLatest([
    this.store.select(UserState.isMyAccount),
    this.store.select(UserState.loggedInAccountRoleType),
  ]).pipe(
    map(([isMyAccount, loggedInAccountRoleType]) =>
      isMyAccount && (!loggedInAccountRoleType || loggedInAccountRoleType === 'admin')
    ),
  );

  loading$ = combineLatest([
    this.store.select(UserState.loading),
    this.store.select(TimelineState.loading),
  ]).pipe(
    map(([userLoading, timelineLoading]) => userLoading || timelineLoading)
  );

  total$ = of(Infinity);

  hourList;
  weekly = [];

  description = false;
  durations = [ ...constants.DURATIONS ];

  showHours = false;

  truncated = {};

  likeColor = '#4d4d4d';

  bookmarkColor = '#4d4d4d';

  environment = environment;

  defaultWeekly = [ ...getDefaultWeekley() ];

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  followersMapping = {};

  followingStatusList = {
    pending: 'PAGES.HOME_LIST.FOLLOWING_STATUS.PENDING',
    success: 'PAGES.HOME_LIST.FOLLOWING_STATUS.SUCCESS',
    approved: 'PAGES.HOME_LIST.FOLLOWING_STATUS.APPROVED',
    rejected: 'PAGES.HOME_LIST.FOLLOWING_STATUS.REJECTED',
  };

  currentUrl = '';
  navigating = false;

  constructor(
    private dialog: MatDialog,
    private store: Store,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.loadUser();

    this.currentUrl = environment.appUrl;

    this.actions$.pipe(
      ofActionSuccessful(LoadViewableUser),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.loadUser();
    });

    this.translateService.stream('PAGES.HOME_LIST.FOLLOWERS_MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.followersMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new TimelineActions.Load(this.page, this.pageSize));
    });

  }

  loadUser() {
    this.store.select(UserState.viewableAccount).pipe(
      filter(account => Boolean(account)),
      take(1),
      switchMap(account => {

        if (!account.isBlocked && !account.isBlocking) {
          this.store.dispatch(new TimelineActions.Load(1, this.pageSize));
        }

        if (account.business) {
          const business = this.store.selectSnapshot(BusinessState.business);
          if (business) {
            return of(business);
          } else {
            return this.store.dispatch(new GetBusiness(account.business.id));
          }
        } else {
          return EMPTY;
        }
      }),
      switchMapTo(this.store.select(BusinessState.business)),
      filter(business => Boolean(business)),
      takeUntil(this.destroy$),
    ).subscribe(business => {
      this.hourList = business.hours;
      this.hourList.forEach((day) => {
        this.addDailySchedule(day);
      });
    });
  }

  // Email Confirmation & Update Email

  resendConfirmation(): void {
    this.store.dispatch(new SendEmailVerification()).subscribe();
  }

  updateEmail(): void {
    const loggedInAccount = this.store.selectSnapshot(UserState.loggedInAccount);

    if (!loggedInAccount.business) {
      this.router.navigate([`../settings`], {
        queryParams: { editEmail: true },
        relativeTo: this.route
      });
    } else {
      const businesses = this.store.selectSnapshot(BusinessState.businessList);
      const personAccount = businesses.find(account => !account.business);

      this.store.dispatch(new ChangeAccount(personAccount.id)).subscribe(async () => {
        await this.router.navigate([`/${personAccount.person.pageName}/home`]);
        await this.router.navigate([`/${personAccount.person.pageName}/settings`], {
          queryParams: { editEmail: true },
          relativeTo: this.route
        });
      });
    }
  }

  // User

  follow() {
    const account = this.store.selectSnapshot(UserState.viewableAccount);

    if (account.following) {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    } else if (account.followingRequest) {
      this.store.dispatch(new UnfollowUser(account.id));
    } else {
      this.store.dispatch(new FollowUser(account.id));
    }
  }

  followers() {
    this.dialog.open(FollowersComponent);
  }

  legalNotice(templateRef) {
    this.dialog.open(templateRef);
  }

  blockUser(): void {
    const account = this.store.selectSnapshot(UserState.viewableAccount);
    this.dialog.open(BlockedDialogComponent, {
      data: {
        account,
        type: account.isBlocked ? 'unblock' : 'block',
      },
    });
  }

  reportUser(): void {

  }

  // Opening Hours

  findDuration(time) {
    return this.durations.filter(item => item.value === time)[0].name;
  }

  getDateValue(dateValue) {
    return DateTime.fromISO(dateValue).toFormat('dd MMM y, HH:mm');
  }

  addDailySchedule(day) {
    const weekly = this.defaultWeekly.slice();
    const currentDay = weekly[day.day.id - 1];
    if (day.from && (!currentDay.from_first || !currentDay.from_first[0])) {
      currentDay.from_first = day.from.substring(0, 5);
    }
    if (day.to && (!currentDay.to_first || !currentDay.to_first[0])) {
      currentDay.to_first = day.to.substring(0, 5);
    }
    if (day.id && (currentDay.from_first === day.from.substring(0, 5))) {
      currentDay.firstPartId = day.id;
    }
    if (day.from && currentDay.from_first !== day.from.substring(0, 5)) {
      if (day.from && currentDay.from_first !== day.from.substring(0, 5)) {
        currentDay.from_second = day.from.substring(0, 5);
      }
      if (day.to && currentDay.to_first !== day.to.substring(0, 5)) {
        currentDay.to_second = day.to.substring(0, 5);
      }
      if (day.id && (day.from && currentDay.from_first !== day.from.substring(0, 5))) {
        currentDay.secondPartId = day.id;
      }
    }
    this.weekly = weekly;
  }

  // Helpers

  getFollowButtonStatus(status: string) {
    return this.followingStatusList[status] || this.followingStatusList.rejected;
  }

  // Text

  likeText(id: number) {
    this.store.dispatch(new TextActions.Like(id));
  }

  showTextLikes(id: number) {
    const data: LikeListComponentData = {
      id,
      State: TextState,
      Actions: TextActions,
    };
    this.dialog.open(LikeListComponent, { data });
  }

  showTextComments(id: number) {
    const data: CommentListComponentData = {
      id,
      State: TextState,
      Actions: TextActions,
    };
    this.dialog.open(CommentListComponent, {
      data,
      panelClass: 'dialog-with-message-input-panel',
    });
  }

  bookmarkText() {

  }

  editText(textId) {
    this.router.navigate([`/page/edit/${textId}`]);
  }

  deleteTextDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteText(id: number) {
    this.store.dispatch(new TextActions.Delete(id));
  }

  // Edit & Delete Content

  deletePhoto(id) {
    this.store.dispatch(new PhotoActions.Delete(id));
  }

  deletePhotoDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteVideo(id) {
    this.store.dispatch(new VideoActions.Delete(id));
  }

  deleteVideoDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteArticle(id) {
    this.store.dispatch(new ArticleActions.Delete(id));
  }

  deleteArticleDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteService(id) {
    this.store.dispatch(new ServiceActions.Delete(id));
  }

  deleteServiceDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  // Report

  reportDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  getRouterLink(postData: FeedItemModel) {
    switch (postData.tableName) {
      case 'photo': {
        return '../photos';
      }
      case 'video': {
        return '../videos';
      }
      case 'article': {
        return '../blog';
      }
      case 'service': {
        return '../services';
      }
      case 'text': {
        return '../texts';
      }
      default: {
        return null;
      }
    }
  }

  goToPost(postData: FeedItemModel) {
    const url = `${this.getRouterLink(postData)}/${postData.id}`;
    this.router.navigate([url], { relativeTo: this.route });
  }

  goToEditPost(postData: FeedItemModel) {
    const url = `${this.getRouterLink(postData)}/add`;
    this.router.navigate([url], { relativeTo: this.route, queryParams: { id: postData.id } });
  }

  generateCover(data): string {
    return data.cover ? `${data.cover}` : 'assets/images/photo-placeholder.png';
  }

  trackByHash(index: number, item: AbstractEntityModel | FeedItemModel) {
    return `${item.tableName}-${item.id}`;
  }

  inactiveService(service: ServiceModel) {
    const payload = { id: service.id, service: { active: !service.active } };
    this.store.dispatch(new ServiceActions.ChangeActiveState(payload));
  }
}
