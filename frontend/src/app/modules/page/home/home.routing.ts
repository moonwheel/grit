import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { ProfileGuard, ViewerGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home-list/home-list.module').then(m => m.HomeListModule),
    data: { preload: true }
  },
  {
    path: 'add',
    loadChildren: () => import('./home-add/home-add.module').then(m => m.HomeAddModule),
    canActivate: [ProfileGuard, ViewerGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRouting {
}
