import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { HomeRouting } from './home.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    HomeRouting,
    SharedModule,
  ]
})
export class HomeModule { }
