import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { HomeAddComponent } from './home-add.component';

const routes: Routes = [
  {
    path: '',
    component: HomeAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeAddRouting {}
