import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { HomeAddRouting } from './home-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { HomeAddComponent } from './home-add.component';


@NgModule({
  declarations: [
    HomeAddComponent,
  ],
  imports: [
    CommonModule,
    HomeAddRouting,
    SharedModule,
  ]
})
export class HomeAddModule { }
