import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { UserState } from '../../../../store/state/user.state';
import { NavigationService } from 'src/app/shared/services/navigation.service';

@Component({
  selector: 'app-home-add',
  templateUrl: './home-add.component.html',
  styleUrls: ['./home-add.component.scss'],
})

export class HomeAddComponent implements OnInit, OnDestroy {

  @Select(UserState.loggedInAccountPhoto) loggedInAccountPhoto$: Observable<string>;

  constructor(
    private navigationService: NavigationService,
    private store: Store,
  ) {
  }

  public ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleSearch: false, isVisibleUserTabs: false }));
  }

  public ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: true, isVisibleSearch: true, isVisibleUserTabs: true }));
  }

  public back(): void {
    this.navigationService.back();
  }
}
