import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { CanDeactivateGuard, ProfileGuard } from '../../../core/guards';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', loadChildren: () => import('./article-list/article-list.module').then(m => m.ArticleListModule) },
      { path: 'add', loadChildren: () => import('./article-add/article-add.module').then(m => m.ArticleAddModule) },
      { path: ':id', loadChildren: () => import('./article-detail/article-detail.module').then(m => m.ArticleDetailModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ArticlesRouting {}
