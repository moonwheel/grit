import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { ArticleActions } from 'src/app/store/actions/article.actions';
import { ArticleState } from 'src/app/store/state/article.state';
import { Observable } from 'rxjs';
import { ArticleModel } from 'src/app/shared/models/article.model';
import { UserState } from 'src/app/store/state/user.state';
import { MatDialog } from '@angular/material/dialog';
import { AccountModel } from 'src/app/shared/models/user/account.model';
import { Utils } from 'src/app/shared/utils/utils';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { VisibleState } from 'src/app/store/state/visible.state';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})

export class ArticleListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ArticleState.articles) articles$: Observable<ArticleModel[]>;
  @Select(ArticleState.total) total$: Observable<number>;
  @Select(ArticleState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountFullName) viewableAccountFullName$: Observable<string>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.isMyAccount) isMyAccount$: Observable<boolean>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(ArticleState.lazyLoading) lazyLoading$: Observable<boolean>

  articleMapping = {};

  page = 1;
  pageSize = 15;
  itemSize = 120;
  filter = 'date';
  searchValue = '';
  navigating = false;

  currentUrl = '';

  constructor(private store: Store,
              private dialog: MatDialog,
              private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new ArticleActions.Init(this.pageSize));

    this.translateService.stream('PAGES.ARTICLE_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.articleMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new ArticleActions.Load(this.page, this.pageSize, this.filter, 'asc', '',true));
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  // Filter
  applyFilter(filterName: string): void {
    this.filter = filterName;
    this.store.dispatch(new ArticleActions.Init(this.pageSize, this.filter, 'desc'));
  }

  // Actions

  deleteDialog(tempRef): void {
    this.dialog.open(tempRef);
  }

  deleteArticle(id) {
    this.store.dispatch(new ArticleActions.Delete(id));
  }

  reportDialog(tempRef) {
    this.dialog.open(tempRef);
  }

  getAccountPageName(account: AccountModel): string {
    return Utils.getAccountPageName(account);
  }

}
