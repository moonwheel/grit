import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ArticleListRouting } from './article-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListViewLayout } from '../../components/layouts/list-view-layout/list-view-layout.module';

// Components
import { ArticleListComponent } from './article-list.component';



@NgModule({
  declarations: [
    ArticleListComponent,
  ],
  imports: [
    CommonModule,
    ArticleListRouting,
    SharedModule,
    ListViewLayout,
  ]
})
export class ArticleListModule { }
