import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ArticleDetailRouting } from './article-detail.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ArticleDetailComponent } from './article-detail.component';


@NgModule({
  declarations: [
    ArticleDetailComponent,
  ],
  imports: [
    CommonModule,
    ArticleDetailRouting,
    SharedModule,
  ]
})
export class ArticleDetailModule { }
