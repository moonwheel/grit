import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { ArticleDetailComponent } from './article-detail.component';

import { UploadingGuard, UploadingGuardStateToken } from 'src/app/core/guards';
import { ArticleState } from 'src/app/store/state/article.state';

const routes: Routes = [
  {
    path: '',
    component: ArticleDetailComponent,
    canActivate: [ UploadingGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UploadingGuard,
    { provide: UploadingGuardStateToken, useValue: ArticleState }
  ]
})
export class ArticleDetailRouting {}
