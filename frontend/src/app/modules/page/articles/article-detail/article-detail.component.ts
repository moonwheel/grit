import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { ArticleState } from 'src/app/store/state/article.state';
import { Observable } from 'rxjs';
import { ArticleActions } from 'src/app/store/actions/article.actions';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ArticleModel } from 'src/app/shared/models';
import { switchMap, switchMapTo, tap } from 'rxjs/operators';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss'],
})

export class ArticleDetailComponent implements OnInit, OnDestroy {

  @Select(ArticleState.loading) loading$: Observable<boolean>;

  article$: Observable<ArticleModel>;

  templateDialogRef: MatDialogRef<{}, any>;

  constructor(protected store: Store,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: false }));

    this.article$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params.id;
        const item = this.store.selectSnapshot(ArticleState.article(id));

        if (!item || !item.isDetailed) {
          return this.store.dispatch(new ArticleActions.GetOne(id)).pipe(
            switchMapTo(this.store.select(ArticleState.article(id))),
            tap(article => {
              if (article && !article.viewed) {
                this.store.dispatch(new ArticleActions.View(id));
              }
            }),
          );
        } else {
          return this.store.select(ArticleState.article(id));
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true }));
  }

  onDelete() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
