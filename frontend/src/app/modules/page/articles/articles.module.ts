import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { ArticlesRouting } from './articles.routing';
import { SharedModule } from '../../../shared/shared.module';
import { AddViewLayout } from '../components/layouts/add-view-layout/add-view-layout.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ArticlesRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class ArticlesModule { }
