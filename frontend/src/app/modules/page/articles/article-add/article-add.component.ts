import 'quill-mention';
import { Component, OnDestroy, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ArticleState } from 'src/app/store/state/article.state';
import { Select, Store } from '@ngxs/store';
import { Observable, of, EMPTY, Subject } from 'rxjs';
import { ArticleModel } from 'src/app/shared/models/article.model';
import { UserState } from 'src/app/store/state/user.state';
import { ArticleActions } from 'src/app/store/actions/article.actions';
import { Utils } from 'src/app/shared/utils/utils';
import {
  map,
  catchError,
  switchMap,
  takeUntil,
  switchMapTo,
  debounceTime,
  distinctUntilChanged,
  take,
} from 'rxjs/operators';
import { ToggleVisible } from '../../../../store/actions/visible.actions';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from '../../../../shared/components/abstract-add-page/abstract-add-page.component';
import { QuillModules, QuillEditorComponent } from 'ngx-quill';
import { constants } from 'src/app/core/constants/constants';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/shared/services';
import { AccountModel, MentionModel } from 'src/app/shared/models';
import Quill from 'quill';

const icons = Quill.import('ui/icons');

icons.bold = '<i class="material-icons">format_bold</i>';
icons.italic = '<i class="material-icons">format_italic</i>';
icons.underline = '<i class="material-icons">format_underline</i>';
icons.header[1] = '<i class="material-icons">title</i>';
icons.list.ordered = '<i class="material-icons">format_list_numbered</i>';
icons.list.bullet = '<i class="material-icons">format_list_bulleted</i>';
icons.image = '<i class="material-icons">insert_photo</i>';
icons.link = '<i class="material-icons">link</i>';


@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.scss'],
})
export class ArticleAddComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select(ArticleState.articles) articles$: Observable<ArticleModel[]>;
  @Select(ArticleState.drafts) drafts$: Observable<ArticleModel[]>;
  @Select(ArticleState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccountPhoto) viewableAccountPhoto$: Observable<string>;

  @ViewChild('fileInput', { static: true }) fileInput: ElementRef<HTMLInputElement>;
  @ViewChild(QuillEditorComponent, { static: true }) editor: QuillEditorComponent;

  form: FormGroup;

  oldCover: string;

  isSubmitted = false;
  isEditFlow = false;
  isDraft = false;

  isInputValueChanged = false;
  invalidCover = false;
  photoLoading = false;
  mentions = [];

  articleId: number;

  categories = [ ...constants.MEDIA_CATEGORIES ];

  quillConfig: QuillModules = {
    toolbar: {
      container: '.quill-toolbar',
      handlers: {
        image: this.editorImageHandler.bind(this),
      }
    },
    mention: {
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
      onSelect: (item, insertItem) => {
        const editor = this.editor.quillEditor;
        insertItem(item);
        // necessary because quill-mention triggers changes as 'api' instead of 'user'
        editor.insertText(editor.getLength() - 1, '', 'user');

        this.onMentionSelect(item.id, item.value);
      },
      source: async (searchTerm, renderList) => {
        this.searchSubject$.next(searchTerm);

        this.searchEmitter$.pipe(
          take(1),
          takeUntil(this.searchSubject$),
          takeUntil(this.destroy$),
        ).subscribe(users => {
          const values = users.map(Utils.extendAccountWithBaseInfo).map(item => ({
            id: item.id,
            value: item.name,
          }));
          renderList(values, searchTerm);
        });
      },
    }
  };

  quillEditorRef: any;

  articleDraftsMapping = {};

  trackById = Utils.trackById;

  photosToUpload: { url: string, file: File }[] = [];

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  searchSubject$ = new Subject<string>();
  searchEmitter$ = new Subject<AccountModel[]>();

  private coverFile: File;

  get cover(): string {
    const control = this.form.get('cover');
    return control && control.value;
  }

  constructor(
    protected formBuilder: FormBuilder,
    protected store: Store,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialog: MatDialog,
    protected navigationService: NavigationService,
    protected zone: NgZone,
    protected translateService: TranslateService,
    protected userService: UserService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({
      isVisibleMain: false,
      isVisibleSearch: false,
      isVisibleUserTabs: false,
      isPullToRefreshEnabled: false,
    }));
    this.store.dispatch(new ArticleActions.LoadDrafts());

    this.form = this.formBuilder.group({
      id: null,
      cover: [null, Validators.required],
      title: ['', Validators.required],
      category: ['', Validators.required],
      text: ['', Validators.required],
      location: '',
      business_address: null,
      created: new Date()
    });

    this.setFormData();

    this.translateService.stream('GENERAL.DRAFTS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.articleDraftsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });

    this.searchSubject$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(searchTerm => {
        const exceptIds = this.mentions.map(item => item.target.id);
        return this.userService.search(searchTerm, 1, 20, exceptIds);
      }),
      takeUntil(this.destroy$),
    ).subscribe(users => this.searchEmitter$.next(users));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    this.searchSubject$.complete();
    this.searchEmitter$.complete();

    this.store.dispatch(new ToggleVisible({
      isVisibleMain: true,
      isVisibleSearch: true,
      isVisibleUserTabs: true,
      isPullToRefreshEnabled: true,
    }));
  }

  setFormData(): void {
    this.route.queryParams.pipe(
      switchMap(params => {
        this.isEditFlow = 'id' in params;

        this.articleId = +params.id;
        this.oldCover = null;
        this.form.reset();

        if (this.isEditFlow) {
          this.setEditViewMode(true);
          const itemInStore = this.store.selectSnapshot(ArticleState.article(this.articleId));

          if (!itemInStore || !itemInStore.isDetailed) {
            return this.store.dispatch(new ArticleActions.GetOne(this.articleId)).pipe(
              switchMapTo(this.store.selectOnce(ArticleState.article(this.articleId)))
            );
          } else {
            return of(itemInStore);
          }
        } else {
          return EMPTY;
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(article => {
      this.oldCover = article.cover;
      this.mentions = article.mentions || [];
      this.isDraft = article.draft;
      this.form.patchValue(article);
      this.form.get('business_address').patchValue(article.entity_business_address);
      this.setEditReadyState(true);
    });
  }

  getEditorInstance(editorInstance: any) {
    this.quillEditorRef = editorInstance;
    this.quillEditorRef.root.dataset.placeholder = '';
  }

  onEditorContentChanged(event) {
    const value = this.form.get('text').value;
    if (value !== event.html) {
      this.form.get('text').patchValue(event.html);
    }

    if (event.html !== null) {
      this.mentions = this.mentions.filter(mention => {
        return mention.target && event.html.match(`data-id="${mention.target.id}"`);
      });
    }
  }

  editorImageHandler() {
    this.zone.runOutsideAngular(() => {
      const input = this.fileInput.nativeElement;
      input.onchange = async () => {
        const { file, url } = await Utils.getImageData(input.files[0]);
        const range = this.quillEditorRef.getSelection();
        const img = `<img src="${url}" />`;
        this.quillEditorRef.clipboard.dangerouslyPasteHTML(range.index, img);
        input.onchange = null;
        input.value = null;

        this.photosToUpload.push({ file, url });
      };

      input.click();
    });
  }

  selectCover(event: Event): void {
    const target = event.target as HTMLInputElement;
    const file = target.files && target.files[0];
    if (file) {
      this.photoLoading = true;
      this.coverFile = file;
      target.value = null;

      Utils.getImageData(file).then(({ url }) => {
        this.form.get('cover').setValue(url);
        this.isInputValueChanged = true;
        this.photoLoading = false;
      });
    }
  }

  removeCover(event: Event): void {
    event.stopPropagation();
    this.isInputValueChanged = true;
    this.form.get('cover').setValue(null);
  }

  addLocation(): void {

  }

  deleteDraft(article: ArticleModel): void {
    this.store.dispatch(new ArticleActions.Delete(article.id));
    this.dialog.closeAll();
    if (+article.id === +this.form.get('id').value) {
      this.canImmediatelyDeactivate = true;
      this.router.navigate(['../add'], { relativeTo: this.route });
    }
  }

  save(isDraft = false): Observable<boolean> {

    if (this.form.valid || isDraft) {
      this.isSubmitted = true;

      const article = this.form.value;

      article.draft = isDraft;
      article.text = this.trimText(article.text);
      article.id = this.articleId ? this.articleId : null;

      const transformedArticle = this.transformDataArticle(article);

      delete transformedArticle.cover;

      let observable: Observable<any>;

      const files = [];

      if (this.coverFile) {
        files.push({ file: this.coverFile, uniqueId: 0 });
      }

      this.photosToUpload.forEach((item, index) => {
        const uniqueId = index + 1;
        files.push({ file: item.file, uniqueId });
        transformedArticle.text = article.text.replace(item.url, `{{${uniqueId}}}`);
      });

      transformedArticle.mentions = this.mentions;

      if (this.isEditFlow) {
        observable = this.store.dispatch(new ArticleActions.Edit(transformedArticle, files));
      } else {
        observable = this.store.dispatch(new ArticleActions.Add(transformedArticle, files));
      }

      return observable.pipe(
        map(() => this.canImmediatelyDeactivate = true),
        catchError(() => of(false)),
      );
    } else {
      this.invalidCover = true;
      Utils.touchForm(this.form);

      return of(false);
    }
  }

  wasChanged(): boolean {
    return (
      !this.form.pristine
      || this.isInputValueChanged
    );
  }

  onMentionSelect(accountId: number, mentionText: string) {
    mentionText = `@${mentionText}`;

    const found = this.mentions.find(item => +item.target.id === +accountId);

    if (!found) {
      const newMention: MentionModel = {
        id: null,
        target: { id: accountId } as any,
        mention_text: mentionText,
      };

      this.mentions.push(newMention);
    }
  }

  private transformDataArticle(article: ArticleModel): any {
    return Object.entries(article).reduce((accum, itemArticle) => {
      const [key, value] = itemArticle;
      const shortKey = key.indexOf('_') !== -1 ? key.split('_')[1] : key;
      accum[shortKey] = value;
      return accum;
    }, {});
  }

  private trimText(text: string): string {
    if (!text) {
      return text;
    }

    text = text.replace(/^(<p><br><\/p>)*/gm, '');
    text = text.replace(/(<p><br><\/p>)*$/gm, '');

    return text;
  }
}
