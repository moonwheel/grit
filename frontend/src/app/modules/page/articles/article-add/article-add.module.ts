import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { ArticleAddRouting } from './article-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ArticleAddComponent } from './article-add.component';
import { AddViewLayout } from '../../components/layouts/add-view-layout/add-view-layout.module';


@NgModule({
  declarations: [
    ArticleAddComponent,
  ],
  imports: [
    CommonModule,
    ArticleAddRouting,
    SharedModule,
    AddViewLayout,
  ]
})
export class ArticleAddModule { }
