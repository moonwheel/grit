import { NgModule } from '@angular/core';

// Components
import { LoginComponent } from './login.component';

// Modules
import { LoginRouting } from './login.routing';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    SharedModule,
    LoginRouting,
  ]
})
export class LoginModule { }
