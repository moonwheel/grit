import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { Login, VerifyPasswordWithoutAuth, SetAuthLoading } from 'src/app/store/actions/auth.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AuthState } from 'src/app/store/state/auth.state';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  @Select(AuthState.loading) loading$: Observable<boolean>;

  loginForm: FormGroup;
  emailForm: FormGroup;
  registerForm: FormGroup;

  password = false;

  mailDialogRef: MatDialogRef<{}, any>;

  private destroy$ = new Subject();

  constructor(
    private navigationService: NavigationService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private store: Store,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.emailForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  mailDialog(templateRef: TemplateRef<any>) {
    const { email } = this.loginForm.value;

    if (email) {
      this.emailForm.get('email').setValue(email);
    }

    this.mailDialogRef = this.dialog.open(templateRef);
  }

  showPassword(event: Event) {
    this.password = !this.password;
    event.stopPropagation();
  }

  login() {
    if (!this.loginForm.valid) {
      Utils.touchForm(this.loginForm);
    } else {
      this.store.dispatch(new SetAuthLoading(true));
      this.store.dispatch(new Login(this.loginForm.value)).subscribe(
        async () => {
          await this.router.navigate(['/feed']);
          this.store.dispatch(new SetAuthLoading(false));
        },
        error => {
          this.store.dispatch(new SetAuthLoading(false));
          this.loginForm.setErrors({ badLogging: true });
        }
      );
    }
  }

  verifyPassword() {
    if (!this.emailForm.valid) {
      Utils.touchForm(this.emailForm);
    } else {
      this.mailDialogRef.close();
      this.store.dispatch(new VerifyPasswordWithoutAuth(this.emailForm.value));
    }
  }

  back() {
    this.navigationService.back();
  }

}
