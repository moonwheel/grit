import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { ErrorRouting } from './error.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { ErrorComponent } from './error.component';

@NgModule({
  declarations: [
    ErrorComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ErrorRouting,
  ]
})
export class ErrorModule { }
