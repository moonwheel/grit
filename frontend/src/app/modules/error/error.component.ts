import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})

export class ErrorComponent implements OnInit {

  error$: Observable<any>;

  constructor(private navigationService: NavigationService,
              private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.error$ = this.translateService.stream('PAGES.ERROR').pipe(
      map((res: any) => ({
        title: res.TITLE,
        text: res.TEXT,
      }))
    );
  }

  back() {
    this.navigationService.back();
  }

}
