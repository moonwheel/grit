import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';

import { GetWallet } from 'src/app/store/actions/payment.actions';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'order-booking-tabs',
  templateUrl: './order-booking-tabs.component.html',
  styleUrls: ['./order-booking-tabs.component.scss']
})

export class OrderBookingTabsComponent implements OnInit {

  @Select(PaymentsState.balance)
  balance$: Observable<any>;

  constructor(
    private store: Store,
  ) { }

  ngOnInit() {
    this.store.dispatch(new GetWallet());
  }
}
