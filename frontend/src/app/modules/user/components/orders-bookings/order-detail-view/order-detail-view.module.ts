import { NgModule } from "@angular/core";
import { OrderDetailViewRouting } from "./order-detail-view.routing";
import { SharedModule } from "../../../../../shared/shared.module";
import { CommonModule } from "@angular/common";
import { OrderDetailComponent } from "./order-detail.component";

@NgModule({
    declarations: [OrderDetailComponent],
    imports: [OrderDetailViewRouting, SharedModule, CommonModule]
})
export class OrderDetailModule {}