import { Component, OnInit } from "@angular/core";
import { Store } from "@ngxs/store";
import { OrderState, SingleOrderEntity } from "../../../../../store/state/order.state";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";
import { GetOneSale, GetOnePurchase } from "../../../../../store/actions/order.actions";
import { OrderService } from "../../../../../shared/services/order.service";

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.component.html'
})
export class OrderDetailComponent implements OnInit {
    loading$ = new BehaviorSubject(true);

    orderType = ''
    pageType = '';
    order$ = new BehaviorSubject(null);

    constructor(private store: Store, private route: ActivatedRoute,private orderService: OrderService, private router: Router) {

    }

    ngOnInit() {
        this.getRouteData().subscribe(({orderType, type}) => {
            this.orderType = orderType;
            this.pageType = type;

            this.getOrderId().subscribe(async (id) => {
                this.fetchOrder(id).subscribe(() => {
                    this.store.select(OrderState.selectOrder(orderType, id)).subscribe((order: SingleOrderEntity) => {
                        if(!order.order && !order.loading) {
                            return this.router.navigateByUrl('/404');
                        }

                        this.loading$.next(order.loading);
                        this.order$.next(order.order);
                    })
                })
            })
            
        })
    }

    fetchOrder = (id: number): Observable<any> => {
        if(this.pageType === 'purchases') {
            return this.store.dispatch(new GetOnePurchase(String(id), this.orderType as any))
        }

        if(this.pageType === 'sales') {
            return this.store.dispatch(new GetOneSale(String(id), this.orderType as any));
        }

        throw new Error('incorrect page type')
    }

    private getOrderId = () => {
        return this.route.params.pipe(map(params => {
            return params['id'];
        }));
    }

    private getRouteData = (): Observable<{orderType: string, type: string}> => {
        return this.route.data.pipe(map(data => data as any))
    }
}