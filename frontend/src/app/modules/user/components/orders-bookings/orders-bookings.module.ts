import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { OrdersBookingsRouting } from './orders-bookings.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { OrderBookingTabsComponent } from './order-booking-tabs/order-booking-tabs.component';

@NgModule({
  declarations: [
    OrderBookingTabsComponent,
  ],
  imports: [
    CommonModule,
    OrdersBookingsRouting,
    SharedModule,
  ],
  exports: [
    OrderBookingTabsComponent,
  ],
})

export class OrdersBookingsModule { }
