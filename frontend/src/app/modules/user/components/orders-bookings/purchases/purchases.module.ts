import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PurchasesRouting } from './purchases.routing';

// Components
import { PurchasesComponent } from './purchases.component';
import { OrderDetailModule } from '../order-detail-view/order-detail-view.module';


@NgModule({
  declarations: [
    PurchasesComponent,
  ],
  imports: [
    CommonModule,
    PurchasesRouting,
    SharedModule,
    OrderDetailModule,
  ]
})

export class PurchasesModule { }
