import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchasesComponent } from './purchases.component';
import { OrderDetailComponent } from '../order-detail-view/order-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PurchasesComponent,
  },
  {
    path: 'product/:id',
    component: OrderDetailComponent,
    data: {
      orderType: 'product',
      type: 'purchases'
    }
  },
  {
    path: 'service/:id',
    component: OrderDetailComponent,
    data: {
      orderType: 'service',
      type: 'purchases'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PurchasesRouting {}
