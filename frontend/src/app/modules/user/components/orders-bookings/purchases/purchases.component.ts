import {
  Component,
  OnInit,
  QueryList,
  ViewChildren,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  Renderer2,
} from '@angular/core';
import { NgModel } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { Select, Store } from '@ngxs/store';
import { Observable, EMPTY, of } from 'rxjs';
import { switchMap, skip, debounceTime, takeUntil } from 'rxjs/operators';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { OrderState } from 'src/app/store/state/order.state';
import { InitPurchases, LoadPurchases } from 'src/app/store/actions/order.actions';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { ScrollObservableService } from 'src/app/shared/services/scroll-observable.service';
import { ResultDialogComponent } from 'src/app/shared/components/result-dialog/result-dialog.component';
import { UserState } from 'src/app/store/state/user.state';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss'],
})

export class PurchasesComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(UserState.loggedInAccountVerification)
  loggedInAccountVerification$: Observable<any>;

  @Select(PaymentsState.balance)
  balance$: Observable<any>;

  @Select(OrderState.purchases)
  purchases$: Observable<any>;

  @Select(OrderState.totalItems)
  totalItems$: Observable<number>;

  @Select(OrderState.totalPurchases)
  total$: Observable<number>;

  @Select(OrderState.loading)
  loading$: Observable<boolean>;

  @Select(OrderState.lazyLoading)
  lazyLoading$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel })
  searchInput: QueryList<NgModel>;

  @ViewChild('searchInputElement')
  searchInputElement: ElementRef;

  @ViewChild('tabsElement')
  tabsElement: ElementRef;

  ordersBookingsMapping = {};

  header = true;
  search = false;

  page = 1;
  pageSize = 15;
  filter = 'all';

  searchValue = '';

  constructor(
    private store: Store,
    private scrollService: ScrollObservableService,
    private dialog: MatDialog,
    private elRef: ElementRef,
    private renderer: Renderer2,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new InitPurchases());

    this.translateService.stream('PAGES.PURCHASES.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.ordersBookingsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  
  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new LoadPurchases(this.page, this.pageSize, this.filter, this.searchValue, true));
    });

    const stickyTabs = this.tabsElement.nativeElement.offsetTop;

    this.scrollService.scrolled$.subscribe(event => {
      if (event.target.scrollTop >= stickyTabs) {
        this.renderer.addClass(this.tabsElement.nativeElement, 'fixed');
      } else {
        this.renderer.removeClass(this.tabsElement.nativeElement, 'fixed');
      }
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new LoadPurchases(this.page, this.pageSize, this.filter, this.searchValue, true));
    });
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new LoadPurchases(this.page, this.pageSize, this.filter, this.searchValue, true));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  applyFilter(type: string) {
    this.filter = type;
    this.page = 1;
    this.store.dispatch(new LoadPurchases(this.page, this.pageSize, this.filter, this.searchValue));
  }

  openKycDialog() {
    this.translateService.get('INFO_MESSAGES.WALLET_VERIFICATION_SUCCESSFUL').subscribe(message => {
      this.dialog.open(ResultDialogComponent, { data: { message } });
    });
  }

  showVariantData(data: any) {
    if (!data) {
      return '';
    }
    return [data.size, data.color, data.flavor, data.material].filter(Boolean).join(', ');
  }

  showAddress(data: any) {
    if (!data) {
      return '';
    }
    return [data.street, data.zip, data.city].filter(Boolean).join(', ');
  }
}
