import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesComponent } from './sales.component';
import { OrderDetailComponent } from '../order-detail-view/order-detail.component';

const routes: Routes = [
  {
    path: '',
    component: SalesComponent,
  },
  {
    path: 'product/:id',
    component: OrderDetailComponent,
    data: {
      orderType: 'product',
      type: 'sales'
    }
  },
  {
    path: 'service/:id',
    component: OrderDetailComponent,
    data: {
      orderType: 'service',
      type: 'sales'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SalesRouting {}
