import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalesRouting } from './sales.routing';

// Components
import { SalesComponent } from './sales.component';


@NgModule({
  declarations: [
    SalesComponent,
  ],
  imports: [
    CommonModule,
    SalesRouting,
    SharedModule,
  ]
})

export class SalesModule { }
