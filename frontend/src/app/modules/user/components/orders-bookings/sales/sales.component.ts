import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ViewChildren, QueryList, ElementRef, Renderer2 } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { LoadSales, InitSales } from 'src/app/store/actions/order.actions';
import { OrderState } from 'src/app/store/state/order.state';
import { Observable, of, EMPTY } from 'rxjs';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { switchMap, debounceTime, skip, takeUntil } from 'rxjs/operators';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { ScrollObservableService } from 'src/app/shared/services/scroll-observable.service';
import { UserState } from 'src/app/store/state/user.state';
import { TranslateService } from '@ngx-translate/core';
import { GetWallet } from 'src/app/store/actions/payment.actions';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})

export class SalesComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(UserState.loggedInAccountVerification)
  loggedInAccountVerification$: Observable<any>;

  @Select(PaymentsState.balance)
  balance$: Observable<any>;

  @Select(OrderState.sales)
  sales$: Observable<any>;

  @Select(OrderState.totalItems)
  total$: Observable<number>;

  @Select(OrderState.totalItems)
  totalItems$: Observable<number>;

  @Select(OrderState.loading)
  loading$: Observable<boolean>;

  @Select(OrderState.lazyLoading)
  lazyLoading$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel })
  searchInput: QueryList<NgModel>;

  @ViewChild('searchInputElement')
  searchInputElement: ElementRef;

  @ViewChild('tabsElement')
  tabsElement: ElementRef;

  salesMapping = {};

  header = true;
  search = false;

  page = 1;
  pageSize = 15;
  filter = 'all';

  searchValue = '';

  constructor(
    private location: Location,
    private scrollService: ScrollObservableService,
    private store: Store,
    private dialog: MatDialog,
    private renderer: Renderer2,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new InitSales());

    this.store.dispatch(new GetWallet());

    this.translateService.stream('PAGES.SALES.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.salesMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new LoadSales(this.page, this.pageSize, this.filter, this.searchValue, true));
    });

    const stickyTabs = this.tabsElement.nativeElement.offsetTop;

    this.scrollService.scrolled$.subscribe(event => {
      if (event.target.scrollTop >= stickyTabs) {
        this.renderer.addClass(this.tabsElement.nativeElement, 'fixed');
      } else {
        this.renderer.removeClass(this.tabsElement.nativeElement, 'fixed');
      }
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new LoadSales(this.page, this.pageSize, this.filter, this.searchValue, true));
    });
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new LoadSales(this.page, this.pageSize, this.filter, this.searchValue));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  back() {
    this.location.back();
  }

  applyFilter(type: string) {
    this.filter = type;
    this.page = 1;
    this.store.dispatch(new LoadSales(this.page, this.pageSize, this.filter, this.searchValue));
  }
}
