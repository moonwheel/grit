import { NgModule } from "@angular/core";
import { WalletComponent } from "./wallet.component";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../../shared/shared.module";
import { WalletRouting } from "./wallet.routing";
import {WalletTransactionComponent} from './wallet-transaction/wallet-transaction.component';

@NgModule({
    declarations: [WalletComponent, WalletTransactionComponent],
    imports: [
        CommonModule,
        SharedModule,
        WalletRouting,
    ]
})
export class WalletModule {}