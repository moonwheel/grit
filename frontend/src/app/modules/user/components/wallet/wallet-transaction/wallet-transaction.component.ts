import { Component, EventEmitter, Input, Output } from "@angular/core";
import { WalletTransaction } from "../../../../../store/state/wallet.state";

@Component({
    selector: 'wallet-transaction',
    styleUrls: ['./wallet-transaction.component.scss'],
    templateUrl: './wallet-transaction.component.html'
})
export class WalletTransactionComponent {
    @Input() transaction: WalletTransaction;
    @Output() click = new EventEmitter();
}