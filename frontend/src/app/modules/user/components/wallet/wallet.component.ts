import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ViewChildren,
  QueryList,
  AfterViewInit,
  OnDestroy,
} from "@angular/core";
import { of, EMPTY, Subject } from "rxjs";
import { switchMap, skip, debounceTime, takeUntil } from "rxjs/operators";
import { AbstractInfiniteListComponent } from "src/app/shared/components/abstract-infinite-list/abstract-infinite-list.component";
import { NgModel } from "@angular/forms";
import { PaymentsState } from "src/app/store/state/payment.state";
import { Select, Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { UserState } from "../../../../store/state/user.state";
import { WalletActions } from "../../../../store/actions/wallet.actions";
import {
  WalletState,
  WalletTransaction,
} from "../../../../store/state/wallet.state";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { GetWallet } from 'src/app/store/actions/payment.actions';
import { MatDialog } from "@angular/material/dialog";
import { KycValidationDialogComponent } from 'src/app/shared/components/kyc-validation-dialog/kyc-validation-dialog.component';
import { PayoutDialogComponent } from 'src/app/shared/components/payout-dialog/payout-dialog.component';

@Component({
  selector: "app-wallet",
  styleUrls: ["./wallet.component.scss"],
  templateUrl: "./wallet.component.html",
})
export class WalletComponent
  extends AbstractInfiniteListComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @Select(UserState.loggedInAccountVerification)
  loggedInAccountVerification$: Observable<any>;

  @Select(PaymentsState.balance)
  balance$: Observable<any>;

  @Select(WalletState.lazyLoading)
  lazyLoading$: Observable<boolean>;

  @Select(WalletState.loading)
  loading$: Observable<boolean>;

  @Select(PaymentsState.loading)
  walletLoading$: Observable<boolean>;

  @Select(WalletState.transactions)
  transactions$: Observable<boolean>;

  @Select(WalletState.fetched)
  fetched$: Observable<boolean>;

  @Select(WalletState.total)
  total$: Observable<number>;

  @Select(WalletState.searching)
  searching$: Observable<boolean>;

  @ViewChild("searchInputElement")
  searchInputElement: ElementRef;

  @ViewChildren("searchInput", { read: NgModel })
  searchInput: QueryList<NgModel>;

  displayedColumns: string[] = ["orderId", "amount", "orderType", "date"];

  header = true;
  search = false;

  filterType: string = "all";

  page = 1;
  pageSize = 16;

  transactionsMapping = {};

  constructor(
    private store: Store,
    private router: Router,
    private translateService: TranslateService,
    private dialog: MatDialog,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new GetWallet());
    this.store.dispatch(new WalletActions.LoadTransactions(1, 15, "all"));

    this.translateService
      .stream("PAGES.WALLET.MAPPING")
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.transactionsMapping = {
          "=0": res.ZERO,
          "=1": res.ONE,
          other: res.MANY,
        };
      });
  }

  ngOnDestroy() {
    this.store.dispatch(new WalletActions.ResetWallet());
  }

  ngAfterViewInit() {
    this.searchInput.changes
      .pipe(
        switchMap(() =>
          this.searchInput.first ? of(this.searchInput.first) : EMPTY
        ),
        switchMap((model: NgModel) => model.valueChanges),
        skip(1),
        debounceTime(200),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.page = 1;
        this.store.dispatch(
          new WalletActions.LoadTransactions(
            this.page,
            this.pageSize,
            "all",
            this.searchValue,
            false,
            this.filterType
          )
        );
      });

    this.scrolled().subscribe(() => {
      this.store.dispatch(
        new WalletActions.LoadTransactions(
          this.page,
          this.pageSize,
          this.filterType as any,
          this.searchValue,
          true
        )
      );
    });
  }

  rowClick(tx: WalletTransaction) {
    const { orderId, orderType, type, refTo } = tx;

    const navUrl = `/${refTo}/${
      orderType === "order" ? "product" : "service"
    }/${orderId}`;

    this.router.navigateByUrl(navUrl);
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  showHeader() {
    this.search = false;
    this.header = true;
  }

  applyFilter(filter: string) {
    this.page = 1;
    this.filterType = filter;

    this.store.dispatch(
      new WalletActions.LoadTransactions(
        this.page,
        this.pageSize,
        "all",
        this.searchValue,
        false,
        this.filterType == "all" ? "" : this.filterType
      )
    );
  }

  validate() {
    this.dialog.open(KycValidationDialogComponent, {
      disableClose: true,
      width: '500px',
    });
  }

  payout() {
    this.dialog.open(PayoutDialogComponent, {
      disableClose: true,
      width: '500px',
    });
  }
}
