import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { SettingsComponent } from './settings.component';
// Guards
import { CanDeactivateGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    canDeactivate: [CanDeactivateGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRouting {}
