import { Component, ElementRef, OnInit, OnDestroy, TemplateRef, Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subject, of, combineLatest } from 'rxjs';
import { AddressModel, UserModel } from 'src/app/shared/models/user/user.model';
import { Select, Store } from '@ngxs/store';
import { SatPopover } from '@ncstate/sat-popover';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { UserState } from 'src/app/store/state/user.state';
import { DateTime } from 'luxon';
import { ChangeUserLanguage, UpdateUser } from 'src/app/store/actions/user.actions';
import { constants } from 'src/app/core/constants/constants';
import { AddAddress, DeleteAddress, EditAddress, InitAddress } from 'src/app/store/actions/address.actions';
import { AddressState } from 'src/app/store/state/address.state';
import { Utils } from 'src/app/shared/utils/utils';
import { ChangePassword, Logout, VerifyPasswordWithAuth, ChangeAccount, UpdateTokens } from 'src/app/store/actions/auth.actions';
import { UserService } from 'src/app/shared/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessCategoryStateModel, BusinessState } from '../../../../store/state/business.state';
import { UpdateBusiness, InitBusiness, GetBusiness, GetBusinessCategories } from '../../../../store/actions/business.actions';
import { BusinessService } from 'src/app/shared/services/business.service';
import { switchMap, filter, take, catchError, tap, map, takeUntil } from 'rxjs/operators';
import { getDefaultWeekley } from 'src/app/shared/constants';
import { AddressDialogComponent } from 'src/app/shared/components/address-dialog/address-dialog.component';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { AbstractAddPageComponent } from 'src/app/shared/components/abstract-add-page/abstract-add-page.component';
import { TranslateService } from '@ngx-translate/core';
import { SetGlobalLoading } from 'src/app/store/actions/visible.actions';

@Injectable()
export class ChangePassErrorStateMatcher implements ErrorStateMatcher {
  isErrorState( control: FormControl | null, form: FormGroupDirective | NgForm | null ): boolean {
    const invalidCtrl = !!(control && control.invalid && (control.touched || control.dirty));

    return (invalidCtrl);
  }
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent extends AbstractAddPageComponent implements OnInit, OnDestroy {

  @Select( UserState.type ) userType$: Observable<string>;
  @Select( UserState.info ) userInfo$: Observable<UserModel>;
  @Select( UserState.loggedInAccountPhoto ) loggedInAccountPhoto$: Observable<string>;
  @Select( UserState.loggedInAccountRoleType ) loggedInAccountRoleType$: Observable<string>;

  @Select( AddressState.addresses ) addresses$: Observable<AddressModel[]>;
  @Select( BusinessState.businessList ) businessList$: Observable<any[]>;
  @Select( BusinessState.businessPhoto ) businessPhoto$: Observable<any>;

  loading$ = combineLatest([
    this.store.select(UserState.loading),
    this.store.select(BusinessState.loading),
  ]).pipe(
    map(([userLoading, businessLoading]) => userLoading || businessLoading),
  );

  canDeletePage$ = combineLatest([
    this.store.select(UserState.type),
    this.store.select(UserState.loggedInAccountRoleType),
  ]).pipe(
    map(([userType, loggedInAccountRoleType]) => userType === 'person' || loggedInAccountRoleType === 'admin'),
  );

  timeValues = [
    {name: '00:00', value: '00:00:00'},
    {name: '00:30', value: '00:30:00'},
    {name: '01:00', value: '01:00:00'},
    {name: '01:30', value: '01:30:00'},
    {name: '02:00', value: '02:00:00'},
    {name: '02:30', value: '02:30:00'},
    {name: '03:00', value: '03:00:00'},
    {name: '03:30', value: '03:30:00'},
    {name: '04:00', value: '04:00:00'},
    {name: '04:30', value: '04:30:00'},
    {name: '05:00', value: '05:00:00'},
    {name: '05:30', value: '05:30:00'},
    {name: '06:00', value: '06:00:00'},
    {name: '06:30', value: '06:30:00'},
    {name: '07:00', value: '07:00:00'},
    {name: '07:30', value: '07:30:00'},
    {name: '08:00', value: '08:00:00'},
    {name: '08:30', value: '08:30:00'},
    {name: '09:00', value: '09:00:00'},
    {name: '09:30', value: '09:30:00'},
    {name: '10:00', value: '10:00:00'},
    {name: '10:30', value: '10:30:00'},
    {name: '11:00', value: '11:00:00'},
    {name: '11:30', value: '11:30:00'},
    {name: '12:00', value: '12:00:00'},
    {name: '12:30', value: '12:30:00'},
    {name: '13:00', value: '13:00:00'},
    {name: '13:30', value: '13:30:00'},
    {name: '14:00', value: '14:00:00'},
    {name: '14:30', value: '14:30:00'},
    {name: '15:00', value: '15:00:00'},
    {name: '15:30', value: '15:30:00'},
    {name: '16:00', value: '16:00:00'},
    {name: '16:30', value: '16:30:00'},
    {name: '17:00', value: '17:00:00'},
    {name: '17:30', value: '17:30:00'},
    {name: '18:00', value: '18:00:00'},
    {name: '18:30', value: '18:30:00'},
    {name: '19:00', value: '19:00:00'},
    {name: '19:30', value: '19:30:00'},
    {name: '20:00', value: '20:00:00'},
    {name: '20:30', value: '20:30:00'},
    {name: '21:00', value: '21:00:00'},
    {name: '21:30', value: '21:30:00'},
    {name: '22:00', value: '22:00:00'},
    {name: '22:30', value: '22:30:00'},
    {name: '23:00', value: '23:00:00'},
    {name: '23:30', value: '23:30:00'},
  ];

  defaultWeekly = [ ...getDefaultWeekley() ];

  months = [ ...constants.MONTHS ];

  form: FormGroup;
  userForm: FormGroup;
  businessForm: FormGroup;
  bankForm: FormGroup;
  changePassForm: FormGroup;

  matcher = new ChangePassErrorStateMatcher();

  userInfo;
  weekly;

  maxDate = 31;
  maxYear = DateTime.local().year;
  minYear = DateTime.local().minus( {years: 100} ).year;
  oldEmail;
  oldPageName;
  oldBusinessPageName;
  oldBusinessName;

  businessCategories = [];

  imageChangedEvent: any = '';
  croppedPhoto = '';

  photoData;
  isDeleted = false;
  isEditFlow = true;

  editAddressIndex = -1;

  password = false;
  oldPassword = false;
  showConfirmDeletePassword = false;
  confirmPassword = false;
  showHours = false;

  monday = true;
  tuesday = true;
  wednesday = true;
  thursday = true;
  friday = true;
  saturday = false;
  sunday = false;

  mondayMore = false;
  tuesdayMore = false;
  wednesdayMore = false;
  thursdayMore = false;
  fridayMore = false;
  saturdayMore = false;
  sundayMore = false;
  pageRegexp = new RegExp( '^[a-z0-9.-]*$' );

  addresses = [];

  banks = [];

  constants = constants;
  addFocus = false;
  localPhotoLoading = false;

  confirmDeleteForm = this.formBuilder.group({
    password: ['', Validators.required]
  });

  private cropperDialogRef: MatDialogRef<any>;

  get dateRangeForm() {
    return this.businessForm.get( 'businessDateRange' ) as FormArray;
  }

  constructor(
    protected navigationService: NavigationService,
    protected formBuilder: FormBuilder,
    protected dialog: MatDialog,
    protected store: Store,
    protected userService: UserService,
    protected businessService: BusinessService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackbar: MatSnackBar,
    protected translateService: TranslateService,
  ) {
    super(router, route, dialog, navigationService, translateService);
  }

  ngOnInit() {
    this.store.dispatch(new InitAddress());
    this.store.dispatch(new GetBusinessCategories());

    this.loggedInAccountPhoto$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(photo => {
      this.croppedPhoto = photo;
    });

    this.route.queryParams.pipe(
      takeUntil(this.destroy$)
    ).subscribe(params => {
      if (params.editEmail) {
        this.addFocus = true;
      }
    });

    this.userForm = this.formBuilder.group( {
      photo: '',
      pageName: ['', [Validators.required, Validators.minLength( 3 )]],
      //
      fullName: ['', Validators.required],
      residence: '',
      gender: ['', Validators.required],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      birthday: ['', Validators.required],
      //
      description: '',
      email: ['', Validators.required],
      language: ['deutsch', Validators.required],
      privacy: ['', Validators.required],
      delivery_address: null,
      created: new Date(),
      // payment: null,
    } );

    this.businessForm = this.formBuilder.group({
      businessPhoto: '',
      businessPageName: ['', [Validators.required, Validators.minLength(3)]],
      businessName: ['', Validators.required],
      businessCategory: ['', Validators.required],
      businessDateRange: this.formBuilder.array([]),
      businessPhone: ['', [Validators.required, Validators.minLength(3)]],
      businessLegal_notice: '',
      businessDescription: '',
      businessLanguage: ['deutsch', Validators.required],
      businessCreated: new Date(),
      businessDelivery_address: null,
      businessAddress: this.formBuilder.group({
        id: null,
        street: [null, Validators.required],
        zip: [null, Validators.required],
        city: [null, Validators.required],
      }),
    });

    this.bankForm = this.formBuilder.group({
      holder: ['', Validators.required],
      iban: ['', Validators.required],
      created: new Date(),
    });

    this.changePassForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validator: this.checkPasswords,
    });

    this.setSettingsForms();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  wasChanged(): boolean {
    return Boolean(
      !this.userForm.pristine
      || !this.businessForm.pristine
    );
  }

  setSettingsForms() {
    this.store.selectOnce(UserState.loggedInAccount).pipe(
      switchMap(account => {
        const business = this.store.selectSnapshot(BusinessState.business);

        if (account.business && !business) {
          return this.store.dispatch(new GetBusiness(account.business.id));
        } else {
          const snapshot = this.store.snapshot();
          return of(snapshot);
        }
      }),
    ).subscribe(storeSnapshot => {
      const userInfo = { ...storeSnapshot };
      if ( userInfo.business.business && (userInfo.user.loggedInAccount.person.type !== 'person') ) {
        this.oldBusinessPageName = userInfo.business.business.pageName;
        this.oldBusinessName = userInfo.business.business.name;
        const businessInfo: any = Object.entries( userInfo.business.business ).reduce( ( accum, item ) => {
          const [key, value] = item;
          const upperKey = `${ key[0].toUpperCase() }${ key.slice( 1 ) }`;
          accum[`business${ upperKey }`] = value;
          if ( key === 'category' ) {
            const objectValue: any = value;
            accum[`business${ upperKey }`] = objectValue.id;
          }
          return accum;
        }, {} );

        this.businessForm.patchValue(businessInfo);
        this.form = this.businessForm;

        const deliveryAddressId = businessInfo.businessDelivery_address && businessInfo.businessDelivery_address.id;
        this.businessForm.get('businessDelivery_address').setValue(deliveryAddressId);
        delete userInfo.business;
        this.userInfo = { ...userInfo, ...businessInfo };

        this.getBusinessCategories();
        this.addDayRange();
        if ( this.userInfo.businessHours.length ) {
          this.showHours = true;
          this.userInfo.businessHours.forEach( day => {
            this.addDailySchedule( day );
          } );
        }
      } else {
        const userData = userInfo.user.loggedInAccount.person;

        this.userForm.patchValue(userData);
        this.userForm.get( 'day' ).setValue( new Date( userData.birthday ).getDate() );
        this.userForm.get( 'month' ).setValue( new Date( userData.birthday ).getMonth() );
        this.userForm.get( 'year' ).setValue( new Date( userData.birthday ).getFullYear() );
        this.userForm.get( 'delivery_address' ).setValue(userData.delivery_address && userData.delivery_address.id);

        this.oldEmail = userData.email;
        this.oldPageName = userData.pageName;
        this.form = this.userForm;
      }
    });

  }

  getBusinessCategories() {
    this.store.selectOnce(BusinessState.categories).subscribe(categoriesData => {
      const categories = Object.getOwnPropertyNames(categoriesData);
      categories.forEach(item => {
        this.businessCategories.push({
          name: item,
          categories: categoriesData[item],
        });
      });
    });
  }

  addDayRange() {
    this.dateRangeForm.setErrors( null );
    if ( this.dateRangeForm.value[0] && !this.dateRangeForm.value[0].day ) {
      this.dateRangeForm.clear();
    }
    if ( !this.dateRangeForm.value[0] ) {
      this.weekly = this.defaultWeekly.slice();
      this.weekly.forEach( day => {
        day.enabled = false;
        if ( day.enabled ) {
          day.from_first = ['', Validators.required];
          day.to_first = ['', Validators.required];
        }
        const item = this.formBuilder.group( day );
        this.dateRangeForm.push( item );
      } );
    }
  }

  /** Profile Photo START */

  selectPhoto(event, tempRef: TemplateRef<any> ) {
    this.imageChangedEvent = event;
    this.isDeleted = false;
    this.localPhotoLoading = true;
    this.cropperDialogRef = this.dialog.open(tempRef);
    this.cropperDialogRef.afterClosed().pipe(
      takeUntil(this.destroy$),
    ).subscribe(yes => {
      event.target.value = null;
      if (!yes) {
        this.croppedPhoto = this.store.selectSnapshot(UserState.loggedInAccountPhoto);
      }
    });
  }

  removePhoto( event ) {
    event.stopPropagation();
    this.croppedPhoto = constants.PLACEHOLDER_AVATAR_PATH;
    this.photoData = null;
    this.isDeleted = true;
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedPhoto = event.base64;
    this.photoData = event.base64;
  }

  cropperReady() {
    this.localPhotoLoading = false;
  }

  closeCropperDialog() {
    this.cropperDialogRef.close(true);
  }

  /** Profile Photo END */

  addDailySchedule( day ) {
    const weekly = this.defaultWeekly.slice();
    const currentDay = weekly[day.day.id - 1];
    currentDay.enabled = true;
    if ( day.from && (!currentDay.from_first || !currentDay.from_first[0]) ) {
      currentDay.from_first = day.from;
    }
    if ( day.to && (!currentDay.to_first || !currentDay.to_first[0]) ) {
      currentDay.to_first = day.to;
    }
    if ( day.id && (currentDay.from_first === day.from) ) {
      currentDay.firstPartId = day.id;
    }
    if ( day.from && currentDay.from_first !== day.from ) {
      currentDay.addition = true;
      if ( day.from && currentDay.from_first !== day.from ) {
        currentDay.from_second = day.from;
      }
      if ( day.to && currentDay.to_first !== day.to ) {
        currentDay.to_second = day.to;
      }
      if ( day.id && (day.from && currentDay.from_first !== day.from) ) {
        currentDay.secondPartId = day.id;
      }
    }
    this.dateRangeForm.controls.forEach( ( item, index ) => {
      this.dateRangeForm.controls[index].setValue( weekly[index] );
    } );
  }

  toggleDay( day ) {
    const index = this.dateRangeForm.value.findIndex( element => element.day === day.value.day );
    const currentDayControls = this.dateRangeForm.controls[index];
    if ( !currentDayControls ) {
      return;
    }
    if ( day.value.enabled ) {
      this.dateRangeForm.setErrors( null );
      currentDayControls.get( 'from_first' ).setValidators( [Validators.required] );
      currentDayControls.get( 'to_first' ).setValidators( [Validators.required] );
    } else {
      currentDayControls.get( 'from_second' ).clearValidators();
      currentDayControls.get( 'from_second' ).updateValueAndValidity();
      currentDayControls.get( 'to_second' ).clearValidators();
      currentDayControls.get( 'to_second' ).updateValueAndValidity();
      currentDayControls.get( 'from_first' ).clearValidators();
      currentDayControls.get( 'from_first' ).updateValueAndValidity();
      currentDayControls.get( 'to_first' ).clearValidators();
      currentDayControls.get( 'to_first' ).updateValueAndValidity();
    }
  }

  toggleSecondTimeRange( day, show ) {
    const currentDayControls = this.dateRangeForm.controls[this.dateRangeForm.value.findIndex( element => element.day === day.value.day )];
    currentDayControls.get( 'addition' ).setValue( show );
    if ( !currentDayControls ) {
      return;
    }
    if ( show ) {
      currentDayControls.get( 'from_second' ).setValidators( [Validators.required] );
      currentDayControls.get( 'to_second' ).setValidators( [Validators.required] );
    } else {
      currentDayControls.get( 'from_second' ).clearValidators();
      currentDayControls.get( 'from_second' ).updateValueAndValidity();
      currentDayControls.get( 'to_second' ).clearValidators();
      currentDayControls.get( 'to_second' ).updateValueAndValidity();
    }
  }

  removeErrors( day, order ) {
    day.get( `from_${ order }` ).setErrors( null );
    day.get( `to_${ order }` ).setErrors( null );
  }

  buildBirthdayValue() {
    let errorFound = false;
    const day = this.userForm.get( 'day' );
    const month = this.userForm.get( 'month' );
    const year = this.userForm.get( 'year' );
    this.countMaxDate();
    if ( +day.value > this.maxDate || !day.value ) {
      day.setErrors( {badDate: true} );
      errorFound = true;
    } else {
      day.setErrors( null );
    }
    if ( year.value > this.maxYear || year.value < this.minYear ) {
      year.setErrors( {badYear: true} );
      errorFound = true;
    } else {
      year.setErrors( null );
    }
    if ( errorFound || month.value === '' ) {
      return;
    }
    const textMonth = this.months.filter( item => item.id === month.value )[0].name;
    const textDate = `${ day.value } ${ textMonth } ${ year.value }`;
    return DateTime.fromFormat( textDate, 'd MMM yyyy' ).toISO();
  }

  countMaxDate() {
    const chosenMonth = this.userForm.get( 'month' ).value;
    if ( [0, 2, 4, 6, 7, 9, 11].includes( chosenMonth ) ) {
      this.maxDate = 31;
    }
    if ( [3, 5, 8, 10].includes( chosenMonth ) ) {
      this.maxDate = 30;
    }
    if ( chosenMonth === 1 ) {
      if ( this.userForm.get( 'year' ).value && !(this.userForm.get( 'year' ).value % 4) ) {
        this.maxDate = 29;
      } else {
        this.maxDate = 28;
      }
    }
  }

  checkPasswords( group: FormGroup ) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;
    const notMatch = (pass !== confirmPass);
    if ( notMatch ) {
      group.controls.confirmPassword.setErrors( {notSame: true} );
    } else {
      group.controls.confirmPassword.setErrors( null );
    }
  }

  // Hours Form

  newHours( hours, event: Event ) {
    hours.toggle();
    event.stopPropagation();
  }

  addHours( hours ) {
    // this.aboutState.addHours(this.hoursForm.value);
    // this.hoursForm.reset();
    hours.close();
  }

  // Address Form

  openAddAddressDialog(event: Event) {
    this.dialog.open(AddressDialogComponent);
    event.stopPropagation();
  }

  openEditAddressDialog(address: AddressModel, popover: SatPopover) {
    this.dialog.open(AddressDialogComponent, { data: { address } });
    popover.close();
  }

  openAddressMorePopover(popover: SatPopover, event: Event) {
    popover.toggle();
    event.stopPropagation();
  }

  deleteAddress(id: number, popover: SatPopover) {
    this.store.dispatch(new DeleteAddress(id));
    popover.close();
  }

  // Bank Form

  bank( templateRef, event: Event ) {
    this.dialog.open( templateRef );
    event.stopPropagation();
  }

  addBank( bank ) {
    // this.aboutState.addBank(this.bankForm.value);
    // this.bankForm.reset();
    bank.close();
  }

  moreBank( bankMore, event: Event ) {
    bankMore.toggle();
    event.stopPropagation();
  }

  editBank() {

  }

  deleteBank() {

  }

  showPassword( event: Event, name ) {
    this[name] = !this[name];
    event.stopPropagation();
  }

  showprivacyInfo( privacyInfo, event: Event ) {
    privacyInfo.toggle();
    event.stopPropagation();
  }

  deletePage( templateRef ) {
    const dialogRef = this.dialog.open( templateRef );
  }

  save() {
    const userType = this.store.selectSnapshot(UserState.type);
    let form: FormGroup;

    if (userType === 'person') {
      form = this.userForm;
    } else {
      form = this.businessForm;
    }

    form.markAsTouched();
    if ( form.value.birthday ) {
      const settings = form.value;
      delete form.value.day;
      delete form.value.month;
      delete form.value.year;
      form.value.birthday = this.buildBirthdayValue();
      if ( form.value.email === this.oldEmail ) {
        delete form.value.email;
      }
      if ( form.value.pageName === this.oldPageName ) {
        delete form.value.pageName;
      }
      settings.photoData = this.photoData;
      settings.isDeleted = this.isDeleted;
      settings.description = settings.description && settings.description.trim();
      if ( form.valid ) {
        return this.store.dispatch( new UpdateUser( settings ) ).pipe(
          map(() => true),
          catchError(err => {
            if ( err.error.error === 'User with this email already exists in database' ) {
              if (form.controls.email) {
                form.controls.email.setErrors( {emailExists: true} );
              }
            }
            if ( err.error.error === 'Page with this name already exist' ) {
              if (form.controls.pageName) {
                form.controls.pageName.setErrors( {nameExists: true} );
              }
            }
            return of(false);
          })
        );
      } else {
        Utils.touchForm(form);
        return of(false);
      }
    } else {
      const payload = {
        language: form.value.businessLanguage,
        business: {
          category: form.value.businessCategory,
          description: (form.value.businessDescription || '').trim(),
          legal_notice: form.value.businessLegal_notice,
          name: form.value.businessName,
          pageName: form.value.businessPageName,
          phone: form.value.businessPhone,
          photo: this.photoData,
          address: form.value.businessAddress,
          delivery_address: form.value.businessDelivery_address,
          hours: []
        }
      };
      this.businessForm.value.businessDateRange.forEach( day => {
        const dayPayload = [];
        if ( day.enabled && (day.from_first || day.to_first) ) {
          dayPayload.push( {
            from: day.from_first,
            to: day.to_first,
            day: day.id,
            id: day.firstPartId
          } );
        }
        if ( day.enabled && day.addition && day.from_second ) {
          dayPayload.push( {
            from: day.from_second,
            to: day.to_second,
            day: day.id,
            id: day.secondPartId,
          } );
        }
        if ( !day.enabled && day.firstPartId ) {
          dayPayload.push( {
            id: day.firstPartId
          } );
        }
        if ( (!day.enabled || !day.addition || !day.from_second) && day.secondPartId ) {
          dayPayload.push( {
            id: day.secondPartId
          } );
        }
        if ( dayPayload.length ) {
          payload.business.hours = payload.business.hours.concat( dayPayload );
        }
      } );
      if ( form.value.businessPageName === this.oldBusinessPageName ) {
        delete payload.business.pageName;
      }
      if ( form.value.businessName === this.oldBusinessName ) {
        delete payload.business.name;
      }

      if ( form.valid ) {
        return this.store.dispatch( new UpdateBusiness( payload, this.userInfo.businessId, this.oldBusinessName ) ).pipe(
          catchError(err => {
            if ( err === 'Page with this name already exist' ) {
              if (form.controls.businessPageName) {
                form.controls.businessPageName.setErrors( {nameExists: true} );
              }
            }
            return of(false);
          })
        );
      } else {
        Utils.touchForm(form);
        return of(false);
      }
    }
  }

  saveAndBack() {
    this.save().subscribe(success => {
      if (success) {
        const pageName = this.store.selectSnapshot(UserState.loggedInAccountPageName);
        this.canImmediatelyDeactivate = true;
        this.router.navigateByUrl(`/${pageName}/home`);
      }
    });
  }

  changePassword() {
    if ( this.changePassForm.valid ) {
      const formValue = this.changePassForm.value;
      this.store.dispatch( new ChangePassword( formValue ) );
    } else {
      Utils.touchForm( this.changePassForm );
    }
  }

  verifyPassword() {
    this.store.dispatch( new VerifyPasswordWithAuth() );
  }

  deleteUser() {
    const type = this.store.selectSnapshot( UserState.type );
    const user = this.store.selectSnapshot( UserState.loggedInAccount );

    switch (type) {
      case 'person':
        if (this.confirmDeleteForm.valid) {
          const password = this.confirmDeleteForm.value.password;
          this.userService.delete(password).subscribe(() => {
            this.dialog.closeAll();
            this.store.dispatch(new Logout());
            this.router.navigate(['/']).then(() => {
              this.translateService.get('INFO_MESSAGES.PERSONAL_PAGE_DELETED').subscribe(message => {
                this.snackbar.open(message);
              });
            });
          }, (errorResponse) => {
            if (errorResponse.error && errorResponse.error.error) {
              if (errorResponse.error.error === 'Wrong password') {
                this.confirmDeleteForm.get('password').setErrors({ wrongPassword: true });
              } else {
                this.snackbar.open(errorResponse.error.error);
              }
            }
            console.error('Delete settings', errorResponse);
          });
        }
        break;

      case 'business':
        this.businessService.delete(user.business.id).pipe(
          switchMap(data => this.store.dispatch(new UpdateTokens({ token: data.token }))),
          switchMap(() => this.store.dispatch(new InitBusiness())),
          switchMap(() => {
            const accounts = this.store.selectSnapshot(BusinessState.businessList);
            const mainUserAccount = accounts.find(account => !account.business);
            return this.store.dispatch(new ChangeAccount(mainUserAccount.id));
          }),
        ).subscribe(() => {
          this.dialog.closeAll();
          this.router.navigate([`/${user.person.pageName}/home`]);
        }, (error) => {
          console.error('Delete settings', error);
        });
        break;

      default:
        break;
    }
  }

  checkValue( event, controlName ) {
    let control;
    if ( controlName === 'pageName' ) {
      control = this.userForm.controls.pageName;
    }
    if ( controlName === 'businessPageName' ) {
      control = this.businessForm.controls.businessPageName;
    }
    if ( !control ) {
      return;
    }
    const res = this.pageRegexp.test( event.key );
    if ( !res ) {
      control.setErrors( {invalidChar: true} );
      return false;
    } else {
      control.setErrors( {invalidChar: null} );
    }
  }

  onKey( $event ) {
    const value = $event;
    if ( value >= 1000 ) {
      if ( value >= this.maxYear ) {
        this.userForm.get( 'year' ).setValue( this.maxYear, {
          emitEvent: false,
          emitViewToModelChange: false
        } );
      } else if ( value <= this.minYear ) {
        this.userForm.get( 'year' ).setValue( this.minYear, {
          emitEvent: false,
          emitViewToModelChange: false
        } );
      }
    }
  }

  openDialog(dialogRef: TemplateRef<any>) {
    this.dialog.open(dialogRef);
  }

  openFullscreenDialog(dialogRef: TemplateRef<any>) {
    this.dialog.open(dialogRef, { panelClass: ['full-screen-dialog'] });
  }
}
