import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { SettingsRouting } from './settings.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { ImageCropperModule } from 'ngx-image-cropper';

// Components
import { SettingsComponent } from './settings.component';


@NgModule({
  declarations: [
    SettingsComponent,
  ],
  imports: [
    CommonModule,
    SettingsRouting,
    SharedModule,
    ImageCropperModule
  ]
})
export class SettingsModule { }
