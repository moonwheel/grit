import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { TeamRouting } from './team.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// Components
import { TeamListComponent } from './team-list/team-list.component';
import { TeamAddComponent } from './team-add/team-add.component';
import { TeamListModule } from './team-list/team-list.module';

@NgModule({
  declarations: [
    TeamAddComponent,
  ],
  imports: [
    CommonModule,
    TeamRouting,
    SharedModule,
    InfiniteScrollModule,
    TeamListModule
  ]
})
export class TeamModule { }
