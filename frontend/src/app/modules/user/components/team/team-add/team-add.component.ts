import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AccountModel } from 'src/app/shared/models';
import { Subject, fromEvent, Observable } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, takeUntil, switchMap } from 'rxjs/operators';
import { TeamActions } from 'src/app/store/actions/team.actions';
import { TeamState } from 'src/app/store/state/team.state';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-team-add',
  templateUrl: './team-add.component.html',
  styleUrls: [ './team-add.component.scss' ],
})

export class TeamAddComponent implements OnInit, OnDestroy {

  @Select(TeamState.autoSuggestions) autoSuggestions$: Observable<AccountModel[]>;
  @Select(TeamState.loading) loading$: Observable<boolean>;

  @ViewChild('searchInput', { static: true }) searchInputRef: ElementRef<HTMLInputElement>;

  role = '';
  search = '';

  accounts: AccountModel[] = [];

  currentAccount: AccountModel;

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  private destroy$ = new Subject();

  constructor(
    private store: Store,
    private navigationService: NavigationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleSearch: false }));

    fromEvent<Event>(this.searchInputRef.nativeElement, 'keyup').pipe(
      debounceTime(100),
      map(event => (event.target as HTMLInputElement).value.toLowerCase()),
      distinctUntilChanged(),
      switchMap(value => this.store.dispatch(new TeamActions.GetAutosuggestions(value))),
      takeUntil(this.destroy$),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    this.store.dispatch(new ToggleVisible({ isVisibleMain: true, isVisibleSearch: true }));
  }

  getAccountName(account: AccountModel): string {
    return account && account.name || '';
  }

  onRoleChange() {
    if (this.currentAccount) {
      const found = this.accounts.find(item => item.id === this.currentAccount.id);

      if (found) {
        found.role = this.role;
      } else {
        this.accounts.push({
          ...this.currentAccount,
          role: this.role,
        });
      }

      this.search = '';
      this.role = null;
      this.currentAccount = null;
      this.store.dispatch(new TeamActions.GetAutosuggestions(null));
    }
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    this.currentAccount = { ...event.option.value };
    this.role = '';
  }

  removeAccountFromList(account: AccountModel) {
    this.accounts = this.accounts.filter(item => item.id !== account.id);
  }

  changeRole(account: AccountModel, role: string) {
    account.role = role;
  }

  back() {
    this.navigationService.back();
  }

  save() {
    if (this.accounts.length) {
      this.store.dispatch(new TeamActions.AddMany(this.accounts)).subscribe(() => {
        this.router.navigate(['../'], { relativeTo: this.activatedRoute });
      });
    }
  }

}
