import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { TeamListComponent } from './team-list/team-list.component';
import { TeamAddComponent } from './team-add/team-add.component';
import { AdminGuard } from 'src/app/core/guards';

const routes: Routes = [
  { path: '', component: TeamListComponent },
  { path: 'add', component: TeamAddComponent, canActivate: [AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRouting {
}
