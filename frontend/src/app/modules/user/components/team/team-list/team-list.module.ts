import { NgModule } from "@angular/core";
import { TeamListComponent } from "./team-list.component";
import { SharedModule } from 'src/app/shared/shared.module';
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
    declarations: [
        TeamListComponent,
    ],
    imports: [
        SharedModule,
        InfiniteScrollModule
    ]
})
export class TeamListModule { }
