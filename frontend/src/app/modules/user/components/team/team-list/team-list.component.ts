import { Component, OnInit, TemplateRef, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, of, EMPTY } from 'rxjs';

import { AbstractEntityModel, AccountModel } from 'src/app/shared/models';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { TeamActions } from 'src/app/store/actions/team.actions';
import { TeamState } from 'src/app/store/state/team.state';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { NgModel } from '@angular/forms';
import { switchMap, skip, debounceTime, takeUntil } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: [ './team-list.component.scss' ],
})

export class TeamListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(TeamState.total) total$: Observable<number>;
  @Select(TeamState.items) items$: Observable<AccountModel[]>;
  @Select(TeamState.loading) loading$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(UserState.loggedInAccountId) loggedInAccountId$: Observable<number>;
  @Select(TeamState.lazyLoading) lazyLoading$: Observable<boolean>;
  @Select(TeamState.searching) searching$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;

  page = 1;
  pageSize = 10;
  filter = 'date';
  searchValue = '';

  header = true;
  search = false;

  roleToChange = '';
  navigating = false;
  deletingAccount = false;
  changingRole = false;

  i18nPluralMapping = {};

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  deleteDialog: MatDialogRef<any>;
  changeRoleDialog: MatDialogRef<any>;

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new ToggleVisible({ isVisibleUserTabs: true, isVisibleSearch: true }));
    this.store.dispatch(new TeamActions.Load());

    this.translateService.stream('PAGES.TEAM_LIST.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.i18nPluralMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new TeamActions.Load(this.searchValue, this.page, this.pageSize, true));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      switchMap(() => {
        this.page = 1;
        const value = this.searchValue.toLowerCase();
        return this.store.dispatch(new TeamActions.Load(value, this.page, this.pageSize, true));
      }),
      takeUntil(this.destroy$),
    ).subscribe();
  }

  trackByHash(index: number, item: AbstractEntityModel) {
    return `${item.tableName}-${item.id}`;
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new TeamActions.Load(this.searchValue, this.page, this.pageSize));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
  }

  delete(userId: number) {
    this.deletingAccount = true;
    this.store.dispatch(new TeamActions.Delete(userId)).subscribe(() => {
      if (this.deleteDialog) {
        this.deleteDialog.close();
      }

      this.deletingAccount = false;
    });
  }

  openDeleteDialog(templateRef: TemplateRef<any>) {
    this.deleteDialog = this.dialog.open(templateRef);
  }

  openChangeRoleDialog(templateRef: TemplateRef<any>, role: string) {
    this.roleToChange = role;
    this.changeRoleDialog = this.dialog.open(templateRef);
  }

  changeRole(userId: number, role: string) {
    this.changingRole = true;

    this.store.dispatch(new TeamActions.UpdateRole(userId, role)).subscribe(() => {
      if (this.changeRoleDialog) {
        this.changeRoleDialog.close();
      }

      this.changingRole = false;
    });
  }

}
