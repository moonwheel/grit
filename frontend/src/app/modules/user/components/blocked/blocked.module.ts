import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

import { BlockedRouting } from './blocked.routing';

import { BlockedComponent } from './blocked.component';


@NgModule({
  declarations: [
    BlockedComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    BlockedRouting,
  ]
})
export class BlockedModule { }
