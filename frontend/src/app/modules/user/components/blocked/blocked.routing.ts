import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlockedComponent } from './blocked.component';

const routes: Routes = [{
  path: '',
  component: BlockedComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BlockedRouting {}
