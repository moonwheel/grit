import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';

import { BlockedList, BlockUser } from 'src/app/store/actions/user.actions';
import { UserState } from 'src/app/store/state/user.state';
import { BlockedDialogComponent } from 'src/app/shared/components/actions/blocked/blocked-dialog/blocked-dialog.component';
import { Utils } from 'src/app/shared/utils/utils';
import { AccountModel } from 'src/app/shared/models';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-blocked-list',
  templateUrl: './blocked.component.html',
  styleUrls: ['./blocked.component.scss']
})

export class BlockedComponent implements OnInit, OnDestroy {

  @Select(UserState.blocked)
  blocked$: Observable<any>;

  @Select(UserState.loadingBlocked)
  loading$: Observable<boolean>;

  itemSize = 76;

  blockedMapping = {};

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  private destroy$ = new Subject();

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private router: Router,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.store.dispatch(new BlockedList());

    this.translateService.stream('PAGES.BLOCKED.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.blockedMapping = {
        '=0': res.ZERO,
        other: res.MANY,
      };
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  blockUser(id: number, isBlocked: boolean) {
    this.dialog.open(BlockedDialogComponent, {
      data: {
        account: { id },
        type: isBlocked ? 'unblock' : 'block',
      },
    });
  }

  getFullName(account: AccountModel) {
    return Utils.getAccountFullName(account);
  }

  getPageName(account: AccountModel) {
    return Utils.getAccountPageName(account);
  }

  goToPage(item: AccountModel, event: MouseEvent) {
    const target = event.target as HTMLElement;
    if (target && !target.classList.contains('unblock-btn')) {
      this.router.navigateByUrl(`/${this.getPageName(item)}/home`);
    }
  }
}
