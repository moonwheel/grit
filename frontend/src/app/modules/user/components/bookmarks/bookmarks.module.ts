import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

import { BookmarksRouting } from './bookmarks.routing';

import { BookmarksComponent } from './bookmarks.component';


@NgModule({
  declarations: [
    BookmarksComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    BookmarksRouting,
  ]
})

export class BookmarksModule { }
