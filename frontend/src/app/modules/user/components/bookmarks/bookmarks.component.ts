import { Component, OnInit, QueryList, ViewChildren, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, of, EMPTY } from 'rxjs';
import { switchMap, skip, debounceTime, takeUntil } from 'rxjs/operators';

import { AbstractInfiniteListComponent } from 'src/app/shared/components/abstract-infinite-list/abstract-infinite-list.component';
import { DeleteBookmark, InitBookmark, LoadBookmark } from 'src/app/store/actions/bookmark.actions';
import { BookmarkState } from 'src/app/store/state/bookmark.state';
import { UserState } from 'src/app/store/state/user.state';
import { constants } from 'src/app/core/constants/constants';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.style.scss'],
})

export class BookmarksComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(BookmarkState.bookmarks) bookmarks$: Observable<any>;

  @Select(BookmarkState.total) total$: Observable<number>;
  @Select(BookmarkState.loading) loading$: Observable<boolean>;
  @Select(BookmarkState.fetched) fetched$: Observable<boolean>;

  @Select(UserState.loggedInAccountPageName) loggedInAccountPageName$: Observable<any>;
  @Select(BookmarkState.lazyLoading) lazyLoading$: Observable<boolean>;
  @Select(BookmarkState.searching) searching$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;

  bookmarksMapping = {};

  header = true;

  search = false;
  truncating = true;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;
  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  currentUrl = '';

  page = 1;
  pageSize = 15;
  filter = '';

  searchValue = '';

  constructor(
    private store: Store,
    private translateService: TranslateService,
  ) {
    super();

    this.bookmarks$.subscribe((books) => {
      console.log('books', books);
    })
  }

  ngOnInit() {
    this.store.dispatch(new InitBookmark());
    this.currentUrl = environment.appUrl;

    this.translateService.stream('PAGES.BOOKMARKS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.bookmarksMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new LoadBookmark(this.page, this.pageSize, this.searchValue));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new LoadBookmark(this.page, this.pageSize, this.searchValue, true));
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchValue) {
      this.page = 1;
      this.searchValue = '';
      this.store.dispatch(new LoadBookmark(this.page, this.pageSize, this.searchValue));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  getPhoto(item) {
    if (item.type === 'article') {
      return (`${item.cover}` || constants.PLACEHOLDER_NO_PHOTO_PATH);
    }
    if (item.type === 'photo') {
      return (`${item.photo}` || constants.PLACEHOLDER_NO_PHOTO_PATH);
    }
    if (item.type === 'video') {
      return item.photos && item.photos.length ? (`${item.photos[0].photo}`) : constants.PLACEHOLDER_NO_PHOTO_PATH;
    }
    if (item.type === 'product') {
      return (`${item.photo}` || constants.PLACEHOLDER_NO_PHOTO_PATH);
    }
    if (item.type === 'service') {
      return (`${item.photo}` || constants.PLACEHOLDER_NO_PHOTO_PATH);
    }
  }

  removeBookmark(id: number, type: string, bookmarkId: number) {
    this.store.dispatch(new DeleteBookmark({ id, type, bookmarkId }));
  }
}
