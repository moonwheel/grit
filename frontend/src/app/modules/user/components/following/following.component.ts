import { Component, OnInit, AfterViewInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { Observable, of, EMPTY } from 'rxjs';
import { AccountModel } from 'src/app/shared/models';
import { GetFollowings } from 'src/app/store/actions/user.actions';
import { filter, map, takeUntil, debounceTime, switchMap, tap, skip } from 'rxjs/operators';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { NgModel } from '@angular/forms';
import { UnfollowDialogComponent } from 'src/app/shared/components/actions/follow/unfollow-dialog/unfollow-dialog.component';
import { MatDialog } from '@angular/material/dialog';

import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss'],
})

export class FollowingComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(UserState.followings) followings$: Observable<AccountModel[]>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;
  @Select(UserState.loadingFollow) loadingFollow$: Observable<boolean>;
  @Select(UserState.lazyLoadingFollowing) lazyLoading$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;

  total$ = this.loggedInAccount$.pipe(
    filter(item => Boolean(item)),
    map(account => account.followingsCount)
  );

  followingsMapping = {};

  header = true;
  search = false;

  searchText = '';

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  currentUrl = '';

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  constructor(private store: Store,
              private dialog: MatDialog,
              private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new GetFollowings());
    this.currentUrl = environment.appUrl;

    this.translateService.stream('PAGES.FOLLOWINGS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.followingsMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new GetFollowings(this.searchText, this.page, this.pageSize));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new GetFollowings(this.searchText, this.page, this.pageSize));
    });
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchText) {
      this.page = 1;
      this.searchText = '';
      this.store.dispatch(new GetFollowings(this.searchText, this.page, this.pageSize));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
  }

  unfollow(account: AccountModel) {
    this.dialog.open(UnfollowDialogComponent, { data: { account } });
  }

  searchFollowings() {
    this.store.dispatch(new GetFollowings(this.searchText));
  }

  onSearchKeypress(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.searchFollowings();
    }
  }

}
