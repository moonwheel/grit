import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { FollowingRouting } from './following.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { FollowingComponent } from './following.component';


@NgModule({
  declarations: [
    FollowingComponent,
  ],
  imports: [
    CommonModule,
    FollowingRouting,
    SharedModule,
  ]
})
export class FollowingModule { }
