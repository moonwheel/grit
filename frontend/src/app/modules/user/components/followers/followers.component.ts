import { Component, OnInit, NgZone, OnDestroy, AfterViewInit, ViewChildren, ViewChild, QueryList, ElementRef } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/store/state/user.state';
import { Observable, of, EMPTY } from 'rxjs';
import { AccountModel } from 'src/app/shared/models';
import { GetFollowers, FollowUser } from 'src/app/store/actions/user.actions';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UnfollowDialogComponent } from 'src/app/shared/components/actions/follow/unfollow-dialog/unfollow-dialog.component';
import { Router } from '@angular/router';
import { map, tap, debounceTime, switchMap, takeUntil, filter, skip, startWith } from 'rxjs/operators';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { NgModel } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss'],
})

export class FollowersComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {
  @Select(UserState.followers) followers$: Observable<AccountModel[]>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;
  @Select(UserState.viewableAccount) viewableAccount$: Observable<AccountModel>;
  @Select(UserState.loadingFollow) loadingFollow$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;

  total$ = this.viewableAccount$.pipe(
    filter(item => Boolean(item)),
    map(account => account.followersCount)
  );

  searchString = '';

  followersMapping = {};

  header = true;

  search = false;

  itemSize = 61;

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  virtualScrollHeight$ = this.createVirtualScrollHeightObservable(
    this.followers$,
    this.itemSize,
    5,
    this.zone,
    '61px'
  );

  constructor(private store: Store,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<FollowersComponent>,
              private router: Router,
              private zone: NgZone,
              private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    const id = this.store.selectSnapshot(UserState.viewableAccountId);
    this.store.dispatch(new GetFollowers(id, null));

    this.translateService.stream('PAGES.FOLLOWERS.MAPPING').pipe(
      takeUntil(this.destroy$),
    ).subscribe(res => {
      this.followersMapping = {
        '=0': res.ZERO,
        '=1': res.ONE,
        other: res.MANY,
      };
    });
  }

  ngAfterViewInit() {
    const id = this.store.selectSnapshot(UserState.viewableAccountId);

    this.scrolled().subscribe(() => {
      this.store.dispatch(new GetFollowers(id, this.searchString, this.page, this.pageSize));
    });

    this.searchInput.changes.pipe(
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new GetFollowers(id, this.searchString, this.page, this.pageSize));
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  showHeader() {
    this.header = true;
    this.search = false;

    if (this.searchString) {
      const id = this.store.selectSnapshot(UserState.viewableAccountId);

      this.page = 1;
      this.searchString = '';
      this.store.dispatch(new GetFollowers(id, this.searchString, this.page, this.pageSize));
    }
  }

  showSearch() {
    this.header = false;
    this.search = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 0);
  }

  follow(account: AccountModel) {
    if (account.following) {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    } else {
      this.store.dispatch(new FollowUser(account.id));
    }
  }

  goToProfile(account: AccountModel) {
    this.dialogRef.afterClosed().subscribe(() => {
      this.router.navigateByUrl(`/${account.pageName}/home`);
    });
    this.dialogRef.close();
  }
}
