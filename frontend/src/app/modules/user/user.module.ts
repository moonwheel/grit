import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { UserRouting } from './user.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    UserRouting,
    SharedModule,
  ]
})
export class UserModule { }
