import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./message-list/message-list.module').then(m => m.MessageListModule) },
  { path: 'add', loadChildren: () => import('./message-add/message-add.module').then(m => m.MessageAddModule) },
  { path: ':id', loadChildren: () => import('./message-detail/message-detail.module').then(m => m.MessageDetailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRouting {
}
