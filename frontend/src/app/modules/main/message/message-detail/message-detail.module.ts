import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { MessageDetailRouting } from "./message-detail.routing";
import { SharedModule } from "src/app/shared/shared.module";

// Components
import { MessageDetailComponent } from "./message-detail.component";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@NgModule({
  declarations: [MessageDetailComponent],
  imports: [
    CommonModule,
    MessageDetailRouting,
    SharedModule,
    InfiniteScrollModule,
  ],
  providers: [{ provide: MAT_DIALOG_DATA, useValue: {} }],
})
export class MessageDetailModule {}
