import { Capacitor, CameraResultType, CameraDirection } from '@capacitor/core';
import { Component, OnDestroy, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, AfterViewInit, TemplateRef, OnChanges, SimpleChange, SimpleChanges, Inject } from '@angular/core';
import { Store, Select, Actions, ofActionDispatched } from '@ngxs/store';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ChatState } from 'src/app/store/state/chat.state';
import { AccountModel } from 'src/app/shared/models';
import { Observable, Subject, fromEvent, merge, of } from 'rxjs';
import { MessageModel, ChatroomModel } from 'src/app/shared/models/chat';
import { UserState } from 'src/app/store/state/user.state';
import { ChatActions } from 'src/app/store/actions/chat.actions';
import { constants } from 'src/app/core/constants/constants';
import { takeUntil, filter, auditTime, switchMap, map, tap, take, delay } from 'rxjs/operators';
import { EmojiEvent } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { Utils } from 'src/app/shared/utils/utils';
import { Plugins, KeyboardInfo } from '@capacitor/core';
import { SaveDialogComponent } from 'src/app/shared/components';
import { BlockedDialogComponent } from 'src/app/shared/components/actions/blocked/blocked-dialog/blocked-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { assert } from 'console';

const { Camera } = Plugins;
const maxFileSize = constants.MAX_MESSAGE_FILE_SIZE_MB * Math.pow(2, 20);

@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.component.html',
  styleUrls: [ './message-detail.component.scss' ],
})
export class MessageDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(ChatState.currentChatroom) currentChatroom$: Observable<ChatroomModel>;
  @Select(ChatState.currentChatroomMessages) messages$: Observable<MessageModel[]>;
  @Select(ChatState.loading) loading$: Observable<boolean>;
  @Select(UserState.viewableAccount) viewableAccount$: Observable<AccountModel>;
  @Select(ChatState.lazyLoading) lazyLoading$: Observable<boolean>;

  @ViewChild('messageTextareaElement', { static: true }) messageTextareaElementRef: ElementRef<HTMLTextAreaElement>;
  @ViewChildren('messageElement') messageElementQueryList: QueryList<HTMLDivElement>;

  page = 1;
  pageSize = constants.CHAT_MESSAGES_PAGE_SIZE;
  emoji: boolean | null = null;
  templateDialogRef: MatDialogRef<{}, any>;
  newMessageText = '';
  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  loadedAssets = false;

  scrollThrottleTime = 150;
  scrollDebounceTime = 300;

  documentExtensions = [
    '.doc',
    '.docx',
    '.odt',
    '.txt',

    '.rtf',

    '.xls',
    '.xlsx',
    '.ods',
    '.csv',
    '.tsv',

    '.ppt',
    '.pptx',
    '.odp',

    '.zip',
    '.rar',
    '.7z',
    '.bz2',
    '.gz',
    '.xz',
    '.z',

    '.tar.bz2',
    '.tar.gz',
    '.tar.xz',
    '.tar.z',
    '.tbz2',
    '.tgz',

    '.txz',

    '.pdf',
    '.html',
    '.htm',
    '.xml',
    '.vcf',
  ].join(', ');

  private isNewChat = false;
  private autoscrollEnabled = true;
  private destroy$ = new Subject();
  
  initialEmoji = true;


  private get scrollElem() {
    return document.documentElement;
  }

  private get newChatUserId() {
    return Number(this.activatedRoute.snapshot.queryParamMap.get('userId'));
  }

  private get chatId() {
    return Number(this.activatedRoute.snapshot.paramMap.get('id'));
  }

  constructor(
    private navigationService: NavigationService,
    private store: Store,
    private dialog: MatDialog,
    private router: Router,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private matSnackBar: MatSnackBar,
    private translateService: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}


  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleSearch: false }));

    this.setupRouterParamsSubscription();
  }

  ngAfterViewInit() {
    this.setupScrollSubscription();
    this.setupScrollUpdateSubscription();
    setTimeout(() => this.initialEmoji = false, 10);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ChatActions.ClearCurrentChat());
    this.store.dispatch(new ToggleVisible({ isVisibleMain: true, isVisibleSearch: true }));
  }

  setupRouterParamsSubscription() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');

      this.isNewChat = id === 'new';

      if (this.isNewChat) {
        this.store.dispatch(new ChatActions.CreateNewLocalChat(this.newChatUserId));
      } else {
        this.store.dispatch(new ChatActions.LoadCurrentChat(+id));
      }
    });
  }

  setupScrollSubscription() {
    fromEvent(window, 'scroll').pipe(
      auditTime(this.scrollDebounceTime),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      const { clientHeight, scrollTop, scrollHeight } = this.scrollElem;
      this.autoscrollEnabled = clientHeight + scrollTop === scrollHeight;
    });
  }

  setupScrollUpdateSubscription() {
    merge(
      this.messageElementQueryList.changes,
      this.actions$.pipe(ofActionDispatched(ChatActions.UpdateScroll))
    ).pipe(
      filter(() => this.autoscrollEnabled),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      window.scrollTo(0, this.scrollElem.scrollHeight);
    });
  }

  back(): void {
    this.canDeactivate().subscribe(canDeactivate => {
      if (canDeactivate) {
        this.newMessageText = '';
        this.navigationService.back();
      }
    });
  }

  toogleEmoji() {
    this.emoji = !this.emoji; 
  }

  addEmoji(event: EmojiEvent) {
    const textarea = this.messageTextareaElementRef.nativeElement;
    this.newMessageText = Utils.replaceInputSelectionText(textarea, event.emoji.native);
  }

  send(event?: KeyboardEvent) {
    console.log('sent event')
    if (event) {
      event.preventDefault();
    }

    if (this.newMessageText) {
      const message = this.newMessageText.trim();
      if (this.isNewChat) {
        this.store.dispatch(new ChatActions.CreateNewRemoteChat(this.newChatUserId, message));
      } else {
        this.store.dispatch(new ChatActions.SendMessageToChat(this.chatId, message)).pipe(
          delay(100),
          takeUntil(this.destroy$),
        ).subscribe(() => {
          window.scrollTo(0, this.scrollElem.scrollHeight);
        });
      }
      this.newMessageText = '';
    }
  }

  muteChat(): void {
    const currentChatroom = this.store.selectSnapshot(ChatState.currentChatroom);

    if (currentChatroom) {
      this.store.dispatch(new ChatActions.ToggleMutedStatus(currentChatroom.id));
    }
  }

  deleteChat(): void {
    const currentChatroom = this.store.selectSnapshot(ChatState.currentChatroom);

    if (currentChatroom) {
      this.store.dispatch(new ChatActions.DeleteChatInit(currentChatroom.id));
    }
  }

  blockUser(): void {
    const account = this.store.selectSnapshot(UserState.viewableAccount);
    this.dialog.open(BlockedDialogComponent, {
      data: {
        account,
        type: account.isBlocked ? 'unblock' : 'block',
      },
    });
  }

  async sharePhoto() {
    const showErrorMessage = () => {
      this.translateService.get('ERRORS.NO_CAMERA').subscribe(message => {
        this.matSnackBar.open(message);
      });
    };

    if (Capacitor.isPluginAvailable('Camera')) {
      try {
        await navigator.mediaDevices.getUserMedia({video: true});
      } catch (error) {
        showErrorMessage();
      }

      try {
        const photo = await Camera.getPhoto({
          quality: 90,
          direction: CameraDirection.Front,
          allowEditing: true,
          resultType: CameraResultType.DataUrl
        });

        const mimeType = photo.dataUrl.match(/^data:([^;]+);/)[1];
        const body = await fetch(photo.dataUrl);
        const buffer = await body.arrayBuffer();
        const file = new File([buffer], 'photo.jpg', {type: mimeType});

        this.tryToPostFile(file);
      } catch (error) {
        console.error('error during Camera plugin call', error);
      }
    } else {
      showErrorMessage();
    }
  }

  shareDocument(): void {

  }

  onScroll(): void {
    console.log('scroll evt')
    this.page++;
    this.store.dispatch(new ChatActions.LoadCurrentChatMessages(this.page, this.pageSize, true));
  }

  reportDialog(tempRef): void {
    this.templateDialogRef = this.dialog.open(tempRef);
  }

  addLinebreak(event: KeyboardEvent) {
    console.log('enter event')
    this.newMessageText = Utils.replaceInputSelectionText(event.target as HTMLTextAreaElement, '\n');
  }

  onFileChange(event: Event) {
    const input = event.target as HTMLInputElement;
    const file = input.files[0];

    if (file) {
      this.tryToPostFile(file);
    }

    input.files = null;
    input.value = null;
  }

  tryToPostFile(file: File) {
    if (file.size > maxFileSize) {
      const num = constants.MAX_MESSAGE_FILE_SIZE_MB;
      this.translateService.get('ERRORS.MAX_MESSAGE_FILE_SIZE', { num }).subscribe(message => {
        this.matSnackBar.open(message);
      });
    } else {
      this.store.dispatch(new ChatActions.SendAttachmentToChat(this.chatId, file)).pipe(
        delay(100),
        takeUntil(this.destroy$),
      ).subscribe(() => {
        window.scrollTo(0, this.scrollElem.scrollHeight);
      });
    }
  }

  onStreamStarted() {
    if (this.autoscrollEnabled) {
      window.scrollTo(0, this.scrollElem.scrollHeight);
    }
  }

  openDialog(templateRef: TemplateRef<any>, url?: string) {
    this.dialog.open(templateRef);
  }

  openImageDialog(templateRef: TemplateRef<any>, url: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      imageUrl: url
    }

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '90vh'

    this.dialog.open(templateRef, dialogConfig);
  }

  canDeactivate() {
    if (this.newMessageText) {
      return this.translateService.get('DIALOGS.DISCARD_MESSAGE.TITLE').pipe(
        switchMap(text => {
          return this.dialog.open(SaveDialogComponent, { data: { text } }).afterClosed();
        }),
        map(yes => !!yes),
        take(1),
      );
    } else {
      return of(true);
    }
  }

  goToChatmatePage() {
    const chatroom = this.store.selectSnapshot(ChatState.currentChatroom);

    if (chatroom.link) {
      this.router.navigateByUrl(chatroom.link);
    } else {
      console.error('Cant navigate for chatroom users', chatroom.chatroom_users);
    }
  }

  isAttachmentExpired(date: string): boolean {
    const expiryDate = +new Date(date);
    const now = Date.now();

    const isExpired = now > expiryDate;

    return isExpired;
  }

  trackBy(index: number, message: any) {
    return message.id;
  }
}
