import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { MessageDetailComponent } from './message-detail.component';
import { CanDeactivateGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    component: MessageDetailComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageDetailRouting {}
