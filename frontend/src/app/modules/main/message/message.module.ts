import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { MessageRouting } from './message.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { PickerModule } from '@ctrl/ngx-emoji-mart';

// Components
import { MaterialModule } from '../../../shared/material.module';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    MessageRouting,
    FormsModule,
    ReactiveFormsModule,
    PickerModule,
    SatPopoverModule,
    MaterialModule,
    SharedModule,
  ]
})
export class MessageModule { }
