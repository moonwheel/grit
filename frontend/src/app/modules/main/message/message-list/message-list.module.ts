import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { MessageListRouting } from './message-list.routing';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { MessageListComponent } from './message-list.component';


@NgModule({
  declarations: [
    MessageListComponent,
  ],
  imports: [
    CommonModule,
    MessageListRouting,
    SharedModule,
    InfiniteScrollModule,
  ]
})
export class MessageListModule { }
