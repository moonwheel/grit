import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { VisibleState } from 'src/app/store/state/visible.state';
import { Observable } from 'rxjs';
import { ChatState } from 'src/app/store/state/chat.state';
import { ChatroomModel } from 'src/app/shared/models/chat';
import { Router } from '@angular/router';
import { ChatActions } from 'src/app/store/actions/chat.actions';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { constants } from 'src/app/core/constants/constants';
import { UserState } from 'src/app/store/state/user.state';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: [ './message-list.component.scss' ],
})

export class MessageListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(ChatState.loading) loading$: Observable<boolean>;
  @Select(ChatState.chatrooms) chatrooms$: Observable<ChatroomModel[]>;
  @Select(ChatState.total) total$: Observable<number>;
  @Select(ChatState.lazyLoading) lazyLoading$: Observable<boolean>;

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';
  navigating = false;

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;

  constructor(private store: Store,
              private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new ChatActions.Load());
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new ChatActions.Load(this.page, this.pageSize));
    });
  }

  isChatRead(chatroom: ChatroomModel): boolean {
    return Boolean(
      chatroom.chatroomUser.last_read_message
      && chatroom.chatroomUser.last_read_message.id === chatroom.last_message.id
    );
  }

}
