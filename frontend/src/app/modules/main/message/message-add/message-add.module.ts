import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { MessageAddRouting } from './message-add.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// Components
import { MessageAddComponent } from './message-add.component';


@NgModule({
  declarations: [
    MessageAddComponent,
  ],
  imports: [
    CommonModule,
    MessageAddRouting,
    InfiniteScrollModule,
    SharedModule,
  ]
})
export class MessageAddModule { }
