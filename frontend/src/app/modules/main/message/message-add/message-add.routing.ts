import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { MessageAddComponent } from './message-add.component';

const routes: Routes = [
  {
    path: '',
    component: MessageAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageAddRouting {}
