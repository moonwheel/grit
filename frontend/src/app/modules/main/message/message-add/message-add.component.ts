import { Component, OnDestroy, OnInit, QueryList, ViewChildren, AfterViewInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { ChatActions } from 'src/app/store/actions/chat.actions';
import { UserState } from 'src/app/store/state/user.state';
import { AccountModel } from 'src/app/shared/models';
import { Observable, of, EMPTY } from 'rxjs';
import { NgModel } from '@angular/forms';
import { filter, map, switchMap, skip, debounceTime, takeUntil, startWith } from 'rxjs/operators';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { ChatState } from 'src/app/store/state/chat.state';

@Component({
  selector: 'app-message-add',
  templateUrl: './message-add.component.html',
  styleUrls: [ './message-add.component.scss' ],
})

export class MessageAddComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;
  @Select(ChatState.searchResults) searchResults$: Observable<AccountModel[]>;
  @Select(ChatState.searchLoading) loading$: Observable<boolean>;

  @ViewChildren('searchInput', { read: NgModel }) searchInput: QueryList<NgModel>;

  total$ = this.loggedInAccount$.pipe(
    filter(item => Boolean(item)),
    map(account => account.followingsCount)
  );

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  constructor(
    private navigationService: NavigationService,
    private store: Store,
  ) {
    super();
  }

  ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleSearch: false }));
    this.store.dispatch(new ChatActions.SearchUsersForNewChat());
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new ChatActions.SearchUsersForNewChat(this.searchValue, this.page, this.pageSize));
    });

    this.searchInput.changes.pipe(
      startWith(this.searchInput),
      switchMap(() => this.searchInput.first ? of(this.searchInput.first) : EMPTY),
      switchMap((model: NgModel) => model.valueChanges),
      skip(1),
      debounceTime(200),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      this.page = 1;
      this.store.dispatch(new ChatActions.SearchUsersForNewChat(this.searchValue, this.page, this.pageSize));
    });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.store.dispatch(new ToggleVisible({ isVisibleMain: true, isVisibleSearch: true }));
  }

  back(): void {
    this.navigationService.back();
  }

  searchFollowings() {
    this.store.dispatch(new ChatActions.SearchUsersForNewChat(this.searchValue));
  }

  onSearchKeypress(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.searchFollowings();
    }
  }

}
