import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { SearchRouting } from './search.routing';
import { SharedModule } from '../../../shared/shared.module';

// Components
import { SearchListComponent } from './search-list/search-list.component';

@NgModule({
  declarations: [
    SearchListComponent,
  ],
  imports: [
    CommonModule,
    SearchRouting,
    SharedModule,
  ]
})
export class SearchModule { }
