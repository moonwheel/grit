import { Component, OnInit, AfterViewInit, ViewChildren, ViewChild, ElementRef, QueryList, HostListener } from '@angular/core';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { SearchActions, SearchEntityType } from 'src/app/store/actions/search.actions';
import { SearchState, SearchItemModel } from 'src/app/store/state/search.state';
import { Observable } from 'rxjs';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { Utils } from 'src/app/shared/utils/utils';
import { switchMap, tap, takeUntil } from 'rxjs/operators';
import { AccountModel } from 'src/app/shared/models';
import { UserState } from 'src/app/store/state/user.state';
import { FollowUser } from 'src/app/store/actions/user.actions';
import { UnfollowDialogComponent } from 'src/app/shared/components/actions/follow/unfollow-dialog/unfollow-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { constants } from 'src/app/core/constants/constants';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-search',
  templateUrl: './search-list.component.html',
  styleUrls: [ './search-list.component.scss' ],
})

export class SearchListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(SearchState.loading) loading$: Observable<boolean>;
  @Select(UserState.loggedInAccount) loggedInAccount$: Observable<AccountModel>;
  @Select(SearchState.lazyLoading) lazyLoading$: Observable<boolean>;
  @Select(SearchState.localLoading) localLoading$: Observable<boolean>;

  @ViewChildren(RouterLinkWithHref) links: QueryList<RouterLinkWithHref>;
  @ViewChild('tabs') tabsElem: ElementRef<HTMLDivElement>;
  @ViewChildren(MatMenuTrigger) trigger: QueryList<MatMenuTrigger>

  items$ = this.route.queryParams.pipe(
    switchMap(params => this.store.select(SearchState.items(params.type))),
  );

  users$ = this.store.select(SearchState.items('account'));

  photos$ = this.store.select(SearchState.items('photo'));

  total$ = this.route.queryParams.pipe(
    switchMap(params => this.store.select(SearchState.total(params.type)))
  );

  photosTotal$ = this.store.select(SearchState.total('photo'));

  searchText = '';
  location = false;
  searchType: SearchEntityType = 'all';

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  currentUrl = '';

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(private route: ActivatedRoute,
              private store: Store,
              private dialog: MatDialog,
  ) {
    super();
  }

  @HostListener('window:scroll', [])
  handleScroll() {
    this.trigger.toArray().forEach(trigger => trigger.closeMenu())
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.location = params.location === 'true';

      this.page = 1;
      this.search(params);

      const term = Utils.replacePlusesWithSpaces(params.q);

      if (params.type === 'all' && params.type !== 'photo') {
        this.store.dispatch(new SearchActions.Search(term, 'photo', 1, 10, true, this.location));
      }

      if (params.type === 'all' && params.type !== 'account') {
        this.store.dispatch(new SearchActions.Search(term, 'account', 1, 3, true, this.location));
      }
    });

    this.currentUrl = environment.appUrl;
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      const params = this.route.snapshot.queryParams;
      this.search(params, true);
    });

    this.scrollToSelectedTab();
  }

  isFollowing(account: AccountModel): boolean {
    return this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));
  }

  follow(account: AccountModel) {
    const isFollowing = this.store.selectSnapshot(UserState.isCachedFollowing(account.id, account.following));

    if (!isFollowing) {
      this.store.dispatch(new FollowUser(account.id));
    } else {
      this.dialog.open(UnfollowDialogComponent, { data: { account } });
    }
  }

  onScroll() {
    const params = this.route.snapshot.queryParams;
    this.search(params);
  }

  report(item: SearchItemModel, type: string) {
    // TODO: implement report when ready
    console.warn('Method not implemented');
  }

  private search(params: any, lazy = false) {
    if ('q' in params) {
      this.searchText = Utils.replacePlusesWithSpaces(params.q);
      this.searchType = params.type;
      if (this.viewport) {
        this.viewport.scrollToIndex(0);
      }
      this.store.dispatch(new SearchActions.Search(this.searchText, this.searchType, this.page, this.pageSize, false, this.location, lazy));
    }
  }

  private scrollToSelectedTab() {
    setTimeout(() => {
      this.links.forEach((routerLink, index) => {
        const isActive = routerLink.queryParams.type === this.searchType;
        if (isActive && this.tabsElem) {
          const tabs = this.tabsElem.nativeElement;
          const tab = tabs.children[index] as HTMLElement;
          const paddingLeft = parseFloat(getComputedStyle(tabs).paddingLeft);
          const offset = tab.offsetLeft - paddingLeft;
          const widthWithTab = offset + tab.offsetWidth + paddingLeft * 2;
          const allTabsWidth = tabs.offsetWidth;

          if (widthWithTab > allTabsWidth) {
            this.tabsElem.nativeElement.scrollTo(offset, 0);
          }
        }
      });
    });
  }

}
