import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MenuListComponent } from './menu-list/menu-list.component';

// Resolvers
import { BusinessListResolver } from 'src/app/core/resolvers';

const routes: Routes = [
  {
    path: '',
    component: MenuListComponent,
    data: { preload: true },
    resolve: { business: BusinessListResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRouting {
}
