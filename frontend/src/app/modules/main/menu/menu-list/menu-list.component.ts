import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Share, Device } from '@capacitor/core';

import { Select, Store } from '@ngxs/store';

import { combineLatest, Observable, Subject } from 'rxjs';
import { takeUntil, filter, switchMap, take, debounceTime, switchMapTo, map } from 'rxjs/operators';

import { UserModel } from 'src/app/shared/models/user/user.model';
import { UserState } from 'src/app/store/state/user.state';
import { ChangeAccount, Logout } from 'src/app/store/actions/auth.actions';
import { environment } from 'src/environments/environment';
import { BusinessState } from '../../../../store/state/business.state';
import { AccountModel } from 'src/app/shared/models';
import { Utils } from 'src/app/shared/utils/utils';
import { RestoreBusiness } from 'src/app/store/actions/business.actions';
import { TranslateService } from '@ngx-translate/core';
import { constants } from 'src/app/core/constants/constants';

@Component( {
  selector: 'app-menu',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss'],
} )

export class MenuListComponent implements OnInit, OnDestroy {
  @Select(UserState.loggedInAccount)
  loggedInAccount$: Observable<AccountModel>;

  @Select(UserState.loggedInAccountPageName)
  loggedInAccountPageName$: Observable<string>;

  @Select(UserState.loggedInAccountRoleType)
  loggedInAccountRoleType$: Observable<string>;

  @Select(BusinessState.businessList)
  businessList$: Observable<AccountModel[]>;

  canEditSettings$ = this.store.select(UserState.loggedInAccountRoleType).pipe(
    map((loggedInAccountRoleType) => !loggedInAccountRoleType || loggedInAccountRoleType === 'admin'),
  );

  environment = environment;

  user: UserModel;

  pageUrl: string;
  pageName: string;

  showGrit = false;
  accountList: AccountModel[] = [];
  selected: AccountModel;
  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  accountChanging = false;

  private destroy$ = new Subject();

  constructor(
    private router: Router,
    private store: Store,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.loggedInAccount$.pipe(
      takeUntil(this.destroy$),
      filter(item => !!item),
    ).subscribe(loggedInAccount => {
      const pageName = Utils.getAccountPageName(loggedInAccount);
      this.pageName = pageName;
      this.pageUrl = `/${pageName}`;
    });

    this.route.data.pipe(
      takeUntil(this.destroy$),
      switchMap(() => this.businessList$)
    ).subscribe(data => {
      this.getBusinessAccounts(data);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getBusinessAccounts( businessData: AccountModel[] ): void {
    this.loggedInAccount$.pipe(
      filter(item => !!item),
      take(1),
    ).subscribe(loggedInAccount => {
      this.accountList = businessData.map( account => {
        if (account.id === loggedInAccount.id) {
          account = loggedInAccount;
        }

        return Utils.extendAccountWithBaseInfo(account);
      });

      this.selected = this.accountList.find(item => +item.id === +loggedInAccount.id);
    });
  }

  switchAccount(account: AccountModel): void {
    this.accountChanging = true;
    this.store.dispatch(new ChangeAccount(account)).subscribe({
      complete: () => {
        this.selected = this.accountList.find(item => item.id === account.id);
        this.accountChanging = false;
      }
    });
    this.pageUrl = `/${account.pageName}`;
  }

  checkCountAccount( countAccounts ): void {
    if ( countAccounts <= constants.MAX_BUSINESSES_NUMBER ) {
      this.router.navigate( ['/register'], {queryParams: {type: 'business'}} );
    } else {
      const num = constants.MAX_BUSINESSES_NUMBER;
      this.translateService.get('ERRORS.MAX_BUSINESSES', { num }).subscribe(message => {
        this.snackBar.open(message, '', { duration: 3000 });
      });
    }
  }

  logOut(): void {
    this.store.dispatch( new Logout() ).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

  generatePageUrl( path: string ): string {
    return `${ this.pageUrl }/${ path }`;
  }

  goTo(path: string) {
    combineLatest([
      this.store.select(UserState.loading),
      this.store.select(BusinessState.loading),
    ]).pipe(
      debounceTime(100),
      filter(([userLoading, businessLoading]) => !userLoading && !businessLoading),
      take(1),
      switchMapTo(
        this.store.select(UserState.loggedInAccountPageName).pipe(
          filter(item => Boolean(item)),
          take(1),
        )
      ),
      takeUntil(this.destroy$),
    ).subscribe(pageUrl => {
      this.pageUrl = pageUrl;
      const url = this.generatePageUrl(path);
      this.router.navigateByUrl(url);
    });
  }

  navigateTo(path: string) {
    this.router.navigateByUrl(path);
  }

  async inviteFriends() {
    try {
      const deviceInfo = await Device.getInfo();
      const params = { pageName: this.pageName };
      const text = await this.translateService.get('DIALOGS.INVITE_FRIENDS.TEXT', params).toPromise();
      const title = await this.translateService.get('DIALOGS.INVITE_FRIENDS.TITLE').toPromise();
      const url = environment.appUrl;

      if (deviceInfo.platform === 'web') {
        await (navigator as any).share({
          title,
          text,
          url,
        }, {
          print: false,
          sms: false,
        });
      } else {
        await Share.share({
          dialogTitle: title,
          text,
          url,
        });
      }
    } catch (e) {
      console.warn('Invite error: ', e);
    }
  }

  onAccountOptionClick(event: Event, account: AccountModel, select: MatSelect) {
    if (account.deleted) {
      event.preventDefault();
      event.stopPropagation();
      event.stopImmediatePropagation();

      select.close();
    }
  }

  restoreBusiness(account: AccountModel, select: MatSelect) {
    select.close();

    if (account.business) {
      this.accountChanging = true;

      this.store.dispatch(new RestoreBusiness(account.business.reactivationtoken)).subscribe(() => {
        this.switchAccount(account);
      }, error => {
        this.accountChanging = false;
      });
    }
  }
}
