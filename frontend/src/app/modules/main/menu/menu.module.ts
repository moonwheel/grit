import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { MenuRouting } from './menu.routing';
import { SharedModule } from '../../../shared/shared.module';

// Components
import { MenuListComponent } from './menu-list/menu-list.component';

@NgModule({
  declarations: [
    MenuListComponent,
  ],
  imports: [
    CommonModule,
    MenuRouting,
    SharedModule
  ]
})
export class MenuModule { }
