import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, combineLatest } from 'rxjs';

import {
  ApproveFollowingRequest,
  RejectFollowingRequest,
  GetFollowRequests,
  ToggleNotificationSetting
} from 'src/app/store/actions/user.actions';
import { NotificationActions } from 'src/app/store/actions/notification.actions';
import { NotificationState, NOTIFICATIONS_PAGE_SIZE } from 'src/app/store/state/notifications.state';
import { NotificationModel, AccountModel } from 'src/app/shared/models';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { UserState } from 'src/app/store/state/user.state';
import { map } from 'rxjs/operators';
import { constants } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-notification',
  templateUrl: './notification-list.component.html',
  styleUrls: [ './notification-list.component.scss' ]
})

export class NotificationListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(UserState.followRequests)
  followRequests$: Observable<any>;

  @Select(UserState.loggedInAccount)
  loggedInAccount$: Observable<AccountModel>;

  @Select(UserState.followRequestsCount)
  followRequestsCount$: Observable<number>;

  @Select(NotificationState.items)
  notifications$: Observable<NotificationModel[]>;

  @Select(NotificationState.total)
  total$: Observable<number>;

  @Select(NotificationState.lazyLoading)
  lazyLoading$: Observable<boolean>;

  loading$ = combineLatest([
    this.store.select(NotificationState.loading),
    this.store.select(UserState.loadingFollow),
  ]).pipe(
    map(([notificationsLoading, requestsLoading]) => notificationsLoading || requestsLoading)
  );

  page = 1;
  pageSize = NOTIFICATIONS_PAGE_SIZE;

  defaultAvatar = constants.PLACEHOLDER_AVATAR_PATH;
  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(private store: Store) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new GetFollowRequests());
    this.store.dispatch(new NotificationActions.MarkAllAsRead());
    this.store.dispatch(new NotificationActions.Load(this.page, this.pageSize)).subscribe(() => {
      this.store.dispatch(new NotificationActions.MarkAllAsRead());
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new NotificationActions.Load(this.page, this.pageSize, true)).subscribe(() => {
        this.store.dispatch(new NotificationActions.MarkAllAsRead());
      });
    });
  }

  approve(followId: number) {
    this.store.dispatch(new ApproveFollowingRequest(followId));
  }

  reject(followId: number) {
    this.store.dispatch(new RejectFollowingRequest(followId));
  }

  loadMoreRequests() {
    const requests = this.store.selectSnapshot(UserState.followRequests);
    this.store.dispatch(new GetFollowRequests(requests.length, 10));
  }

  toggleNotification(key: any) {
    this.store.dispatch(new ToggleNotificationSetting(key));
  }

  getNotificationText(notification: any) {
    const result = `PAGES.NOTIFICATION_LIST.NOTIFICATION_TEXT.${notification.notifications.action.toUpperCase()}`;

    switch (notification.notifications.action) {
      case 'follows':
      case 'tagged':
      case 'ordered':
      case 'shipped':
      case 'booked':
        return result;

      default:
        return `${result}_${notification.notifications.content.display.name.toUpperCase()}`;
    }
  }

  trackBy(entity: any) {
    return entity.id;
  }
}
