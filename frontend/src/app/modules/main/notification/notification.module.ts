import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { NotificationRouting } from './notification.routing';
import { SharedModule } from '../../../shared/shared.module';

// Components
import { NotificationListComponent } from './notification-list/notification-list.component';

@NgModule({
  declarations: [
    NotificationListComponent,
  ],
  imports: [
    CommonModule,
    NotificationRouting,
    SharedModule,
  ]
})
export class NotificationModule { }
