import { NgModule } from '@angular/core';

// Modules
import { MainRouting } from './main.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [],
  imports: [
    MainRouting,
    SharedModule,
  ],
})
export class MainModule { }
