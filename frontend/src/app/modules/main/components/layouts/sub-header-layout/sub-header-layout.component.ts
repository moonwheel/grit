import { Component } from "@angular/core";

@Component({
    selector: 'app-sub-header-layout',
    templateUrl: './sub-header-layout.component.html',
    styleUrls: ['./sub-header-layout.component.scss'],
})
export class SubHeaderLayoutComponent {}