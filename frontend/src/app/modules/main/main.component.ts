import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { NavigationEnd, Router, ActivatedRoute, UrlSerializer, UrlTree, DefaultUrlSerializer } from '@angular/router';
import { Select, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { Observable, Subject, fromEvent, of, Subscription, combineLatest } from 'rxjs';
import { VisibleState, VisibleStateModel } from '../../store/state/visible.state';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { takeUntil, debounceTime, map, distinctUntilChanged, switchMap, filter, startWith, switchMapTo, tap, share } from 'rxjs/operators';
import { SearchActions } from 'src/app/store/actions/search.actions';
import { SearchState } from 'src/app/store/state/search.state';
import { UserState } from 'src/app/store/state/user.state';
import { CartState } from 'src/app/store/state/cart.state';
import { Utils } from 'src/app/shared/utils/utils';
import { ChatState } from 'src/app/store/state/chat.state';
import { NotificationState } from 'src/app/store/state/notifications.state';
import { NotificationActions } from 'src/app/store/actions/notification.actions';
import { ScrollObservableService } from 'src/app/shared/services/scroll-observable.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {

  @Select(VisibleState.visible) visibleBlocks$: Observable<VisibleStateModel>;
  @Select(SearchState.suggestions) suggestions$: Observable<string[]>;
  @Select(UserState.followRequestsCount) followRequestsCount$: Observable<number>;
  @Select(CartState.total) cartTotal$: Observable<number>;
  @Select(ChatState.unreadChatsCount) unreadChatsCount$: Observable<number>;

  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  notificationsCount$ = this.actions$.pipe(
    ofActionSuccessful(NotificationActions.GetUnreadCount),
    switchMapTo(combineLatest([
      this.store.select(UserState.followRequestsCount),
      this.store.select(NotificationState.unread),
      this.store.select(UserState.loggedInAccount).pipe(
        filter(item => !!item)
      ),
    ])),
    map(([requests, notifications, loggedInAccount]) => {
      if (!loggedInAccount.followers_notification) {
        requests = 0;
      }
      return requests + notifications;
    }),
    debounceTime(100),
    share(),
  );

  globalBack = false;

  searchTabs = false;

  currentUrl = this.router.url;

  isVisibleSearch = true;
  isVisibleMain = true;

  value: string;
  searchType = 'all';

  showSearchBackdrop = false;

  private searchInputSubscription: Subscription;

  private destroy$ = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private store: Store,
    private actions$: Actions,
    private scrollService: ScrollObservableService,
  ) { }

  ngOnInit() {
    this.visibleBlocks$.pipe(
      takeUntil(this.destroy$),
    ).subscribe(dataVisible => {
      if (dataVisible) {
        // Promise.resolve is used to avoid 'expression was changed after checked' error
        Promise.resolve().then(() => {
          this.isVisibleSearch = dataVisible.isVisibleSearch;
          this.isVisibleMain = dataVisible.isVisibleMain && !dataVisible.isKeyboardShown;
        });
      }
    });

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      startWith(null),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      const url = this.router.url;

      const showGlobal = [
        /\/search/,
        /\/.+\/photos\/\d+/,
        /\/.+\/videos\/\d+/,
        /\/.+\/blog\/\d+/,
        /\/.+\/shop\/\d+/,
        /\/.+\/services\/\d+/,
        /\/purchases/,
        /\/sales/,
        /\/saved/,
        /\/following/,
        /\/blocked/,
        /\/texts/,
        /\/team/,
        /\/wallet/
      ].some(item => url.match(item));

      if (showGlobal) {
        this.globalBack = true;
        this.searchTabs = true;
      } else {
        this.globalBack = false;
        this.searchTabs = false;
      }
    });

    this.route.queryParams.pipe(
      takeUntil(this.destroy$),
    ).subscribe(params => {
      if ('q' in params) {
        this.value = Utils.replacePlusesWithSpaces(params.q);
        this.searchType = params.type || 'all';
      }
    });
  }

  onSearchKeypress(event: KeyboardEvent) {
    if (event.key === 'Enter' && this.value) {
      this.goToSuggestion(this.value);
      this.searchInput.nativeElement.blur();
      this.resetSuggestions();
    }
  }

  onSearchInputFocus() {
    const input = this.searchInput.nativeElement;

    this.showSearchBackdrop = true;
    this.store.dispatch(new SearchActions.GetAutoSuggestions(input.value, this.searchType));

    this.searchInputSubscription = fromEvent<Event>(input, 'keyup').pipe(
      debounceTime(100),
      map(event => (event.target as HTMLInputElement).value),
      distinctUntilChanged(),
      switchMap(value => of(value)),
      switchMap(value => this.store.dispatch(new SearchActions.GetAutoSuggestions(value, this.searchType))),
      takeUntil(this.destroy$),
    ).subscribe();
  }

  onContentScroll(event) {
    this.scrollService.handleScroll(event);
  }

  resetSuggestions() {
    this.showSearchBackdrop = false;

    if (this.searchInputSubscription) {
      this.searchInputSubscription.unsubscribe();
    }
  }

  search(): void {
    // this.router.navigateByUrl('/search');
    // this.globalBack = true;
    // this.searchTabs = true;
  }

  clearSearch() {
    this.value = '';
    this.resetSuggestions();
  }

  back(): void {
    this.navigationService.back();
  }

  goToSuggestion(suggestion: string) {
    this.router.navigate(['/search'], {
      queryParams: {
        type: this.searchType,
        q: Utils.replaceSpacesWithPluses(suggestion),
      }
    });
    this.resetSuggestions();
  }

  getContentClass(isVisibleMain: boolean, isVisibleSearch: boolean): Record<string, boolean> {
    const styles: Record<string, boolean> = {};

    if(isVisibleMain && isVisibleSearch) {
      styles['include-full-layout'] = true;
      return styles;
    }

    if(isVisibleMain) {
      styles['with-main-tabs'] = true;
      return styles;
    }

    if(isVisibleSearch) {
      styles['with-search'] = true;
      return styles;
    }

    return styles;
  }
}
