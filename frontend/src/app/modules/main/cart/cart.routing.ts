import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./cart-list/cart-list.module').then(m => m.CartListModule) },
  { path: 'checkout', loadChildren: () => import('./cart-checkout/cart-checkout.module').then(m => m.CartCheckoutModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRouting {
}
