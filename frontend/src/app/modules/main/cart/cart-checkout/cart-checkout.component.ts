import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { CartState } from 'src/app/store/state/cart.state';
import { InitCart, DeleteFromCart, CheckoutCart, EditInCart } from 'src/app/store/actions/cart.actions';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { ToggleVisible } from 'src/app/store/actions/visible.actions';

@Component({
  selector: 'app-cart-checkout',
  templateUrl: './cart-checkout.component.html',
  styleUrls: ['./cart-checkout.component.scss'],
})

export class CartCheckoutComponent implements OnInit, OnDestroy {

  @Select(CartState.cart)
  cart$: Observable<any[]>;

  @Select(CartState.loading)
  loading$: Observable<boolean>;

  @Select(CartState.totalPrice)
  totalPrice$: Observable<number>;

  checkoutForm: FormGroup;

  quantities = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

  collectIds: number[] = [];

  total = 200;

  constructor(
    private navigationService: NavigationService,
    private formBuilder: FormBuilder,
    private store: Store,
    public dialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: false, isVisibleSearch: false }));
    this.store.dispatch(new InitCart());

    this.checkoutForm = this.formBuilder.group({
      address: ['', Validators.required],
      bank: '',
    });
  }

  public ngOnDestroy(): void {
    this.store.dispatch(new ToggleVisible({ isVisibleMain: true, isVisibleSearch: true }));
  }

  back() {
    this.navigationService.back();
  }

  showVariantData(data: any) {
    if (!data) {
      return '';
    }
    return [ data.size, data.color, data.flavor, data.material ].filter(Boolean).join(', ');
  }

  changeQuantity(id: number, product_id: number, quantity: number) {
    this.store.dispatch(new EditInCart({
      id,
      product_id,
      quantity,
    }));
  }

  removeItem(id: number, event: Event) {
    event.stopPropagation();
    this.store.dispatch(new DeleteFromCart(id));
  }

  checkoutCart() {
    this.store.dispatch(new CheckoutCart({
      bank_id: this.checkoutForm.controls.bank.value,
      address_id: this.checkoutForm.controls.address.value,
      collect_ids: [],
    }));
  }
}
