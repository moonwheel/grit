import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { CartCheckoutComponent } from './cart-checkout.component';

const routes: Routes = [
  {
    path: '',
    component: CartCheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartCheckoutRouting {}
