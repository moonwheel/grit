import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { CartCheckoutRouting } from './cart-checkout.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { CartCheckoutComponent } from './cart-checkout.component';


@NgModule({
  declarations: [
    CartCheckoutComponent,
  ],
  imports: [
    CommonModule,
    CartCheckoutRouting,
    SharedModule,
  ]
})
export class CartCheckoutModule { }
