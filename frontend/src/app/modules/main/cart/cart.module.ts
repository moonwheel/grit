import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { CartRouting } from './cart.routing';

// Components
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    CartRouting,
    SharedModule,
  ]
})
export class CartModule { }
