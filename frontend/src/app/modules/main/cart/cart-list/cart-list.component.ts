import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { Observable } from 'rxjs';

import { InitCart, EditInCart, DeleteFromCart, CheckoutCart, LoadCart, ResetCart } from 'src/app/store/actions/cart.actions';
import { CartState } from 'src/app/store/state/cart.state';
import { CartProductModel, ProductModel, AccountModel } from 'src/app/shared/models';
import { PaymentsState } from 'src/app/store/state/payment.state';
import { takeUntil } from 'rxjs/operators';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { ResultDialogComponent } from 'src/app/shared/components/result-dialog/result-dialog.component';
import { Router } from '@angular/router';
import { constants } from 'src/app/core/constants/constants';
import { ScrollObservableService } from 'src/app/shared/services/scroll-observable.service';
import { GetWallet } from 'src/app/store/actions/payment.actions';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss'],
})

export class CartListComponent extends AbstractInfiniteListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(CartState.cart)
  cart$: Observable<CartProductModel[]>

  @Select(CartState.total)
  total$: Observable<number>;

  @Select(CartState.loading)
  loading$: Observable<boolean>;

  @Select(CartState.totalPrice)
  totalPrice$: Observable<number>;

  @Select(CartState.productsPrice)
  productsPrice$: Observable<number>;

  @Select(CartState.shippingPrice)
  shippingPrice$: Observable<number>;

  @Select(PaymentsState.balance)
  balance$: Observable<number>;

  @Select(CartState.lazyLoading)
  lazyLoading$: Observable<boolean>;

  @ViewChild('totalPriceElement')
  totalPriceElement: ElementRef;

  @ViewChild('cartItemsElement')
  cartItemsElement: ElementRef;

  checkoutForm: FormGroup;

  quantities = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

  filter = '';
  searchValue = '';
  page = 1;
  pageSize = 1;

  total = 0;

  constructor(
    private store: Store,
    private actions$: Actions,
    private scrollService: ScrollObservableService,
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    public renderer: Renderer2,
    private changeDetector : ChangeDetectorRef,
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new InitCart());

    this.store.dispatch(new GetWallet());

    this.checkoutForm = this.formBuilder.group({
      delivery_address: ['', Validators.required],
      is_billing: false,
      billing_address: '',
      payment: [null, Validators.required],
      grit_wallet: false,
    });

    this.checkoutForm.controls.is_billing.valueChanges.subscribe(value => {
      if (value) {
        this.checkoutForm.controls.billing_address.enable();
        this.checkoutForm.controls.billing_address.setValidators(Validators.required);
      } else {
        this.checkoutForm.controls.billing_address.disable();
        this.checkoutForm.controls.billing_address.setValidators(null);
      }
      this.checkoutForm.controls.billing_address.updateValueAndValidity();
    });

    this.checkoutForm.controls.grit_wallet.valueChanges.subscribe(value => {
      if (value) {
        this.checkoutForm.controls.payment.setValidators(null);
      } else {
        this.checkoutForm.controls.payment.setValidators(Validators.required);
      }
      this.checkoutForm.controls.payment.updateValueAndValidity();
    });

    this.actions$.pipe(
      ofActionSuccessful(CheckoutCart),
      takeUntil(this.destroy$),
    ).subscribe(data => {
      return this.dialog.open(ResultDialogComponent, {
        data: { message: 'Thanks for your order' },
      }).afterClosed().subscribe(() => {
        this.store.dispatch(new ResetCart());
        this.router.navigateByUrl('/purchases');
      });
    });

    this.actions$.pipe(
      ofActionErrored(CheckoutCart),
      takeUntil(this.destroy$),
    ).subscribe(data => {
      return this.dialog.open(ResultDialogComponent, {
        data: {
          message: 'Error',
        },
      });
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.store.dispatch(new LoadCart(this.page, this.pageSize));
    });

    this.changeDetector.detectChanges();

    const stickyBlock = this.totalPriceElement.nativeElement.offsetTop;
    const blockOffset = 68;

    this.scrollService.scrolled$.subscribe(event => {
      if (event.target.scrollTop >= stickyBlock + blockOffset) {
        this.renderer.addClass(this.totalPriceElement.nativeElement, 'fixed');
        this.renderer.addClass(this.cartItemsElement.nativeElement, 'fixed');
      } else {
        this.renderer.removeClass(this.totalPriceElement.nativeElement, 'fixed');
        this.renderer.removeClass(this.cartItemsElement.nativeElement, 'fixed');
      }
    });
  }
  
  showVariantData(data: any) {
    if (!data) {
      return '';
    }
    return [ data.size, data.color, data.flavor, data.material ].filter(Boolean).join(', ');
  }

  changeQuantity(id: number, product_id: number, quantity: number) {
    this.store.dispatch(new EditInCart({
      id,
      product_id,
      quantity,
    }));
  }

  changeCollected(id: number, collected: boolean) {
    this.store.dispatch(new EditInCart({
      id,
      collected,
    }));
  }

  removeItem(id: number, event: Event) {
    event.stopPropagation();
    this.store.dispatch(new DeleteFromCart(id));
  }

  checkoutCart() {
    const form = this.checkoutForm;
    const values = form.value;

    if (form.valid) {
      this.store.dispatch(new CheckoutCart({
        payment: values.payment,
        delivery_address: values.delivery_address,
        billing_address: values.billing_address,
        grit_wallet: values.grit_wallet,
      }));
    }
  }

  getProductPhoto(product: ProductModel) {
    if (product.selectedVariant && product.selectedVariant.cover) {
      return product.selectedVariant.cover.photo.thumbUrl || product.selectedVariant.cover.photo.photoUrl;
    } else if (product.photos.length) {
      return product.photos[0].thumbUrl;
    } else {
      return constants.PLACEHOLDER_NO_PHOTO_PATH;
    }
  }

  getProductUrl(product: ProductModel) {
    return `/${product.user.business ? product.user.business.pageName : product.user.person.pageName}/shop/${product.id}`;
  }
}
