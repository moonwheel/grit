import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { CartListRouting } from './cart-list.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { CartListComponent } from './cart-list.component';


@NgModule({
  declarations: [
    CartListComponent,
  ],
  imports: [
    CommonModule,
    CartListRouting,
    SharedModule,
  ]
})
export class CartListModule { }
