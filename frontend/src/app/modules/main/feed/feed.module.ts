import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { FeedRouting } from './feed.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { FeedListComponent } from './feed-list/feed-list.component';

@NgModule({
  declarations: [
    FeedListComponent,
  ],
  imports: [
    CommonModule,
    FeedRouting,
    SharedModule,
  ]
})
export class FeedModule { }
