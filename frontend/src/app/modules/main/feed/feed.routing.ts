import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { FeedListComponent } from './feed-list/feed-list.component';

const routes: Routes = [
  { path: '', component: FeedListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedRouting {
}
