import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ViewChildren,
  ElementRef,
  QueryList,
  Renderer2,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, fromEvent, interval, Subscription } from 'rxjs';
import { throttle } from 'rxjs/operators';

import { Share, Device } from '@capacitor/core';

import { VisibleState } from 'src/app/store/state/visible.state';
import { FeedActions } from 'src/app/store/actions/feed.actions';
import { FeedState } from 'src/app/store/state/feed.state';
import { AbstractEntityModel } from 'src/app/shared/models';

import { environment } from 'src/environments/environment';

import { UserState } from 'src/app/store/state/user.state';
import { AbstractFeedItemComponent } from 'src/app/shared/components';
import { TranslateService } from '@ngx-translate/core';
import { FeedItemModel } from '../../../../shared/models';

@Component({
  selector: 'app-feed',
  templateUrl: './feed-list.component.html',
  styleUrls: ['./feed-list.component.scss'],
})

export class FeedListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(UserState.loggedInAccountPageName) loggedInAccountPageName$: Observable<string>;
  @Select(UserState.loggedInAccountRoleType) loggedInAccountRoleType$: Observable<string>;
  @Select(VisibleState.isVisibleAddButton) isVisibleAddButton$: Observable<boolean>;

  @Select(FeedState.items) items$: Observable<AbstractEntityModel[]>;
  @Select(FeedState.loading) loading$: Observable<boolean>;
  @Select(FeedState.lazyLoading) lazyLoading$: Observable<boolean>;

  @ViewChildren('feedItem')
  feedItemsList: QueryList<AbstractFeedItemComponent>;

  private scroll$ = Observable.create().pipe(
    throttle(() => interval(1000)),
  );

  private scrollEvt;

  page = 1;
  pageSize = 10;

  pageName = '';
  navigating = false;

  constructor(
    private store: Store,
    private translateService: TranslateService,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {
    this.store.dispatch(new FeedActions.Load(this.page, this.pageSize));
    this.loggedInAccountPageName$.subscribe(pageName => {
      this.pageName = pageName;
    });
    // This needed to enable pull to refresh only on this view
    this.renderer.setStyle(document.documentElement, 'overflow', 'auto');
  }

  ngAfterViewInit() {
    this.scrollEvt = this.scroll$.subscribe(() => {
      this.feedItemsList.forEach(feed => {
        if (!feed.item.viewed) {
          this.store.dispatch(new FeedActions.ViewItem(feed.item.id, feed.item.tableName));
        }
      });
    });
  }

  ngOnDestroy() {
    this.scrollEvt.unsubscribe();
    this.renderer.setStyle(document.documentElement, 'overflow', null);
  }

  onScroll() {
    this.page++;
    this.store.dispatch(new FeedActions.Load(this.page, this.pageSize, true));
    this.scrollEvt.next();
  }

  async inviteFriends() {
    try {
      const deviceInfo = await Device.getInfo();
      const params = { pageName: this.pageName };
      const text = await this.translateService.get('DIALOGS.INVITE_FRIENDS.TEXT', params).toPromise();
      const title = await this.translateService.get('DIALOGS.INVITE_FRIENDS.TITLE').toPromise();
      const url = environment.appUrl;

      if (deviceInfo.platform === 'web') {
        await (navigator as any).share({
          title,
          text,
          url,
        }, {
            print: false,
            sms: false,
          });
      } else {
        await Share.share({
          dialogTitle: title,
          text,
          url,
        });
      }
    } catch (e) {
      console.warn('Invite error: ', e);
    }
  }

  trackBy(index: number, item: FeedItemModel) {
    return item.id;
  }
}
