import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MainComponent } from './main.component';
import { AuthGuard } from 'src/app/core/guards';
import { UserLoadingResolver } from 'src/app/core/resolvers';

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      {
        path: 'search',
        loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
      },
      {
        path: 'cart',
        loadChildren: () => import('./cart/cart.module').then(m => m.CartModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./notification/notification.module').then(m => m.NotificationModule),
        resolve: { user: UserLoadingResolver },
      },
      {
        path: 'discover',
        loadChildren: () => import('./discover/discover.module').then(m => m.DiscoverModule),
        resolve: { user: UserLoadingResolver },
      },
      {
        path: 'feed',
        loadChildren: () => import('./feed/feed.module').then(m => m.FeedModule),
        resolve: { user: UserLoadingResolver },
      },
      {
        path: 'chats',
        loadChildren: () => import('./message/message.module').then(m => m.MessageModule),
        resolve: { user: UserLoadingResolver },
      },
      {
        path: 'menu',
        loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule),
        data: { preload: true },
      },
      {
        path: 'reports',
        loadChildren: () => import('../grit/grit.module').then(m => m.GritModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'purchases',
        loadChildren: () => import('../user/components/orders-bookings/purchases/purchases.module').then(m => m.PurchasesModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'sales',
        loadChildren: () => import('../user/components/orders-bookings/sales/sales.module').then(m => m.SalesModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'sales/:id',
        loadChildren: () => import('../user/components/orders-bookings/order-detail-view/order-detail-view.module').then(m => m.OrderDetailModule),
        data: {
          type: 'sales'
        }
      },
      {
        path: 'wallet',
        loadChildren: () => import('../user/components/wallet/wallet.module').then(m => m.WalletModule)
      },
      {
        path: ':username',
        loadChildren: () => import('../page/page.module').then(m => m.PageModule),
        canActivate: [AuthGuard]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class MainRouting { }
