import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, from } from 'rxjs';
import { switchMap, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { DiscoverState, DiscoverLocationAutosuggestion } from '../../../../store/state/discover.state';
import { DiscoverEntityType, DiscoverActions, discoverEntityTypeSet } from 'src/app/store/actions/discover.actions';
import { AbstractInfiniteListComponent } from 'src/app/shared/components';
import { constants } from 'src/app/core/constants/constants';
import { SERVICE_CATEGORIES } from 'src/app/modules/page/services/shared';
import { PRODUCT_CATEGORIES } from 'src/app/modules/page/products/shared';
import { PhotoModel, VideoModel, ArticleModel, ProductModel, ServiceModel } from 'src/app/shared/models';
import { TranslateService } from '@ngx-translate/core';

const DEFAULT_RADIUS = 50;

@Component({
  selector: 'app-discover',
  templateUrl: './discover-list.component.html',
  styleUrls: [ './discover-list.component.scss' ],
})
export class DiscoverListComponent extends AbstractInfiniteListComponent implements OnInit, AfterViewInit {

  @Select(DiscoverState.currentEntityType) currentEntityType$: Observable<DiscoverEntityType>;
  @Select(DiscoverState.currentEntityCategory) currentEntityCategory$: Observable<string>;
  @Select(DiscoverState.locationAutosuggestions) locationAutosuggestions$: Observable<DiscoverLocationAutosuggestion[]>;
  @Select(DiscoverState.loading) loading$: Observable<boolean>;

  @Select(DiscoverState.items('photo')) photos$: Observable<PhotoModel[]>;
  @Select(DiscoverState.items('video')) videos$: Observable<VideoModel[]>;
  @Select(DiscoverState.items('article')) articles$: Observable<ArticleModel[]>;
  @Select(DiscoverState.items('product')) products$: Observable<ProductModel[]>;
  @Select(DiscoverState.items('service')) services$: Observable<ServiceModel[]>;
  @Select(DiscoverState.lazyLoading) lazyLoading$: Observable<boolean>;

  total$ = this.currentEntityType$.pipe(
    switchMap(type => this.store.select(DiscoverState.total(type)))
  );

  inputControl = new FormControl();

  page = 1;
  pageSize = 15;
  filter = 'date';
  searchValue = '';

  lat: number;
  lng: number;
  radius = DEFAULT_RADIUS;
  defaultCategory = constants.DEFAULT_MEDIA_CATEGORY;
  selectedCategory = constants.DEFAULT_MEDIA_CATEGORY;

  mediaCategories = [
    ...constants.MEDIA_CATEGORIES
  ];

  productCategories = [
    ...PRODUCT_CATEGORIES
  ];

  serviceCategories = [
    ...SERVICE_CATEGORIES
  ];

  radiuses = [ 5, 10, 25, 50, 100, 250, 500 ];

  defaultImage = constants.PLACEHOLDER_NO_PHOTO_PATH;

  constructor(private store: Store,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(paramMap => {
      let type = paramMap.get('type') as DiscoverEntityType;
      let category = paramMap.get('category');
      let address = paramMap.get('address');

      if (!type || !discoverEntityTypeSet.has(type)) {
        type = 'photo';
      }

      if (!category) {
        category = this.defaultCategory;
      } else {
        let categoriesSet: Set<string>;
        if (type === 'product') {
          categoriesSet = new Set(this.productCategories);
        } else if (type === 'service') {
          categoriesSet = new Set(this.serviceCategories);
        } else {
          categoriesSet = new Set(this.mediaCategories);
        }

        if (!categoriesSet.has(category)) {
          category = this.defaultCategory;
        }
      }

      let lat: number = null;
      let lng: number = null;
      let radius: number = DEFAULT_RADIUS;

      if (paramMap.has('lat') && paramMap.has('lng') && paramMap.has('radius')) {
        lat = Number(paramMap.get('lat'));
        lng = Number(paramMap.get('lng'));
        radius = Number(paramMap.get('radius'));
        this.inputControl.setValue({ address, lat, lng }, { emitEvent: false });
      } else {
        address = null;
        this.inputControl.setValue(null, { emitEvent: false });
      }

      this.page = 1;
      this.lat = lat;
      this.lng = lng;
      this.radius = radius;

      this.store.dispatch(new DiscoverActions.Load(
        type,
        category,
        this.page,
        this.pageSize,
        false,
        this.lat,
        this.lng,
        this.radius,
        address,
      ));
    });
  }

  ngAfterViewInit() {
    this.scrolled().subscribe(() => {
      this.onScroll();
    });

    this.inputControl.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      takeUntil(this.destroy$),
      switchMap(value => {
        if (!value) {
          const type = this.store.selectSnapshot(DiscoverState.currentEntityType);
          const category = this.store.selectSnapshot(DiscoverState.currentEntityCategory);

          const queryParams = {
            type,
            category,
          };

          return from(this.router.navigate(['/discover'], { queryParams }));
        } else {
          const text = typeof value === 'string' ? value : value.address;
          return this.store.dispatch(new DiscoverActions.GetLocationAutosuggestions(text));
        }
      })
    ).subscribe();
  }

  onScroll() {
    const type = this.store.selectSnapshot(DiscoverState.currentEntityType);
    const category = this.store.selectSnapshot(DiscoverState.currentEntityCategory);
    this.store.dispatch(new DiscoverActions.Load(type, category, this.page, this.pageSize, true, this.lat, this.lng, this.radius));
  }

  onAddressSuggestionSelect($event: MatAutocompleteSelectedEvent) {
    const { address, lat, lng } = $event.option.value;
    const type = this.store.selectSnapshot(DiscoverState.currentEntityType);
    const category = this.store.selectSnapshot(DiscoverState.currentEntityCategory);

    const queryParams = {
      type,
      category,
      address,
      lat,
      lng,
      radius: this.radius
    };

    this.router.navigate(['/discover'], { queryParams });
  }

  getSuggestionText(suggestion: DiscoverLocationAutosuggestion) {
    return suggestion && suggestion.address;
  }

  search() {
    const type = this.store.selectSnapshot(DiscoverState.currentEntityType);
    const category = this.store.selectSnapshot(DiscoverState.currentEntityCategory);
    this.store.dispatch(new DiscoverActions.Load(type, category, this.page, this.pageSize, false, this.lat, this.lng, this.radius));
  }

  onRadiusChange() {
    const queryParams = { ...this.activatedRoute.snapshot.queryParams };
    queryParams.radius = this.radius;
    this.router.navigate(['/discover'], { queryParams });
  }

  buildTranslationMapText = (type: string) => {
    return `PAGES.DISCOVER.EMPTY_LIST.${type.toUpperCase()}S`;
  }

}
