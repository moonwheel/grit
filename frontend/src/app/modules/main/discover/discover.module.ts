import { NgModule } from '@angular/core';

// Modules
import { CommonModule } from '@angular/common';
import { DiscoverRouting } from './discover.routing';
import { SharedModule } from '../../../shared/shared.module';

// Components
import { DiscoverListComponent } from './discover-list/discover-list.component';

@NgModule({
  declarations: [
    DiscoverListComponent,
  ],
  imports: [
    CommonModule,
    DiscoverRouting,
    SharedModule,
  ]
})
export class DiscoverModule { }
