import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { DiscoverListComponent } from './discover-list/discover-list.component';

const routes: Routes = [
  { path: '', component: DiscoverListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscoverRouting {
}
