import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Utils } from 'src/app/shared/utils/utils';
import {ChangeForgottenPassword, ChangePassword} from 'src/app/store/actions/auth.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangePassErrorStateMatcher } from '../../../user/components/settings/settings.component';
import { ValidatePassword } from '../../../../core/validators/password.validator';

@Component({
    selector: 'app-forgotpass',
    templateUrl: './forgotpass.component.html',
    styleUrls: ['./forgotpass.component.scss'],
})

export class ForgotPassComponent implements OnInit {

  changePassForm: FormGroup;
  passwordNew = false;
  passwordConfirm = false;
  token;
  matcher = new ChangePassErrorStateMatcher();

    constructor(
        private formBuilder: FormBuilder, private store: Store, private route: ActivatedRoute,
    ) { }

    ngOnInit() {
      this.route.queryParams.subscribe(params => {
        this.token = params.token;
      });

        this.changePassForm = this.formBuilder.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        }, { validator: this.checkPasswords });
    }

    checkPasswords(group: FormGroup) {
        const pass = group.controls.password.value;
        const isNotValidPassword = ValidatePassword(group.controls.password);
        const confirmPass = group.controls.confirmPassword.value;
        const notMatch = (pass !== confirmPass);

        if (isNotValidPassword) {
          group.controls.password.setErrors({ passwordNotValid: true });
        } else {
          group.controls.password.setErrors(null);
        }
        if (notMatch) {
          group.controls.confirmPassword.setErrors({notSame: true});
        } else {
          group.controls.confirmPassword.setErrors(null);
        }
    }

    showPassword(event: Event, name) {
        this[name] = !this[name];
        event.stopPropagation();
    }

    changePassword() {
        if (this.changePassForm.valid) {
            const formValue = {
              password: this.changePassForm.value.password,
              token: this.token
            };
            this.store.dispatch(new ChangeForgottenPassword(formValue));
        } else {
            Utils.touchForm(this.changePassForm);
        }
    }


}
