import { NgModule } from '@angular/core';

// Modules
import { ForgotPassRouting } from './forgotpass.routing';
import { SharedModule } from '../../shared/shared.module';

// Components
import { ForgotPassComponent } from './components/forgotpass/forgotpass.component';

@NgModule({
  declarations: [
    ForgotPassComponent,
  ],
  imports: [
    ForgotPassRouting,
    SharedModule,
  ],
})
export class ForgotPassModule {
}
