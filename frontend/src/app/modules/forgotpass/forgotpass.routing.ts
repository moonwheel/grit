import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgotPassComponent } from './components/forgotpass/forgotpass.component';

const routes: Routes = [
  {
    path: '',
    component: ForgotPassComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForgotPassRouting {
}
