import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { NotFoundRouting } from './not-found.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { NotFoundComponent } from './not-found.component';

@NgModule({
  declarations: [
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NotFoundRouting,
  ]
})
export class NotFoundModule { }
