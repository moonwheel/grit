import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Select } from '@ngxs/store';
import {UserState} from '../../store/state/user.state';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})

export class NotFoundComponent implements OnInit {

  user: any = null;

  notFound$: Observable<any>;

  @Select(UserState.loggedInAccount)
  user$: Observable<any>;


  constructor(private navigationService: NavigationService,
              private translateService: TranslateService,
              private router: Router,
  ) {
    this.user$.subscribe(user => {
      this.user = user ? user : null;
    })
   }

  ngOnInit() {
    this.notFound$ = this.translateService.stream('PAGES.NOT_FOUND').pipe(
      map((res: any) => ({
        title: res.TITLE,
        text: res.TEXT,
      }))
    );
  }

  back() {
    if(!this.user) {
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('/feed')
    }
  }

}
