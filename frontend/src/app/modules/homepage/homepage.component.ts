import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Store } from '@ngxs/store';
import { Login, VerifyPasswordWithoutAuth } from 'src/app/store/actions/auth.actions';
import { Utils } from 'src/app/shared/utils/utils';
import { Router } from '@angular/router';
import { SetLocalLanguage } from 'src/app/store/actions/user.actions';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { UserState } from 'src/app/store/state/user.state';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, OnDestroy {

  config: SwiperConfigInterface = {
    autoplay: { delay: 4000 }
  };

  loginForm: FormGroup;
  emailForm: FormGroup;

  person = true;
  password = false;
  createNewBusiness = false;

  languages = [
    { name: 'deutsch', code: 'de' },
    { name: 'english', code: 'en' },
  ];

  selectedLanguage = null;

  loginDialogRef: MatDialogRef<{}, any>;
  mailDialogRef: MatDialogRef<{}, any>;

  slides$: Observable<any[]>;

  private destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private store: Store,
    private router: Router,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.emailForm = this.formBuilder.group({
      email: ['', Validators.required]
    });

    this.slides$ = this.translateService.stream('PAGES.HOMEPAGE.SLIDES').pipe(
      map(result => {
        return [
          {
            photo: 'assets/images/social.jpg',
            header: result[2].HEADER,
            textOne: result[2].TEXT_ONE,
            textTwo: result[2].TEXT_TWO,
          },
          {
            photo: 'assets/images/page.jpg',
            header: result[1].HEADER,
            textOne: result[1].TEXT_ONE,
            textTwo: result[1].TEXT_TWO,
          },
          
          {
            photo: 'assets/images/commerce.jpg',
            header: result[3].HEADER,
            textOne: result[3].TEXT_ONE,
            textTwo: result[3].TEXT_TWO,
          }
        ];
      })
    );

    this.store.select(UserState.language).pipe(
      takeUntil(this.destroy$)
    ).subscribe(language => {
      this.selectedLanguage = this.languages.find(item => item.name === language);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  loginDialog(templateRef) {
    this.loginDialogRef = this.dialog.open(templateRef);
    // this.createNewBusiness = !!createBusiness;
  }

  mailDialog(templateRef) {
    const { email } = this.loginForm.value;
    if (email.trim() !== '') {
      this.emailForm.controls.email.setValue(email);
    }
    this.loginDialogRef.close();
    this.mailDialogRef = this.dialog.open(templateRef);
  }

  showPassword(event: Event) {
    this.password = !this.password;
    event.stopPropagation();
  }


  login() {
    if (!this.loginForm.valid) {
      Utils.touchForm(this.loginForm);
    } else {
      if (this.loginDialogRef) {
        this.store.dispatch(new Login(this.loginForm.value))
          .subscribe(
            result => {
              this.loginDialogRef.close();
            },
            error => {
              this.loginForm.setErrors({ badLogging: true });
            },
            () => this.router.navigate(['/feed'])
          );
      }
    }
  }

  verifyPassword() {
    if (!this.emailForm.valid) {
      Utils.touchForm(this.emailForm);
    } else {
      this.mailDialogRef.close();
      this.loginDialogRef.close();
      this.store.dispatch(new VerifyPasswordWithoutAuth(this.emailForm.value));
    }
  }

  changeLanguage(lang: any) {
    this.store.dispatch(new SetLocalLanguage(lang.name));
  }
}
