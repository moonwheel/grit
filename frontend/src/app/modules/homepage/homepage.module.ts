import { NgModule } from '@angular/core';

// Modules
import { HomepageRouting } from './homepage.routing';
import { SharedModule } from '../../shared/shared.module';

// Components
import { HomepageComponent } from './homepage.component';

@NgModule({
  declarations: [
    HomepageComponent,
  ],
  imports: [
    HomepageRouting,
    SharedModule,
  ],
})
export class HomepageModule {
}
