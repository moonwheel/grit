export const environment = {
  production: true,
  baseUrl: 'https://staging.grit.sc',
  appUrl: 'https://staging.grit.sc',
};
