// you can use these functions for formatting of lang json file
// in browser console or nodejs cli

function transform(obj) {
  for (let key in obj) {
    if (typeof obj[key] === 'object') {
      transform(obj[key])
    }
    if (key.toUpperCase() !== key) {
      obj[key.toUpperCase()] = obj[key];
      delete obj[key];
    }
  }
  return obj;
}

function transformAndPrint(test) {
  return JSON.stringify(transform(test), null, '  ');
}