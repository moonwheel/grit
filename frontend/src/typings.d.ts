declare var $ENV: Env;

declare module 'share-api-polyfill';

interface Env {
  API_URL: string;
}
