const express     = require('express');
const path        = require('path');
const cors        = require('cors');
const app         = express();
const port        = process.env.PORT || 4200;
const app_folder = 'dist/grit-frontend';

app.use(cors())

app.set('port', port);
app.get('*.*', express.static(__dirname + '/' + app_folder));

app.get('*', function (req, res, next) {
  res.status(200).sendFile(`/`, {root: app_folder});
})

app.listen(app.get('port'), () => {
  console.log(` App is running at http://localhost:${port}`);
  console.log(' Press CTRL-C to stop\n');
});
