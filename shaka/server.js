console.log('process.env.IS_LOCALHOST: ', process.env.IS_LOCALHOST )
console.log('process.env.IS_LOCALHOST: ', !!process.env.IS_LOCALHOST )
console.log('process.env.APM_SERVICE_NAME', process.env.APM_SERVICE_NAME)
console.log('process.env.APM_SERVER_URL', process.env.APM_SERVER_URL)

// if(!process.env.IS_LOCALHOST) {
// try {

setTimeout(() => {
  const apm = require('elastic-apm-node').start({
    // wait 3 mins for APM server successfully bootstraped

    // Override service name from package.json
    // Allowed characters: a-z, A-Z, 0-9, -, _, and space
    serviceName: process.env.APM_SERVICE_NAME || 'shaka',

    // Use if APM Server requires a token
    // secretToken: '',

    // Set custom APM Server URL (default: http://localhost:8200)
    serverUrl: process.env.APM_SERVER_URL || 'http://tracing-apm-server.kube-system:8200',
  })

}, 1000 * 60 * 3);

// } catch (error) {
//   console.log('APM ERROR', error)
// }

// }

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const { isMainThread} = require('worker_threads');
const { publish, subscribe, unsubscribe } = require('./sse');
const shakaService = require('./services/shaka');
const ffmpegConvertService = require('./services/ffmpeg-convert');
const ffmpegProbeService = require('./services/ffmpeg-probe');
const ffmpegScreenshotService = require('./services/ffmpeg-screenshot');
const minioService = require('./services/minio');
const MinioInstance = require('./utils/minio-instance');

// console.log('process.env', process.env)

MinioInstance.createBucket();

app.use(bodyParser.json())

app.use(express.static('static'));
app.use(express.static('upload'));

app.get('/upload/:foldername/:filename', function (req, res) {
  res.sendFile(`${__dirname}/upload/${req.params.foldername}/${req.params.filename}`)
});

app.get('/shaka', function (req, res) {
  res.sendFile(__dirname + '/static/shaka/shaka.html')
});

app.get(
  '/media-server-events/:userId/:videoId',
  (req, res, next) => {
    req.user = {
      id: req.params.userId
    };
    req.video = {
      id: req.params.videoId
    }
    next();
  },
  subscribe
 )

app.post('/convert', async (req, res) => {
  const { input_filename, input_extension, sizes, needScreenshot, userId, videoId, tempFile = false } = req.body;
  console.log('CONVERT TEMP FILE', tempFile);
  const outputFilename = tempFile ? `temp/${input_filename}` : input_filename;
  try {
    console.log('convert req.body', req.body);


    res.status('200').send(`Video ${input_filename}.${input_extension} is being procesed...`)

    if (isMainThread) {

      if (needScreenshot) {
        await ffmpegScreenshotService(input_filename, input_extension);
      }

      const {newSizes, hasAudio} = await ffmpegProbeService(input_filename, input_extension, sizes);

      console.log('newSizes', newSizes);
      console.log('hasAudio', hasAudio);

      await ffmpegConvertService(input_filename, input_extension, newSizes);
      await shakaService(input_filename, input_extension, newSizes, hasAudio);
      const links = await minioService(input_filename, input_extension, tempFile);
      if (needScreenshot) {
        links.cover = `${process.env.MINIO_PUBLIC_URL}/video/${tempFile ? 'temp/': ''}${input_filename}/${input_filename}_cover.jpg`;
      }
      console.log('links', links)
      publish({
        userId,
        videoId,
        type: 'file_processing_complete',
        filename: outputFilename,
        links: links
      });
      unsubscribe(videoId);

    }
  } catch (error) {
    console.log('error', error);
    publish({
      userId,
      type: 'file_processing_failure',
      filename: input_filename,
      error
    });
    unsubscribe(videoId);
  }
})

app.listen(+process.env.SHAKA_SERVER_PORT, function () {
  console.log(`Example app listening on port ${process.env.SHAKA_SERVER_PORT}!`);
});