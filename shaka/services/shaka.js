const {
  Worker
} = require('worker_threads');

const shakaService = (input_filename, input_extension, sizes, hasAudio) => {
  return new Promise((resolve, reject) => {
    console.log('shaka')
    const shakaThread = new Worker('./workers/shaka-worker.js', {
      workerData: {
        input_filename,
        input_extension,
        sizes,
        hasAudio
      }
    })

    shakaThread.on('message', (data) => {
      console.log('shaka done data', data)
      resolve(data)
    })

    shakaThread.on("error", (err) => {
      console.error("shakaThread error err", err);
      reject(err)
    })

    shakaThread.on('exit', (code) => {
      if (code != 0) {
        console.error(`SCREENSHOT Worker stopped with exit code ${code}`);
        reject(code);
      }
    })
  })
}
module.exports = shakaService;