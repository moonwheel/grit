const {
  Worker
} = require('worker_threads');

const minioSevice = (input_filename, input_extension, tempFile) => {
  return new Promise((resolve, reject) => {
    const minioThread = new Worker('./workers/minio-worker.js', {
      workerData: {
        input_filename,
        input_extension,
        tempFile,
      }
    })

    minioThread.on('message', (data) => {
      console.log('minio done data', data);
      resolve(data)
    })

    minioThread.on("error", (err) => {
      console.error("minio Worker err", err);
      reject(err)
    })

    minioThread.on('exit', (code) => {
      if (code != 0) {
        console.error(`MINIO Worker stopped with exit code ${code}`)
        reject(code);
      }
    })

  })
}

module.exports = minioSevice;