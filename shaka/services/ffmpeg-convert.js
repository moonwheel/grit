const {
  Worker
} = require('worker_threads');

const ffmpegConvert = (input_filename, input_extension, sizes) => {
  // const promises = [];
  return new Promise((resolve, reject) => {
    // sizes.forEach((size, i) => {
      const ffmpegThread = new Worker('./workers/ffmpeg-worker.js', {
        workerData: {
          input_filename,
          input_extension,
          sizes: sizes
        }
      })

      ffmpegThread.on('message', async (data) => {
        // console.log('message i data', i, data)
        // promises.push(i)
        console.log('sizes', sizes)
        // console.log('promises', promises)
        // if (promises.length === sizes.length) {
          // console.log('promises', promises)
          resolve()
        // }

      })

      ffmpegThread.on("error", (err) => {
        console.error("error err", err);
        reject(err)
      })

      ffmpegThread.on('exit', (code) => {
        if (code != 0) {
          console.error(`Worker stopped with exit code ${code}`)
          reject(code)
        }
      })
    // });
  })
}

module.exports = ffmpegConvert;