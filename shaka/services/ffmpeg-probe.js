const {
  Worker
} = require('worker_threads');

const ffmpegProbe = (input_filename, input_extension, sizes) => {
  return new Promise((resolve, reject) => {
      const ffmpegProbeThread = new Worker('./workers/ffmpeg-probe-worker.js', {
        workerData: {
          input_filename,
          input_extension,
          sizes
        }
      })

      ffmpegProbeThread.on('message', async (data) => {
        console.log('ffmpegProbe message data', data)
        resolve({newSizes: data.newSizes, hasAudio: data.hasAudio})

      })

      ffmpegProbeThread.on("error", (err) => {
        console.error("ffmpegProbe err", err);
        reject(err)
      })

      ffmpegProbeThread.on('exit', (code) => {
        if (code != 0) {
          console.error(`Worker ffmpegProbe stopped with exit code ${code}`)
          reject(code)
        }
      })
  });
}

module.exports = ffmpegProbe;