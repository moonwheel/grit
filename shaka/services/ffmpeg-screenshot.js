const {
  Worker
} = require('worker_threads');

const ffmpegScreenshot = (input_filename, input_extension) => {
  return new Promise((resolve, reject) => {
    const screenshotThread = new Worker('./workers/ffmpeg-screenshot-worker.js', {
      workerData: {
        input_filename,
        input_extension,
      }
    })

    screenshotThread.on('message', (data) => {
      console.log('screenshot done data', data)
      resolve(data)
    })

    screenshotThread.on("error", (err) => {
      console.error("error err", err);
      reject(err)
    })

    screenshotThread.on('exit', (code) => {
      if (code != 0) {
        console.error(`SCREENSHOT Worker stopped with exit code ${code}`)
        reject(code)
      }
    })

  })
}

module.exports = ffmpegScreenshot