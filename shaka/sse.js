const EventEmitter = require('events');

const emitter = new EventEmitter();

// export type ServerEventType = 'file_processing_complete';

// export interface ServerEvent {
//   userId: number,
//   type: ServerEventType,
//   entityType: string,
//   entity: any
// }
let heartbit = {}, onEvent = {};

const unsubscribe = (videoId) => {
  console.log('unsubscribe videoId', videoId)
  clearInterval(heartbit[videoId]);
	emitter.removeListener('event', onEvent[videoId]);
}

const subscribe = (req, res) => {
  console.log('subscribe', req.video, req.user)
	res.writeHead(200, {
		'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    "Access-Control-Allow-Origin": "*",
		Connection: 'keep-alive'
  });

  res.write('{}');

	heartbit[req.video.id] = setInterval(() => {
    res.write('{}');
  }, 15000);

	onEvent[req.video.id] = (event) => {
    console.log('ssejs event', event)
    if (event.userId.toString() === req.user.id.toString() && event.videoId.toString() === req.video.id.toString() ) {
      const stringData = JSON.stringify(event);
      res.write(JSON.stringify({data: stringData}));
    }
	};

	emitter.on('event', onEvent[req.video.id]);

	req.on('close', () => {
    unsubscribe(req.video.id)
	});
}

const publish = (eventData) => {
	emitter.emit('event', eventData);
}

module.exports = {
  subscribe,
  unsubscribe,
  publish
}

