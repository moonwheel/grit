// myapp.js
var manifestUri =
  'http://localhost:9000/video/video-406rad1dk5p5dvfy/video-406rad1dk5p5dvfy_h264_master.m3u8';


async function init() {
  // When using the UI, the player is made automatically by the UI object.
  const video = document.getElementById('video');
  const controlsContainer = document.querySelector('.shaka-controls-container');
  const bottomControls = controlsContainer.querySelectorAll('.shaka-bottom-controls>div')
  const settingsMenu = document.querySelector('.shaka-settings-menu')
  const ui = video['ui'];
  const controls = ui.getControls();
  const player = controls.getPlayer();

  // Attach player and ui to the window to make it easy to access in the JS console.
  window.player = player;
  window.ui = ui;

  controlsContainer.addEventListener('mouseenter', () => {
    console.log('mouseenter', bottomControls)
    document.querySelector('.shaka-controls-container > .shaka-fade-out-on-mouse-out').setAttribute('shown', true)
    document.querySelector('.shaka-controls-container .shaka-controls-button-panel').setAttribute('shown', true)
    document.querySelector('.shaka-controls-container .shaka-seek-bar-container').setAttribute('shown', true)
    document.querySelector('.shaka-controls-container .shaka-play-button').setAttribute('shown', true)
  })
  controlsContainer.addEventListener('mouseleave', () => {
    console.log('mouseleave', bottomControls)
    document.querySelector('.shaka-controls-container > .shaka-fade-out-on-mouse-out').setAttribute('shown', false)
    document.querySelector('.shaka-controls-container .shaka-controls-button-panel').setAttribute('shown', false)
    document.querySelector('.shaka-controls-container .shaka-seek-bar-container').setAttribute('shown', false)
    document.querySelector('.shaka-controls-container .shaka-play-button').setAttribute('shown', false)
  })

  document.querySelector('.shaka-overflow-menu-button').addEventListener('click', () => {
    console.log('menu click')
    settingsMenu.setAttribute('shown', !settingsMenu.getAttribute('shown'))
    // element.setAttribute('shown', true)
  })
  //.shaka-settings-menu

  const config = {
    'overflowMenuButtons' : ['quality']
  }
  ui.configure(config);
  // Listen for error events.
  player.addEventListener('error', onPlayerErrorEvent);
  controls.addEventListener('error', onUIErrorEvent);

  // Try to load a manifest.
  // This is an asynchronous process.
  try {
    await player.load(manifestUri);
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
  } catch (error) {
    onPlayerError(error);
  }
}

function onPlayerErrorEvent(errorEvent) {
  // Extract the shaka.util.Error object from the event.
  onPlayerError(event.detail);
}

function onPlayerError(error) {
  // Handle player error
  console.error('Error code', error.code, 'object', error);
}

function onUIErrorEvent(errorEvent) {
  // Extract the shaka.util.Error object from the event.
  onPlayerError(event.detail);
}

function initFailed() {
  // Handle the failure to load
  console.error('Unable to load the UI library!');
}

// Listen to the custom shaka-ui-loaded event, to wait until the UI is loaded.
document.addEventListener('shaka-ui-loaded', init);
// Listen to the custom shaka-ui-load-failed event, in case Shaka Player fails
// to load (e.g. due to lack of browser support).
document.addEventListener('shaka-ui-load-failed', initFailed);