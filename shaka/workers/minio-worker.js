const fs = require("fs");
const glob = require("glob");
const {
  parentPort,
  workerData
} = require('worker_threads');
const MinioInstance = require('../utils/minio-instance');

const UPLOAD_FOLDER = '/app/upload/';

return new Promise((resolve, reject) => {
  const { input_filename, input_extension, tempFile } = workerData;
  const counter = [];
  console.log('temp file WORKER', tempFile);
  console.log('input_filename', input_filename)
  console.log('input_extension', input_extension)
  // options is optional
  glob(`${UPLOAD_FOLDER}${input_filename}/${input_filename}_*`, function (er, files) {
    console.log('er', er)
    if(er) reject('er');
    console.log('files', files)
    console.log('input_filename', input_filename)
    files.forEach(async (localFilePath, i) => {
      const filename = localFilePath.match(new RegExp(`${input_filename}_.*`))[0]
      let contentType
      if (filename.search(/.m3u8/) > 0) contentType = 'application/x-mpegURL'
      else if (filename.search(/.mp4/) > 0) contentType = 'video/mp4'
      else if (filename.search(/.mpd/) > 0) contentType = 'application/dash+xml'
      else if (filename.search(/.jpg/) > 0) contentType = 'image/jpeg'
      console.log('filename', filename);
      console.log('localFilePath', localFilePath)
      console.log('contentType', contentType)
      const metadata = {
        'Content-Type': contentType
      }
      await MinioInstance.uploadFile('video', `${tempFile ? 'temp/': ''}${input_filename}/${filename}`, localFilePath, metadata);
      fs.unlinkSync(localFilePath);
      counter.push(i)
      if(counter.length === files.length) {
        setTimeout(() => {
          fs.unlink(`${UPLOAD_FOLDER}${input_filename}/${input_filename}.${input_extension}`, (err) => {
            fs.rmdirSync(`${UPLOAD_FOLDER}${input_filename}`);
          });
        }, 1);
        resolve({
          video: `${process.env.MINIO_PUBLIC_URL}/video/${tempFile ? 'temp/': ''}${input_filename}/${input_filename}_h264_master`,
        })
      }
    });
    // files is an array of filenames.
    // If the `nonull` option is set, and nothing
    // was found, then files is ["**/*.js"]
    // er is an error object or null.
  })


})
.then((result) => {
  parentPort.postMessage(result)

})
.catch((e) => {
  console.log('error minio-worker', e)
  parentPort.postMessage({error: e})
})