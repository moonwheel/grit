const {
  parentPort,
  workerData
} = require('worker_threads');
const ffmpeg = require("fluent-ffmpeg");

const UPLOAD_FOLDER = '/app/upload/';

// const generateOutputPath = (filename, extension, suffix) => {
//   return `${UPLOAD_FOLDER}${filename}/${filename}_${suffix}.${extension}`;
// }

const { input_filename, input_extension, sizes } = workerData

const inputPath = `${UPLOAD_FOLDER}${input_filename}/${input_filename}.${input_extension}`;

return new Promise((resolve, reject) => {

  ffmpeg.ffprobe(inputPath, function (err, metadata) {
    if (err || !metadata) {
      console.error(err);
      reject(err || 'No metadata')
    } else {
      // metadata should contain 'width', 'height' and 'display_aspect_ratio'
      // TODO: CHECK video's size or length and throw an error if it more than limit

      if(metadata.streams) {
        resolve({
          video: metadata.streams.find(stream => stream.width && stream.height && stream.codec_type === 'video'),
          audio: metadata.streams.find(stream => stream.codec_type === 'audio')
        })
      } else {
        reject('No Streams found')
      }

    }
  });
})
  .then((stream) => {
    // console.log('stream', stream);
    if(!stream.video) throw new Error('Video stream not found')

    let newSizes
    if(stream.video.width >= 1920 && stream.video.height >= 1080) {
      newSizes = ['1920x1080', '640x360']
    } else {
      newSizes = ['854x480', '640x360']
    }
    // console.log('newSizes', newSizes)
    return {
      newSizes: newSizes,
      hasAudio: !!stream.audio
    }
  })
  .then((response) => {
    console.log('post message', response)
    parentPort.postMessage({ success: true, newSizes: response.newSizes, hasAudio: response.hasAudio})
  })
  .catch((e) => {
    console.log('error ffmpeg-probe-worker', e);
    parentPort.postMessage({ error: e })
  })
