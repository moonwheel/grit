const {
  parentPort,
  workerData
} = require('worker_threads');
const { exec } = require('child_process');
// const fs = require('fs');

const UPLOAD_FOLDER = '/app/upload/';

const generatePath = (filename, size) => {
  return `${UPLOAD_FOLDER}${filename}/${filename}_${size}`;
}

return new Promise((resolve, reject) => {
  const { input_filename, input_extension, sizes, hasAudio } = workerData
  let command = `packager `;

  // console.log('FILES', fs.readdirSync(`${UPLOAD_FOLDER}${input_filename}`))
  sizes.forEach(size => {
    const path = generatePath(input_filename, size)
    command += `in=${path}.mp4,stream=video,output==${path}.mp4,playlist_name==${path}.m3u8,iframe_playlist_name==${path}_iframe.m3u8     `
  });
  if (hasAudio) {
    command += `in=${generatePath(input_filename, sizes[0])}.mp4,stream=audio,output=${UPLOAD_FOLDER}${input_filename}/${input_filename}_audio.mp4,playlist_name=${UPLOAD_FOLDER}${input_filename}/${input_filename}_audio.m3u8,hls_group_id=audio,hls_name=MAIN     `
    // command += `in=${UPLOAD_FOLDER}${input_filename}/${input_filename}.${input_extension},stream=audio,output=${UPLOAD_FOLDER}${input_filename}/${input_filename}_audio.mp4,playlist_name=${UPLOAD_FOLDER}${input_filename}/${input_filename}_audio.m3u8,hls_group_id=audio,hls_name=MAIN     `
  }
  command += `--hls_master_playlist_output ${UPLOAD_FOLDER}${input_filename}/${input_filename}_h264_master.m3u8     `
  command += `--mpd_output ${UPLOAD_FOLDER}${input_filename}/${input_filename}_h264_master.mpd`

  // const command = `packager \
  //   in={0}_144p_300.mp4,stream=video,output={0}_144p_300.mp4,playlist_name={0}_144p_300.m3u8,iframe_playlist_name={0}_144p_300_iframe.m3u8 \
  //   in={0}_240p_500.mp4,stream=video,output={0}_240p_500.mp4,playlist_name={0}_240p_500.m3u8,iframe_playlist_name={0}_240p_500_iframe.m3u8 \
  //   in={0}_360p_1000.mp4,stream=video,output={0}_360p_1000.mp4,playlist_name={0}_360p_1000.m3u8,iframe_playlist_name={0}_360p_1000_iframe.m3u8 \
  //   in={0}_480p_1500.mp4,stream=video,output={0}_480p_1500.mp4,playlist_name={0}_480p_1500.m3u8,iframe_playlist_name={0}_480p_1500_iframe.m3u8 \
  //   in={0}_720p_3000.mp4,stream=video,output={0}_720p_3000.mp4,playlist_name={0}_720p_3000.m3u8,iframe_playlist_name={0}_720p_3000_iframe.m3u8 \
  //   --hls_master_playlist_output {0}_h264_master.m3u8 \
  //   --mpd_output {0}_h264_master.mpd`

  console.log('command', command);

  exec(command, (error, stdout, stderr) => {
    if (error) {
      // console.error(`exec error: ${error}`);
      return reject(new Error(error))
    }
    console.log(`stdout: ${stdout}`);
    // console.error(`stderr: ${stderr}`);
    resolve({success: stdout, filename: input_filename})
  });

})
.then((result) => {
  parentPort.postMessage(result)

})
.catch((e) => {
  console.log('error shaka-worker', e)
  parentPort.postMessage({error: e})
})