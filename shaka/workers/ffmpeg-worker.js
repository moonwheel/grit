const {
  parentPort,
  workerData
} = require('worker_threads');
const ffmpeg = require("fluent-ffmpeg");
// const { exec, spawn } = require("child_process");

const UPLOAD_FOLDER = '/app/upload/';

const generateOutputPath = (filename, extension, suffix) => {
  return `${UPLOAD_FOLDER}${filename}/${filename}_${suffix}.${extension}`;
}

console.log('workerData', workerData)
const { input_filename, input_extension, sizes } = workerData

const inputPath = `${UPLOAD_FOLDER}${input_filename}/${input_filename}.${input_extension}`;



let videoBitrate;
let startDate, finishDate;

return new Promise((_resolve, _reject) => {
  let ffmpegInstance = ffmpeg(inputPath)
    .on('start', function (commandLine) {
      startDate = new Date();
      console.log(`${startDate}: Spawned Ffmpeg with command: ${commandLine}`);
      console.log('     ');
      console.log('     ');
    })
    .on('end', function (stdout, stderr) {
      finishDate = new Date();
      console.log(`${new Date()}: Finished processing in ${(finishDate - startDate) / 1000} seconds`);
      console.log('FFMPEG on END stdout, stderr\n\n', stdout, stderr)
      console.log('     ');
      console.log('     ');
      const response = {
        video: input_filename,
      };
      // console.log('response', response)
      _resolve(response);
    })
    .on('error', function (err) {
      console.log('An error occurred: ' + err.message);
      _reject(err.message)
    })

// Experiments with splitting a video file
    // exec(`ffmpeg \
    //   -i ${inputPath} \
    //   -map 0 \
    //   -segment_time 5 \
    //   -segment_format ${input_extension} \
    //   -segment_list ${generateOutputPath(input_filename, 'ffcat', 'chunk_list')} \
    //   -g 125 \
    //   -sc_threshold 0 \
    //   -force_key_frames "expr:gte(t,n_forced*5)" \
    //   -f segment \
    //   ${generateOutputPath(input_filename, input_extension, '%03d')}`,
    // (error, stdout, stderr) => {
    //   if (error) {
    //       console.log(`error2: ${error.message}`);
    //       return;
    //   }
    //   if (stderr) {
    //       console.log(`stderr2: ${stderr}`);
    //       return;
    //   }
    //   console.log(`stdout2: ${stdout}`);
    // });

    // const ls = spawn(`ffmpeg -fflags +genpts -i ${inputPath} -map 0 -c copy -f segment -segment_format mp4 -segment_list ${UPLOAD_FOLDER}${input_filename}/${input_filename}.ffcat -reset_timestamps 1 -v error ${UPLOAD_FOLDER}${input_filename}/chunk-%03d.seg`);
    // const ls = spawn( //output%03d.mp4
    // );
    //   ls.stdout.on("data", data => {
    //       console.log(`stdout3: ${data}`);
    //   });

    //   ls.stderr.on("data", data => {
    //       console.log(`stderr3: ${data}`);
    //   });

    //   ls.on('error', (error) => {
    //       console.log(`error3: ${error.message}`);
    //   });

    //   ls.on("close", code => {
    //       console.log(`child process exited with code ${code}`);
    //   });

    sizes.forEach((size, index) => {
      const outputVideoPath = generateOutputPath(input_filename, 'mp4', size);

      switch (size) {
        case '1920x1080':
          videoBitrate = '5000k'
          break;
        case '1280x720':
          videoBitrate = '3000k'
          break;
        case '854x480':
          videoBitrate = '1500k'
          break;
        default:
          videoBitrate = '1000k'
          break;
      }
      ffmpegInstance = ffmpegInstance
        .output(outputVideoPath)
        .outputOptions(['-preset veryfast'])
        .audioCodec('aac')
        .videoCodec('libx264')
        .audioBitrate(256)
        .format('mp4')
        .fps(30)
        .autopad()
        .videoBitrate(videoBitrate, true)
        .size(size)
        .addOption('-benchmark_all')
        .addOption('-benchmark')

      if (index === sizes.length - 1) {
        ffmpegInstance.run()
      }
    });
})
  .then((response) => {
    console.log('post message', response)
    parentPort.postMessage({ success: response })
  })
  .catch((e) => {
    console.log('error ffmpeg-worker', e);
    parentPort.postMessage({ error: e })
  })
