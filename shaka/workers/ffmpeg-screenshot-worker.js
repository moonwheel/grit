const {
  parentPort,
  workerData
} = require('worker_threads');
const ffmpeg = require("fluent-ffmpeg");

const UPLOAD_FOLDER = '/app/upload/';

const generateOutputPath = (filename, extension, suffix) => {
  return `${UPLOAD_FOLDER}${filename}/${filename}_${suffix}.${extension}`;
}

console.log('workerData', workerData)
const { input_filename, input_extension } = workerData;
let startDate, finishDate;
const inputPath = `${UPLOAD_FOLDER}${input_filename}/${input_filename}.${input_extension}`;
const coverFileName = `${input_filename}_cover.jpg`
return new Promise((_resolve, _reject) => {
  ffmpeg(inputPath)
    .screenshots({
      timestamps: ['50%'],
      filename: coverFileName,
      folder: `${UPLOAD_FOLDER}${input_filename}`,
      size: '100x100'
    })
    .on('start', function (commandLine) {
      startDate = new Date();
      console.log(`${startDate}: Spawned Ffmpeg with command: ${commandLine}`);
      console.log('     ');
      console.log('     ');
    })
    .on('end', function () {
      finishDate = new Date();
      console.log(`${new Date()}: Finished processing in ${(finishDate - startDate)/1000} seconds`);
      console.log('     ');
      console.log('     ');
      const response = generateOutputPath(input_filename, 'jpg', 'cover')
      _resolve(response);
    })
    .on('error', function (err) {
      console.log('An error occurred during Screenshot: ' + err.message);
      _reject(err.message)
    })
})
  .then((response) => {
    parentPort.postMessage({success: response})
  })
  .catch((e) => {
    console.log('error screenshot', e);
    parentPort.postMessage({error: e})
  })
