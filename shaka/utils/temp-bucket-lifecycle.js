module.exports.tempBucketLifecycle = {
    "Rules": [
        {
            "Expiration": {
                "Days": 10
            },
            "ID": "MessageAttachments",
            "Filter": {
                "Prefix": "temp/"
            },
            "Status": "Enabled"
        }
    ]
}