function putBucketLifecycle(s3, bucketName, config) {
    return new Promise((resolve, reject) => {
        s3.putBucketLifecycleConfiguration({Bucket: bucketName, LifecycleConfiguration: config},(err) => {
            if(err) {
                console.log(`Failed to put lifecycle configuration for bucket: ${bucketName}`);
                return reject(err);
            }
            console.log(`Lifecycle configuration has added for bucket: ${bucketName}`);
            resolve();
        })
    })
}

module.exports.putBucketLifecycle = putBucketLifecycle;