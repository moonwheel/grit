const Minio = require('minio');
const AWS = require('aws-sdk');
const downloadPolicy = require('./minio-bucket-policy');
const { tempBucketLifecycle } = require('./temp-bucket-lifecycle');
const { putBucketLifecycle } = require('./put-bucket-lifecylce');

const MINIO_BUCKETS = ['photo', 'video', 'document']

const S3 = new AWS.S3({
  accessKeyId: process.env.MINIO_ACCESS_KEY,
  secretAccessKey: process.env.MINIO_SECRET_KEY,
  endpoint: `http://${process.env.MINIO_HOST}:${process.env.MINIO_PORT}`,
  signatureVersion: 'v4',
  s3ForcePathStyle: true, // needed with minio?
  sslEnabled: false
})

const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_HOST,
  port: +process.env.MINIO_PORT,
  useSSL: false,
  // useSSL: !!process.env.MINIO_SSL,
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY
}),
  MinioInstance = {
    client: minioClient,
    createBucket: () => {
      try {
        MINIO_BUCKETS.forEach(bucketName => {
          S3.headBucket({ Bucket: bucketName }, async (err, data) => {
            if (err && err.statusCode === 404) {
              console.log(`Bucket ${bucketName} does not exist`);
              try {
                console.log(`Creating bucket: ${bucketName}`);
                await minioClient.makeBucket(bucketName);
                console.log(`Bucket: ${bucketName} created.`);

                const policy = downloadPolicy(bucketName);
                console.log(`Setting bucket policy for bucket: ${bucketName}`);
                await minioClient.setBucketPolicy(bucketName, policy);
                console.log(`Policy set for bucket: ${bucketName}`);

                console.log(`Adding bucket lifecycle configuration for bucket: ${bucketName}`);
                await putBucketLifecycle(S3, bucketName, tempBucketLifecycle);
                console.log(`Lifecycle configration added for bucket: ${bucketName}`);
                return;

              } catch (e) {
                console.log(`Failed to create bucket ${bucketName} with policy`, e);
                throw e;
              }
            }

            else {
              console.log(`Bucket: ${bucketName} exists.Checking if bucket has correct rules.`);
              console.log(`Getting lifecycle configuration rules for bucket: ${bucketName}`);
              S3.getBucketLifecycleConfiguration({ Bucket: bucketName }, async (err, data) => { // check if bucket has rules.for cases where bucket was created outside of shaka server
                if (err && err.statusCode != 404) {
                  console.log(`Failed to get bucket Lifecycle configration list for bucket: ${bucketName}`);
                  throw err;
                }

                const isBucketHasRule = data && data.Rules ? data.Rules.filter(rule => rule.ID === tempBucketLifecycle.Rules[0].ID) : false;
                if (!isBucketHasRule) {
                  console.log(`Lifecycle configuration is not set on bucket: ${bucketName}`);
                  console.log(`Adding lifecycle configuration for bucket: ${bucketName}`);
                  await putBucketLifecycle(S3, bucketName, tempBucketLifecycle);
                  console.log(`Lifecycle configration added for bucket: ${bucketName}`);
                  return;
                }

              })
            }
          })

        });
      } catch (error) {
        console.log('createBucket error', error)
      }
    },

    uploadFile: (type, name, url, metadata = {}) => {
      return new Promise((resolve, reject) => {
        minioClient.fPutObject(type, name, url, metadata, async function (err, etag) {
          if (err) return reject(err)
          resolve(etag)
        })
      });
    },
    deleteFile: (bucket, name) => {
      return new Promise((resolve, reject) => {
        minioClient.removeObject(bucket, name, function (err) {
          if (err) return reject(err)
          resolve()
        })
      });
    },
    uploadStream: (type, name, stream, size, meta) => {
      return new Promise((resolve, reject) => {
        minioClient.putObject(type, name, stream, size, meta, function (err, etag) {
          if (err) return reject(err)
          resolve(etag)
        })
      });
    }
  }

module.exports = MinioInstance;