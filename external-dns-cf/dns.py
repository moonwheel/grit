import CloudFlare
import os

def main():

    zone_name = os.getenv('DNS_ZONE_NAME', 'grit.sc')
    cloudflare_token = os.getenv('CF_TOKEN', 'nDDYUGrb1WW-vGKqix4USoA5H9va8a2yUZlel4jh')
    load_balancer_dns = os.getenv('LB_DNS', 'lb.grit.sc')
    cf = CloudFlare.CloudFlare(token=cloudflare_token)
    kubernetes_namespaces = ["production", "staging", "kube-system"]

    ### Collecting DNS records which should be present in the Cloudflare

    hostnames = []
    for kube_namespace in kubernetes_namespaces:
        kube_virtualservices = os.popen('kubectl get virtualservices -n ' + kube_namespace + '|awk \'{print $3}\'|grep -v "HOSTS" &2> /dev/null')
        for virtualservice in kube_virtualservices:
            start = virtualservice.find("[") + len("[")
            end = virtualservice.find("]")
            hostnames.append(virtualservice[start:end])

    ### END collecting DNS records which should be present in the Cloudflare

    ### Getting DNS zone ID

    try:
        zones = cf.zones.get(params = {'name':zone_name,'per_page':1})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones.get %d %s - api call failed' % (e, e))
    except Exception as e:
        exit('/zones.get - %s - api call failed' % (e))

    if len(zones) == 0:
        exit('No zones found')

    zone = zones[0]
    zone_id = zone['id']

    ### END getting DNS zone ID


    ### Find the records present in the Cloudflare

    try:
        params = {'type': "TXT", "content": "OWNER: Kuberntes"}
        dns_records = cf.zones.dns_records.get(zone_id,params=params)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones/dns_records.get %d %s - api call failed' % (e, e))

    ### End find the records present in the Cloudflare

    ### Collecting records from Cloudflare

    dns_to_delete = []
    dns_in_cf = []
    for dns_record in dns_records:
        dns_name = dns_record['name']
        if dns_name not in hostnames:
            dns_to_delete.append(dns_name)
        else:
            dns_in_cf.append(dns_name)

    ### END collecting records from Cloudflare

    ### Creating records in cloudflare

    dns_records_to_add = []
    for hostname in hostnames:
        if hostname not in dns_in_cf:
            dns_record_txt = '{"name":"' + hostname + '", "type": "TXT", "content": "OWNER: Kuberntes"}'
            dns_record_cname = '{"name":"' + hostname + '", "type": "CNAME", "content": "' + load_balancer_dns + '"}'
            dns_records_to_add.append(dns_record_txt)
            dns_records_to_add.append(dns_record_cname)

    for dns_record in dns_records_to_add:
        try:
            r = cf.zones.dns_records.post(zone_id, data=dns_record)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            continue
        # Print respose info - they should be the same
        dns_record = r
        print('\tAdded dns record: Name=%s, Type=%s, Data=%s' % (
            dns_record['name'],
            dns_record['type'],
            dns_record['content']
        ))

        # set proxied flag to false - for example
        dns_record_id = dns_record['id']

        new_dns_record = {
            'type': dns_record['type'],
            'name': dns_record['name'],
            'content': dns_record['content'],
            'proxied': False
        }

        try:
            dns_record = cf.zones.dns_records.put(zone_id, dns_record_id, data=new_dns_record)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones/dns_records.put %d %s - api call failed' % (e, e))

    ### END creating records in cloudflare

    ### Deleting unneeded records in cloudflare

    for dns_record in dns_to_delete:
        dns_records = cf.zones.dns_records.get(zone_id, params={'name': dns_record})
        for dns_record in dns_records:
            dns_record_id = dns_record['id']
            try:
                r = cf.zones.dns_records.delete(zone_id, dns_record_id)
                print('\tRemoved dns record: Name=%s' % (
                    dns_record['name']
                ))
            except CloudFlare.exceptions.CloudFlareAPIError as e:
                continue

    ### END deleting unneeded records in cloudflare

    exit(0)
if __name__ == '__main__':
    main()
