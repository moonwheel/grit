use grit;

CREATE SEQUENCE migrations_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE migrations (
	id INT8 NOT NULL DEFAULT nextval('"migrations_id_seq"':::STRING),
	"timestamp" INT8 NOT NULL,
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_8c82d7f526340ab734260ea46be" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, "timestamp", name)
);

CREATE SEQUENCE t_category_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_category (
	id INT8 NOT NULL DEFAULT nextval('"t_category_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	type VARCHAR NULL,
	CONSTRAINT "PK_3e0349fc1fd50ba2db2a3edac4e" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_96c2cee25fbe17122f0e2386c7f" (name ASC, type ASC),
	FAMILY "primary" (id, name, type)
);

CREATE SEQUENCE t_business_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_business (
	id INT8 NOT NULL DEFAULT nextval('"t_business_id_seq"':::STRING),
	photo VARCHAR NULL,
	thumb VARCHAR NULL,
	name VARCHAR NULL,
	"pageName" VARCHAR NULL,
	phone VARCHAR NULL,
	description VARCHAR NULL,
	legal_notice VARCHAR NULL,
	privacy VARCHAR NULL,
	mango_user_id INT8 NULL,
	wallet INT8 NULL,
	bank_alias_id INT8 NULL,
	kyc_document_id VARCHAR NULL,
	verification_status VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	reactivationtoken VARCHAR NULL DEFAULT NULL,
	"categoryId" INT8 NULL,
	"deliveryAddressId" INT8 NULL,
	"addressId" INT8 NULL,
	CONSTRAINT "PK_cb1e376352956d5292cf99e8156" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_989a7e83854e7a4f23f0ce2814d" ("pageName" ASC),
	UNIQUE INDEX "REL_d608f158f657779a0b20a04bd0" ("deliveryAddressId" ASC),
	UNIQUE INDEX "REL_e269995d4d6e50563a08a9016b" ("addressId" ASC),
	INDEX "IDX_6e870aac767cc2a0bbca173005" ("categoryId" ASC),
	INDEX "IDX_d608f158f657779a0b20a04bd0" ("deliveryAddressId" ASC),
	INDEX "IDX_e269995d4d6e50563a08a9016b" ("addressId" ASC),
	FAMILY "primary" (id, photo, thumb, name, "pageName", phone, description, legal_notice, privacy, mango_user_id, wallet, bank_alias_id, kyc_document_id, verification_status, created, updated, deleted, reactivationtoken, "categoryId", "deliveryAddressId", "addressId")
);

CREATE SEQUENCE t_address_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_address (
	id INT8 NOT NULL DEFAULT nextval('"t_address_id_seq"':::STRING),
	type VARCHAR NULL,
	"addrType" VARCHAR NULL,
	"fullName" VARCHAR NULL,
	"careOf" VARCHAR NULL,
	street VARCHAR NULL,
	appendix VARCHAR NULL,
	zip INT8 NULL,
	city VARCHAR NULL,
	state VARCHAR NULL,
	country VARCHAR NULL,
	selected BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	lat FLOAT8 NULL,
	lng FLOAT8 NULL,
	"personId" INT8 NULL,
	"businessId" INT8 NULL,
	CONSTRAINT "PK_4f14d53c5cfcf5556dc10a063c2" PRIMARY KEY (id ASC),
	INDEX "IDX_dc133bee8ecf186bfcab7e0d2d" ("personId" ASC),
	INDEX "IDX_ae3a39cc32d9b206c55fc9c4aa" ("businessId" ASC),
	FAMILY "primary" (id, type, "addrType", "fullName", "careOf", street, appendix, zip, city, state, country, selected, created, updated, deleted, lat, lng, "personId", "businessId")
);

CREATE SEQUENCE t_person_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_person (
	id INT8 NOT NULL DEFAULT nextval('"t_person_id_seq"':::STRING),
	photo VARCHAR NULL,
	thumb VARCHAR NULL,
	"pageName" VARCHAR NULL,
	"fullName" VARCHAR NULL,
	email VARCHAR NULL,
	password VARCHAR NULL,
	mango_user_id INT8 NULL,
	wallet INT8 NULL,
	bank_alias_id INT8 NULL,
	kyc_document_id VARCHAR NULL,
	verification_status VARCHAR NULL,
	gender VARCHAR NULL,
	birthday VARCHAR NULL,
	residence VARCHAR NULL,
	description VARCHAR NULL,
	language VARCHAR NULL DEFAULT 'deutsch':::STRING,
	privacy VARCHAR NULL DEFAULT 'public':::STRING,
	active BOOL NOT NULL DEFAULT false,
	token VARCHAR NULL,
	passtoken VARCHAR NULL,
	"sellerRating" INT8 NULL,
	"sellerReviews" INT8 NULL,
	"tokenAttempts" INT8 NULL,
	"tokenAttemptsStamp" TIMESTAMP NULL DEFAULT current_timestamp():::TIMESTAMP,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deactivated TIMESTAMP NULL DEFAULT NULL,
	reactivationtoken VARCHAR NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	address_id INT8 NULL,
	"deliveryAddressId" INT8 NULL,
	CONSTRAINT "PK_7923ecc35d388095254078659a6" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_192dc27a314fd7d02ae31300299" ("pageName" ASC),
	UNIQUE INDEX "REL_1b6f23f5feaf9294e1cb27a8f1" (address_id ASC),
	UNIQUE INDEX "REL_5f30eec8cabee18d383292cda3" ("deliveryAddressId" ASC),
	INDEX "IDX_1b6f23f5feaf9294e1cb27a8f1" (address_id ASC),
	INDEX "IDX_5f30eec8cabee18d383292cda3" ("deliveryAddressId" ASC),
	FAMILY "primary" (id, photo, thumb, "pageName", "fullName", email, password, mango_user_id, wallet, bank_alias_id, kyc_document_id, verification_status, gender, birthday, residence, description, language, privacy, active, token, passtoken, "sellerRating", "sellerReviews", "tokenAttempts", "tokenAttemptsStamp", created, updated, deactivated, reactivationtoken, deleted, address_id, "deliveryAddressId")
);

CREATE SEQUENCE t_roles_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_roles (
	id INT8 NOT NULL DEFAULT nextval('"t_roles_id_seq"':::STRING),
	type VARCHAR NOT NULL,
	CONSTRAINT "PK_121ab4de2ff554150c687a36505" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_fd7cbae2f15317b3847b907e2d0" (type ASC),
	FAMILY "primary" (id, type)
);

CREATE SEQUENCE t_account_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_account (
	id INT8 NOT NULL DEFAULT nextval('"t_account_id_seq"':::STRING),
	followers_notification BOOL NOT NULL DEFAULT true,
	likes_notification BOOL NOT NULL DEFAULT true,
	comments_notification BOOL NOT NULL DEFAULT true,
	tags_notification BOOL NOT NULL DEFAULT true,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	deleted TIMESTAMP NULL,
	"personId" INT8 NULL,
	"businessId" INT8 NULL,
	role_id INT8 NULL,
	"baseBusinessAccountId" INT8 NULL,
	CONSTRAINT "PK_ecbc1d0ca9786e5106474ca46f2" PRIMARY KEY (id ASC),
	INDEX "IDX_a18399d9a2042373ee13fd4e59" ("personId" ASC),
	INDEX "IDX_37b15b7df488f6f0daac2f8015" ("businessId" ASC),
	INDEX "IDX_a9cf1e14d962521484050d231a" (role_id ASC),
	INDEX "IDX_e015a7e38c56f4315214287c14" ("baseBusinessAccountId" ASC),
	FAMILY "primary" (id, followers_notification, likes_notification, comments_notification, tags_notification, created, updated, deleted, "personId", "businessId", role_id, "baseBusinessAccountId")
);

CREATE TABLE t_account_blacklist_t_account (
	"tAccountId_1" INT8 NOT NULL,
	"tAccountId_2" INT8 NOT NULL,
	CONSTRAINT "PK_3bad87f6b222bd6e32d2c55f21c" PRIMARY KEY ("tAccountId_1" ASC, "tAccountId_2" ASC),
	INDEX "IDX_fc056810fc2513bbbfc6419a0b" ("tAccountId_1" ASC),
	INDEX "IDX_fe827b9ed2028fba31442501b0" ("tAccountId_2" ASC),
	FAMILY "primary" ("tAccountId_1", "tAccountId_2")
);

CREATE SEQUENCE t_shop_category_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_shop_category (
	id INT8 NOT NULL DEFAULT nextval('"t_shop_category_id_seq"':::STRING),
	parent_id INT8 NULL,
	name VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_83b06a66f54c91850e1bfe78605" PRIMARY KEY (id ASC),
	INDEX shop_category_product_id_idx (user_id ASC),
	INDEX "IDX_2d1323724d239cb787b0f4c5af" (user_id ASC),
	UNIQUE INDEX "UQ_a562340a819bcc02fb6bdcf5879" (user_id ASC, name ASC),
	FAMILY "primary" (id, parent_id, name, created, updated, deleted, user_id)
);

CREATE SEQUENCE t_product_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_product_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_product_photo_id_seq"':::STRING),
	"photoUrl" VARCHAR NULL,
	"thumbUrl" VARCHAR NULL,
	"order" INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	product_id INT8 NULL,
	CONSTRAINT "PK_8212016d44668179635365e256c" PRIMARY KEY (id ASC),
	INDEX product_photo_product_id_idx (product_id ASC),
	INDEX "IDX_695941bdc0db4f13bba822f22a" (product_id ASC),
	FAMILY "primary" (id, "photoUrl", "thumbUrl", "order", created, deleted, product_id)
);

CREATE SEQUENCE t_product_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_product (
	id INT8 NOT NULL DEFAULT nextval('"t_product_id_seq"':::STRING),
	type VARCHAR NULL,
	title VARCHAR NULL,
	condition VARCHAR NULL,
	price DECIMAL NULL,
	quantity INT8 NULL,
	category VARCHAR NULL,
	description VARCHAR NULL,
	"variantOptions" VARCHAR NULL,
	"deliveryOptions" VARCHAR NULL,
	"deliveryTime" INT8 NULL,
	"shippingCosts" INT8 NULL,
	"payerReturn" VARCHAR NULL,
	bank_id INT8 NULL,
	active BOOL NOT NULL DEFAULT true,
	draft BOOL NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	business_id INT8 NULL,
	shopcategory_id INT8 NULL,
	address_id INT8 NULL,
	cover_id INT8 NULL,
	CONSTRAINT "PK_9ffec69006be8318dd2e6a59b3f" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_e4c8164fb9c0e01953f1cbe9e1" (cover_id ASC),
	INDEX product_bank_id_idx (bank_id ASC),
	INDEX product_address_id_idx (address_id ASC),
	INDEX product_shopcategory_id_idx (shopcategory_id ASC),
	INDEX product_business_id_idx (business_id ASC),
	INDEX product_user_id_idx (user_id ASC),
	INDEX "IDX_45054858963ac0a72830b18adf" (user_id ASC),
	INDEX "IDX_c4cb695880ad06d3e24b4c2fd4" (business_id ASC),
	INDEX "IDX_251ba14ed3702ab8bafa9ef91c" (shopcategory_id ASC),
	INDEX "IDX_885547ba0f6e14a4c45486c135" (address_id ASC),
	INDEX "IDX_e4c8164fb9c0e01953f1cbe9e1" (cover_id ASC),
	FAMILY "primary" (id, type, title, condition, price, quantity, category, description, "variantOptions", "deliveryOptions", "deliveryTime", "shippingCosts", "payerReturn", bank_id, active, draft, created, updated, deleted, user_id, business_id, shopcategory_id, address_id, cover_id)
);

CREATE SEQUENCE t_review_video_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_review_video (
	id INT8 NOT NULL DEFAULT nextval('"t_review_video_id_seq"':::STRING),
	link VARCHAR NULL,
	cover VARCHAR NULL,
	title VARCHAR NULL,
	duration VARCHAR NULL,
	CONSTRAINT "PK_66162f133725a5fbe3588e78b5f" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, link, cover, title, duration)
);

CREATE SEQUENCE t_product_review_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_product_review (
	id INT8 NOT NULL DEFAULT nextval('"t_product_review_id_seq"':::STRING),
	rating INT8 NULL,
	text VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	product_id INT8 NULL,
	"videoId" INT8 NULL,
	CONSTRAINT "PK_cd0f4a4ae08c16b73a74190cd29" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_1626973a7ecc572cb4d15e92ed" ("videoId" ASC),
	INDEX product_review_user_id_idx (user_id ASC),
	INDEX product_review_product_id_idx (product_id ASC),
	INDEX "IDX_660bd3d57a18e21acb61904b86" (user_id ASC),
	INDEX "IDX_611cf5777fd1d9226f22bd5cbd" (product_id ASC),
	INDEX "IDX_1626973a7ecc572cb4d15e92ed" ("videoId" ASC),
	FAMILY "primary" (id, rating, text, created, updated, user_id, product_id, "videoId")
);

CREATE TABLE t_account_product_reviews_likes_t_product_review (
	"tAccountId" INT8 NOT NULL,
	"tProductReviewId" INT8 NOT NULL,
	CONSTRAINT "PK_ce87fd55f17787423d58024e311" PRIMARY KEY ("tAccountId" ASC, "tProductReviewId" ASC),
	INDEX "IDX_f711073fa7c4e10bdd686e0f20" ("tAccountId" ASC),
	INDEX "IDX_2dd67ea2bb4227c14aa8a990fe" ("tProductReviewId" ASC),
	FAMILY "primary" ("tAccountId", "tProductReviewId")
);

CREATE SEQUENCE t_product_review_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_product_review_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_product_review_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	review_id INT8 NULL,
	product_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_f42d666d05e59d6a07bb409a012" PRIMARY KEY (id ASC),
	INDEX "IDX_e67b423683e651eb0615bfe3a2" (review_id ASC),
	INDEX "IDX_f58f40ef57ec1a29fb74f22c7a" (product_id ASC),
	INDEX "IDX_4689d99dc7eca19aac1aff8920" (user_id ASC),
	FAMILY "primary" (id, text, created, updated, review_id, product_id, user_id)
);

CREATE TABLE t_account_product_reviews_replies_likes_t_product_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tProductReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_bc5ae617d0132e08d08efbffa76" PRIMARY KEY ("tAccountId" ASC, "tProductReviewRepliesId" ASC),
	INDEX "IDX_e8ead629df496341580f7087fb" ("tAccountId" ASC),
	INDEX "IDX_7a4955c2b07ad3f92a091de5c9" ("tProductReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tProductReviewRepliesId")
);

CREATE TABLE t_account_product_reviews_replies_t_product_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tProductReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_9733d146f05ea82b155d2481a4c" PRIMARY KEY ("tAccountId" ASC, "tProductReviewRepliesId" ASC),
	INDEX "IDX_475b4f566c68581247dfcb2c30" ("tAccountId" ASC),
	INDEX "IDX_c621ba8d0b19f9a08b8cec949a" ("tProductReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tProductReviewRepliesId")
);

CREATE SEQUENCE t_seller_review_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_seller_review (
	id INT8 NOT NULL DEFAULT nextval('"t_seller_review_id_seq"':::STRING),
	rating INT8 NULL,
	text VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	seller_id INT8 NULL,
	"videoId" INT8 NULL,
	CONSTRAINT "PK_0d6b336eac25728f22980e67937" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_9231df22c5dc5c2006e3244f9b" ("videoId" ASC),
	INDEX seller_review_user_id_idx (user_id ASC),
	INDEX seller_review_seller_id_idx (seller_id ASC),
	INDEX "IDX_be93a478544b14262a99a8838b" (user_id ASC),
	INDEX "IDX_8838197aabdabc1f72ce3b7c30" (seller_id ASC),
	INDEX "IDX_9231df22c5dc5c2006e3244f9b" ("videoId" ASC),
	FAMILY "primary" (id, rating, text, created, updated, user_id, seller_id, "videoId")
);

CREATE TABLE t_account_seller_reviews_likes_t_seller_review (
	"tAccountId" INT8 NOT NULL,
	"tSellerReviewId" INT8 NOT NULL,
	CONSTRAINT "PK_b641671c82c0d96b8f8db3b8495" PRIMARY KEY ("tAccountId" ASC, "tSellerReviewId" ASC),
	INDEX "IDX_66d4b247265510e63fdcc75bcb" ("tAccountId" ASC),
	INDEX "IDX_83432966d9d78e0f4405badaab" ("tSellerReviewId" ASC),
	FAMILY "primary" ("tAccountId", "tSellerReviewId")
);

CREATE SEQUENCE t_seller_review_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_seller_review_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_seller_review_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	review_id INT8 NULL,
	seller_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_7cf7188a6425fe3e66284f05c5e" PRIMARY KEY (id ASC),
	INDEX "IDX_a388eebedd1e25eaad6c49986f" (review_id ASC),
	INDEX "IDX_0355fea30ef0ccbe1a868aa938" (seller_id ASC),
	INDEX "IDX_bf7c0d8714b18a237501e74924" (user_id ASC),
	FAMILY "primary" (id, text, created, updated, review_id, seller_id, user_id)
);

CREATE TABLE t_account_seller_reviews_replies_likes_t_seller_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tSellerReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_ebae5489f6a59264741b8e4bf30" PRIMARY KEY ("tAccountId" ASC, "tSellerReviewRepliesId" ASC),
	INDEX "IDX_588ea502be4a7bdf2bd0f005ce" ("tAccountId" ASC),
	INDEX "IDX_bb1b5027012679e7112cb220c9" ("tSellerReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tSellerReviewRepliesId")
);

CREATE TABLE t_account_seller_reviews_replies_t_seller_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tSellerReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_88309a7a2d60df9bd96ea6ce8d1" PRIMARY KEY ("tAccountId" ASC, "tSellerReviewRepliesId" ASC),
	INDEX "IDX_4404e8d386f5547b36099aff5d" ("tAccountId" ASC),
	INDEX "IDX_1a2fa4a19071300cdee74de30b" ("tSellerReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tSellerReviewRepliesId")
);

CREATE SEQUENCE t_service_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_service (
	id INT8 NOT NULL DEFAULT nextval('"t_service_id_seq"':::STRING),
	active BOOL NULL DEFAULT true,
	draft BOOL NULL DEFAULT false,
	title VARCHAR NULL,
	price DECIMAL NULL,
	quantity INT8 NULL,
	"hourlyPrice" BOOL NULL,
	description VARCHAR NULL,
	performance VARCHAR NULL,
	category VARCHAR NULL,
	bank_id INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	deleted TIMESTAMP NULL,
	user_id INT8 NULL,
	address_id INT8 NULL,
	CONSTRAINT "PK_1a91f546d9cdd542302607e23ce" PRIMARY KEY (id ASC),
	INDEX service_bank_id_idx (bank_id ASC),
	INDEX service_user_id_idx (user_id ASC),
	INDEX "IDX_d6b26007534e6761e1c4e65bcb" (user_id ASC),
	INDEX "IDX_b779a48fb2db76d52987c14c2e" (address_id ASC),
	FAMILY "primary" (id, active, draft, title, price, quantity, "hourlyPrice", description, performance, category, bank_id, created, updated, deleted, user_id, address_id)
);

CREATE SEQUENCE t_service_review_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_service_review (
	id INT8 NOT NULL DEFAULT nextval('"t_service_review_id_seq"':::STRING),
	rating INT8 NULL,
	text VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	service_id INT8 NULL,
	"videoId" INT8 NULL,
	CONSTRAINT "PK_77bbd1e3711d0ca43d188bcb8f3" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_dde0c7acb9a4fa3445d84dd53e" ("videoId" ASC),
	INDEX service_review_user_id_idx (user_id ASC),
	INDEX service_review_service_id_idx (service_id ASC),
	INDEX "IDX_1288cda092c439239c91d56248" (user_id ASC),
	INDEX "IDX_4bce49de508fc298cac740a03b" (service_id ASC),
	INDEX "IDX_dde0c7acb9a4fa3445d84dd53e" ("videoId" ASC),
	FAMILY "primary" (id, rating, text, created, updated, user_id, service_id, "videoId")
);

CREATE TABLE t_account_service_reviews_likes_t_service_review (
	"tAccountId" INT8 NOT NULL,
	"tServiceReviewId" INT8 NOT NULL,
	CONSTRAINT "PK_53afc1ec9877daf588c8ea01d67" PRIMARY KEY ("tAccountId" ASC, "tServiceReviewId" ASC),
	INDEX "IDX_e9ca884fa9ad48df5fcd168c04" ("tAccountId" ASC),
	INDEX "IDX_cca6de899e6c57b6d90f05d613" ("tServiceReviewId" ASC),
	FAMILY "primary" ("tAccountId", "tServiceReviewId")
);

CREATE SEQUENCE t_service_review_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_service_review_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_service_review_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	review_id INT8 NULL,
	service_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_4905ad36679d54a95f54e02bfde" PRIMARY KEY (id ASC),
	INDEX "IDX_4484e7182b1b15f13b0470186a" (review_id ASC),
	INDEX "IDX_c1d7e93610f3b3d62d5542ee5c" (service_id ASC),
	INDEX "IDX_7f95aa959f15274bbf145565d3" (user_id ASC),
	FAMILY "primary" (id, text, created, updated, review_id, service_id, user_id)
);

CREATE TABLE t_account_service_reviews_replies_likes_t_service_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tServiceReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_5b2079e8a382612322030d47c55" PRIMARY KEY ("tAccountId" ASC, "tServiceReviewRepliesId" ASC),
	INDEX "IDX_80a0bacf97d740c40cd21c08c9" ("tAccountId" ASC),
	INDEX "IDX_f1f1c69857942a5e141e9da852" ("tServiceReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tServiceReviewRepliesId")
);

CREATE TABLE t_account_service_reviews_replies_t_service_review_replies (
	"tAccountId" INT8 NOT NULL,
	"tServiceReviewRepliesId" INT8 NOT NULL,
	CONSTRAINT "PK_a976dba24bb32fef961e8c8e147" PRIMARY KEY ("tAccountId" ASC, "tServiceReviewRepliesId" ASC),
	INDEX "IDX_71fffd2c5ff9fd018bb4267314" ("tAccountId" ASC),
	INDEX "IDX_1283472915eb3aab7b4126dd7c" ("tServiceReviewRepliesId" ASC),
	FAMILY "primary" ("tAccountId", "tServiceReviewRepliesId")
);

CREATE SEQUENCE t_article_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article (
	id INT8 NOT NULL DEFAULT nextval('"t_article_id_seq"':::STRING),
	cover VARCHAR NULL,
	thumb VARCHAR NULL,
	title VARCHAR NULL,
	text STRING NULL,
	category VARCHAR NULL,
	location VARCHAR NULL,
	draft BOOL NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	business_address INT8 NULL,
	CONSTRAINT "PK_299f495f41fa2acbde808b4dcd4" PRIMARY KEY (id ASC),
	INDEX article_user_id_idx (user_id ASC),
	INDEX "IDX_9ac0a29f753ed5cac0d1a4321b" (user_id ASC),
	INDEX "IDX_d43809009ac48a652788386bea" (business_address ASC),
	FAMILY "primary" (id, cover, thumb, title, text, category, location, draft, created, updated, deleted, user_id, business_address)
);

CREATE SEQUENCE t_article_comment_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_comment (
	id INT8 NOT NULL DEFAULT nextval('"t_article_comment_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_e03ef89aa02d8fc7cb96b67611e" PRIMARY KEY (id ASC),
	INDEX article_comment_user_id_idx (user_id ASC),
	INDEX "IDX_c5df8481acb3ec66783fda56c5" (user_id ASC),
	INDEX "IDX_d69e86b9ccd74f68e63e16bba4" (article_id ASC),
	FAMILY "primary" (id, text, created, updated, user_id, article_id)
);

CREATE SEQUENCE t_article_comment_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_comment_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_article_comment_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	comment_id INT8 NULL,
	user_id INT8 NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_9ac2d08878790ba432b13868acb" PRIMARY KEY (id ASC),
	INDEX "IDX_d5205061e115011e8efa4b75c2" (comment_id ASC),
	INDEX "IDX_2c3249b01bb62f3ffcd8e4ce59" (user_id ASC),
	INDEX "IDX_03da36fc98270a36927619e034" (article_id ASC),
	FAMILY "primary" (id, text, created, updated, comment_id, user_id, article_id)
);

CREATE SEQUENCE t_article_comment_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_comment_like (
	id INT8 NOT NULL DEFAULT nextval('"t_article_comment_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	comment_id INT8 NULL,
	reply_id INT8 NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_f32c9744a1b7eb8fe577aea6caf" PRIMARY KEY (id ASC),
	INDEX "IDX_2f857dad4c7de1443478f2c7c7" (user_id ASC),
	INDEX "IDX_fd9293bcde871e023e9c0c78e1" (comment_id ASC),
	INDEX "IDX_38c83e283f7e4754cef6a8c2b7" (reply_id ASC),
	INDEX "IDX_fd58ff9a644e5fc49ab6113c9f" (article_id ASC),
	FAMILY "primary" (id, created, updated, user_id, comment_id, reply_id, article_id)
);

CREATE SEQUENCE t_article_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_like (
	id INT8 NOT NULL DEFAULT nextval('"t_article_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	user_id INT8 NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_b59a091520611a98ee98c85437c" PRIMARY KEY (id ASC),
	INDEX like_article_id_idx (article_id ASC),
	INDEX "IDX_00be0b022149db48d5a97921df" (user_id ASC),
	INDEX "IDX_9da4512fc30d1907eb3ac3916b" (article_id ASC),
	INDEX like_user_id_idx (user_id ASC),
	FAMILY "primary" (id, created, user_id, article_id)
);

CREATE SEQUENCE t_article_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_article_photo_id_seq"':::STRING),
	photo VARCHAR NULL,
	thumb VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_a805851ab664701ff811fb77cee" PRIMARY KEY (id ASC),
	INDEX article_photo_article_id_idx (article_id ASC),
	INDEX "IDX_bd5f3c88dfe2de55f177f2b2a7" (article_id ASC),
	FAMILY "primary" (id, photo, thumb, created, deleted, article_id)
);

CREATE SEQUENCE t_article_view_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_article_view (
	id INT8 NOT NULL DEFAULT nextval('"t_article_view_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	user_id INT8 NULL,
	article_id INT8 NULL,
	CONSTRAINT "PK_d3d0d5ecbf36378e85b1af7dec1" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_6464a74e25a930a369f95c0a55e" (user_id ASC, article_id ASC),
	INDEX t_article_view_article_idx (article_id ASC),
	INDEX t_article_view_user_idx (user_id ASC),
	INDEX "IDX_819087eceaa34ed8ffe82e3fb2" (user_id ASC),
	INDEX "IDX_d5e33c2f327a3f8127d6b9fb55" (article_id ASC),
	FAMILY "primary" (id, created, user_id, article_id)
);

CREATE SEQUENCE t_bank_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_bank (
	id INT8 NOT NULL DEFAULT nextval('"t_bank_id_seq"':::STRING),
	holder VARCHAR NULL,
	iban VARCHAR NULL,
	bic VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_2854df7760a96a874e378f5ccf3" PRIMARY KEY (id ASC),
	INDEX bank_user_id_idx (user_id ASC),
	INDEX "IDX_631acaea16abc66e7cf763f06a" (user_id ASC),
	FAMILY "primary" (id, holder, iban, bic, created, updated, deleted, user_id)
);

CREATE SEQUENCE t_booking_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_booking (
	id INT8 NOT NULL DEFAULT nextval('"t_booking_id_seq"':::STRING),
	total_price DECIMAL NOT NULL,
	total_time DECIMAL NOT NULL,
	fee DECIMAL NULL,
	refund_amount DECIMAL NULL,
	amount_card DECIMAL NULL,
	payment_id INT8 NULL,
	payment_status VARCHAR NULL,
	payment_type VARCHAR NULL,
	payment_date TIMESTAMP NULL DEFAULT NULL,
	pay_in_id VARCHAR NULL,
	pay_in_status VARCHAR NULL,
	pay_in_date TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	pay_in_refund_status VARCHAR NULL,
	pay_in_refund_date TIMESTAMP NULL,
	pay_out_status VARCHAR NULL,
	pay_out_date TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	pay_out_refund_status VARCHAR NULL,
	pay_out_refund_date TIMESTAMP NULL,
	transaction INT8 NULL,
	transfer_id INT8 NULL,
	transfer_status VARCHAR NULL,
	transfer_date TIMESTAMP NULL,
	who_cancelled VARCHAR NULL,
	when_cancelled VARCHAR NULL,
	cancel_reason VARCHAR(255) NULL,
	cancel_description VARCHAR(500) NULL,
	paid TIMESTAMP NULL,
	is_completed BOOL NOT NULL DEFAULT false,
	cancelled TIMESTAMP NULL,
	refunded TIMESTAMP NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	buyer_id INT8 NULL,
	seller_id INT8 NULL,
	delivery_address_id INT8 NULL,
	billing_address_id INT8 NULL,
	service_id INT8 NULL,
	CONSTRAINT "PK_8fbe64b4cf867d7ce533eb1050e" PRIMARY KEY (id ASC),
	INDEX "IDX_fc481f354786482f37adadad68" (buyer_id ASC),
	INDEX "IDX_8996cbb5f77368677b8b426dee" (seller_id ASC),
	INDEX "IDX_7ac87a8295c1e57a9ab49d7c25" (delivery_address_id ASC),
	INDEX "IDX_d0cc1171cbb9057a783f65710f" (billing_address_id ASC),
	INDEX "IDX_7b0c47906c6b219d60538c0bbb" (service_id ASC),
	INDEX billing_address_id_idx (billing_address_id ASC),
	INDEX delivery_address_id_idx (delivery_address_id ASC),
	INDEX seller_id_idx (seller_id ASC),
	INDEX buyer_id_idx (buyer_id ASC),
	FAMILY "primary" (id, total_price, total_time, fee, refund_amount, amount_card, payment_id, payment_status, payment_type, payment_date, pay_in_id, pay_in_status, pay_in_date, pay_in_refund_status, pay_in_refund_date, pay_out_status, pay_out_date, pay_out_refund_status, pay_out_refund_date, transaction, transfer_id, transfer_status, transfer_date, who_cancelled, when_cancelled, cancel_reason, cancel_description, paid, is_completed, cancelled, refunded, created, updated, buyer_id, seller_id, delivery_address_id, billing_address_id, service_id)
);

CREATE SEQUENCE t_booking_appointments_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_booking_appointments (
	id INT8 NOT NULL DEFAULT nextval('"t_booking_appointments_id_seq"':::STRING),
	start TIMESTAMP NULL,
	"end" TIMESTAMP NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	booking_id INT8 NULL,
	CONSTRAINT "PK_1264d52dade2cea18c6f9818a6b" PRIMARY KEY (id ASC),
	INDEX booking_id_idx (booking_id ASC),
	INDEX "IDX_1eb29647cb75a44260748c5092" (booking_id ASC),
	FAMILY "primary" (id, start, "end", created, updated, booking_id)
);

CREATE SEQUENCE t_text_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text (
	id INT8 NOT NULL DEFAULT nextval('"t_text_id_seq"':::STRING),
	text STRING NULL,
	draft BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_29d7cfd91b95c14e8753171d153" PRIMARY KEY (id ASC),
	INDEX text_user_id_idx (user_id ASC),
	INDEX "IDX_242e82f7c6adf503b79043e80b" (user_id ASC),
	FAMILY "primary" (id, text, draft, created, updated, deleted, user_id)
);

CREATE SEQUENCE t_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_id_seq"':::STRING),
	photo VARCHAR NULL,
	thumb VARCHAR NULL,
	title VARCHAR NULL,
	description VARCHAR NULL,
	category VARCHAR NULL,
	location VARCHAR NULL,
	draft BOOL NOT NULL DEFAULT false,
	"filterName" VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	business_address INT8 NULL,
	CONSTRAINT "PK_fa06b0c009be5a330409caa4a90" PRIMARY KEY (id ASC),
	INDEX photo_user_id_idx (user_id ASC),
	INDEX "IDX_f7442330e2a46235db02d16583" (user_id ASC),
	INDEX "IDX_f6d9d497516a33a35003796a37" (business_address ASC),
	FAMILY "primary" (id, photo, thumb, title, description, category, location, draft, "filterName", created, updated, deleted, user_id, business_address)
);

CREATE SEQUENCE t_video_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video (
	id INT8 NOT NULL DEFAULT nextval('"t_video_id_seq"':::STRING),
	video VARCHAR NULL,
	cover VARCHAR NULL,
	thumb VARCHAR NULL,
	title VARCHAR NULL,
	description VARCHAR NULL,
	duration VARCHAR NULL,
	category VARCHAR NULL,
	location VARCHAR NULL,
	draft BOOL NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	business_address INT8 NULL,
	CONSTRAINT "PK_e20917db584a2ee0a58af679cf5" PRIMARY KEY (id ASC),
	INDEX video_user_id_idx (user_id ASC),
	INDEX "IDX_e70f50a324682a7b52545a6e2b" (user_id ASC),
	INDEX "IDX_e01f6ac798813d598a91fa3930" (business_address ASC),
	FAMILY "primary" (id, video, cover, thumb, title, description, duration, category, location, draft, created, updated, deleted, user_id, business_address)
);

CREATE SEQUENCE t_bookmarks_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_bookmarks (
	id INT8 NOT NULL DEFAULT nextval('"t_bookmarks_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	owner_id INT8 NULL,
	account_id INT8 NULL,
	text_id INT8 NULL,
	photo_id INT8 NULL,
	video_id INT8 NULL,
	article_id INT8 NULL,
	product_id INT8 NULL,
	service_id INT8 NULL,
	CONSTRAINT "PK_ba8f36f7c53db3606c65e426819" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_b5d3bd3b63288c632004cfb06f1" (owner_id ASC, account_id ASC, text_id ASC, photo_id ASC, video_id ASC, article_id ASC, product_id ASC, service_id ASC),
	INDEX "IDX_edcf0c528ad1a832f2aba3de29" (created ASC),
	INDEX service_id_idx (service_id ASC),
	INDEX text_id_idx (text_id ASC),
	INDEX photo_id_idx (photo_id ASC),
	INDEX video_id_idx (video_id ASC),
	INDEX article_id_idx (article_id ASC),
	INDEX bookmarks_owner_id_idx (owner_id ASC),
	INDEX "IDX_e17dc82a08cd8da59f7e831330" (owner_id ASC),
	INDEX "IDX_7a20246b7dc1966d51222f1217" (account_id ASC),
	INDEX "IDX_7f3d4605368f1bbe200805a21a" (text_id ASC),
	INDEX "IDX_e1f00b4dfa6f8cc34b524d2c22" (photo_id ASC),
	INDEX "IDX_e991c5cbb90e41dd581e7e3ee5" (video_id ASC),
	INDEX "IDX_5786afa3f5f20f9a08bbc71346" (article_id ASC),
	INDEX "IDX_883b1491144e3c76eeb53643c5" (product_id ASC),
	INDEX "IDX_0712cbce462ecd7770d968baa4" (service_id ASC),
	FAMILY "primary" (id, created, owner_id, account_id, text_id, photo_id, video_id, article_id, product_id, service_id)
);

CREATE SEQUENCE t_business_user_category_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_business_user_category (
	id INT8 NOT NULL DEFAULT nextval('"t_business_user_category_id_seq"':::STRING),
	parent_id INT8 NULL,
	name VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	CONSTRAINT "PK_da66102c804f8b987d6f77eb2ac" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, parent_id, name, created, updated, deleted)
);

CREATE SEQUENCE t_cancel_booking_reason_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_cancel_booking_reason (
	id INT8 NOT NULL DEFAULT nextval('"t_cancel_booking_reason_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_fbfd76a988107b84afa6b75b67c" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_db19d8bec675badc16b63d2ceec" (name ASC),
	FAMILY "primary" (id, name)
);

CREATE SEQUENCE t_cancel_order_reason_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_cancel_order_reason (
	id INT8 NOT NULL DEFAULT nextval('"t_cancel_order_reason_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_a7ae2704b580aeb176d69a2e92c" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_5e6f7e480f1731ff7b2d53cfc86" (name ASC),
	FAMILY "primary" (id, name)
);

CREATE SEQUENCE t_product_variant_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_product_variant (
	id INT8 NOT NULL DEFAULT nextval('"t_product_variant_id_seq"':::STRING),
	size VARCHAR NULL,
	color VARCHAR NULL,
	flavor VARCHAR NULL,
	material VARCHAR NULL,
	price DECIMAL NULL,
	quantity INT8 NULL,
	enable BOOL NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	product_id INT8 NULL,
	CONSTRAINT "PK_1d76ed7ba407bec56509f5e5b69" PRIMARY KEY (id ASC),
	INDEX product_variant_product_id_idx (product_id ASC),
	INDEX "IDX_54cb1b5feee114a2a46f39cdc0" (product_id ASC),
	FAMILY "primary" (id, size, color, flavor, material, price, quantity, enable, created, updated, deleted, product_id)
);

CREATE SEQUENCE t_cart_product_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_cart_product (
	id INT8 NOT NULL DEFAULT nextval('"t_cart_product_id_seq"':::STRING),
	quantity INT8 NULL,
	collected BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	product_id INT8 NULL,
	product_variant_id INT8 NULL,
	CONSTRAINT "PK_898ba321929af6e65d281e58f06" PRIMARY KEY (id ASC),
	INDEX "IDX_163dbcfd5a10909c6a9c55e763" (user_id ASC),
	INDEX "IDX_bc5c7493732ee0ef7a76ae62f5" (product_id ASC),
	INDEX "IDX_0226892fb83f0e6f04fd7c0755" (product_variant_id ASC),
	FAMILY "primary" (id, quantity, collected, created, updated, user_id, product_id, product_variant_id)
);

CREATE SEQUENCE t_chatroom_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_chatroom (
	id INT8 NOT NULL DEFAULT nextval('"t_chatroom_id_seq"':::STRING),
	title VARCHAR NULL,
	cover VARCHAR NULL,
	thumb VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	updated TIMESTAMP NULL,
	CONSTRAINT "PK_6fae98f858ecfed3a5ba636d024" PRIMARY KEY (id ASC),
	INDEX "IDX_b7925c3a5b1a98b1f9196118eb" (title ASC),
	FAMILY "primary" (id, title, cover, thumb, created, deleted, updated)
);

CREATE SEQUENCE t_message_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_message (
	id INT8 NOT NULL DEFAULT nextval('"t_message_id_seq"':::STRING),
	text VARCHAR NULL,
	is_forwarded BOOL NOT NULL DEFAULT false,
	is_service_message BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	updated TIMESTAMP NULL,
	reply_to INT8 NULL,
	chatroom_id INT8 NULL,
	user_id INT8 NULL,
	shared_user_id INT8 NULL,
	shared_text_id INT8 NULL,
	shared_photo_id INT8 NULL,
	shared_video_id INT8 NULL,
	shared_article_id INT8 NULL,
	shared_product_id INT8 NULL,
	shared_service_id INT8 NULL,
	CONSTRAINT "PK_3049afb68b218c04f03a0e451ef" PRIMARY KEY (id ASC),
	INDEX "IDX_e797c553e3566025fa1904e417" (text ASC),
	INDEX "IDX_7aef1713d0d8ae2d08ec0a69d9" (reply_to ASC),
	INDEX "IDX_0623f6ce819048037202484576" (chatroom_id ASC),
	INDEX "IDX_285a93487e9c8f44eb9a141f42" (user_id ASC),
	INDEX "IDX_0d3eff2cf377d3aeb81e9e24df" (shared_user_id ASC),
	INDEX "IDX_725da9ab95c73ad42ba931b448" (shared_text_id ASC),
	INDEX "IDX_d9c65b8d75d4ea1f4e60ca8393" (shared_photo_id ASC),
	INDEX "IDX_3dbc933de81bad609549823e5b" (shared_video_id ASC),
	INDEX "IDX_b9ba907c408c60674f1911c1a0" (shared_article_id ASC),
	INDEX "IDX_4d5288130018cd5bb414b0c9c3" (shared_product_id ASC),
	INDEX "IDX_ffdef196f083b2c7d886abf646" (shared_service_id ASC),
	FAMILY "primary" (id, text, is_forwarded, is_service_message, created, deleted, updated, reply_to, chatroom_id, user_id, shared_user_id, shared_text_id, shared_photo_id, shared_video_id, shared_article_id, shared_product_id, shared_service_id)
);

CREATE SEQUENCE t_chatroom_user_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_chatroom_user (
	id INT8 NOT NULL DEFAULT nextval('"t_chatroom_user_id_seq"':::STRING),
	is_muted BOOL NOT NULL DEFAULT false,
	is_blocked BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	updated TIMESTAMP NULL,
	chatroom_id INT8 NULL,
	user_id INT8 NULL,
	last_read_message INT8 NULL,
	CONSTRAINT "PK_eb24d4d8586f7d5fc47c3b7baab" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_6607be8cf4a1c1b60c79fd286d0" (chatroom_id ASC, user_id ASC),
	INDEX "IDX_923b7160e827046cf221f68122" (chatroom_id ASC),
	INDEX "IDX_21566604d65d58adc527c2979b" (user_id ASC),
	INDEX "IDX_88e695691b3ba95913a0cff17c" (last_read_message ASC),
	FAMILY "primary" (id, is_muted, is_blocked, created, deleted, updated, chatroom_id, user_id, last_read_message)
);

CREATE SEQUENCE t_contact_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_contact (
	id INT8 NOT NULL DEFAULT nextval('"t_contact_id_seq"':::STRING),
	contact INT8 NULL,
	total INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_1d66f7e6f6ed52b8584bdef005f" PRIMARY KEY (id ASC),
	INDEX contact_user_id_idx (user_id ASC),
	INDEX "IDX_ca821105d7986c98db6d39c3e4" (user_id ASC),
	FAMILY "primary" (id, contact, total, created, updated, user_id)
);

CREATE SEQUENCE t_days_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_days (
	id INT8 NOT NULL DEFAULT nextval('"t_days_id_seq"':::STRING),
	day VARCHAR NOT NULL,
	CONSTRAINT "PK_7bd361eba9b05b03b26735f3060" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_b3c67bd42d28cf14bd489162555" (day ASC),
	FAMILY "primary" (id, day)
);

CREATE SEQUENCE t_followings_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_followings (
	id INT8 NOT NULL DEFAULT nextval('"t_followings_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	"accountId" INT8 NULL,
	"followedAccountId" INT8 NULL,
	CONSTRAINT "PK_2d92a1eb337a2b6b8b02708758a" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_2d5e601e33822dc7a9e4e164b92" ("accountId" ASC, "followedAccountId" ASC),
	INDEX "IDX_b818092d1ced5a1910b392d197" ("accountId" ASC),
	INDEX "IDX_e4b6ea262c5c80358090b82ac6" ("followedAccountId" ASC),
	FAMILY "primary" (id, created, "accountId", "followedAccountId")
);

CREATE SEQUENCE t_followings_request_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_followings_request (
	id INT8 NOT NULL DEFAULT nextval('"t_followings_request_id_seq"':::STRING),
	status VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	"accountId" INT8 NULL,
	"followedAccountId" INT8 NULL,
	CONSTRAINT "PK_14cf9762423b54373e1b01f60dd" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_a428b1794ac7c122547396729fc" ("accountId" ASC, "followedAccountId" ASC),
	INDEX "IDX_8bc4d5a28feeffcb9a8fc2c970" ("accountId" ASC),
	INDEX "IDX_2fa247efdbc9a14f68e49fc9fb" ("followedAccountId" ASC),
	FAMILY "primary" (id, status, created, updated, "accountId", "followedAccountId")
);

CREATE SEQUENCE t_hashtag_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_hashtag (
	id INT8 NOT NULL DEFAULT nextval('"t_hashtag_id_seq"':::STRING),
	tag VARCHAR NOT NULL,
	photo_id INT8 NULL,
	video_id INT8 NULL,
	article_id INT8 NULL,
	text_id INT8 NULL,
	product_id INT8 NULL,
	service_id INT8 NULL,
	CONSTRAINT "PK_b35bf758a3a61ee8cca6b3b9fce" PRIMARY KEY (id ASC),
	INDEX "IDX_ac74d00d3e335a645dd222cad1" (photo_id ASC),
	INDEX "IDX_729199c811532a10df28afcea3" (video_id ASC),
	INDEX "IDX_9485ccfbc9b48f6c349307e243" (article_id ASC),
	INDEX "IDX_9e418db5f56016f946801f7740" (text_id ASC),
	INDEX "IDX_37562e8b412412cf04a26122f8" (product_id ASC),
	INDEX "IDX_a5c447ada045b8ead93a20ff5a" (service_id ASC),
	FAMILY "primary" (id, tag, photo_id, video_id, article_id, text_id, product_id, service_id)
);

CREATE SEQUENCE t_hours_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_hours (
	id INT8 NOT NULL DEFAULT nextval('"t_hours_id_seq"':::STRING),
	from_h TIME NULL,
	to_h TIME NULL,
	created TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	business_id INT8 NULL,
	day_id INT8 NULL,
	CONSTRAINT "PK_cf6240e48674eb5c1802369d3a0" PRIMARY KEY (id ASC),
	INDEX hours_business_id_idx (business_id ASC),
	INDEX "IDX_d4d7c9d84552c5488cc4e68ac0" (business_id ASC),
	INDEX "IDX_7edce1846c726be8729da94c68" (day_id ASC),
	FAMILY "primary" (id, from_h, to_h, created, updated, business_id, day_id)
);

CREATE SEQUENCE t_languages_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_languages (
	id INT8 NOT NULL DEFAULT nextval('"t_languages_id_seq"':::STRING),
	value VARCHAR NOT NULL,
	display_name VARCHAR NOT NULL,
	"default" VARCHAR NULL,
	CONSTRAINT "PK_bcb2789e823ea34267373fca3a0" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, value, display_name, "default")
);

CREATE SEQUENCE t_photo_comment_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_comment (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_comment_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_94d252e4c419d8894aa45e59342" PRIMARY KEY (id ASC),
	INDEX comment_photo_id_idx (photo_id ASC),
	INDEX comment_user_id_idx (user_id ASC),
	INDEX "IDX_a25bd5566d4892334db9b93b4f" (user_id ASC),
	INDEX "IDX_5f691231453016a73b881efcce" (photo_id ASC),
	FAMILY "primary" (id, text, created, updated, user_id, photo_id)
);

CREATE SEQUENCE t_photo_comment_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_comment_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_comment_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	comment_id INT8 NULL,
	user_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_fae9340cb9af464576d1f639baf" PRIMARY KEY (id ASC),
	INDEX "IDX_f7c9dc334b6490793d402fd570" (comment_id ASC),
	INDEX "IDX_5c87d3c4e0e9b5bac7e13d655a" (user_id ASC),
	INDEX "IDX_c65dd5b367b232c9bd8d008810" (photo_id ASC),
	FAMILY "primary" (id, text, created, updated, comment_id, user_id, photo_id)
);

CREATE SEQUENCE t_text_comment_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text_comment (
	id INT8 NOT NULL DEFAULT nextval('"t_text_comment_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	text_id INT8 NULL,
	CONSTRAINT "PK_f88f2c913fe279611c43b3bbf9d" PRIMARY KEY (id ASC),
	INDEX text_comment_user_id_idx (user_id ASC),
	INDEX "IDX_0c6c2da8e5d0ea2e9283cfd0bb" (user_id ASC),
	INDEX "IDX_863094a95086518839f6b2c5b8" (text_id ASC),
	FAMILY "primary" (id, text, created, updated, user_id, text_id)
);

CREATE SEQUENCE t_text_comment_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text_comment_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_text_comment_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	comment_id INT8 NULL,
	text_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_f519071c42b8189c7afa87d2252" PRIMARY KEY (id ASC),
	INDEX "IDX_91dd94bf184b02c916d160768d" (comment_id ASC),
	INDEX "IDX_94b7688020275540f2f81e45c1" (text_id ASC),
	INDEX "IDX_9964632e254c6403d17681fe07" (user_id ASC),
	FAMILY "primary" (id, text, created, updated, comment_id, text_id, user_id)
);

CREATE SEQUENCE t_video_comment_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video_comment (
	id INT8 NOT NULL DEFAULT nextval('"t_video_comment_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	video_id INT8 NULL,
	CONSTRAINT "PK_3c57ac139634255cc600c91b4d8" PRIMARY KEY (id ASC),
	INDEX video_comment_user_id_idx (user_id ASC),
	INDEX "IDX_205378b58cdd060c3aec791cb0" (user_id ASC),
	INDEX "IDX_701faf0d0560c553ec1cf3c867" (video_id ASC),
	FAMILY "primary" (id, text, created, updated, user_id, video_id)
);

CREATE SEQUENCE t_video_comment_replies_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video_comment_replies (
	id INT8 NOT NULL DEFAULT nextval('"t_video_comment_replies_id_seq"':::STRING),
	text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	comment_id INT8 NULL,
	video_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_93db44fb3d70248450df8a938f7" PRIMARY KEY (id ASC),
	INDEX "IDX_69aeaf2544a265802d7d1eda84" (comment_id ASC),
	INDEX "IDX_ea4a5d256d2d3e592459ccda20" (video_id ASC),
	INDEX "IDX_692b6a04ccf7aa668af06b8d7b" (user_id ASC),
	FAMILY "primary" (id, text, created, updated, comment_id, video_id, user_id)
);

CREATE SEQUENCE t_mentions_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_mentions (
	id INT8 NOT NULL DEFAULT nextval('"t_mentions_id_seq"':::STRING),
	mention_text VARCHAR NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	"targetId" INT8 NULL,
	"articleId" INT8 NULL,
	"photoId" INT8 NULL,
	"textId" INT8 NULL,
	"videoId" INT8 NULL,
	"sellerreviewId" INT8 NULL,
	"productId" INT8 NULL,
	"productReviewId" INT8 NULL,
	"serviceReviewId" INT8 NULL,
	"serviceId" INT8 NULL,
	"articleCommentId" INT8 NULL,
	"articleCommentReplyId" INT8 NULL,
	"photoCommentId" INT8 NULL,
	"photoCommentReplyId" INT8 NULL,
	"textCommentId" INT8 NULL,
	"textCommentReplyId" INT8 NULL,
	"videoCommentId" INT8 NULL,
	"videoCommentReplyId" INT8 NULL,
	"productReviewReplyId" INT8 NULL,
	"serviceReviewReplyId" INT8 NULL,
	"sellerReviewReplyId" INT8 NULL,
	CONSTRAINT "PK_a5d519c4edcff41eabbeb075eb5" PRIMARY KEY (id ASC),
	INDEX "IDX_eeb477829ab5d787007057f63c" ("targetId" ASC),
	INDEX "IDX_0f374b7c29f1f53e113330f9cb" ("articleId" ASC),
	INDEX "IDX_cc7d843a880162589ddb0d56d6" ("photoId" ASC),
	INDEX "IDX_d358d7c8a748dc9d92089b48dd" ("textId" ASC),
	INDEX "IDX_f6ec87772fe708eea08df801ab" ("videoId" ASC),
	INDEX "IDX_99214cbf26145afc34c57c4154" ("sellerreviewId" ASC),
	INDEX "IDX_94abe551a95ba041ce9f7bbe05" ("productId" ASC),
	INDEX "IDX_272e565ef73e595f5c94163e0c" ("productReviewId" ASC),
	INDEX "IDX_d5adc12b709d6eab1e60c4f562" ("serviceReviewId" ASC),
	INDEX "IDX_e6ee40a24c7d51956df16603d7" ("serviceId" ASC),
	INDEX "IDX_37e9ba2b109caa824be5b46725" ("articleCommentId" ASC),
	INDEX "IDX_a88d79c41f5ac0b8ba7eb9f473" ("articleCommentReplyId" ASC),
	INDEX "IDX_6230a68b13e82f06ab22b5c471" ("photoCommentId" ASC),
	INDEX "IDX_feb971b64a7150496b0149b82c" ("photoCommentReplyId" ASC),
	INDEX "IDX_417ffd432c47dbd643fa7a827d" ("textCommentId" ASC),
	INDEX "IDX_105fe0692ecff8b2c129e583fd" ("textCommentReplyId" ASC),
	INDEX "IDX_fa2012dfc0a949bb7586ac74de" ("videoCommentId" ASC),
	INDEX "IDX_07f86ec42eee63eaaa368ed1c3" ("videoCommentReplyId" ASC),
	INDEX "IDX_678b749b5850adf61ccdd4b6ce" ("productReviewReplyId" ASC),
	INDEX "IDX_f2c52d3bd2365501cd4d4bb6bf" ("serviceReviewReplyId" ASC),
	INDEX "IDX_3ab9e09306811e5e845bed9dbc" ("sellerReviewReplyId" ASC),
	FAMILY "primary" (id, mention_text, created, "targetId", "articleId", "photoId", "textId", "videoId", "sellerreviewId", "productId", "productReviewId", "serviceReviewId", "serviceId", "articleCommentId", "articleCommentReplyId", "photoCommentId", "photoCommentReplyId", "textCommentId", "textCommentReplyId", "videoCommentId", "videoCommentReplyId", "productReviewReplyId", "serviceReviewReplyId", "sellerReviewReplyId")
);

CREATE SEQUENCE t_message_attachment_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_message_attachment (
	id INT8 NOT NULL DEFAULT nextval('"t_message_attachment_id_seq"':::STRING),
	link VARCHAR NULL,
	thumb VARCHAR NULL,
	duration VARCHAR NULL,
	file_name VARCHAR NULL,
	type VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	message_id INT8 NULL,
	chatroom_id INT8 NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_3831708696a94550b930078fde8" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_d44b182239cc3b0ab697fed96f" (message_id ASC),
	INDEX "IDX_d5587456c88c43fd1cf518c5b9" (type ASC),
	INDEX "IDX_d44b182239cc3b0ab697fed96f" (message_id ASC),
	INDEX "IDX_55a37dc0128b8755737f741602" (chatroom_id ASC),
	INDEX "IDX_a2fd1fb1d131e3e5a4c47bd4de" (user_id ASC),
	FAMILY "primary" (id, link, thumb, duration, file_name, type, created, deleted, message_id, chatroom_id, user_id)
);

CREATE SEQUENCE t_notifications_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_notifications (
	id INT8 NOT NULL DEFAULT nextval('"t_notifications_id_seq"':::STRING),
	action VARCHAR NOT NULL,
	content_display_name VARCHAR NULL,
	content_type VARCHAR NULL,
	read BOOL NOT NULL DEFAULT false,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	"textId" INT8 NULL,
	"photoId" INT8 NULL,
	"videoId" INT8 NULL,
	"articleId" INT8 NULL,
	"productId" INT8 NULL,
	"serviceId" INT8 NULL,
	"productReviewId" INT8 NULL,
	"serviceReviewId" INT8 NULL,
	"sellerReviewId" INT8 NULL,
	"issuerId" INT8 NULL,
	"ownerId" INT8 NULL,
	"productVariantId" INT8 NULL,
	CONSTRAINT "PK_f1b6a4ac070d9a7330f7018b287" PRIMARY KEY (id ASC),
	INDEX "IDX_603eb0f989e26b4968093835c7" (created ASC),
	INDEX notifications_owner_id_idx ("ownerId" ASC),
	INDEX "IDX_478d23250857f9ad58bacf1db4" ("textId" ASC),
	INDEX "IDX_b366a18fe393d680a8315ebae7" ("photoId" ASC),
	INDEX "IDX_8b1553392dc9779840c3995c12" ("videoId" ASC),
	INDEX "IDX_6491ef304f384feb8023a1607e" ("articleId" ASC),
	INDEX "IDX_2f115993f8f854ebd4b9820b16" ("productId" ASC),
	INDEX "IDX_4d231f422eeb04b9827fc2e299" ("serviceId" ASC),
	INDEX "IDX_2c1f712a42da2408796d3263f4" ("productReviewId" ASC),
	INDEX "IDX_29577a7c98e1c44435e00e9a34" ("serviceReviewId" ASC),
	INDEX "IDX_a6c2c5dc815a42bb5810c086d8" ("sellerReviewId" ASC),
	INDEX "IDX_235f58a5420e14ef0856605ad8" ("issuerId" ASC),
	INDEX "IDX_6aa389cf5592d9d286c2a66cc7" ("ownerId" ASC),
	INDEX "IDX_92aa19cfebba003b8c82b6ef7f" ("productVariantId" ASC),
	FAMILY "primary" (id, action, content_display_name, content_type, read, created, "textId", "photoId", "videoId", "articleId", "productId", "serviceId", "productReviewId", "serviceReviewId", "sellerReviewId", "issuerId", "ownerId", "productVariantId")
);

CREATE SEQUENCE t_order_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_order (
	id INT8 NOT NULL DEFAULT nextval('"t_order_id_seq"':::STRING),
	total_price DECIMAL NOT NULL,
	shipping_price DECIMAL NOT NULL,
	fee DECIMAL NULL,
	refund_amount DECIMAL NULL,
	amount_card DECIMAL NOT NULL DEFAULT 0,
	amount_wallet DECIMAL NOT NULL DEFAULT 0,
	payment_id INT8 NULL,
	payment_status VARCHAR NULL,
	payment_type VARCHAR NULL,
	payment_date TIMESTAMP NULL DEFAULT NULL,
	is_wallet BOOL NOT NULL DEFAULT false,
	pay_in_id VARCHAR NULL,
	pay_in_status VARCHAR NULL,
	pay_in_date TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	pay_in_refund_status VARCHAR NULL,
	pay_in_refund_date TIMESTAMP NULL,
	pay_out_status VARCHAR NULL,
	pay_out_date TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	pay_out_refund_status VARCHAR NULL,
	pay_out_refund_date TIMESTAMP NULL,
	transaction INT8 NULL,
	shipping_transfer_id INT8 NULL,
	transfer_status VARCHAR NULL,
	transfer_date TIMESTAMP NULL,
	who_cancelled VARCHAR NULL,
	when_cancelled VARCHAR NULL,
	cancel_reason VARCHAR(255) NULL,
	cancel_description VARCHAR(500) NULL,
	return_reason VARCHAR(255) NULL,
	return_description VARCHAR(500) NULL,
	paid TIMESTAMP NULL,
	cancelled TIMESTAMP NULL,
	refunded TIMESTAMP NULL,
	shipped TIMESTAMP NULL,
	received TIMESTAMP NULL,
	returned TIMESTAMP NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	buyer_id INT8 NULL,
	seller_id INT8 NULL,
	delivery_address_id INT8 NULL,
	billing_address_id INT8 NULL,
	CONSTRAINT "PK_5920ec217212c660b67ba1073a2" PRIMARY KEY (id ASC),
	INDEX "IDX_01e601f83c27b53fed6c64d4f8" (buyer_id ASC),
	INDEX "IDX_cb3fb4f54860f59bd8a173b25d" (seller_id ASC),
	INDEX "IDX_bdecbd24fbcf8a9ffdea09bd5a" (delivery_address_id ASC),
	INDEX "IDX_818c71780238bdb89b1f8ca5f2" (billing_address_id ASC),
	INDEX billing_address_id_idx (billing_address_id ASC),
	INDEX delivery_address_id_idx (delivery_address_id ASC),
	INDEX seller_id_idx (seller_id ASC),
	INDEX buyer_id_idx (buyer_id ASC),
	FAMILY "primary" (id, total_price, shipping_price, fee, refund_amount, amount_card, amount_wallet, payment_id, payment_status, payment_type, payment_date, is_wallet, pay_in_id, pay_in_status, pay_in_date, pay_in_refund_status, pay_in_refund_date, pay_out_status, pay_out_date, pay_out_refund_status, pay_out_refund_date, transaction, shipping_transfer_id, transfer_status, transfer_date, who_cancelled, when_cancelled, cancel_reason, cancel_description, return_reason, return_description, paid, cancelled, refunded, shipped, received, returned, created, updated, buyer_id, seller_id, delivery_address_id, billing_address_id)
);

CREATE SEQUENCE t_order_product_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_order_product (
	id INT8 NOT NULL DEFAULT nextval('"t_order_product_id_seq"':::STRING),
	quantity INT8 NOT NULL,
	collect BOOL NULL,
	price DECIMAL NOT NULL DEFAULT 0,
	shipping_price DECIMAL NOT NULL DEFAULT 0,
	payment_id VARCHAR NULL,
	reason VARCHAR NULL,
	who_cancelled VARCHAR NULL,
	when_cancelled VARCHAR NULL,
	cancel_reason VARCHAR(255) NULL,
	cancel_description VARCHAR(500) NULL,
	return_reason VARCHAR(255) NULL,
	return_description VARCHAR(500) NULL,
	shipped TIMESTAMP NULL DEFAULT NULL,
	collected TIMESTAMP NULL DEFAULT NULL,
	canceled TIMESTAMP NULL DEFAULT NULL,
	returned TIMESTAMP NULL DEFAULT NULL,
	received TIMESTAMP NULL DEFAULT NULL,
	refunded TIMESTAMP NULL DEFAULT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	transfer_id INT8 NULL,
	transfer_status VARCHAR NULL,
	transfer_date TIMESTAMP NULL,
	pay_in_id INT8 NULL,
	reviewed TIMESTAMP NULL DEFAULT NULL,
	order_id INT8 NULL,
	product_id INT8 NULL,
	product_variant_id INT8 NULL,
	return_shipping_cost DECIMAL NULL,
	CONSTRAINT "PK_8ae7260addd7034cc7a64fec5b6" PRIMARY KEY (id ASC),
	INDEX product_variant_id_idx (product_variant_id ASC),
	INDEX product_id_idx (product_id ASC),
	INDEX order_id_idx (order_id ASC),
	INDEX "IDX_aa1b4e0484b429d143391e26d5" (order_id ASC),
	INDEX "IDX_7d08b062a34133a930ece4418a" (product_id ASC),
	INDEX "IDX_16655d6f38366aefebc33f2391" (product_variant_id ASC),
	FAMILY "primary" (id, quantity, collect, price, shipping_price, payment_id, reason, who_cancelled, when_cancelled, cancel_reason, cancel_description, return_reason, return_description, shipped, collected, canceled, returned, received, refunded, created, updated, transfer_id, transfer_status, transfer_date, pay_in_id, reviewed, order_id, product_id, product_variant_id, return_shipping_cost)
);

CREATE SEQUENCE t_photo_annotation_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_annotation (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_annotation_id_seq"':::STRING),
	x INT8 NOT NULL,
	y INT8 NOT NULL,
	target_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_b9ac207aad646f6489a14232208" PRIMARY KEY (id ASC),
	INDEX "IDX_25ab23c20cb3ce4afcf74fddb6" (target_id ASC),
	INDEX "IDX_c6f20d0a4fe95a7e12e291d74c" (photo_id ASC),
	FAMILY "primary" (id, x, y, target_id, photo_id)
);

CREATE SEQUENCE t_photo_comment_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_comment_like (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_comment_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	comment_id INT8 NULL,
	reply_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_f05ab656c2ba1f2fce2ee1301e6" PRIMARY KEY (id ASC),
	INDEX "IDX_30b668823a066d4006740dd88c" (user_id ASC),
	INDEX "IDX_1b033d72d97e0bb1e3b8797b6a" (comment_id ASC),
	INDEX "IDX_6f39cbd82a241b3610c9367557" (reply_id ASC),
	INDEX "IDX_9341690c97d0ac9fc7435c8d64" (photo_id ASC),
	FAMILY "primary" (id, created, updated, user_id, comment_id, reply_id, photo_id)
);

CREATE SEQUENCE t_photo_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_like (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_40ce3cbd7830393bf8a500315f0" PRIMARY KEY (id ASC),
	INDEX like_photo_id_idx (photo_id ASC),
	INDEX "IDX_3369e5fdb8423ed9c023bee2c4" (user_id ASC),
	INDEX "IDX_dbb0770b00ad85537eafcb22cb" (photo_id ASC),
	INDEX like_user_id_idx (user_id ASC),
	FAMILY "primary" (id, created, updated, user_id, photo_id)
);

CREATE SEQUENCE t_photo_view_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_photo_view (
	id INT8 NOT NULL DEFAULT nextval('"t_photo_view_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	user_id INT8 NULL,
	photo_id INT8 NULL,
	CONSTRAINT "PK_8c520c8e53e6bc1ca9726a48538" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_d0f9733130766da743093f931b8" (user_id ASC, photo_id ASC),
	INDEX t_photo_view_photo_idx (photo_id ASC),
	INDEX t_photo_view_user_idx (user_id ASC),
	INDEX "IDX_b5787b9bb5a8d9658d56b2703c" (user_id ASC),
	INDEX "IDX_59f521e779b0914c1f05ead614" (photo_id ASC),
	FAMILY "primary" (id, created, user_id, photo_id)
);

CREATE TABLE t_product_review_likes_t_account (
	"tProductReviewId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_eb8382714862010f3df6e9501dd" PRIMARY KEY ("tProductReviewId" ASC, "tAccountId" ASC),
	INDEX "IDX_e483fc2911e375d657fcb805df" ("tProductReviewId" ASC),
	INDEX "IDX_e3718fd33901024aa726623008" ("tAccountId" ASC),
	FAMILY "primary" ("tProductReviewId", "tAccountId")
);

CREATE TABLE t_product_review_replies_likes_t_account (
	"tProductReviewRepliesId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_8af6ada439891da86c3e4456441" PRIMARY KEY ("tProductReviewRepliesId" ASC, "tAccountId" ASC),
	INDEX "IDX_9b85063c135fffa25fb7d4e142" ("tProductReviewRepliesId" ASC),
	INDEX "IDX_31d8840aa79ec08134746b4f2c" ("tAccountId" ASC),
	FAMILY "primary" ("tProductReviewRepliesId", "tAccountId")
);

CREATE TABLE t_product_variant_t_product_photos (
	"variantId" INT8 NOT NULL,
	"photoId" INT8 NOT NULL,
	"order" INT8 NOT NULL,
	"productId" INT8 NOT NULL,
	CONSTRAINT "PK_a308adf053a105c11bffefd0fb1" PRIMARY KEY ("variantId" ASC, "photoId" ASC, "order" ASC, "productId" ASC),
	UNIQUE INDEX "UQ_105e6e859fb4e6e714e11f0ede6" ("variantId" ASC, "photoId" ASC, "order" ASC),
	INDEX "IDX_0cdb47895dc4a030af5a5c0c32" ("variantId" ASC),
	INDEX "IDX_e441673ce8154b92cc5c747230" ("photoId" ASC),
	FAMILY "primary" ("variantId", "photoId", "order", "productId")
);

CREATE SEQUENCE t_profile_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_profile_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_profile_photo_id_seq"':::STRING),
	"photoUrl" VARCHAR NULL,
	"thumbUrl" VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	CONSTRAINT "PK_30b95855439c52fa5868d251da5" PRIMARY KEY (id ASC),
	INDEX profile_photo_user_id_idx (user_id ASC),
	INDEX "IDX_f31147f584104cc782c9bd0bfc" (user_id ASC),
	FAMILY "primary" (id, "photoUrl", "thumbUrl", created, deleted, user_id)
);

CREATE SEQUENCE t_report_category_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_report_category (
	id INT8 NOT NULL DEFAULT nextval('"t_report_category_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_fbbebbc2b02e8656bc5e185fa22" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_dd59c75adb0751f37fde51ad904" (name ASC),
	FAMILY "primary" (id, name)
);

CREATE SEQUENCE t_report_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_report (
	id INT8 NOT NULL DEFAULT nextval('"t_report_id_seq"':::STRING),
	type VARCHAR NULL,
	description VARCHAR NULL,
	status VARCHAR NULL,
	created TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	target_id INT8 NULL,
	user_id INT8 NULL,
	category_id INT8 NULL,
	CONSTRAINT "PK_410e9dc9ef5559a98a0845931c3" PRIMARY KEY (id ASC),
	INDEX report_product_category_id_idx (category_id ASC),
	INDEX report_product_type_id_idx (type ASC),
	INDEX report_product_target_id_idx (target_id ASC),
	INDEX report_product_user_id_idx (user_id ASC),
	INDEX "IDX_5fefee3d1fd2ed0286b776a359" (target_id ASC),
	INDEX "IDX_99ea86edf3e764fe773f65430d" (user_id ASC),
	INDEX "IDX_fbbebbc2b02e8656bc5e185fa2" (category_id ASC),
	FAMILY "primary" (id, type, description, status, created, updated, target_id, user_id, category_id)
);

CREATE SEQUENCE t_report_type_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_report_type (
	id INT8 NOT NULL DEFAULT nextval('"t_report_type_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_ebfcb541ab082c22939d55e9249" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_e31a33046d3694cf37cd889b5ff" (name ASC),
	FAMILY "primary" (id, name)
);

CREATE SEQUENCE t_return_order_reason_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_return_order_reason (
	id INT8 NOT NULL DEFAULT nextval('"t_return_order_reason_id_seq"':::STRING),
	name VARCHAR NOT NULL,
	CONSTRAINT "PK_d6777d0d3d3c3f1c615dd49c947" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_f2f255c72079e8154c0a3aed96b" (name ASC),
	FAMILY "primary" (id, name)
);

CREATE SEQUENCE t_review_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_review_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_review_photo_id_seq"':::STRING),
	link VARCHAR NULL,
	thumb VARCHAR NULL,
	user_id INT8 NULL,
	"productReviewId" INT8 NULL,
	"serviceReviewId" INT8 NULL,
	"sellerReviewId" INT8 NULL,
	CONSTRAINT "PK_37c2fd1f8d0070daa66704f2ed7" PRIMARY KEY (id ASC),
	INDEX "IDX_bc5471818f7e25a3499ca1ab92" (user_id ASC),
	INDEX "IDX_a7d200f67e184e667fbe4c79a2" ("productReviewId" ASC),
	INDEX "IDX_ee5feed2d10bd2021fb7c84a91" ("serviceReviewId" ASC),
	INDEX "IDX_85ba5800d85c6b219ad76e2358" ("sellerReviewId" ASC),
	FAMILY "primary" (id, link, thumb, user_id, "productReviewId", "serviceReviewId", "sellerReviewId")
);

CREATE SEQUENCE t_service_schedule_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_service_schedule (
	id INT8 NOT NULL DEFAULT nextval('"t_service_schedule_id_seq"':::STRING),
	duration STRING NULL,
	break STRING NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	deleted TIMESTAMP NULL,
	service_id INT8 NULL,
	CONSTRAINT "PK_42020af7a7f6887ef83c8f7da3d" PRIMARY KEY (id ASC),
	UNIQUE INDEX "REL_987e1bbf5a4ec3bfa95c1376fe" (service_id ASC),
	INDEX service_schedule_service_id_idx (service_id ASC),
	INDEX "IDX_987e1bbf5a4ec3bfa95c1376fe" (service_id ASC),
	FAMILY "primary" (id, duration, break, created, updated, deleted, service_id)
);

CREATE SEQUENCE t_schedule_date_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_schedule_date (
	id INT8 NOT NULL DEFAULT nextval('"t_schedule_date_id_seq"':::STRING),
	"from" TIMESTAMP NULL,
	"to" TIMESTAMP NULL,
	bookings INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	deleted TIMESTAMP NULL,
	"scheduleId" INT8 NULL,
	CONSTRAINT "PK_3e296c72d929b46495e17053ba0" PRIMARY KEY (id ASC),
	INDEX "IDX_8b18d5e97764496a0193b19eda" ("scheduleId" ASC),
	FAMILY "primary" (id, "from", "to", bookings, created, updated, deleted, "scheduleId")
);

CREATE SEQUENCE t_schedule_days_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_schedule_days (
	id INT8 NOT NULL DEFAULT nextval('"t_schedule_days_id_seq"':::STRING),
	"from" TIME NULL,
	"to" TIME NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	deleted TIMESTAMP NULL,
	"scheduleId" INT8 NULL,
	"dayId" INT8 NULL,
	CONSTRAINT "PK_09bbadd2a132a7a80c1e98e6768" PRIMARY KEY (id ASC),
	INDEX "IDX_c3995dfe480bcd8d159fde801a" ("scheduleId" ASC),
	INDEX "IDX_c8e39ddee70608f4cd7bb5312a" ("dayId" ASC),
	FAMILY "primary" (id, "from", "to", created, updated, deleted, "scheduleId", "dayId")
);

CREATE SEQUENCE t_schedule_days_intervals_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_schedule_days_intervals (
	id INT8 NOT NULL DEFAULT nextval('"t_schedule_days_intervals_id_seq"':::STRING),
	"from" TIME NULL,
	"to" TIME NULL,
	"scheduleByDayId" INT8 NULL,
	CONSTRAINT "PK_0457344df032acadeb2000b7857" PRIMARY KEY (id ASC),
	INDEX "IDX_abbfb135fad847d8f94172574b" ("scheduleByDayId" ASC),
	FAMILY "primary" (id, "from", "to", "scheduleByDayId")
);

CREATE SEQUENCE t_search_dictionary_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_search_dictionary (
	id INT8 NOT NULL DEFAULT nextval('"t_search_dictionary_id_seq"':::STRING),
	word VARCHAR NOT NULL,
	type VARCHAR NOT NULL,
	CONSTRAINT "PK_7630f7b7f4724e40ee58a28f022" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_ef9b6684a5ada9548d5b52457d3" (word ASC, type ASC),
	FAMILY "primary" (id, word, type)
);

CREATE TABLE t_seller_review_likes_t_account (
	"tSellerReviewId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_c5410cc287236a78645e162868f" PRIMARY KEY ("tSellerReviewId" ASC, "tAccountId" ASC),
	INDEX "IDX_2a8c22c70e20681081f322409d" ("tSellerReviewId" ASC),
	INDEX "IDX_9545639072952b438f83e5322a" ("tAccountId" ASC),
	FAMILY "primary" ("tSellerReviewId", "tAccountId")
);

CREATE TABLE t_seller_review_replies_likes_t_account (
	"tSellerReviewRepliesId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_31ce440447953bcffbbe2e64220" PRIMARY KEY ("tSellerReviewRepliesId" ASC, "tAccountId" ASC),
	INDEX "IDX_b87c96930b5caa20b38e9f96d3" ("tSellerReviewRepliesId" ASC),
	INDEX "IDX_18922b4630ffc505a92aa2d836" ("tAccountId" ASC),
	FAMILY "primary" ("tSellerReviewRepliesId", "tAccountId")
);

CREATE SEQUENCE t_service_photo_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_service_photo (
	id INT8 NOT NULL DEFAULT nextval('"t_service_photo_id_seq"':::STRING),
	link VARCHAR NULL,
	thumb VARCHAR NULL,
	"index" INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	deleted TIMESTAMP NULL,
	service_id INT8 NULL,
	CONSTRAINT "PK_94981420fe5a3b99c8561bcc988" PRIMARY KEY (id ASC),
	INDEX "IDX_2cbaf08ad70bbf4e51d9c3c9c6" (service_id ASC),
	FAMILY "primary" (id, link, thumb, "index", created, updated, deleted, service_id)
);

CREATE TABLE t_service_review_likes_t_account (
	"tServiceReviewId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_d56e4abae2707be153b3833171b" PRIMARY KEY ("tServiceReviewId" ASC, "tAccountId" ASC),
	INDEX "IDX_8762e3a598c7a313730ccf12b7" ("tServiceReviewId" ASC),
	INDEX "IDX_7305d28bfdc30ffe186cad5219" ("tAccountId" ASC),
	FAMILY "primary" ("tServiceReviewId", "tAccountId")
);

CREATE TABLE t_service_review_replies_likes_t_account (
	"tServiceReviewRepliesId" INT8 NOT NULL,
	"tAccountId" INT8 NOT NULL,
	CONSTRAINT "PK_519c3147ce8d863d6e131628fbe" PRIMARY KEY ("tServiceReviewRepliesId" ASC, "tAccountId" ASC),
	INDEX "IDX_ddb945cd579b14e0f6f0bd2d1f" ("tServiceReviewRepliesId" ASC),
	INDEX "IDX_06c01ee3f71eb8d31531de7ddf" ("tAccountId" ASC),
	FAMILY "primary" ("tServiceReviewRepliesId", "tAccountId")
);

CREATE SEQUENCE t_share_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_share (
	id INT8 NOT NULL DEFAULT nextval('"t_share_id_seq"':::STRING),
	type VARCHAR NULL,
	total INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	text_id INT8 NULL,
	photo_id INT8 NULL,
	video_id INT8 NULL,
	article_id INT8 NULL,
	product_id INT8 NULL,
	CONSTRAINT "PK_fc70a5a9dd26e469d3718e0bcac" PRIMARY KEY (id ASC),
	INDEX share_user_id_idx (user_id ASC),
	INDEX share_text_id_idx (text_id ASC),
	INDEX share_photo_id_idx (photo_id ASC),
	INDEX share_video_id_idx (video_id ASC),
	INDEX share_article_id_idx (article_id ASC),
	INDEX share_product_id_idx (product_id ASC),
	INDEX "IDX_69488771f8df06c0e283670f51" (user_id ASC),
	INDEX "IDX_8edb04c10c29276769300d14c3" (text_id ASC),
	INDEX "IDX_29763ee09ee3a2882b67a56aa6" (photo_id ASC),
	INDEX "IDX_675074bc1aa506b4b724dbaa86" (video_id ASC),
	INDEX "IDX_feb2fafe832322fdcb0993f57a" (article_id ASC),
	INDEX "IDX_c4105ba27ad4774428f5c1a661" (product_id ASC),
	FAMILY "primary" (id, type, total, created, updated, deleted, user_id, text_id, photo_id, video_id, article_id, product_id)
);

CREATE SEQUENCE t_text_comment_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text_comment_like (
	id INT8 NOT NULL DEFAULT nextval('"t_text_comment_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	comment_id INT8 NULL,
	reply_id INT8 NULL,
	text_id INT8 NULL,
	CONSTRAINT "PK_eb0beb0973e8f8cf40f5dfd497e" PRIMARY KEY (id ASC),
	INDEX "IDX_94d35660ae5baf62735f570763" (user_id ASC),
	INDEX "IDX_ebc0603ac08e08b1b3a9cdb523" (comment_id ASC),
	INDEX "IDX_c1afe38f8c8601820d39442b7a" (reply_id ASC),
	INDEX "IDX_34962ce23213f435fc2b84e9be" (text_id ASC),
	FAMILY "primary" (id, created, updated, user_id, comment_id, reply_id, text_id)
);

CREATE SEQUENCE t_text_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text_like (
	id INT8 NOT NULL DEFAULT nextval('"t_text_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	text_id INT8 NULL,
	CONSTRAINT "PK_1a5d1f2c9b7573af8ac567a5794" PRIMARY KEY (id ASC),
	INDEX like_text_id_idx (text_id ASC),
	INDEX "IDX_5ed398d9bc25f8ca8a18ccdb63" (user_id ASC),
	INDEX "IDX_a17c3da6530f9ea4f6c72b69ce" (text_id ASC),
	INDEX like_user_id_idx (user_id ASC),
	FAMILY "primary" (id, created, updated, user_id, text_id)
);

CREATE SEQUENCE t_text_view_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_text_view (
	id INT8 NOT NULL DEFAULT nextval('"t_text_view_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	user_id INT8 NULL,
	text_id INT8 NULL,
	CONSTRAINT "PK_bc283e6d22a015cd4c443892063" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_60c09a45c0bafd08429cb06ad0e" (user_id ASC, text_id ASC),
	INDEX t_text_view_text_idx (text_id ASC),
	INDEX t_text_view_user_idx (user_id ASC),
	INDEX "IDX_9bf0dc207b37307379f78d832b" (user_id ASC),
	INDEX "IDX_ea613a2921d8f138e5541bd5aa" (text_id ASC),
	FAMILY "primary" (id, created, user_id, text_id)
);

CREATE SEQUENCE t_user_business_user_category_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_user_business_user_category (
	id INT8 NOT NULL DEFAULT nextval('"t_user_business_user_category_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	category_id INT8 NULL,
	CONSTRAINT "PK_2e498103e4d9da519cc9ae6f841" PRIMARY KEY (id ASC),
	INDEX "IDX_39ce3243077646ec31709c4f92" (category_id ASC),
	FAMILY "primary" (id, created, category_id)
);

CREATE SEQUENCE t_video_comment_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video_comment_like (
	id INT8 NOT NULL DEFAULT nextval('"t_video_comment_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	comment_id INT8 NULL,
	reply_id INT8 NULL,
	video_id INT8 NULL,
	CONSTRAINT "PK_f4c0fcbbff11bb867777f594e39" PRIMARY KEY (id ASC),
	INDEX "IDX_315595f46bb9cf8654ffdbdea6" (user_id ASC),
	INDEX "IDX_85a247580f74fe5309dee2296c" (comment_id ASC),
	INDEX "IDX_ba0132a450bfcb9adf616b5041" (reply_id ASC),
	INDEX "IDX_1a5cdbf945847dc72aa5fe49d3" (video_id ASC),
	FAMILY "primary" (id, created, updated, user_id, comment_id, reply_id, video_id)
);

CREATE SEQUENCE t_video_like_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video_like (
	id INT8 NOT NULL DEFAULT nextval('"t_video_like_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL,
	user_id INT8 NULL,
	video_id INT8 NULL,
	CONSTRAINT "PK_3a8f07cdbb71ed001b46b508476" PRIMARY KEY (id ASC),
	INDEX like_video_id_idx (video_id ASC),
	INDEX "IDX_ee71f80444a0c0666f54538c3d" (user_id ASC),
	INDEX "IDX_a1f778acc37f7d325a0a126b21" (video_id ASC),
	INDEX like_user_id_idx (user_id ASC),
	FAMILY "primary" (id, created, updated, user_id, video_id)
);

CREATE SEQUENCE t_video_view_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_video_view (
	id INT8 NOT NULL DEFAULT nextval('"t_video_view_id_seq"':::STRING),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	user_id INT8 NULL,
	video_id INT8 NULL,
	CONSTRAINT "PK_895e2eef28f2a2d504a7dcf01d4" PRIMARY KEY (id ASC),
	UNIQUE INDEX "UQ_24e1dfb5cb8403f88fe2bf22b59" (user_id ASC, video_id ASC),
	INDEX t_video_view_video_idx (video_id ASC),
	INDEX t_video_view_user_idx (user_id ASC),
	INDEX "IDX_08c390d8dd9b5f613c3f27b5c8" (user_id ASC),
	INDEX "IDX_cc4af00ad15106ab9e4066a862" (video_id ASC),
	FAMILY "primary" (id, created, user_id, video_id)
);

CREATE SEQUENCE t_view_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE t_view (
	id INT8 NOT NULL DEFAULT nextval('"t_view_id_seq"':::STRING),
	view INT8 NULL,
	total INT8 NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp():::TIMESTAMP,
	updated TIMESTAMP NULL DEFAULT NULL,
	deleted TIMESTAMP NULL DEFAULT NULL,
	user_id INT8 NULL,
	text_id INT8 NULL,
	photo_id INT8 NULL,
	video_id INT8 NULL,
	article_id INT8 NULL,
	product_id INT8 NULL,
	CONSTRAINT "PK_5785eb4cabfdea773d858c5ff21" PRIMARY KEY (id ASC),
	INDEX view_product_id_idx (product_id ASC),
	INDEX view_article_id_idx (article_id ASC),
	INDEX view_video_id_idx (video_id ASC),
	INDEX view_photo_id_idx (photo_id ASC),
	INDEX view_text_id_idx (text_id ASC),
	INDEX view_user_id_idx (user_id ASC),
	INDEX "IDX_b1645fb71714698a25768cd334" (user_id ASC),
	INDEX "IDX_389d3a1f4893d3f42f23eb914e" (text_id ASC),
	INDEX "IDX_91ab9ac6cad20476cc7538a917" (photo_id ASC),
	INDEX "IDX_05f3f42178d1325102bef0644f" (video_id ASC),
	INDEX "IDX_c65cc890988c50c0d3329cabdb" (article_id ASC),
	INDEX "IDX_61ec15ef08575cc2c536c09336" (product_id ASC),
	FAMILY "primary" (id, view, total, created, updated, deleted, user_id, text_id, photo_id, video_id, article_id, product_id)
);

SELECT setval('migrations_id_seq', 8, false);

INSERT INTO migrations (id, "timestamp", name) VALUES
	(1, 1587480306324, 'addCheckoutAndOrders1587480306324'),
	(2, 1587997568330, 'addNewFieldsToPerson1587997568330'),
	(3, 1588085271029, 'changedBookingTable1588085271029'),
	(4, 1588595291979, 'changesInTablesRelatedToBuyProducts1588595291979'),
	(5, 1588668051066, 'changesInTables1588668051066'),
	(6, 1588946237790, 'addNewFieldsToOrdersAndPersonBusiness1588946237790'),
	(7, 1597324944664, 'addProductVariantToNotification1597324944664');

SELECT setval('t_category_id_seq', 41, false);

INSERT INTO t_category (id, name, type) VALUES
	(1, 'Agriculture', 'Producer/Merchant'),
	(2, 'Electronics', 'Producer/Merchant'),
	(3, 'Erotic', 'Producer/Merchant'),
	(4, 'Fashion/Accessory', 'Producer/Merchant'),
	(5, 'Food/Beverage', 'Producer/Merchant'),
	(6, 'FMCG', 'Producer/Merchant'),
	(7, 'Furniture/Home', 'Producer/Merchant'),
	(8, 'Garden Supplies', 'Producer/Merchant'),
	(9, 'Materials', 'Producer/Merchant'),
	(10, 'Media', 'Producer/Merchant'),
	(11, 'Pet Supplies', 'Producer/Merchant'),
	(12, 'Pharmaceutical', 'Producer/Merchant'),
	(13, 'Real Estate', 'Producer/Merchant'),
	(14, 'Toys', 'Producer/Merchant'),
	(15, 'Vehicles', 'Producer/Merchant'),
	(16, 'Other', 'Producer/Merchant'),
	(17, 'Accommodation', 'Service Provider'),
	(18, 'Banking', 'Service Provider'),
	(19, 'Craft', 'Service Provider'),
	(20, 'Education', 'Service Provider'),
	(21, 'Energy', 'Service Provider'),
	(22, 'Engineering', 'Service Provider'),
	(23, 'Event', 'Service Provider'),
	(24, 'Gastronomy', 'Service Provider'),
	(25, 'Healthcare', 'Service Provider'),
	(26, 'Insurance', 'Service Provider'),
	(27, 'IT', 'Service Provider'),
	(28, 'Law', 'Service Provider'),
	(29, 'Leisure', 'Service Provider'),
	(30, 'Media', 'Service Provider'),
	(31, 'Transportation (Goods)', 'Service Provider'),
	(32, 'Transportation (Passengers)', 'Service Provider'),
	(33, 'Travel', 'Service Provider'),
	(34, 'Other', 'Service Provider'),
	(35, 'Aid Agency', 'Organization'),
	(36, 'Association', 'Organization'),
	(37, 'Governmental Institution', 'Organization'),
	(38, 'Political Party', 'Organization'),
	(39, 'University/School', 'Organization'),
	(40, 'Other', 'Organization');

SELECT setval('t_business_id_seq', 7, false);

INSERT INTO t_business (id, photo, thumb, name, "pageName", phone, description, legal_notice, privacy, mango_user_id, wallet, bank_alias_id, kyc_document_id, verification_status, created, updated, deleted, reactivationtoken, "categoryId", "deliveryAddressId", "addressId") VALUES
	(1, NULL, NULL, 'Grit', 'grit', '02218765432', '', NULL, NULL, 86323673, 86323674, 86323675, NULL, NULL, '2020-08-23 10:00:52.171332+00:00', '2020-09-20 11:02:21.5894+00:00', NULL, NULL, 27, NULL, 3),
	(2, 'https://minio.staging.grit.sc/photo/GVsHGE0-W-photo.jpg', 'https://minio.staging.grit.sc/photo/GVsHGE0-W-thumb.jpg', 'Grit', 'grit1', '123', '', NULL, NULL, 86323847, 86323848, 86323849, NULL, NULL, '2020-08-23 10:12:21.12502+00:00', '2020-09-20 16:11:37.321382+00:00', NULL, NULL, 27, 19, 4),
	(3, NULL, NULL, 'Abcde', 'abcde', '0', NULL, NULL, NULL, 86346705, 86346706, 86346707, NULL, NULL, '2020-08-24 07:57:08.217153+00:00', '2020-08-24 08:00:27.37524+00:00', '2020-08-24 08:00:27+00:00', '5eu4v21cke88dfca', 12, NULL, 5),
	(4, NULL, NULL, 'mybusiness', 'mybusiness', '123123123', '', '1323123', NULL, 86652055, 86652056, 86652057, NULL, NULL, '2020-08-27 15:17:04.007401+00:00', '2020-09-17 12:41:59.800473+00:00', NULL, NULL, 1, 14, 12),
	(5, 'https://minio.staging.grit.sc/photo/U4lnHAsPk-photo.jpg', 'https://minio.staging.grit.sc/photo/U4lnHAsPk-thumb.jpg', 'Shop', 'shop', '123456', '', NULL, NULL, 88019722, 88019723, 88019724, NULL, NULL, '2020-09-17 09:54:23.556944+00:00', '2020-09-21 11:10:19.731548+00:00', NULL, NULL, 2, 16, 15),
	(6, NULL, NULL, 'ilyabusiness2', 'ilyabusiness2', '123123123', NULL, NULL, NULL, 88237279, 88237280, 88237281, NULL, NULL, '2020-09-21 15:04:05.253083+00:00', '2020-09-21 15:04:05.507673+00:00', NULL, NULL, 1, NULL, 20);

SELECT setval('t_address_id_seq', 21, false);

INSERT INTO t_address (id, type, "addrType", "fullName", "careOf", street, appendix, zip, city, state, country, selected, created, updated, deleted, lat, lng, "personId", "businessId") VALUES
	(1, '', NULL, 'Denys', NULL, 'Freedom Avenue', '', 79235, 'Lviv', NULL, NULL, false, '2020-08-20 10:56:49.276222+00:00', NULL, NULL, 36.03054, (-96.0807), 3, NULL),
	(2, '', NULL, 'Natalie Clever', NULL, e'Nikolausstra\u00DFe 132', '', 50937, e'K\u00F6ln ', NULL, NULL, false, '2020-08-23 09:33:11.706665+00:00', NULL, NULL, 50.91992, 6.92346, 6, NULL),
	(3, 'businessAddress', NULL, NULL, NULL, 'Test 10', NULL, 50829, e'K\u00F6ln', NULL, NULL, false, '2020-08-23 10:00:52.301618+00:00', NULL, NULL, NULL, NULL, NULL, 1),
	(4, 'businessAddress', NULL, NULL, NULL, 'Teststret 10', NULL, 12345, e'K\u00F6ln', NULL, NULL, false, '2020-08-23 10:12:21.253457+00:00', NULL, NULL, NULL, NULL, NULL, 2),
	(5, 'businessAddress', NULL, NULL, NULL, 'Abc', NULL, 50024, 'Xxxx', NULL, NULL, false, '2020-08-24 07:57:08.305212+00:00', NULL, NULL, NULL, NULL, NULL, 3),
	(6, '', NULL, 'Thomas Lohmann', NULL, 'Kuckucksweg 21', '', 50829, e'K\u00F6ln', NULL, NULL, false, '2020-08-24 07:58:20.236166+00:00', NULL, NULL, 50.95352, 6.88384, 4, NULL),
	(7, '', NULL, 'Grit', NULL, e'Nikolausstra\u00DFe', '', 50937, e'K\u00F6ln', NULL, NULL, false, '2020-08-25 06:34:24.763634+00:00', NULL, NULL, 50.92157, 6.92642, NULL, 1),
	(8, '', NULL, 'Grit', NULL, e'Nikolausstra\u00DFe', '', 50937, e'K\u00F6ln', NULL, NULL, false, '2020-08-25 06:34:25.674521+00:00', NULL, NULL, 50.92157, 6.92642, NULL, 1),
	(9, '', NULL, 'Grit', NULL, e'L\u00FCttringhauserstra\u00DFe', '', 51103, e'K\u00F6ln', NULL, NULL, false, '2020-08-25 06:38:18.185071+00:00', NULL, NULL, 50.94583, 7.00713, NULL, 1),
	(10, '', NULL, 'Grit', NULL, e'L\u00FCttringhauserstra\u00DFe', '', 51103, e'K\u00F6ln', NULL, NULL, false, '2020-08-25 06:38:18.788665+00:00', NULL, NULL, 50.94583, 7.00713, NULL, 1),
	(11, '', NULL, 'Grit', NULL, e'L\u00FCttringhauserstra\u00DFe', '', 51103, e'K\u00F6ln', NULL, NULL, false, '2020-08-25 06:38:19.307592+00:00', NULL, NULL, 50.94583, 7.00713, NULL, 1),
	(12, 'businessAddress', NULL, NULL, NULL, '123123213', NULL, 12123, '123123', NULL, NULL, false, '2020-08-27 15:17:04.21775+00:00', NULL, NULL, NULL, NULL, NULL, 4),
	(13, '', NULL, 'ilya2', NULL, '111111', '11111', 1111, '11111', NULL, NULL, false, '2020-08-28 14:33:52.581032+00:00', NULL, NULL, 32.35318, (-94.98124), 9, NULL),
	(14, '', NULL, 'mybusiness', NULL, '2222', '22222', 2222, '222222', NULL, NULL, false, '2020-08-28 14:34:31.360894+00:00', NULL, NULL, 48.4528, 16.59974, NULL, 4),
	(15, 'businessAddress', NULL, NULL, NULL, 'Teststret 10', NULL, 12345, e'K\u00F6ln', NULL, NULL, false, '2020-09-17 09:54:23.802954+00:00', NULL, NULL, NULL, NULL, NULL, 5),
	(16, '', NULL, 'Grit', NULL, 'Teststret 10', '', 12345, e'K\u00F6ln', NULL, NULL, false, '2020-09-18 10:07:43.932589+00:00', NULL, NULL, 50.6915, 6.64894, NULL, 5),
	(17, '', NULL, 'Shop', NULL, '111', '1111', 11, '11', NULL, NULL, false, '2020-09-20 15:43:08.097071+00:00', NULL, NULL, 40.80113, 29.43864, NULL, 5),
	(18, '', NULL, 'Thomas Lohmann', NULL, 'Test', 'Address', 123, 'Test', NULL, NULL, false, '2020-09-20 15:55:01.394565+00:00', NULL, NULL, 41.26035, (-95.84325), 4, NULL),
	(19, '', NULL, 'Grit', NULL, 'asdf', 'sdffd', 123123, 'sdff', NULL, NULL, false, '2020-09-20 16:11:31.130765+00:00', NULL, NULL, 35.72801, (-97.37259), NULL, 2),
	(20, 'businessAddress', NULL, NULL, NULL, '123123', NULL, 123123, '123123', NULL, NULL, false, '2020-09-21 15:04:05.420112+00:00', NULL, NULL, NULL, NULL, NULL, 6);

SELECT setval('t_person_id_seq', 12, false);

INSERT INTO t_person (id, photo, thumb, "pageName", "fullName", email, password, mango_user_id, wallet, bank_alias_id, kyc_document_id, verification_status, gender, birthday, residence, description, language, privacy, active, token, passtoken, "sellerRating", "sellerReviews", "tokenAttempts", "tokenAttemptsStamp", created, updated, deactivated, reactivationtoken, deleted, address_id, "deliveryAddressId") VALUES
	(1, NULL, NULL, 'sergeyFegon', 'Sergey Fegon', 'sergey.fegon@websailors.pro', '$argon2i$v=19$m=4096,t=3,p=1$hJkfMK12yntknygbb0EnvA$0M+gfyrSHxOm0+u/ZvzVn8sefE7HkQBP6CFngl7tKcA', 86123094, 86123095, 86123096, NULL, NULL, 'male', '1991-01-01T00:00:00.000+03:00', NULL, NULL, 'english', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NDQ0Mjg5NDg1LCJpZGVudGlmaWVyIjoic2VyZ2V5LmZlZ29uQHdlYnNhaWxvcnMucHJvIn0.usT1gnBl4gBUSflHr8F4CSFnDb3rf2VSWdoK3VTEjtdHcChDXCO3z7OyTmyVIK6ZizA7sY1xRpudLmWkztqiXEYtOjlQCUqUU6lKxQ8rj62-X7dE1gpne48nDD329ZUr49NXfNpgo2b1aiJBXhtVhbGxMFYxM5b9DAnchamCgo_0ad6zJmGMAJuXlhu6Jiw0L1Z5nEs6I0c03LxQ9muvTAyrhEqCa3OyBxZsHsxQGgEr6hRjRbnmxCc129gf6sRVUiLzWJGAMduX5emwIbHf1Qm_SYc4J8gSzCB0KYLmtc0H2Y-VzBK5KuZtalhamkWW8HYWd9Nys3i_BjWrANajYA', NULL, NULL, NULL, NULL, '2020-08-19 13:38:10.148239+00:00', '2020-08-19 13:38:10.148+00:00', '2020-08-19 13:38:39+00:00', NULL, NULL, NULL, NULL, NULL),
	(2, NULL, NULL, 'webSailor', 'Web Sailor', 'ws.fegon@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$sYvgviC+TkU/b9y2y3Lu5Q$f6j+nH3+9P34RW5gVjNCKljTWB2svFWFUlOur/RHYGk', 86123367, 86123368, 86123369, NULL, NULL, 'male', '1991-01-01T00:00:00.000+03:00', NULL, NULL, 'english', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NDQ0NDI5MTE5LCJpZGVudGlmaWVyIjoid3MuZmVnb25AZ21haWwuY29tIn0.IsznwyBUqBAe10DJrJBMX9I_lIhO2MgfTb5rENXE0Zq9RXVV-oHk46NI0QvFUJRe6PIfzA6XFk2qdstv4g1ArNKvcIqPIiJ99NP97eIrf0EyS-ghL9G87hP40F87Jr0OjBrgDd_thMHB5oYZUcK9zByGhPVArSbhvJR8HOkADfC3YMcjuUlfcoAa9wj8Qgndz5Juudjp32F4WBTrMOV0WSvTFTcq0PIOTaSdsoE05-AV4ZroSSLw9m_md1thAcPkcFbRfNqgGCvyI_bHO3i7q--gt8m_NgQrCAn7rWGrQuM1dNayjeFy6YehDIly1srfqWoYM3jWARLi2tEO_DIR3g', NULL, NULL, NULL, NULL, '2020-08-19 13:40:29.722908+00:00', '2020-08-19 13:40:29.722+00:00', '2020-08-19 13:40:52+00:00', NULL, NULL, NULL, NULL, NULL),
	(3, 'https://minio.staging.grit.sc/photo/QJ9jbttBW-photo.jpg', 'https://minio.staging.grit.sc/photo/QJ9jbttBW-thumb.jpg', 'denys', 'Denys', 'denys.x95@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$itj3qdp92XgI5qKUvZQvuw$Bzw/KI6ZGDv/LQxLFtTKNVLx30VAMyu8CeNdqhpXZiI', 86161641, 86161642, 86161644, NULL, NULL, 'male', '1995-05-28T00:00:00.000+03:00', NULL, NULL, 'english', 'private', true, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NTEyMDM1OTM3LCJpZGVudGlmaWVyIjoiZGVueXMueDk1QGdtYWlsLmNvbSJ9.GKXDtJ-wMcPrw132bZ--IzznnEmgGM-W_YJaQh6yN4mi7iJMuXCd7xgfpCJgEMq6i24x7RUZfm7MtpGHfuw8R7XrRb7ddsE0hAP84lQFEZMHLt7IhvCnV-FG80pv3-9Z8cWTldNKMuHTe5VSMmTDBtTYHKpnjXK-5PevepjHQeyhG0r2raHtW2U69bMY5u7FLHUV-yqWXaDHKMq-jYCC8qCvy5cCRx746O5L6W_zdnUNILcviPb0aOlHwvf4CN2anoe-kzJvDeN5neQPR3hYX3IaucDE-8W2p3PwiB71JbL71BrkSrf_rDWP3UC7EVfRA5RQxxetN6yNn4OUU9k3uw', NULL, NULL, NULL, NULL, '2020-08-20 08:27:16.69222+00:00', '2020-08-20 08:27:16.692+00:00', '2020-08-20 11:06:33+00:00', NULL, NULL, NULL, NULL, 1),
	(4, 'https://minio.staging.grit.sc/photo/ZDR9B4avm-photo.jpg', 'https://minio.staging.grit.sc/photo/jl8GFI0FF-thumb.jpg', 'thomasLohmann', 'Thomas Lohmann', 'thomas.lohmann@mail.de', '$argon2i$v=19$m=4096,t=3,p=1$Fj9D6ElTB71sxE9uWRjdiA$G6XeDISyGOCZWwMfdZJxkP3SUfjNUUAJNJGdNXBtIOA', 86168351, 86168353, 86168357, NULL, NULL, 'male', '1991-10-21T00:00:00.000+01:00', NULL, NULL, 'english', 'public', true, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NTE1OTYxMzI1LCJpZGVudGlmaWVyIjoidGhvbWFzLmxvaG1hbm5AbWFpbC5kZSJ9.BxwadkiWUkk9SiHVYzNdwwarfLVVaAkUtC7K-Bv9VOfM39evMi39XiZhNc3Tjil7nIwuDDHl45z0ZTcxS-dnXlRw5jjykN83RSUxtAsFASUeAeiufiyHaCgFmTZuQl6oc5cSbC0h9OTcw7yuvQpXx_T5IaMASE6eWimV9NnaTwimwjOOc2IJGB_7HV5HF4UiWTnXR5lbErI7PRAJmliKS546g-_UojstPiL-p2f6hcVujUQBBLKTO9bkgqB0u9eUiQBI6ArW4q-T50c1LwoVS2iInzt87ew6zDZr0A-Q8wow7RkNKzEER2tq0lnOCwVV4kGlXuwFtLeMHbRD_Gl0VA', NULL, NULL, NULL, NULL, '2020-08-20 09:32:42.235256+00:00', '2020-08-20 09:32:42.235+00:00', '2020-09-21 16:06:03+00:00', NULL, NULL, NULL, NULL, 18),
	(5, NULL, NULL, 'alex', 'Alex', 'alexander.karamushko@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$rEW85seUC8BXPv3fVqV/9A$cJ3EWnoeKAhZp0r0CCUuXTZ+wusSglNfQgcnvxXxW5Q', 86176382, 86176383, 86176384, NULL, NULL, 'male', '1999-05-02T00:00:00.000+03:00', NULL, NULL, 'english', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NTIxMTk4ODk3LCJpZGVudGlmaWVyIjoiYWxleGFuZGVyLmthcmFtdXNoa29AZ21haWwuY29tIn0.v021vvv0BrXfIIEKFdHUQuG_6OAsEHa0xko4ZHRc-PiGajbvLkVbIH6FaX_bytNs9rk3f1sA4vZVTf0o5kkuIuQc7rrYjS1-EbFItgoLzsa9jAWno6csjAEbIZlaRmz4g2B0bGlUmlukZ7p9qvgGLcI06MojKQXMmGQ5PDh624KD7qkGeEbnD9icT2nirx5Q7FL1Qzhdys2m-dz1deMJvxTw9xczxTg57rko9kFfq7mtms1oXltl97l2NpMezqpAk2MElxLkxbnuW9WtQzL6M8cIHiPfAAgicih7OFcY_5mra7jsik6UfdKPgs9FIB8SR_ZSxtpEXINoTuf1oAxcTg', NULL, NULL, NULL, NULL, '2020-08-20 10:59:59.687614+00:00', '2020-08-20 10:59:59.687+00:00', '2020-09-17 13:32:34+00:00', NULL, NULL, NULL, NULL, NULL),
	(6, 'https://minio.staging.grit.sc/photo/VPrWpLwcL-photo.jpg', 'https://minio.staging.grit.sc/photo/VPrWpLwcL-thumb.jpg', 'natti', 'Natalie Clever', 'natalie.clever@hotmail.de', '$argon2i$v=19$m=4096,t=3,p=1$hw3wGg/RBPtFrywiVjF5rg$FfQudfJeTdh1xzVtPVxKudKOO8TbH8rA9jijUBN0B5k', 86322396, 86322397, 86322398, NULL, NULL, 'female', '1994-03-20T00:00:00.000+01:00', e'K\u00F6ln', e'\U0001F538\U0000FE0FMake up Artist \n\U0001F538\U0000FE0FProfessional Beautician \n\U0001F538\U0000FE0FBeauty Consulting\n\U0001F4CDCologne', 'deutsch', 'public', true, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NzcyMDAwODc5LCJpZGVudGlmaWVyIjoibmF0YWxpZS5jbGV2ZXJAaG90bWFpbC5kZSJ9.oS8qbrwkXT8mPNnivx_SxgxNU3cbSAgi4Mg34niTinxHRDr5L9ldw_SQgD5I4ZxVcILjzZ82VCko5e-QZ85J84_CwzRN26YlmcmXtJ0CenmX_cZ-0JWBTqQPn1soBbsBl3P-Myz5e_P9f-2BNo2V7hrfC0Au9rKR46LwwMO5iOWHA1PA21QfDsKuJl8K5MXGt2L2PIrsiVVW-6tRmXrwt7mxKRiFSg3o-Z9mqRRwhXE7i6hiCvnmba8YKvvbTnnbwtRn8ndp9np6wkTXEwaFiAhPNrQDD4SvGS0VBAtxF0vZunHzN0nUFISQE7VNErCko0dcGK5hv2BIz3WhN1OiTw', NULL, NULL, NULL, 1, '2020-08-23 08:37:42.451+00:00', '2020-08-23 08:37:42.451+00:00', '2020-09-20 11:21:28+00:00', NULL, NULL, NULL, NULL, 2),
	(7, NULL, NULL, 'thomas1', 'Thomas ', 'thomas.lohmann10@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$2aTdclcF9oOyaiaY/f9Y9g$GpQ5jfDvzSx6ig1Rh08WCBX/TV1GPoVgcVLgIYAqIwo', 86322695, 86322696, 86322697, NULL, NULL, 'male', '1991-10-22T00:00:00.000+01:00', NULL, NULL, 'english', 'public', true, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk4NzczMDYzMzM3LCJpZGVudGlmaWVyIjoidGhvbWFzLmxvaG1hbm4xMEBnbWFpbC5jb20ifQ.vIBCldfN3VhvnEqooB1m7J14IhGfgq9ayKqLIaIorAcNulKbpyRgMBrai0StpeI4_h32ofk2jlCSQvdMYnEXaGjNwWJYgX5aMxNrnfHixz20erEappy1W8uO9STw6sm1kaETNBnATvzUJA11xIV4xM7dWlZdNtQSTuyZfnAduTGsTVIPMcfNrDQeJHw9uF4zYL55HWGbws9fy9acVznGd_LE8AqFyv-TAM0B6Nf6K7Dty3avr9x_FEJjOPitad6_8_jXkJ_PMFYBcRq0I8fuuOowI3jsfFtJc-hfhyuKYhLg1MnUB12lA1-pI1HhCUQBkc7-xewE3H32_9tWWfMYrg', NULL, NULL, NULL, NULL, '2020-08-23 08:57:43.935003+00:00', '2020-08-23 08:57:43.935+00:00', '2020-08-23 10:11:25+00:00', NULL, NULL, NULL, NULL, NULL),
	(8, NULL, NULL, 'ilya', 'Ilya', 'ws.kadirmaev@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$PtrGL9BwdkLInm/q/uA7kg$/dBpRStFoCAEPBvgruBmxzbQcOQSHYiEnGhy+x0HBUA', 86627043, 86627044, 86627045, NULL, NULL, 'male', '1993-01-01T00:00:00.000+03:00', NULL, NULL, 'english', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk5MTI0ODgyNzk1LCJpZGVudGlmaWVyIjoid3Mua2FkaXJtYWV2QGdtYWlsLmNvbSJ9.qrMbx_laGCPhR4CbcBhMDRq8WE68qlprkR2Ye36kGxqTp74D5VkPlM2QHRp7Qk5dQbidngvZsFajedmVz2jK8XoeKrqus2qKmBEGwvr2Zwyr-qtp-jNIMRurhgTHl64AEDnb_a6dCPs-NtmYvtKUcTSQOg1CrmWYsLWzae6Ta3oU7OJTs10rULzeFylbwzVKPrujGZXpacLi3cjHzlNxYg7A2Eyl2hqlnKkncjVU33N8MV2ubVWPCMfcIks8LnB6HW4Z6JvfG_VJBWvdP9e9gGfFeFIVnAysju4X-8Qc8JCxooPgu7xoQLCFrji_o6hIMbZp351Qbku5ftrXa4lGVQ', NULL, NULL, NULL, NULL, '2020-08-27 10:41:23.481007+00:00', '2020-08-27 10:41:23.481+00:00', '2020-09-08 09:12:30+00:00', NULL, NULL, NULL, NULL, NULL),
	(9, 'https://minio.staging.grit.sc/photo/o5lnnncR2-photo.jpg', 'https://minio.staging.grit.sc/photo/o5lnnncR2-thumb.jpg', 'ilya2', 'ilya2', 'ws.kadirmaev2@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$NDMYilxi5foiq1qg4UDeFg$dsWB79KuTugj983Xw4m4kVMvFrJAuIhNWO38KkraItk', 86627049, 86627050, 86627051, NULL, NULL, 'male', '1992-01-01T00:00:00.000+02:00', 'asdfasdf', '123123asdfasdfasdf', 'english', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk5MTI0OTI2Mjg0LCJpZGVudGlmaWVyIjoid3Mua2FkaXJtYWV2MkBnbWFpbC5jb20ifQ.nKnv99LbPf8gE6ygY2tcrrEwYQNpom0xzow8oFRw4YWg_KBFduLdG3PAZRtpNaXmdcSm5sxBGUnwC2uIHzPrv5gzYtfqGV8Bm47m8ZabKbQQ5ZlYHTOh4JdmoQhWoBibG57R3sEQ5UUe43XhIUlNKsjlT7PMHIyx0Y-tm08ER3CA5W49vBr6D2Jfa6N02i2aBfwTx1nvh6Mrzh_w46LVwE_nRRRkV8Yd2KWSL3uaJ51HwOJ8XQeEiuPIxbHkoOp4w0Ev1wjK_SGOYXIxoHhnm9oa2iFaVTGKY1Fk_hxB6wORwg23-B7l2IoLoJ2U4xKa-YZdLCaYlgmNJ74_TMC9kQ', 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoicGFzc3dvcmQiLCJleHBpcmVzIjoxNTk5ODE0MDg2Mjc2LCJpZGVudGlmaWVyIjo5fQ.E_cjtgb_d8PGruKB3DfbYOyBvwZWjgeM7PxC-dJVpC79KFxApcKti0odkLQR-skuyU752kU3sUZvjwsbakxzf1FPFn2nEfGA9jKIZgcNYEvHVX2viv0FeceD9Rzd9AwjoiZtPzdTgo4WN3IDhRMlOxQYcDE-0Br9g6Tq9-sCSJk54rgNjGf8iF00GY0Y5MatDWQn7G4-k7My22N48EklM5gu-Aw1O1z5-nLsYoIxjpS6nkyLWVEAXEjo0kDNlrGH2ffPwWh9BS_MTcFA9sgSRkU8SpPhfNQhwG_A2XgT8pAwDEgOtfniefKU_4KcbwTbuCzLfKMqz_ck4XBH7nJJ4w', NULL, NULL, NULL, '2020-08-27 10:42:06.916087+00:00', '2020-08-27 10:42:06.916+00:00', '2020-09-21 14:50:27+00:00', NULL, NULL, NULL, NULL, 13),
	(10, NULL, NULL, 'purchases', 'purchases', 'thomas.lohmann10@mail.de', '$argon2i$v=19$m=4096,t=3,p=1$vkjZZdMQN+d0Z9OUSpLm1w$3flLZnNWSC1LhreADYvEW2uUdmQ7s7DV8KGrtqwcv0w', 87253526, 87253527, 87253528, NULL, NULL, 'male', '1991-10-22T00:00:00.000+01:00', NULL, NULL, 'deutsch', 'public', false, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNTk5OTA0MDc0ODc3LCJpZGVudGlmaWVyIjoidGhvbWFzLmxvaG1hbm4xMEBtYWlsLmRlIn0.QK1IVjvJJneKlS0HOjU2hS2PyUnKo6oIjzaXb16LhlcId44lNpfvnZbzkreEwGwUk_aNL1tyOsVBQpcTEMC3gGR0DWrQswpjSJSWvaAtSKUOq8ps2GWYtz1nTU44gmRgIJ92xHp2hPeCy206X0m4X-OajJ14FQ6z33GHhL3BfGUaWDnR2nvmu0q6AWJr8JR0aNFmolQ9sAO2yxr3oQn3E9KGtAuFIK-diPOAU2eNizVb8Kb7jnzY0iAss5AWb1ly_6TwZ2DGxVT-aTzuvKnl89B5TtX2bycBZOQRUbUgookEH4GsQjIBVLRWPRgaXGdBLTQIwwxZGmvtjH7pTh4ilA', NULL, NULL, NULL, NULL, '2020-09-05 11:07:55.512442+00:00', '2020-09-05 11:07:55.512442+00:00', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, NULL, 'devtestt', 'Dev Test', 'cheskmr@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$v1rKWnH7tJfeNjP/XvCUhA$QEeNpLrd+en48UpzFB4wh6rQRjZ9Q4jce0s/6HsvrMo', 87366441, 87366442, 87366443, NULL, NULL, 'male', '1996-03-01T00:00:00.000+02:00', NULL, NULL, 'english', 'public', true, 'eyJhbGciOiJSUzI1NiJ9.eyJ0eXBlIjoiZW1haWwiLCJleHBpcmVzIjoxNjAwMDc1NzMxODE3LCJpZGVudGlmaWVyIjoiY2hlc2ttckBnbWFpbC5jb20ifQ.uQ1GOYrmcmS9NJy7aaIR2c9Sz9vqfbwfYA05YpZkSXVoAEbIe70spYIbUmGit2R4XntQ7UelsDMGfzPNWIuBAYJ9S1w6E-NcKuPd1ykW-heT1T6Y53kKgS5mLO1wA07RWbe8GBSUBoD4TWkuIu2hc5Dit_hqw2opGLVy7k659iY4bJOVBLOICDUs83GZx_O1552h-LIDHDVuLoAkevqoRzXO6o0TbarBjvGqglljac75A_UMDPlqLeDmSt9ZoXut11Y5zasSEm7anhy0YDk4BRJEdIsOndUkqQw2DFUzyB7iUGc8vplwwbnoxCcDeFjSwdAIQbVN037xcjdsxGz-kA', NULL, NULL, NULL, NULL, '2020-09-07 10:48:52.6337+00:00', '2020-09-07 10:48:52.633+00:00', '2020-09-14 14:29:22+00:00', NULL, NULL, NULL, NULL, NULL);

SELECT setval('t_roles_id_seq', 4, false);

INSERT INTO t_roles (id, type) VALUES
	(1, 'creator'),
	(2, 'admin'),
	(3, 'viewer');

SELECT setval('t_account_id_seq', 22, false);

INSERT INTO t_account (id, followers_notification, likes_notification, comments_notification, tags_notification, created, updated, deleted, "personId", "businessId", role_id, "baseBusinessAccountId") VALUES
	(1, true, true, true, true, '2020-08-19 13:38:10.283706+00:00', NULL, NULL, 1, NULL, NULL, NULL),
	(2, true, true, true, true, '2020-08-19 13:40:29.816417+00:00', NULL, NULL, 2, NULL, NULL, NULL),
	(3, true, true, true, true, '2020-08-20 08:27:16.93969+00:00', NULL, NULL, 3, NULL, NULL, NULL),
	(4, true, true, true, true, '2020-08-20 09:32:42.298483+00:00', NULL, NULL, 4, NULL, NULL, NULL),
	(5, true, true, true, true, '2020-08-20 10:59:59.756834+00:00', NULL, NULL, 5, NULL, NULL, NULL),
	(6, true, true, true, true, '2020-08-23 08:37:42.535627+00:00', NULL, NULL, 6, NULL, NULL, NULL),
	(7, true, true, true, true, '2020-08-23 08:57:44.011119+00:00', NULL, NULL, 7, NULL, NULL, NULL),
	(8, true, true, true, true, '2020-08-23 10:00:52.243687+00:00', NULL, NULL, 7, 1, 2, NULL),
	(9, true, true, true, true, '2020-08-23 10:12:21.202406+00:00', '2020-09-16 09:26:53+00:00', NULL, 4, 2, 2, NULL),
	(10, true, true, true, true, '2020-08-23 17:50:08.311842+00:00', NULL, '2020-08-23 18:10:28+00:00', 6, 1, 2, 8),
	(11, true, true, true, true, '2020-08-23 18:12:35.963298+00:00', '2020-08-25 09:31:24.802+00:00', NULL, 6, 1, 2, 8),
	(12, true, true, true, true, '2020-08-24 07:57:08.259837+00:00', NULL, '2020-08-24 08:00:27+00:00', 6, 3, 2, NULL),
	(13, true, true, true, true, '2020-08-27 10:41:23.571165+00:00', NULL, NULL, 8, NULL, NULL, NULL),
	(14, true, true, true, true, '2020-08-27 10:42:06.991137+00:00', NULL, NULL, 9, NULL, NULL, NULL),
	(15, true, true, true, true, '2020-08-27 15:17:04.146002+00:00', NULL, NULL, 9, 4, 2, NULL),
	(17, true, true, true, true, '2020-09-05 11:07:55.593387+00:00', NULL, NULL, 10, NULL, NULL, NULL),
	(18, true, true, true, true, '2020-09-07 10:48:52.687559+00:00', NULL, NULL, 11, NULL, NULL, NULL),
	(20, true, true, true, true, '2020-09-17 09:54:23.661328+00:00', NULL, NULL, 4, 5, 2, NULL),
	(21, true, true, true, true, '2020-09-21 15:04:05.357466+00:00', NULL, NULL, 9, 6, 2, NULL);

SELECT setval('t_shop_category_id_seq', 12, false);

INSERT INTO t_shop_category (id, parent_id, name, created, updated, deleted, user_id) VALUES
	(1, NULL, 'Backpacks', '2020-08-20 10:49:12.170254+00:00', NULL, NULL, 3),
	(2, NULL, 'Wash Gear', '2020-08-20 10:53:42.792844+00:00', NULL, NULL, 3),
	(3, NULL, 'Trekking stoves', '2020-08-20 11:30:59.092716+00:00', NULL, NULL, 3),
	(4, NULL, e'Bettw\u00E4sche', '2020-08-23 09:41:15.999081+00:00', NULL, NULL, 6),
	(5, NULL, 'test', '2020-08-24 10:55:32.994532+00:00', NULL, NULL, 9),
	(6, NULL, 'Test', '2020-08-24 11:09:34.124683+00:00', NULL, NULL, 4),
	(7, NULL, 'Spiegel', '2020-08-25 06:33:27.766225+00:00', NULL, NULL, 8),
	(8, NULL, 'Schuhe', '2020-08-29 10:49:55.608523+00:00', NULL, NULL, 4),
	(9, NULL, 'test', '2020-09-15 09:23:11.119908+00:00', NULL, NULL, 18),
	(10, NULL, 'some test shop category', '2020-09-16 14:42:04.584075+00:00', NULL, NULL, 15),
	(11, NULL, 'Category1', '2020-09-18 09:47:25.316436+00:00', NULL, NULL, 20);

SELECT setval('t_product_photo_id_seq', 51, false);

INSERT INTO t_product_photo (id, "photoUrl", "thumbUrl", "order", created, deleted, product_id) VALUES
	(1, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onszg-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onszg-thumb.jpg', 0, '2020-08-20 10:49:44.920719+00:00', NULL, 1),
	(2, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onu0m-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onu0m-thumb.jpg', 1, '2020-08-20 10:49:45.000657+00:00', NULL, 1),
	(3, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onvat-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onvat-thumb.jpg', 2, '2020-08-20 10:49:45.031485+00:00', NULL, 1),
	(4, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onwi8-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2onwi8-thumb.jpg', 3, '2020-08-20 10:49:45.093411+00:00', NULL, 1),
	(5, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxfee-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxfee-thumb.jpg', 0, '2020-08-20 10:57:13.260481+00:00', NULL, 2),
	(6, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxgpj-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxgpj-thumb.jpg', 1, '2020-08-20 10:57:13.302468+00:00', NULL, 2),
	(7, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxhrs-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxhrs-thumb.jpg', 2, '2020-08-20 10:57:13.364554+00:00', NULL, 2),
	(8, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxj1f-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxj1f-thumb.jpg', 3, '2020-08-20 10:57:13.397749+00:00', NULL, 2),
	(9, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxkdp-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxkdp-thumb.jpg', 4, '2020-08-20 10:57:13.463894+00:00', NULL, 2),
	(10, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxl1m-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2oxl1m-thumb.jpg', 5, '2020-08-20 10:57:13.502909+00:00', NULL, 2),
	(11, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2q62te-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2q62te-thumb.jpg', 0, '2020-08-20 11:31:57.469134+00:00', NULL, 3),
	(12, 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6wl8yi-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6wl8yi-thumb.jpg', 0, '2020-08-23 09:42:46.536192+00:00', NULL, 5),
	(13, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8en7ae-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8en7ae-thumb.jpg', 0, '2020-08-24 10:55:57.001503+00:00', NULL, 6),
	(14, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8f52kb-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8f52kb-thumb.jpg', 0, '2020-08-24 11:09:51.614698+00:00', NULL, 7),
	(15, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8f5m4j-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8f5m4j-thumb.jpg', 0, '2020-08-24 11:10:16.965844+00:00', NULL, 8),
	(16, NULL, NULL, 0, '2020-08-25 06:34:32.314302+00:00', NULL, 9),
	(17, NULL, NULL, 0, '2020-08-25 06:34:45.735777+00:00', NULL, 10),
	(18, NULL, NULL, 0, '2020-08-25 06:34:50.696363+00:00', NULL, 11),
	(19, NULL, NULL, 0, '2020-08-25 06:36:01.877238+00:00', NULL, 12),
	(20, NULL, NULL, 0, '2020-08-25 06:36:07.69816+00:00', NULL, 13),
	(21, NULL, NULL, 0, '2020-08-25 06:36:10.895249+00:00', NULL, 14),
	(22, NULL, NULL, 0, '2020-08-25 06:38:01.466151+00:00', NULL, 15),
	(23, NULL, NULL, 0, '2020-08-25 06:38:22.593701+00:00', NULL, 16),
	(24, NULL, NULL, 0, '2020-08-25 06:38:27.394088+00:00', NULL, 17),
	(25, NULL, NULL, 0, '2020-08-25 06:39:38.984095+00:00', NULL, 18),
	(26, 'https://minio.staging.grit.sc/photo/photo-fcw2fe11kecxcuei-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-fcw2fe11kecxcuei-thumb.jpg', 0, '2020-08-27 14:50:52.24307+00:00', NULL, 19),
	(27, 'https://minio.staging.grit.sc/photo/photo-1lmmhf1ckewnscvt-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1lmmhf1ckewnscvt-thumb.jpg', 1, '2020-09-10 10:18:22.209822+00:00', NULL, 20),
	(28, 'https://minio.staging.grit.sc/photo/photo-igncc1ckf3r0y82-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-igncc1ckf3r0y82-thumb.jpg', 0, '2020-09-15 09:23:26.794171+00:00', NULL, 21),
	(29, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hut7x-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hut7x-thumb.jpg', 0, '2020-09-16 14:42:14.57285+00:00', NULL, 22),
	(30, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hwnp0-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hwnp0-thumb.jpg', 0, '2020-09-16 14:43:40.705482+00:00', NULL, 23),
	(31, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hwp6r-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5hwp6r-thumb.jpg', 1, '2020-09-16 14:43:40.7717+00:00', NULL, 23),
	(32, 'https://minio.staging.grit.sc/photo/photo-lb11kf827tno-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf827tno-thumb.jpg', 2, '2020-09-18 09:47:44.093375+00:00', NULL, 24),
	(33, 'https://minio.staging.grit.sc/photo/photo-lb11kf827x8b-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf827x8b-thumb.jpg', 1, '2020-09-18 09:47:44.181884+00:00', NULL, 24),
	(35, 'https://minio.staging.grit.sc/photo/photo-lb11kf82p5bx-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf82p5bx-thumb.jpg', 0, '2020-09-18 10:01:15.016137+00:00', NULL, 24),
	(36, 'https://minio.staging.grit.sc/photo/photo-lb11kf86349i-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf86349i-thumb.jpg', 2, '2020-09-18 11:36:06.718067+00:00', NULL, 20),
	(37, 'https://minio.staging.grit.sc/photo/photo-lb11kf863s0q-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf863s0q-thumb.jpg', 0, '2020-09-18 11:36:37.658745+00:00', NULL, 20),
	(38, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayvwu6-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayvwu6-thumb.jpg', 0, '2020-09-20 10:37:47.354825+00:00', NULL, 25),
	(39, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayvyho-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayvyho-thumb.jpg', 1, '2020-09-20 10:37:47.415547+00:00', NULL, 25),
	(40, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw0m1-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw0m1-thumb.jpg', 2, '2020-09-20 10:37:47.447319+00:00', NULL, 25),
	(41, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw2ms-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw2ms-thumb.jpg', 3, '2020-09-20 10:37:47.475917+00:00', NULL, 25),
	(42, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw485-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfayw485-thumb.jpg', 4, '2020-09-20 10:37:47.558601+00:00', NULL, 25),
	(43, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb1vs7z-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb1vs7z-thumb.jpg', 0, '2020-09-20 12:01:40.309089+00:00', NULL, 26),
	(44, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb1vu0w-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb1vu0w-thumb.jpg', 1, '2020-09-20 12:01:40.402069+00:00', NULL, 26),
	(45, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb2ecxh-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb2ecxh-thumb.jpg', 0, '2020-09-20 12:16:08.658108+00:00', NULL, 27),
	(46, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb2eduj-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb2eduj-thumb.jpg', 1, '2020-09-20 12:16:08.708931+00:00', NULL, 27),
	(47, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb3mncs-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb3mncs-thumb.jpg', 0, '2020-09-20 12:50:35.757381+00:00', NULL, 28),
	(48, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb3mp4o-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb3mp4o-thumb.jpg', 1, '2020-09-20 12:50:35.833483+00:00', NULL, 28),
	(49, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfbfabgf-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfbfabgf-thumb.jpg', 0, '2020-09-20 18:16:56.474147+00:00', NULL, 29),
	(50, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfbfavhx-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfbfavhx-thumb.jpg', 0, '2020-09-20 18:17:21.537203+00:00', NULL, 30);

SELECT setval('t_product_id_seq', 31, false);

INSERT INTO t_product (id, type, title, condition, price, quantity, category, description, "variantOptions", "deliveryOptions", "deliveryTime", "shippingCosts", "payerReturn", bank_id, active, draft, created, updated, deleted, user_id, business_id, shopcategory_id, address_id, cover_id) VALUES
	(1, 'singleProduct', 'Aether Pro 70', 'new', 300, 8, 'SPORTS_AND_OUTDOORS', e'Men\'s 70L multi-day lightweight expedition backpack with removable componentry', NULL, 'onlyShipping', 5, 0, 'seller', NULL, true, false, '2020-08-20 10:49:44.840962+00:00', '2020-08-20 10:49:45.221+00:00', NULL, 3, NULL, 1, NULL, NULL),
	(2, 'productVariants', 'SoftFibre Printed Towel', 'new', NULL, NULL, 'SPORTS_AND_OUTDOORS', e'The SoftFibre travel towel is the original and our most popular travel towel - it\U00002019s a great all-rounder. Super soft and lightweight, it absorbs 9 times its own weight in water and dries 8 times faster than a conventional towel. The towel is an ideal travel companion, no matter how far your adventure takes you.\n\nOur range of printed towels have been enhanced and redeveloped to bring a fresh feel with exciting prints.', 'color', 'both', 4, 10, 'buyer', NULL, true, false, '2020-08-20 10:57:13.182306+00:00', '2020-08-20 10:57:14.071+00:00', NULL, 3, NULL, 2, 1, NULL),
	(3, 'singleProduct', 'Primus Mimer Stove', 'new', 27.95, 20, 'OTHER', e'Launched in the early 1980\U00002019s, the Mimer Stove has become the essential workhorse stove that has traveled on countless adventures around the world.\n\nQuickly attach your Mimer Stove onto your gas canister and you can have your stove up and running in under a minute. The robust pot supports houses a wide flame that is perfect for cooking for up to three people and provides a 2800W flame that can boil a liter of water in under four minutes.\n\nEasily break down your trekking stove by simply removing the burner and stowing it in its nylon storage bag.', NULL, 'onlyCollection', NULL, NULL, 'buyer', NULL, true, false, '2020-08-20 11:31:57.383657+00:00', '2020-08-20 11:31:57.585+00:00', NULL, 3, NULL, 3, 1, NULL),
	(4, 'productVariants', NULL, 'used', NULL, NULL, NULL, NULL, NULL, 'onlyShipping', NULL, NULL, NULL, NULL, true, true, '2020-08-23 08:39:31.934631+00:00', '2020-08-23 08:39:32.077+00:00', NULL, 6, NULL, NULL, NULL, NULL),
	(5, 'singleProduct', e'DINO Kinderbettw\u00E4sche in hellblau', 'used', 12, 0, 'HOME_AND_GARDEN', e'Ich verkaufe eine sch\u00F6ne Dino Bettw\u00E4sche f\u00FCr Kinder in der Farbe hellblau. \n\nNur per Abholung m\u00F6glich. Da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-23 09:42:46.443689+00:00', '2020-08-23 09:42:46.656+00:00', NULL, 6, NULL, 4, 2, NULL),
	(6, 'singleProduct', 'Playstation 4', 'new', 10, 10, 'BICYCLES', 'aergerhzerherh', NULL, 'onlyShipping', 3, 3, NULL, NULL, false, false, '2020-08-24 10:55:56.928156+00:00', '2020-08-24 10:55:57.085+00:00', NULL, 9, 2, 5, NULL, NULL),
	(7, 'singleProduct', 'loki', 'used', 10, 5, 'BABY_AND_KIDS', 'rrtjtjtrj', NULL, 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-08-24 11:09:51.543984+00:00', '2020-08-24 11:09:51.701+00:00', NULL, 4, NULL, 6, NULL, NULL),
	(8, 'singleProduct', 'hello', 'new', 20, 50, 'B2B', 'gddgdg', NULL, 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-08-24 11:10:16.879515+00:00', '2020-08-24 11:10:17.1+00:00', NULL, 4, NULL, 6, NULL, NULL),
	(9, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:34:32.245293+00:00', '2020-08-25 06:34:32.561+00:00', NULL, 8, 1, 7, 7, NULL),
	(10, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:34:45.650424+00:00', '2020-08-25 06:34:46.07+00:00', NULL, 8, 1, 7, 7, NULL),
	(11, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:34:50.65277+00:00', '2020-08-25 06:34:50.774+00:00', NULL, 8, 1, 7, 7, NULL),
	(12, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:36:01.779067+00:00', '2020-08-25 06:36:01.98+00:00', NULL, 8, 1, 7, 7, NULL),
	(13, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:36:07.647604+00:00', '2020-08-25 06:36:07.782+00:00', NULL, 8, 1, 7, 7, NULL),
	(14, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:36:10.840541+00:00', '2020-08-25 06:36:10.982+00:00', NULL, 8, 1, 7, 7, NULL),
	(15, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:38:01.363828+00:00', '2020-08-25 06:38:01.564+00:00', NULL, 8, 1, 7, 7, NULL),
	(16, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, false, '2020-08-25 06:38:22.550442+00:00', '2020-08-25 06:38:22.673+00:00', NULL, 8, 1, 7, 9, NULL),
	(17, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, true, '2020-08-25 06:38:27.336424+00:00', '2020-08-25 06:38:27.486+00:00', NULL, 8, 1, 7, 9, NULL),
	(18, 'singleProduct', 'Spiegel', 'used', 25, 1, 'HOME_AND_GARDEN', e'Ich verkaufe einen sch\u00F6nen gebrauchten massiven Spiegel. \n\nNur per Abholung m\u00F6glich, da Privatverkauf!', NULL, 'onlyCollection', NULL, NULL, NULL, NULL, true, true, '2020-08-25 06:39:38.847104+00:00', '2020-08-25 06:39:39.081+00:00', NULL, 8, 1, 7, 9, NULL),
	(19, 'singleProduct', 'asdfasdf', 'new', 13, 13, 'B2B', '123', NULL, 'onlyShipping', 13, 13, NULL, NULL, true, false, '2020-08-27 14:50:52.139848+00:00', '2020-08-27 14:50:52.369+00:00', NULL, 14, NULL, NULL, NULL, NULL),
	(20, 'singleProduct', 'Test', 'new', 10, 9, 'BOOKS', 'hellow orld', NULL, 'onlyShipping', 10, 10, NULL, NULL, true, false, '2020-09-10 10:18:22.119217+00:00', '2020-09-18 11:36:37.887+00:00', NULL, 4, NULL, 6, NULL, NULL),
	(21, 'singleProduct', 'test product', 'new', 1, 1, 'BABY_AND_KIDS', 'product', NULL, 'onlyShipping', 1, 1, NULL, NULL, true, false, '2020-09-15 09:23:26.571676+00:00', '2020-09-15 09:23:26.884+00:00', NULL, 18, NULL, 9, NULL, NULL),
	(22, 'singleProduct', 'Test Single Product', 'new', 13, 13, 'B2B', 'asdfasdfasdfasdf', NULL, 'onlyShipping', 13, 13, NULL, NULL, true, false, '2020-09-16 14:42:14.454914+00:00', '2020-09-16 14:42:14.689+00:00', '2020-09-21 16:00:11+00:00', 15, 4, 10, NULL, NULL),
	(23, 'productVariants', 'Test Product with variants', 'new', NULL, NULL, 'B2B', 'asdfas', 'size', 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-09-16 14:43:40.593453+00:00', '2020-09-16 14:43:41.176+00:00', '2020-09-21 15:59:52+00:00', 15, 4, 10, NULL, NULL),
	(24, 'singleProduct', 'Product', 'used', 10, 15, 'BABY_AND_KIDS', 'Hello World!', NULL, 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-09-18 09:47:44.009872+00:00', '2020-09-18 10:01:15.19+00:00', NULL, 20, 5, 11, NULL, NULL),
	(25, 'productVariants', 'Product Variant', 'new', NULL, NULL, 'BICYCLES', 'Hello World', 'sizeColorMaterial', 'onlyShipping', 3, 3, NULL, NULL, false, false, '2020-09-20 10:37:47.046994+00:00', '2020-09-20 10:37:49.45+00:00', NULL, 20, 5, 11, NULL, NULL),
	(26, 'productVariants', 'Test', 'new', NULL, NULL, 'BABY_AND_KIDS', 'Hello world', 'sizeColorMaterial', 'onlyShipping', 3, 3, NULL, NULL, false, false, '2020-09-20 12:01:40.235971+00:00', '2020-09-20 12:01:41.873+00:00', NULL, 20, 5, 11, NULL, NULL),
	(27, 'productVariants', 'Test', 'new', NULL, NULL, 'B2B', 'rgergegerg', 'sizeColor', 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-09-20 12:16:08.557792+00:00', '2020-09-20 12:16:09.439+00:00', NULL, 20, 5, 11, NULL, NULL),
	(28, 'productVariants', 'New product', 'new', NULL, NULL, 'B2B', 'hello world', 'sizeColorMaterial', 'onlyShipping', 3, 3, NULL, NULL, true, false, '2020-09-20 12:50:35.633829+00:00', '2020-09-20 12:50:36.148+00:00', NULL, 20, 5, 11, NULL, NULL),
	(29, 'singleProduct', 'test 1', 'new', 11, 11, 'CARS', '111', NULL, 'onlyShipping', 11, 11, NULL, NULL, true, false, '2020-09-20 18:16:56.39002+00:00', '2020-09-20 18:16:56.579+00:00', NULL, 9, 2, NULL, NULL, NULL),
	(30, 'singleProduct', 'test 2', 'new', 22, 22, 'CARS', '222', NULL, 'onlyShipping', 22, 22, NULL, NULL, true, false, '2020-09-20 18:17:21.475832+00:00', '2020-09-20 18:17:21.644+00:00', NULL, 9, 2, NULL, NULL, NULL);

SELECT setval('t_review_video_id_seq', 1, false);

SELECT setval('t_product_review_id_seq', 1, false);

SELECT setval('t_product_review_replies_id_seq', 1, false);

SELECT setval('t_seller_review_id_seq', 2, false);

INSERT INTO t_seller_review (id, rating, text, created, updated, user_id, seller_id, "videoId") VALUES
	(1, 2, 'Hello world', '2020-09-21 10:42:42.929815+00:00', NULL, 20, 20, NULL);

SELECT setval('t_seller_review_replies_id_seq', 2, false);

INSERT INTO t_seller_review_replies (id, text, created, updated, review_id, seller_id, user_id) VALUES
	(1, 'Test', '2020-09-21 10:44:19.47191+00:00', NULL, 1, NULL, 20);

SELECT setval('t_service_id_seq', 7, false);

INSERT INTO t_service (id, active, draft, title, price, quantity, "hourlyPrice", description, performance, category, bank_id, created, updated, deleted, user_id, address_id) VALUES
	(1, true, false, 'Angular - The Complete Guide (2020 Edition)', 9.99, 1, false, 'Master Angular 10 (formerly "Angular 2") and build awesome, reactive web apps with the successor of Angular.js', 'customer', 'EDUCATION', NULL, '2020-08-20 11:01:43.464679+00:00', '2020-08-20 11:01:43.464679+00:00', NULL, 3, NULL),
	(2, true, false, 'Tages und Abend Make - Up Kurs ', 30, 1, false, 'Make - Up Kurs', 'customer', 'BEAUTY_AND_WELLNESS', NULL, '2020-08-23 19:00:54.484811+00:00', '2020-08-23 19:00:54.484811+00:00', NULL, 6, NULL),
	(3, true, false, 'Tisch Dekoration', 25, 1, false, 'Ich biete Tisch dekorationen an.', 'customer', 'CHORES', NULL, '2020-09-13 16:21:49.77626+00:00', '2020-09-13 16:22:45.001811+00:00', NULL, 6, NULL),
	(4, true, false, 'Service', 10, 23, false, 'Hello world', 'provider', 'BEAUTY_AND_WELLNESS', NULL, '2020-09-18 10:20:30.60243+00:00', '2020-09-18 10:20:30.60243+00:00', NULL, 20, 16),
	(5, true, false, 'Test', 10, 23, false, 'hello world', 'provider', 'CHORES', NULL, '2020-09-18 11:37:34.549406+00:00', '2020-09-18 11:38:01.748608+00:00', NULL, 4, 6),
	(6, true, false, 'Service2', 20, 25, false, 'Hello World', 'provider', 'CHORES', NULL, '2020-09-18 11:42:07.075001+00:00', '2020-09-18 11:45:14.80873+00:00', NULL, 20, 16);

SELECT setval('t_service_review_id_seq', 1, false);

SELECT setval('t_service_review_replies_id_seq', 1, false);

SELECT setval('t_article_id_seq', 20, false);

INSERT INTO t_article (id, cover, thumb, title, text, category, location, draft, created, updated, deleted, user_id, business_address) VALUES
	(1, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke7gfh0a-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke7gfh0a-thumb.jpg', 'Gutschein - Make Up Workshop', e'<h1><strong>Liebe Leser, </strong></h1><h1>ich hei\u00DFe Natalie Clever, bin 26 Jahre alt und biete ab sofort ein Tages- &amp; Abend Make Up Kurs an.</h1><h1>Freue mich auf jeden der Lust und Zeit hat!</h1><h1>Meldet Euch!</h1><h1><br></h1><h1><br></h1><h1>Liebe Gr\u00FC\u00DFe</h1><p><img src="https://minio.staging.grit.sc/photo/photo-5eu4v21cke7gfj0t-photo.jpg"></p>', 'BEAUTY', e'K\u00F6ln', true, '2020-08-23 18:58:11.024285+00:00', '2020-08-23 18:58:16.976+00:00', NULL, 6, NULL),
	(2, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke88cjml-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke88cjml-thumb.jpg', 'Vjjkggcj', e'<p>Vbobok vuchckvlbl lk. Kvivivo k kvichovocovovovico. I ivovuxtfxuvk \u00F6n\u00F6nivuczcl\u00F6i j</p>', 'BEAUTY', 'Vuvvuvi', false, '2020-08-24 07:59:42.045508+00:00', '2020-08-24 07:59:46.58+00:00', '2020-08-24 08:00:19+00:00', 12, NULL),
	(3, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke9kjacb-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke9kjacb-thumb.jpg', 'Thermalbad Eifel Therme ', e'<p>Hallo Ihr Lieben,</p><p><br></p><p>ich berichte \u00FCber das Thermalbad " Eifel Therme" in Bad Bertrich! Diese Therme ist richtig sch\u00F6n. \U0000263A\U0000FE0F Es ist nicht nur ein Thermalbad, sondern auch eine tolle Sauna. \U00002600\U0000FE0F\U0001F321\U0000FE0FKommt vorbei! Probiert es selber aus, ich kann es nur weiter empfehlen.\U0001F44D\U0000263A\U0000FE0F\U0001F60D</p><p><br></p><p>Viel Spa\u00DF!</p><p><br></p><p>Bis bald Ihr Lieben \U0001F48B</p><p>Eure Natti</p>', 'BEAUTY', 'Eifel', false, '2020-08-25 06:28:38.954266+00:00', '2020-08-25 07:06:21+00:00', NULL, 8, NULL),
	(4, NULL, NULL, 'test article draft', '<p>sdfgsdfgsdg 22222</p>', 'ANIMALS', NULL, false, '2020-08-27 15:42:42.988302+00:00', '2020-08-28 10:48:56+00:00', NULL, 15, NULL),
	(5, 'https://minio.staging.grit.sc/photo/photo-3ohm512kee2o8e2-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512kee2o8e2-thumb.jpg', 'test article', '<p>sdfasdf</p>', 'ANIMALS', 'a', false, '2020-08-28 10:07:28.739155+00:00', '2020-08-28 10:07:31.889+00:00', NULL, 14, NULL),
	(6, 'https://minio.staging.grit.sc/photo/photo-3ohm512kee40i3k-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512kee40i3k-thumb.jpg', NULL, NULL, NULL, NULL, true, '2020-08-28 10:45:00.022747+00:00', '2020-08-28 10:45:04.081+00:00', NULL, 15, NULL),
	(7, 'https://minio.staging.grit.sc/photo/photo-1lmmhf12kevbngf3-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1lmmhf12kevbngf3-thumb.jpg', 'Test Article', '<p>test</p>', 'BEAUTY', NULL, false, '2020-09-09 11:50:51.7263+00:00', '2020-09-11 12:25:14+00:00', NULL, 18, NULL),
	(8, 'https://minio.staging.grit.sc/photo/photo-1lmmhf1ckey2kecd-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1lmmhf1ckey2kecd-thumb.jpg', 'test article ', e'<p>asdfasdfasdf <span class="mention" data-index="0" data-denotation-char="@" data-id="4" data-value="Thomas Lohmann">\U0000FEFF<span contenteditable="false"><span class="ql-mention-denotation-char">@</span>Thomas Lohmann</span>\U0000FEFF</span> </p>', 'ANIMALS', NULL, false, '2020-09-11 09:59:51.439829+00:00', '2020-09-11 09:59:56.179+00:00', '2020-09-16 11:51:03+00:00', 13, NULL),
	(9, 'https://minio.staging.grit.sc/photo/photo-igncc1ckf1az2jx-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-igncc1ckf1az2jx-thumb.jpg', 'HOTEL Therme in Bad Teinach ', e'<p>Liebe Leser,</p><p><br></p><p>hier in Bad Teinach, n\u00E4he Calw im Schwarzwald, gibt es ein tolles Wellness Hotel. \U00002600\U0000FE0F\U0001F60D\U0001F4AF</p><p>Probiert es selber aus! ....</p><p>#natur #hotel #badteinach #wellness #therme #sonnenschein #spa #erholung #sonne \U00002600\U0000FE0F\U0001F60D\U0001F3D8\U0000FE0F</p>', 'BEAUTY', NULL, false, '2020-09-13 16:18:26.126318+00:00', '2020-09-20 11:52:25+00:00', NULL, 6, NULL),
	(10, 'https://minio.staging.grit.sc/photo/photo-4slz11kf58qnep-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf58qnep-thumb.jpg', 'test', '<p><img src="{{1}}"></p>', 'EDUCATION', NULL, true, '2020-09-16 10:27:04.344723+00:00', '2020-09-16 10:27:08.677+00:00', '2020-09-16 10:31:00+00:00', 4, NULL),
	(11, 'https://minio.staging.grit.sc/photo/photo-4slz11kf58rfta-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf58rfta-thumb.jpg', 'hello', NULL, 'BEAUTY', NULL, true, '2020-09-16 10:27:42.277583+00:00', '2020-09-16 10:27:45.364+00:00', '2020-09-16 10:27:55+00:00', 4, NULL),
	(12, 'https://minio.staging.grit.sc/photo/photo-4slz11kf58wbyx-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf58wbyx-thumb.jpg', 'test', NULL, NULL, NULL, true, '2020-09-16 10:31:30.862796+00:00', '2020-09-16 10:31:34.134+00:00', '2020-09-16 10:31:50+00:00', 4, NULL),
	(13, 'https://minio.staging.grit.sc/photo/photo-4slz11kf58z0ug-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf58z0ug-thumb.jpg', 'hello world', '<p>thrhhrthrh</p>', 'DECOR', NULL, false, '2020-09-16 10:33:36.070191+00:00', '2020-09-16 10:33:39.683+00:00', '2020-09-16 10:33:52+00:00', 4, NULL),
	(14, 'https://minio.staging.grit.sc/photo/photo-4slz11kf590b4c-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf590b4c-thumb.jpg', 'hello world', NULL, NULL, NULL, true, '2020-09-16 10:34:36.371715+00:00', '2020-09-16 10:34:39.403+00:00', '2020-09-16 10:35:40+00:00', 4, NULL),
	(15, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5924er-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5924er-thumb.jpg', 'hello world', '<p>gtweegeg</p>', 'EDUCATION', NULL, true, '2020-09-16 10:35:59.717424+00:00', '2020-09-16 10:36:03.7+00:00', '2020-09-16 10:37:57+00:00', 4, NULL),
	(16, 'https://minio.staging.grit.sc/photo/photo-4slz11kf595ad5-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf595ad5-thumb.jpg', 'hello world', '<p>gergerg</p>', 'DECOR', NULL, true, '2020-09-16 10:38:28.67242+00:00', '2020-09-16 10:38:31.606+00:00', NULL, 4, NULL),
	(17, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5clrxw-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5clrxw-thumb.jpg', 'sdfsdfsdf', NULL, 'ANIMALS', NULL, true, '2020-09-16 12:15:07.385589+00:00', '2020-09-16 12:15:23.412+00:00', NULL, 13, NULL),
	(18, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb0ikpr-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfb0ikpr-thumb.jpg', e'Sonnenuntergang \U00002600\U0000FE0F\U0001F304\U0001F307', e'<h1><strong>Sonnenuntergang im Saarland!</strong></h1><h1><strong>Einfach nur traumhaft... \U00002600\U0000FE0F\U0001F304\U0001F307\U0001F4AF\U00002763\U0000FE0F</strong></h1><p>#sonnenuntergang #landschaft #b\u00E4ume #lebenslustig #freude #sonne #abendrot </p>', 'TRAVEL', NULL, false, '2020-09-20 11:23:20.977002+00:00', '2020-09-20 11:34:13+00:00', NULL, 6, NULL),
	(19, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfc9nc18-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfc9nc18-thumb.jpg', 'Test', '<p>gergergerg</p>', 'ANIMALS', NULL, false, '2020-09-21 08:26:53.037242+00:00', '2020-09-21 08:26:56.672+00:00', NULL, 20, NULL);

SELECT setval('t_article_comment_id_seq', 4, false);

INSERT INTO t_article_comment (id, text, created, updated, user_id, article_id) VALUES
	(1, e'Cooler Beitrag \U0001F44D', '2020-08-25 06:30:41.03108+00:00', NULL, 11, 3),
	(3, 'some comment', '2020-09-16 13:40:27.227048+00:00', NULL, 13, 5);

SELECT setval('t_article_comment_replies_id_seq', 1, false);

SELECT setval('t_article_comment_like_id_seq', 2, false);

INSERT INTO t_article_comment_like (id, created, updated, user_id, comment_id, reply_id, article_id) VALUES
	(1, '2020-08-25 07:05:15.416263+00:00', NULL, 11, 1, NULL, 3);

SELECT setval('t_article_like_id_seq', 5, false);

INSERT INTO t_article_like (id, created, user_id, article_id) VALUES
	(1, '2020-08-25 06:30:24.557968+00:00', 11, 3),
	(2, '2020-08-25 06:45:29.217944+00:00', 6, 3),
	(3, '2020-09-13 16:19:04.054443+00:00', 6, 9),
	(4, '2020-09-20 11:23:46.48096+00:00', 6, 18);

SELECT setval('t_article_photo_id_seq', 1, false);

SELECT setval('t_article_view_id_seq', 20, false);

INSERT INTO t_article_view (id, created, user_id, article_id) VALUES
	(1, '2020-08-24 07:59:50.279777+00:00', 12, 2),
	(2, '2020-08-25 06:29:02.884502+00:00', 8, 3),
	(3, '2020-08-25 06:45:18.787083+00:00', 6, 3),
	(5, '2020-08-27 15:42:58.893945+00:00', 15, 4),
	(6, '2020-08-31 13:55:36.540622+00:00', 14, 5),
	(7, '2020-09-09 11:51:00.12392+00:00', 18, 7),
	(8, '2020-09-13 16:18:39.0495+00:00', 6, 9),
	(9, '2020-09-14 08:51:10.963049+00:00', 4, 9),
	(10, '2020-09-16 10:27:57.16022+00:00', 4, 10),
	(11, '2020-09-16 10:37:45.995807+00:00', 4, 15),
	(12, '2020-09-16 11:50:19.315332+00:00', 13, 8),
	(13, '2020-09-16 13:40:18.882949+00:00', 13, 5),
	(14, '2020-09-16 13:47:39.199327+00:00', 13, 16),
	(15, '2020-09-18 08:14:21.981393+00:00', 4, 17),
	(17, '2020-09-20 11:23:34.560615+00:00', 6, 18),
	(18, '2020-09-20 15:29:56.99052+00:00', 4, 18),
	(19, '2020-09-21 15:50:59.881731+00:00', 4, 3);

SELECT setval('t_bank_id_seq', 1, false);

SELECT setval('t_booking_id_seq', 7, false);

INSERT INTO t_booking (id, total_price, total_time, fee, refund_amount, amount_card, payment_id, payment_status, payment_type, payment_date, pay_in_id, pay_in_status, pay_in_date, pay_in_refund_status, pay_in_refund_date, pay_out_status, pay_out_date, pay_out_refund_status, pay_out_refund_date, transaction, transfer_id, transfer_status, transfer_date, who_cancelled, when_cancelled, cancel_reason, cancel_description, paid, is_completed, cancelled, refunded, created, updated, buyer_id, seller_id, delivery_address_id, billing_address_id, service_id) VALUES
	(1, 10, 1, NULL, NULL, NULL, 88166471, NULL, NULL, NULL, '88166472', NULL, '2020-09-20 10:59:19.220919+00:00', NULL, NULL, NULL, '2020-09-20 10:59:19.220919+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 10:59:19.218+00:00', false, NULL, NULL, '2020-09-20 10:59:19.220919+00:00', NULL, 6, 4, NULL, NULL, 5),
	(2, 10, 1, NULL, NULL, NULL, 88166471, NULL, NULL, NULL, '88166473', NULL, '2020-09-20 10:59:23.297399+00:00', NULL, NULL, NULL, '2020-09-20 10:59:23.297399+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 10:59:23.268+00:00', false, NULL, NULL, '2020-09-20 10:59:23.297399+00:00', NULL, 6, 4, NULL, NULL, 5),
	(3, 30, 2, NULL, NULL, NULL, 88166547, NULL, NULL, NULL, '88166550', NULL, '2020-09-20 11:01:35.240673+00:00', NULL, NULL, NULL, '2020-09-20 11:01:35.240673+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 11:01:35.202+00:00', false, NULL, NULL, '2020-09-20 11:01:35.240673+00:00', NULL, 11, 6, NULL, NULL, 2),
	(4, 30, 2, NULL, NULL, NULL, 88166547, NULL, NULL, NULL, '88166553', NULL, '2020-09-20 11:01:37.647076+00:00', NULL, NULL, NULL, '2020-09-20 11:01:37.647076+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 11:01:37.643+00:00', false, NULL, NULL, '2020-09-20 11:01:37.647076+00:00', NULL, 11, 6, NULL, NULL, 2),
	(5, 30, 2, NULL, NULL, NULL, 88166547, NULL, NULL, NULL, '88166555', NULL, '2020-09-20 11:01:39.661615+00:00', NULL, NULL, NULL, '2020-09-20 11:01:39.661615+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 11:01:39.646+00:00', false, NULL, NULL, '2020-09-20 11:01:39.661615+00:00', NULL, 11, 6, NULL, NULL, 2),
	(6, 10, 3, NULL, NULL, NULL, 88305583, NULL, NULL, NULL, '88323454', NULL, '2020-09-22 12:15:28.455622+00:00', NULL, NULL, NULL, '2020-09-22 12:15:28.455622+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-22 12:15:28.451+00:00', false, NULL, NULL, '2020-09-22 12:15:28.455622+00:00', NULL, 9, 4, NULL, NULL, 5);

SELECT setval('t_booking_appointments_id_seq', 30, false);

INSERT INTO t_booking_appointments (id, start, "end", created, updated, booking_id) VALUES
	(1, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:26.993355+00:00', NULL, NULL),
	(2, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:27.990891+00:00', NULL, NULL),
	(3, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:28.897711+00:00', NULL, NULL),
	(4, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:51.888414+00:00', NULL, NULL),
	(5, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:52.877382+00:00', NULL, NULL),
	(6, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:53.010416+00:00', NULL, NULL),
	(7, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 06:55:53.368711+00:00', NULL, NULL),
	(8, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 07:02:09.348194+00:00', NULL, NULL),
	(9, '2020-08-26 15:00:00+00:00', '2020-08-26 17:00:00+00:00', '2020-08-25 07:02:11.165042+00:00', NULL, NULL),
	(10, '2020-09-18 12:00:00+00:00', '2020-09-18 14:00:00+00:00', '2020-09-13 16:23:06.442316+00:00', NULL, NULL),
	(11, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 07:18:58.241542+00:00', NULL, NULL),
	(12, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 07:18:58.085885+00:00', NULL, NULL),
	(13, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 07:18:59.440902+00:00', NULL, NULL),
	(14, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 07:19:28.20638+00:00', NULL, NULL),
	(15, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 07:19:33.945386+00:00', NULL, NULL),
	(16, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:57:05.277756+00:00', NULL, NULL),
	(17, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:57:07.380885+00:00', NULL, NULL),
	(18, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:57:07.851589+00:00', NULL, NULL),
	(19, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:57:07.94012+00:00', NULL, NULL),
	(20, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:57:08.05297+00:00', NULL, NULL),
	(21, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:58:43.904879+00:00', NULL, NULL),
	(22, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:59:17.815852+00:00', NULL, 1),
	(23, '2020-09-21 11:00:00+00:00', '2020-09-21 12:30:00+00:00', '2020-09-20 10:59:22.157653+00:00', NULL, 2),
	(24, '2020-09-24 09:00:00+00:00', '2020-09-24 11:00:00+00:00', '2020-09-20 11:01:33.682378+00:00', NULL, 3),
	(25, '2020-09-24 09:00:00+00:00', '2020-09-24 11:00:00+00:00', '2020-09-20 11:01:36.248239+00:00', NULL, 4),
	(26, '2020-09-24 09:00:00+00:00', '2020-09-24 11:00:00+00:00', '2020-09-20 11:01:37.94654+00:00', NULL, 5),
	(27, '2020-09-28 11:00:00+00:00', '2020-09-28 12:30:00+00:00', '2020-09-22 12:15:26.29843+00:00', NULL, 6),
	(28, '2020-10-12 11:00:00+00:00', '2020-10-12 12:30:00+00:00', '2020-09-22 12:15:26.386833+00:00', NULL, 6),
	(29, '2020-10-26 11:00:00+00:00', '2020-10-26 12:30:00+00:00', '2020-09-22 12:15:26.42137+00:00', NULL, 6);

SELECT setval('t_text_id_seq', 32, false);

INSERT INTO t_text (id, text, draft, created, updated, deleted, user_id) VALUES
	(1, 'Test', false, '2020-08-20 09:34:28.974665+00:00', NULL, NULL, 4),
	(2, 'Hello World', false, '2020-08-20 09:42:46.685911+00:00', NULL, NULL, 4),
	(3, 'Testthrthjzjkukjhkhbbbbbbbbbbbbbb', false, '2020-08-20 09:52:38.461416+00:00', '2020-08-20 09:55:52+00:00', NULL, 4),
	(4, 'Test', false, '2020-08-23 11:09:45.637192+00:00', NULL, '2020-08-23 18:31:04+00:00', 8),
	(5, 'Tezshdfjdudj', false, '2020-08-23 18:30:42.358277+00:00', '2020-08-23 18:30:54+00:00', '2020-08-23 18:31:00+00:00', 8),
	(6, 'Jdjddjdudbbd', false, '2020-08-23 18:31:10.593022+00:00', '2020-08-23 18:32:14+00:00', '2020-08-23 18:35:23+00:00', 8),
	(7, 'Test', false, '2020-08-23 18:34:59.385053+00:00', NULL, '2020-08-23 18:35:28+00:00', 8),
	(8, 'Xxxxxx', false, '2020-08-23 18:47:40.679952+00:00', '2020-09-20 07:43:51+00:00', NULL, 8),
	(9, 'Aupjvj hxzjv cuxcokcxcu n bvihiuu kckc', true, '2020-08-24 07:57:42.688895+00:00', NULL, NULL, 12),
	(10, 'test', false, '2020-08-24 10:26:19.66575+00:00', NULL, NULL, 9),
	(11, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', false, '2020-09-07 10:39:48.283974+00:00', '2020-09-08 09:35:42+00:00', '2020-09-16 10:23:23+00:00', 4),
	(12, e'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,uzououzoo', false, '2020-09-08 16:19:00.069604+00:00', '2020-09-16 10:19:00+00:00', '2020-09-16 10:19:12+00:00', 4),
	(13, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio, assumenda iure. Dignissimos ducimus culpa necessitatibus doloremque ut? Voluptatum laboriosam recusandae ab enim consequatur officia suscipit magnam. Voluptatem necessitatibus corrupti dicta.', false, '2020-09-09 11:02:54.003228+00:00', NULL, NULL, 18),
	(14, 'testfgjfgjgj', false, '2020-09-10 10:12:22.215087+00:00', '2020-09-16 10:17:59+00:00', '2020-09-16 10:18:07+00:00', 4),
	(15, 'liuluiluliljtzktzktkzztkzulllllllllllllllll', false, '2020-09-16 09:57:11.325204+00:00', '2020-09-16 09:58:21+00:00', '2020-09-16 09:58:28+00:00', 4),
	(16, e'kukk,,,\u00F6p\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6', false, '2020-09-16 09:58:41.786079+00:00', '2020-09-16 09:59:07+00:00', '2020-09-16 09:59:13+00:00', 4),
	(17, e'oooooooo\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6', true, '2020-09-16 09:59:19.790414+00:00', '2020-09-16 09:59:33+00:00', '2020-09-16 10:00:04+00:00', 4),
	(18, e'\u00F6\u00E4\u00F6\u00F6\u00F6', true, '2020-09-16 10:00:17.65363+00:00', NULL, '2020-09-16 10:00:32+00:00', 4),
	(19, e'\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4\u00E4', true, '2020-09-16 10:00:25.334847+00:00', NULL, '2020-09-16 10:00:36+00:00', 4),
	(20, 'test text', false, '2020-09-16 14:07:42.205556+00:00', NULL, NULL, 14),
	(21, 'asdfasdf', false, '2020-09-21 14:26:44.632375+00:00', NULL, NULL, 14),
	(22, 'fsdfdf', false, '2020-09-21 14:27:03.073612+00:00', NULL, NULL, 14),
	(23, 'fsdfdfdfffdf', false, '2020-09-21 14:27:14.654153+00:00', NULL, NULL, 14),
	(24, 'sdfsdfsdf', false, '2020-09-21 14:27:20.773802+00:00', NULL, NULL, 14),
	(25, 'sdfsdfsdfsdfsdf', false, '2020-09-21 14:27:26.132362+00:00', NULL, NULL, 14),
	(26, 'sdfsdfasdfasdf', false, '2020-09-21 14:27:37.924921+00:00', NULL, NULL, 14),
	(27, 'asdfasdfasdf', false, '2020-09-21 14:27:47.021797+00:00', NULL, NULL, 14),
	(28, 'asdfasdfasdfasdfasdf', false, '2020-09-21 14:27:55.193459+00:00', NULL, NULL, 14),
	(29, 'asdfasdfasdfasdf', false, '2020-09-21 14:29:18.936588+00:00', NULL, NULL, 14),
	(30, 'fdffdfdfdfdfdf', false, '2020-09-21 14:29:27.189928+00:00', NULL, NULL, 14),
	(31, 'gdgdgdgdgd', false, '2020-09-21 14:29:34.92331+00:00', NULL, NULL, 14);

SELECT setval('t_photo_id_seq', 24, false);

INSERT INTO t_photo (id, photo, thumb, title, description, category, location, draft, "filterName", created, updated, deleted, user_id, business_address) VALUES
	(1, 'https://minio.staging.grit.sc/photo/photo-46mdm11ke2m6133-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-46mdm11ke2m6133-thumb.jpg', 'Opera', 'Test', 'TRAVEL', e'K\u00F6ln', false, 'filter-inkwell', '2020-08-20 09:39:57.122726+00:00', '2020-08-20 09:40:00.903+00:00', NULL, 4, NULL),
	(2, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2p1ogh-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2p1ogh-thumb.jpg', 'saasasas', NULL, 'BEAUTY', NULL, false, 'normal', '2020-08-20 11:00:32.273403+00:00', '2020-08-20 11:00:36.311+00:00', '2020-09-17 13:59:59+00:00', 5, NULL),
	(3, 'https://minio.staging.grit.sc/photo/photo-4t4xk11ke2ruv2r-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4t4xk11ke2ruv2r-thumb.jpg', 'Hello', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'DECOR', NULL, false, 'normal', '2020-08-20 12:19:13.042466+00:00', '2020-09-09 14:48:14+00:00', NULL, 4, NULL),
	(4, 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6vdl18-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6vdl18-thumb.jpg', NULL, NULL, NULL, NULL, true, 'normal', '2020-08-23 09:08:17.270245+00:00', '2020-08-23 09:08:54.416+00:00', '2020-08-23 09:10:03+00:00', 6, NULL),
	(5, 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6vfe3c-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v211ke6vfe3c-thumb.jpg', 'Landschaft ', e'Eifel Trip\U00002600\U0000FE0F\U0001F333\U0001F9D8\U0001F3D5\U0000FE0F\U0001F3D8\U0000FE0F\n#eifel #landschaft #ruhe #sonne #felder', 'TRAVEL', 'Eifel', false, 'filter-juno', '2020-08-23 09:10:09.051138+00:00', '2020-09-20 11:50:13+00:00', NULL, 6, NULL),
	(6, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8do3px-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8do3px-thumb.jpg', 'Opera', NULL, 'EDUCATION', NULL, false, 'filter-inkwell', '2020-08-24 10:28:40.560097+00:00', '2020-08-24 10:28:44.377+00:00', '2020-08-24 10:30:32+00:00', 9, NULL),
	(7, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8ebwve-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8ebwve-thumb.jpg', 'test', NULL, 'DECOR', NULL, false, 'normal', '2020-08-24 10:47:10.82851+00:00', '2020-08-24 10:47:15.088+00:00', '2020-08-24 10:51:00+00:00', 9, NULL),
	(8, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8itam9-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke8itam9-thumb.jpg', 'Test', NULL, 'DECOR', NULL, false, 'filter-aden', '2020-08-24 12:52:39.702442+00:00', '2020-08-24 12:52:44.683+00:00', NULL, 9, NULL),
	(9, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke9jzi1m-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke9jzi1m-thumb.jpg', 'Eifel Therme', e'Eifel Therme \n#eifel #therme #sauna #thermalbad #schwimmen #tauchen #spa\u00DF #erlebnis', 'BEAUTY', 'Eifel', false, 'filter-ludwig', '2020-08-25 06:13:15.651139+00:00', '2020-08-25 06:13:19.724+00:00', NULL, 8, NULL),
	(10, 'https://minio.staging.grit.sc/photo/photo-3ohm512kee0ok6m-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512kee0ok6m-thumb.jpg', 'asdfasdf 3333', 'asdfsdf 2222', 'EDUCATION', NULL, false, 'normal', '2020-08-28 09:11:43.79156+00:00', '2020-08-28 10:49:37+00:00', NULL, 15, NULL),
	(11, 'https://minio.staging.grit.sc/photo/photo-3ohm512kee1xfz5-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512kee1xfz5-thumb.jpg', 'sdfasdf', 'dfsdfsdf #test', 'ANIMALS', NULL, false, 'normal', '2020-08-28 09:46:34.501891+00:00', '2020-08-31 13:56:02+00:00', NULL, 14, NULL),
	(12, NULL, NULL, 'asdfas draft', NULL, NULL, NULL, true, 'normal', '2020-08-28 14:03:23.783404+00:00', NULL, NULL, 15, NULL),
	(13, 'https://minio.staging.grit.sc/photo/photo-3ohm512keilzvf2-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512keilzvf2-thumb.jpg', 'test photo', NULL, 'DIY', NULL, false, 'normal', '2020-08-31 14:19:28.667821+00:00', '2020-08-31 14:19:31.971+00:00', NULL, 14, NULL),
	(14, 'https://minio.staging.grit.sc/photo/photo-3ohm512keim3q8t-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-3ohm512keim3q8t-thumb.jpg', 'Some test photo', NULL, 'BEAUTY', NULL, false, 'normal', '2020-08-31 14:22:10.498509+00:00', '2020-08-31 14:22:32.058+00:00', NULL, 14, NULL),
	(15, 'https://minio.staging.grit.sc/photo/photo-1lmmhf12kewnlqei-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1lmmhf12kewnlqei-thumb.jpg', 'test', e'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,', 'DIY', NULL, false, 'normal', '2020-09-10 10:13:13.128299+00:00', '2020-09-10 13:36:28+00:00', '2020-09-16 10:19:42+00:00', 4, NULL),
	(16, 'https://minio.staging.grit.sc/photo/photo-igncc11kf1aqp9b-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-igncc11kf1aqp9b-thumb.jpg', e'\U00002600\U0000FE0F\U00002618\U0000FE0F\U0001F495', e'Nachdenklich ....\n#sonne #sommer #nachdenken #tr\u00E4ume #leben \U00002600\U0000FE0F\U0001F3D8\U0000FE0F\U0001F60D\U0001F4AF', 'OTHER', NULL, false, 'filter-mayfair', '2020-09-13 16:12:02.711942+00:00', '2020-09-20 11:55:05+00:00', NULL, 6, NULL),
	(17, 'https://minio.staging.grit.sc/photo/photo-4slz11kf541f1g-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf541f1g-thumb.jpg', 'dfgdf', 'fdggfdgd sd gd fdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gdfdggfdgd sd gd', 'EDUCATION', NULL, false, 'normal', '2020-09-16 08:15:26.83091+00:00', '2020-09-21 13:07:14+00:00', NULL, 18, NULL),
	(18, 'https://minio.staging.grit.sc/photo/photo-4slz11kf584ve2-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf584ve2-thumb.jpg', e'jzjtjtzj\u00F6oi\u00F6io\u00F6io\u00F6', e'jztjtzjtzj\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6\u00F6 #1 #2 #3 #4 #5 #6 #7 #8 #9 #10 @Natalie Clever', 'DIY', NULL, false, 'filter-inkwell', '2020-09-16 10:10:09.715704+00:00', '2020-09-16 10:24:09+00:00', NULL, 4, NULL),
	(19, 'https://minio.staging.grit.sc/photo/photo-4slz11kf58o7sj-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf58o7sj-thumb.jpg', '8o7o7o87o78o', 'luluil', 'ANIMALS', NULL, false, 'filter-crema', '2020-09-16 10:24:40.290826+00:00', '2020-09-16 10:25:37+00:00', '2020-09-16 10:25:58+00:00', 4, NULL),
	(20, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5bqdbx-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5bqdbx-thumb.jpg', NULL, NULL, NULL, NULL, true, 'normal', '2020-09-16 11:50:51.487039+00:00', '2020-09-16 11:50:55.815+00:00', NULL, 13, NULL),
	(21, 'https://minio.staging.grit.sc/photo/photo-4slz11kf5gn5y3-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-4slz11kf5gn5y3-thumb.jpg', 'test photo', NULL, 'ANIMALS', NULL, false, 'normal', '2020-09-16 14:08:12.197132+00:00', '2020-09-16 14:08:27.198+00:00', NULL, 14, NULL),
	(22, 'https://minio.staging.grit.sc/photo/photo-1rtht11kfc9o3fi-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1rtht11kfc9o3fi-thumb.jpg', 'Test', NULL, 'DECOR', NULL, false, 'normal', '2020-09-21 08:27:28.483715+00:00', '2020-09-21 08:27:32.086+00:00', NULL, 20, NULL),
	(23, NULL, NULL, 'fgdfgd', NULL, NULL, NULL, true, 'normal', '2020-09-21 13:26:38.667316+00:00', NULL, NULL, 18, NULL);

SELECT setval('t_video_id_seq', 15, false);

INSERT INTO t_video (id, video, cover, thumb, title, description, duration, category, location, draft, created, updated, deleted, user_id, business_address) VALUES
	(1, NULL, NULL, NULL, e'Sothys Fr\u00FChjahr / Sommer Look ', 'SOTHYS', NULL, 'BEAUTY', e'K\u00F6ln', false, '2020-08-23 09:45:00.833199+00:00', '2020-08-23 10:01:10+00:00', '2020-08-23 10:16:28+00:00', 6, NULL),
	(2, NULL, NULL, NULL, 'Test', 'Test', NULL, 'HUMOR', NULL, false, '2020-08-23 09:55:50.057175+00:00', NULL, NULL, 7, NULL),
	(3, NULL, NULL, NULL, 'SCHRANK ', e'Schuhe \U0001F44D\U0001F603', NULL, 'BEAUTY', e'K\u00F6ln', false, '2020-08-23 18:50:17.480965+00:00', NULL, '2020-08-24 07:52:46+00:00', 6, NULL),
	(4, NULL, NULL, NULL, 'Eifel Therme', NULL, NULL, NULL, NULL, true, '2020-08-25 06:42:50.102489+00:00', NULL, NULL, 6, NULL),
	(5, NULL, NULL, NULL, 'Test', NULL, NULL, 'ANIMALS', NULL, false, '2020-09-10 10:13:52.125629+00:00', NULL, '2020-09-11 11:06:43+00:00', 4, NULL),
	(6, NULL, NULL, NULL, 'Schrank...', e'Schuhe....\U0001FA70\U0001F461\U0001F462\U0001F97E\U0001F97F\U0001F45F\U0001F45E\U0001F460', NULL, 'BEAUTY', NULL, true, '2020-09-13 16:14:52.674036+00:00', NULL, NULL, 6, NULL),
	(7, NULL, NULL, NULL, 'Test', NULL, NULL, 'ANIMALS', NULL, false, '2020-09-14 13:45:49.144705+00:00', NULL, '2020-09-16 07:58:04+00:00', 4, NULL),
	(8, NULL, NULL, NULL, 'test small', 'asdfasdf', NULL, 'ANIMALS', NULL, false, '2020-09-14 16:58:51.042703+00:00', NULL, NULL, 14, NULL),
	(9, NULL, NULL, NULL, 'test 2', NULL, NULL, NULL, NULL, true, '2020-09-14 17:27:43.768058+00:00', NULL, NULL, 14, NULL),
	(10, NULL, NULL, NULL, 'asaasas', 'saasas', NULL, 'DIY', NULL, false, '2020-09-17 13:34:24.918252+00:00', NULL, '2020-09-17 13:58:35+00:00', 5, NULL),
	(11, NULL, NULL, NULL, '1111', 'ssss', NULL, 'DIY', NULL, false, '2020-09-17 14:00:18.39332+00:00', NULL, '2020-09-17 14:03:01+00:00', 5, NULL),
	(12, 'https://minio.staging.grit.sc/video/video-3l7caz11kf6vy6a3/video-3l7caz11kf6vy6a3_h264_master', 'https://minio.staging.grit.sc/video/video-3l7caz11kf6vy6a3/video-3l7caz11kf6vy6a3_cover.jpg', NULL, 'Test', NULL, '00:03', 'BEAUTY', NULL, false, '2020-09-17 14:04:17.435397+00:00', '2020-09-17 14:05:46.417+00:00', NULL, 20, NULL),
	(13, 'https://minio.staging.grit.sc/video/video-1rtht11kfarzc5h/video-1rtht11kfarzc5h_h264_master', 'https://minio.staging.grit.sc/video/video-1rtht11kfarzc5h/video-1rtht11kfarzc5h_cover.jpg', NULL, e'Begehbarer Schrank \U0001FA70\U0001F45C\U0001F97C\U0001F9E5\U0001FA72\U0001F459\U0001F460\U0001F45F\U0001F45E\U0001F97E\U0001F457\U0001F458\U0001F97B\U0001F454\U0001F45A\U0001F3BD\U0001F455\U0001F456\U0001F9E3', e'\U0001F97E\U0001F45E\U0001F45F\U0001F460\U0001F459\U0001F9E5\U0001F97C\U0001F45C\n#kleidung #lustig #frauen #m\u00E4nner #bier #spa\u00DF \U0001F929\U0001F602', '00:38', 'FASHION', NULL, false, '2020-09-20 07:24:28.575338+00:00', '2020-09-20 11:58:09+00:00', NULL, 6, NULL),
	(14, 'https://minio.staging.grit.sc/video/video-1rtht11kfbahjq4/video-1rtht11kfbahjq4_h264_master', 'https://minio.staging.grit.sc/photo/cover-1rtht11kfbaluoo-photo.jpg', 'https://minio.staging.grit.sc/photo/cover-1rtht11kfbaluoo-thumb.jpg', 'Test', NULL, '00:03', 'BEAUTY', NULL, false, '2020-09-20 16:02:18.967304+00:00', '2020-09-20 16:06:01.038+00:00', '2020-09-20 16:09:16+00:00', 4, NULL);

SELECT setval('t_bookmarks_id_seq', 16, false);

INSERT INTO t_bookmarks (id, created, owner_id, account_id, text_id, photo_id, video_id, article_id, product_id, service_id) VALUES
	(3, '2020-08-24 07:56:17.194+00:00', 6, NULL, NULL, 5, NULL, NULL, NULL, NULL),
	(4, '2020-08-24 08:02:07.406+00:00', 6, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(10, '2020-09-21 08:26:15.509+00:00', 20, NULL, NULL, NULL, NULL, NULL, 28, NULL),
	(11, '2020-09-21 08:26:21.289+00:00', 20, NULL, NULL, NULL, NULL, NULL, 27, NULL),
	(12, '2020-09-21 08:26:27.809+00:00', 20, NULL, NULL, NULL, NULL, NULL, NULL, 6),
	(15, '2020-09-21 13:33:27.334+00:00', 18, NULL, NULL, 17, NULL, NULL, NULL, NULL);

SELECT setval('t_business_user_category_id_seq', 1, false);

SELECT setval('t_cancel_booking_reason_id_seq', 5, false);

INSERT INTO t_cancel_booking_reason (id, name) VALUES
	(1, e'I don\U00002019t like the service'),
	(2, 'Poor service'),
	(3, 'Price-performance'),
	(4, 'Other');

SELECT setval('t_cancel_order_reason_id_seq', 10, false);

INSERT INTO t_cancel_order_reason (id, name) VALUES
	(1, e'I don\U00002019t like the product'),
	(2, 'Poor quality'),
	(3, 'Price-performance'),
	(4, 'Wrong product'),
	(5, 'Product damaged'),
	(6, 'Product defective'),
	(7, 'Delivery incomplete'),
	(8, 'Delivered too late'),
	(9, 'Other');

SELECT setval('t_product_variant_id_seq', 26, false);

INSERT INTO t_product_variant (id, size, color, flavor, material, price, quantity, enable, created, updated, deleted, product_id) VALUES
	(1, NULL, 'Tropical', NULL, NULL, 19.99, 6, true, '2020-08-20 10:57:13.637436+00:00', NULL, NULL, 2),
	(2, NULL, 'Stripped Planks', NULL, NULL, 19.99, 2, true, '2020-08-20 10:57:13.852505+00:00', NULL, NULL, 2),
	(3, 'X', NULL, NULL, NULL, 1, 2, true, '2020-09-16 14:43:40.800302+00:00', NULL, NULL, 23),
	(4, 'XX', NULL, NULL, NULL, 3, 4, true, '2020-09-16 14:43:40.897127+00:00', NULL, NULL, 23),
	(5, 'XXL', NULL, NULL, NULL, 5, 6, true, '2020-09-16 14:43:40.992918+00:00', NULL, NULL, 23),
	(6, 'S', 'Black', NULL, 'Cotton', 10, 5, true, '2020-09-20 10:37:47.686531+00:00', NULL, NULL, 25),
	(7, 'S', 'White', NULL, 'Cotton', 20, 8, true, '2020-09-20 10:37:47.862206+00:00', NULL, NULL, 25),
	(8, 'M', 'Black', NULL, 'Cotton', 10, 6, true, '2020-09-20 10:37:48.053818+00:00', NULL, NULL, 25),
	(9, 'M', 'White', NULL, 'Cotton', 12, 75, true, '2020-09-20 10:37:48.211641+00:00', NULL, NULL, 25),
	(10, 'S', 'Black', NULL, 'Silk', 15, 14, true, '2020-09-20 10:37:48.384027+00:00', NULL, NULL, 25),
	(11, 'S', 'White', NULL, 'Silk', 21, 87, true, '2020-09-20 10:37:48.572503+00:00', NULL, NULL, 25),
	(12, 'M', 'Black', NULL, 'Silk', 12, 78, true, '2020-09-20 10:37:48.790051+00:00', NULL, NULL, 25),
	(13, 'M', 'White', NULL, 'Silk', 12.78, 11, true, '2020-09-20 10:37:48.935176+00:00', NULL, NULL, 25),
	(14, 'S', 'Black', NULL, 'Cotton', 3, 26, true, '2020-09-20 12:01:40.932761+00:00', NULL, NULL, 26),
	(15, 'S', 'White', NULL, 'Cotton', 2, 83, true, '2020-09-20 12:01:41.139462+00:00', NULL, NULL, 26),
	(16, 'M', 'Black', NULL, 'Cotton', 150, 10, true, '2020-09-20 12:01:41.299118+00:00', NULL, NULL, 26),
	(17, 'M', 'White', NULL, 'Cotton', NULL, NULL, false, '2020-09-20 12:01:41.497381+00:00', NULL, NULL, 26),
	(18, 'L', 'Black', NULL, 'Cotton', NULL, NULL, false, '2020-09-20 12:01:41.632693+00:00', NULL, NULL, 26),
	(19, 'L', 'White', NULL, 'Cotton', NULL, NULL, false, '2020-09-20 12:01:41.764758+00:00', NULL, NULL, 26),
	(20, 'S', 'Black', NULL, NULL, 10, 12, true, '2020-09-20 12:16:08.837042+00:00', NULL, NULL, 27),
	(21, 'M', 'Black', NULL, NULL, 23, 8, true, '2020-09-20 12:16:08.973738+00:00', NULL, NULL, 27),
	(22, 'S', 'White', NULL, NULL, NULL, NULL, false, '2020-09-20 12:16:09.134608+00:00', NULL, NULL, 27),
	(23, 'M', 'White', NULL, NULL, 23, 47, true, '2020-09-20 12:16:09.206228+00:00', NULL, NULL, 27),
	(24, 'S', NULL, NULL, NULL, 3, 5, true, '2020-09-20 12:50:35.86485+00:00', NULL, NULL, 28),
	(25, 'M', NULL, NULL, NULL, NULL, NULL, false, '2020-09-20 12:50:35.959024+00:00', NULL, NULL, 28);

SELECT setval('t_cart_product_id_seq', 7, false);

INSERT INTO t_cart_product (id, quantity, collected, created, updated, user_id, product_id, product_variant_id) VALUES
	(2, 1, true, '2020-08-23 11:46:13.980178+00:00', '2020-08-23 11:46:31.933+00:00', 8, 5, NULL),
	(3, 1, true, '2020-08-25 07:55:10.976533+00:00', NULL, 4, 5, NULL),
	(4, 1, false, '2020-09-18 09:54:40.869189+00:00', NULL, 4, 20, NULL),
	(6, 1, false, '2020-09-22 09:50:27.929314+00:00', NULL, 9, 20, NULL);

SELECT setval('t_chatroom_id_seq', 14, false);

INSERT INTO t_chatroom (id, title, cover, thumb, created, deleted, updated) VALUES
	(1, '', '', '', '2020-08-23 11:13:26.861+00:00', NULL, NULL),
	(2, '', '', '', '2020-08-23 18:48:47.461+00:00', NULL, NULL),
	(3, '', '', '', '2020-08-23 19:08:43.869+00:00', NULL, NULL),
	(4, '', '', '', '2020-08-25 06:23:59.768+00:00', NULL, NULL),
	(5, '', '', '', '2020-08-25 06:23:59.966+00:00', NULL, NULL),
	(6, '', '', '', '2020-08-25 06:24:00.179+00:00', NULL, NULL),
	(7, '', '', '', '2020-08-31 13:39:29.588+00:00', NULL, NULL),
	(8, '', '', '', '2020-09-03 11:54:14.503+00:00', NULL, NULL),
	(9, '', '', '', '2020-09-17 10:02:10.252+00:00', NULL, NULL),
	(10, '', '', '', '2020-09-17 13:00:04.917+00:00', NULL, NULL),
	(11, '', '', '', '2020-09-18 09:01:50.115+00:00', NULL, NULL),
	(12, '', '', '', '2020-09-21 08:40:49.291+00:00', NULL, NULL),
	(13, '', '', '', '2020-09-21 15:06:45.311+00:00', NULL, NULL);

SELECT setval('t_message_id_seq', 47, false);

INSERT INTO t_message (id, text, is_forwarded, is_service_message, created, deleted, updated, reply_to, chatroom_id, user_id, shared_user_id, shared_text_id, shared_photo_id, shared_video_id, shared_article_id, shared_product_id, shared_service_id) VALUES
	(1, 'Test', false, false, '2020-08-23 11:13:27.183334+00:00', NULL, NULL, NULL, 1, 8, NULL, 4, NULL, NULL, NULL, NULL, NULL),
	(2, e'Hey Thommy, schau mal \U0001F44D', false, false, '2020-08-23 18:48:47.697421+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, 5, NULL, NULL, NULL, NULL),
	(3, '', false, false, '2020-08-23 19:08:44.171017+00:00', NULL, NULL, NULL, 3, 6, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(4, e'Hey , schau mal rein ...\U0001F44D\U0000263A\U0000FE0F', false, false, '2020-08-25 06:24:00.89104+00:00', NULL, NULL, NULL, 5, 11, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(5, e'Hey , schau mal rein ...\U0001F44D\U0000263A\U0000FE0F', false, false, '2020-08-25 06:24:00.974808+00:00', NULL, NULL, NULL, 6, 11, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(6, e'Hey , schau mal rein ...\U0001F44D\U0000263A\U0000FE0F', false, false, '2020-08-25 06:24:01.673981+00:00', NULL, NULL, NULL, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(7, 'Hi!!!', false, false, '2020-08-31 13:39:29.87443+00:00', NULL, NULL, NULL, 7, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'Test', false, false, '2020-09-03 11:16:08.67981+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'Look my new post!', false, false, '2020-09-03 11:54:14.502103+00:00', NULL, NULL, NULL, 2, 4, NULL, 2, NULL, NULL, NULL, NULL, NULL),
	(10, 'Look my new post!', false, false, '2020-09-03 11:54:15.880948+00:00', NULL, NULL, NULL, 8, 4, NULL, 2, NULL, NULL, NULL, NULL, NULL),
	(11, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', false, false, '2020-09-03 14:07:33.025895+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', false, false, '2020-09-03 14:09:38.383352+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'jzjzj', false, false, '2020-09-07 13:16:15.984388+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'jz', false, false, '2020-09-07 13:16:16.67406+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'j', false, false, '2020-09-07 13:16:16.764927+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, e'Huhu \U0001F603\U00002763\U0000FE0F', false, false, '2020-09-16 18:49:28.285399+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, e'\U0001F60B\U0001F60D\U0001F603\U0001F44D\U0001F4AF\U0001F60A\U0001F602\U0001F605', false, false, '2020-09-16 18:49:39.276674+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, e'\U0001F44D\U0001F602\U0001F4AF', false, false, '2020-09-16 18:50:58.01541+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, 3, NULL, NULL, NULL, NULL),
	(19, e'\U0001F44D\U0001F602\U0001F4AF', false, false, '2020-09-16 18:50:58.172718+00:00', NULL, NULL, NULL, 3, 6, NULL, NULL, 3, NULL, NULL, NULL, NULL),
	(20, e'\U0001F44D\U0001F602\U0001F4AF', false, false, '2020-09-16 18:50:58.115956+00:00', NULL, NULL, NULL, 1, 6, NULL, NULL, 3, NULL, NULL, NULL, NULL),
	(21, 'Wie gehts?', false, false, '2020-09-16 18:52:38.086612+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 'Test', false, false, '2020-09-17 08:38:39.795514+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 'Wie gehts?', false, false, '2020-09-17 08:38:54.764921+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, false, false, '2020-09-17 08:39:10.41324+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, e'\U0001F44D', false, false, '2020-09-17 08:39:36.171282+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, e'\U0001F412', false, false, '2020-09-17 08:42:43.315272+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', false, false, '2020-09-17 08:43:40.477359+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, false, false, '2020-09-17 08:47:54.481282+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, NULL, false, false, '2020-09-17 08:48:38.972141+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, NULL, false, false, '2020-09-17 08:50:29.287722+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 'Test', false, false, '2020-09-17 09:13:43.044527+00:00', NULL, NULL, NULL, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 'Test', false, false, '2020-09-17 10:02:10.604023+00:00', NULL, NULL, NULL, 9, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 'Test', false, false, '2020-09-17 10:02:33.105847+00:00', NULL, NULL, NULL, 9, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 'Test', false, false, '2020-09-17 10:02:44.525666+00:00', NULL, NULL, NULL, 9, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 'Hi', false, false, '2020-09-17 13:00:05.258703+00:00', NULL, NULL, NULL, 10, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, NULL, false, false, '2020-09-17 13:00:50.423222+00:00', NULL, NULL, NULL, 10, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 'Uuuuu', false, false, '2020-09-17 13:01:05.191566+00:00', NULL, NULL, NULL, 10, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 'Test', false, false, '2020-09-18 09:01:50.408999+00:00', NULL, NULL, NULL, 11, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, 'Schau mal rein . ', false, false, '2020-09-20 07:21:22.291551+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(40, e'Spitze \U0001F44D\U0001F603', false, false, '2020-09-20 10:55:33.232905+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, NULL, false, false, '2020-09-20 10:56:07.523699+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, e'Hallo Thomas \U0001F44D\U0001F603', false, false, '2020-09-20 10:56:43.98993+00:00', NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, e'\U0001F600Huhu\U0001F44D', false, false, '2020-09-20 11:03:03.95939+00:00', NULL, NULL, NULL, 8, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, e'\U0001F6B0', false, false, '2020-09-20 11:03:15.443463+00:00', NULL, NULL, NULL, 8, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, 'Test from business to business', false, false, '2020-09-21 08:40:49.565633+00:00', NULL, NULL, NULL, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, 'Test', false, false, '2020-09-21 15:06:45.63875+00:00', NULL, NULL, NULL, 13, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

SELECT setval('t_chatroom_user_id_seq', 27, false);

INSERT INTO t_chatroom_user (id, is_muted, is_blocked, created, deleted, updated, chatroom_id, user_id, last_read_message) VALUES
	(1, false, false, '2020-08-23 11:13:26.933509+00:00', NULL, '2020-09-16 18:51:06.233+00:00', 1, 6, 20),
	(2, false, false, '2020-08-23 11:13:26.945412+00:00', NULL, NULL, 1, 8, NULL),
	(3, false, false, '2020-08-23 18:48:47.527251+00:00', NULL, '2020-09-21 12:41:16.098+00:00', 2, 4, 42),
	(4, false, false, '2020-08-23 18:48:47.565974+00:00', NULL, '2020-09-20 10:56:50.261+00:00', 2, 6, 42),
	(5, false, false, '2020-08-23 19:08:43.963524+00:00', NULL, NULL, 3, 7, NULL),
	(6, false, false, '2020-08-23 19:08:43.962099+00:00', NULL, '2020-09-16 18:51:04.556+00:00', 3, 6, 19),
	(7, false, false, '2020-08-25 06:23:59.983357+00:00', NULL, NULL, 4, 5, NULL),
	(8, false, false, '2020-08-25 06:24:00.086551+00:00', NULL, '2020-09-13 16:09:26.104+00:00', 5, 6, 4),
	(9, false, false, '2020-08-25 06:24:00.170603+00:00', NULL, '2020-09-20 11:02:57.016+00:00', 4, 11, 6),
	(10, false, false, '2020-08-25 06:24:00.266697+00:00', NULL, '2020-09-20 11:00:38.309+00:00', 5, 11, 4),
	(11, false, false, '2020-08-25 06:24:00.577447+00:00', NULL, '2020-09-20 11:02:40.718+00:00', 6, 11, 5),
	(12, false, false, '2020-08-25 06:24:00.578673+00:00', NULL, NULL, 6, 8, NULL),
	(13, false, false, '2020-08-31 13:39:29.682145+00:00', NULL, '2020-08-31 13:40:03.347+00:00', 7, 14, 7),
	(15, false, false, '2020-09-03 11:54:14.627543+00:00', NULL, '2020-09-21 12:33:43.465+00:00', 8, 4, 44),
	(16, false, false, '2020-09-03 11:54:15.37434+00:00', NULL, '2020-09-20 11:03:20.865+00:00', 8, 11, 44),
	(17, false, false, '2020-09-17 10:02:10.331016+00:00', NULL, '2020-09-17 10:02:48.482+00:00', 9, 20, 34),
	(18, false, false, '2020-09-17 10:02:10.332253+00:00', NULL, '2020-09-20 11:08:30.244+00:00', 9, 6, 34),
	(19, false, false, '2020-09-17 13:00:05.063362+00:00', NULL, NULL, 10, 14, NULL),
	(20, false, false, '2020-09-17 13:00:05.072579+00:00', NULL, '2020-09-17 13:01:09.761+00:00', 10, 15, 37),
	(21, false, false, '2020-09-18 09:01:50.215472+00:00', NULL, NULL, 11, 14, NULL),
	(22, false, false, '2020-09-18 09:01:50.22563+00:00', NULL, '2020-09-18 09:01:55.518+00:00', 11, 20, 38),
	(23, false, false, '2020-09-21 08:40:49.398067+00:00', NULL, '2020-09-21 08:40:54.336+00:00', 12, 9, 45),
	(24, false, false, '2020-09-21 08:40:49.428877+00:00', NULL, NULL, 12, 15, NULL),
	(25, false, false, '2020-09-21 15:06:45.403638+00:00', NULL, '2020-09-21 15:06:50.081+00:00', 13, 4, 46),
	(26, false, false, '2020-09-21 15:06:45.413609+00:00', NULL, NULL, 13, 20, NULL);

SELECT setval('t_contact_id_seq', 1, false);

SELECT setval('t_days_id_seq', 8, false);

INSERT INTO t_days (id, day) VALUES
	(1, 'Monday'),
	(2, 'Tuesday'),
	(3, 'Wednesday'),
	(4, 'Thursday'),
	(5, 'Friday'),
	(6, 'Saturday'),
	(7, 'Sunday');

SELECT setval('t_followings_id_seq', 20, false);

INSERT INTO t_followings (id, created, "accountId", "followedAccountId") VALUES
	(1, '2020-08-23 09:34:20.681+00:00', 7, 6),
	(2, '2020-08-23 09:34:48.392+00:00', 6, 7),
	(3, '2020-08-23 09:36:09.977+00:00', 6, 4),
	(4, '2020-08-23 10:13:59.322+00:00', 6, 5),
	(5, '2020-08-23 11:08:51.299+00:00', 8, 6),
	(8, '2020-08-23 18:36:34.975+00:00', 6, 8),
	(11, '2020-08-25 06:17:04.705+00:00', 8, 8),
	(12, '2020-08-25 06:22:05.067+00:00', 8, 5),
	(13, '2020-08-28 09:15:59.024+00:00', 13, 15),
	(14, '2020-09-03 12:47:35.488+00:00', 4, 6),
	(15, '2020-09-13 16:09:57.556+00:00', 6, 14),
	(16, '2020-09-16 18:52:05.472+00:00', 6, 18),
	(18, '2020-09-18 09:04:23.223+00:00', 20, 13),
	(19, '2020-09-21 15:04:08.664+00:00', 4, 20);

SELECT setval('t_followings_request_id_seq', 3, false);

SELECT setval('t_hashtag_id_seq', 51, false);

INSERT INTO t_hashtag (id, tag, photo_id, video_id, article_id, text_id, product_id, service_id) VALUES
	(1, '#eifel', 9, NULL, NULL, NULL, NULL, NULL),
	(2, '#therme', 9, NULL, NULL, NULL, NULL, NULL),
	(3, '#sauna', 9, NULL, NULL, NULL, NULL, NULL),
	(4, '#thermalbad', 9, NULL, NULL, NULL, NULL, NULL),
	(5, '#schwimmen', 9, NULL, NULL, NULL, NULL, NULL),
	(6, '#tauchen', 9, NULL, NULL, NULL, NULL, NULL),
	(7, '#spa', 9, NULL, NULL, NULL, NULL, NULL),
	(8, '#erlebnis', 9, NULL, NULL, NULL, NULL, NULL),
	(9, '#test', 11, NULL, NULL, NULL, NULL, NULL),
	(35, '#eifel', 5, NULL, NULL, NULL, NULL, NULL),
	(36, '#landschaft', 5, NULL, NULL, NULL, NULL, NULL),
	(37, '#ruhe', 5, NULL, NULL, NULL, NULL, NULL),
	(38, '#sonne', 5, NULL, NULL, NULL, NULL, NULL),
	(39, '#felder', 5, NULL, NULL, NULL, NULL, NULL),
	(40, '#sonne', 16, NULL, NULL, NULL, NULL, NULL),
	(41, '#sommer', 16, NULL, NULL, NULL, NULL, NULL),
	(42, '#nachdenken', 16, NULL, NULL, NULL, NULL, NULL),
	(43, e'#tr\u00E4ume', 16, NULL, NULL, NULL, NULL, NULL),
	(44, '#leben', 16, NULL, NULL, NULL, NULL, NULL),
	(45, '#kleidung', NULL, 13, NULL, NULL, NULL, NULL),
	(46, '#lustig', NULL, 13, NULL, NULL, NULL, NULL),
	(47, '#frauen', NULL, 13, NULL, NULL, NULL, NULL),
	(48, e'#m\u00E4nner', NULL, 13, NULL, NULL, NULL, NULL),
	(49, '#bier', NULL, 13, NULL, NULL, NULL, NULL),
	(50, e'#spa\u00DF', NULL, 13, NULL, NULL, NULL, NULL);

SELECT setval('t_hours_id_seq', 13, false);

INSERT INTO t_hours (id, from_h, to_h, created, updated, business_id, day_id) VALUES
	(1, '01:00:00', '11:30:00', '2020-08-24 11:28:14.253763+00:00', '2020-09-20 16:11:37.115222+00:00', 2, 1),
	(2, '16:00:00', '22:30:00', '2020-08-24 11:28:14.319808+00:00', '2020-09-20 16:11:37.159063+00:00', 2, 1),
	(3, '00:00:00', '02:30:00', '2020-08-28 14:34:40.551388+00:00', '2020-08-28 14:34:40.551388+00:00', 4, 1),
	(4, '09:00:00', '13:00:00', '2020-09-20 07:42:24.795407+00:00', '2020-09-20 11:02:21.086577+00:00', 1, 1),
	(5, '14:00:00', '18:00:00', '2020-09-20 07:42:24.895667+00:00', '2020-09-20 11:02:21.141295+00:00', 1, 1),
	(6, '08:00:00', '12:00:00', '2020-09-20 07:42:24.931032+00:00', '2020-09-20 11:02:21.1858+00:00', 1, 2),
	(7, '13:00:00', '18:00:00', '2020-09-20 07:42:24.966012+00:00', '2020-09-20 11:02:21.25582+00:00', 1, 2),
	(8, '09:00:00', '13:00:00', '2020-09-20 07:42:25.003707+00:00', '2020-09-20 11:02:21.302173+00:00', 1, 3),
	(9, '09:00:00', '14:00:00', '2020-09-20 07:42:25.049729+00:00', '2020-09-20 11:02:21.378649+00:00', 1, 4),
	(10, '15:30:00', '20:00:00', '2020-09-20 07:42:25.08431+00:00', '2020-09-20 11:02:21.423983+00:00', 1, 4),
	(12, '10:00:00', '13:00:00', '2020-09-20 07:42:25.216106+00:00', '2020-09-20 11:02:21.484092+00:00', 1, 6);

SELECT setval('t_languages_id_seq', 3, false);

INSERT INTO t_languages (id, value, display_name, "default") VALUES
	(1, 'german', 'German', 'true'),
	(2, 'english', 'English', 'false');

SELECT setval('t_photo_comment_id_seq', 38, false);

INSERT INTO t_photo_comment (id, text, created, updated, user_id, photo_id) VALUES
	(4, e'Sch\u00F6ne Landschaft! \U0001F44D\U0001F333\U00002600\U0000FE0F', '2020-08-23 09:18:15.99968+00:00', NULL, 6, 5),
	(5, e'Wow cool \U0001F44D\U0000263A\U0000FE0F\U00002600\U0000FE0F', '2020-08-25 06:19:11.168443+00:00', NULL, 11, 8),
	(6, e'Wow cool \U0001F44D\U0000263A\U0000FE0F\U00002600\U0000FE0F', '2020-08-25 06:21:01.887181+00:00', NULL, 11, 8),
	(7, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:17.98654+00:00', NULL, 11, 2),
	(8, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:18.169928+00:00', NULL, 11, 2),
	(9, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:18.578166+00:00', NULL, 11, 2),
	(10, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:19.011451+00:00', NULL, 11, 2),
	(11, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:19.176917+00:00', NULL, 11, 2),
	(12, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:20.453753+00:00', NULL, 11, 2),
	(13, e'Cool\U0001F44D\U0000263A\U0000FE0F', '2020-08-25 06:22:21.772024+00:00', NULL, 11, 2),
	(14, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:06.106659+00:00', NULL, 11, 9),
	(15, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:08.467234+00:00', NULL, 11, 9),
	(16, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:10.859046+00:00', NULL, 11, 9),
	(17, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:10.988253+00:00', NULL, 11, 9),
	(18, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:11.004084+00:00', NULL, 11, 9),
	(19, e'\U0001F60D\U0001F48B\U0001F44D\U0001F332', '2020-08-25 07:03:11.189885+00:00', NULL, 11, 9),
	(20, 'asdfasdf', '2020-08-28 09:13:57.009255+00:00', NULL, 13, 10),
	(21, 'asdfasdf', '2020-08-28 09:51:10.110267+00:00', NULL, 13, 11),
	(22, 'asdfasdfsdf 22313', '2020-08-28 10:49:59.80393+00:00', '2020-08-28 10:50:05.081+00:00', 15, 10),
	(23, 'Some comment', '2020-09-04 10:12:02.912932+00:00', NULL, 14, 14),
	(24, 'Anothercomment', '2020-09-04 10:12:10.882285+00:00', NULL, 14, 14),
	(25, 'Wwwwww', '2020-09-04 10:12:15.189539+00:00', NULL, 14, 14),
	(26, e'Dhdhf\n', '2020-09-04 10:12:41.594915+00:00', NULL, 14, 14),
	(27, 'SOme comment', '2020-09-16 13:24:34.409421+00:00', NULL, 13, 13),
	(28, 'some comment', '2020-09-16 13:36:36.47849+00:00', NULL, 13, 13),
	(29, 'some comment2', '2020-09-16 13:37:04.324479+00:00', NULL, 13, 13),
	(30, e'Huhu \U0001F44D\U0001F4AF\U0001F60A\U0001F605', '2020-09-16 18:50:24.394735+00:00', NULL, 6, 3),
	(31, e'Sehr s\u00FC\u00DF\U0001F431', '2020-09-16 18:51:51.211354+00:00', NULL, 6, 17),
	(32, e'Wow \U0001F603', '2020-09-20 07:20:21.296228+00:00', NULL, 6, 1),
	(33, e'\U0001F44D\U0001F44D\U0001F44D\U0001F44D', '2020-09-20 07:23:01.394655+00:00', NULL, 6, 3),
	(34, e'\U0001F495\U00002600\U0000FE0F\U0001F48C', '2020-09-20 07:39:14.384422+00:00', NULL, 6, 16),
	(35, e'hhhhhhhhhh\n\nh\n\n\n\nn\nnnnn', '2020-09-21 10:35:21.203442+00:00', '2020-09-21 10:38:03.684+00:00', 20, 21),
	(36, 'Test', '2020-09-21 10:35:46.19721+00:00', NULL, 20, 21);

SELECT setval('t_photo_comment_replies_id_seq', 2, false);

INSERT INTO t_photo_comment_replies (id, text, created, updated, comment_id, user_id, photo_id) VALUES
	(1, 'some reply', '2020-09-16 13:24:51.379472+00:00', NULL, 27, 13, 13);

SELECT setval('t_text_comment_id_seq', 8, false);

INSERT INTO t_text_comment (id, text, created, updated, user_id, text_id) VALUES
	(1, 'Test', '2020-08-20 09:44:35.917103+00:00', NULL, 4, 2),
	(2, e'Hallo \U0001F603', '2020-08-23 09:37:06.326959+00:00', NULL, 6, 2);

SELECT setval('t_text_comment_replies_id_seq', 2, false);

INSERT INTO t_text_comment_replies (id, text, created, updated, comment_id, text_id, user_id) VALUES
	(1, 'Hello', '2020-08-20 09:44:51.046103+00:00', NULL, 1, 2, 4);

SELECT setval('t_video_comment_id_seq', 5, false);

INSERT INTO t_video_comment (id, text, created, updated, user_id, video_id) VALUES
	(1, e'\U0001F44D\U00002600\U0000FE0F\U0001F48B', '2020-08-23 18:35:13.282194+00:00', NULL, 6, 2),
	(3, 'some video', '2020-09-16 13:40:53.386794+00:00', NULL, 13, 8),
	(4, e'\U0001F926\U0001F605\U0001F602\U0001F4AF', '2020-09-20 11:17:06.676474+00:00', NULL, 6, 13);

SELECT setval('t_video_comment_replies_id_seq', 1, false);

SELECT setval('t_mentions_id_seq', 3, false);

SELECT setval('t_message_attachment_id_seq', 7, false);

INSERT INTO t_message_attachment (id, link, thumb, duration, file_name, type, created, deleted, message_id, chatroom_id, user_id) VALUES
	(1, 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb11kf6kbnaj-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb11kf6kbnaj-thumb.jpg', NULL, NULL, 'photo', '2020-09-17 08:39:10.452259+00:00', NULL, 24, 2, 4),
	(2, 'https://minio.staging.grit.sc/document/document-attachment-dno4cb12kf6kmvvv.xlsx', 'assets/images/document.jpg', NULL, 'New tasks estimate from A2SEVEN team.xlsx', 'document', '2020-09-17 08:47:54.573378+00:00', NULL, 28, 2, 4),
	(3, 'https://minio.staging.grit.sc/document/document-attachment-dno4cb12kf6knuak.pdf', 'assets/images/document.jpg', NULL, 'Grit_Contract.pdf', 'document', '2020-09-17 08:48:39.120378+00:00', NULL, 29, 2, 4),
	(4, 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb12kf6kq739-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb12kf6kq739-thumb.jpg', NULL, NULL, 'photo', '2020-09-17 08:50:29.323685+00:00', NULL, 30, 2, 4),
	(5, 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb12kf6to5in-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-attachment-dno4cb12kf6to5in-thumb.jpg', NULL, NULL, 'photo', '2020-09-17 13:00:50.510402+00:00', NULL, 36, 10, 15),
	(6, 'https://minio.staging.grit.sc/photo/photo-attachment-1rtht11kfazjbst-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-attachment-1rtht11kfazjbst-thumb.jpg', NULL, NULL, 'photo', '2020-09-20 10:56:07.558862+00:00', NULL, 41, 2, 6);

SELECT setval('t_notifications_id_seq', 118, false);

INSERT INTO t_notifications (id, action, content_display_name, content_type, read, created, "textId", "photoId", "videoId", "articleId", "productId", "serviceId", "productReviewId", "serviceReviewId", "sellerReviewId", "issuerId", "ownerId", "productVariantId") VALUES
	(1, 'liked', 'photo', 'photo', true, '2020-08-20 10:42:00.369+00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 4, NULL),
	(2, 'liked', 'photo', 'photo', true, '2020-08-23 09:03:16.573+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 5, NULL),
	(3, 'follows', NULL, NULL, true, '2020-08-23 09:34:20.775+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 6, NULL),
	(4, 'follows', NULL, NULL, true, '2020-08-23 09:34:48.507+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 7, NULL),
	(5, 'follows', NULL, NULL, true, '2020-08-23 09:36:10.068+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(6, 'liked', 'photo', 'photo', true, '2020-08-23 09:36:29.812+00:00', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(7, 'commented', 'text', 'text', true, '2020-08-23 09:37:06.497+00:00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(8, 'commented', 'text', 'text', true, '2020-08-23 09:37:08.924+00:00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(9, 'commented', 'text', 'text', true, '2020-08-23 09:37:09.876+00:00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(10, 'commented', 'text', 'text', true, '2020-08-23 09:37:10.346+00:00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(11, 'commented', 'text', 'text', true, '2020-08-23 09:37:10.678+00:00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(12, 'liked', 'photo', 'photo', true, '2020-08-23 09:37:42.963+00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(13, 'follows', NULL, NULL, true, '2020-08-23 10:13:59.428+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 5, NULL),
	(14, 'follows', NULL, NULL, true, '2020-08-23 11:08:51.428+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 6, NULL),
	(15, 'follows', NULL, NULL, true, '2020-08-23 11:09:02.283+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 7, NULL),
	(16, 'liked', 'video', 'video', true, '2020-08-23 18:34:56.351+00:00', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 6, 7, NULL),
	(17, 'commented', 'video', 'video', true, '2020-08-23 18:35:13.377+00:00', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 6, 7, NULL),
	(18, 'commented', 'video', 'video', true, '2020-08-23 18:35:15.034+00:00', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 6, 7, NULL),
	(19, 'follows', NULL, NULL, true, '2020-08-23 18:36:23.75+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 8, NULL),
	(20, 'follows', NULL, NULL, true, '2020-08-23 18:36:35.035+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 8, NULL),
	(21, 'follows', NULL, NULL, true, '2020-08-24 07:55:18.885+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 6, NULL),
	(22, 'liked', 'text', 'text', true, '2020-08-24 08:02:00.663+00:00', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 8, NULL),
	(23, 'liked', 'photo', 'photo', true, '2020-08-25 06:18:50.316+00:00', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 9, NULL),
	(24, 'liked', 'photo', 'photo', true, '2020-08-25 06:18:53.823+00:00', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 9, NULL),
	(25, 'commented', 'photo', 'photo', true, '2020-08-25 06:19:11.263+00:00', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 9, NULL),
	(26, 'commented', 'photo', 'photo', true, '2020-08-25 06:21:02.085+00:00', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 9, NULL),
	(27, 'follows', NULL, NULL, true, '2020-08-25 06:22:05.126+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 5, NULL),
	(28, 'liked', 'photo', 'photo', true, '2020-08-25 06:22:07.086+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(29, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:18.295+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(30, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:18.88+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(31, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:19.165+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(32, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:19.89+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(33, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:19.892+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(34, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:21.866+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(35, 'commented', 'photo', 'photo', true, '2020-08-25 06:22:21.962+00:00', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 5, NULL),
	(36, 'liked', 'article', 'article', true, '2020-08-25 06:30:24.766+00:00', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(37, 'commented', 'article', 'article', true, '2020-08-25 06:30:41.118+00:00', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(38, 'commented', 'article', 'article', true, '2020-08-25 06:30:43.172+00:00', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(39, 'liked', 'article', 'article', true, '2020-08-25 06:45:29.599+00:00', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 6, 8, NULL),
	(40, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:06.197+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(41, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:08.758+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(42, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:11.094+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(43, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:11.16+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(44, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:11.192+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(45, 'commented', 'photo', 'photo', true, '2020-08-25 07:03:11.36+00:00', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 8, NULL),
	(46, 'commented', 'photo', 'photo', true, '2020-08-28 09:13:57.089+00:00', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 15, NULL),
	(47, 'follows', NULL, NULL, true, '2020-08-28 09:15:59.166+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 15, NULL),
	(48, 'liked', 'photo', 'photo', true, '2020-08-28 09:47:29.216+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(49, 'liked', 'photo', 'photo', true, '2020-08-28 09:47:49.71+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(50, 'liked', 'photo', 'photo', true, '2020-08-28 09:47:57.964+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(51, 'liked', 'photo', 'photo', true, '2020-08-28 09:47:59.276+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(52, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:01.103+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(53, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:02.362+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(54, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:07.003+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(55, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:07.672+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(56, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:11.566+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(57, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:11.799+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(58, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:11.801+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(59, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:11.989+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(60, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.301+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(61, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.303+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(62, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.377+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(63, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.366+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(64, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.383+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(65, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.476+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(66, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.501+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(67, 'liked', 'photo', 'photo', true, '2020-08-28 09:48:24.563+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(68, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:36.673+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(69, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:37.16+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(70, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:53.546+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(71, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.147+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(72, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.478+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(73, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.634+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(74, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.722+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(75, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.737+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(76, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:54.734+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(77, 'liked', 'photo', 'photo', true, '2020-08-28 09:49:55.276+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(78, 'commented', 'photo', 'photo', true, '2020-08-28 09:51:10.195+00:00', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(79, 'ordered', 'product', 'product', true, '2020-09-03 10:19:41.47+00:00', NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, 4, 6, 1),
	(80, 'follows', NULL, NULL, true, '2020-09-03 12:47:35.598+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 6, NULL),
	(81, 'cancelled', 'order', 'product', true, '2020-09-05 07:45:07.303+00:00', NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, 4, 6, NULL),
	(82, 'mentioned', 'article', 'article', true, '2020-09-11 09:59:51.717+00:00', NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 13, 4, NULL),
	(83, 'follows', NULL, NULL, true, '2020-09-13 16:09:57.657+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 14, NULL),
	(84, 'liked', 'photo', 'photo', true, '2020-09-13 16:10:06.788+00:00', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 14, NULL),
	(85, 'tagged', 'photo', 'photo', true, '2020-09-16 10:11:33.861+00:00', NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 6, NULL),
	(86, 'mentioned', 'photo', 'photo', true, '2020-09-16 10:14:11.251+00:00', NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 6, NULL),
	(87, 'commented', 'photo', 'photo', true, '2020-09-16 13:24:34.509+00:00', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(88, 'replied', 'comment', 'photo', true, '2020-09-16 13:24:51.474+00:00', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(89, 'commented', 'photo', 'photo', true, '2020-09-16 13:36:36.726+00:00', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(90, 'commented', 'photo', 'photo', true, '2020-09-16 13:37:04.402+00:00', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(91, 'commented', 'article', 'article', true, '2020-09-16 13:40:27.326+00:00', NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(92, 'commented', 'video', 'video', true, '2020-09-16 13:40:53.485+00:00', NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(93, 'liked', 'video', 'video', true, '2020-09-16 13:41:22.373+00:00', NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 13, 14, NULL),
	(94, 'liked', 'photo', 'photo', true, '2020-09-16 18:46:05.722+00:00', NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(95, 'liked', 'photo', 'photo', true, '2020-09-16 18:46:08.614+00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 14, NULL),
	(96, 'commented', 'photo', 'photo', true, '2020-09-16 18:50:24.507+00:00', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(97, 'liked', 'photo', 'photo', true, '2020-09-16 18:51:25.19+00:00', NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 18, NULL),
	(98, 'commented', 'photo', 'photo', true, '2020-09-16 18:51:51.381+00:00', NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 18, NULL),
	(99, 'follows', NULL, NULL, true, '2020-09-16 18:52:05.869+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 18, NULL),
	(100, 'follows', NULL, NULL, true, '2020-09-18 08:59:35.496+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 14, NULL);

INSERT INTO t_notifications (id, action, content_display_name, content_type, read, created, "textId", "photoId", "videoId", "articleId", "productId", "serviceId", "productReviewId", "serviceReviewId", "sellerReviewId", "issuerId", "ownerId", "productVariantId") VALUES
	(101, 'follows', NULL, NULL, true, '2020-09-18 09:04:23.335+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 13, NULL),
	(102, 'commented', 'photo', 'photo', true, '2020-09-20 07:20:21.391+00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(103, 'commented', 'photo', 'photo', true, '2020-09-20 07:23:01.515+00:00', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, NULL),
	(104, 'ordered', 'product', 'product', true, '2020-09-20 10:40:18.95+00:00', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, 20, 20, 13),
	(105, 'booked', 'service', 'service', true, '2020-09-20 10:59:19.446+00:00', NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 6, 4, NULL),
	(106, 'booked', 'service', 'service', true, '2020-09-20 10:59:23.592+00:00', NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 6, 4, NULL),
	(107, 'booked', 'service', 'service', true, '2020-09-20 11:01:35.591+00:00', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 11, 6, NULL),
	(108, 'booked', 'service', 'service', true, '2020-09-20 11:01:38.042+00:00', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 11, 6, NULL),
	(109, 'booked', 'service', 'service', true, '2020-09-20 11:01:40.337+00:00', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 11, 6, NULL),
	(110, 'liked', 'photo', 'photo', true, '2020-09-21 10:33:46.631+00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 14, NULL),
	(111, 'liked', 'photo', 'photo', true, '2020-09-21 10:34:07.809+00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 14, NULL),
	(112, 'commented', 'photo', 'photo', true, '2020-09-21 10:35:21.375+00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 14, NULL),
	(113, 'commented', 'photo', 'photo', true, '2020-09-21 10:35:46.253+00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 14, NULL),
	(114, 'follows', NULL, NULL, true, '2020-09-21 15:04:08.834+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 20, NULL),
	(115, 'ordered', 'product', 'product', false, '2020-09-22 09:51:21.637+00:00', NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, 9, 4, 1),
	(116, 'shipped', 'product', 'product', false, '2020-09-22 09:56:42.601+00:00', NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, 4, 9, NULL),
	(117, 'booked', 'service', 'service', false, '2020-09-22 12:15:28.727+00:00', NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 9, 4, NULL);

SELECT setval('t_order_id_seq', 4, false);

INSERT INTO t_order (id, total_price, shipping_price, fee, refund_amount, amount_card, amount_wallet, payment_id, payment_status, payment_type, payment_date, is_wallet, pay_in_id, pay_in_status, pay_in_date, pay_in_refund_status, pay_in_refund_date, pay_out_status, pay_out_date, pay_out_refund_status, pay_out_refund_date, transaction, shipping_transfer_id, transfer_status, transfer_date, who_cancelled, when_cancelled, cancel_reason, cancel_description, return_reason, return_description, paid, cancelled, refunded, shipped, received, returned, created, updated, buyer_id, seller_id, delivery_address_id, billing_address_id) VALUES
	(1, 12, 0, 0.36, NULL, 1200, 0, 87022634, 'pending', 'card', NULL, false, '87023087', NULL, '2020-09-03 10:19:40.030394+00:00', NULL, NULL, NULL, '2020-09-03 10:19:40.030394+00:00', NULL, NULL, NULL, NULL, NULL, NULL, 'buyer', 'before', NULL, NULL, NULL, NULL, '2020-09-03 10:19:40.022+00:00', '2020-09-05 07:45:04.133+00:00', NULL, NULL, NULL, NULL, '2020-09-03 10:19:40.030394+00:00', NULL, 4, 6, 6, 6),
	(2, 54.12, 3, 1.6236, NULL, 5412, 0, 88166229, 'pending', 'card', NULL, false, '88166231', NULL, '2020-09-20 10:40:17.721642+00:00', NULL, NULL, NULL, '2020-09-20 10:40:17.721642+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 10:40:17.718+00:00', NULL, NULL, NULL, NULL, NULL, '2020-09-20 10:40:17.721642+00:00', NULL, 20, 20, 16, 16),
	(3, 20, 10, 0.6, NULL, 2000, 0, 88305583, 'pending', 'card', NULL, false, '88305603', NULL, '2020-09-22 09:51:20.52948+00:00', NULL, NULL, NULL, '2020-09-22 09:51:20.52948+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-22 09:51:20.526+00:00', NULL, NULL, '2020-09-22 09:56:40.836+00:00', NULL, NULL, '2020-09-22 09:51:20.52948+00:00', NULL, 9, 4, 19, 19);

SELECT setval('t_order_product_id_seq', 4, false);

INSERT INTO t_order_product (id, quantity, collect, price, shipping_price, payment_id, reason, who_cancelled, when_cancelled, cancel_reason, cancel_description, return_reason, return_description, shipped, collected, canceled, returned, received, refunded, created, updated, transfer_id, transfer_status, transfer_date, pay_in_id, reviewed, order_id, product_id, product_variant_id, return_shipping_cost) VALUES
	(1, 1, true, 12, 0, NULL, NULL, 'buyer', 'before', NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 07:45:04.133+00:00', NULL, NULL, '2020-09-05 07:45:04.133+00:00', '2020-09-03 10:19:40.416973+00:00', NULL, NULL, NULL, NULL, NULL, NULL, 1, 5, NULL, NULL),
	(2, 4, false, 12.78, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-20 10:40:17.930262+00:00', NULL, NULL, NULL, NULL, NULL, NULL, 2, 25, 13, NULL),
	(3, 1, false, 10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-22 09:56:40.595+00:00', NULL, NULL, NULL, NULL, NULL, '2020-09-22 09:51:20.715398+00:00', NULL, 88306584, NULL, NULL, NULL, NULL, 3, 20, NULL, NULL);

SELECT setval('t_photo_annotation_id_seq', 2, false);

INSERT INTO t_photo_annotation (id, x, y, target_id, photo_id) VALUES
	(1, (-305), (-88), 6, 18);

SELECT setval('t_photo_comment_like_id_seq', 4, false);

INSERT INTO t_photo_comment_like (id, created, updated, user_id, comment_id, reply_id, photo_id) VALUES
	(1, '2020-08-28 09:14:16.695102+00:00', NULL, 13, 20, NULL, 10);

SELECT setval('t_photo_like_id_seq', 54, false);

INSERT INTO t_photo_like (id, created, updated, user_id, photo_id) VALUES
	(3, '2020-08-20 09:45:19.444838+00:00', NULL, 4, 1),
	(4, '2020-08-20 10:42:00.101547+00:00', NULL, 3, 1),
	(5, '2020-08-23 09:03:15.997285+00:00', NULL, 6, 2),
	(6, '2020-08-23 09:17:51.654325+00:00', NULL, 6, 5),
	(7, '2020-08-23 09:36:29.683907+00:00', NULL, 6, 3),
	(8, '2020-08-23 09:37:42.730118+00:00', NULL, 6, 1),
	(10, '2020-08-25 06:18:53.689412+00:00', NULL, 11, 8),
	(11, '2020-08-25 06:22:06.908465+00:00', NULL, 11, 2),
	(14, '2020-08-26 12:01:47.847368+00:00', NULL, 5, 2),
	(44, '2020-08-28 09:49:52.01322+00:00', NULL, 13, 11),
	(45, '2020-09-13 16:10:06.461547+00:00', NULL, 6, 13),
	(46, '2020-09-16 18:46:05.49567+00:00', NULL, 6, 18),
	(47, '2020-09-16 18:46:07.9589+00:00', NULL, 6, 21),
	(48, '2020-09-16 18:51:25.043608+00:00', NULL, 6, 17),
	(49, '2020-09-20 07:38:58.223665+00:00', NULL, 6, 16);

SELECT setval('t_photo_view_id_seq', 45, false);

INSERT INTO t_photo_view (id, created, user_id, photo_id) VALUES
	(1, '2020-08-20 09:40:03.145831+00:00', 4, 1),
	(2, '2020-08-20 10:41:52.981453+00:00', 3, 1),
	(3, '2020-08-23 08:44:17.10773+00:00', 6, 3),
	(4, '2020-08-23 09:03:10.385382+00:00', 6, 2),
	(5, '2020-08-23 09:03:34.858659+00:00', 6, 1),
	(6, '2020-08-23 09:10:24.226863+00:00', 6, 5),
	(7, '2020-08-23 09:38:57.148188+00:00', 7, 5),
	(8, '2020-08-23 14:52:41.808268+00:00', 8, 3),
	(9, '2020-08-23 14:52:50.065188+00:00', 8, 2),
	(10, '2020-08-24 08:17:24.988721+00:00', 4, 5),
	(11, '2020-08-24 08:17:44.48709+00:00', 4, 2),
	(12, '2020-08-24 10:28:47.473637+00:00', 9, 6),
	(13, '2020-08-24 10:47:18.072839+00:00', 9, 7),
	(14, '2020-08-24 12:52:47.161312+00:00', 9, 8),
	(15, '2020-08-25 06:13:24.299662+00:00', 8, 9),
	(16, '2020-08-25 06:18:47.960154+00:00', 8, 8),
	(17, '2020-08-25 06:22:27.055898+00:00', 8, 5),
	(18, '2020-08-25 06:45:18.06879+00:00', 6, 9),
	(19, '2020-08-26 12:01:04.079187+00:00', 5, 2),
	(20, '2020-08-28 09:13:50.977071+00:00', 13, 10),
	(21, '2020-08-28 09:47:26.662013+00:00', 13, 11),
	(22, '2020-08-28 10:49:29.764722+00:00', 15, 10),
	(23, '2020-08-31 13:55:36.744141+00:00', 14, 11),
	(24, '2020-08-31 14:22:34.867011+00:00', 14, 14),
	(25, '2020-08-31 14:22:35.050945+00:00', 14, 13),
	(26, '2020-09-01 11:41:01.746232+00:00', 4, 3),
	(27, '2020-09-10 13:36:15.270606+00:00', 4, 15),
	(28, '2020-09-10 18:21:37.444649+00:00', 4, 13),
	(29, '2020-09-13 16:03:34.802541+00:00', 6, 15),
	(30, '2020-09-13 16:09:56.369408+00:00', 6, 13),
	(31, '2020-09-14 08:51:11.066819+00:00', 4, 16),
	(32, '2020-09-16 10:10:14.993729+00:00', 4, 18),
	(33, '2020-09-16 10:18:26.780229+00:00', 18, 17),
	(34, '2020-09-16 10:25:54.357058+00:00', 4, 19),
	(35, '2020-09-16 13:24:19.629552+00:00', 13, 13),
	(36, '2020-09-16 18:46:03.848989+00:00', 6, 21),
	(37, '2020-09-16 18:46:04.662045+00:00', 6, 16),
	(38, '2020-09-16 18:46:04.778963+00:00', 6, 18),
	(39, '2020-09-16 18:51:23.669174+00:00', 6, 17),
	(40, '2020-09-18 08:06:01.205991+00:00', 4, 14),
	(41, '2020-09-18 08:11:52.21895+00:00', 4, 21),
	(42, '2020-09-18 08:59:05.955073+00:00', 20, 21),
	(43, '2020-09-18 10:21:02.306965+00:00', 5, 11),
	(44, '2020-09-21 13:48:31.422264+00:00', 18, 18);

INSERT INTO t_product_variant_t_product_photos ("variantId", "photoId", "order", "productId") VALUES
	(1, 5, 0, 2),
	(1, 6, 1, 2),
	(1, 7, 2, 2),
	(2, 8, 0, 2),
	(2, 9, 1, 2),
	(2, 10, 2, 2),
	(3, 30, 0, 23),
	(4, 31, 0, 23),
	(5, 30, 0, 23),
	(5, 31, 1, 23),
	(6, 38, 0, 25),
	(6, 39, 1, 25),
	(7, 40, 0, 25),
	(7, 41, 1, 25),
	(8, 38, 2, 25),
	(8, 40, 0, 25),
	(8, 41, 1, 25),
	(9, 38, 0, 25),
	(9, 39, 1, 25),
	(9, 40, 3, 25),
	(9, 42, 2, 25),
	(10, 38, 1, 25),
	(10, 41, 0, 25),
	(11, 39, 0, 25),
	(11, 41, 2, 25),
	(11, 42, 1, 25),
	(12, 40, 0, 25),
	(12, 41, 1, 25),
	(13, 38, 2, 25),
	(13, 39, 3, 25),
	(13, 40, 4, 25),
	(13, 41, 1, 25),
	(13, 42, 0, 25),
	(14, 43, 0, 26),
	(14, 44, 1, 26),
	(15, 43, 0, 26),
	(15, 44, 1, 26),
	(16, 43, 1, 26),
	(16, 44, 0, 26),
	(20, 46, 0, 27),
	(21, 46, 0, 27),
	(23, 46, 0, 27),
	(24, 47, 1, 28),
	(24, 48, 0, 28),
	(25, 47, 0, 28),
	(25, 48, 1, 28);

SELECT setval('t_profile_photo_id_seq', 10, false);

INSERT INTO t_profile_photo (id, "photoUrl", "thumbUrl", created, deleted, user_id) VALUES
	(1, 'https://minio.staging.grit.sc/photo/QJ9jbttBW-photo.jpg', 'https://minio.staging.grit.sc/photo/QJ9jbttBW-thumb.jpg', '2020-08-20 11:04:30.057275+00:00', '2020-08-20 11:04:30+00:00', 3),
	(2, 'https://minio.staging.grit.sc/photo/gbFKmfVMf-photo.jpg', 'https://minio.staging.grit.sc/photo/gbFKmfVMf-thumb.jpg', '2020-08-23 08:51:01.576202+00:00', '2020-08-23 08:51:01+00:00', 6),
	(3, 'https://minio.staging.grit.sc/photo/ZDR9B4avm-photo.jpg', 'https://minio.staging.grit.sc/photo/ZDR9B4avm-thumb.jpg', '2020-09-01 11:41:26.667475+00:00', '2020-09-01 11:41:26+00:00', 4),
	(4, 'https://minio.staging.grit.sc/photo/Sb3GViTkC-photo.jpg', 'https://minio.staging.grit.sc/photo/Sb3GViTkC-thumb.jpg', '2020-09-05 10:10:57.167261+00:00', '2020-09-05 10:10:57+00:00', 4),
	(5, 'https://minio.staging.grit.sc/photo/AMWugVS8f-photo.jpg', 'https://minio.staging.grit.sc/photo/AMWugVS8f-thumb.jpg', '2020-09-05 10:11:25.349809+00:00', '2020-09-05 10:11:25+00:00', 4),
	(6, 'https://minio.staging.grit.sc/photo/g0-Vr58Oo-photo.jpg', 'https://minio.staging.grit.sc/photo/g0-Vr58Oo-thumb.jpg', '2020-09-05 10:11:42.474029+00:00', '2020-09-05 10:11:42+00:00', 4),
	(7, 'https://minio.staging.grit.sc/photo/jl8GFI0FF-photo.jpg', 'https://minio.staging.grit.sc/photo/jl8GFI0FF-thumb.jpg', '2020-09-05 10:11:56.722288+00:00', '2020-09-05 10:11:56+00:00', 4),
	(8, 'https://minio.staging.grit.sc/photo/o5lnnncR2-photo.jpg', 'https://minio.staging.grit.sc/photo/o5lnnncR2-thumb.jpg', '2020-09-07 14:48:24.438902+00:00', '2020-09-07 14:48:24+00:00', 9),
	(9, 'https://minio.staging.grit.sc/photo/VPrWpLwcL-photo.jpg', 'https://minio.staging.grit.sc/photo/VPrWpLwcL-thumb.jpg', '2020-09-20 11:21:28.42257+00:00', '2020-09-20 11:21:28+00:00', 6);

SELECT setval('t_report_category_id_seq', 7, false);

INSERT INTO t_report_category (id, name) VALUES
	(1, 'Spam'),
	(2, 'Insult'),
	(3, 'Violence'),
	(4, 'Pornography'),
	(5, 'Terrorism'),
	(6, 'Infringement');

SELECT setval('t_report_id_seq', 3, false);

INSERT INTO t_report (id, type, description, status, created, updated, target_id, user_id, category_id) VALUES
	(1, 'product', NULL, 'open', '2020-08-23 09:59:30.640249+00:00', '2020-08-23 09:59:30.640249+00:00', 5, 7, 1),
	(2, 'photo', NULL, 'open', '2020-08-23 10:15:25.469627+00:00', '2020-08-23 10:15:25.469627+00:00', 5, 8, 1);

SELECT setval('t_report_type_id_seq', 10, false);

INSERT INTO t_report_type (id, name) VALUES
	(1, 'User'),
	(2, 'Text'),
	(3, 'Photo'),
	(4, 'Video'),
	(5, 'Article'),
	(6, 'Product'),
	(7, 'Service'),
	(8, 'Comment'),
	(9, 'Review');

SELECT setval('t_return_order_reason_id_seq', 10, false);

INSERT INTO t_return_order_reason (id, name) VALUES
	(1, e'I don\U00002019t like the product'),
	(2, 'Poor quality'),
	(3, 'Price-performance'),
	(4, 'Wrong product'),
	(5, 'Product damaged'),
	(6, 'Product defective'),
	(7, 'Delivery incomplete'),
	(8, 'Delivered too late'),
	(9, 'Other');

SELECT setval('t_review_photo_id_seq', 1, false);

SELECT setval('t_service_schedule_id_seq', 7, false);

INSERT INTO t_service_schedule (id, duration, break, created, updated, deleted, service_id) VALUES
	(1, NULL, NULL, '2020-08-20 11:01:43.657362+00:00', NULL, NULL, 1),
	(2, '02:00', '00:00', '2020-08-23 19:00:54.610868+00:00', NULL, NULL, 2),
	(3, '02:00', '00:00', '2020-09-13 16:21:49.909239+00:00', NULL, NULL, 3),
	(4, '01:30', '00:30', '2020-09-18 10:20:31.099042+00:00', NULL, NULL, 4),
	(5, '01:30', '00:30', '2020-09-18 11:37:34.973621+00:00', NULL, NULL, 5),
	(6, '01:30', '01:00', '2020-09-18 11:42:07.287835+00:00', NULL, NULL, 6);

SELECT setval('t_schedule_date_id_seq', 2, false);

INSERT INTO t_schedule_date (id, "from", "to", bookings, created, updated, deleted, "scheduleId") VALUES
	(1, '2020-08-20 11:00:27+00:00', '2020-08-24 11:00:30+00:00', NULL, '2020-08-20 11:01:43.720846+00:00', NULL, NULL, 1);

SELECT setval('t_schedule_days_id_seq', 17, false);

INSERT INTO t_schedule_days (id, "from", "to", created, updated, deleted, "scheduleId", "dayId") VALUES
	(1, '08:00:00', '10:00:00', '2020-08-23 19:00:54.685999+00:00', NULL, NULL, 2, 1),
	(2, '17:00:00', '19:00:00', '2020-08-23 19:00:54.76269+00:00', NULL, NULL, 2, 2),
	(3, '15:00:00', '17:00:00', '2020-08-23 19:00:54.80029+00:00', NULL, NULL, 2, 3),
	(4, '09:00:00', '11:00:00', '2020-08-23 19:00:54.875305+00:00', NULL, NULL, 2, 4),
	(5, '19:00:00', '21:00:00', '2020-08-23 19:00:54.917984+00:00', NULL, NULL, 2, 5),
	(6, '10:00:00', '16:00:00', '2020-09-13 16:21:49.963944+00:00', NULL, NULL, 3, 1),
	(7, '12:00:00', '18:00:00', '2020-09-13 16:21:50.046076+00:00', NULL, NULL, 3, 2),
	(8, '08:30:00', '14:30:00', '2020-09-13 16:21:50.085626+00:00', NULL, NULL, 3, 3),
	(9, '11:00:00', '16:30:00', '2020-09-13 16:21:50.156859+00:00', NULL, NULL, 3, 4),
	(10, '12:00:00', '15:30:00', '2020-09-13 16:21:50.239658+00:00', NULL, NULL, 3, 5),
	(11, '11:00:00', '13:00:00', '2020-09-18 10:20:31.289844+00:00', NULL, NULL, 4, 1),
	(12, '15:30:00', '17:00:00', '2020-09-18 10:20:31.498628+00:00', NULL, NULL, 4, 1),
	(13, '08:00:00', '15:30:00', '2020-09-18 10:20:31.538755+00:00', NULL, NULL, 4, 3),
	(14, '11:00:00', '15:00:00', '2020-09-18 11:37:35.061102+00:00', NULL, NULL, 5, 1),
	(15, '19:00:00', '21:00:00', '2020-09-18 11:37:35.203478+00:00', NULL, NULL, 5, 1),
	(16, '02:00:00', '11:30:00', '2020-09-18 11:42:07.338468+00:00', NULL, NULL, 6, 1);

SELECT setval('t_schedule_days_intervals_id_seq', 60, false);

INSERT INTO t_schedule_days_intervals (id, "from", "to", "scheduleByDayId") VALUES
	(1, '08:00:00', '10:00:00', 1),
	(2, '15:00:00', '17:00:00', 3),
	(3, '09:00:00', '11:00:00', 4),
	(4, '19:00:00', '21:00:00', 5),
	(5, '17:00:00', '19:00:00', 2),
	(18, '10:00:00', '12:00:00', 6),
	(19, '12:00:00', '14:00:00', 6),
	(20, '14:00:00', '16:00:00', 6),
	(21, '08:30:00', '10:30:00', 8),
	(22, '10:30:00', '12:30:00', 8),
	(23, '12:30:00', '14:30:00', 8),
	(24, '11:00:00', '13:00:00', 9),
	(25, '13:00:00', '15:00:00', 9),
	(26, '12:00:00', '14:00:00', 10),
	(27, '12:00:00', '14:00:00', 7),
	(28, '14:00:00', '16:00:00', 7),
	(29, '16:00:00', '18:00:00', 7),
	(30, '08:00:00', '09:30:00', 13),
	(31, '10:00:00', '11:30:00', 13),
	(32, '12:00:00', '13:30:00', 13),
	(33, '14:00:00', '15:30:00', 13),
	(35, '11:00:00', '12:30:00', 14),
	(57, '02:00:00', '03:30:00', 16),
	(58, '04:30:00', '06:00:00', 16),
	(59, '07:00:00', '08:30:00', 16);

SELECT setval('t_search_dictionary_id_seq', 94, false);

INSERT INTO t_search_dictionary (id, word, type) VALUES
	(1, 'black', 'color'),
	(2, 'white', 'color'),
	(3, 'grey', 'color'),
	(4, 'red', 'color'),
	(5, 'blue', 'color'),
	(6, 'yellow', 'color'),
	(7, 'purple', 'color'),
	(8, 'green', 'color'),
	(9, 'brown', 'color'),
	(10, 'khaki', 'color'),
	(11, 'cotton', 'material'),
	(12, 'acrylic', 'material'),
	(13, 'alloy', 'material'),
	(14, 'aluminum', 'material'),
	(15, 'brass', 'material'),
	(16, 'brick', 'material'),
	(17, 'bronze', 'material'),
	(18, 'carbon', 'material'),
	(19, 'cardboard', 'material'),
	(20, 'cast', 'material'),
	(21, 'iron', 'material'),
	(22, 'cement', 'material'),
	(23, 'ceramics', 'material'),
	(24, 'copper', 'material'),
	(25, 'diamond', 'material'),
	(26, 'epoxy', 'material'),
	(27, 'fiber', 'material'),
	(28, 'fiberglass', 'material'),
	(29, 'glass', 'material'),
	(30, 'glue', 'material'),
	(31, 'gold', 'material'),
	(32, 'leather', 'material'),
	(33, 'linen', 'material'),
	(34, 'nylon', 'material'),
	(35, 'paper', 'material'),
	(36, 'polyester', 'material'),
	(37, 'rubber', 'material'),
	(38, 'sand', 'material'),
	(39, 'silica', 'material'),
	(40, 'silver', 'material'),
	(41, 'skin', 'material'),
	(42, 'steel', 'material'),
	(43, 'stone', 'material'),
	(44, 'titanium', 'material'),
	(45, 'vinyl', 'material'),
	(46, 'viscose', 'material'),
	(47, 'wood', 'material'),
	(48, 'wool', 'material'),
	(49, 'denim', 'material'),
	(50, 'vanilla', 'flavor'),
	(51, 'chocolate', 'flavor'),
	(52, 'mint', 'flavor'),
	(53, 'strawberry', 'flavor'),
	(54, 'banana', 'flavor'),
	(55, 'apple', 'flavor'),
	(56, 'orange', 'flavor'),
	(57, 'lemon', 'flavor'),
	(58, 'honey', 'flavor'),
	(59, 'coffee', 'flavor'),
	(60, 'Tropical', 'color'),
	(61, 'Stripped Planks', 'color'),
	(62, 'Black', 'color'),
	(63, 'Cotton', 'material'),
	(64, 'White', 'color'),
	(71, 'Silk', 'material');

SELECT setval('t_service_photo_id_seq', 17, false);

INSERT INTO t_service_photo (id, link, thumb, "index", created, updated, deleted, service_id) VALUES
	(1, 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2p384u-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-1n4k11ke2p384u-thumb.jpg', 0, '2020-08-20 11:01:43.600194+00:00', '2020-08-20 11:01:48.355+00:00', NULL, 1),
	(2, 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke7gj1j5-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-5eu4v21cke7gj1j5-thumb.jpg', 0, '2020-08-23 19:00:54.55841+00:00', '2020-08-23 19:01:00.852+00:00', NULL, 2),
	(3, 'https://minio.staging.grit.sc/photo/photo-igncc1ckf1b3brr-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-igncc1ckf1b3brr-thumb.jpg', 0, '2020-09-13 16:21:49.835652+00:00', '2020-09-13 16:21:55.271+00:00', NULL, 3),
	(4, 'https://minio.staging.grit.sc/photo/photo-lb11kf83e4s9-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf83e4s9-thumb.jpg', 0, '2020-09-18 10:20:30.79345+00:00', '2020-09-18 10:20:45.008+00:00', NULL, 4),
	(5, 'https://minio.staging.grit.sc/photo/photo-lb11kf83e7br-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf83e7br-thumb.jpg', 1, '2020-09-18 10:20:30.995905+00:00', '2020-09-18 10:20:48.183+00:00', NULL, 4),
	(6, 'https://minio.staging.grit.sc/photo/photo-lb11kf83e90g-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf83e90g-thumb.jpg', 2, '2020-09-18 10:20:31.061518+00:00', '2020-09-18 10:20:50.465+00:00', NULL, 4),
	(7, 'https://minio.staging.grit.sc/photo/photo-lb11kf865253-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf865253-thumb.jpg', 1, '2020-09-18 11:37:34.632335+00:00', '2020-09-18 11:37:41.13+00:00', NULL, 5),
	(8, 'https://minio.staging.grit.sc/photo/photo-lb11kf8652xs-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf8652xs-thumb.jpg', 3, '2020-09-18 11:37:34.70228+00:00', '2020-09-18 11:37:41.693+00:00', NULL, 5),
	(9, 'https://minio.staging.grit.sc/photo/photo-lb11kf865474-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf865474-thumb.jpg', 2, '2020-09-18 11:37:34.751533+00:00', '2020-09-18 11:37:43.254+00:00', NULL, 5),
	(10, 'https://minio.staging.grit.sc/photo/photo-lb11kf865n18-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf865n18-thumb.jpg', 0, '2020-09-18 11:37:59.503461+00:00', '2020-09-18 11:38:07.383+00:00', NULL, 5),
	(13, 'https://minio.staging.grit.sc/photo/photo-lb11kf86b0z5-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf86b0z5-thumb.jpg', 1, '2020-09-18 11:42:07.219772+00:00', '2020-09-18 11:42:19.002+00:00', NULL, 6),
	(15, 'https://minio.staging.grit.sc/photo/photo-lb11kf86dwvf-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf86dwvf-thumb.jpg', 2, '2020-09-18 11:44:25.235959+00:00', '2020-09-18 11:44:33.475+00:00', NULL, 6),
	(16, 'https://minio.staging.grit.sc/photo/photo-lb11kf86eyre-photo.jpg', 'https://minio.staging.grit.sc/photo/photo-lb11kf86eyre-thumb.jpg', 0, '2020-09-18 11:45:13.041286+00:00', '2020-09-18 11:45:22.704+00:00', NULL, 6);

SELECT setval('t_share_id_seq', 1, false);

SELECT setval('t_text_comment_like_id_seq', 3, false);

INSERT INTO t_text_comment_like (id, created, updated, user_id, comment_id, reply_id, text_id) VALUES
	(1, '2020-08-20 09:44:42.454835+00:00', NULL, 4, 1, NULL, 2),
	(2, '2020-08-20 09:44:53.979377+00:00', NULL, 4, NULL, 1, 2);

SELECT setval('t_text_like_id_seq', 7, false);

INSERT INTO t_text_like (id, created, updated, user_id, text_id) VALUES
	(1, '2020-08-20 09:35:45.122218+00:00', NULL, 4, 1),
	(2, '2020-08-20 09:44:59.941674+00:00', NULL, 4, 2),
	(3, '2020-08-24 08:02:00.481529+00:00', NULL, 6, 8),
	(6, '2020-09-10 14:56:53.436872+00:00', NULL, 4, 14);

SELECT setval('t_text_view_id_seq', 16, false);

INSERT INTO t_text_view (id, created, user_id, text_id) VALUES
	(1, '2020-08-20 09:35:38.336002+00:00', 4, 1),
	(2, '2020-08-20 09:43:51.390817+00:00', 4, 2),
	(3, '2020-08-20 09:55:37.855981+00:00', 4, 3),
	(4, '2020-08-23 09:36:48.769934+00:00', 6, 2),
	(5, '2020-08-23 18:48:32.691595+00:00', 6, 8),
	(6, '2020-08-24 10:28:47.772818+00:00', 9, 10),
	(7, '2020-08-24 13:49:38.547992+00:00', 8, 8),
	(8, '2020-09-07 10:41:07.720068+00:00', 4, 11),
	(9, '2020-09-08 17:05:12.25543+00:00', 6, 12),
	(10, '2020-09-08 17:05:12.589369+00:00', 6, 11),
	(11, '2020-09-08 19:13:00.101503+00:00', 4, 12),
	(12, '2020-09-09 11:51:01.097531+00:00', 18, 13),
	(13, '2020-09-10 13:36:15.917489+00:00', 4, 14),
	(14, '2020-09-13 16:03:35.163583+00:00', 6, 14),
	(15, '2020-09-16 18:46:05.09519+00:00', 6, 20);

SELECT setval('t_user_business_user_category_id_seq', 1, false);

SELECT setval('t_video_comment_like_id_seq', 1, false);

SELECT setval('t_video_like_id_seq', 4, false);

INSERT INTO t_video_like (id, created, updated, user_id, video_id) VALUES
	(1, '2020-08-23 18:34:55.800089+00:00', NULL, 6, 2),
	(2, '2020-09-16 13:41:22.088442+00:00', NULL, 13, 8),
	(3, '2020-09-20 11:16:48.091734+00:00', NULL, 6, 13);

SELECT setval('t_video_view_id_seq', 18, false);

INSERT INTO t_video_view (id, created, user_id, video_id) VALUES
	(1, '2020-08-23 09:59:29.616242+00:00', 6, 1),
	(2, '2020-08-23 09:59:29.899548+00:00', 6, 2),
	(3, '2020-08-23 14:54:54.907835+00:00', 8, 2),
	(4, '2020-08-24 07:45:14.139738+00:00', 6, 3),
	(5, '2020-09-10 13:36:15.089891+00:00', 4, 5),
	(6, '2020-09-15 09:14:28.643599+00:00', 4, 7),
	(7, '2020-09-16 13:40:44.067358+00:00', 13, 8),
	(8, '2020-09-16 18:46:03.915376+00:00', 6, 8),
	(9, '2020-09-17 14:02:24.944707+00:00', 5, 11),
	(10, '2020-09-17 14:07:47.761818+00:00', 5, 12),
	(11, '2020-09-17 14:10:04.773911+00:00', 20, 12),
	(12, '2020-09-18 08:06:19.604757+00:00', 4, 12),
	(13, '2020-09-18 08:06:48.482961+00:00', 4, 11),
	(14, '2020-09-20 07:26:44.021874+00:00', 6, 13),
	(15, '2020-09-20 15:29:57.151602+00:00', 4, 13),
	(16, '2020-09-20 16:03:13.74694+00:00', 4, 14),
	(17, '2020-09-21 14:53:16.686973+00:00', 14, 8);

SELECT setval('t_view_id_seq', 1, false);

ALTER TABLE t_business ADD CONSTRAINT "FK_6e870aac767cc2a0bbca1730050" FOREIGN KEY ("categoryId") REFERENCES t_category(id) ON DELETE RESTRICT;
ALTER TABLE t_business ADD CONSTRAINT "FK_d608f158f657779a0b20a04bd07" FOREIGN KEY ("deliveryAddressId") REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_business ADD CONSTRAINT "FK_e269995d4d6e50563a08a9016b6" FOREIGN KEY ("addressId") REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_address ADD CONSTRAINT "FK_dc133bee8ecf186bfcab7e0d2df" FOREIGN KEY ("personId") REFERENCES t_person(id) ON DELETE CASCADE;
ALTER TABLE t_address ADD CONSTRAINT "FK_ae3a39cc32d9b206c55fc9c4aa0" FOREIGN KEY ("businessId") REFERENCES t_business(id) ON DELETE CASCADE;
ALTER TABLE t_person ADD CONSTRAINT "FK_1b6f23f5feaf9294e1cb27a8f1c" FOREIGN KEY (address_id) REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_person ADD CONSTRAINT "FK_5f30eec8cabee18d383292cda3b" FOREIGN KEY ("deliveryAddressId") REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_account ADD CONSTRAINT "FK_a18399d9a2042373ee13fd4e593" FOREIGN KEY ("personId") REFERENCES t_person(id) ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE t_account ADD CONSTRAINT "FK_37b15b7df488f6f0daac2f8015c" FOREIGN KEY ("businessId") REFERENCES t_business(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_account ADD CONSTRAINT "FK_a9cf1e14d962521484050d231a1" FOREIGN KEY (role_id) REFERENCES t_roles(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE t_account ADD CONSTRAINT "FK_e015a7e38c56f4315214287c14e" FOREIGN KEY ("baseBusinessAccountId") REFERENCES t_account(id);
ALTER TABLE t_account_blacklist_t_account ADD CONSTRAINT "FK_fc056810fc2513bbbfc6419a0b5" FOREIGN KEY ("tAccountId_1") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_blacklist_t_account ADD CONSTRAINT "FK_fe827b9ed2028fba31442501b07" FOREIGN KEY ("tAccountId_2") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_shop_category ADD CONSTRAINT "FK_2d1323724d239cb787b0f4c5af7" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_product_photo ADD CONSTRAINT "FK_695941bdc0db4f13bba822f22ad" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_product ADD CONSTRAINT "FK_45054858963ac0a72830b18adf1" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_product ADD CONSTRAINT "FK_c4cb695880ad06d3e24b4c2fd46" FOREIGN KEY (business_id) REFERENCES t_business(id) ON DELETE CASCADE;
ALTER TABLE t_product ADD CONSTRAINT "FK_251ba14ed3702ab8bafa9ef91c0" FOREIGN KEY (shopcategory_id) REFERENCES t_shop_category(id) ON DELETE RESTRICT;
ALTER TABLE t_product ADD CONSTRAINT "FK_885547ba0f6e14a4c45486c1359" FOREIGN KEY (address_id) REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_product ADD CONSTRAINT "FK_e4c8164fb9c0e01953f1cbe9e1c" FOREIGN KEY (cover_id) REFERENCES t_product_photo(id);
ALTER TABLE t_product_review ADD CONSTRAINT "FK_660bd3d57a18e21acb61904b865" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_product_review ADD CONSTRAINT "FK_611cf5777fd1d9226f22bd5cbd9" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_product_review ADD CONSTRAINT "FK_1626973a7ecc572cb4d15e92ed0" FOREIGN KEY ("videoId") REFERENCES t_review_video(id);
ALTER TABLE t_account_product_reviews_likes_t_product_review ADD CONSTRAINT "FK_f711073fa7c4e10bdd686e0f207" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_product_reviews_likes_t_product_review ADD CONSTRAINT "FK_2dd67ea2bb4227c14aa8a990fe6" FOREIGN KEY ("tProductReviewId") REFERENCES t_product_review(id) ON DELETE CASCADE;
ALTER TABLE t_product_review_replies ADD CONSTRAINT "FK_e67b423683e651eb0615bfe3a2a" FOREIGN KEY (review_id) REFERENCES t_product_review(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_product_review_replies ADD CONSTRAINT "FK_f58f40ef57ec1a29fb74f22c7ae" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_product_review_replies ADD CONSTRAINT "FK_4689d99dc7eca19aac1aff89204" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_product_reviews_replies_likes_t_product_review_replies ADD CONSTRAINT "FK_e8ead629df496341580f7087fb2" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_product_reviews_replies_likes_t_product_review_replies ADD CONSTRAINT "FK_7a4955c2b07ad3f92a091de5c9b" FOREIGN KEY ("tProductReviewRepliesId") REFERENCES t_product_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_account_product_reviews_replies_t_product_review_replies ADD CONSTRAINT "FK_475b4f566c68581247dfcb2c305" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_product_reviews_replies_t_product_review_replies ADD CONSTRAINT "FK_c621ba8d0b19f9a08b8cec949a0" FOREIGN KEY ("tProductReviewRepliesId") REFERENCES t_product_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review ADD CONSTRAINT "FK_be93a478544b14262a99a8838b3" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review ADD CONSTRAINT "FK_8838197aabdabc1f72ce3b7c306" FOREIGN KEY (seller_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review ADD CONSTRAINT "FK_9231df22c5dc5c2006e3244f9b8" FOREIGN KEY ("videoId") REFERENCES t_review_video(id);
ALTER TABLE t_account_seller_reviews_likes_t_seller_review ADD CONSTRAINT "FK_66d4b247265510e63fdcc75bcbf" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_seller_reviews_likes_t_seller_review ADD CONSTRAINT "FK_83432966d9d78e0f4405badaab1" FOREIGN KEY ("tSellerReviewId") REFERENCES t_seller_review(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review_replies ADD CONSTRAINT "FK_a388eebedd1e25eaad6c49986fa" FOREIGN KEY (review_id) REFERENCES t_seller_review(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_seller_review_replies ADD CONSTRAINT "FK_0355fea30ef0ccbe1a868aa938d" FOREIGN KEY (seller_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review_replies ADD CONSTRAINT "FK_bf7c0d8714b18a237501e749246" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_seller_reviews_replies_likes_t_seller_review_replies ADD CONSTRAINT "FK_588ea502be4a7bdf2bd0f005ceb" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_seller_reviews_replies_likes_t_seller_review_replies ADD CONSTRAINT "FK_bb1b5027012679e7112cb220c9f" FOREIGN KEY ("tSellerReviewRepliesId") REFERENCES t_seller_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_account_seller_reviews_replies_t_seller_review_replies ADD CONSTRAINT "FK_4404e8d386f5547b36099aff5d2" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_seller_reviews_replies_t_seller_review_replies ADD CONSTRAINT "FK_1a2fa4a19071300cdee74de30b4" FOREIGN KEY ("tSellerReviewRepliesId") REFERENCES t_seller_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_service ADD CONSTRAINT "FK_d6b26007534e6761e1c4e65bcb7" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_service ADD CONSTRAINT "FK_b779a48fb2db76d52987c14c2e3" FOREIGN KEY (address_id) REFERENCES t_address(id) ON DELETE SET NULL;
ALTER TABLE t_service_review ADD CONSTRAINT "FK_1288cda092c439239c91d562484" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_service_review ADD CONSTRAINT "FK_4bce49de508fc298cac740a03b5" FOREIGN KEY (service_id) REFERENCES t_service(id);
ALTER TABLE t_service_review ADD CONSTRAINT "FK_dde0c7acb9a4fa3445d84dd53e8" FOREIGN KEY ("videoId") REFERENCES t_review_video(id);
ALTER TABLE t_account_service_reviews_likes_t_service_review ADD CONSTRAINT "FK_e9ca884fa9ad48df5fcd168c044" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_service_reviews_likes_t_service_review ADD CONSTRAINT "FK_cca6de899e6c57b6d90f05d6131" FOREIGN KEY ("tServiceReviewId") REFERENCES t_service_review(id) ON DELETE CASCADE;
ALTER TABLE t_service_review_replies ADD CONSTRAINT "FK_4484e7182b1b15f13b0470186a0" FOREIGN KEY (review_id) REFERENCES t_service_review(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_service_review_replies ADD CONSTRAINT "FK_c1d7e93610f3b3d62d5542ee5c8" FOREIGN KEY (service_id) REFERENCES t_service(id) ON DELETE CASCADE;
ALTER TABLE t_service_review_replies ADD CONSTRAINT "FK_7f95aa959f15274bbf145565d38" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_service_reviews_replies_likes_t_service_review_replies ADD CONSTRAINT "FK_80a0bacf97d740c40cd21c08c9d" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_service_reviews_replies_likes_t_service_review_replies ADD CONSTRAINT "FK_f1f1c69857942a5e141e9da8522" FOREIGN KEY ("tServiceReviewRepliesId") REFERENCES t_service_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_account_service_reviews_replies_t_service_review_replies ADD CONSTRAINT "FK_71fffd2c5ff9fd018bb4267314c" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_account_service_reviews_replies_t_service_review_replies ADD CONSTRAINT "FK_1283472915eb3aab7b4126dd7ca" FOREIGN KEY ("tServiceReviewRepliesId") REFERENCES t_service_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_article ADD CONSTRAINT "FK_9ac0a29f753ed5cac0d1a4321ba" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_article ADD CONSTRAINT "FK_d43809009ac48a652788386bea1" FOREIGN KEY (business_address) REFERENCES t_address(id);
ALTER TABLE t_article_comment ADD CONSTRAINT "FK_c5df8481acb3ec66783fda56c51" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_article_comment ADD CONSTRAINT "FK_d69e86b9ccd74f68e63e16bba4b" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_comment_replies ADD CONSTRAINT "FK_d5205061e115011e8efa4b75c2f" FOREIGN KEY (comment_id) REFERENCES t_article_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_comment_replies ADD CONSTRAINT "FK_2c3249b01bb62f3ffcd8e4ce59b" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_article_comment_replies ADD CONSTRAINT "FK_03da36fc98270a36927619e0349" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_comment_like ADD CONSTRAINT "FK_2f857dad4c7de1443478f2c7c7d" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_article_comment_like ADD CONSTRAINT "FK_fd9293bcde871e023e9c0c78e16" FOREIGN KEY (comment_id) REFERENCES t_article_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_comment_like ADD CONSTRAINT "FK_38c83e283f7e4754cef6a8c2b7c" FOREIGN KEY (reply_id) REFERENCES t_article_comment_replies(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_comment_like ADD CONSTRAINT "FK_fd58ff9a644e5fc49ab6113c9f4" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_like ADD CONSTRAINT "FK_00be0b022149db48d5a97921df4" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_article_like ADD CONSTRAINT "FK_9da4512fc30d1907eb3ac3916bf" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_article_photo ADD CONSTRAINT "FK_bd5f3c88dfe2de55f177f2b2a72" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE;
ALTER TABLE t_article_view ADD CONSTRAINT "FK_819087eceaa34ed8ffe82e3fb27" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_article_view ADD CONSTRAINT "FK_d5e33c2f327a3f8127d6b9fb55d" FOREIGN KEY (article_id) REFERENCES t_article(id);
ALTER TABLE t_bank ADD CONSTRAINT "FK_631acaea16abc66e7cf763f06ad" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_booking ADD CONSTRAINT "FK_fc481f354786482f37adadad680" FOREIGN KEY (buyer_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_booking ADD CONSTRAINT "FK_8996cbb5f77368677b8b426dee5" FOREIGN KEY (seller_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_booking ADD CONSTRAINT "FK_7ac87a8295c1e57a9ab49d7c25c" FOREIGN KEY (delivery_address_id) REFERENCES t_address(id) ON DELETE CASCADE;
ALTER TABLE t_booking ADD CONSTRAINT "FK_d0cc1171cbb9057a783f65710f1" FOREIGN KEY (billing_address_id) REFERENCES t_address(id) ON DELETE CASCADE;
ALTER TABLE t_booking ADD CONSTRAINT "FK_7b0c47906c6b219d60538c0bbb2" FOREIGN KEY (service_id) REFERENCES t_service(id) ON DELETE CASCADE;
ALTER TABLE t_booking_appointments ADD CONSTRAINT "FK_1eb29647cb75a44260748c5092b" FOREIGN KEY (booking_id) REFERENCES t_booking(id) ON DELETE CASCADE;
ALTER TABLE t_text ADD CONSTRAINT "FK_242e82f7c6adf503b79043e80b1" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_photo ADD CONSTRAINT "FK_f7442330e2a46235db02d165836" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_photo ADD CONSTRAINT "FK_f6d9d497516a33a35003796a37c" FOREIGN KEY (business_address) REFERENCES t_address(id);
ALTER TABLE t_video ADD CONSTRAINT "FK_e70f50a324682a7b52545a6e2b5" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_video ADD CONSTRAINT "FK_e01f6ac798813d598a91fa3930a" FOREIGN KEY (business_address) REFERENCES t_address(id);
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_e17dc82a08cd8da59f7e831330e" FOREIGN KEY (owner_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_7a20246b7dc1966d51222f1217d" FOREIGN KEY (account_id) REFERENCES t_account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_7f3d4605368f1bbe200805a21a8" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_e1f00b4dfa6f8cc34b524d2c22a" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_e991c5cbb90e41dd581e7e3ee5a" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_5786afa3f5f20f9a08bbc71346c" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_883b1491144e3c76eeb53643c55" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_bookmarks ADD CONSTRAINT "FK_0712cbce462ecd7770d968baa4c" FOREIGN KEY (service_id) REFERENCES t_service(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_product_variant ADD CONSTRAINT "FK_54cb1b5feee114a2a46f39cdc0a" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_cart_product ADD CONSTRAINT "FK_163dbcfd5a10909c6a9c55e7634" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_cart_product ADD CONSTRAINT "FK_bc5c7493732ee0ef7a76ae62f55" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_cart_product ADD CONSTRAINT "FK_0226892fb83f0e6f04fd7c07550" FOREIGN KEY (product_variant_id) REFERENCES t_product_variant(id) ON DELETE CASCADE;
ALTER TABLE t_message ADD CONSTRAINT "FK_7aef1713d0d8ae2d08ec0a69d92" FOREIGN KEY (reply_to) REFERENCES t_message(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_0623f6ce8190480372024845761" FOREIGN KEY (chatroom_id) REFERENCES t_chatroom(id) ON DELETE CASCADE;
ALTER TABLE t_message ADD CONSTRAINT "FK_285a93487e9c8f44eb9a141f420" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_message ADD CONSTRAINT "FK_0d3eff2cf377d3aeb81e9e24df9" FOREIGN KEY (shared_user_id) REFERENCES t_account(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_725da9ab95c73ad42ba931b448f" FOREIGN KEY (shared_text_id) REFERENCES t_text(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_d9c65b8d75d4ea1f4e60ca83939" FOREIGN KEY (shared_photo_id) REFERENCES t_photo(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_3dbc933de81bad609549823e5b0" FOREIGN KEY (shared_video_id) REFERENCES t_video(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_b9ba907c408c60674f1911c1a0d" FOREIGN KEY (shared_article_id) REFERENCES t_article(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_4d5288130018cd5bb414b0c9c3e" FOREIGN KEY (shared_product_id) REFERENCES t_product(id);
ALTER TABLE t_message ADD CONSTRAINT "FK_ffdef196f083b2c7d886abf646d" FOREIGN KEY (shared_service_id) REFERENCES t_service(id);
ALTER TABLE t_chatroom_user ADD CONSTRAINT "FK_923b7160e827046cf221f68122c" FOREIGN KEY (chatroom_id) REFERENCES t_chatroom(id) ON DELETE CASCADE;
ALTER TABLE t_chatroom_user ADD CONSTRAINT "FK_21566604d65d58adc527c2979bd" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_chatroom_user ADD CONSTRAINT "FK_88e695691b3ba95913a0cff17c6" FOREIGN KEY (last_read_message) REFERENCES t_message(id);
ALTER TABLE t_contact ADD CONSTRAINT "FK_ca821105d7986c98db6d39c3e4b" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_followings ADD CONSTRAINT "FK_b818092d1ced5a1910b392d1975" FOREIGN KEY ("accountId") REFERENCES t_account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_followings ADD CONSTRAINT "FK_e4b6ea262c5c80358090b82ac66" FOREIGN KEY ("followedAccountId") REFERENCES t_account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_followings_request ADD CONSTRAINT "FK_8bc4d5a28feeffcb9a8fc2c970a" FOREIGN KEY ("accountId") REFERENCES t_account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_followings_request ADD CONSTRAINT "FK_2fa247efdbc9a14f68e49fc9fb3" FOREIGN KEY ("followedAccountId") REFERENCES t_account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_ac74d00d3e335a645dd222cad1d" FOREIGN KEY (photo_id) REFERENCES t_photo(id);
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_729199c811532a10df28afcea36" FOREIGN KEY (video_id) REFERENCES t_video(id);
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_9485ccfbc9b48f6c349307e243c" FOREIGN KEY (article_id) REFERENCES t_article(id);
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_9e418db5f56016f946801f7740a" FOREIGN KEY (text_id) REFERENCES t_text(id);
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_37562e8b412412cf04a26122f8b" FOREIGN KEY (product_id) REFERENCES t_product(id);
ALTER TABLE t_hashtag ADD CONSTRAINT "FK_a5c447ada045b8ead93a20ff5ae" FOREIGN KEY (service_id) REFERENCES t_service(id);
ALTER TABLE t_hours ADD CONSTRAINT "FK_d4d7c9d84552c5488cc4e68ac06" FOREIGN KEY (business_id) REFERENCES t_business(id);
ALTER TABLE t_hours ADD CONSTRAINT "FK_7edce1846c726be8729da94c68b" FOREIGN KEY (day_id) REFERENCES t_days(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_photo_comment ADD CONSTRAINT "FK_a25bd5566d4892334db9b93b4ff" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_photo_comment ADD CONSTRAINT "FK_5f691231453016a73b881efcce9" FOREIGN KEY (photo_id) REFERENCES t_photo(id);
ALTER TABLE t_photo_comment_replies ADD CONSTRAINT "FK_f7c9dc334b6490793d402fd5709" FOREIGN KEY (comment_id) REFERENCES t_photo_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_photo_comment_replies ADD CONSTRAINT "FK_5c87d3c4e0e9b5bac7e13d655ae" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_photo_comment_replies ADD CONSTRAINT "FK_c65dd5b367b232c9bd8d0088104" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment ADD CONSTRAINT "FK_0c6c2da8e5d0ea2e9283cfd0bb3" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment ADD CONSTRAINT "FK_863094a95086518839f6b2c5b81" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment_replies ADD CONSTRAINT "FK_91dd94bf184b02c916d160768d9" FOREIGN KEY (comment_id) REFERENCES t_text_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_text_comment_replies ADD CONSTRAINT "FK_94b7688020275540f2f81e45c1f" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment_replies ADD CONSTRAINT "FK_9964632e254c6403d17681fe07c" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_video_comment ADD CONSTRAINT "FK_205378b58cdd060c3aec791cb0b" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_video_comment ADD CONSTRAINT "FK_701faf0d0560c553ec1cf3c867b" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_video_comment_replies ADD CONSTRAINT "FK_69aeaf2544a265802d7d1eda84b" FOREIGN KEY (comment_id) REFERENCES t_video_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_video_comment_replies ADD CONSTRAINT "FK_ea4a5d256d2d3e592459ccda20e" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_video_comment_replies ADD CONSTRAINT "FK_692b6a04ccf7aa668af06b8d7bd" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_eeb477829ab5d787007057f63cb" FOREIGN KEY ("targetId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_0f374b7c29f1f53e113330f9cb4" FOREIGN KEY ("articleId") REFERENCES t_article(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_cc7d843a880162589ddb0d56d60" FOREIGN KEY ("photoId") REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_d358d7c8a748dc9d92089b48ddb" FOREIGN KEY ("textId") REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_f6ec87772fe708eea08df801ab0" FOREIGN KEY ("videoId") REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_99214cbf26145afc34c57c41541" FOREIGN KEY ("sellerreviewId") REFERENCES t_seller_review(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_94abe551a95ba041ce9f7bbe055" FOREIGN KEY ("productId") REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_272e565ef73e595f5c94163e0cc" FOREIGN KEY ("productReviewId") REFERENCES t_product_review(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_d5adc12b709d6eab1e60c4f5623" FOREIGN KEY ("serviceReviewId") REFERENCES t_service_review(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_e6ee40a24c7d51956df16603d78" FOREIGN KEY ("serviceId") REFERENCES t_service(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_37e9ba2b109caa824be5b467251" FOREIGN KEY ("articleCommentId") REFERENCES t_article_comment(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_a88d79c41f5ac0b8ba7eb9f473b" FOREIGN KEY ("articleCommentReplyId") REFERENCES t_article_comment_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_6230a68b13e82f06ab22b5c4711" FOREIGN KEY ("photoCommentId") REFERENCES t_photo_comment(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_feb971b64a7150496b0149b82c7" FOREIGN KEY ("photoCommentReplyId") REFERENCES t_photo_comment_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_417ffd432c47dbd643fa7a827d6" FOREIGN KEY ("textCommentId") REFERENCES t_text_comment(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_105fe0692ecff8b2c129e583fdd" FOREIGN KEY ("textCommentReplyId") REFERENCES t_text_comment_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_fa2012dfc0a949bb7586ac74de8" FOREIGN KEY ("videoCommentId") REFERENCES t_video_comment(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_07f86ec42eee63eaaa368ed1c36" FOREIGN KEY ("videoCommentReplyId") REFERENCES t_video_comment_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_678b749b5850adf61ccdd4b6ced" FOREIGN KEY ("productReviewReplyId") REFERENCES t_product_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_f2c52d3bd2365501cd4d4bb6bf6" FOREIGN KEY ("serviceReviewReplyId") REFERENCES t_service_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_mentions ADD CONSTRAINT "FK_3ab9e09306811e5e845bed9dbc9" FOREIGN KEY ("sellerReviewReplyId") REFERENCES t_seller_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_message_attachment ADD CONSTRAINT "FK_d44b182239cc3b0ab697fed96f8" FOREIGN KEY (message_id) REFERENCES t_message(id) ON DELETE CASCADE;
ALTER TABLE t_message_attachment ADD CONSTRAINT "FK_55a37dc0128b8755737f7416026" FOREIGN KEY (chatroom_id) REFERENCES t_chatroom(id) ON DELETE CASCADE;
ALTER TABLE t_message_attachment ADD CONSTRAINT "FK_a2fd1fb1d131e3e5a4c47bd4de9" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_478d23250857f9ad58bacf1db48" FOREIGN KEY ("textId") REFERENCES t_text(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_b366a18fe393d680a8315ebae7d" FOREIGN KEY ("photoId") REFERENCES t_photo(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_8b1553392dc9779840c3995c12c" FOREIGN KEY ("videoId") REFERENCES t_video(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_6491ef304f384feb8023a1607e8" FOREIGN KEY ("articleId") REFERENCES t_article(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_2f115993f8f854ebd4b9820b165" FOREIGN KEY ("productId") REFERENCES t_product(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_4d231f422eeb04b9827fc2e2994" FOREIGN KEY ("serviceId") REFERENCES t_service(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_2c1f712a42da2408796d3263f48" FOREIGN KEY ("productReviewId") REFERENCES t_product_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_29577a7c98e1c44435e00e9a345" FOREIGN KEY ("serviceReviewId") REFERENCES t_service_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_a6c2c5dc815a42bb5810c086d86" FOREIGN KEY ("sellerReviewId") REFERENCES t_seller_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_235f58a5420e14ef0856605ad81" FOREIGN KEY ("issuerId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_6aa389cf5592d9d286c2a66cc72" FOREIGN KEY ("ownerId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_notifications ADD CONSTRAINT "FK_92aa19cfebba003b8c82b6ef7f4" FOREIGN KEY ("productVariantId") REFERENCES t_product_variant(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_order ADD CONSTRAINT "FK_01e601f83c27b53fed6c64d4f88" FOREIGN KEY (buyer_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_order ADD CONSTRAINT "FK_cb3fb4f54860f59bd8a173b25d2" FOREIGN KEY (seller_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_order ADD CONSTRAINT "FK_bdecbd24fbcf8a9ffdea09bd5aa" FOREIGN KEY (delivery_address_id) REFERENCES t_address(id) ON DELETE CASCADE;
ALTER TABLE t_order ADD CONSTRAINT "FK_818c71780238bdb89b1f8ca5f21" FOREIGN KEY (billing_address_id) REFERENCES t_address(id) ON DELETE CASCADE;
ALTER TABLE t_order_product ADD CONSTRAINT "FK_aa1b4e0484b429d143391e26d5f" FOREIGN KEY (order_id) REFERENCES t_order(id) ON DELETE CASCADE;
ALTER TABLE t_order_product ADD CONSTRAINT "FK_7d08b062a34133a930ece4418a9" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_order_product ADD CONSTRAINT "FK_16655d6f38366aefebc33f2391f" FOREIGN KEY (product_variant_id) REFERENCES t_product_variant(id) ON DELETE CASCADE;
ALTER TABLE t_photo_annotation ADD CONSTRAINT "FK_25ab23c20cb3ce4afcf74fddb6b" FOREIGN KEY (target_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_photo_annotation ADD CONSTRAINT "FK_c6f20d0a4fe95a7e12e291d74c5" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_photo_comment_like ADD CONSTRAINT "FK_30b668823a066d4006740dd88c7" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_photo_comment_like ADD CONSTRAINT "FK_1b033d72d97e0bb1e3b8797b6a3" FOREIGN KEY (comment_id) REFERENCES t_photo_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_photo_comment_like ADD CONSTRAINT "FK_6f39cbd82a241b3610c93675573" FOREIGN KEY (reply_id) REFERENCES t_photo_comment_replies(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_photo_comment_like ADD CONSTRAINT "FK_9341690c97d0ac9fc7435c8d64c" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_photo_like ADD CONSTRAINT "FK_3369e5fdb8423ed9c023bee2c4a" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_photo_like ADD CONSTRAINT "FK_dbb0770b00ad85537eafcb22cbc" FOREIGN KEY (photo_id) REFERENCES t_photo(id);
ALTER TABLE t_photo_view ADD CONSTRAINT "FK_b5787b9bb5a8d9658d56b2703c8" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_photo_view ADD CONSTRAINT "FK_59f521e779b0914c1f05ead614c" FOREIGN KEY (photo_id) REFERENCES t_photo(id);
ALTER TABLE t_product_review_likes_t_account ADD CONSTRAINT "FK_e483fc2911e375d657fcb805dfe" FOREIGN KEY ("tProductReviewId") REFERENCES t_product_review(id) ON DELETE CASCADE;
ALTER TABLE t_product_review_likes_t_account ADD CONSTRAINT "FK_e3718fd33901024aa7266230084" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_product_review_replies_likes_t_account ADD CONSTRAINT "FK_9b85063c135fffa25fb7d4e1420" FOREIGN KEY ("tProductReviewRepliesId") REFERENCES t_product_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_product_review_replies_likes_t_account ADD CONSTRAINT "FK_31d8840aa79ec08134746b4f2cc" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_product_variant_t_product_photos ADD CONSTRAINT "FK_0cdb47895dc4a030af5a5c0c32e" FOREIGN KEY ("variantId") REFERENCES t_product_variant(id) ON DELETE CASCADE;
ALTER TABLE t_product_variant_t_product_photos ADD CONSTRAINT "FK_e441673ce8154b92cc5c747230a" FOREIGN KEY ("photoId") REFERENCES t_product_photo(id) ON DELETE CASCADE;
ALTER TABLE t_profile_photo ADD CONSTRAINT "FK_f31147f584104cc782c9bd0bfcd" FOREIGN KEY (user_id) REFERENCES t_person(id) ON DELETE CASCADE;
ALTER TABLE t_report ADD CONSTRAINT "FK_5fefee3d1fd2ed0286b776a3591" FOREIGN KEY (target_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_report ADD CONSTRAINT "FK_99ea86edf3e764fe773f65430d6" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_report ADD CONSTRAINT "FK_fbbebbc2b02e8656bc5e185fa22" FOREIGN KEY (category_id) REFERENCES t_report_category(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE t_review_photo ADD CONSTRAINT "FK_bc5471818f7e25a3499ca1ab92f" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_review_photo ADD CONSTRAINT "FK_a7d200f67e184e667fbe4c79a2e" FOREIGN KEY ("productReviewId") REFERENCES t_product_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_review_photo ADD CONSTRAINT "FK_ee5feed2d10bd2021fb7c84a91d" FOREIGN KEY ("serviceReviewId") REFERENCES t_service_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_review_photo ADD CONSTRAINT "FK_85ba5800d85c6b219ad76e2358e" FOREIGN KEY ("sellerReviewId") REFERENCES t_seller_review(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_service_schedule ADD CONSTRAINT "FK_987e1bbf5a4ec3bfa95c1376fe0" FOREIGN KEY (service_id) REFERENCES t_service(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_schedule_date ADD CONSTRAINT "FK_8b18d5e97764496a0193b19edab" FOREIGN KEY ("scheduleId") REFERENCES t_service_schedule(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_schedule_days ADD CONSTRAINT "FK_c3995dfe480bcd8d159fde801ab" FOREIGN KEY ("scheduleId") REFERENCES t_service_schedule(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_schedule_days ADD CONSTRAINT "FK_c8e39ddee70608f4cd7bb5312a2" FOREIGN KEY ("dayId") REFERENCES t_days(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_schedule_days_intervals ADD CONSTRAINT "FK_abbfb135fad847d8f94172574b2" FOREIGN KEY ("scheduleByDayId") REFERENCES t_schedule_days(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_seller_review_likes_t_account ADD CONSTRAINT "FK_2a8c22c70e20681081f322409d3" FOREIGN KEY ("tSellerReviewId") REFERENCES t_seller_review(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review_likes_t_account ADD CONSTRAINT "FK_9545639072952b438f83e5322a5" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review_replies_likes_t_account ADD CONSTRAINT "FK_b87c96930b5caa20b38e9f96d3c" FOREIGN KEY ("tSellerReviewRepliesId") REFERENCES t_seller_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_seller_review_replies_likes_t_account ADD CONSTRAINT "FK_18922b4630ffc505a92aa2d836c" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_service_photo ADD CONSTRAINT "FK_2cbaf08ad70bbf4e51d9c3c9c64" FOREIGN KEY (service_id) REFERENCES t_service(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE t_service_review_likes_t_account ADD CONSTRAINT "FK_8762e3a598c7a313730ccf12b74" FOREIGN KEY ("tServiceReviewId") REFERENCES t_service_review(id) ON DELETE CASCADE;
ALTER TABLE t_service_review_likes_t_account ADD CONSTRAINT "FK_7305d28bfdc30ffe186cad52194" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_service_review_replies_likes_t_account ADD CONSTRAINT "FK_ddb945cd579b14e0f6f0bd2d1ff" FOREIGN KEY ("tServiceReviewRepliesId") REFERENCES t_service_review_replies(id) ON DELETE CASCADE;
ALTER TABLE t_service_review_replies_likes_t_account ADD CONSTRAINT "FK_06c01ee3f71eb8d31531de7ddf4" FOREIGN KEY ("tAccountId") REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_share ADD CONSTRAINT "FK_69488771f8df06c0e283670f51b" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_share ADD CONSTRAINT "FK_8edb04c10c29276769300d14c3f" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_share ADD CONSTRAINT "FK_29763ee09ee3a2882b67a56aa69" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_share ADD CONSTRAINT "FK_675074bc1aa506b4b724dbaa866" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_share ADD CONSTRAINT "FK_feb2fafe832322fdcb0993f57ae" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE;
ALTER TABLE t_share ADD CONSTRAINT "FK_c4105ba27ad4774428f5c1a6614" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment_like ADD CONSTRAINT "FK_94d35660ae5baf62735f570763f" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_text_comment_like ADD CONSTRAINT "FK_ebc0603ac08e08b1b3a9cdb523c" FOREIGN KEY (comment_id) REFERENCES t_text_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_text_comment_like ADD CONSTRAINT "FK_c1afe38f8c8601820d39442b7a8" FOREIGN KEY (reply_id) REFERENCES t_text_comment_replies(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_text_comment_like ADD CONSTRAINT "FK_34962ce23213f435fc2b84e9be9" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_text_like ADD CONSTRAINT "FK_5ed398d9bc25f8ca8a18ccdb63f" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_text_like ADD CONSTRAINT "FK_a17c3da6530f9ea4f6c72b69cea" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_text_view ADD CONSTRAINT "FK_9bf0dc207b37307379f78d832b2" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_text_view ADD CONSTRAINT "FK_ea613a2921d8f138e5541bd5aa4" FOREIGN KEY (text_id) REFERENCES t_text(id);
ALTER TABLE t_user_business_user_category ADD CONSTRAINT "FK_39ce3243077646ec31709c4f925" FOREIGN KEY (category_id) REFERENCES t_business_user_category(id) ON DELETE CASCADE;
ALTER TABLE t_video_comment_like ADD CONSTRAINT "FK_315595f46bb9cf8654ffdbdea63" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_video_comment_like ADD CONSTRAINT "FK_85a247580f74fe5309dee2296c6" FOREIGN KEY (comment_id) REFERENCES t_video_comment(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_video_comment_like ADD CONSTRAINT "FK_ba0132a450bfcb9adf616b5041f" FOREIGN KEY (reply_id) REFERENCES t_video_comment_replies(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE t_video_comment_like ADD CONSTRAINT "FK_1a5cdbf945847dc72aa5fe49d3c" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_video_like ADD CONSTRAINT "FK_ee71f80444a0c0666f54538c3d1" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_video_like ADD CONSTRAINT "FK_a1f778acc37f7d325a0a126b21c" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_video_view ADD CONSTRAINT "FK_08c390d8dd9b5f613c3f27b5c8d" FOREIGN KEY (user_id) REFERENCES t_account(id);
ALTER TABLE t_video_view ADD CONSTRAINT "FK_cc4af00ad15106ab9e4066a862e" FOREIGN KEY (video_id) REFERENCES t_video(id);
ALTER TABLE t_view ADD CONSTRAINT "FK_b1645fb71714698a25768cd334b" FOREIGN KEY (user_id) REFERENCES t_account(id) ON DELETE CASCADE;
ALTER TABLE t_view ADD CONSTRAINT "FK_389d3a1f4893d3f42f23eb914ec" FOREIGN KEY (text_id) REFERENCES t_text(id) ON DELETE CASCADE;
ALTER TABLE t_view ADD CONSTRAINT "FK_91ab9ac6cad20476cc7538a917a" FOREIGN KEY (photo_id) REFERENCES t_photo(id) ON DELETE CASCADE;
ALTER TABLE t_view ADD CONSTRAINT "FK_05f3f42178d1325102bef0644fe" FOREIGN KEY (video_id) REFERENCES t_video(id) ON DELETE CASCADE;
ALTER TABLE t_view ADD CONSTRAINT "FK_c65cc890988c50c0d3329cabdb1" FOREIGN KEY (article_id) REFERENCES t_article(id) ON DELETE CASCADE;
ALTER TABLE t_view ADD CONSTRAINT "FK_61ec15ef08575cc2c536c09336f" FOREIGN KEY (product_id) REFERENCES t_product(id) ON DELETE CASCADE;

-- Validate foreign key constraints. These can fail if there was unvalidated data during the dump.
ALTER TABLE t_business VALIDATE CONSTRAINT "FK_6e870aac767cc2a0bbca1730050";
ALTER TABLE t_business VALIDATE CONSTRAINT "FK_d608f158f657779a0b20a04bd07";
ALTER TABLE t_business VALIDATE CONSTRAINT "FK_e269995d4d6e50563a08a9016b6";
ALTER TABLE t_address VALIDATE CONSTRAINT "FK_dc133bee8ecf186bfcab7e0d2df";
ALTER TABLE t_address VALIDATE CONSTRAINT "FK_ae3a39cc32d9b206c55fc9c4aa0";
ALTER TABLE t_person VALIDATE CONSTRAINT "FK_1b6f23f5feaf9294e1cb27a8f1c";
ALTER TABLE t_person VALIDATE CONSTRAINT "FK_5f30eec8cabee18d383292cda3b";
ALTER TABLE t_account VALIDATE CONSTRAINT "FK_a18399d9a2042373ee13fd4e593";
ALTER TABLE t_account VALIDATE CONSTRAINT "FK_37b15b7df488f6f0daac2f8015c";
ALTER TABLE t_account VALIDATE CONSTRAINT "FK_a9cf1e14d962521484050d231a1";
ALTER TABLE t_account VALIDATE CONSTRAINT "FK_e015a7e38c56f4315214287c14e";
ALTER TABLE t_account_blacklist_t_account VALIDATE CONSTRAINT "FK_fc056810fc2513bbbfc6419a0b5";
ALTER TABLE t_account_blacklist_t_account VALIDATE CONSTRAINT "FK_fe827b9ed2028fba31442501b07";
ALTER TABLE t_shop_category VALIDATE CONSTRAINT "FK_2d1323724d239cb787b0f4c5af7";
ALTER TABLE t_product_photo VALIDATE CONSTRAINT "FK_695941bdc0db4f13bba822f22ad";
ALTER TABLE t_product VALIDATE CONSTRAINT "FK_45054858963ac0a72830b18adf1";
ALTER TABLE t_product VALIDATE CONSTRAINT "FK_c4cb695880ad06d3e24b4c2fd46";
ALTER TABLE t_product VALIDATE CONSTRAINT "FK_251ba14ed3702ab8bafa9ef91c0";
ALTER TABLE t_product VALIDATE CONSTRAINT "FK_885547ba0f6e14a4c45486c1359";
ALTER TABLE t_product VALIDATE CONSTRAINT "FK_e4c8164fb9c0e01953f1cbe9e1c";
ALTER TABLE t_product_review VALIDATE CONSTRAINT "FK_660bd3d57a18e21acb61904b865";
ALTER TABLE t_product_review VALIDATE CONSTRAINT "FK_611cf5777fd1d9226f22bd5cbd9";
ALTER TABLE t_product_review VALIDATE CONSTRAINT "FK_1626973a7ecc572cb4d15e92ed0";
ALTER TABLE t_account_product_reviews_likes_t_product_review VALIDATE CONSTRAINT "FK_f711073fa7c4e10bdd686e0f207";
ALTER TABLE t_account_product_reviews_likes_t_product_review VALIDATE CONSTRAINT "FK_2dd67ea2bb4227c14aa8a990fe6";
ALTER TABLE t_product_review_replies VALIDATE CONSTRAINT "FK_e67b423683e651eb0615bfe3a2a";
ALTER TABLE t_product_review_replies VALIDATE CONSTRAINT "FK_f58f40ef57ec1a29fb74f22c7ae";
ALTER TABLE t_product_review_replies VALIDATE CONSTRAINT "FK_4689d99dc7eca19aac1aff89204";
ALTER TABLE t_account_product_reviews_replies_likes_t_product_review_replies VALIDATE CONSTRAINT "FK_e8ead629df496341580f7087fb2";
ALTER TABLE t_account_product_reviews_replies_likes_t_product_review_replies VALIDATE CONSTRAINT "FK_7a4955c2b07ad3f92a091de5c9b";
ALTER TABLE t_account_product_reviews_replies_t_product_review_replies VALIDATE CONSTRAINT "FK_475b4f566c68581247dfcb2c305";
ALTER TABLE t_account_product_reviews_replies_t_product_review_replies VALIDATE CONSTRAINT "FK_c621ba8d0b19f9a08b8cec949a0";
ALTER TABLE t_seller_review VALIDATE CONSTRAINT "FK_be93a478544b14262a99a8838b3";
ALTER TABLE t_seller_review VALIDATE CONSTRAINT "FK_8838197aabdabc1f72ce3b7c306";
ALTER TABLE t_seller_review VALIDATE CONSTRAINT "FK_9231df22c5dc5c2006e3244f9b8";
ALTER TABLE t_account_seller_reviews_likes_t_seller_review VALIDATE CONSTRAINT "FK_66d4b247265510e63fdcc75bcbf";
ALTER TABLE t_account_seller_reviews_likes_t_seller_review VALIDATE CONSTRAINT "FK_83432966d9d78e0f4405badaab1";
ALTER TABLE t_seller_review_replies VALIDATE CONSTRAINT "FK_a388eebedd1e25eaad6c49986fa";
ALTER TABLE t_seller_review_replies VALIDATE CONSTRAINT "FK_0355fea30ef0ccbe1a868aa938d";
ALTER TABLE t_seller_review_replies VALIDATE CONSTRAINT "FK_bf7c0d8714b18a237501e749246";
ALTER TABLE t_account_seller_reviews_replies_likes_t_seller_review_replies VALIDATE CONSTRAINT "FK_588ea502be4a7bdf2bd0f005ceb";
ALTER TABLE t_account_seller_reviews_replies_likes_t_seller_review_replies VALIDATE CONSTRAINT "FK_bb1b5027012679e7112cb220c9f";
ALTER TABLE t_account_seller_reviews_replies_t_seller_review_replies VALIDATE CONSTRAINT "FK_4404e8d386f5547b36099aff5d2";
ALTER TABLE t_account_seller_reviews_replies_t_seller_review_replies VALIDATE CONSTRAINT "FK_1a2fa4a19071300cdee74de30b4";
ALTER TABLE t_service VALIDATE CONSTRAINT "FK_d6b26007534e6761e1c4e65bcb7";
ALTER TABLE t_service VALIDATE CONSTRAINT "FK_b779a48fb2db76d52987c14c2e3";
ALTER TABLE t_service_review VALIDATE CONSTRAINT "FK_1288cda092c439239c91d562484";
ALTER TABLE t_service_review VALIDATE CONSTRAINT "FK_4bce49de508fc298cac740a03b5";
ALTER TABLE t_service_review VALIDATE CONSTRAINT "FK_dde0c7acb9a4fa3445d84dd53e8";
ALTER TABLE t_account_service_reviews_likes_t_service_review VALIDATE CONSTRAINT "FK_e9ca884fa9ad48df5fcd168c044";
ALTER TABLE t_account_service_reviews_likes_t_service_review VALIDATE CONSTRAINT "FK_cca6de899e6c57b6d90f05d6131";
ALTER TABLE t_service_review_replies VALIDATE CONSTRAINT "FK_4484e7182b1b15f13b0470186a0";
ALTER TABLE t_service_review_replies VALIDATE CONSTRAINT "FK_c1d7e93610f3b3d62d5542ee5c8";
ALTER TABLE t_service_review_replies VALIDATE CONSTRAINT "FK_7f95aa959f15274bbf145565d38";
ALTER TABLE t_account_service_reviews_replies_likes_t_service_review_replies VALIDATE CONSTRAINT "FK_80a0bacf97d740c40cd21c08c9d";
ALTER TABLE t_account_service_reviews_replies_likes_t_service_review_replies VALIDATE CONSTRAINT "FK_f1f1c69857942a5e141e9da8522";
ALTER TABLE t_account_service_reviews_replies_t_service_review_replies VALIDATE CONSTRAINT "FK_71fffd2c5ff9fd018bb4267314c";
ALTER TABLE t_account_service_reviews_replies_t_service_review_replies VALIDATE CONSTRAINT "FK_1283472915eb3aab7b4126dd7ca";
ALTER TABLE t_article VALIDATE CONSTRAINT "FK_9ac0a29f753ed5cac0d1a4321ba";
ALTER TABLE t_article VALIDATE CONSTRAINT "FK_d43809009ac48a652788386bea1";
ALTER TABLE t_article_comment VALIDATE CONSTRAINT "FK_c5df8481acb3ec66783fda56c51";
ALTER TABLE t_article_comment VALIDATE CONSTRAINT "FK_d69e86b9ccd74f68e63e16bba4b";
ALTER TABLE t_article_comment_replies VALIDATE CONSTRAINT "FK_d5205061e115011e8efa4b75c2f";
ALTER TABLE t_article_comment_replies VALIDATE CONSTRAINT "FK_2c3249b01bb62f3ffcd8e4ce59b";
ALTER TABLE t_article_comment_replies VALIDATE CONSTRAINT "FK_03da36fc98270a36927619e0349";
ALTER TABLE t_article_comment_like VALIDATE CONSTRAINT "FK_2f857dad4c7de1443478f2c7c7d";
ALTER TABLE t_article_comment_like VALIDATE CONSTRAINT "FK_fd9293bcde871e023e9c0c78e16";
ALTER TABLE t_article_comment_like VALIDATE CONSTRAINT "FK_38c83e283f7e4754cef6a8c2b7c";
ALTER TABLE t_article_comment_like VALIDATE CONSTRAINT "FK_fd58ff9a644e5fc49ab6113c9f4";
ALTER TABLE t_article_like VALIDATE CONSTRAINT "FK_00be0b022149db48d5a97921df4";
ALTER TABLE t_article_like VALIDATE CONSTRAINT "FK_9da4512fc30d1907eb3ac3916bf";
ALTER TABLE t_article_photo VALIDATE CONSTRAINT "FK_bd5f3c88dfe2de55f177f2b2a72";
ALTER TABLE t_article_view VALIDATE CONSTRAINT "FK_819087eceaa34ed8ffe82e3fb27";
ALTER TABLE t_article_view VALIDATE CONSTRAINT "FK_d5e33c2f327a3f8127d6b9fb55d";
ALTER TABLE t_bank VALIDATE CONSTRAINT "FK_631acaea16abc66e7cf763f06ad";
ALTER TABLE t_booking VALIDATE CONSTRAINT "FK_fc481f354786482f37adadad680";
ALTER TABLE t_booking VALIDATE CONSTRAINT "FK_8996cbb5f77368677b8b426dee5";
ALTER TABLE t_booking VALIDATE CONSTRAINT "FK_7ac87a8295c1e57a9ab49d7c25c";
ALTER TABLE t_booking VALIDATE CONSTRAINT "FK_d0cc1171cbb9057a783f65710f1";
ALTER TABLE t_booking VALIDATE CONSTRAINT "FK_7b0c47906c6b219d60538c0bbb2";
ALTER TABLE t_booking_appointments VALIDATE CONSTRAINT "FK_1eb29647cb75a44260748c5092b";
ALTER TABLE t_text VALIDATE CONSTRAINT "FK_242e82f7c6adf503b79043e80b1";
ALTER TABLE t_photo VALIDATE CONSTRAINT "FK_f7442330e2a46235db02d165836";
ALTER TABLE t_photo VALIDATE CONSTRAINT "FK_f6d9d497516a33a35003796a37c";
ALTER TABLE t_video VALIDATE CONSTRAINT "FK_e70f50a324682a7b52545a6e2b5";
ALTER TABLE t_video VALIDATE CONSTRAINT "FK_e01f6ac798813d598a91fa3930a";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_e17dc82a08cd8da59f7e831330e";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_7a20246b7dc1966d51222f1217d";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_7f3d4605368f1bbe200805a21a8";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_e1f00b4dfa6f8cc34b524d2c22a";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_e991c5cbb90e41dd581e7e3ee5a";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_5786afa3f5f20f9a08bbc71346c";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_883b1491144e3c76eeb53643c55";
ALTER TABLE t_bookmarks VALIDATE CONSTRAINT "FK_0712cbce462ecd7770d968baa4c";
ALTER TABLE t_product_variant VALIDATE CONSTRAINT "FK_54cb1b5feee114a2a46f39cdc0a";
ALTER TABLE t_cart_product VALIDATE CONSTRAINT "FK_163dbcfd5a10909c6a9c55e7634";
ALTER TABLE t_cart_product VALIDATE CONSTRAINT "FK_bc5c7493732ee0ef7a76ae62f55";
ALTER TABLE t_cart_product VALIDATE CONSTRAINT "FK_0226892fb83f0e6f04fd7c07550";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_7aef1713d0d8ae2d08ec0a69d92";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_0623f6ce8190480372024845761";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_285a93487e9c8f44eb9a141f420";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_0d3eff2cf377d3aeb81e9e24df9";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_725da9ab95c73ad42ba931b448f";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_d9c65b8d75d4ea1f4e60ca83939";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_3dbc933de81bad609549823e5b0";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_b9ba907c408c60674f1911c1a0d";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_4d5288130018cd5bb414b0c9c3e";
ALTER TABLE t_message VALIDATE CONSTRAINT "FK_ffdef196f083b2c7d886abf646d";
ALTER TABLE t_chatroom_user VALIDATE CONSTRAINT "FK_923b7160e827046cf221f68122c";
ALTER TABLE t_chatroom_user VALIDATE CONSTRAINT "FK_21566604d65d58adc527c2979bd";
ALTER TABLE t_chatroom_user VALIDATE CONSTRAINT "FK_88e695691b3ba95913a0cff17c6";
ALTER TABLE t_contact VALIDATE CONSTRAINT "FK_ca821105d7986c98db6d39c3e4b";
ALTER TABLE t_followings VALIDATE CONSTRAINT "FK_b818092d1ced5a1910b392d1975";
ALTER TABLE t_followings VALIDATE CONSTRAINT "FK_e4b6ea262c5c80358090b82ac66";
ALTER TABLE t_followings_request VALIDATE CONSTRAINT "FK_8bc4d5a28feeffcb9a8fc2c970a";
ALTER TABLE t_followings_request VALIDATE CONSTRAINT "FK_2fa247efdbc9a14f68e49fc9fb3";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_ac74d00d3e335a645dd222cad1d";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_729199c811532a10df28afcea36";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_9485ccfbc9b48f6c349307e243c";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_9e418db5f56016f946801f7740a";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_37562e8b412412cf04a26122f8b";
ALTER TABLE t_hashtag VALIDATE CONSTRAINT "FK_a5c447ada045b8ead93a20ff5ae";
ALTER TABLE t_hours VALIDATE CONSTRAINT "FK_d4d7c9d84552c5488cc4e68ac06";
ALTER TABLE t_hours VALIDATE CONSTRAINT "FK_7edce1846c726be8729da94c68b";
ALTER TABLE t_photo_comment VALIDATE CONSTRAINT "FK_a25bd5566d4892334db9b93b4ff";
ALTER TABLE t_photo_comment VALIDATE CONSTRAINT "FK_5f691231453016a73b881efcce9";
ALTER TABLE t_photo_comment_replies VALIDATE CONSTRAINT "FK_f7c9dc334b6490793d402fd5709";
ALTER TABLE t_photo_comment_replies VALIDATE CONSTRAINT "FK_5c87d3c4e0e9b5bac7e13d655ae";
ALTER TABLE t_photo_comment_replies VALIDATE CONSTRAINT "FK_c65dd5b367b232c9bd8d0088104";
ALTER TABLE t_text_comment VALIDATE CONSTRAINT "FK_0c6c2da8e5d0ea2e9283cfd0bb3";
ALTER TABLE t_text_comment VALIDATE CONSTRAINT "FK_863094a95086518839f6b2c5b81";
ALTER TABLE t_text_comment_replies VALIDATE CONSTRAINT "FK_91dd94bf184b02c916d160768d9";
ALTER TABLE t_text_comment_replies VALIDATE CONSTRAINT "FK_94b7688020275540f2f81e45c1f";
ALTER TABLE t_text_comment_replies VALIDATE CONSTRAINT "FK_9964632e254c6403d17681fe07c";
ALTER TABLE t_video_comment VALIDATE CONSTRAINT "FK_205378b58cdd060c3aec791cb0b";
ALTER TABLE t_video_comment VALIDATE CONSTRAINT "FK_701faf0d0560c553ec1cf3c867b";
ALTER TABLE t_video_comment_replies VALIDATE CONSTRAINT "FK_69aeaf2544a265802d7d1eda84b";
ALTER TABLE t_video_comment_replies VALIDATE CONSTRAINT "FK_ea4a5d256d2d3e592459ccda20e";
ALTER TABLE t_video_comment_replies VALIDATE CONSTRAINT "FK_692b6a04ccf7aa668af06b8d7bd";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_eeb477829ab5d787007057f63cb";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_0f374b7c29f1f53e113330f9cb4";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_cc7d843a880162589ddb0d56d60";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_d358d7c8a748dc9d92089b48ddb";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_f6ec87772fe708eea08df801ab0";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_99214cbf26145afc34c57c41541";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_94abe551a95ba041ce9f7bbe055";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_272e565ef73e595f5c94163e0cc";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_d5adc12b709d6eab1e60c4f5623";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_e6ee40a24c7d51956df16603d78";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_37e9ba2b109caa824be5b467251";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_a88d79c41f5ac0b8ba7eb9f473b";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_6230a68b13e82f06ab22b5c4711";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_feb971b64a7150496b0149b82c7";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_417ffd432c47dbd643fa7a827d6";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_105fe0692ecff8b2c129e583fdd";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_fa2012dfc0a949bb7586ac74de8";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_07f86ec42eee63eaaa368ed1c36";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_678b749b5850adf61ccdd4b6ced";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_f2c52d3bd2365501cd4d4bb6bf6";
ALTER TABLE t_mentions VALIDATE CONSTRAINT "FK_3ab9e09306811e5e845bed9dbc9";
ALTER TABLE t_message_attachment VALIDATE CONSTRAINT "FK_d44b182239cc3b0ab697fed96f8";
ALTER TABLE t_message_attachment VALIDATE CONSTRAINT "FK_55a37dc0128b8755737f7416026";
ALTER TABLE t_message_attachment VALIDATE CONSTRAINT "FK_a2fd1fb1d131e3e5a4c47bd4de9";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_478d23250857f9ad58bacf1db48";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_b366a18fe393d680a8315ebae7d";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_8b1553392dc9779840c3995c12c";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_6491ef304f384feb8023a1607e8";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_2f115993f8f854ebd4b9820b165";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_4d231f422eeb04b9827fc2e2994";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_2c1f712a42da2408796d3263f48";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_29577a7c98e1c44435e00e9a345";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_a6c2c5dc815a42bb5810c086d86";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_235f58a5420e14ef0856605ad81";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_6aa389cf5592d9d286c2a66cc72";
ALTER TABLE t_notifications VALIDATE CONSTRAINT "FK_92aa19cfebba003b8c82b6ef7f4";
ALTER TABLE t_order VALIDATE CONSTRAINT "FK_01e601f83c27b53fed6c64d4f88";
ALTER TABLE t_order VALIDATE CONSTRAINT "FK_cb3fb4f54860f59bd8a173b25d2";
ALTER TABLE t_order VALIDATE CONSTRAINT "FK_bdecbd24fbcf8a9ffdea09bd5aa";
ALTER TABLE t_order VALIDATE CONSTRAINT "FK_818c71780238bdb89b1f8ca5f21";
ALTER TABLE t_order_product VALIDATE CONSTRAINT "FK_aa1b4e0484b429d143391e26d5f";
ALTER TABLE t_order_product VALIDATE CONSTRAINT "FK_7d08b062a34133a930ece4418a9";
ALTER TABLE t_order_product VALIDATE CONSTRAINT "FK_16655d6f38366aefebc33f2391f";
ALTER TABLE t_photo_annotation VALIDATE CONSTRAINT "FK_25ab23c20cb3ce4afcf74fddb6b";
ALTER TABLE t_photo_annotation VALIDATE CONSTRAINT "FK_c6f20d0a4fe95a7e12e291d74c5";
ALTER TABLE t_photo_comment_like VALIDATE CONSTRAINT "FK_30b668823a066d4006740dd88c7";
ALTER TABLE t_photo_comment_like VALIDATE CONSTRAINT "FK_1b033d72d97e0bb1e3b8797b6a3";
ALTER TABLE t_photo_comment_like VALIDATE CONSTRAINT "FK_6f39cbd82a241b3610c93675573";
ALTER TABLE t_photo_comment_like VALIDATE CONSTRAINT "FK_9341690c97d0ac9fc7435c8d64c";
ALTER TABLE t_photo_like VALIDATE CONSTRAINT "FK_3369e5fdb8423ed9c023bee2c4a";
ALTER TABLE t_photo_like VALIDATE CONSTRAINT "FK_dbb0770b00ad85537eafcb22cbc";
ALTER TABLE t_photo_view VALIDATE CONSTRAINT "FK_b5787b9bb5a8d9658d56b2703c8";
ALTER TABLE t_photo_view VALIDATE CONSTRAINT "FK_59f521e779b0914c1f05ead614c";
ALTER TABLE t_product_review_likes_t_account VALIDATE CONSTRAINT "FK_e483fc2911e375d657fcb805dfe";
ALTER TABLE t_product_review_likes_t_account VALIDATE CONSTRAINT "FK_e3718fd33901024aa7266230084";
ALTER TABLE t_product_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_9b85063c135fffa25fb7d4e1420";
ALTER TABLE t_product_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_31d8840aa79ec08134746b4f2cc";
ALTER TABLE t_product_variant_t_product_photos VALIDATE CONSTRAINT "FK_0cdb47895dc4a030af5a5c0c32e";
ALTER TABLE t_product_variant_t_product_photos VALIDATE CONSTRAINT "FK_e441673ce8154b92cc5c747230a";
ALTER TABLE t_profile_photo VALIDATE CONSTRAINT "FK_f31147f584104cc782c9bd0bfcd";
ALTER TABLE t_report VALIDATE CONSTRAINT "FK_5fefee3d1fd2ed0286b776a3591";
ALTER TABLE t_report VALIDATE CONSTRAINT "FK_99ea86edf3e764fe773f65430d6";
ALTER TABLE t_report VALIDATE CONSTRAINT "FK_fbbebbc2b02e8656bc5e185fa22";
ALTER TABLE t_review_photo VALIDATE CONSTRAINT "FK_bc5471818f7e25a3499ca1ab92f";
ALTER TABLE t_review_photo VALIDATE CONSTRAINT "FK_a7d200f67e184e667fbe4c79a2e";
ALTER TABLE t_review_photo VALIDATE CONSTRAINT "FK_ee5feed2d10bd2021fb7c84a91d";
ALTER TABLE t_review_photo VALIDATE CONSTRAINT "FK_85ba5800d85c6b219ad76e2358e";
ALTER TABLE t_service_schedule VALIDATE CONSTRAINT "FK_987e1bbf5a4ec3bfa95c1376fe0";
ALTER TABLE t_schedule_date VALIDATE CONSTRAINT "FK_8b18d5e97764496a0193b19edab";
ALTER TABLE t_schedule_days VALIDATE CONSTRAINT "FK_c3995dfe480bcd8d159fde801ab";
ALTER TABLE t_schedule_days VALIDATE CONSTRAINT "FK_c8e39ddee70608f4cd7bb5312a2";
ALTER TABLE t_schedule_days_intervals VALIDATE CONSTRAINT "FK_abbfb135fad847d8f94172574b2";
ALTER TABLE t_seller_review_likes_t_account VALIDATE CONSTRAINT "FK_2a8c22c70e20681081f322409d3";
ALTER TABLE t_seller_review_likes_t_account VALIDATE CONSTRAINT "FK_9545639072952b438f83e5322a5";
ALTER TABLE t_seller_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_b87c96930b5caa20b38e9f96d3c";
ALTER TABLE t_seller_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_18922b4630ffc505a92aa2d836c";
ALTER TABLE t_service_photo VALIDATE CONSTRAINT "FK_2cbaf08ad70bbf4e51d9c3c9c64";
ALTER TABLE t_service_review_likes_t_account VALIDATE CONSTRAINT "FK_8762e3a598c7a313730ccf12b74";
ALTER TABLE t_service_review_likes_t_account VALIDATE CONSTRAINT "FK_7305d28bfdc30ffe186cad52194";
ALTER TABLE t_service_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_ddb945cd579b14e0f6f0bd2d1ff";
ALTER TABLE t_service_review_replies_likes_t_account VALIDATE CONSTRAINT "FK_06c01ee3f71eb8d31531de7ddf4";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_69488771f8df06c0e283670f51b";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_8edb04c10c29276769300d14c3f";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_29763ee09ee3a2882b67a56aa69";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_675074bc1aa506b4b724dbaa866";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_feb2fafe832322fdcb0993f57ae";
ALTER TABLE t_share VALIDATE CONSTRAINT "FK_c4105ba27ad4774428f5c1a6614";
ALTER TABLE t_text_comment_like VALIDATE CONSTRAINT "FK_94d35660ae5baf62735f570763f";
ALTER TABLE t_text_comment_like VALIDATE CONSTRAINT "FK_ebc0603ac08e08b1b3a9cdb523c";
ALTER TABLE t_text_comment_like VALIDATE CONSTRAINT "FK_c1afe38f8c8601820d39442b7a8";
ALTER TABLE t_text_comment_like VALIDATE CONSTRAINT "FK_34962ce23213f435fc2b84e9be9";
ALTER TABLE t_text_like VALIDATE CONSTRAINT "FK_5ed398d9bc25f8ca8a18ccdb63f";
ALTER TABLE t_text_like VALIDATE CONSTRAINT "FK_a17c3da6530f9ea4f6c72b69cea";
ALTER TABLE t_text_view VALIDATE CONSTRAINT "FK_9bf0dc207b37307379f78d832b2";
ALTER TABLE t_text_view VALIDATE CONSTRAINT "FK_ea613a2921d8f138e5541bd5aa4";
ALTER TABLE t_user_business_user_category VALIDATE CONSTRAINT "FK_39ce3243077646ec31709c4f925";
ALTER TABLE t_video_comment_like VALIDATE CONSTRAINT "FK_315595f46bb9cf8654ffdbdea63";
ALTER TABLE t_video_comment_like VALIDATE CONSTRAINT "FK_85a247580f74fe5309dee2296c6";
ALTER TABLE t_video_comment_like VALIDATE CONSTRAINT "FK_ba0132a450bfcb9adf616b5041f";
ALTER TABLE t_video_comment_like VALIDATE CONSTRAINT "FK_1a5cdbf945847dc72aa5fe49d3c";
ALTER TABLE t_video_like VALIDATE CONSTRAINT "FK_ee71f80444a0c0666f54538c3d1";
ALTER TABLE t_video_like VALIDATE CONSTRAINT "FK_a1f778acc37f7d325a0a126b21c";
ALTER TABLE t_video_view VALIDATE CONSTRAINT "FK_08c390d8dd9b5f613c3f27b5c8d";
ALTER TABLE t_video_view VALIDATE CONSTRAINT "FK_cc4af00ad15106ab9e4066a862e";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_b1645fb71714698a25768cd334b";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_389d3a1f4893d3f42f23eb914ec";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_91ab9ac6cad20476cc7538a917a";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_05f3f42178d1325102bef0644fe";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_c65cc890988c50c0d3329cabdb1";
ALTER TABLE t_view VALIDATE CONSTRAINT "FK_61ec15ef08575cc2c536c09336f";
