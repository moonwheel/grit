#!/bin/bash
npm run build &&
npm run db:sync &&
npm run db:seed &&
npm run db:migrate
