#!/bin/bash
helm init --wait --upgrade --force-upgrade
helm delete staging

kubectl delete configmap logstash-pipeline
kubectl delete configmap logstash-jdbc-jar-driver

sleep 15

kubectl create configmap logstash-pipeline --from-file=./logstash/logstash.conf
kubectl create configmap logstash-jdbc-jar-driver --from-file=./logstash/postgresql-42.2.8.jar

helm upgrade --install --force staging ./chart 